.class public Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;
.super Ljava/lang/Object;
.source "KeystoreHelper.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "KeyStoreHelper"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mKeyStore:Ljava/security/KeyStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->mContext:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private getCertificate(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    .locals 12
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 84
    const-string v9, "KeyStoreHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " getCertificate Entry for appId:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    if-nez p1, :cond_0

    .line 86
    const-string v9, "KeyStoreHelper"

    const-string v10, " getCertificate Id is NULL"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 152
    :goto_0
    return-object v0

    .line 90
    :cond_0
    const/4 v3, 0x0

    .line 91
    .local v3, "error":Z
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 92
    const-string v10, "Acms_Keystore"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 91
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 93
    .local v5, "filenameKeyStore":Ljava/lang/String;
    const-string v9, "KeyStoreHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "getCertChain keystore location "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const/4 v6, 0x0

    .line 95
    .local v6, "fis":Ljava/io/FileInputStream;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    .local v4, "f":Ljava/io/File;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 98
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    :try_start_1
    const-string v9, "PKCS12"

    invoke-static {v9}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->mKeyStore:Ljava/security/KeyStore;

    .line 99
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->mKeyStore:Ljava/security/KeyStore;

    .line 100
    const-string v10, "8Q!@hs1#{1:2c$h"

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    .line 99
    invoke-virtual {v9, v7, v10}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    move-object v6, v7

    .line 123
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :goto_1
    if-eqz v6, :cond_1

    .line 125
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 130
    :cond_1
    :goto_2
    if-eqz v3, :cond_2

    move-object v0, v8

    .line 131
    goto :goto_0

    .line 102
    :catch_0
    move-exception v2

    .line 103
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_3
    const-string v9, "KeyStoreHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "getCertificate FileNotFoundException occured"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 105
    const/4 v3, 0x1

    goto :goto_1

    .line 106
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 107
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    :goto_4
    const-string v9, "KeyStoreHelper"

    const-string v10, "getCertificate NoSuchAlgorithmException occured"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 109
    const/4 v3, 0x1

    goto :goto_1

    .line 110
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v2

    .line 111
    .local v2, "e":Ljava/security/cert/CertificateException;
    :goto_5
    const-string v9, "KeyStoreHelper"

    const-string v10, "getCertificate CertificateException occured"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v2}, Ljava/security/cert/CertificateException;->printStackTrace()V

    .line 113
    const/4 v3, 0x1

    goto :goto_1

    .line 114
    .end local v2    # "e":Ljava/security/cert/CertificateException;
    :catch_3
    move-exception v2

    .line 115
    .local v2, "e":Ljava/security/KeyStoreException;
    :goto_6
    const-string v9, "KeyStoreHelper"

    const-string v10, "getCertificate KeyStoreException occured"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v2}, Ljava/security/KeyStoreException;->printStackTrace()V

    .line 117
    const/4 v3, 0x1

    goto :goto_1

    .line 118
    .end local v2    # "e":Ljava/security/KeyStoreException;
    :catch_4
    move-exception v2

    .line 119
    .local v2, "e":Ljava/io/IOException;
    :goto_7
    const-string v9, "KeyStoreHelper"

    const-string v10, "getCertificate IOException occured"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 121
    const/4 v3, 0x1

    goto :goto_1

    .line 126
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 127
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 133
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v0, 0x0

    .line 134
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    const/4 v1, 0x0

    .line 137
    .local v1, "certs":[Ljava/security/cert/Certificate;
    :try_start_3
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v9, p1}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v1

    .line 139
    if-nez v1, :cond_3

    .line 140
    const-string v9, "KeyStoreHelper"

    const-string v10, "Given appId not present in Keystore"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 141
    goto/16 :goto_0

    .line 145
    :cond_3
    const/4 v9, 0x0

    aget-object v0, v1, v9

    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    check-cast v0, Ljava/security/cert/X509Certificate;
    :try_end_3
    .catch Ljava/security/KeyStoreException; {:try_start_3 .. :try_end_3} :catch_6

    .line 151
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    const-string v8, "KeyStoreHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getCertificate Exit for Id:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 147
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    :catch_6
    move-exception v2

    .line 148
    .local v2, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v2}, Ljava/security/KeyStoreException;->printStackTrace()V

    move-object v0, v8

    .line 149
    goto/16 :goto_0

    .line 118
    .end local v1    # "certs":[Ljava/security/cert/Certificate;
    .end local v2    # "e":Ljava/security/KeyStoreException;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_7

    .line 114
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .line 110
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_9
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 106
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_a
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 102
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_b
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method


# virtual methods
.method public getKeystoreContent(Ljava/lang/String;)[B
    .locals 7
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->getCertificate(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 65
    .local v0, "appCert":Ljava/security/cert/X509Certificate;
    const-string v4, "KeyStoreHelper"

    const-string v5, "Reading the Certificate from Keystore"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    if-nez v0, :cond_0

    .line 68
    const-string v4, "KeyStoreHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getKeystoreContent No certificate present in Keystore for appId"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 80
    :goto_0
    return-object v1

    .line 71
    :cond_0
    const/4 v1, 0x0

    .line 74
    .local v1, "cert":[B
    :try_start_0
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getEncoded()[B
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 75
    :catch_0
    move-exception v2

    .line 76
    .local v2, "e":Ljava/security/cert/CertificateEncodingException;
    const-string v4, "KeyStoreHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getKeystoreContent CertificateEncodingException for appId"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v2}, Ljava/security/cert/CertificateEncodingException;->printStackTrace()V

    move-object v1, v3

    .line 78
    goto :goto_0
.end method
