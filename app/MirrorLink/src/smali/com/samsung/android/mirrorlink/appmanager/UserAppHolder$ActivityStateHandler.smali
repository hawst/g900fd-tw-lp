.class Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;
.super Landroid/os/Handler;
.source "UserAppHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityStateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1009
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1013
    const-string v3, "TMSUserAppHolder"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ActivityStateHandler.handleMessage msg"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 1017
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 1018
    .local v1, "bndl":Landroid/os/Bundle;
    const-string v3, "state"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1019
    .local v2, "state":Ljava/lang/String;
    const-string v3, "notrunning"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1021
    const-string v3, "AppName"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1022
    .local v0, "appName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1023
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    move-result-object v3

    const/16 v4, 0x16

    invoke-interface {v3, v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    goto :goto_0

    .line 1027
    .end local v0    # "appName":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$7(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$8(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Ljava/lang/String;)V

    .line 1028
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    const-string v4, "AppName"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$9(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Ljava/lang/String;)V

    .line 1029
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$7(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1030
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$7(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x14

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 1032
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPrevTopApp:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$10(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1033
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPrevTopApp:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$10(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x15

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    goto :goto_0

    .line 1014
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
