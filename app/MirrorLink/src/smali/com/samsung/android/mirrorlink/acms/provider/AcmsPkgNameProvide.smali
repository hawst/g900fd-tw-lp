.class public Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;
.super Landroid/content/ContentProvider;
.source "AcmsPkgNameProvide.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide$DatabaseHelper;
    }
.end annotation


# static fields
.field static final BASE_PATH_PKGNAME:Ljava/lang/String; = "pkgname"

.field static final CONTENT_URI_PKGNAMES:Landroid/net/Uri;

.field static final CREATE_DB_TABLE:Ljava/lang/String; = " CREATE TABLE if not exists pkgnames (id INTEGER PRIMARY KEY AUTOINCREMENT,  pkgname TEXT NOT NULL);"

.field static final DATABASE_NAME:Ljava/lang/String; = "pkgnamedb"

.field static final DATABASE_VERSION:I = 0x1

.field public static final PKGNAME:I = 0x1

.field private static final PKGNAME_AUTHORITY:Ljava/lang/String; = "com.samsung.mirrorlink.acms.pkgnames"

.field static final TABLE_NAME:Ljava/lang/String; = "pkgnames"

.field private static final TAG:Ljava/lang/String; = "AcmsPkgNameProvide"

.field static final id:Ljava/lang/String; = "id"

.field private static final sUriMatcher:Landroid/content/UriMatcher;

.field static final uriCode:I = 0x1


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    const-string v0, "content://com.samsung.mirrorlink.acms.pkgnames/pkgname"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->CONTENT_URI_PKGNAMES:Landroid/net/Uri;

    .line 51
    new-instance v0, Landroid/content/UriMatcher;

    .line 52
    const/4 v1, -0x1

    .line 51
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->sUriMatcher:Landroid/content/UriMatcher;

    .line 54
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.mirrorlink.acms.pkgnames"

    const-string v2, "pkgname"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 149
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 89
    const-string v1, "AcmsPkgNameProvide"

    const-string v2, "delete()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "count":I
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 96
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 93
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "pkgnames"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 99
    return v0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 77
    const-string v3, "AcmsPkgNameProvide"

    const-string v4, "insert()"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "pkgnames"

    const-string v5, ""

    invoke-virtual {v3, v4, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    .line 79
    .local v1, "rowID":J
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    .line 80
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->CONTENT_URI_PKGNAMES:Landroid/net/Uri;

    invoke-static {v3, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 81
    .local v0, "_uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 82
    return-object v0

    .line 84
    .end local v0    # "_uri":Landroid/net/Uri;
    :cond_0
    new-instance v3, Landroid/database/SQLException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to add a record into "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide$DatabaseHelper;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide$DatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 61
    .local v0, "dbHelper":Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide$DatabaseHelper;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 62
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_0

    .line 64
    const/4 v1, 0x1

    .line 66
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 107
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 108
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "pkgnames"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 109
    if-eqz p5, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    :cond_0
    const-string p5, "pkgname"

    .line 112
    :cond_1
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 117
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 120
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 122
    .local v8, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 123
    return-object v8

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 129
    const-string v1, "AcmsPkgNameProvide"

    const-string v2, "update()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "count":I
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 136
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 133
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "pkgnames"

    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 139
    return v0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
