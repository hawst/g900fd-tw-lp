.class Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;
.super Ljava/lang/Object;
.source "AcsAlarmReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->mNative:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    if-nez v0, :cond_0

    .line 22
    const-string v0, "TMSAlarmRecv"

    const-string v1, "AcsAlarmReceiver: mNative is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->mNative:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->aTid:I
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->access$0(Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->alarmExpiryCallback(I)V

    .line 27
    const-string v0, "TMSAlarmRecv"

    const-string v1, "AcsAlarmTask: Over"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
