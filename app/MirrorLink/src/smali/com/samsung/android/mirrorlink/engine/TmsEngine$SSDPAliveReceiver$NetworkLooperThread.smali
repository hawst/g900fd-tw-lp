.class Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;
.super Ljava/lang/Thread;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NetworkLooperThread"
.end annotation


# instance fields
.field private looper:Landroid/os/Looper;

.field private mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;Landroid/os/Handler;)V
    .locals 0
    .param p1, "tmsServrDevc"    # Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1457
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1458
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .line 1459
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    .locals 1

    .prologue
    .line 1454
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    return-object v0
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->looper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 1486
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .line 1488
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 1463
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 1464
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->looper:Landroid/os/Looper;

    .line 1465
    const-string v0, "TMSEngine"

    const-string v1, "SSDPAliveReceiver.run() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    new-instance v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread$1;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;)V

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->access$0(Landroid/os/Handler;)V

    .line 1480
    const-string v0, "TMSEngine"

    const-string v1, "SSDPAliveReceiver.run() Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1481
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 1482
    return-void
.end method
