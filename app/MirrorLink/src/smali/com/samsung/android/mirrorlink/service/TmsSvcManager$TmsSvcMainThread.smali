.class Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;
.super Ljava/lang/Thread;
.source "TmsSvcManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TmsSvcMainThread"
.end annotation


# instance fields
.field private looper:Landroid/os/Looper;

.field private mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)V
    .locals 2

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    .line 171
    const-string v0, "TMS_SVC_MAIN"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 172
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcMainThread.TmsSvcMainThread() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcMainThread.TmsSvcMainThread() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Landroid/os/Looper;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->looper:Landroid/os/Looper;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    return-object v0
.end method


# virtual methods
.method public myLooper()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->looper:Landroid/os/Looper;

    return-object v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 182
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcMainThread.run() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 185
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->looper:Landroid/os/Looper;

    .line 187
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    new-instance v1, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;-><init>(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)V

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$4(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;Landroid/os/Handler;)V

    .line 275
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 276
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcMainThread.run() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public terminate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 292
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$5(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 296
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->looper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 297
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$4(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;Landroid/os/Handler;)V

    .line 298
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$6(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->interrupt()V

    .line 300
    return-void
.end method
