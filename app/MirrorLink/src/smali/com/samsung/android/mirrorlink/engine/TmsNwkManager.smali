.class public Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;
.super Ljava/lang/Object;
.source "TmsNwkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;,
        Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_USB_STATE:Ljava/lang/String; = "android.hardware.usb.action.USB_STATE"

.field private static final TAG:Ljava/lang/String; = "TMSSvc"

.field public static final TMS_NWK_EVT_CONNECTED:I = 0x1

.field public static final TMS_NWK_EVT_DISCONNECTED:I = 0x2

.field public static final TMS_NWK_TYPE_3G:I = 0x2

.field public static final TMS_NWK_TYPE_NONE:I = 0x0

.field public static final TMS_NWK_TYPE_USB:I = 0x4

.field public static final TMS_NWK_TYPE_WIFI:I = 0x1

.field public static final USB_CONFIGURED:Ljava/lang/String; = "configured"

.field public static final USB_CONNECTED:Ljava/lang/String; = "connected"


# instance fields
.field private ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

.field private EXTRA_ACTIVE_TETHER:Ljava/lang/String;

.field private mCm:Landroid/net/ConnectivityManager;

.field private mCmClass:Ljava/lang/Class;

.field private mCxt:Landroid/content/Context;

.field private mTetheractive:Z

.field private mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

.field private mUsbConnected:Z

.field private mUsbEventReceiver:Landroid/content/BroadcastReceiver;

.field private mWiFiMngr:Landroid/net/wifi/WifiManager;

.field private mWifiEventReceiver:Landroid/content/BroadcastReceiver;

.field private regH:Landroid/os/Handler;

.field private regNwkType:I

.field private regWhat:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "cxt"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTetheractive:Z

    .line 82
    const-string v0, "android.net.conn.TETHER_STATE_CHANGED"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

    .line 83
    const-string v0, "activeArray"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->EXTRA_ACTIVE_TETHER:Ljava/lang/String;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 96
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager.TmsNwkManager() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    .line 99
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager.TmsNwkManager() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/wifi/WifiManager;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 691
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->notifyEvent(IILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->EXTRA_ACTIVE_TETHER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Landroid/net/ConnectivityManager;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTetheractive:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTetheractive:Z

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getLocalIpAddress(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbConnected:Z

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbConnected:Z

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

    return-object v0
.end method

.method private getLocalIpAddress(I)Ljava/lang/String;
    .locals 19
    .param p1, "nwkType"    # I

    .prologue
    .line 189
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "TmsNwkManager.getLocalIpAddress(): Enter: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v11, 0x0

    .line 193
    .local v11, "nwkIntfName":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 267
    :pswitch_0
    const-string v14, "TMSSvc"

    const-string v15, "TmsNwkManager.getLocalIpAddress(): invalid nwk type"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v14, 0x0

    .line 307
    :goto_0
    return-object v14

    .line 196
    :pswitch_1
    const/4 v13, 0x0

    .line 198
    .local v13, "usbRegexs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 202
    .local v7, "ifaces":[Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;

    const-string v15, "getTetherableUsbRegexs"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 203
    .local v10, "listener":Ljava/lang/reflect/Method;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual {v10, v14, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, [Ljava/lang/String;

    move-object v13, v0

    .line 204
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "usbRegexs: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    if-eqz v13, :cond_0

    const/4 v14, 0x0

    aget-object v14, v13, v14

    if-eqz v14, :cond_0

    .line 206
    const/4 v14, 0x0

    aget-object v11, v13, v14

    .line 208
    :cond_0
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "usbRegexs: nwkIntfName: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 220
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;

    const-string v15, "getTetheredIfaces"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 221
    .local v6, "ifacelistener":Ljava/lang/reflect/Method;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual {v6, v14, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, [Ljava/lang/String;

    move-object v7, v0

    .line 222
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "ifaces: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5

    .line 234
    if-eqz v7, :cond_1

    if-nez v13, :cond_3

    .line 235
    :cond_1
    const-string v14, "TMSSvc"

    const-string v15, "invalid interface as input"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 209
    .end local v6    # "ifacelistener":Ljava/lang/reflect/Method;
    .end local v10    # "listener":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Could not connect to getTetherableUsbRegexs."

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 212
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Could not connect to getTetherableUsbRegexs."

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 215
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v1

    .line 216
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Could not connect to getTetherableUsbRegexs."

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 223
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .restart local v10    # "listener":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v1

    .line 224
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Could not connect to getTetheredIfaces."

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    .end local v7    # "ifaces":[Ljava/lang/String;
    .end local v10    # "listener":Ljava/lang/reflect/Method;
    .end local v13    # "usbRegexs":[Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_2
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v2

    .line 274
    .local v2, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    if-nez v2, :cond_8

    .line 275
    const-string v14, "TMSSvc"

    const-string v15, "TmsNwkManager.getLocalIpAddress():getNetworkInterfaces == null "

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_6

    .line 276
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 226
    .end local v2    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .restart local v7    # "ifaces":[Ljava/lang/String;
    .restart local v10    # "listener":Ljava/lang/reflect/Method;
    .restart local v13    # "usbRegexs":[Ljava/lang/String;
    :catch_4
    move-exception v1

    .line 227
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Could not connect to getTetheredIfaces."

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 229
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_5
    move-exception v1

    .line 230
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Could not connect to getTetheredIfaces."

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 239
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .restart local v6    # "ifacelistener":Ljava/lang/reflect/Method;
    :cond_3
    array-length v0, v7

    move/from16 v16, v0

    const/4 v14, 0x0

    move v15, v14

    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_2

    aget-object v5, v7, v15

    .line 240
    .local v5, "iface":Ljava/lang/String;
    array-length v0, v13

    move/from16 v17, v0

    const/4 v14, 0x0

    :goto_3
    move/from16 v0, v17

    if-lt v14, v0, :cond_4

    .line 239
    :goto_4
    add-int/lit8 v14, v15, 0x1

    move v15, v14

    goto :goto_2

    .line 240
    :cond_4
    aget-object v12, v13, v14

    .line 241
    .local v12, "regex":Ljava/lang/String;
    invoke-virtual {v5, v12}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 245
    move-object v11, v12

    .line 246
    const-string v14, "TMSSvc"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "USB nwkIntfName selected is: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v14, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 240
    :cond_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 259
    .end local v5    # "iface":Ljava/lang/String;
    .end local v6    # "ifacelistener":Ljava/lang/reflect/Method;
    .end local v7    # "ifaces":[Ljava/lang/String;
    .end local v10    # "listener":Ljava/lang/reflect/Method;
    .end local v12    # "regex":Ljava/lang/String;
    .end local v13    # "usbRegexs":[Ljava/lang/String;
    :pswitch_2
    const-string v11, "wlan0"

    .line 260
    goto :goto_1

    .line 263
    :pswitch_3
    const-string v11, "ppp0"

    .line 264
    goto :goto_1

    .line 281
    .restart local v2    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_6
    :try_start_3
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/net/NetworkInterface;

    .line 282
    .local v9, "intf":Ljava/net/NetworkInterface;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "TmsNwkManager.getLocalIpAddress() NetworkInterface :"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {v9}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_8

    invoke-virtual {v9}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 286
    invoke-virtual {v9}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v3

    .local v3, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_7
    :goto_5
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v14

    if-nez v14, :cond_9

    .line 279
    .end local v3    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v9    # "intf":Ljava/net/NetworkInterface;
    :cond_8
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v14

    if-nez v14, :cond_6

    .line 307
    .end local v2    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_6
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 288
    .restart local v2    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .restart local v3    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .restart local v9    # "intf":Ljava/net/NetworkInterface;
    :cond_9
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/net/InetAddress;

    .line 289
    .local v8, "inetAddress":Ljava/net/InetAddress;
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, " TmsNwkManager.getLocalIpAddress(): inetAddress :"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-virtual {v8}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v14

    if-nez v14, :cond_7

    .line 292
    const-string v14, "TMSSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, " TmsNwkManager.getLocalIpAddress(): inetAddress IP:"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    instance-of v14, v8, Ljava/net/Inet4Address;

    if-eqz v14, :cond_a

    .line 295
    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 298
    :cond_a
    const-string v14, "TMSSvc"

    const-string v15, "TmsNwkManager.getLocalIpAddress is not of ipv4 type"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_6

    goto :goto_5

    .line 304
    .end local v2    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v3    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v8    # "inetAddress":Ljava/net/InetAddress;
    .end local v9    # "intf":Ljava/net/NetworkInterface;
    :catch_6
    move-exception v4

    .line 305
    .local v4, "ex":Ljava/net/SocketException;
    const-string v14, "TMSSvc"

    invoke-virtual {v4}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private notifyEvent(IILjava/lang/String;)V
    .locals 4
    .param p1, "nwkType"    # I
    .param p2, "evtType"    # I
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 692
    const-string v1, "TMSSvc"

    const-string v2, "TmsNwkManager:notifyEvent() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 695
    const-string v1, "TMSSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TmsNwkManager:notifyEvent() - sending w:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regWhat:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 696
    const-string v3, " nwk:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " evt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " d:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 695
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    iget v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regWhat:I

    invoke-virtual {v1, v2, p1, p2, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 698
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 700
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    const-string v1, "TMSSvc"

    const-string v2, "TmsNwkManager:notifyEvent() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    return-void
.end method

.method private registerUsbNwk()V
    .locals 3

    .prologue
    .line 410
    const-string v1, "TMSSvc"

    const-string v2, "TmsNwkManager:registerUsbNwk() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;

    .line 415
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;

    .line 418
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 419
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 420
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 427
    new-instance v1, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbEventReceiver:Landroid/content/BroadcastReceiver;

    .line 428
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 430
    const-string v1, "TMSSvc"

    const-string v2, "TmsNwkManager:registerUsbNwk() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    return-void
.end method

.method private registerWiFiNwk()V
    .locals 3

    .prologue
    .line 367
    const-string v1, "TMSSvc"

    const-string v2, "TmsNwkManager:registerWiFiNwk() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    .line 373
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 374
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 375
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 378
    new-instance v1, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWifiEventReceiver:Landroid/content/BroadcastReceiver;

    .line 381
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWifiEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 383
    const-string v1, "TMSSvc"

    const-string v2, "TmsNwkManager:registerWiFiNwk() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method private unregisterUsbNwk()V
    .locals 2

    .prologue
    .line 438
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterUsbNwk() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbEventReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 446
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbEventReceiver:Landroid/content/BroadcastReceiver;

    .line 448
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterUsbNwk() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method private unregisterWiFiNwk()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 390
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterWiFiNwk() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWifiEventReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWifiEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 399
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWifiEventReceiver:Landroid/content/BroadcastReceiver;

    .line 401
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    .line 403
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterWiFiNwk() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return-void
.end method


# virtual methods
.method public getConnectedIpAddress()Ljava/lang/String;
    .locals 4

    .prologue
    .line 177
    iget v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getLocalIpAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "ip":Ljava/lang/String;
    const-string v1, "TMSSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TmsNwkManager.getConnectedIpAddress "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-object v0
.end method

.method public getWiFiIpAddress()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 336
    const-string v3, "TMSSvc"

    const-string v4, "TmsNwkManager:getWiFiIpAddress: Enter "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const/4 v2, 0x0

    .line 339
    .local v2, "ip":I
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    if-eqz v3, :cond_1

    .line 340
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 341
    .local v1, "info":Landroid/net/wifi/WifiInfo;
    if-nez v1, :cond_0

    .line 342
    const-string v3, "TMSSvc"

    const-string v4, "TmsNwkManager:getWiFiIpAddress: info is null "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    .end local v1    # "info":Landroid/net/wifi/WifiInfo;
    :goto_0
    return-object v0

    .line 346
    .restart local v1    # "info":Landroid/net/wifi/WifiInfo;
    :cond_0
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v2

    .line 348
    .end local v1    # "info":Landroid/net/wifi/WifiInfo;
    :cond_1
    if-nez v2, :cond_2

    .line 349
    const-string v3, "TMSSvc"

    .line 350
    const-string v4, "TmsNwkManager:getWiFiIpAddress: Exit: No Valid Ip Address"

    .line 349
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 354
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    and-int/lit16 v4, v2, 0xff

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    shr-int/lit8 v4, v2, 0x8

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 355
    shr-int/lit8 v4, v2, 0x10

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    shr-int/lit8 v4, v2, 0x18

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 354
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 357
    .local v0, "addr":Ljava/lang/String;
    const-string v3, "TMSSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsNwkManager:getWiFiIpAddress: Exit: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 144
    iget v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    if-ne v4, v2, :cond_4

    .line 146
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 147
    const-string v2, "TMSSvc"

    const-string v4, "TmsNwkManager.isConnected  Wifi Not Enabled"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    :goto_0
    return v3

    .line 150
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 151
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-nez v1, :cond_2

    .line 152
    const-string v2, "TMSSvc"

    const-string v4, "TmsNwkManager.isConnected  Wifi getConnectionInfo Failed"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 156
    :cond_2
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v0

    .line 157
    .local v0, "state":Landroid/net/wifi/SupplicantState;
    sget-object v4, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    if-eq v0, v4, :cond_3

    .line 158
    const-string v2, "TMSSvc"

    const-string v4, "TmsNwkManager.isConnected  Not COMPLETED"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v3, v2

    .line 161
    goto :goto_0

    .line 163
    .end local v0    # "state":Landroid/net/wifi/SupplicantState;
    .end local v1    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    :cond_4
    iget v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getConnectedIpAddress()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_5
    move v2, v3

    goto :goto_1
.end method

.method public registerForNwkEvents(Landroid/os/Handler;II)V
    .locals 3
    .param p1, "h"    # Landroid/os/Handler;
    .param p2, "nwkType"    # I
    .param p3, "what"    # I

    .prologue
    .line 113
    const-string v0, "TMSSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TmsNwkManager:registerForNwkEvents: Enter -nwkType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 116
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    .line 117
    iget v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    or-int/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    .line 118
    iput p3, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regWhat:I

    .line 120
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->registerWiFiNwk()V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 130
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    .line 131
    iget v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    or-int/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    .line 132
    iput p3, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regWhat:I

    .line 134
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->registerUsbNwk()V

    goto :goto_0
.end method

.method public setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 0
    .param p1, "tmsEngine"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 104
    return-void
.end method

.method public unregisterForNwkEvents(Landroid/os/Handler;I)V
    .locals 2
    .param p1, "h"    # Landroid/os/Handler;
    .param p2, "nwkType"    # I

    .prologue
    .line 316
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterForNwkEvents: Enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterForNwkEvents: unregistered "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regH:Landroid/os/Handler;

    .line 322
    :cond_0
    iget v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 323
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->unregisterWiFiNwk()V

    .line 328
    :cond_1
    :goto_0
    const-string v0, "TMSSvc"

    const-string v1, "TmsNwkManager:unregisterForNwkEvents: Exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    return-void

    .line 325
    :cond_2
    iget v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->regNwkType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 326
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->unregisterUsbNwk()V

    goto :goto_0
.end method
