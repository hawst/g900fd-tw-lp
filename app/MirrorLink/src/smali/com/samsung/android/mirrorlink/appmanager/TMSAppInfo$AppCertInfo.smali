.class public Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
.super Ljava/lang/Object;
.source "TMSAppInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppCertInfo"
.end annotation


# instance fields
.field public certType:I

.field public mAppCertificateEntityInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mAppId:Ljava/lang/String;

.field public mAppUUID:Ljava/lang/String;

.field public mBlackListedClients:Ljava/lang/String;

.field public mProperties:Ljava/lang/String;

.field public mSignature:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    if-ne p0, p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v1

    .line 129
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 130
    goto :goto_0

    .line 131
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 132
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 133
    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    .line 134
    .local v0, "other":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-nez v3, :cond_4

    .line 135
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 136
    const-string v1, "TMSAppInfo"

    const-string v3, "mAppCertificateEntityInfo is null"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 137
    goto :goto_0

    .line 139
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    .line 140
    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 141
    const-string v1, "TMSAppInfo"

    const-string v3, "mAppCertificateEntityInfo is is changed"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 142
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 117
    const/16 v0, 0x1f

    .line 118
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 121
    .local v1, "result":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 119
    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 123
    return v1

    .line 122
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0
.end method
