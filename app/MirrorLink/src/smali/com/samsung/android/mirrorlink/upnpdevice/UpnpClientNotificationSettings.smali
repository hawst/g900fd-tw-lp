.class public Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;
.super Ljava/lang/Object;
.source "UpnpClientNotificationSettings.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSUPnP"


# instance fields
.field public bRecvdClientSettings:Z

.field public mActionMaxLen:I

.field public mMaxActions:I

.field public mNotiBodyMaxLen:I

.field public mNotiSupportedApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mNotiTitleMaxLen:I

.field public mNotiUiSupport:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "TMSUPnP"

    const-string v1, "UpnpClientNotificationSettings.UpnpClientNotificationSettings"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiUiSupport:Z

    .line 22
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mMaxActions:I

    .line 23
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mActionMaxLen:I

    .line 24
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiTitleMaxLen:I

    .line 25
    const/16 v0, 0x50

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiBodyMaxLen:I

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    .line 27
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->bRecvdClientSettings:Z

    .line 28
    return-void
.end method


# virtual methods
.method public setNotiSupportedApps(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "apps"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "appMngrNotiApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "UpnpClientNotificationSettings.setNotiSupportedApps apps = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "appMngrNotiAppssize() =  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v3, "newNotiSupportedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v4, ","

    invoke-direct {v0, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .local v0, "appTkz":Ljava/util/StringTokenizer;
    const-string v4, "*"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    .line 39
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 40
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 41
    const-string v4, "TMSUPnP"

    const-string v5, "UpnpClientNotificationSettings.setNotiSupportedApps add all apps"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    .line 61
    :goto_0
    return-object v4

    .line 45
    :cond_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "hexAppId":Ljava/lang/String;
    const-string v4, "0x"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 47
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    :cond_2
    const/16 v4, 0x10

    invoke-static {v2, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 50
    .local v1, "appid":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 51
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .end local v1    # "appid":I
    .end local v2    # "hexAppId":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-nez v4, :cond_1

    .line 58
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 59
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 60
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "UpnpClientNotificationSettings.setNotiSupportedApps exit newNotiSupportedApps.size()"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    goto :goto_0

    .line 53
    .restart local v1    # "appid":I
    .restart local v2    # "hexAppId":Ljava/lang/String;
    :cond_4
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "UpnpClientNotificationSettings.setNotiSupportedApps app id "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mismatching return"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const/4 v4, 0x0

    goto :goto_0
.end method
