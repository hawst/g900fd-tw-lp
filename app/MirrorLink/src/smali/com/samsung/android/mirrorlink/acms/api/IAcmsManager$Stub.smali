.class public abstract Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;
.super Landroid/os/Binder;
.source "IAcmsManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

.field static final TRANSACTION_performRevocation:I = 0x4

.field static final TRANSACTION_performRevocationCheck:I = 0x3

.field static final TRANSACTION_registerListener:I = 0x1

.field static final TRANSACTION_setDevMode:I = 0x5

.field static final TRANSACTION_unRegisterListener:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 93
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 46
    :sswitch_0
    const-string v2, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v2, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;

    move-result-object v0

    .line 54
    .local v0, "_arg0":Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;
    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->registerListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V

    .line 55
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 60
    .end local v0    # "_arg0":Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;
    :sswitch_2
    const-string v2, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;

    move-result-object v0

    .line 63
    .restart local v0    # "_arg0":Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;
    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->unRegisterListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V

    .line 64
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 69
    .end local v0    # "_arg0":Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;
    :sswitch_3
    const-string v2, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->performRevocationCheck(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 78
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_4
    const-string v2, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->performRevocation()V

    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 85
    :sswitch_5
    const-string v2, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 88
    .local v0, "_arg0":Z
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->setDevMode(Z)V

    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 87
    .end local v0    # "_arg0":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
