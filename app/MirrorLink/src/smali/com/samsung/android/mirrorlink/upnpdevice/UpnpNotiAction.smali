.class public Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;
.super Ljava/lang/Object;
.source "UpnpNotiAction.java"


# static fields
.field private static final BASEACTID:I = 0xa

.field private static final TAG:Ljava/lang/String; = "TMSUPnP"

.field private static actionCount:I


# instance fields
.field private mActionId:I

.field public mActionName:Ljava/lang/String;

.field public mLaunchApp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ZI)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "launcApp"    # Z
    .param p3, "mUid"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotiAction.UpnpNotiAction enter name = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "launchapp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p3}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->generateActionId(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    .line 24
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionName:Ljava/lang/String;

    .line 25
    iput-boolean p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mLaunchApp:Z

    .line 26
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotiAction.UpnpNotiAction new action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method private generateActionId(I)I
    .locals 3
    .param p1, "mNotiUid"    # I

    .prologue
    .line 37
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotiAction.generateActionId enter actionCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mNotiUid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    if-nez v0, :cond_0

    .line 39
    const-string v0, "TMSUPnP"

    const-string v1, "UpnpNotiAction.generateActionId resetting mActionId to BASEACTID"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    .line 43
    :cond_0
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    .line 44
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    .line 45
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotiAction.generateActionId enter new mActionId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    return v0
.end method


# virtual methods
.method public getActionId()I
    .locals 3

    .prologue
    .line 53
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotiAction.getActionId  = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    return v0
.end method

.method public removeAction()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotiAction.removeAction = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->actionCount:I

    .line 64
    return-void
.end method
