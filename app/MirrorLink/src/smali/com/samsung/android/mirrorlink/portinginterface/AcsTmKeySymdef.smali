.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeySymdef;
.super Ljava/lang/Object;
.source "AcsTmKeySymdef.java"


# static fields
.field public static final ACS_TM_Device_Application:I = 0x30000205

.field public static final ACS_TM_Device_Backward:I = 0x3000020c

.field public static final ACS_TM_Device_Clear:I = 0x3000020a

.field public static final ACS_TM_Device_Delete:I = 0x30000207

.field public static final ACS_TM_Device_Forward:I = 0x3000020b

.field public static final ACS_TM_Device_Home:I = 0x3000020d

.field public static final ACS_TM_Device_Menu:I = 0x3000020f

.field public static final ACS_TM_Device_Ok:I = 0x30000206

.field public static final ACS_TM_Device_Phone_call:I = 0x30000200

.field public static final ACS_TM_Device_Phone_end:I = 0x30000201

.field public static final ACS_TM_Device_Search:I = 0x3000020e

.field public static final ACS_TM_Device_Soft_left:I = 0x30000202

.field public static final ACS_TM_Device_Soft_middle:I = 0x30000203

.field public static final ACS_TM_Device_Soft_right:I = 0x30000204

.field public static final ACS_TM_Device_Zoom_in:I = 0x30000208

.field public static final ACS_TM_Device_Zoom_out:I = 0x30000209

.field public static final ACS_TM_Function_Key_0:I = 0x30000300

.field public static final ACS_TM_Function_Key_1:I = 0x30000301

.field public static final ACS_TM_Function_Key_10:I = 0x30000310

.field public static final ACS_TM_Function_Key_11:I = 0x30000311

.field public static final ACS_TM_Function_Key_2:I = 0x30000302

.field public static final ACS_TM_Function_Key_255:I = 0x300003ff

.field public static final ACS_TM_Function_Key_3:I = 0x30000303

.field public static final ACS_TM_Function_Key_4:I = 0x30000304

.field public static final ACS_TM_Function_Key_5:I = 0x30000305

.field public static final ACS_TM_Function_Key_6:I = 0x30000306

.field public static final ACS_TM_Function_Key_7:I = 0x30000307

.field public static final ACS_TM_Function_Key_8:I = 0x30000308

.field public static final ACS_TM_Function_Key_9:I = 0x30000309

.field public static final ACS_TM_ITU_Key_0:I = 0x30000100

.field public static final ACS_TM_ITU_Key_1:I = 0x30000101

.field public static final ACS_TM_ITU_Key_2:I = 0x30000102

.field public static final ACS_TM_ITU_Key_3:I = 0x30000103

.field public static final ACS_TM_ITU_Key_4:I = 0x30000104

.field public static final ACS_TM_ITU_Key_5:I = 0x30000105

.field public static final ACS_TM_ITU_Key_6:I = 0x30000106

.field public static final ACS_TM_ITU_Key_7:I = 0x30000107

.field public static final ACS_TM_ITU_Key_8:I = 0x30000108

.field public static final ACS_TM_ITU_Key_9:I = 0x30000109

.field public static final ACS_TM_ITU_Key_Asterix:I = 0x3000010a

.field public static final ACS_TM_ITU_Key_Pound:I = 0x3000010b

.field public static final ACS_TM_Knob_2D_0_rotate_X:I = 0x3000000b

.field public static final ACS_TM_Knob_2D_0_rotate_Y:I = 0x3000000d

.field public static final ACS_TM_Knob_2D_0_rotate_Z:I = 0x3000000f

.field public static final ACS_TM_Knob_2D_0_rotate_x:I = 0x3000000a

.field public static final ACS_TM_Knob_2D_0_rotate_y:I = 0x3000000c

.field public static final ACS_TM_Knob_2D_0_rotate_z:I = 0x3000000e

.field public static final ACS_TM_Knob_2D_0_shift_down:I = 0x30000005

.field public static final ACS_TM_Knob_2D_0_shift_down_left:I = 0x30000007

.field public static final ACS_TM_Knob_2D_0_shift_down_right:I = 0x30000006

.field public static final ACS_TM_Knob_2D_0_shift_left:I = 0x30000001

.field public static final ACS_TM_Knob_2D_0_shift_pull:I = 0x30000009

.field public static final ACS_TM_Knob_2D_0_shift_push:I = 0x30000008

.field public static final ACS_TM_Knob_2D_0_shift_right:I = 0x30000000

.field public static final ACS_TM_Knob_2D_0_shift_up:I = 0x30000002

.field public static final ACS_TM_Knob_2D_0_shift_up_left:I = 0x30000004

.field public static final ACS_TM_Knob_2D_0_shift_up_right:I = 0x30000003

.field public static final ACS_TM_Knob_2D_Min:I = 0x30000000

.field public static final ACS_TM_Multimedia_Forward:I = 0x30000403

.field public static final ACS_TM_Multimedia_Mute:I = 0x30000407

.field public static final ACS_TM_Multimedia_Next:I = 0x30000405

.field public static final ACS_TM_Multimedia_Pause:I = 0x30000401

.field public static final ACS_TM_Multimedia_Photo:I = 0x30000409

.field public static final ACS_TM_Multimedia_Play:I = 0x30000400

.field public static final ACS_TM_Multimedia_Previous:I = 0x30000406

.field public static final ACS_TM_Multimedia_Rewind:I = 0x30000404

.field public static final ACS_TM_Multimedia_Stop:I = 0x30000402

.field public static final ACS_TM_Multimedia_Unmute:I = 0x30000408

.field public static final ACS_TM_Reserved_Max:I = 0x3000ffff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
