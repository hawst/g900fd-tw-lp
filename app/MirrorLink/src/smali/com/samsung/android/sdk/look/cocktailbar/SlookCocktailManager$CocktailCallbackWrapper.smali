.class Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;
.super Ljava/lang/Object;
.source "SlookCocktailManager.java"

# interfaces
.implements Lcom/samsung/android/cocktailbar/CocktailBarManager$ICocktailCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CocktailCallbackWrapper"
.end annotation


# instance fields
.field private mCallback:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;

.field final synthetic this$0:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;)V
    .locals 0
    .param p2, "callback"    # Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;

    .prologue
    .line 432
    iput-object p1, p0, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;->this$0:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    iput-object p2, p0, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;->mCallback:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;

    .line 434
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;->mCallback:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;

    invoke-interface {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;->onCreate()V

    .line 439
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;->this$0:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;->mCallbackWrapper:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;->access$502(Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;)Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;

    .line 444
    iget-object v0, p0, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;->mCallback:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;

    invoke-interface {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;->onDestroy()V

    .line 445
    return-void
.end method

.method public responseCocktailCallback(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 449
    iget-object v0, p0, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailCallbackWrapper;->mCallback:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$ISlookCocktailCallback;->responseCocktailCallback(Landroid/os/Bundle;)V

    .line 450
    return-void
.end method
