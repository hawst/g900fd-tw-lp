.class public Lcom/samsung/net/HostInterface;
.super Ljava/lang/Object;
.source "HostInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/net/HostInterface$NWK_TYPE;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$net$HostInterface$NWK_TYPE:[I

.field public static USE_LOOPBACK_ADDR:Z

.field public static USE_ONLY_IPV4_ADDR:Z

.field public static USE_ONLY_IPV6_ADDR:Z

.field private static ifAddress:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$net$HostInterface$NWK_TYPE()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/net/HostInterface;->$SWITCH_TABLE$com$samsung$net$HostInterface$NWK_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/net/HostInterface$NWK_TYPE;->values()[Lcom/samsung/net/HostInterface$NWK_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/net/HostInterface$NWK_TYPE;->USB:Lcom/samsung/net/HostInterface$NWK_TYPE;

    invoke-virtual {v1}, Lcom/samsung/net/HostInterface$NWK_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/net/HostInterface$NWK_TYPE;->WIFI:Lcom/samsung/net/HostInterface$NWK_TYPE;

    invoke-virtual {v1}, Lcom/samsung/net/HostInterface$NWK_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/samsung/net/HostInterface;->$SWITCH_TABLE$com$samsung$net$HostInterface$NWK_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    sput-boolean v0, Lcom/samsung/net/HostInterface;->USE_LOOPBACK_ADDR:Z

    .line 46
    sput-boolean v0, Lcom/samsung/net/HostInterface;->USE_ONLY_IPV4_ADDR:Z

    .line 47
    sput-boolean v0, Lcom/samsung/net/HostInterface;->USE_ONLY_IPV6_ADDR:Z

    .line 53
    const-string v0, ""

    sput-object v0, Lcom/samsung/net/HostInterface;->ifAddress:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getHostAddress(I)Ljava/lang/String;
    .locals 8
    .param p0, "n"    # I

    .prologue
    .line 169
    invoke-static {}, Lcom/samsung/net/HostInterface;->hasAssignedInterface()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 170
    invoke-static {}, Lcom/samsung/net/HostInterface;->getInterface()Ljava/lang/String;

    move-result-object v3

    .line 198
    :goto_0
    return-object v3

    .line 172
    :cond_0
    const/4 v4, 0x0

    .line 174
    .local v4, "hostAddrCnt":I
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 175
    .local v6, "nis":Ljava/util/Enumeration;
    if-nez v6, :cond_3

    .line 176
    const-string v3, ""

    goto :goto_0

    .line 178
    :cond_1
    :try_start_1
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 179
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 180
    .local v1, "addrs":Ljava/util/Enumeration;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-nez v7, :cond_4

    .line 177
    .end local v1    # "addrs":Ljava/util/Enumeration;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    :cond_3
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    if-nez v7, :cond_1

    .line 198
    .end local v6    # "nis":Ljava/util/Enumeration;
    :goto_2
    const-string v3, ""

    goto :goto_0

    .line 181
    .restart local v1    # "addrs":Ljava/util/Enumeration;
    .restart local v5    # "ni":Ljava/net/NetworkInterface;
    .restart local v6    # "nis":Ljava/util/Enumeration;
    :cond_4
    :try_start_2
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 182
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/net/HostInterface;->isUsableAddress(Ljava/net/InetAddress;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 184
    if-ge v4, p0, :cond_5

    .line 185
    add-int/lit8 v4, v4, 0x1

    .line 186
    goto :goto_1

    .line 188
    :cond_5
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    .line 191
    .local v3, "host":Ljava/lang/String;
    goto :goto_0

    .line 195
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;
    .end local v3    # "host":Ljava/lang/String;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/Enumeration;
    :catch_0
    move-exception v2

    .line 196
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static final getHostAddress(Lcom/samsung/net/HostInterface$NWK_TYPE;I)Ljava/lang/String;
    .locals 9
    .param p0, "nwkType"    # Lcom/samsung/net/HostInterface$NWK_TYPE;
    .param p1, "n"    # I

    .prologue
    .line 205
    invoke-static {}, Lcom/samsung/net/HostInterface;->hasAssignedInterface()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 206
    invoke-static {}, Lcom/samsung/net/HostInterface;->getInterface()Ljava/lang/String;

    move-result-object v3

    .line 251
    :goto_0
    return-object v3

    .line 208
    :cond_0
    const/4 v4, 0x0

    .line 211
    .local v4, "hostAddrCnt":I
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 213
    .local v6, "nis":Ljava/util/Enumeration;
    if-nez v6, :cond_2

    .line 214
    const-string v3, ""

    goto :goto_0

    .line 216
    :cond_1
    :try_start_1
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 217
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 218
    .local v1, "addrs":Ljava/util/Enumeration;
    invoke-static {}, Lcom/samsung/net/HostInterface;->$SWITCH_TABLE$com$samsung$net$HostInterface$NWK_TYPE()[I

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/net/HostInterface$NWK_TYPE;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 228
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ncm"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "usb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 215
    .end local v1    # "addrs":Ljava/util/Enumeration;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    if-nez v7, :cond_1

    .line 251
    .end local v6    # "nis":Ljava/util/Enumeration;
    :goto_2
    const-string v3, ""

    goto :goto_0

    .line 220
    .restart local v1    # "addrs":Ljava/util/Enumeration;
    .restart local v5    # "ni":Ljava/net/NetworkInterface;
    .restart local v6    # "nis":Ljava/util/Enumeration;
    :pswitch_0
    :try_start_2
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "wlan"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "eth"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 233
    :cond_3
    :goto_3
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 234
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 235
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/net/HostInterface;->isUsableAddress(Ljava/net/InetAddress;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 237
    if-ge v4, p1, :cond_4

    .line 238
    add-int/lit8 v4, v4, 0x1

    .line 239
    goto :goto_3

    .line 224
    .end local v0    # "addr":Ljava/net/InetAddress;
    :pswitch_1
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ncm"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "usb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    goto :goto_1

    .line 241
    .restart local v0    # "addr":Ljava/net/InetAddress;
    :cond_4
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    .line 245
    .local v3, "host":Ljava/lang/String;
    goto/16 :goto_0

    .line 248
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;
    .end local v3    # "host":Ljava/lang/String;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/Enumeration;
    :catch_0
    move-exception v2

    .line 249
    .local v2, "e":Ljava/net/SocketException;
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_2

    .line 218
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final getHostURL(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "host"    # Ljava/lang/String;
    .param p1, "port"    # I
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 339
    move-object v0, p0

    .line 340
    .local v0, "hostAddr":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/net/HostInterface;->isIPv6Address(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 343
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 345
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 346
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 343
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 342
    return-object v1
.end method

.method public static final getInterface()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/samsung/net/HostInterface;->ifAddress:Ljava/lang/String;

    return-object v0
.end method

.method public static final getNHostAddresses()I
    .locals 6

    .prologue
    .line 98
    invoke-static {}, Lcom/samsung/net/HostInterface;->hasAssignedInterface()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 99
    const/4 v2, 0x1

    .line 118
    :goto_0
    return v2

    .line 101
    :cond_0
    const/4 v2, 0x0

    .line 103
    .local v2, "nHostAddrs":I
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v4

    .line 104
    .local v4, "nis":Ljava/util/Enumeration;
    if-nez v4, :cond_3

    .line 105
    const/4 v2, 0x0

    goto :goto_0

    .line 107
    :cond_1
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/NetworkInterface;

    .line 108
    .local v3, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 109
    .local v1, "addrs":Ljava/util/Enumeration;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-nez v5, :cond_4

    .line 106
    .end local v1    # "addrs":Ljava/util/Enumeration;
    .end local v3    # "ni":Ljava/net/NetworkInterface;
    :cond_3
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_0

    .line 110
    .restart local v1    # "addrs":Ljava/util/Enumeration;
    .restart local v3    # "ni":Ljava/net/NetworkInterface;
    :cond_4
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 111
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/net/HostInterface;->isUsableAddress(Ljava/net/InetAddress;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_2

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 117
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;
    .end local v3    # "ni":Ljava/net/NetworkInterface;
    .end local v4    # "nis":Ljava/util/Enumeration;
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public static final getNHostAddresses(Lcom/samsung/net/HostInterface$NWK_TYPE;)I
    .locals 8
    .param p0, "nwkType"    # Lcom/samsung/net/HostInterface$NWK_TYPE;

    .prologue
    .line 128
    invoke-static {}, Lcom/samsung/net/HostInterface;->hasAssignedInterface()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 129
    const/4 v3, 0x1

    .line 165
    :goto_0
    return v3

    .line 131
    :cond_0
    const/4 v3, 0x0

    .line 134
    .local v3, "nHostAddrs":I
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v5

    .line 135
    .local v5, "nis":Ljava/util/Enumeration;
    if-nez v5, :cond_2

    .line 136
    const/4 v3, 0x0

    goto :goto_0

    .line 138
    :cond_1
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 139
    .local v4, "ni":Ljava/net/NetworkInterface;
    invoke-static {}, Lcom/samsung/net/HostInterface;->$SWITCH_TABLE$com$samsung$net$HostInterface$NWK_TYPE()[I

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/net/HostInterface$NWK_TYPE;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 150
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ncm"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "usb"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 137
    .end local v4    # "ni":Ljava/net/NetworkInterface;
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_1

    goto :goto_0

    .line 142
    .restart local v4    # "ni":Ljava/net/NetworkInterface;
    :pswitch_0
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "wlan"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "eth"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 154
    :cond_3
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 155
    .local v1, "addrs":Ljava/util/Enumeration;
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 156
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 157
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/samsung/net/HostInterface;->isUsableAddress(Ljava/net/InetAddress;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 159
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 146
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/Enumeration;
    :pswitch_1
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ncm"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "usb"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_3

    goto :goto_1

    .line 162
    .end local v4    # "ni":Ljava/net/NetworkInterface;
    .end local v5    # "nis":Ljava/util/Enumeration;
    :catch_0
    move-exception v2

    .line 163
    .local v2, "e":Ljava/net/SocketException;
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    goto/16 :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final hasAssignedInterface()Z
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/net/HostInterface;->ifAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final isIPv4Address(Ljava/lang/String;)Z
    .locals 3
    .param p0, "host"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 272
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 273
    .local v0, "addr":Ljava/net/InetAddress;
    instance-of v2, v0, Ljava/net/Inet4Address;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    .line 274
    const/4 v1, 0x1

    .line 278
    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_0
    :goto_0
    return v1

    .line 277
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static final isIPv6Address(Ljava/lang/String;)Z
    .locals 3
    .param p0, "host"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 260
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 261
    .local v0, "addr":Ljava/net/InetAddress;
    instance-of v2, v0, Ljava/net/Inet6Address;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    .line 262
    const/4 v1, 0x1

    .line 266
    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_0
    :goto_0
    return v1

    .line 265
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private static final isUsableAddress(Ljava/net/InetAddress;)Z
    .locals 2
    .param p0, "addr"    # Ljava/net/InetAddress;

    .prologue
    const/4 v0, 0x0

    .line 81
    sget-boolean v1, Lcom/samsung/net/HostInterface;->USE_LOOPBACK_ADDR:Z

    if-nez v1, :cond_1

    .line 82
    invoke-virtual {p0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    sget-boolean v1, Lcom/samsung/net/HostInterface;->USE_ONLY_IPV4_ADDR:Z

    if-eqz v1, :cond_2

    .line 86
    instance-of v1, p0, Ljava/net/Inet6Address;

    if-nez v1, :cond_0

    .line 89
    :cond_2
    sget-boolean v1, Lcom/samsung/net/HostInterface;->USE_ONLY_IPV6_ADDR:Z

    if-eqz v1, :cond_3

    .line 90
    instance-of v1, p0, Ljava/net/Inet4Address;

    if-nez v1, :cond_0

    .line 93
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final setInterface(Ljava/lang/String;)V
    .locals 1
    .param p0, "ifaddr"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v0, "0.0.0.0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, ""

    sput-object v0, Lcom/samsung/net/HostInterface;->ifAddress:Ljava/lang/String;

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    sput-object p0, Lcom/samsung/net/HostInterface;->ifAddress:Ljava/lang/String;

    goto :goto_0
.end method
