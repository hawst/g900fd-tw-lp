.class public Lcom/samsung/api/DeviceItem;
.super Ljava/lang/Object;
.source "DeviceItem.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/samsung/api/DeviceItem;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final DEVICE_DMPR:I = 0x4

.field public static final DEVICE_DMR:I = 0x2

.field public static final DEVICE_DMS:I = 0x1

.field public static final DEVICE_ETC:I = 0x5

.field public static final DEVICE_PMR:I = 0x3

.field private static final SERIALVERSIONUID:J = -0x5fc3c38c0312e73cL


# instance fields
.field private mBaseUrl:Ljava/lang/String;

.field private mCompany:Ljava/lang/String;

.field private mDeviceIP:Ljava/lang/String;

.field private mDeviceType:I

.field private mFirstIcon:Ljava/lang/String;

.field private mFriendlyName:Ljava/lang/String;

.field private mIconList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIgnoreNotification:Z

.field private mModelName:Ljava/lang/String;

.field private mModelNo:Ljava/lang/String;

.field private mUdn:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    .line 85
    iput-object v1, p0, Lcom/samsung/api/DeviceItem;->mFirstIcon:Ljava/lang/String;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/api/DeviceItem;->mIgnoreNotification:Z

    .line 87
    iput-object v1, p0, Lcom/samsung/api/DeviceItem;->mBaseUrl:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public constructor <init>(Lcom/samsung/upnp/Device;)V
    .locals 13
    .param p1, "dev"    # Lcom/samsung/upnp/Device;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v9, 0x5

    iput v9, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    .line 84
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    .line 85
    iput-object v10, p0, Lcom/samsung/api/DeviceItem;->mFirstIcon:Ljava/lang/String;

    .line 86
    iput-boolean v11, p0, Lcom/samsung/api/DeviceItem;->mIgnoreNotification:Z

    .line 87
    iput-object v10, p0, Lcom/samsung/api/DeviceItem;->mBaseUrl:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mFriendlyName:Ljava/lang/String;

    .line 133
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mUdn:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getModelName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mModelName:Ljava/lang/String;

    .line 135
    iget-object v9, p0, Lcom/samsung/api/DeviceItem;->mModelName:Ljava/lang/String;

    const-string v10, "Windows Media Player"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 136
    iput-boolean v12, p0, Lcom/samsung/api/DeviceItem;->mIgnoreNotification:Z

    .line 137
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getManufacture()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mCompany:Ljava/lang/String;

    .line 138
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getModelNumber()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mModelNo:Ljava/lang/String;

    .line 140
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/api/DeviceItem;->setDeviceType(Ljava/lang/String;)V

    .line 143
    :try_start_0
    new-instance v7, Ljava/net/URI;

    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 144
    .local v7, "uri":Ljava/net/URI;
    invoke-virtual {v7}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    .end local v7    # "uri":Ljava/net/URI;
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v5

    .line 151
    .local v5, "location":Ljava/lang/String;
    const-string v8, ""

    .line 152
    .local v8, "urlBase":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 153
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "http://"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/samsung/http/HTTP;->getHost(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5}, Lcom/samsung/http/HTTP;->getPort(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 156
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getIconList()Lcom/samsung/upnp/IconList;

    move-result-object v4

    .line 157
    .local v4, "icons":Lcom/samsung/upnp/IconList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v4}, Lcom/samsung/upnp/IconList;->size()I

    move-result v9

    if-lt v1, v9, :cond_3

    .line 173
    iget-object v9, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 174
    iget-object v9, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mFirstIcon:Ljava/lang/String;

    .line 178
    :cond_2
    return-void

    .line 145
    .end local v1    # "i":I
    .end local v4    # "icons":Lcom/samsung/upnp/IconList;
    .end local v5    # "location":Ljava/lang/String;
    .end local v8    # "urlBase":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    const-string v9, ""

    iput-object v9, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    goto :goto_0

    .line 158
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    .restart local v4    # "icons":Lcom/samsung/upnp/IconList;
    .restart local v5    # "location":Ljava/lang/String;
    .restart local v8    # "urlBase":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4, v1}, Lcom/samsung/upnp/IconList;->getIcon(I)Lcom/samsung/upnp/Icon;

    move-result-object v2

    .line 159
    .local v2, "icon":Lcom/samsung/upnp/Icon;
    if-nez v2, :cond_4

    .line 157
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 161
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/upnp/Icon;->getURL()Ljava/lang/String;

    move-result-object v3

    .line 164
    .local v3, "iconUrl":Ljava/lang/String;
    const-string v9, "/"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 165
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 170
    .local v6, "tempStr":Ljava/lang/String;
    :goto_3
    iget-object v9, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 167
    .end local v6    # "tempStr":Ljava/lang/String;
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "tempStr":Ljava/lang/String;
    goto :goto_3
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "friendlyName"    # Ljava/lang/String;
    .param p2, "udn"    # Ljava/lang/String;
    .param p3, "modelName"    # Ljava/lang/String;
    .param p4, "company"    # Ljava/lang/String;
    .param p5, "modelNo"    # Ljava/lang/String;
    .param p6, "deviceIP"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x5

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    .line 85
    iput-object v2, p0, Lcom/samsung/api/DeviceItem;->mFirstIcon:Ljava/lang/String;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/api/DeviceItem;->mIgnoreNotification:Z

    .line 87
    iput-object v2, p0, Lcom/samsung/api/DeviceItem;->mBaseUrl:Ljava/lang/String;

    .line 191
    iput-object p1, p0, Lcom/samsung/api/DeviceItem;->mFriendlyName:Ljava/lang/String;

    .line 192
    iput-object p2, p0, Lcom/samsung/api/DeviceItem;->mUdn:Ljava/lang/String;

    .line 193
    iput-object p3, p0, Lcom/samsung/api/DeviceItem;->mModelName:Ljava/lang/String;

    .line 194
    if-eqz p3, :cond_0

    const-string v0, "Windows Media Player"

    invoke-virtual {p3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/api/DeviceItem;->mIgnoreNotification:Z

    .line 196
    :cond_0
    iput-object p4, p0, Lcom/samsung/api/DeviceItem;->mCompany:Ljava/lang/String;

    .line 197
    iput-object p5, p0, Lcom/samsung/api/DeviceItem;->mModelNo:Ljava/lang/String;

    .line 198
    iput-object p6, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    .line 199
    iput v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    .line 200
    return-void
.end method

.method public static getLodcalDeviceItem()Lcom/samsung/api/DeviceItem;
    .locals 2

    .prologue
    .line 209
    new-instance v0, Lcom/samsung/api/DeviceItem;

    invoke-direct {v0}, Lcom/samsung/api/DeviceItem;-><init>()V

    .line 210
    .local v0, "deviceItem":Lcom/samsung/api/DeviceItem;
    invoke-static {}, Lcom/samsung/net/HostInterface;->getInterface()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    .line 211
    const-string v1, "Local Device"

    iput-object v1, v0, Lcom/samsung/api/DeviceItem;->mFriendlyName:Ljava/lang/String;

    .line 212
    const-string v1, "0"

    iput-object v1, v0, Lcom/samsung/api/DeviceItem;->mUdn:Ljava/lang/String;

    .line 213
    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/samsung/api/DeviceItem;)I
    .locals 1
    .param p1, "arg0"    # Lcom/samsung/api/DeviceItem;

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/samsung/api/DeviceItem;->isLocalDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const/4 v0, -0x1

    .line 349
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/samsung/api/DeviceItem;

    invoke-virtual {p0, p1}, Lcom/samsung/api/DeviceItem;->compareTo(Lcom/samsung/api/DeviceItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 358
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/api/DeviceItem;

    if-nez v0, :cond_1

    .line 359
    :cond_0
    const/4 v0, 0x0

    .line 360
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/api/DeviceItem;->getUdn()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lcom/samsung/api/DeviceItem;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/samsung/api/DeviceItem;->getUdn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getCompany()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mCompany:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceIP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    return v0
.end method

.method public getFirstIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mFirstIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mFriendlyName:Ljava/lang/String;

    return-object v0
.end method

.method public getIconList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mIconList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mModelName:Ljava/lang/String;

    return-object v0
.end method

.method public getModelNo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mModelNo:Ljava/lang/String;

    return-object v0
.end method

.method public getUdn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/api/DeviceItem;->mUdn:Ljava/lang/String;

    return-object v0
.end method

.method public isIgnoreNotification()Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/samsung/api/DeviceItem;->mIgnoreNotification:Z

    return v0
.end method

.method public isLocalDevice()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 326
    iget-object v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 328
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    const-string v2, "127.0.0.1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    const-string v2, "localhost"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceIP:Ljava/lang/String;

    invoke-static {}, Lcom/samsung/net/HostInterface;->getInterface()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDeviceType(I)V
    .locals 1
    .param p1, "deviceType"    # I

    .prologue
    const/4 v0, 0x5

    .line 264
    if-lez p1, :cond_0

    if-le p1, v0, :cond_1

    .line 265
    :cond_0
    iput v0, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    .line 268
    :goto_0
    return-void

    .line 267
    :cond_1
    iput p1, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    goto :goto_0
.end method

.method setDeviceType(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x4

    .line 271
    if-nez p1, :cond_0

    .line 272
    iput v2, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    .line 283
    :goto_0
    return-void

    .line 273
    :cond_0
    const-string v0, "MediaRenderer"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    goto :goto_0

    .line 275
    :cond_1
    const-string v0, "MediaServer"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 276
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    goto :goto_0

    .line 277
    :cond_2
    const-string v0, "MediaPrinter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 278
    iput v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    goto :goto_0

    .line 279
    :cond_3
    const-string v0, "Personal"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 280
    iput v1, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    goto :goto_0

    .line 282
    :cond_4
    iput v2, p0, Lcom/samsung/api/DeviceItem;->mDeviceType:I

    goto :goto_0
.end method
