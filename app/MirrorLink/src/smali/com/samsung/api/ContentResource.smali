.class public Lcom/samsung/api/ContentResource;
.super Ljava/lang/Object;
.source "ContentResource.java"


# instance fields
.field private mDuration:Ljava/lang/String;

.field private mMediaProfile:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;

.field private mProtocolInfo:Ljava/lang/String;

.field private mResolutionInfo:Ljava/lang/String;

.field private mResuUrl:Ljava/lang/String;

.field private mSize:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mResuUrl:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mProtocolInfo:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mSize:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mResolutionInfo:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mDuration:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mMimeType:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mMediaProfile:Ljava/lang/String;

    .line 69
    return-void
.end method


# virtual methods
.method public getCreateObjectProtocolInfo()Ljava/lang/String;
    .locals 5

    .prologue
    .line 121
    const-string v0, "*"

    .line 122
    .local v0, "cProtocolInfo":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/api/ContentResource;->mProtocolInfo:Ljava/lang/String;

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "strSplit":[Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_0

    .line 127
    return-object v0

    .line 125
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mDuration:Ljava/lang/String;

    return-object v0
.end method

.method public getFileExtension()Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/api/ContentResource;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getPostfixByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaProfile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mMediaProfile:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocolInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mProtocolInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getResolutionInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mResolutionInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mResuUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/api/ContentResource;->mSize:Ljava/lang/String;

    return-object v0
.end method

.method setDuration(Ljava/lang/String;)V
    .locals 1
    .param p1, "duration"    # Ljava/lang/String;

    .prologue
    .line 184
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    const-string p1, "00:00:00"

    .line 186
    :cond_1
    iput-object p1, p0, Lcom/samsung/api/ContentResource;->mDuration:Ljava/lang/String;

    .line 187
    return-void
.end method

.method setProtocolInfo(Ljava/lang/String;)V
    .locals 4
    .param p1, "protocolInfo"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 136
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "strSplit":[Ljava/lang/String;
    array-length v1, v0

    if-le v1, v2, :cond_0

    .line 139
    aget-object v1, v0, v2

    iput-object v1, p0, Lcom/samsung/api/ContentResource;->mMimeType:Ljava/lang/String;

    .line 140
    :cond_0
    array-length v1, v0

    if-le v1, v3, :cond_1

    .line 141
    aget-object v1, v0, v3

    iput-object v1, p0, Lcom/samsung/api/ContentResource;->mMediaProfile:Ljava/lang/String;

    .line 142
    :cond_1
    iput-object p1, p0, Lcom/samsung/api/ContentResource;->mProtocolInfo:Ljava/lang/String;

    .line 143
    return-void
.end method

.method setResolutionInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "resolutionInfo"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/api/ContentResource;->mResolutionInfo:Ljava/lang/String;

    .line 173
    return-void
.end method

.method setResourceUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "resuUrl"    # Ljava/lang/String;

    .prologue
    .line 86
    if-nez p1, :cond_0

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentResource;->mResuUrl:Ljava/lang/String;

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_0
    iput-object p1, p0, Lcom/samsung/api/ContentResource;->mResuUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method setSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "size"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/api/ContentResource;->mSize:Ljava/lang/String;

    .line 159
    return-void
.end method
