.class public Lcom/samsung/api/HTTP_API;
.super Ljava/lang/Object;
.source "HTTP_API.java"


# static fields
.field public static final CACHE_CONTROL:Ljava/lang/String; = "Cache-Control"

.field public static final CHUNKED:Ljava/lang/String; = "Chunked"

.field public static final CLOSE:Ljava/lang/String; = "close"

.field public static final CONNECTION:Ljava/lang/String; = "Connection"

.field public static final CONTENT_LENGTH:Ljava/lang/String; = "Content-Length"

.field public static final CONTENT_RANGE:Ljava/lang/String; = "Content-Range"

.field public static final CONTENT_RANGE_BYTES:Ljava/lang/String; = "bytes"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field public static final CONTINUE_100:Ljava/lang/String; = "100-continue"

.field public static final CRLF:Ljava/lang/String; = "\r\n"

.field public static final DATE:Ljava/lang/String; = "Date"

.field public static final DEFAULT_CHUNK_SIZE:I = 0x4b000

.field public static final DEFAULT_DEVICENAME:Ljava/lang/String; = "Samsung Mobile"

.field public static final DEFAULT_PORT:I = 0x50

.field public static final DEFAULT_TIMEOUT:I = 0x1e

.field public static DEVICE_NAME:Ljava/lang/String; = null

.field public static DEVICE_TYPE:Ljava/lang/String; = null

.field static final DEVICE_TYPE_LIST:[Ljava/lang/String;

.field public static final EXPECT:Ljava/lang/String; = "Expect"

.field public static FRIENDLYNAME:Ljava/lang/String; = null

.field public static final GET:Ljava/lang/String; = "GET"

.field public static final HEAD:Ljava/lang/String; = "HEAD"

.field public static final HOST:Ljava/lang/String; = "HOST"

.field public static final KEEP_ALIVE:Ljava/lang/String; = "Keep-Alive"

.field public static final LOCATION:Ljava/lang/String; = "Location"

.field public static final MAX_AGE:Ljava/lang/String; = "max-age"

.field public static final M_SEARCH:Ljava/lang/String; = "M-SEARCH"

.field public static final NOTIFY:Ljava/lang/String; = "NOTIFY"

.field public static final NO_CACHE:Ljava/lang/String; = "no-cache"

.field public static final POST:Ljava/lang/String; = "POST"

.field public static final RANGE:Ljava/lang/String; = "Range"

.field public static final SERVER:Ljava/lang/String; = "Server"

.field public static final SOAP_ACTION:Ljava/lang/String; = "SOAPACTION"

.field public static final SUBSCRIBE:Ljava/lang/String; = "SUBSCRIBE"

.field public static final TAB:Ljava/lang/String; = "\t"

.field public static final TRANSFER_ENCODING:Ljava/lang/String; = "Transfer-Encoding"

.field public static final UNSUBSCRIBE:Ljava/lang/String; = "UNSUBSCRIBE"

.field public static final VERSION:Ljava/lang/String; = "1.1"

.field public static final VERSION_10:Ljava/lang/String; = "1.0"

.field public static final VERSION_11:Ljava/lang/String; = "1.1"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 113
    const-string v0, "SEC_HHP_Samsung Mobile/1.0"

    sput-object v0, Lcom/samsung/api/HTTP_API;->DEVICE_NAME:Ljava/lang/String;

    .line 114
    const-string v0, "Samsung Mobile"

    sput-object v0, Lcom/samsung/api/HTTP_API;->FRIENDLYNAME:Ljava/lang/String;

    .line 115
    const-string v0, "Mobile"

    sput-object v0, Lcom/samsung/api/HTTP_API;->DEVICE_TYPE:Ljava/lang/String;

    .line 116
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TV"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BD"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Mobile"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Tablet"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Camera"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Camcorder"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Notebook"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Netbook"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Desktop"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "AIO PC"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Ref"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "STB"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Media Player"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "NAS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/api/HTTP_API;->DEVICE_TYPE_LIST:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setDeviceName(Ljava/lang/String;)Z
    .locals 5
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 124
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_1

    .line 125
    :cond_0
    const/4 v3, 0x0

    .line 148
    :goto_0
    return v3

    .line 127
    :cond_1
    sput-object p0, Lcom/samsung/api/HTTP_API;->FRIENDLYNAME:Ljava/lang/String;

    .line 128
    const/4 v2, 0x1

    .line 130
    .local v2, "isAscii":Z
    const-string v3, "\r\n"

    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 131
    const/4 v2, 0x0

    .line 142
    :cond_2
    :goto_1
    if-eqz v2, :cond_6

    .line 143
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SEC_HHP_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/1.0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/api/HTTP_API;->DEVICE_NAME:Ljava/lang/String;

    .line 148
    :goto_2
    const/4 v3, 0x1

    goto :goto_0

    .line 133
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 134
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 135
    .local v0, "c":C
    if-ltz v0, :cond_4

    const/16 v3, 0x7f

    if-le v0, v3, :cond_5

    .line 136
    :cond_4
    const/4 v2, 0x0

    .line 137
    goto :goto_1

    .line 133
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 145
    .end local v0    # "c":C
    .end local v1    # "i":I
    :cond_6
    const-string v3, "SEC_HHP_Samsung Mobile/1.0"

    sput-object v3, Lcom/samsung/api/HTTP_API;->DEVICE_NAME:Ljava/lang/String;

    goto :goto_2
.end method

.method public static setDeviceType(Ljava/lang/String;)Z
    .locals 6
    .param p0, "deviceType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 156
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v1

    .line 158
    :cond_1
    sget-object v3, Lcom/samsung/api/HTTP_API;->DEVICE_TYPE_LIST:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 159
    .local v0, "type":Ljava/lang/String;
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 160
    sput-object p0, Lcom/samsung/api/HTTP_API;->DEVICE_TYPE:Ljava/lang/String;

    .line 161
    const/4 v1, 0x1

    goto :goto_0

    .line 158
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
