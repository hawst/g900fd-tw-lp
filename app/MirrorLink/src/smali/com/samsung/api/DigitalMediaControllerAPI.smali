.class public Lcom/samsung/api/DigitalMediaControllerAPI;
.super Ljava/lang/Object;
.source "DigitalMediaControllerAPI.java"


# static fields
.field public static final RENDERER:Ljava/lang/String; = "urn:schemas-upnp-org:device:MediaRenderer:1"

.field public static final SERVER:Ljava/lang/String; = "urn:schemas-upnp-org:device:MediaServer:1"

.field private static mCp:Lcom/samsung/upnp/ControlPoint;

.field private static volatile mInstance:Lcom/samsung/api/DigitalMediaControllerAPI;

.field private static mMutex:Ljava/lang/Object;


# instance fields
.field private mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

.field private mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

.field private mVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mInstance:Lcom/samsung/api/DigitalMediaControllerAPI;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mMutex:Ljava/lang/Object;

    .line 62
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-string v0, "libdoa1001svn321"

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mVersion:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 110
    new-instance v0, Lcom/samsung/upnp/ControlPoint;

    invoke-direct {v0}, Lcom/samsung/upnp/ControlPoint;-><init>()V

    sput-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    .line 111
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    const-string v1, "upnp:rootdevice"

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->addSearchTarget(Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    const-string v1, "urn:schemas-upnp-org:device:MediaServer:1"

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->addSearchTarget(Ljava/lang/String;)V

    .line 113
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    const-string v1, "urn:schemas-upnp-org:device:MediaRenderer:1"

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->addSearchTarget(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method private browseItemByObjectId(Lcom/samsung/api/DeviceItem;Ljava/lang/String;II)Ljava/util/List;
    .locals 19
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "objectId"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .param p4, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/api/DeviceItem;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/ContentItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 407
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 409
    .local v9, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/api/ContentItem;>;"
    if-nez p1, :cond_0

    .line 410
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0xb

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 412
    :cond_0
    if-ltz p3, :cond_1

    if-gez p4, :cond_2

    .line 413
    :cond_1
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x1

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 415
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v15

    if-nez v15, :cond_4

    .line 416
    :cond_3
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x11

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 418
    :cond_4
    sget-object v15, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v6

    .line 419
    .local v6, "device":Lcom/samsung/upnp/Device;
    if-nez v6, :cond_5

    .line 420
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0xa

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 422
    :cond_5
    const-string v15, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual {v6, v15}, Lcom/samsung/upnp/Device;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 423
    .local v4, "cds":Lcom/samsung/upnp/Service;
    if-nez v4, :cond_6

    .line 424
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x191

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 425
    :cond_6
    invoke-virtual {v4}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/upnp/media/server/action/BrowseAction;->createDefaultBrowseAction(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Action;

    move-result-object v2

    .line 429
    .local v2, "action":Lcom/samsung/upnp/Action;
    if-nez v2, :cond_7

    .line 430
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x191

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 432
    :cond_7
    const-string v15, "ObjectID"

    move-object/from16 v0, p2

    invoke-virtual {v2, v15, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string v15, "BrowseFlag"

    const-string v16, "BrowseDirectChildren"

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v15, "Filter"

    const-string v16, "*"

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v15, "StartingIndex"

    move/from16 v0, p3

    invoke-virtual {v2, v15, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 438
    const-string v15, "RequestedCount"

    move/from16 v0, p4

    invoke-virtual {v2, v15, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 440
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v15

    if-nez v15, :cond_9

    .line 441
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "DLNA : DMC Service"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "action fail:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 442
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v14

    .line 443
    .local v14, "status":Lcom/samsung/upnp/UPnPStatus;
    invoke-virtual {v14}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v15

    const/16 v16, 0x19a

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 444
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0xa

    .line 445
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v18

    .line 444
    invoke-direct/range {v15 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v15

    .line 447
    :cond_8
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x1f5

    .line 448
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v18

    .line 447
    invoke-direct/range {v15 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v15

    .line 452
    .end local v14    # "status":Lcom/samsung/upnp/UPnPStatus;
    :cond_9
    const-string v15, "Result"

    invoke-virtual {v2, v15}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 453
    .local v3, "argument":Lcom/samsung/upnp/Argument;
    if-nez v3, :cond_a

    .line 454
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x7

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 455
    :cond_a
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v12

    .line 457
    .local v12, "result":Ljava/lang/String;
    if-nez v12, :cond_b

    .line 458
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x7

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15

    .line 460
    :cond_b
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v11

    .line 463
    .local v11, "parser":Lcom/samsung/xml/Parser;
    :try_start_0
    invoke-virtual {v11, v12}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 464
    .local v13, "rootNode":Lcom/samsung/xml/Node;
    invoke-virtual {v13}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v1

    .line 466
    .local v1, "N":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-lt v8, v1, :cond_c

    .line 478
    .end local v1    # "N":I
    .end local v8    # "i":I
    .end local v13    # "rootNode":Lcom/samsung/xml/Node;
    :goto_1
    return-object v9

    .line 467
    .restart local v1    # "N":I
    .restart local v8    # "i":I
    .restart local v13    # "rootNode":Lcom/samsung/xml/Node;
    :cond_c
    invoke-virtual {v13, v8}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v10

    .line 468
    .local v10, "node":Lcom/samsung/xml/Node;
    new-instance v5, Lcom/samsung/api/ContentItem;

    invoke-direct {v5, v10}, Lcom/samsung/api/ContentItem;-><init>(Lcom/samsung/xml/Node;)V

    .line 469
    .local v5, "citem":Lcom/samsung/api/ContentItem;
    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 466
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 472
    .end local v1    # "N":I
    .end local v5    # "citem":Lcom/samsung/api/ContentItem;
    .end local v8    # "i":I
    .end local v10    # "node":Lcom/samsung/xml/Node;
    .end local v13    # "rootNode":Lcom/samsung/xml/Node;
    :catch_0
    move-exception v7

    .line 473
    .local v7, "e":Lcom/samsung/xml/ParserException;
    const-string v15, "DLNA : DMC Service (BrowseItem) xml parser error"

    invoke-static {v15}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 474
    .end local v7    # "e":Lcom/samsung/xml/ParserException;
    :catch_1
    move-exception v7

    .line 475
    .local v7, "e":Ljava/lang/NullPointerException;
    const-string v15, "DLNA : DMC Service (BrowseItem) xml parser error"

    invoke-static {v15}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 476
    new-instance v15, Lcom/samsung/api/DMCAPIException;

    const/16 v16, 0x7

    invoke-direct/range {v15 .. v16}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v15
.end method

.method public static getInstance()Lcom/samsung/api/DigitalMediaControllerAPI;
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mInstance:Lcom/samsung/api/DigitalMediaControllerAPI;

    if-nez v0, :cond_1

    .line 100
    sget-object v1, Lcom/samsung/api/DigitalMediaControllerAPI;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 101
    :try_start_0
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mInstance:Lcom/samsung/api/DigitalMediaControllerAPI;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/samsung/api/DigitalMediaControllerAPI;

    invoke-direct {v0}, Lcom/samsung/api/DigitalMediaControllerAPI;-><init>()V

    sput-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mInstance:Lcom/samsung/api/DigitalMediaControllerAPI;

    .line 100
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_1
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mInstance:Lcom/samsung/api/DigitalMediaControllerAPI;

    return-object v0

    .line 100
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private makeRequestedList(IILjava/util/ArrayList;)Ljava/util/List;
    .locals 2
    .param p1, "offset"    # I
    .param p2, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 278
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/DeviceItem;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 279
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 280
    :cond_0
    add-int v0, p1, p2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 281
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p3, p1, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 283
    :goto_0
    return-object v0

    :cond_1
    add-int v0, p1, p2

    invoke-virtual {p3, p1, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static final setDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-static {p0}, Lcom/samsung/api/HTTP_API;->setDeviceName(Ljava/lang/String;)Z

    .line 82
    return-void
.end method

.method public static final setInterface(Ljava/lang/String;)V
    .locals 0
    .param p0, "ifaddr"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-static {p0}, Lcom/samsung/net/HostInterface;->setInterface(Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method private unsubscribeDevice(Ljava/lang/String;)V
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 287
    if-nez p1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    sget-object v1, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v1, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 290
    .local v0, "device":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    .line 291
    sget-object v1, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ControlPoint;->unsubscribe(Lcom/samsung/upnp/Device;)V

    goto :goto_0
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 126
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z

    move-result v0

    return v0
.end method

.method public addDeviceChangeListener(Lcom/samsung/upnp/device/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/DeviceChangeListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->addDeviceChangeListener(Lcom/samsung/upnp/device/DeviceChangeListener;)Z

    move-result v0

    return v0
.end method

.method public addEventListener(Lcom/samsung/api/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/EventListener;

    .prologue
    .line 155
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->addEventListener(Lcom/samsung/api/EventListener;)Z

    move-result v0

    return v0
.end method

.method public addEventListener(Lcom/samsung/upnp/event/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/event/EventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 168
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->addEventListener(Lcom/samsung/upnp/event/EventListener;)Z

    move-result v0

    return v0
.end method

.method public browseItemsDown(Lcom/samsung/api/DeviceItem;Lcom/samsung/api/ContentItem;II)Ljava/util/List;
    .locals 2
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "item"    # Lcom/samsung/api/ContentItem;
    .param p3, "offset"    # I
    .param p4, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/api/DeviceItem;",
            "Lcom/samsung/api/ContentItem;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/ContentItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 321
    if-nez p2, :cond_0

    .line 322
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/16 v1, 0x11

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 324
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getContentType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 325
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 327
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/samsung/api/DigitalMediaControllerAPI;->browseItemByObjectId(Lcom/samsung/api/DeviceItem;Ljava/lang/String;II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method convertStringToDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 1684
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "hh:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1687
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1691
    :goto_0
    return-object v0

    .line 1689
    :catch_0
    move-exception v1

    .line 1691
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createObject(Lcom/samsung/api/ContentItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p1, "item"    # Lcom/samsung/api/ContentItem;
    .param p2, "targetDms"    # Ljava/lang/String;
    .param p3, "containerID"    # Ljava/lang/String;
    .param p4, "Elements"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 1203
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getResourceList()Ljava/util/List;

    move-result-object v17

    if-eqz v17, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getDefaultResouce()Lcom/samsung/api/ContentResource;

    move-result-object v17

    if-nez v17, :cond_1

    .line 1204
    :cond_0
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x11

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1207
    :cond_1
    sget-object v17, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v15

    .line 1208
    .local v15, "target":Lcom/samsung/upnp/Device;
    if-nez v15, :cond_2

    .line 1209
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x3

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1212
    :cond_2
    if-nez p3, :cond_3

    .line 1213
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x2

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1216
    :cond_3
    const-string v17, "CreateObject"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v5

    .line 1217
    .local v5, "createObject":Lcom/samsung/upnp/Action;
    if-nez v5, :cond_4

    .line 1218
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x191

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1220
    :cond_4
    const-string v17, "ContainerID"

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-virtual {v5, v0, v1}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    new-instance v6, Lcom/samsung/upnp/media/server/object/DIDLLite;

    invoke-direct {v6}, Lcom/samsung/upnp/media/server/object/DIDLLite;-><init>()V

    .line 1222
    .local v6, "didl":Lcom/samsung/upnp/media/server/object/DIDLLite;
    new-instance v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-direct {v3}, Lcom/samsung/upnp/media/server/object/item/ItemNode;-><init>()V

    .line 1223
    .local v3, "cNode":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setID(Ljava/lang/String;)V

    .line 1224
    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setParentID(Ljava/lang/String;)V

    .line 1225
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setRestricted(I)V

    .line 1226
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setTitle(Ljava/lang/String;)V

    .line 1227
    const-string v14, ""

    .line 1228
    .local v14, "strClass":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getContentType()I

    move-result v17

    packed-switch v17, :pswitch_data_0

    .line 1239
    :goto_0
    invoke-virtual {v3, v14}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setUPnPClass(Ljava/lang/String;)V

    .line 1241
    new-instance v10, Lcom/samsung/upnp/media/server/object/ContentProperty;

    invoke-direct {v10}, Lcom/samsung/upnp/media/server/object/ContentProperty;-><init>()V

    .line 1242
    .local v10, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    const-string v17, "res"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->setName(Ljava/lang/String;)V

    .line 1243
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getDefaultResouce()Lcom/samsung/api/ContentResource;

    move-result-object v4

    .line 1244
    .local v4, "contentResource":Lcom/samsung/api/ContentResource;
    if-eqz v4, :cond_5

    .line 1245
    const-string v17, "protocolInfo"

    invoke-virtual {v4}, Lcom/samsung/api/ContentResource;->getCreateObjectProtocolInfo()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Lcom/samsung/upnp/media/server/object/ContentProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    :cond_5
    const-string v17, "size"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getDefaultResourceSize()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Lcom/samsung/upnp/media/server/object/ContentProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    invoke-virtual {v3, v10}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)V

    .line 1250
    if-eqz v4, :cond_6

    .line 1251
    const-string v17, "sec:uploadItemID"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/samsung/api/ContentResource;->getMimeType()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/api/ContentItem;->getObjectId()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    :cond_6
    const-string v17, "sec:uploadDeviceType"

    sget-object v18, Lcom/samsung/api/HTTP_API;->DEVICE_TYPE:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1253
    const-string v17, "sec:uploadDeviceName"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "["

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v19, Lcom/samsung/api/HTTP_API;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/samsung/api/HTTP_API;->FRIENDLYNAME:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/media/server/object/DIDLLite;->setContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 1256
    const-string v17, "Elements"

    invoke-virtual {v6}, Lcom/samsung/upnp/media/server/object/DIDLLite;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    invoke-virtual {v5}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v17

    if-nez v17, :cond_7

    .line 1259
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "DLNA : DMC Service"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "action fail"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1260
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x1f5

    .line 1261
    invoke-virtual {v5}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v19

    .line 1262
    invoke-virtual {v5}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v20

    .line 1260
    invoke-direct/range {v17 .. v20}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v17

    .line 1230
    .end local v4    # "contentResource":Lcom/samsung/api/ContentResource;
    .end local v10    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    :pswitch_0
    const-string v14, "object.item.audioItem.musicTrack"

    .line 1231
    goto/16 :goto_0

    .line 1233
    :pswitch_1
    const-string v14, "object.item.imageItem.photo"

    .line 1234
    goto/16 :goto_0

    .line 1236
    :pswitch_2
    const-string v14, "object.item.videoItem.movie"

    goto/16 :goto_0

    .line 1265
    .restart local v4    # "contentResource":Lcom/samsung/api/ContentResource;
    .restart local v10    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    :cond_7
    const-string v17, "Result"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 1266
    .local v2, "argument":Lcom/samsung/upnp/Argument;
    if-nez v2, :cond_8

    .line 1267
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x1f5

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1269
    :cond_8
    invoke-virtual {v2}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v12

    .line 1271
    .local v12, "result":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v9

    .line 1272
    .local v9, "parser":Lcom/samsung/xml/Parser;
    if-nez v9, :cond_9

    .line 1273
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, -0x1

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1275
    :cond_9
    const/16 v16, 0x0

    .line 1277
    .local v16, "targetUri":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v9, v12}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 1278
    .local v13, "rootNode":Lcom/samsung/xml/Node;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v8

    .line 1279
    .local v8, "node":Lcom/samsung/xml/Node;
    if-eqz v8, :cond_a

    .line 1280
    const-string v17, "res"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v11

    .line 1281
    .local v11, "resNode":Lcom/samsung/xml/Node;
    if-eqz v11, :cond_a

    .line 1282
    const-string v17, "importUri"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v16

    .line 1290
    .end local v11    # "resNode":Lcom/samsung/xml/Node;
    :cond_a
    return-object v16

    .line 1284
    .end local v8    # "node":Lcom/samsung/xml/Node;
    .end local v13    # "rootNode":Lcom/samsung/xml/Node;
    :catch_0
    move-exception v7

    .line 1285
    .local v7, "e":Lcom/samsung/xml/ParserException;
    invoke-virtual {v7}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    .line 1286
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, 0x1f5

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1287
    .end local v7    # "e":Lcom/samsung/xml/ParserException;
    :catch_1
    move-exception v7

    .line 1288
    .local v7, "e":Ljava/lang/Exception;
    new-instance v17, Lcom/samsung/api/DMCAPIException;

    const/16 v18, -0x1

    invoke-direct/range {v17 .. v18}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v17

    .line 1228
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getBrowseTotalMatchCount(Lcom/samsung/api/DeviceItem;Lcom/samsung/api/ContentItem;)I
    .locals 9
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "item"    # Lcom/samsung/api/ContentItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x191

    const/16 v8, 0xa

    .line 354
    if-nez p2, :cond_0

    .line 355
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/16 v6, 0x11

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 357
    :cond_0
    if-nez p1, :cond_1

    .line 358
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/16 v6, 0xb

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 360
    :cond_1
    sget-object v5, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v5, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v3

    .line 361
    .local v3, "device":Lcom/samsung/upnp/Device;
    if-nez v3, :cond_2

    .line 362
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 365
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getChildCount()I

    move-result v5

    if-ltz v5, :cond_3

    .line 366
    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getChildCount()I

    move-result v5

    .line 401
    :goto_0
    return v5

    .line 368
    :cond_3
    const-string v5, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual {v3, v5}, Lcom/samsung/upnp/Device;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v2

    .line 369
    .local v2, "cds":Lcom/samsung/upnp/Service;
    if-nez v2, :cond_4

    .line 370
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 371
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/upnp/media/server/action/BrowseAction;->createDefaultBrowseAction(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 375
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_5

    .line 376
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 378
    :cond_5
    const-string v5, "ObjectID"

    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getObjectId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v5, "BrowseFlag"

    const-string v6, "BrowseDirectChildren"

    invoke-virtual {v0, v5, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v5, "Filter"

    const-string v6, "*"

    invoke-virtual {v0, v5, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const-string v5, "StartingIndex"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 384
    const-string v5, "RequestedCount"

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 386
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v5

    if-nez v5, :cond_7

    .line 387
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DLNA : DMC Service"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "action fail:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 388
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 389
    .local v4, "status":Lcom/samsung/upnp/UPnPStatus;
    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v5

    const/16 v6, 0x19a

    if-ne v5, v6, :cond_6

    .line 390
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    .line 391
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 390
    invoke-direct {v5, v8, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v5

    .line 393
    :cond_6
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/16 v6, 0x1f5

    .line 394
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v8

    .line 393
    invoke-direct {v5, v6, v7, v8}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v5

    .line 398
    .end local v4    # "status":Lcom/samsung/upnp/UPnPStatus;
    :cond_7
    const-string v5, "TotalMatches"

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 399
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    if-nez v1, :cond_8

    .line 400
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/4 v6, 0x7

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 401
    :cond_8
    invoke-virtual {v1}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v5

    goto/16 :goto_0
.end method

.method public getDMRList(II)Ljava/util/List;
    .locals 7
    .param p1, "offset"    # I
    .param p2, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 242
    sget-object v4, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4}, Lcom/samsung/upnp/ControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v1

    .line 243
    .local v1, "devList":Lcom/samsung/upnp/DeviceList;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 245
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/DeviceItem;>;"
    if-ltz p1, :cond_0

    if-gtz p2, :cond_1

    .line 246
    :cond_0
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 250
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 258
    invoke-direct {p0, p1, p2, v3}, Lcom/samsung/api/DigitalMediaControllerAPI;->makeRequestedList(IILjava/util/ArrayList;)Ljava/util/List;

    move-result-object v4

    return-object v4

    .line 250
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/Device;

    .line 251
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MediaRenderer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 252
    new-instance v2, Lcom/samsung/api/DeviceItem;

    invoke-direct {v2, v0}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    .line 253
    .local v2, "item":Lcom/samsung/api/DeviceItem;
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Lcom/samsung/api/DeviceItem;->setDeviceType(I)V

    .line 254
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDMSList(II)Ljava/util/List;
    .locals 8
    .param p1, "offset"    # I
    .param p2, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 215
    sget-object v4, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4}, Lcom/samsung/upnp/ControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v1

    .line 216
    .local v1, "devList":Lcom/samsung/upnp/DeviceList;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 218
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/DeviceItem;>;"
    if-ltz p1, :cond_0

    if-gtz p2, :cond_1

    .line 219
    :cond_0
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 221
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 229
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 230
    invoke-direct {p0, p1, p2, v3}, Lcom/samsung/api/DigitalMediaControllerAPI;->makeRequestedList(IILjava/util/ArrayList;)Ljava/util/List;

    move-result-object v4

    return-object v4

    .line 221
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/Device;

    .line 222
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MediaServer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 223
    new-instance v2, Lcom/samsung/api/DeviceItem;

    invoke-direct {v2, v0}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    .line 224
    .local v2, "item":Lcom/samsung/api/DeviceItem;
    invoke-virtual {v2, v7}, Lcom/samsung/api/DeviceItem;->setDeviceType(I)V

    .line 225
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/api/DeviceItem;
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 269
    sget-object v1, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v1, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 270
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_0

    .line 271
    const/4 v1, 0x0

    .line 273
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/api/DeviceItem;

    invoke-direct {v1, v0}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    goto :goto_0
.end method

.method public getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;
    .locals 1
    .param p1, "device"    # Lcom/samsung/api/DeviceItem;

    .prologue
    .line 617
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    return-object v0
.end method

.method public getMute(Lcom/samsung/api/DeviceItem;ILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x9

    .line 1075
    if-nez p1, :cond_0

    .line 1076
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1078
    :cond_0
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1079
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 1080
    const-string v2, "DLNA : DMC Service (getMute) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1081
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1084
    :cond_1
    const-string v2, "GetMute"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1085
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1086
    const-string v2, "DLNA : DMC Service (getMute) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1087
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1090
    :cond_2
    const-string v2, "InstanceID"

    invoke-virtual {v0, v2, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1091
    const-string v2, "Channel"

    invoke-virtual {v0, v2, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1094
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1095
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 1096
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1098
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1101
    :cond_4
    const-string v2, "CurrentMute"

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getPosition(Lcom/samsung/api/DeviceItem;)Lcom/samsung/api/ContentPositionInfo;
    .locals 12
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x9

    .line 1326
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    if-nez v8, :cond_0

    .line 1327
    new-instance v8, Lcom/samsung/api/ContentPositionInfo;

    invoke-direct {v8}, Lcom/samsung/api/ContentPositionInfo;-><init>()V

    iput-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    .line 1328
    :cond_0
    if-nez p1, :cond_1

    .line 1329
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0xc

    invoke-direct {v8, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 1330
    :cond_1
    sget-object v8, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v8, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v4

    .line 1332
    .local v4, "dmr":Lcom/samsung/upnp/Device;
    if-nez v4, :cond_2

    .line 1333
    const-string v8, "DLNA : DMC Service (getPosition) Fail cuz there is no DMR"

    invoke-static {v8}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1334
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v8, v11}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 1336
    :cond_2
    const-string v8, "GetPositionInfo"

    invoke-virtual {v4, v8}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v2

    .line 1337
    .local v2, "action":Lcom/samsung/upnp/Action;
    if-nez v2, :cond_3

    .line 1338
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v8, v11}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 1340
    :cond_3
    const-string v8, "InstanceID"

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1343
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1344
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "DLNA : DMC Service"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "action fail"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1345
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v8

    const/16 v9, 0x19a

    if-ne v8, v9, :cond_4

    .line 1346
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    .line 1347
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v9

    .line 1348
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v10

    .line 1346
    invoke-direct {v8, v11, v9, v10}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v8

    .line 1350
    :cond_4
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0x1f5

    .line 1351
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v10

    .line 1352
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v11

    .line 1350
    invoke-direct {v8, v9, v10, v11}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v8

    .line 1356
    :cond_5
    const-string v7, ""

    .line 1357
    .local v7, "trackDuration":Ljava/lang/String;
    const-string v6, ""

    .line 1358
    .local v6, "relTime":Ljava/lang/String;
    const-string v1, ""

    .line 1359
    .local v1, "absTime":Ljava/lang/String;
    const-string v5, ""

    .line 1360
    .local v5, "relCount":Ljava/lang/String;
    const-string v0, ""

    .line 1362
    .local v0, "absCount":Ljava/lang/String;
    const-string v8, "TrackDuration"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 1363
    .local v3, "arg":Lcom/samsung/upnp/Argument;
    if-eqz v3, :cond_6

    .line 1364
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 1365
    :cond_6
    const-string v8, "RelTime"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 1366
    if-eqz v3, :cond_7

    .line 1367
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 1368
    :cond_7
    const-string v8, "AbsTime"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 1369
    if-eqz v3, :cond_8

    .line 1370
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1371
    :cond_8
    const-string v8, "RelCount"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 1372
    if-eqz v3, :cond_9

    .line 1373
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 1374
    :cond_9
    const-string v8, "AbsCount"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 1375
    if-eqz v3, :cond_a

    .line 1376
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1378
    :cond_a
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v7}, Lcom/samsung/api/ContentPositionInfo;->setTrackDuration(Ljava/lang/String;)V

    .line 1379
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v6}, Lcom/samsung/api/ContentPositionInfo;->setRelTime(Ljava/lang/String;)V

    .line 1380
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v1}, Lcom/samsung/api/ContentPositionInfo;->setAbsTime(Ljava/lang/String;)V

    .line 1381
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v5}, Lcom/samsung/api/ContentPositionInfo;->setRelCount(Ljava/lang/String;)V

    .line 1382
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v0}, Lcom/samsung/api/ContentPositionInfo;->setAbsCount(Ljava/lang/String;)V

    .line 1384
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mPositionInfo:Lcom/samsung/api/ContentPositionInfo;

    return-object v8
.end method

.method public getSystemUpdateID(Lcom/samsung/api/DeviceItem;)I
    .locals 10
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x1f5

    const/16 v8, 0xa

    .line 1601
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1603
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/ContentItem;>;"
    if-nez p1, :cond_0

    .line 1604
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    const/16 v7, 0xb

    invoke-direct {v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 1606
    :cond_0
    sget-object v6, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v6, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 1607
    .local v2, "device":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 1608
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 1610
    :cond_1
    const-string v6, "GetSystemUpdateID"

    invoke-virtual {v2, v6}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1612
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1613
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    const/16 v7, 0x191

    invoke-direct {v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 1615
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1616
    const-string v6, "DLNA : DMC Service (GetSystemUpdateID) action fail"

    invoke-static {v6}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1617
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v6

    const/16 v7, 0x19a

    if-ne v6, v7, :cond_3

    .line 1618
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 1620
    :cond_3
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 1624
    :cond_4
    const-string v6, "Result"

    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 1626
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    const/4 v5, 0x0

    .line 1628
    .local v5, "result":I
    :try_start_0
    const-string v6, "Id"

    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 1632
    return v5

    .line 1629
    :catch_0
    move-exception v3

    .line 1630
    .local v3, "e":Ljava/lang/Exception;
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6
.end method

.method public getTakeOutGroupNames(Lcom/samsung/api/DeviceItem;)Ljava/lang/String;
    .locals 8
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xa

    .line 1436
    if-nez p1, :cond_0

    .line 1437
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1438
    :cond_0
    sget-object v4, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 1439
    .local v2, "device":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 1440
    const-string v4, "DLNA : DMC Service (getTakeOutGroupNames) Fail cuz there is no DMS"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1441
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1443
    :cond_1
    const-string v4, "X_GetTakeOutGroupNames"

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1444
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1445
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1447
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1448
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "action fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1449
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_3

    .line 1450
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    .line 1451
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    .line 1452
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    .line 1450
    invoke-direct {v4, v7, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 1454
    :cond_3
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x1f5

    .line 1455
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 1456
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 1454
    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 1459
    :cond_4
    const-string v4, "GroupNames"

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 1460
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    if-nez v1, :cond_5

    .line 1461
    const-string v3, ""

    .line 1463
    :goto_0
    return-object v3

    .line 1462
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 1463
    .local v3, "result":Ljava/lang/String;
    goto :goto_0
.end method

.method public getTotalMatchesInPlaylist(Lcom/samsung/api/DeviceItem;Ljava/lang/String;IILjava/lang/String;)I
    .locals 8
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "searchcriteriaString"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "sortCriteria"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x1f5

    const/16 v6, 0xa

    .line 1647
    if-nez p1, :cond_0

    .line 1648
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1650
    :cond_0
    sget-object v4, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1651
    .local v1, "device":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 1652
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1654
    :cond_1
    const-string v4, "Search"

    invoke-virtual {v1, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1655
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1656
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x191

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1658
    :cond_2
    const-string v4, "ContainerID"

    const-string v5, "0"

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    const-string v4, "SearchCriteria"

    invoke-virtual {v0, v4, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    const-string v4, "Filter"

    const-string v5, "*"

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    const-string v4, "StartingIndex"

    invoke-virtual {v0, v4, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1662
    const-string v4, "RequestedCount"

    invoke-virtual {v0, v4, p4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1663
    const-string v4, "SortCriteria"

    invoke-virtual {v0, v4, p5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1665
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1666
    const-string v4, "DLNA : DMC Service (SearchItem) action fail"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1667
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_3

    .line 1668
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1670
    :cond_3
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1674
    :cond_4
    const/4 v3, 0x0

    .line 1676
    .local v3, "result":I
    :try_start_0
    const-string v4, "TotalMatches"

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1680
    return v3

    .line 1677
    :catch_0
    move-exception v2

    .line 1678
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4
.end method

.method public getTransportInfo(Lcom/samsung/api/DeviceItem;)Ljava/lang/String;
    .locals 8
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x9

    .line 1398
    if-nez p1, :cond_0

    .line 1399
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1400
    :cond_0
    sget-object v4, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 1401
    .local v2, "dmr":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 1402
    const-string v4, "DLNA : DMC Service (getTransportInfo) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1403
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1405
    :cond_1
    const-string v4, "GetTransportInfo"

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1406
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1407
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1408
    :cond_2
    const-string v4, "InstanceID"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1410
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1411
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "action fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1412
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_3

    .line 1413
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    .line 1414
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    .line 1415
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    .line 1413
    invoke-direct {v4, v7, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 1417
    :cond_3
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x1f5

    .line 1418
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 1419
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 1417
    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 1422
    :cond_4
    const-string v4, "CurrentTransportState"

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 1423
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    if-nez v1, :cond_5

    .line 1424
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x7

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1425
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 1426
    .local v3, "result":Ljava/lang/String;
    return-object v3
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getVolume(Lcom/samsung/api/DeviceItem;ILjava/lang/String;)I
    .locals 10
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x1f5

    const/16 v8, 0x9

    .line 975
    if-nez p1, :cond_0

    .line 976
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 977
    :cond_0
    sget-object v5, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v5, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 979
    .local v2, "dmr":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 980
    const-string v5, "DLNA : DMC Service (getVolume) Fail cuz there is no DMR"

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 981
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 983
    :cond_1
    const-string v5, "GetVolume"

    invoke-virtual {v2, v5}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 984
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 985
    const-string v5, "DLNA : DMC Service (GetVolume) Fail cuz there is no DMR"

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 986
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 988
    :cond_2
    const-string v5, "InstanceID"

    invoke-virtual {v0, v5, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 989
    const-string v5, "Channel"

    invoke-virtual {v0, v5, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    const-string v5, "Channel"

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 991
    .local v1, "ch":Lcom/samsung/upnp/Argument;
    if-eqz v1, :cond_3

    invoke-virtual {v1, p3}, Lcom/samsung/upnp/Argument;->isAllowedValue(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 992
    :cond_3
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 993
    :cond_4
    invoke-virtual {v1, p3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 995
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v5

    if-nez v5, :cond_6

    .line 996
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DLNA : DMC Service"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "action fail"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 997
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v5

    const/16 v6, 0x19a

    if-ne v5, v6, :cond_5

    .line 998
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    .line 999
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 1000
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 998
    invoke-direct {v5, v8, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v5

    .line 1002
    :cond_5
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    .line 1003
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 1004
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 1002
    invoke-direct {v5, v9, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v5

    .line 1008
    :cond_6
    const/4 v4, 0x0

    .line 1010
    .local v4, "result":I
    :try_start_0
    const-string v5, "CurrentVolume"

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1014
    return v4

    .line 1011
    :catch_0
    move-exception v3

    .line 1012
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5
.end method

.method public isSearchable(Lcom/samsung/api/DeviceItem;)Z
    .locals 4
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 597
    if-nez p1, :cond_0

    .line 598
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 600
    :cond_0
    sget-object v3, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 601
    .local v1, "device":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_2

    .line 608
    :cond_1
    :goto_0
    return v2

    .line 604
    :cond_2
    const-string v3, "Search"

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 605
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-eqz v0, :cond_1

    .line 608
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isUploaderble(Lcom/samsung/api/DeviceItem;)Z
    .locals 4
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1301
    if-nez p1, :cond_0

    .line 1302
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1304
    :cond_0
    sget-object v3, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1305
    .local v1, "device":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_2

    .line 1312
    :cond_1
    :goto_0
    return v2

    .line 1308
    :cond_2
    const-string v3, "CreateObject"

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1309
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-eqz v0, :cond_1

    .line 1312
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public pauseItem(Lcom/samsung/api/DeviceItem;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 799
    if-nez p1, :cond_0

    .line 800
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 801
    :cond_0
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 803
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 804
    const-string v2, "DLNA : DMC Service (Pause) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 805
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 808
    :cond_1
    const-string v2, "Pause"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 809
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 810
    const-string v2, "DLNA : DMC Service (Pause) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 811
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 813
    :cond_2
    const-string v2, "InstanceID"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 815
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 816
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 817
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 818
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 819
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 820
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 818
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 822
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 823
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 824
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 822
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 827
    :cond_4
    return-void
.end method

.method public play(Lcom/samsung/api/DeviceItem;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 717
    if-nez p1, :cond_0

    .line 718
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 719
    :cond_0
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 720
    .local v0, "dmr":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_1

    .line 721
    const-string v2, "DLNA : DMC Service (Play) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 722
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 725
    :cond_1
    const-string v2, "Play"

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v1

    .line 726
    .local v1, "play":Lcom/samsung/upnp/Action;
    if-nez v1, :cond_2

    .line 727
    const-string v2, "DLNA : DMC Service (Play) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 728
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 730
    :cond_2
    const-string v2, "InstanceID"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 731
    const-string v2, "Speed"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 734
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 736
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 737
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 738
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 739
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 737
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 741
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 742
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 743
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 741
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 746
    :cond_4
    return-void
.end method

.method public refreshDeviceList()V
    .locals 1

    .prologue
    .line 1486
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/upnp/ControlPoint;->search()V

    .line 1487
    return-void
.end method

.method public refreshDeviceList(Ljava/lang/String;)V
    .locals 1
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 1472
    if-nez p1, :cond_1

    .line 1481
    :cond_0
    :goto_0
    return-void

    .line 1475
    :cond_1
    const-string v0, "urn:schemas-upnp-org:device:MediaRenderer:1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1476
    const-string v0, "urn:schemas-upnp-org:device:MediaServer:1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    :cond_2
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDeviceList(Ljava/lang/String;)V

    .line 1479
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->searchTarget(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 1493
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDeviceItem(Ljava/lang/String;)V

    .line 1494
    return-void
.end method

.method public removeDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 134
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z

    move-result v0

    return v0
.end method

.method public removeDeviceChangeListener(Lcom/samsung/upnp/device/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/DeviceChangeListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDeviceChangeListener(Lcom/samsung/upnp/device/DeviceChangeListener;)Z

    move-result v0

    return v0
.end method

.method public removeEventListener(Lcom/samsung/api/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/EventListener;

    .prologue
    .line 163
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeEventListener(Lcom/samsung/api/EventListener;)Z

    move-result v0

    return v0
.end method

.method public removeEventListener(Lcom/samsung/upnp/event/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/event/EventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 173
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeEventListener(Lcom/samsung/upnp/event/EventListener;)Z

    move-result v0

    return v0
.end method

.method public searchItem(Lcom/samsung/api/DeviceItem;Ljava/lang/String;IILjava/lang/String;)Ljava/util/List;
    .locals 15
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "searchcriteriaString"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "sortCriteria"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/api/DeviceItem;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/ContentItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 528
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 531
    .local v8, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/ContentItem;>;"
    if-nez p1, :cond_0

    .line 532
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/16 v14, 0xb

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 534
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_2

    .line 535
    :cond_1
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/4 v14, 0x2

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 537
    :cond_2
    if-ltz p3, :cond_3

    if-gez p4, :cond_4

    .line 538
    :cond_3
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/4 v14, 0x1

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 540
    :cond_4
    if-nez p5, :cond_5

    .line 541
    const-string p5, ""

    .line 543
    :cond_5
    sget-object v13, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v4

    .line 544
    .local v4, "device":Lcom/samsung/upnp/Device;
    if-nez v4, :cond_6

    .line 545
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/16 v14, 0xa

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 547
    :cond_6
    const-string v13, "Search"

    invoke-virtual {v4, v13}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v2

    .line 548
    .local v2, "action":Lcom/samsung/upnp/Action;
    if-nez v2, :cond_7

    .line 549
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/16 v14, 0x191

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 551
    :cond_7
    const-string v13, "ContainerID"

    const-string v14, "0"

    invoke-virtual {v2, v13, v14}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    const-string v13, "SearchCriteria"

    move-object/from16 v0, p2

    invoke-virtual {v2, v13, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    const-string v13, "Filter"

    const-string v14, "*"

    invoke-virtual {v2, v13, v14}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const-string v13, "StartingIndex"

    move/from16 v0, p3

    invoke-virtual {v2, v13, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 555
    const-string v13, "RequestedCount"

    move/from16 v0, p4

    invoke-virtual {v2, v13, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 556
    const-string v13, "SortCriteria"

    move-object/from16 v0, p5

    invoke-virtual {v2, v13, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v13

    if-nez v13, :cond_9

    .line 559
    const-string v13, "DLNA : DMC Service (SearchItem) action fail"

    invoke-static {v13}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 560
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v13

    const/16 v14, 0x19a

    if-ne v13, v14, :cond_8

    .line 561
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/16 v14, 0xa

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 563
    :cond_8
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/16 v14, 0x1f5

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 567
    :cond_9
    const-string v13, "Result"

    invoke-virtual {v2, v13}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 568
    .local v3, "argument":Lcom/samsung/upnp/Argument;
    if-nez v3, :cond_a

    .line 569
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/4 v14, 0x7

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13

    .line 570
    :cond_a
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v11

    .line 572
    .local v11, "result":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v10

    .line 574
    .local v10, "parser":Lcom/samsung/xml/Parser;
    :try_start_0
    invoke-virtual {v10, v11}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v12

    .line 575
    .local v12, "rootNode":Lcom/samsung/xml/Node;
    if-nez v12, :cond_c

    .line 576
    new-instance v13, Lcom/samsung/api/DMCAPIException;

    const/16 v14, 0x1f5

    invoke-direct {v13, v14}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v13
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 583
    .end local v12    # "rootNode":Lcom/samsung/xml/Node;
    :catch_0
    move-exception v5

    .line 584
    .local v5, "e":Lcom/samsung/xml/ParserException;
    const-string v13, "DLNA : DMC Service (BrowseItem) xml parser error"

    invoke-static {v13}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 585
    invoke-virtual {v5}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    .line 587
    .end local v5    # "e":Lcom/samsung/xml/ParserException;
    :cond_b
    return-object v8

    .line 577
    .restart local v12    # "rootNode":Lcom/samsung/xml/Node;
    :cond_c
    :try_start_1
    invoke-virtual {v12}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v1

    .line 578
    .local v1, "N":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v1, :cond_b

    .line 579
    invoke-virtual {v12, v6}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v9

    .line 580
    .local v9, "node":Lcom/samsung/xml/Node;
    new-instance v7, Lcom/samsung/api/ContentItem;

    invoke-direct {v7, v9}, Lcom/samsung/api/ContentItem;-><init>(Lcom/samsung/xml/Node;)V

    .line 581
    .local v7, "item":Lcom/samsung/api/ContentItem;
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/samsung/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_0

    .line 578
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public seek(Lcom/samsung/api/DeviceItem;ILjava/lang/String;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "target"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 848
    if-nez p1, :cond_0

    .line 849
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 850
    :cond_0
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 852
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 853
    const-string v2, "DLNA : DMC Service (Seek) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 854
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 857
    :cond_1
    invoke-virtual {p0, p3}, Lcom/samsung/api/DigitalMediaControllerAPI;->convertStringToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_2

    .line 858
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 860
    :cond_2
    const-string v2, "Seek"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 861
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_3

    .line 862
    const-string v2, "DLNA : DMC Service (Seek) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 863
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 866
    :cond_3
    const-string v2, "InstanceID"

    invoke-virtual {v0, v2, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 867
    const-string v2, "Unit"

    const-string v3, "REL_TIME"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    const-string v2, "Target"

    invoke-virtual {v0, v2, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_5

    .line 871
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 872
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_4

    .line 873
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 874
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 875
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 873
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 877
    :cond_4
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 878
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 879
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 877
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 882
    :cond_5
    return-void
.end method

.method public sendXMessage(Lcom/samsung/api/DeviceItem;Ljava/lang/String;)Z
    .locals 5
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v4, 0xa

    .line 1703
    if-nez p1, :cond_0

    .line 1704
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1706
    :cond_0
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1707
    .local v1, "device":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 1708
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1710
    :cond_1
    const-string v2, "X_SendMessage"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1711
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1712
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1713
    :cond_2
    const-string v2, "Message"

    invoke-virtual {v0, v2, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1716
    const-string v2, "DLNA : DMC Service (GetSystemUpdateID) action fail"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1717
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 1718
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1720
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 1723
    :cond_4
    const/4 v2, 0x1

    return v2
.end method

.method public setAvtTransport(Lcom/samsung/api/DeviceItem;Lcom/samsung/api/ContentItem;)V
    .locals 8
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "item"    # Lcom/samsung/api/ContentItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v7, 0x9

    .line 643
    if-nez p2, :cond_0

    .line 644
    const-string v4, "DLNA : DMC Service (SetAVTransportURI) Fail cuz there is no selected contents"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 645
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x11

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 648
    :cond_0
    if-nez p1, :cond_1

    .line 649
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 650
    :cond_1
    sget-object v4, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 652
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_2

    .line 653
    const-string v4, "DLNA : DMC Service (setAVTransportURI) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 654
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 657
    :cond_2
    const-string v4, "SetAVTransportURI"

    invoke-virtual {v1, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 658
    .local v0, "actionToDmr":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_3

    .line 659
    const-string v4, "DLNA : DMC Service (SetAVTransportURI) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 660
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 663
    :cond_3
    const-string v4, "InstanceID"

    invoke-virtual {v0, v4, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 664
    const-string v5, "CurrentURI"

    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getResourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/api/ContentResource;

    invoke-virtual {v4}, Lcom/samsung/api/ContentResource;->getResourceUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const-string v4, "CurrentURIMetaData"

    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const/4 v3, 0x0

    .line 669
    .local v3, "tryCount":I
    :goto_0
    const/4 v4, 0x3

    if-lt v3, v4, :cond_5

    .line 697
    :cond_4
    return-void

    .line 670
    :cond_5
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_4

    .line 671
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "action fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 673
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_6

    .line 674
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    invoke-direct {v4, v7, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 676
    :cond_6
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x2c1

    if-ne v4, v5, :cond_7

    .line 678
    add-int/lit8 v3, v3, 0x1

    .line 679
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service Retry setAVTranport Action!!!"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 681
    mul-int/lit16 v4, v3, 0xc8

    int-to-long v4, v4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 682
    :catch_0
    move-exception v2

    .line 683
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    .line 687
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_7
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x1f5

    .line 688
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 689
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 688
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 690
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 687
    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4
.end method

.method public setMute(Lcom/samsung/api/DeviceItem;ILjava/lang/String;Z)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "desiredMute"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 1029
    if-nez p1, :cond_0

    .line 1030
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0xc

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1032
    :cond_0
    sget-object v3, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 1033
    .local v2, "dmr":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 1034
    const-string v3, "DLNA : DMC Service (getMute) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1035
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1038
    :cond_1
    const-string v3, "SetMute"

    invoke-virtual {v2, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1039
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1040
    const-string v3, "DLNA : DMC Service (SetMute) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1041
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x191

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1043
    :cond_2
    const-string v3, "InstanceID"

    invoke-virtual {v0, v3, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1044
    const-string v3, "Channel"

    invoke-virtual {v0, v3, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    const-string v1, ""

    .line 1046
    .local v1, "desiredMuteStr":Ljava/lang/String;
    if-eqz p4, :cond_3

    .line 1047
    const-string v1, "true"

    .line 1052
    :goto_0
    const-string v3, "DesiredMute"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1056
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DLNA : DMC Service"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "action fail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1057
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v3

    const/16 v4, 0x19a

    if-ne v3, v4, :cond_4

    .line 1058
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1049
    :cond_3
    const-string v1, "false"

    goto :goto_0

    .line 1060
    :cond_4
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x1f5

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1063
    :cond_5
    return-void
.end method

.method public setVolume(Lcom/samsung/api/DeviceItem;ILjava/lang/String;I)V
    .locals 7
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "vol"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x9

    const/4 v4, 0x2

    .line 910
    if-nez p1, :cond_0

    .line 911
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0xc

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 912
    :cond_0
    sget-object v3, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 914
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 915
    const-string v3, "DLNA : DMC Service (SetVolume) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 916
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 919
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 920
    :cond_2
    const-string p3, "Master"

    .line 922
    :cond_3
    const-string v3, "SetVolume"

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 923
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_4

    .line 924
    const-string v3, "DLNA : DMC Service (SetVolume) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 925
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x191

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 928
    :cond_4
    const-string v3, "Channel"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 929
    .local v2, "temp":Lcom/samsung/upnp/Argument;
    if-eqz v2, :cond_5

    invoke-virtual {v2, p3}, Lcom/samsung/upnp/Argument;->isAllowedValue(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 930
    :cond_5
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 932
    :cond_6
    const-string v3, "DesiredVolume"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 933
    if-eqz v2, :cond_7

    invoke-virtual {v2, p4}, Lcom/samsung/upnp/Argument;->isAllowedValue(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 934
    :cond_7
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 936
    :cond_8
    const-string v3, "InstanceID"

    invoke-virtual {v0, v3, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 937
    const-string v3, "Channel"

    invoke-virtual {v0, v3, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    const-string v3, "DesiredVolume"

    invoke-virtual {v0, v3, p4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 940
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v3

    if-nez v3, :cond_a

    .line 941
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DLNA : DMC Service"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "action fail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 942
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v3

    const/16 v4, 0x19a

    if-ne v3, v4, :cond_9

    .line 943
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    .line 944
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 945
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 943
    invoke-direct {v3, v6, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v3

    .line 947
    :cond_9
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x1f5

    .line 948
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    .line 949
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    .line 947
    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v3

    .line 952
    :cond_a
    return-void
.end method

.method public startDMC()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 186
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/upnp/ControlPoint;->start()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 188
    :cond_0
    return-void
.end method

.method public startSimpleServer(Ljava/util/HashMap;)Lcom/samsung/api/ContentItem;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/api/ContentItem;"
        }
    .end annotation

    .prologue
    .line 1151
    .local p1, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    if-eqz v2, :cond_0

    .line 1152
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v2}, Lcom/samsung/upnp/SimpleDevice;->stop()Z

    .line 1153
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 1155
    :cond_0
    new-instance v2, Lcom/samsung/upnp/SimpleDevice;

    invoke-direct {v2, p1}, Lcom/samsung/upnp/SimpleDevice;-><init>(Ljava/util/HashMap;)V

    iput-object v2, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 1156
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v2}, Lcom/samsung/upnp/SimpleDevice;->start()Z

    .line 1158
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v2}, Lcom/samsung/upnp/SimpleDevice;->getItemNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 1159
    .local v0, "info":Lcom/samsung/xml/Node;
    new-instance v1, Lcom/samsung/api/ContentItem;

    invoke-direct {v1, v0}, Lcom/samsung/api/ContentItem;-><init>(Lcom/samsung/xml/Node;)V

    .line 1160
    .local v1, "result":Lcom/samsung/api/ContentItem;
    return-object v1
.end method

.method public stopItem(Lcom/samsung/api/DeviceItem;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 759
    if-nez p1, :cond_0

    .line 760
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 761
    :cond_0
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 762
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 763
    const-string v2, "DLNA : DMC Service (Stop) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 764
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 767
    :cond_1
    const-string v2, "Stop"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 768
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 769
    const-string v2, "DLNA : DMC Service (Stop) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 770
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 772
    :cond_2
    const-string v2, "InstanceID"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 773
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    .line 774
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 775
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 776
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 777
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 778
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 779
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 777
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 781
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 782
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 783
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 781
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 786
    :cond_4
    return-void
.end method

.method public stopSimpleServer()V
    .locals 1

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    if-eqz v0, :cond_0

    .line 1168
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v0}, Lcom/samsung/upnp/SimpleDevice;->stop()Z

    .line 1169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 1171
    :cond_0
    return-void
.end method

.method public subscribePlayer(Lcom/samsung/api/DeviceItem;)Z
    .locals 8
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1502
    if-nez p1, :cond_0

    move v3, v4

    .line 1520
    :goto_0
    return v3

    .line 1505
    :cond_0
    sget-object v3, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1507
    .local v0, "device":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_5

    .line 1508
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v1

    .line 1509
    .local v1, "sList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ServiceList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .end local v1    # "sList":Lcom/samsung/upnp/ServiceList;
    :cond_2
    move v3, v5

    .line 1520
    goto :goto_0

    .line 1509
    .restart local v1    # "sList":Lcom/samsung/upnp/ServiceList;
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .local v2, "service":Ljava/lang/Object;
    move-object v3, v2

    .line 1510
    check-cast v3, Lcom/samsung/upnp/Service;

    invoke-virtual {v3}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v3

    const-string v7, "AVTransport"

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1511
    sget-object v3, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    check-cast v2, Lcom/samsung/upnp/Service;

    .end local v2    # "service":Ljava/lang/Object;
    invoke-virtual {v3, v2}, Lcom/samsung/upnp/ControlPoint;->subscribe(Lcom/samsung/upnp/Service;)Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v5

    .line 1512
    goto :goto_0

    :cond_4
    move v3, v4

    .line 1514
    goto :goto_0

    .line 1517
    .end local v1    # "sList":Lcom/samsung/upnp/ServiceList;
    :cond_5
    if-nez v0, :cond_2

    move v3, v5

    .line 1518
    goto :goto_0
.end method

.method public switchPlayer(Lcom/samsung/api/DeviceItem;Lcom/samsung/api/DeviceItem;)Z
    .locals 4
    .param p1, "oldPlayer"    # Lcom/samsung/api/DeviceItem;
    .param p2, "newPlayer"    # Lcom/samsung/api/DeviceItem;

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 1531
    sget-object v2, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1533
    .local v0, "device":Lcom/samsung/upnp/Device;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/api/DeviceItem;->getDeviceType()I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 1543
    :cond_0
    :goto_0
    return v1

    .line 1536
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/samsung/api/DeviceItem;->getDeviceType()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1539
    :cond_2
    if-eqz v0, :cond_3

    .line 1540
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/api/DigitalMediaControllerAPI;->unsubscribeDevice(Ljava/lang/String;)V

    .line 1543
    :cond_3
    invoke-virtual {p0, p2}, Lcom/samsung/api/DigitalMediaControllerAPI;->subscribePlayer(Lcom/samsung/api/DeviceItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public terminateDMC()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 197
    sget-object v0, Lcom/samsung/api/DigitalMediaControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/upnp/ControlPoint;->stop()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 199
    :cond_0
    return-void
.end method
