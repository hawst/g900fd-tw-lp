.class public Lcom/samsung/api/ContentItem;
.super Ljava/lang/Object;
.source "ContentItem.java"


# static fields
.field public static final CONTENT_AUDIO:I = 0x3

.field public static final CONTENT_CONTAINER:I = 0x2

.field public static final CONTENT_ETC:I = 0x6

.field public static final CONTENT_IMAGE:I = 0x5

.field public static final CONTENT_VIDEO:I = 0x4


# instance fields
.field private mAlbumArtUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChildCount:I

.field private mContentType:I

.field private mContentTypeStr:Ljava/lang/String;

.field private mDate:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mObjectId:Ljava/lang/String;

.field private mObjectUpdateID:I

.field private mParentID:Ljava/lang/String;

.field private mRawNode:Lcom/samsung/xml/Node;

.field private mResourceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/api/ContentResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mParentID:Ljava/lang/String;

    .line 84
    iput v1, p0, Lcom/samsung/api/ContentItem;->mChildCount:I

    .line 88
    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mRawNode:Lcom/samsung/xml/Node;

    .line 124
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mObjectId:Ljava/lang/String;

    .line 125
    const-string v0, "-1"

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mParentID:Ljava/lang/String;

    .line 126
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/api/ContentItem;->mContentType:I

    .line 127
    iput v1, p0, Lcom/samsung/api/ContentItem;->mChildCount:I

    .line 128
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mName:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    .line 131
    return-void
.end method

.method public constructor <init>(Lcom/samsung/xml/Node;)V
    .locals 2
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    const/4 v1, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object v1, p0, Lcom/samsung/api/ContentItem;->mParentID:Ljava/lang/String;

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/api/ContentItem;->mChildCount:I

    .line 88
    iput-object v1, p0, Lcom/samsung/api/ContentItem;->mRawNode:Lcom/samsung/xml/Node;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    .line 140
    iput-object p1, p0, Lcom/samsung/api/ContentItem;->mRawNode:Lcom/samsung/xml/Node;

    .line 141
    invoke-direct {p0, p1}, Lcom/samsung/api/ContentItem;->parseValues(Lcom/samsung/xml/Node;)V

    .line 142
    return-void
.end method

.method private getResourceByProfile(Ljava/lang/String;)Lcom/samsung/api/ContentResource;
    .locals 4
    .param p1, "profile"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 361
    iget-object v3, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    move-object v1, v2

    .line 369
    :cond_0
    :goto_0
    return-object v1

    .line 364
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    move-object v1, v2

    .line 369
    goto :goto_0

    .line 365
    :cond_2
    iget-object v3, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/api/ContentResource;

    .line 366
    .local v1, "res":Lcom/samsung/api/ContentResource;
    invoke-virtual {v1}, Lcom/samsung/api/ContentResource;->getProtocolInfo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private parseValues(Lcom/samsung/xml/Node;)V
    .locals 17
    .param p1, "rawNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 385
    if-nez p1, :cond_1

    .line 497
    :cond_0
    return-void

    .line 388
    :cond_1
    move-object/from16 v0, p1

    instance-of v15, v0, Lcom/samsung/upnp/media/server/object/ContentNode;

    if-eqz v15, :cond_6

    move-object/from16 v1, p1

    .line 389
    check-cast v1, Lcom/samsung/upnp/media/server/object/ContentNode;

    .line 390
    .local v1, "cNode":Lcom/samsung/upnp/media/server/object/ContentNode;
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getParentID()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mParentID:Ljava/lang/String;

    .line 391
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mObjectId:Ljava/lang/String;

    .line 392
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mName:Ljava/lang/String;

    .line 393
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getUPnPClass()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    .line 394
    const-string v15, "dc:date"

    invoke-virtual {v1, v15}, Lcom/samsung/upnp/media/server/object/ContentNode;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mDate:Ljava/lang/String;

    .line 396
    :try_start_0
    const-string v15, "upnp:objectUpdateID"

    invoke-virtual {v1, v15}, Lcom/samsung/upnp/media/server/object/ContentNode;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mObjectUpdateID:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    const-string v16, "object.container"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 402
    const/4 v15, 0x2

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    .line 413
    :goto_1
    instance-of v15, v1, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    if-eqz v15, :cond_0

    move-object v7, v1

    .line 414
    check-cast v7, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .line 415
    .local v7, "iNode":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    invoke-virtual {v7}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getResources()Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    move-result-object v12

    .line 417
    .local v12, "resList":Lcom/samsung/upnp/media/server/object/ContentPropertyList;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    invoke-virtual {v12}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->size()I

    move-result v15

    if-ge v6, v15, :cond_0

    .line 418
    invoke-virtual {v12, v6}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/upnp/media/server/object/ContentProperty;

    .line 419
    .local v3, "cp":Lcom/samsung/upnp/media/server/object/ContentProperty;
    new-instance v13, Lcom/samsung/api/ContentResource;

    invoke-direct {v13}, Lcom/samsung/api/ContentResource;-><init>()V

    .line 420
    .local v13, "resource":Lcom/samsung/api/ContentResource;
    const-string v15, "duration"

    invoke-virtual {v3, v15}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setDuration(Ljava/lang/String;)V

    .line 421
    const-string v15, "protocolInfo"

    invoke-virtual {v3, v15}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setProtocolInfo(Ljava/lang/String;)V

    .line 422
    const-string v15, "resolution"

    invoke-virtual {v3, v15}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setResolutionInfo(Ljava/lang/String;)V

    .line 423
    invoke-virtual {v3}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setResourceUrl(Ljava/lang/String;)V

    .line 424
    const-string v15, "size"

    invoke-virtual {v3, v15}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setSize(Ljava/lang/String;)V

    .line 425
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 397
    .end local v3    # "cp":Lcom/samsung/upnp/media/server/object/ContentProperty;
    .end local v6    # "i":I
    .end local v7    # "iNode":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .end local v12    # "resList":Lcom/samsung/upnp/media/server/object/ContentPropertyList;
    .end local v13    # "resource":Lcom/samsung/api/ContentResource;
    :catch_0
    move-exception v5

    .line 398
    .local v5, "e":Ljava/lang/Exception;
    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mObjectUpdateID:I

    goto :goto_0

    .line 403
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    const-string v16, "object.item.imageItem"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 404
    const/4 v15, 0x5

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto :goto_1

    .line 405
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    const-string v16, "object.item.audioItem"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 406
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto :goto_1

    .line 407
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    const-string v16, "object.item.videoItem"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 408
    const/4 v15, 0x4

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto/16 :goto_1

    .line 410
    :cond_5
    const/4 v15, 0x6

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto/16 :goto_1

    .line 431
    .end local v1    # "cNode":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_6
    const-string v15, "parentID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mParentID:Ljava/lang/String;

    .line 432
    const-string v15, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getAttribute(Ljava/lang/String;)Lcom/samsung/xml/Attribute;

    move-result-object v8

    .line 433
    .local v8, "idAttribute":Lcom/samsung/xml/Attribute;
    if-eqz v8, :cond_7

    .line 434
    invoke-virtual {v8}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mObjectId:Ljava/lang/String;

    .line 436
    :cond_7
    const-string v15, "dc:title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v9

    .line 437
    .local v9, "nameNode":Lcom/samsung/xml/Node;
    if-eqz v9, :cond_8

    .line 438
    invoke-virtual {v9}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mName:Ljava/lang/String;

    .line 441
    :cond_8
    const-string v15, "upnp:class"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 442
    .local v2, "classNode":Lcom/samsung/xml/Node;
    const-string v14, ""

    .line 443
    .local v14, "strClass":Ljava/lang/String;
    if-eqz v2, :cond_9

    .line 444
    invoke-virtual {v2}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v14

    .line 445
    :cond_9
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    .line 447
    const-string v15, "dc:date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v4

    .line 448
    .local v4, "dateNode":Lcom/samsung/xml/Node;
    if-eqz v4, :cond_a

    .line 449
    invoke-virtual {v4}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/api/ContentItem;->mDate:Ljava/lang/String;

    .line 452
    :cond_a
    :try_start_1
    const-string v15, "upnp:objectUpdateID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v11

    .line 453
    .local v11, "objectUpdateNode":Lcom/samsung/xml/Node;
    if-eqz v11, :cond_b

    .line 454
    invoke-virtual {v11}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mObjectUpdateID:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 460
    .end local v11    # "objectUpdateNode":Lcom/samsung/xml/Node;
    :cond_b
    :goto_3
    :try_start_2
    const-string v15, "childCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mChildCount:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    .line 465
    :goto_4
    const-string v15, "object.container"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 466
    const/4 v15, 0x2

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    .line 477
    :goto_5
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v15

    if-lt v6, v15, :cond_11

    .line 490
    const/4 v6, 0x0

    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v15

    if-ge v6, v15, :cond_0

    .line 491
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v10

    .line 492
    .local v10, "node":Lcom/samsung/xml/Node;
    if-eqz v10, :cond_c

    invoke-virtual {v10}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "albumArtURI"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 493
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    invoke-virtual {v10}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 455
    .end local v6    # "i":I
    .end local v10    # "node":Lcom/samsung/xml/Node;
    :catch_1
    move-exception v5

    .line 456
    .local v5, "e":Ljava/lang/NumberFormatException;
    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mObjectUpdateID:I

    goto :goto_3

    .line 461
    .end local v5    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v5

    .line 462
    .restart local v5    # "e":Ljava/lang/NumberFormatException;
    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mChildCount:I

    goto :goto_4

    .line 467
    .end local v5    # "e":Ljava/lang/NumberFormatException;
    :cond_d
    const-string v15, "object.item.imageItem"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 468
    const/4 v15, 0x5

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto :goto_5

    .line 469
    :cond_e
    const-string v15, "object.item.audioItem"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 470
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto :goto_5

    .line 471
    :cond_f
    const-string v15, "object.item.videoItem"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 472
    const/4 v15, 0x4

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto :goto_5

    .line 474
    :cond_10
    const/4 v15, 0x6

    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/api/ContentItem;->mContentType:I

    goto :goto_5

    .line 478
    .restart local v6    # "i":I
    :cond_11
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v10

    .line 479
    .restart local v10    # "node":Lcom/samsung/xml/Node;
    if-eqz v10, :cond_12

    invoke-virtual {v10}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "res"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_12

    .line 480
    new-instance v13, Lcom/samsung/api/ContentResource;

    invoke-direct {v13}, Lcom/samsung/api/ContentResource;-><init>()V

    .line 481
    .restart local v13    # "resource":Lcom/samsung/api/ContentResource;
    const-string v15, "duration"

    invoke-virtual {v10, v15}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setDuration(Ljava/lang/String;)V

    .line 482
    const-string v15, "protocolInfo"

    invoke-virtual {v10, v15}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setProtocolInfo(Ljava/lang/String;)V

    .line 483
    const-string v15, "resolution"

    invoke-virtual {v10, v15}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setResolutionInfo(Ljava/lang/String;)V

    .line 484
    invoke-virtual {v10}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setResourceUrl(Ljava/lang/String;)V

    .line 485
    const-string v15, "size"

    invoke-virtual {v10, v15}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/api/ContentResource;->setSize(Ljava/lang/String;)V

    .line 486
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    .end local v13    # "resource":Lcom/samsung/api/ContentResource;
    :cond_12
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_6
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 342
    if-nez p1, :cond_0

    move v0, v1

    .line 352
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .line 344
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/samsung/api/ContentItem;

    if-nez v0, :cond_1

    move v0, v1

    .line 345
    goto :goto_0

    .line 346
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 347
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 348
    check-cast v0, Lcom/samsung/api/ContentItem;

    invoke-virtual {v0}, Lcom/samsung/api/ContentItem;->getContentType()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/api/ContentItem;->getContentType()I

    move-result v2

    if-eq v0, v2, :cond_3

    move v0, v1

    .line 349
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 350
    check-cast v0, Lcom/samsung/api/ContentItem;

    invoke-virtual {v0}, Lcom/samsung/api/ContentItem;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast p1, Lcom/samsung/api/ContentItem;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/samsung/api/ContentItem;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/api/ContentItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 351
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 352
    goto :goto_0
.end method

.method public getAlbumArtUris()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChildCount()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/samsung/api/ContentItem;->mChildCount:I

    return v0
.end method

.method public getContentType()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/samsung/api/ContentItem;->mContentType:I

    return v0
.end method

.method public getContentTypeStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mContentTypeStr:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultResouce()Lcom/samsung/api/ContentResource;
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 242
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/api/ContentResource;

    goto :goto_0
.end method

.method public getDefaultResourceDuration()Ljava/lang/String;
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 298
    const-string v0, "00:00:00"

    .line 299
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/api/ContentResource;

    invoke-virtual {v0}, Lcom/samsung/api/ContentResource;->getDuration()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDefaultResourceSize()Ljava/lang/String;
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 312
    const-string v0, "0"

    .line 313
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/api/ContentResource;

    invoke-virtual {v0}, Lcom/samsung/api/ContentResource;->getSize()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getObjectId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mObjectId:Ljava/lang/String;

    return-object v0
.end method

.method public getObjectUpdateID()I
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lcom/samsung/api/ContentItem;->mObjectUpdateID:I

    return v0
.end method

.method public getParentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mParentID:Ljava/lang/String;

    return-object v0
.end method

.method public getRawNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mRawNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method public getResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/ContentResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/api/ContentItem;->mResourceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSnapImageUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 272
    const/4 v0, 0x0

    .line 274
    .local v0, "res":Lcom/samsung/api/ContentResource;
    invoke-virtual {p0}, Lcom/samsung/api/ContentItem;->getThumbnailResource()Lcom/samsung/api/ContentResource;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {v0}, Lcom/samsung/api/ContentResource;->getResourceUrl()Ljava/lang/String;

    move-result-object v1

    .line 285
    :goto_0
    return-object v1

    .line 278
    :cond_0
    const-string v1, "_SM"

    invoke-direct {p0, v1}, Lcom/samsung/api/ContentItem;->getResourceByProfile(Ljava/lang/String;)Lcom/samsung/api/ContentResource;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_1

    .line 280
    invoke-virtual {v0}, Lcom/samsung/api/ContentResource;->getResourceUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 282
    :cond_1
    iget-object v1, p0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 283
    iget-object v1, p0, Lcom/samsung/api/ContentItem;->mAlbumArtUris:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    .line 285
    :cond_2
    const-string v1, ""

    goto :goto_0
.end method

.method public getThumbnailResource()Lcom/samsung/api/ContentResource;
    .locals 1

    .prologue
    .line 253
    const-string v0, "_TN"

    invoke-direct {p0, v0}, Lcom/samsung/api/ContentItem;->getResourceByProfile(Ljava/lang/String;)Lcom/samsung/api/ContentResource;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 321
    new-instance v0, Lcom/samsung/upnp/media/server/object/DIDLLiteNode;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/DIDLLiteNode;-><init>()V

    .line 322
    .local v0, "node":Lcom/samsung/upnp/media/server/object/DIDLLiteNode;
    iget-object v1, p0, Lcom/samsung/api/ContentItem;->mRawNode:Lcom/samsung/xml/Node;

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/DIDLLiteNode;->addNode(Lcom/samsung/xml/Node;)V

    .line 323
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/DIDLLiteNode;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
