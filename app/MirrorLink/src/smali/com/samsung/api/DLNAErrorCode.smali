.class public Lcom/samsung/api/DLNAErrorCode;
.super Ljava/lang/Object;
.source "DLNAErrorCode.java"


# static fields
.field public static final ACTIONFAILEDRESPONSE:I = 0x1f5

.field public static final APINOTSUPPORTED:I = -0x2

.field public static final BADRESPONSE:I = 0x7

.field public static final BUSY:I = 0xe

.field public static final CONTENTSNOTSELECTED:I = 0x11

.field public static final DEVICEGONE:I = 0x3

.field public static final DEVICENOTACTIVATED:I = 0x4

.field public static final DMRGONE:I = 0x9

.field public static final DMRNOTSELECTED:I = 0xc

.field public static final DMSGONE:I = 0xa

.field public static final DMSNOTSELECTED:I = 0xb

.field public static final DMSRUNNING:I = 0xd

.field public static final ERROR_TYPES:[I

.field public static final INVALIDACTION:I = 0x191

.field public static final INVALIDINDEX:I = 0x1

.field public static final INVALIDPARAM:I = 0x2

.field public static final INVALIDUPNPDEVICE:I = 0x5

.field public static final INVALIDVAR:I = 0x194

.field public static final NOERROR:I = 0x0

.field public static final NORESPONSE:I = 0x6

.field public static final OUTOFMEMORY:I = 0x8

.field public static final OUTOFSYNC:I = 0x193

.field public static final PRECONDITIONFAILED:I = 0x19c

.field public static final RESNOTFOUND:I = 0xf

.field public static final UNKNOWN:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 163
    const/16 v0, 0x18

    new-array v0, v0, [I

    .line 165
    aput v1, v0, v1

    .line 166
    aput v2, v0, v2

    .line 167
    aput v3, v0, v3

    .line 168
    aput v4, v0, v4

    .line 169
    aput v5, v0, v5

    const/4 v1, 0x6

    .line 170
    const/4 v2, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 171
    const/4 v2, 0x7

    aput v2, v0, v1

    const/16 v1, 0x8

    .line 172
    const/16 v2, 0x8

    aput v2, v0, v1

    const/16 v1, 0x9

    .line 173
    const/16 v2, 0x9

    aput v2, v0, v1

    const/16 v1, 0xa

    .line 174
    const/16 v2, 0xa

    aput v2, v0, v1

    const/16 v1, 0xb

    .line 175
    const/16 v2, 0xb

    aput v2, v0, v1

    const/16 v1, 0xc

    .line 176
    const/16 v2, 0xc

    aput v2, v0, v1

    const/16 v1, 0xd

    .line 177
    const/16 v2, 0xd

    aput v2, v0, v1

    const/16 v1, 0xe

    .line 178
    const/16 v2, 0xe

    aput v2, v0, v1

    const/16 v1, 0xf

    .line 179
    const/16 v2, 0xf

    aput v2, v0, v1

    const/16 v1, 0x10

    .line 180
    const/16 v2, 0x11

    aput v2, v0, v1

    const/16 v1, 0x11

    .line 181
    const/16 v2, 0x191

    aput v2, v0, v1

    const/16 v1, 0x12

    .line 182
    const/16 v2, 0x1f5

    aput v2, v0, v1

    const/16 v1, 0x13

    .line 183
    const/4 v2, -0x1

    aput v2, v0, v1

    const/16 v1, 0x14

    .line 184
    const/4 v2, -0x2

    aput v2, v0, v1

    const/16 v1, 0x15

    .line 185
    const/16 v2, 0x193

    aput v2, v0, v1

    const/16 v1, 0x16

    .line 186
    const/16 v2, 0x194

    aput v2, v0, v1

    const/16 v1, 0x17

    .line 187
    const/16 v2, 0x19c

    aput v2, v0, v1

    .line 163
    sput-object v0, Lcom/samsung/api/DLNAErrorCode;->ERROR_TYPES:[I

    .line 188
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
