.class public Lcom/samsung/api/MediaProfileEnums;
.super Ljava/lang/Object;
.source "MediaProfileEnums.java"


# static fields
.field public static final AAC_ADTS:Ljava/lang/String; = "AAC_ADTS"

.field public static final AAC_ADTS_320:Ljava/lang/String; = "AAC_ADTS_320"

.field public static final AAC_ISO:Ljava/lang/String; = "AAC_ISO"

.field public static final AAC_ISO_320:Ljava/lang/String; = "AAC_ISO_320"

.field public static final AAC_LTP_ISO:Ljava/lang/String; = "AAC_LTP_ISO"

.field public static final AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AAC_LTP_MULT5_ISO"

.field public static final AAC_LTP_MULT7_ISO:Ljava/lang/String; = "AAC_LTP_MULT7_ISO"

.field public static final AAC_MULT5_ADTS:Ljava/lang/String; = "AAC_MULT5_ADTS"

.field public static final AAC_MULT5_ISO:Ljava/lang/String; = "AAC_MULT5_ISO"

.field public static final AC3:Ljava/lang/String; = "AC3"

.field public static final AMR_3GPP:Ljava/lang/String; = "AMR_3GPP"

.field public static final AMR_WBplus:Ljava/lang/String; = "AMR_WBplus"

.field public static final ATRAC3plus:Ljava/lang/String; = "ATRAC3plus"

.field public static final AVC_3GPP_BL_CIF15_AMR_WBplus:Ljava/lang/String; = "AVC_3GPP_BL_CIF15_AMR_WBplus"

.field public static final AVC_3GPP_BL_CIF30_AMR_WBplus:Ljava/lang/String; = "AVC_3GPP_BL_CIF30_AMR_WBplus"

.field public static final AVC_3GPP_BL_QCIF15_AAC:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AAC"

.field public static final AVC_3GPP_BL_QCIF15_AAC_LTP:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AAC_LTP"

.field public static final AVC_3GPP_BL_QCIF15_AMR:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AMR"

.field public static final AVC_3GPP_BL_QCIF15_AMR_WBplus:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AMR_WBplus"

.field public static final AVC_3GPP_BL_QCIF15_HEAAC:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_HEAAC"

.field public static final AVC_MP4_BL_CIF15_AAC:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC"

.field public static final AVC_MP4_BL_CIF15_AAC_520:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_520"

.field public static final AVC_MP4_BL_CIF15_AAC_LTP:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_LTP"

.field public static final AVC_MP4_BL_CIF15_AAC_LTP_520:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_LTP_520"

.field public static final AVC_MP4_BL_CIF15_AMR:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AMR"

.field public static final AVC_MP4_BL_CIF15_BSAC:Ljava/lang/String; = "AVC_MP4_BL_CIF15_BSAC"

.field public static final AVC_MP4_BL_CIF15_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_CIF15_HEAAC"

.field public static final AVC_MP4_BL_CIF30_AAC_LTP:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AAC_LTP"

.field public static final AVC_MP4_BL_CIF30_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AAC_LTP_MULT5"

.field public static final AVC_MP4_BL_CIF30_AAC_MULT5:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AAC_MULT5"

.field public static final AVC_MP4_BL_CIF30_AC3:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AC3"

.field public static final AVC_MP4_BL_CIF30_BSAC:Ljava/lang/String; = "AVC_MP4_BL_CIF30_BSAC"

.field public static final AVC_MP4_BL_CIF30_BSAC_MULT5:Ljava/lang/String; = "AVC_MP4_BL_CIF30_BSAC_MULT5"

.field public static final AVC_MP4_BL_CIF30_HEAAC_L2:Ljava/lang/String; = "AVC_MP4_BL_CIF30_HEAAC_L2"

.field public static final AVC_MP4_BL_CIF30_MPEG1_L3:Ljava/lang/String; = "AVC_MP4_BL_CIF30_MPEG1_L3"

.field public static final AVC_MP4_BL_L12_CIF15_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_L12_CIF15_HEAAC"

.field public static final AVC_MP4_BL_L1B_QCIF15_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_L1B_QCIF15_HEAAC"

.field public static final AVC_MP4_BL_L2_CIF30_AAC:Ljava/lang/String; = "AVC_MP4_BL_L2_CIF30_AAC"

.field public static final AVC_MP4_BL_L3L_SD_AAC:Ljava/lang/String; = "AVC_MP4_BL_L3L_SD_AAC"

.field public static final AVC_MP4_BL_L3L_SD_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_L3L_SD_HEAAC"

.field public static final AVC_MP4_BL_L3_SD_AAC:Ljava/lang/String; = "AVC_MP4_BL_L3_SD_AAC"

.field public static final AVC_MP4_MP_SD_AAC_LTP:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_LTP"

.field public static final AVC_MP4_MP_SD_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_LTP_MULT5"

.field public static final AVC_MP4_MP_SD_AAC_LTP_MULT7:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_LTP_MULT7"

.field public static final AVC_MP4_MP_SD_AAC_MULT5:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_MULT5"

.field public static final AVC_MP4_MP_SD_AC3:Ljava/lang/String; = "AVC_MP4_MP_SD_AC3"

.field public static final AVC_MP4_MP_SD_ATRAC3plus:Ljava/lang/String; = "AVC_MP4_MP_SD_ATRAC3plus"

.field public static final AVC_MP4_MP_SD_BSAC:Ljava/lang/String; = "AVC_MP4_MP_SD_BSAC"

.field public static final AVC_MP4_MP_SD_HEAAC_L2:Ljava/lang/String; = "AVC_MP4_MP_SD_HEAAC_L2"

.field public static final AVC_MP4_MP_SD_MPEG1_L3:Ljava/lang/String; = "AVC_MP4_MP_SD_MPEG1_L3"

.field public static final AVC_TS_BL_CIF15_AAC:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC"

.field public static final AVC_TS_BL_CIF15_AAC_540:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_540"

.field public static final AVC_TS_BL_CIF15_AAC_540_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_540_ISO"

.field public static final AVC_TS_BL_CIF15_AAC_540_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_540_T"

.field public static final AVC_TS_BL_CIF15_AAC_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_ISO"

.field public static final AVC_TS_BL_CIF15_AAC_LTP:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_LTP"

.field public static final AVC_TS_BL_CIF15_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_LTP_ISO"

.field public static final AVC_TS_BL_CIF15_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_LTP_T"

.field public static final AVC_TS_BL_CIF15_AAC_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_T"

.field public static final AVC_TS_BL_CIF15_BSAC:Ljava/lang/String; = "AVC_TS_BL_CIF15_BSAC"

.field public static final AVC_TS_BL_CIF15_BSAC_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_BSAC_ISO"

.field public static final AVC_TS_BL_CIF15_BSAC_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_BSAC_T"

.field public static final AVC_TS_BL_CIF30_AAC_940:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_940"

.field public static final AVC_TS_BL_CIF30_AAC_940_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_940_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_940_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_940_T"

.field public static final AVC_TS_BL_CIF30_AAC_LTP:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_MULT5"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_MULT5_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_MULT5_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_MULT5_T"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_T"

.field public static final AVC_TS_BL_CIF30_AAC_MULT5:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_MULT5"

.field public static final AVC_TS_BL_CIF30_AAC_MULT5_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_MULT5_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_MULT5_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_MULT5_T"

.field public static final AVC_TS_BL_CIF30_AC3:Ljava/lang/String; = "AVC_TS_BL_CIF30_AC3"

.field public static final AVC_TS_BL_CIF30_AC3_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AC3_ISO"

.field public static final AVC_TS_BL_CIF30_AC3_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AC3_T"

.field public static final AVC_TS_BL_CIF30_HEAAC_L2:Ljava/lang/String; = "AVC_TS_BL_CIF30_HEAAC_L2"

.field public static final AVC_TS_BL_CIF30_HEAAC_L2_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_HEAAC_L2_ISO"

.field public static final AVC_TS_BL_CIF30_HEAAC_L2_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_HEAAC_L2_T"

.field public static final AVC_TS_BL_CIF30_MPEG1_L3:Ljava/lang/String; = "AVC_TS_BL_CIF30_MPEG1_L3"

.field public static final AVC_TS_BL_CIF30_MPEG1_L3_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_MPEG1_L3_ISO"

.field public static final AVC_TS_BL_CIF30_MPEG1_L3_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_MPEG1_L3_T"

.field public static final AVC_TS_MP_HD_AAC:Ljava/lang/String; = "AVC_TS_MP_HD_AAC"

.field public static final AVC_TS_MP_HD_AAC_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP"

.field public static final AVC_TS_MP_HD_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT5"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT5_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT5_T"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT7:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT7"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT7_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT7_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT7_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT7_T"

.field public static final AVC_TS_MP_HD_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_T"

.field public static final AVC_TS_MP_HD_AAC_MULT5:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_MULT5"

.field public static final AVC_TS_MP_HD_AAC_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_MULT5_ISO"

.field public static final AVC_TS_MP_HD_AAC_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_MULT5_T"

.field public static final AVC_TS_MP_HD_AAC_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_T"

.field public static final AVC_TS_MP_HD_AC3:Ljava/lang/String; = "AVC_TS_MP_HD_AC3"

.field public static final AVC_TS_MP_HD_AC3_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AC3_ISO"

.field public static final AVC_TS_MP_HD_AC3_T:Ljava/lang/String; = "AVC_TS_MP_HD_AC3_T"

.field public static final AVC_TS_MP_HD_HEAAC_L2:Ljava/lang/String; = "AVC_TS_MP_HD_HEAAC_L2"

.field public static final AVC_TS_MP_HD_HEAAC_L2_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_HEAAC_L2_ISO"

.field public static final AVC_TS_MP_HD_HEAAC_L2_T:Ljava/lang/String; = "AVC_TS_MP_HD_HEAAC_L2_T"

.field public static final AVC_TS_MP_HD_MPEG1_L3:Ljava/lang/String; = "AVC_TS_MP_HD_MPEG1_L3"

.field public static final AVC_TS_MP_HD_MPEG1_L3_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_MPEG1_L3_ISO"

.field public static final AVC_TS_MP_HD_MPEG1_L3_T:Ljava/lang/String; = "AVC_TS_MP_HD_MPEG1_L3_T"

.field public static final AVC_TS_MP_SD_AAC_LTP:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP"

.field public static final AVC_TS_MP_SD_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_ISO"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT5"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT5_ISO"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT5_T"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT7:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT7"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT7_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT7_ISO"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT7_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT7_T"

.field public static final AVC_TS_MP_SD_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_T"

.field public static final AVC_TS_MP_SD_AAC_MULT5:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_MULT5"

.field public static final AVC_TS_MP_SD_AAC_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_MULT5_ISO"

.field public static final AVC_TS_MP_SD_AAC_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_MULT5_T"

.field public static final AVC_TS_MP_SD_AC3:Ljava/lang/String; = "AVC_TS_MP_SD_AC3"

.field public static final AVC_TS_MP_SD_AC3_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AC3_ISO"

.field public static final AVC_TS_MP_SD_AC3_T:Ljava/lang/String; = "AVC_TS_MP_SD_AC3_T"

.field public static final AVC_TS_MP_SD_BSAC:Ljava/lang/String; = "AVC_TS_MP_SD_BSAC"

.field public static final AVC_TS_MP_SD_BSAC_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_BSAC_ISO"

.field public static final AVC_TS_MP_SD_BSAC_T:Ljava/lang/String; = "AVC_TS_MP_SD_BSAC_T"

.field public static final AVC_TS_MP_SD_HEAAC_L2:Ljava/lang/String; = "AVC_TS_MP_SD_HEAAC_L2"

.field public static final AVC_TS_MP_SD_HEAAC_L2_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_HEAAC_L2_ISO"

.field public static final AVC_TS_MP_SD_HEAAC_L2_T:Ljava/lang/String; = "AVC_TS_MP_SD_HEAAC_L2_T"

.field public static final AVC_TS_MP_SD_MPEG1_L3:Ljava/lang/String; = "AVC_TS_MP_SD_MPEG1_L3"

.field public static final AVC_TS_MP_SD_MPEG1_L3_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_MPEG1_L3_ISO"

.field public static final AVC_TS_MP_SD_MPEG1_L3_T:Ljava/lang/String; = "AVC_TS_MP_SD_MPEG1_L3_T"

.field public static final BSAC_ISO:Ljava/lang/String; = "BSAC_ISO"

.field public static final BSAC_MULT5_ISO:Ljava/lang/String; = "BSAC_MULT5_ISO"

.field public static final HEAAC_L2_ADTS:Ljava/lang/String; = "HEAAC_L2_ADTS"

.field public static final HEAAC_L2_ADTS_320:Ljava/lang/String; = "HEAAC_L2_ADTS_320"

.field public static final HEAAC_L2_ISO:Ljava/lang/String; = "HEAAC_L2_ISO"

.field public static final HEAAC_L2_ISO_320:Ljava/lang/String; = "HEAAC_L2_ISO_320"

.field public static final HEAAC_L3_ADTS:Ljava/lang/String; = "HEAAC_L3_ADTS"

.field public static final HEAAC_L3_ISO:Ljava/lang/String; = "HEAAC_L3_ISO"

.field public static final HEAAC_MULT5_ADTS:Ljava/lang/String; = "HEAAC_MULT5_ADTS"

.field public static final HEAAC_MULT5_ISO:Ljava/lang/String; = "HEAAC_MULT5_ISO"

.field public static final JPEG_LRG:Ljava/lang/String; = "JPEG_LRG"

.field public static final JPEG_LRG_ICO:Ljava/lang/String; = "JPEG_LRG_ICO"

.field public static final JPEG_MED:Ljava/lang/String; = "JPEG_MED"

.field public static final JPEG_SM:Ljava/lang/String; = "JPEG_SM"

.field public static final JPEG_SM_ICO:Ljava/lang/String; = "JPEG_SM_ICO"

.field public static final JPEG_TN:Ljava/lang/String; = "JPEG_TN"

.field public static final LPCM:Ljava/lang/String; = "LPCM"

.field public static final MIME_AUDIO_3GPP:Ljava/lang/String; = "audio/3gpp"

.field public static final MIME_AUDIO_ADTS:Ljava/lang/String; = "audio/vnd.dlna.adts"

.field public static final MIME_AUDIO_L16:Ljava/lang/String; = "audio/L16"

.field public static final MIME_AUDIO_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIME_AUDIO_MPEG:Ljava/lang/String; = "audio/mpeg"

.field public static final MIME_AUDIO_RAW:Ljava/lang/String; = "audio/vnd.dolby.dd-raw"

.field public static final MIME_AUDIO_SONY_OMA:Ljava/lang/String; = "audio/x-sony-oma"

.field public static final MIME_AUDIO_WMA:Ljava/lang/String; = "audio/x-ms-wma"

.field public static final MIME_IMAGE_JPEG:Ljava/lang/String; = "image/jpeg"

.field public static final MIME_IMAGE_PNG:Ljava/lang/String; = "image/png"

.field public static final MIME_VIDEO_3GPP:Ljava/lang/String; = "video/3gpp"

.field public static final MIME_VIDEO_ASF:Ljava/lang/String; = "video/x-ms-asf"

.field public static final MIME_VIDEO_AVI:Ljava/lang/String; = "video/avi"

.field public static final MIME_VIDEO_MP4:Ljava/lang/String; = "video/mp4"

.field public static final MIME_VIDEO_MPEG:Ljava/lang/String; = "video/mpeg"

.field public static final MIME_VIDEO_TTS:Ljava/lang/String; = "video/vnd.dlna.mpeg-tts"

.field public static final MIME_VIDEO_WMV:Ljava/lang/String; = "video/x-ms-wmv"

.field public static final MP3:Ljava/lang/String; = "MP3"

.field public static final MP3X:Ljava/lang/String; = "MP3X"

.field public static final MPEG1:Ljava/lang/String; = "MPEG1"

.field public static final MPEG4_H263_3GPP_P0_L10_AMR_WBplus:Ljava/lang/String; = "MPEG4_H263_3GPP_P0_L10_AMR_WBplus"

.field public static final MPEG4_H263_3GPP_P3_L10_AMR:Ljava/lang/String; = "MPEG4_H263_3GPP_P3_L10_AMR"

.field public static final MPEG4_H263_MP4_P0_L10_AAC:Ljava/lang/String; = "MPEG4_H263_MP4_P0_L10_AAC"

.field public static final MPEG4_H263_MP4_P0_L10_AAC_LTP:Ljava/lang/String; = "MPEG4_H263_MP4_P0_L10_AAC_LTP"

.field public static final MPEG4_P2_3GPP_SP_L0B_AAC:Ljava/lang/String; = "MPEG4_P2_3GPP_SP_L0B_AAC"

.field public static final MPEG4_P2_3GPP_SP_L0B_AMR:Ljava/lang/String; = "MPEG4_P2_3GPP_SP_L0B_AMR"

.field public static final MPEG4_P2_ASF_ASP_L4_SO_G726:Ljava/lang/String; = "MPEG4_P2_ASF_ASP_L4_SO_G726"

.field public static final MPEG4_P2_ASF_ASP_L5_SO_G726:Ljava/lang/String; = "MPEG4_P2_ASF_ASP_L5_SO_G726"

.field public static final MPEG4_P2_ASF_SP_G726:Ljava/lang/String; = "MPEG4_P2_ASF_SP_G726"

.field public static final MPEG4_P2_MP4_ASP_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_AAC"

.field public static final MPEG4_P2_MP4_ASP_ATRAC3plus:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_ATRAC3plus"

.field public static final MPEG4_P2_MP4_ASP_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_HEAAC"

.field public static final MPEG4_P2_MP4_ASP_HEAAC_MULT5:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_HEAAC_MULT5"

.field public static final MPEG4_P2_MP4_ASP_L4_SO_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L4_SO_AAC"

.field public static final MPEG4_P2_MP4_ASP_L4_SO_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L4_SO_HEAAC"

.field public static final MPEG4_P2_MP4_ASP_L4_SO_HEAAC_MULT5:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L4_SO_HEAAC_MULT5"

.field public static final MPEG4_P2_MP4_ASP_L5_SO_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L5_SO_AAC"

.field public static final MPEG4_P2_MP4_ASP_L5_SO_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L5_SO_HEAAC"

.field public static final MPEG4_P2_MP4_ASP_L5_SO_HEAAC_MULT5:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L5_SO_HEAAC_MULT5"

.field public static final MPEG4_P2_MP4_SP_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_AAC"

.field public static final MPEG4_P2_MP4_SP_AAC_LTP:Ljava/lang/String; = "MPEG4_P2_MP4_SP_AAC_LTP"

.field public static final MPEG4_P2_MP4_SP_ATRAC3plus:Ljava/lang/String; = "MPEG4_P2_MP4_SP_ATRAC3plus"

.field public static final MPEG4_P2_MP4_SP_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_HEAAC"

.field public static final MPEG4_P2_MP4_SP_L2_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_L2_AAC"

.field public static final MPEG4_P2_MP4_SP_L2_AMR:Ljava/lang/String; = "MPEG4_P2_MP4_SP_L2_AMR"

.field public static final MPEG4_P2_MP4_SP_VGA_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_VGA_AAC"

.field public static final MPEG4_P2_MP4_SP_VGA_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_VGA_HEAAC"

.field public static final MPEG4_P2_TS_ASP_AAC:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AAC"

.field public static final MPEG4_P2_TS_ASP_AAC_ISO:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AAC_ISO"

.field public static final MPEG4_P2_TS_ASP_AAC_T:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AAC_T"

.field public static final MPEG4_P2_TS_ASP_AC3:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AC3"

.field public static final MPEG4_P2_TS_ASP_AC3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AC3_ISO"

.field public static final MPEG4_P2_TS_ASP_AC3_T:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AC3_T"

.field public static final MPEG4_P2_TS_ASP_MPEG1_L3:Ljava/lang/String; = "MPEG4_P2_TS_ASP_MPEG1_L3"

.field public static final MPEG4_P2_TS_ASP_MPEG1_L3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_ASP_MPEG1_L3_ISO"

.field public static final MPEG4_P2_TS_ASP_MPEG1_L3_T:Ljava/lang/String; = "MPEG4_P2_TS_ASP_MPEG1_L3_T"

.field public static final MPEG4_P2_TS_CO_AC3:Ljava/lang/String; = "MPEG4_P2_TS_CO_AC3"

.field public static final MPEG4_P2_TS_CO_AC3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_CO_AC3_ISO"

.field public static final MPEG4_P2_TS_CO_AC3_T:Ljava/lang/String; = "MPEG4_P2_TS_CO_AC3_T"

.field public static final MPEG4_P2_TS_CO_MPEG2_L2:Ljava/lang/String; = "MPEG4_P2_TS_CO_MPEG2_L2"

.field public static final MPEG4_P2_TS_CO_MPEG2_L2_ISO:Ljava/lang/String; = "MPEG4_P2_TS_CO_MPEG2_L2_ISO"

.field public static final MPEG4_P2_TS_CO_MPEG2_L2_T:Ljava/lang/String; = "MPEG4_P2_TS_CO_MPEG2_L2_T"

.field public static final MPEG4_P2_TS_SP_AAC:Ljava/lang/String; = "MPEG4_P2_TS_SP_AAC"

.field public static final MPEG4_P2_TS_SP_AAC_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_AAC_ISO"

.field public static final MPEG4_P2_TS_SP_AAC_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_AAC_T"

.field public static final MPEG4_P2_TS_SP_AC3:Ljava/lang/String; = "MPEG4_P2_TS_SP_AC3"

.field public static final MPEG4_P2_TS_SP_AC3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_AC3_ISO"

.field public static final MPEG4_P2_TS_SP_AC3_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_AC3_T"

.field public static final MPEG4_P2_TS_SP_MEPG1_L3:Ljava/lang/String; = "MPEG4_P2_TS_SP_MEPG1_L3"

.field public static final MPEG4_P2_TS_SP_MPEG1_L3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG1_L3_ISO"

.field public static final MPEG4_P2_TS_SP_MPEG1_L3_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG1_L3_T"

.field public static final MPEG4_P2_TS_SP_MPEG2_L2:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG2_L2"

.field public static final MPEG4_P2_TS_SP_MPEG2_L2_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG2_L2_ISO"

.field public static final MPEG4_P2_TS_SP_MPEG2_L2_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG2_L2_T"

.field public static final MPEG_ES_NTSC:Ljava/lang/String; = "MPEG_ES_NTSC"

.field public static final MPEG_ES_NTSC_XAC3:Ljava/lang/String; = "MPEG_ES_NTSC_XAC3"

.field public static final MPEG_ES_PAL:Ljava/lang/String; = "MPEG_ES_PAL"

.field public static final MPEG_ES_PAL_XAC3:Ljava/lang/String; = "MPEG_ES_PAL_XAC3"

.field public static final MPEG_PS_NTSC:Ljava/lang/String; = "MPEG_PS_NTSC"

.field public static final MPEG_PS_NTSC_X_AC3:Ljava/lang/String; = "MPEG_PS_NTSC_X_AC3"

.field public static final MPEG_PS_PAL:Ljava/lang/String; = "MPEG_PS_PAL"

.field public static final MPEG_PS_PAL_XAC3:Ljava/lang/String; = "MPEG_PS_PAL_XAC3"

.field public static final MPEG_TS_HD_KO:Ljava/lang/String; = "MPEG_TS_HD_KO"

.field public static final MPEG_TS_HD_KO_ISO:Ljava/lang/String; = "MPEG_TS_HD_KO_ISO"

.field public static final MPEG_TS_HD_KO_T:Ljava/lang/String; = "MPEG_TS_HD_KO_T"

.field public static final MPEG_TS_HD_KO_XAC3:Ljava/lang/String; = "MPEG_TS_HD_KO_XAC3"

.field public static final MPEG_TS_HD_KO_XAC3_ISO:Ljava/lang/String; = "MPEG_TS_HD_KO_XAC3_ISO"

.field public static final MPEG_TS_HD_KO_XAC3_T:Ljava/lang/String; = "MPEG_TS_HD_KO_XAC3_T"

.field public static final MPEG_TS_HD_NA:Ljava/lang/String; = "MPEG_TS_HD_NA"

.field public static final MPEG_TS_HD_NA_ISO:Ljava/lang/String; = "MPEG_TS_HD_NA_ISO"

.field public static final MPEG_TS_HD_NA_T:Ljava/lang/String; = "MPEG_TS_HD_NA_T"

.field public static final MPEG_TS_HD_NA_XAC3:Ljava/lang/String; = "MPEG_TS_HD_NA_XAC3"

.field public static final MPEG_TS_HD_NA_XAC3_ISO:Ljava/lang/String; = "MPEG_TS_HD_NA_XAC3_ISO"

.field public static final MPEG_TS_HD_NA_XAC3_T:Ljava/lang/String; = "MPEG_TS_HD_NA_XAC3_T"

.field public static final MPEG_TS_MP_LL_AAC:Ljava/lang/String; = "MPEG_TS_MP_LL_AAC"

.field public static final MPEG_TS_MP_LL_AAC_ISO:Ljava/lang/String; = "MPEG_TS_MP_LL_AAC_ISO"

.field public static final MPEG_TS_MP_LL_AAC_T:Ljava/lang/String; = "MPEG_TS_MP_LL_AAC_T"

.field public static final MPEG_TS_SD_EU:Ljava/lang/String; = "MPEG_TS_SD_EU"

.field public static final MPEG_TS_SD_EU_ISO:Ljava/lang/String; = "MPEG_TS_SD_EU_ISO"

.field public static final MPEG_TS_SD_EU_T:Ljava/lang/String; = "MPEG_TS_SD_EU_T"

.field public static final MPEG_TS_SD_KO:Ljava/lang/String; = "MPEG_TS_SD_KO"

.field public static final MPEG_TS_SD_KO_ISO:Ljava/lang/String; = "MPEG_TS_SD_KO_ISO"

.field public static final MPEG_TS_SD_KO_T:Ljava/lang/String; = "MPEG_TS_SD_KO_T"

.field public static final MPEG_TS_SD_KO_XAC3_T:Ljava/lang/String; = "MPEG_TS_SD_KO_XAC3_T"

.field public static final MPEG_TS_SD_KO_X_AC3:Ljava/lang/String; = "MPEG_TS_SD_KO_X_AC3"

.field public static final MPEG_TS_SD_KO_X_AC3_ISO:Ljava/lang/String; = "MPEG_TS_SD_KO_X_AC3_ISO"

.field public static final MPEG_TS_SD_NA:Ljava/lang/String; = "MPEG_TS_SD_NA"

.field public static final MPEG_TS_SD_NA_ISO:Ljava/lang/String; = "MPEG_TS_SD_NA_ISO"

.field public static final MPEG_TS_SD_NA_T:Ljava/lang/String; = "MPEG_TS_SD_NA_T"

.field public static final MPEG_TS_SD_NA_XAC3:Ljava/lang/String; = "MPEG_TS_SD_NA_XAC3"

.field public static final MPEG_TS_SD_NA_XAC3_ISO:Ljava/lang/String; = "MPEG_TS_SD_NA_XAC3_ISO"

.field public static final MPEG_TS_SD_NA_XAC3_T:Ljava/lang/String; = "MPEG_TS_SD_NA_XAC3_T"

.field public static final PNG_LRG:Ljava/lang/String; = "PNG_LRG"

.field public static final PNG_LRG_ICO:Ljava/lang/String; = "PNG_LRG_ICO"

.field public static final PNG_SM_ICO:Ljava/lang/String; = "PNG_SM_ICO"

.field public static final PNG_TN:Ljava/lang/String; = "PNG_TN"

.field public static final POSTFIX_3GP:Ljava/lang/String; = ".3gp"

.field public static final POSTFIX_3GPP:Ljava/lang/String; = ".3gpp"

.field public static final POSTFIX_ADTS:Ljava/lang/String; = ".adts"

.field public static final POSTFIX_ASF:Ljava/lang/String; = ".asf"

.field public static final POSTFIX_AVI:Ljava/lang/String; = ".avi"

.field public static final POSTFIX_JPEG:Ljava/lang/String; = ".jpeg"

.field public static final POSTFIX_LPCM:Ljava/lang/String; = ".lpcm"

.field public static final POSTFIX_MP3:Ljava/lang/String; = ".mp3"

.field public static final POSTFIX_MP4:Ljava/lang/String; = ".mp4"

.field public static final POSTFIX_MPEG:Ljava/lang/String; = ".mpeg"

.field public static final POSTFIX_PNG:Ljava/lang/String; = ".png"

.field public static final POSTFIX_RAW:Ljava/lang/String; = ".raw"

.field public static final POSTFIX_WMA:Ljava/lang/String; = ".wma"

.field public static final POSTFIX_WMV:Ljava/lang/String; = ".wmv"

.field public static final WMABASE:Ljava/lang/String; = "WMABASE"

.field public static final WMAFULL:Ljava/lang/String; = "WMAFULL"

.field public static final WMAPRO:Ljava/lang/String; = "WMAPRO"

.field public static final WMVHIGH_FULL:Ljava/lang/String; = "WMVHIGH_FULL"

.field public static final WMVHIGH_PRO:Ljava/lang/String; = "WMVHIGH_PRO"

.field public static final WMVHM_BASE:Ljava/lang/String; = "WMVHM_BASE"

.field public static final WMVMED_BASE:Ljava/lang/String; = "WMVMED_BASE"

.field public static final WMVMED_FULL:Ljava/lang/String; = "WMVMED_FULL"

.field public static final WMVMED_PRO:Ljava/lang/String; = "WMVMED_PRO"

.field public static final WMVSPLL_BASE:Ljava/lang/String; = "WMVSPLL_BASE"

.field public static final WMVSPML_BASE:Ljava/lang/String; = "WMVSPML_BASE"

.field public static final WMVSPML_MP3:Ljava/lang/String; = "WMVSPML_MP3"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
