.class public Lcom/samsung/api/DigitalMediaAddinControllerAPI;
.super Ljava/lang/Object;
.source "DigitalMediaAddinControllerAPI.java"


# static fields
.field private static volatile instance:Lcom/samsung/api/DigitalMediaAddinControllerAPI;

.field private static mMutex:Ljava/lang/Object;


# instance fields
.field private mCp:Lcom/samsung/upnp/ControlPoint;

.field private mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

.field private positionInfo:Lcom/samsung/api/ContentPositionInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->instance:Lcom/samsung/api/DigitalMediaAddinControllerAPI;

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mMutex:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 86
    new-instance v0, Lcom/samsung/upnp/ControlPoint;

    invoke-direct {v0}, Lcom/samsung/upnp/ControlPoint;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    .line 87
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    const-string v1, "upnp:rootdevice"

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->addSearchTarget(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    const-string v1, "urn:schemas-upnp-org:device:MediaRenderer:1"

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->addSearchTarget(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public static getInstance()Lcom/samsung/api/DigitalMediaAddinControllerAPI;
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->instance:Lcom/samsung/api/DigitalMediaAddinControllerAPI;

    if-nez v0, :cond_1

    .line 76
    sget-object v1, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    sget-object v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->instance:Lcom/samsung/api/DigitalMediaAddinControllerAPI;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;

    invoke-direct {v0}, Lcom/samsung/api/DigitalMediaAddinControllerAPI;-><init>()V

    sput-object v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->instance:Lcom/samsung/api/DigitalMediaAddinControllerAPI;

    .line 76
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_1
    sget-object v0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->instance:Lcom/samsung/api/DigitalMediaAddinControllerAPI;

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private makeRequestedList(IILjava/util/ArrayList;)Ljava/util/List;
    .locals 2
    .param p1, "offset"    # I
    .param p2, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 206
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/DeviceItem;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 207
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 208
    :cond_0
    add-int v0, p1, p2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 209
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p3, p1, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    :cond_1
    add-int v0, p1, p2

    invoke-virtual {p3, p1, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static final setDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-static {p0}, Lcom/samsung/api/HTTP_API;->setDeviceName(Ljava/lang/String;)Z

    .line 58
    return-void
.end method

.method public static final setInterface(Ljava/lang/String;)V
    .locals 0
    .param p0, "ifaddr"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-static {p0}, Lcom/samsung/net/HostInterface;->setInterface(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method private unsubscribeDevice(Ljava/lang/String;)V
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 215
    if-nez p1, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v1, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 218
    .local v0, "device":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    .line 219
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ControlPoint;->unsubscribe(Lcom/samsung/upnp/Device;)V

    goto :goto_0
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z

    move-result v0

    return v0
.end method

.method public addEventListener(Lcom/samsung/api/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/EventListener;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->addEventListener(Lcom/samsung/api/EventListener;)Z

    move-result v0

    return v0
.end method

.method convertStringToDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 1056
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "hh:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1059
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1063
    :goto_0
    return-object v0

    .line 1061
    :catch_0
    move-exception v1

    .line 1063
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDMRList(II)Ljava/util/List;
    .locals 7
    .param p1, "offset"    # I
    .param p2, "requestSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/DeviceItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 170
    iget-object v4, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4}, Lcom/samsung/upnp/ControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v1

    .line 171
    .local v1, "devList":Lcom/samsung/upnp/DeviceList;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/DeviceItem;>;"
    if-ltz p1, :cond_0

    if-gtz p2, :cond_1

    .line 174
    :cond_0
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 178
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 186
    invoke-direct {p0, p1, p2, v3}, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->makeRequestedList(IILjava/util/ArrayList;)Ljava/util/List;

    move-result-object v4

    return-object v4

    .line 178
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/Device;

    .line 179
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MediaRenderer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 180
    new-instance v2, Lcom/samsung/api/DeviceItem;

    invoke-direct {v2, v0}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    .line 181
    .local v2, "item":Lcom/samsung/api/DeviceItem;
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Lcom/samsung/api/DeviceItem;->setDeviceType(I)V

    .line 182
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/api/DeviceItem;
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 197
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v1, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 198
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_0

    .line 199
    const/4 v1, 0x0

    .line 201
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/api/DeviceItem;

    invoke-direct {v1, v0}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    goto :goto_0
.end method

.method public getMute(Lcom/samsung/api/DeviceItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 691
    if-nez p1, :cond_0

    .line 692
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0xc

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 694
    :cond_0
    iget-object v3, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 695
    .local v2, "dmr":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 696
    const-string v3, "DLNA : DMC Service (getMute) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 697
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 700
    :cond_1
    const-string v3, "GetMute"

    invoke-virtual {v2, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 701
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 702
    const-string v3, "DLNA : DMC Service (getMute) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 703
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 706
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    .line 707
    :cond_3
    const-string p2, "Master"

    .line 709
    :cond_4
    const-string v3, "Channel"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 710
    .local v1, "argChannel":Lcom/samsung/upnp/Argument;
    if-eqz v1, :cond_5

    invoke-virtual {v1, p2}, Lcom/samsung/upnp/Argument;->isAllowedValue(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 711
    :cond_5
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 712
    :cond_6
    invoke-virtual {v1, p2}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 713
    const-string v3, "InstanceID"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 715
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v3

    if-nez v3, :cond_8

    .line 716
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DLNA : DMC Service"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "action fail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 717
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v3

    const/16 v4, 0x19a

    if-ne v3, v4, :cond_7

    .line 718
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 720
    :cond_7
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x1f5

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 723
    :cond_8
    const-string v3, "CurrentMute"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getPosition(Lcom/samsung/api/DeviceItem;)Lcom/samsung/api/ContentPositionInfo;
    .locals 12
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x9

    .line 739
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    if-nez v8, :cond_0

    .line 740
    new-instance v8, Lcom/samsung/api/ContentPositionInfo;

    invoke-direct {v8}, Lcom/samsung/api/ContentPositionInfo;-><init>()V

    iput-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    .line 741
    :cond_0
    if-nez p1, :cond_1

    .line 742
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0xc

    invoke-direct {v8, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 743
    :cond_1
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v8, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v4

    .line 745
    .local v4, "dmr":Lcom/samsung/upnp/Device;
    if-nez v4, :cond_2

    .line 746
    const-string v8, "DLNA : DMC Service (getPosition) Fail cuz there is no DMR"

    invoke-static {v8}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 747
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v8, v11}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 749
    :cond_2
    const-string v8, "GetPositionInfo"

    invoke-virtual {v4, v8}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v2

    .line 750
    .local v2, "action":Lcom/samsung/upnp/Action;
    if-nez v2, :cond_3

    .line 751
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v8, v11}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 753
    :cond_3
    const-string v8, "InstanceID"

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 756
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v8

    if-nez v8, :cond_5

    .line 757
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "DLNA : DMC Service"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "action fail"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 758
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v8

    const/16 v9, 0x19a

    if-ne v8, v9, :cond_4

    .line 759
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    .line 760
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v9

    .line 761
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v10

    .line 759
    invoke-direct {v8, v11, v9, v10}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v8

    .line 763
    :cond_4
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0x1f5

    .line 764
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v10

    .line 765
    invoke-virtual {v2}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v11

    .line 763
    invoke-direct {v8, v9, v10, v11}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v8

    .line 768
    :cond_5
    const-string v7, ""

    .line 769
    .local v7, "trackDuration":Ljava/lang/String;
    const-string v6, ""

    .line 770
    .local v6, "relTime":Ljava/lang/String;
    const-string v1, ""

    .line 771
    .local v1, "absTime":Ljava/lang/String;
    const-string v5, ""

    .line 772
    .local v5, "relCount":Ljava/lang/String;
    const-string v0, ""

    .line 774
    .local v0, "absCount":Ljava/lang/String;
    const-string v8, "TrackDuration"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 775
    .local v3, "arg":Lcom/samsung/upnp/Argument;
    if-eqz v3, :cond_6

    .line 776
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 777
    :cond_6
    const-string v8, "RelTime"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 778
    if-eqz v3, :cond_7

    .line 779
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 780
    :cond_7
    const-string v8, "AbsTime"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 781
    if-eqz v3, :cond_8

    .line 782
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 783
    :cond_8
    const-string v8, "RelCount"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 784
    if-eqz v3, :cond_9

    .line 785
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 786
    :cond_9
    const-string v8, "AbsCount"

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v3

    .line 787
    if-eqz v3, :cond_a

    .line 788
    invoke-virtual {v3}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 790
    :cond_a
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v7}, Lcom/samsung/api/ContentPositionInfo;->setTrackDuration(Ljava/lang/String;)V

    .line 791
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v6}, Lcom/samsung/api/ContentPositionInfo;->setRelTime(Ljava/lang/String;)V

    .line 792
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v1}, Lcom/samsung/api/ContentPositionInfo;->setAbsTime(Ljava/lang/String;)V

    .line 793
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v5}, Lcom/samsung/api/ContentPositionInfo;->setRelCount(Ljava/lang/String;)V

    .line 794
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    invoke-virtual {v8, v0}, Lcom/samsung/api/ContentPositionInfo;->setAbsCount(Ljava/lang/String;)V

    .line 796
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->positionInfo:Lcom/samsung/api/ContentPositionInfo;

    return-object v8
.end method

.method public getProtocolInfo(Lcom/samsung/api/DeviceItem;)Ljava/util/List;
    .locals 12
    .param p1, "device"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/api/DeviceItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/api/ProtocolInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x9

    .line 913
    if-nez p1, :cond_0

    .line 914
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0xc

    invoke-direct {v8, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 916
    :cond_0
    iget-object v8, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v8, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 918
    .local v1, "dev":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 919
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v8, v11}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 922
    :cond_1
    const-string v8, "GetProtocolInfo"

    invoke-virtual {v1, v8}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 924
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 925
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0x191

    invoke-direct {v8, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v8

    .line 928
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v8

    if-nez v8, :cond_4

    .line 929
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v8

    const/16 v9, 0x19a

    if-ne v8, v9, :cond_3

    .line 930
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    .line 931
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v9

    .line 932
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v10

    .line 930
    invoke-direct {v8, v11, v9, v10}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v8

    .line 934
    :cond_3
    new-instance v8, Lcom/samsung/api/DMCAPIException;

    const/16 v9, 0x1f5

    .line 935
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v10

    .line 936
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v11

    .line 934
    invoke-direct {v8, v9, v10, v11}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v8

    .line 940
    :cond_4
    const-string v8, "Sink"

    invoke-virtual {v0, v8}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 941
    .local v5, "sink":Ljava/lang/String;
    const-string v8, "Source"

    invoke-virtual {v0, v8}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 943
    .local v6, "source":Ljava/lang/String;
    new-instance v7, Ljava/util/StringTokenizer;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, ","

    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 945
    .local v7, "st":Ljava/util/StringTokenizer;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 946
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/ProtocolInfo;>;"
    :goto_0
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-nez v8, :cond_5

    .line 952
    return-object v4

    .line 947
    :cond_5
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 948
    .local v3, "protocol":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/api/ProtocolInfo;->ParseProtocolInfo(Ljava/lang/String;)Lcom/samsung/api/ProtocolInfo;

    move-result-object v2

    .line 949
    .local v2, "pinfo":Lcom/samsung/api/ProtocolInfo;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getSystemUpdateID(Lcom/samsung/api/DeviceItem;)I
    .locals 10
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x1f5

    const/16 v8, 0xa

    .line 963
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 965
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/ContentItem;>;"
    if-nez p1, :cond_0

    .line 966
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    const/16 v7, 0xb

    invoke-direct {v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 968
    :cond_0
    iget-object v6, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v6, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 969
    .local v2, "device":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 970
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 972
    :cond_1
    const-string v6, "GetSystemUpdateID"

    invoke-virtual {v2, v6}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 974
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 975
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    const/16 v7, 0x191

    invoke-direct {v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 977
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v6

    if-nez v6, :cond_4

    .line 978
    const-string v6, "DLNA : DMC Service (GetSystemUpdateID) action fail"

    invoke-static {v6}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 979
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v6

    const/16 v7, 0x19a

    if-ne v6, v7, :cond_3

    .line 980
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 982
    :cond_3
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6

    .line 986
    :cond_4
    const-string v6, "Result"

    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 988
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    const/4 v5, 0x0

    .line 990
    .local v5, "result":I
    :try_start_0
    const-string v6, "Id"

    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 994
    return v5

    .line 991
    :catch_0
    move-exception v3

    .line 992
    .local v3, "e":Ljava/lang/Exception;
    new-instance v6, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v6, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v6
.end method

.method public getTotalMatchesInPlaylist(Lcom/samsung/api/DeviceItem;Ljava/lang/String;IILjava/lang/String;)I
    .locals 8
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "searchcriteriaString"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "sortCriteria"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x1f5

    const/16 v6, 0xa

    .line 1010
    if-nez p1, :cond_0

    .line 1011
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1013
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 1014
    :cond_1
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1016
    :cond_2
    if-ltz p3, :cond_3

    if-gez p4, :cond_4

    .line 1017
    :cond_3
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1019
    :cond_4
    if-nez p5, :cond_5

    .line 1020
    const-string p5, ""

    .line 1022
    :cond_5
    iget-object v4, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1023
    .local v1, "device":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_6

    .line 1024
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1026
    :cond_6
    const-string v4, "Search"

    invoke-virtual {v1, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1027
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_7

    .line 1028
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x191

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1030
    :cond_7
    const-string v4, "ContainerID"

    const-string v5, "0"

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    const-string v4, "SearchCriteria"

    invoke-virtual {v0, v4, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const-string v4, "Filter"

    const-string v5, "*"

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    const-string v4, "StartingIndex"

    invoke-virtual {v0, v4, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1034
    const-string v4, "RequestedCount"

    invoke-virtual {v0, v4, p4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 1035
    const-string v4, "SortCriteria"

    invoke-virtual {v0, v4, p5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_9

    .line 1038
    const-string v4, "DLNA : DMC Service (SearchItem) action fail"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1039
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_8

    .line 1040
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1042
    :cond_8
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 1046
    :cond_9
    const/4 v3, 0x0

    .line 1048
    .local v3, "result":I
    :try_start_0
    const-string v4, "TotalMatches"

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1052
    return v3

    .line 1049
    :catch_0
    move-exception v2

    .line 1050
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4
.end method

.method public getTransportInfo(Lcom/samsung/api/DeviceItem;)Ljava/lang/String;
    .locals 8
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x9

    .line 810
    if-nez p1, :cond_0

    .line 811
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 812
    :cond_0
    iget-object v4, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 813
    .local v2, "dmr":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 814
    const-string v4, "DLNA : DMC Service (getTransportInfo) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 815
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 817
    :cond_1
    const-string v4, "GetTransportInfo"

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 818
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 819
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 820
    :cond_2
    const-string v4, "InstanceID"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 822
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_4

    .line 823
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "action fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 824
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_3

    .line 825
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    .line 826
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    .line 827
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    .line 825
    invoke-direct {v4, v7, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 829
    :cond_3
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x1f5

    .line 830
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 831
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 829
    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 834
    :cond_4
    const-string v4, "CurrentTransportState"

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 835
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    if-nez v1, :cond_5

    .line 836
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x7

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 837
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 838
    .local v3, "result":Ljava/lang/String;
    return-object v3
.end method

.method public getVolume(Lcom/samsung/api/DeviceItem;ILjava/lang/String;)I
    .locals 10
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x1f5

    const/16 v8, 0x9

    .line 582
    if-nez p1, :cond_0

    .line 583
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 584
    :cond_0
    iget-object v5, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v5, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 586
    .local v2, "dmr":Lcom/samsung/upnp/Device;
    if-nez v2, :cond_1

    .line 587
    const-string v5, "DLNA : DMC Service (getVolume) Fail cuz there is no DMR"

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 588
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 590
    :cond_1
    const-string v5, "GetVolume"

    invoke-virtual {v2, v5}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 591
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 592
    const-string v5, "DLNA : DMC Service (GetVolume) Fail cuz there is no DMR"

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 593
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v8}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 595
    :cond_2
    const-string v5, "InstanceID"

    invoke-virtual {v0, v5, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 596
    const-string v5, "Channel"

    invoke-virtual {v0, v5, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v5, "Channel"

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 598
    .local v1, "ch":Lcom/samsung/upnp/Argument;
    if-eqz v1, :cond_3

    invoke-virtual {v1, p3}, Lcom/samsung/upnp/Argument;->isAllowedValue(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 599
    :cond_3
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5

    .line 600
    :cond_4
    invoke-virtual {v1, p3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 602
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v5

    if-nez v5, :cond_6

    .line 603
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DLNA : DMC Service"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "action fail"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 604
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v5

    const/16 v6, 0x19a

    if-ne v5, v6, :cond_5

    .line 605
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    .line 606
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 607
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 605
    invoke-direct {v5, v8, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v5

    .line 609
    :cond_5
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    .line 610
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v6

    .line 611
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 609
    invoke-direct {v5, v9, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v5

    .line 615
    :cond_6
    const/4 v4, 0x0

    .line 617
    .local v4, "result":I
    :try_start_0
    const-string v5, "CurrentVolume"

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 621
    return v4

    .line 618
    :catch_0
    move-exception v3

    .line 619
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v5, v9}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v5
.end method

.method public pauseItem(Lcom/samsung/api/DeviceItem;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 406
    if-nez p1, :cond_0

    .line 407
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 408
    :cond_0
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 410
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 411
    const-string v2, "DLNA : DMC Service (Pause) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 412
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 415
    :cond_1
    const-string v2, "Pause"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 416
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 417
    const-string v2, "DLNA : DMC Service (Pause) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 418
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 420
    :cond_2
    const-string v2, "InstanceID"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 422
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 423
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 424
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 425
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 426
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 427
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 425
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 429
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 430
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 431
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 429
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 434
    :cond_4
    return-void
.end method

.method public play(Lcom/samsung/api/DeviceItem;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 324
    if-nez p1, :cond_0

    .line 325
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 326
    :cond_0
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 327
    .local v0, "dmr":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_1

    .line 328
    const-string v2, "DLNA : DMC Service (Play) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 329
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 332
    :cond_1
    const-string v2, "Play"

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v1

    .line 333
    .local v1, "play":Lcom/samsung/upnp/Action;
    if-nez v1, :cond_2

    .line 334
    const-string v2, "DLNA : DMC Service (Play) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 335
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 337
    :cond_2
    const-string v2, "InstanceID"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 338
    const-string v2, "Speed"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 341
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 343
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 344
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 345
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 346
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 344
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 348
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 349
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 350
    invoke-virtual {v1}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 348
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 353
    :cond_4
    return-void
.end method

.method public refreshDeviceList()V
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/upnp/ControlPoint;->search()V

    .line 846
    return-void
.end method

.method public removeDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 853
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDeviceItem(Ljava/lang/String;)V

    .line 854
    return-void
.end method

.method public removeDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z

    move-result v0

    return v0
.end method

.method public removeEventListener(Lcom/samsung/api/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/EventListener;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/ControlPoint;->removeEventListener(Lcom/samsung/api/EventListener;)Z

    move-result v0

    return v0
.end method

.method public seek(Lcom/samsung/api/DeviceItem;ILjava/lang/String;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "target"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 455
    if-nez p1, :cond_0

    .line 456
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 457
    :cond_0
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 459
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 460
    const-string v2, "DLNA : DMC Service (Seek) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 461
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 464
    :cond_1
    invoke-virtual {p0, p3}, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->convertStringToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_2

    .line 465
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 467
    :cond_2
    const-string v2, "Seek"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 468
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_3

    .line 469
    const-string v2, "DLNA : DMC Service (Seek) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 470
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 473
    :cond_3
    const-string v2, "InstanceID"

    invoke-virtual {v0, v2, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 474
    const-string v2, "Unit"

    const-string v3, "REL_TIME"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v2, "Target"

    invoke-virtual {v0, v2, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_5

    .line 478
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 479
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_4

    .line 480
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 481
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 482
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 480
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 484
    :cond_4
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 485
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 486
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 484
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 489
    :cond_5
    return-void
.end method

.method public sendXMessage(Lcom/samsung/api/DeviceItem;Ljava/lang/String;)Z
    .locals 6
    .param p1, "server"    # Lcom/samsung/api/DeviceItem;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xa

    .line 1075
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1077
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/api/ContentItem;>;"
    if-nez p1, :cond_0

    .line 1078
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0xb

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1080
    :cond_0
    iget-object v3, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1081
    .local v1, "device":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 1082
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1084
    :cond_1
    const-string v3, "X_SendMessage"

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1085
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 1086
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x191

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1087
    :cond_2
    const-string v3, "Message"

    invoke-virtual {v0, v3, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1090
    const-string v3, "DLNA : DMC Service (GetSystemUpdateID) action fail"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 1091
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v3

    const/16 v4, 0x19a

    if-ne v3, v4, :cond_3

    .line 1092
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1094
    :cond_3
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x1f5

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 1097
    :cond_4
    const/4 v3, 0x1

    return v3
.end method

.method public setAvtTransport(Lcom/samsung/api/DeviceItem;Lcom/samsung/api/ContentItem;)V
    .locals 8
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "item"    # Lcom/samsung/api/ContentItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v7, 0x9

    .line 250
    if-nez p2, :cond_0

    .line 251
    const-string v4, "DLNA : DMC Service (SetAVTransportURI) Fail cuz there is no selected contents"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 252
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x11

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 255
    :cond_0
    if-nez p1, :cond_1

    .line 256
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 257
    :cond_1
    iget-object v4, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 259
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_2

    .line 260
    const-string v4, "DLNA : DMC Service (setAVTransportURI) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 261
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 264
    :cond_2
    const-string v4, "SetAVTransportURI"

    invoke-virtual {v1, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 265
    .local v0, "actionToDmr":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_3

    .line 266
    const-string v4, "DLNA : DMC Service (SetAVTransportURI) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 267
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v7}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 270
    :cond_3
    const-string v4, "InstanceID"

    invoke-virtual {v0, v4, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 271
    const-string v5, "CurrentURI"

    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->getResourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/api/ContentResource;

    invoke-virtual {v4}, Lcom/samsung/api/ContentResource;->getResourceUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v4, "CurrentURIMetaData"

    invoke-virtual {p2}, Lcom/samsung/api/ContentItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v3, 0x0

    .line 276
    .local v3, "tryCount":I
    :goto_0
    const/4 v4, 0x3

    if-lt v3, v4, :cond_5

    .line 304
    :cond_4
    return-void

    .line 277
    :cond_5
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_4

    .line 278
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "action fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 280
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_6

    .line 281
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    invoke-direct {v4, v7, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4

    .line 283
    :cond_6
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x2c1

    if-ne v4, v5, :cond_7

    .line 285
    add-int/lit8 v3, v3, 0x1

    .line 286
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service Retry setAVTranport Action!!!"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 288
    mul-int/lit16 v4, v3, 0xc8

    int-to-long v4, v4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 289
    :catch_0
    move-exception v2

    .line 290
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    .line 294
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_7
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x1f5

    .line 295
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 296
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 295
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 297
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v7

    .line 294
    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v4
.end method

.method public setMute(Lcom/samsung/api/DeviceItem;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "desiredMute"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x9

    .line 636
    if-nez p1, :cond_0

    .line 637
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0xc

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 639
    :cond_0
    iget-object v4, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v3

    .line 640
    .local v3, "dmr":Lcom/samsung/upnp/Device;
    if-nez v3, :cond_1

    .line 641
    const-string v4, "DLNA : DMC Service (getMute) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 642
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 645
    :cond_1
    const-string v4, "SetMute"

    invoke-virtual {v3, v4}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 646
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 647
    const-string v4, "DLNA : DMC Service (SetMute) Fail cuz there is no DMR"

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 648
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x191

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 651
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    .line 652
    :cond_3
    const-string p2, "Master"

    .line 654
    :cond_4
    const-string v4, "InstanceID"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 656
    const-string v4, "Channel"

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 657
    .local v1, "argChannel":Lcom/samsung/upnp/Argument;
    if-eqz v1, :cond_5

    invoke-virtual {v1, p2}, Lcom/samsung/upnp/Argument;->isAllowedValue(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 658
    :cond_5
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 659
    :cond_6
    invoke-virtual {v1, p2}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 661
    const-string v2, ""

    .line 662
    .local v2, "desiredMuteStr":Ljava/lang/String;
    if-eqz p3, :cond_7

    .line 663
    const-string v2, "true"

    .line 668
    :goto_0
    const-string v4, "DesiredMute"

    invoke-virtual {v0, v4, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v4

    if-nez v4, :cond_9

    .line 672
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DLNA : DMC Service"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "action fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 673
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_8

    .line 674
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v4, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 665
    :cond_7
    const-string v2, "false"

    goto :goto_0

    .line 676
    :cond_8
    new-instance v4, Lcom/samsung/api/DMCAPIException;

    const/16 v5, 0x1f5

    invoke-direct {v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v4

    .line 679
    :cond_9
    return-void
.end method

.method public setVolume(Lcom/samsung/api/DeviceItem;ILjava/lang/String;I)V
    .locals 7
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .param p2, "instanceID"    # I
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "vol"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x9

    const/4 v4, 0x2

    .line 517
    if-nez p1, :cond_0

    .line 518
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0xc

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 519
    :cond_0
    iget-object v3, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 521
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 522
    const-string v3, "DLNA : DMC Service (SetVolume) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 523
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v6}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 526
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 527
    :cond_2
    const-string p3, "Master"

    .line 529
    :cond_3
    const-string v3, "SetVolume"

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 530
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_4

    .line 531
    const-string v3, "DLNA : DMC Service (SetVolume) Fail cuz there is no DMR"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 532
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x191

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 535
    :cond_4
    const-string v3, "Channel"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 536
    .local v2, "temp":Lcom/samsung/upnp/Argument;
    if-eqz v2, :cond_5

    invoke-virtual {v2, p3}, Lcom/samsung/upnp/Argument;->isAllowedValue(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 537
    :cond_5
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 539
    :cond_6
    const-string v3, "DesiredVolume"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 540
    if-eqz v2, :cond_7

    invoke-virtual {v2, p4}, Lcom/samsung/upnp/Argument;->isAllowedValue(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 541
    :cond_7
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v3

    .line 543
    :cond_8
    const-string v3, "InstanceID"

    invoke-virtual {v0, v3, p2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 544
    const-string v3, "Channel"

    invoke-virtual {v0, v3, p3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string v3, "DesiredVolume"

    invoke-virtual {v0, v3, p4}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 547
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v3

    if-nez v3, :cond_a

    .line 548
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DLNA : DMC Service"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "action fail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 549
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v3

    const/16 v4, 0x19a

    if-ne v3, v4, :cond_9

    .line 550
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    .line 551
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 552
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 550
    invoke-direct {v3, v6, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v3

    .line 554
    :cond_9
    new-instance v3, Lcom/samsung/api/DMCAPIException;

    const/16 v4, 0x1f5

    .line 555
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v5

    .line 556
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v6

    .line 554
    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v3

    .line 559
    :cond_a
    return-void
.end method

.method public startAPI()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/upnp/ControlPoint;->start()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 144
    :cond_0
    return-void
.end method

.method public startSimpleServer(Ljava/util/HashMap;)Lcom/samsung/api/ContentItem;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/api/ContentItem;"
        }
    .end annotation

    .prologue
    .local p1, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 1106
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    if-eqz v2, :cond_0

    .line 1107
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v2}, Lcom/samsung/upnp/SimpleDevice;->stop()Z

    .line 1108
    iput-object v1, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 1110
    :cond_0
    new-instance v2, Lcom/samsung/upnp/SimpleDevice;

    invoke-direct {v2, p1}, Lcom/samsung/upnp/SimpleDevice;-><init>(Ljava/util/HashMap;)V

    iput-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    .line 1111
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v2}, Lcom/samsung/upnp/SimpleDevice;->start()Z

    .line 1113
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mSimpleDevice:Lcom/samsung/upnp/SimpleDevice;

    invoke-virtual {v2}, Lcom/samsung/upnp/SimpleDevice;->getItemNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 1114
    .local v0, "info":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_1

    .line 1115
    new-instance v1, Lcom/samsung/api/ContentItem;

    invoke-direct {v1, v0}, Lcom/samsung/api/ContentItem;-><init>(Lcom/samsung/xml/Node;)V

    .line 1117
    :cond_1
    return-object v1
.end method

.method public stopAPI()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/upnp/ControlPoint;->stop()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Lcom/samsung/api/DMCAPIException;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v0

    .line 155
    :cond_0
    return-void
.end method

.method public stopItem(Lcom/samsung/api/DeviceItem;)V
    .locals 6
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/api/DMCAPIException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    .line 366
    if-nez p1, :cond_0

    .line 367
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 368
    :cond_0
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 369
    .local v1, "dmr":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_1

    .line 370
    const-string v2, "DLNA : DMC Service (Stop) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 371
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    invoke-direct {v2, v5}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 374
    :cond_1
    const-string v2, "Stop"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 375
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_2

    .line 376
    const-string v2, "DLNA : DMC Service (Stop) Fail cuz there is no DMR"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 377
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x191

    invoke-direct {v2, v3}, Lcom/samsung/api/DMCAPIException;-><init>(I)V

    throw v2

    .line 379
    :cond_2
    const-string v2, "InstanceID"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;I)V

    .line 380
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    .line 381
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z

    move-result v2

    if-nez v2, :cond_4

    .line 382
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DLNA : DMC Service"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "action fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;)V

    .line 383
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v2

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_3

    .line 384
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    .line 385
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v3

    .line 386
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v4

    .line 384
    invoke-direct {v2, v5, v3, v4}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 388
    :cond_3
    new-instance v2, Lcom/samsung/api/DMCAPIException;

    const/16 v3, 0x1f5

    .line 389
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v4

    .line 390
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getControlStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v5

    .line 388
    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/api/DMCAPIException;-><init>(ILjava/lang/String;Lcom/samsung/upnp/UPnPStatus;)V

    throw v2

    .line 393
    :cond_4
    return-void
.end method

.method public subscribePlayer(Lcom/samsung/api/DeviceItem;)Z
    .locals 8
    .param p1, "player"    # Lcom/samsung/api/DeviceItem;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 862
    if-nez p1, :cond_0

    move v3, v4

    .line 880
    :goto_0
    return v3

    .line 865
    :cond_0
    iget-object v3, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v3, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 867
    .local v0, "device":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_5

    .line 868
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v1

    .line 869
    .local v1, "sList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ServiceList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .end local v1    # "sList":Lcom/samsung/upnp/ServiceList;
    :cond_2
    move v3, v5

    .line 880
    goto :goto_0

    .line 869
    .restart local v1    # "sList":Lcom/samsung/upnp/ServiceList;
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .local v2, "service":Ljava/lang/Object;
    move-object v3, v2

    .line 870
    check-cast v3, Lcom/samsung/upnp/Service;

    invoke-virtual {v3}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v3

    const-string v7, "AVTransport"

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 871
    iget-object v3, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    check-cast v2, Lcom/samsung/upnp/Service;

    .end local v2    # "service":Ljava/lang/Object;
    invoke-virtual {v3, v2}, Lcom/samsung/upnp/ControlPoint;->subscribe(Lcom/samsung/upnp/Service;)Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v5

    .line 872
    goto :goto_0

    :cond_4
    move v3, v4

    .line 874
    goto :goto_0

    .line 877
    .end local v1    # "sList":Lcom/samsung/upnp/ServiceList;
    :cond_5
    if-nez v0, :cond_2

    move v3, v5

    .line 878
    goto :goto_0
.end method

.method public switchPlayer(Lcom/samsung/api/DeviceItem;Lcom/samsung/api/DeviceItem;)Z
    .locals 4
    .param p1, "oldPlayer"    # Lcom/samsung/api/DeviceItem;
    .param p2, "newPlayer"    # Lcom/samsung/api/DeviceItem;

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 891
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->mCp:Lcom/samsung/upnp/ControlPoint;

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 893
    .local v0, "device":Lcom/samsung/upnp/Device;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/api/DeviceItem;->getDeviceType()I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 903
    :cond_0
    :goto_0
    return v1

    .line 896
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/samsung/api/DeviceItem;->getDeviceType()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 899
    :cond_2
    if-eqz v0, :cond_3

    .line 900
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->unsubscribeDevice(Ljava/lang/String;)V

    .line 903
    :cond_3
    invoke-virtual {p0, p2}, Lcom/samsung/api/DigitalMediaAddinControllerAPI;->subscribePlayer(Lcom/samsung/api/DeviceItem;)Z

    move-result v1

    goto :goto_0
.end method
