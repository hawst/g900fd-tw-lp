.class public Lcom/samsung/api/HTTPStatus_API;
.super Ljava/lang/Object;
.source "HTTPStatus_API.java"


# static fields
.field public static final BAD_REQUEST:I = 0x190

.field public static final CONTINUE:I = 0x64

.field public static final GONE:I = 0x19a

.field public static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final INVALID_RANGE:I = 0x1a0

.field public static final NOT_ACCEPTABLE:I = 0x196

.field public static final NOT_FOUND:I = 0x194

.field public static final OK:I = 0xc8

.field public static final PARTIAL_CONTENT:I = 0xce

.field public static final PRECONDITION_FAILED:I = 0x19c

.field public static final REQUEST_TIMEOUT:I = 0x198


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final code2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 55
    sparse-switch p0, :sswitch_data_0

    .line 68
    const-string v0, ""

    :goto_0
    return-object v0

    .line 56
    :sswitch_0
    const-string v0, "Continue"

    goto :goto_0

    .line 57
    :sswitch_1
    const-string v0, "OK"

    goto :goto_0

    .line 58
    :sswitch_2
    const-string v0, "Partial Content"

    goto :goto_0

    .line 59
    :sswitch_3
    const-string v0, "Bad Request"

    goto :goto_0

    .line 60
    :sswitch_4
    const-string v0, "Not Found"

    goto :goto_0

    .line 61
    :sswitch_5
    const-string v0, "Precondition Failed"

    goto :goto_0

    .line 62
    :sswitch_6
    const-string v0, "Invalid Range"

    goto :goto_0

    .line 63
    :sswitch_7
    const-string v0, "Internal Server Error"

    goto :goto_0

    .line 64
    :sswitch_8
    const-string v0, "Not Acceptable"

    goto :goto_0

    .line 65
    :sswitch_9
    const-string v0, "Device is Gone"

    goto :goto_0

    .line 66
    :sswitch_a
    const-string v0, "Request Time Out"

    goto :goto_0

    .line 55
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xce -> :sswitch_2
        0x190 -> :sswitch_3
        0x194 -> :sswitch_4
        0x196 -> :sswitch_8
        0x198 -> :sswitch_a
        0x19a -> :sswitch_9
        0x19c -> :sswitch_5
        0x1a0 -> :sswitch_6
        0x1f4 -> :sswitch_7
    .end sparse-switch
.end method
