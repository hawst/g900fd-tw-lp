.class public Lcom/samsung/api/DigitalMediaServerAPI;
.super Ljava/lang/Object;
.source "DigitalMediaServerAPI.java"


# static fields
.field public static final AUDIO_CATEGORY:I = 0x1

.field public static final AUDIO_DIR_NAME:Ljava/lang/String; = "Audio"

.field public static final IMAGE_CATEGORY:I = 0x0

.field public static final IMAGE_DIR_NAMe:Ljava/lang/String; = "Image"

.field public static final MACROICON_JPEG:I = 0x0

.field public static final MACROICON_PNG:I = 0x1

.field public static final SMALLICON_JPEG:I = 0x2

.field public static final SMALLICON_PNG:I = 0x3

.field private static final TAG:Ljava/lang/String; = "Samsung DMS"

.field public static final VIDEO_CATEOGRY:I = 0x2

.field public static final VIDEO_DIR_NAME:Ljava/lang/String; = "Video"

.field private static volatile mInstance:Lcom/samsung/api/DigitalMediaServerAPI;

.field private static mMutex:Ljava/lang/Object;


# instance fields
.field private mAudioDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

.field private mImageDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

.field private mServer:Lcom/samsung/upnp/media/server/MediaServer;

.field private mVideoDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/api/DigitalMediaServerAPI;->mInstance:Lcom/samsung/api/DigitalMediaServerAPI;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/api/DigitalMediaServerAPI;->mMutex:Ljava/lang/Object;

    .line 69
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    .line 46
    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mImageDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 47
    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mAudioDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 48
    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mVideoDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 89
    invoke-static {p1}, Lcom/samsung/upnp/media/server/MediaServer;->getMediaServerInstatnce(Landroid/content/Context;)Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    .line 91
    new-instance v0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    const-string v1, "Audio"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mAudioDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 92
    new-instance v0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    const-string v1, "Video"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mVideoDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 93
    new-instance v0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    const-string v1, "Image"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mImageDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 95
    return-void
.end method

.method private getDirectory(I)Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    .locals 1
    .param p1, "category"    # I

    .prologue
    .line 381
    packed-switch p1, :pswitch_data_0

    .line 389
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 383
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mVideoDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    goto :goto_0

    .line 385
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mAudioDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    goto :goto_0

    .line 387
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mImageDir:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    goto :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/api/DigitalMediaServerAPI;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/api/DigitalMediaServerAPI;->mInstance:Lcom/samsung/api/DigitalMediaServerAPI;

    if-nez v0, :cond_1

    .line 79
    sget-object v1, Lcom/samsung/api/DigitalMediaServerAPI;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_0
    sget-object v0, Lcom/samsung/api/DigitalMediaServerAPI;->mInstance:Lcom/samsung/api/DigitalMediaServerAPI;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/samsung/api/DigitalMediaServerAPI;

    invoke-direct {v0, p0}, Lcom/samsung/api/DigitalMediaServerAPI;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/api/DigitalMediaServerAPI;->mInstance:Lcom/samsung/api/DigitalMediaServerAPI;

    .line 79
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :cond_1
    sget-object v0, Lcom/samsung/api/DigitalMediaServerAPI;->mInstance:Lcom/samsung/api/DigitalMediaServerAPI;

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public addDirectory(I)Z
    .locals 3
    .param p1, "category"    # I

    .prologue
    const/4 v1, 0x0

    .line 308
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v2, :cond_1

    .line 315
    :cond_0
    :goto_0
    return v1

    .line 311
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/api/DigitalMediaServerAPI;->getDirectory(I)Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    move-result-object v0

    .line 312
    .local v0, "dir":Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0, v0}, Lcom/samsung/api/DigitalMediaServerAPI;->addDirectory(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Z

    move-result v1

    goto :goto_0
.end method

.method public addDirectory(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 212
    :cond_0
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->addContentDirectory(Lcom/samsung/upnp/media/server/Directory;)Z

    move-result v0

    goto :goto_0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    const-string v0, ""

    .line 231
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v1, :cond_0

    .line 232
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/MediaServer;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    .line 233
    :cond_0
    return-object v0
.end method

.method public getUDN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    const-string v0, ""

    .line 242
    .local v0, "udn":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/MediaServer;->getUDN()Ljava/lang/String;

    move-result-object v0

    .line 244
    :cond_0
    return-object v0
.end method

.method public getUploadPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v0, :cond_0

    .line 297
    const-string v0, ""

    .line 299
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->getUploadPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isDirectoryShared(I)Z
    .locals 3
    .param p1, "category"    # I

    .prologue
    const/4 v1, 0x0

    .line 371
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v2, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v1

    .line 373
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/api/DigitalMediaServerAPI;->getDirectory(I)Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    move-result-object v0

    .line 374
    .local v0, "dir":Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    if-eqz v0, :cond_0

    .line 377
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->isContainedDirectory(Lcom/samsung/upnp/media/server/Directory;)Z

    move-result v1

    goto :goto_0
.end method

.method public isDirectoryShared(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 262
    :cond_0
    const/4 v0, 0x0

    .line 264
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->isContainedDirectory(Lcom/samsung/upnp/media/server/Directory;)Z

    move-result v0

    goto :goto_0
.end method

.method public isServerStarted()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->isServerStarted()Z

    move-result v0

    goto :goto_0
.end method

.method public removeDirectory(I)Z
    .locals 3
    .param p1, "category"    # I

    .prologue
    const/4 v1, 0x0

    .line 324
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v2, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v1

    .line 327
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/api/DigitalMediaServerAPI;->getDirectory(I)Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    move-result-object v0

    .line 328
    .local v0, "dir":Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {p0, v0}, Lcom/samsung/api/DigitalMediaServerAPI;->removeDirectory(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Z

    move-result v1

    goto :goto_0
.end method

.method public removeDirectory(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 220
    :cond_0
    const/4 v0, 0x0

    .line 222
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->removeContentDirectory(Lcom/samsung/upnp/media/server/Directory;)Z

    move-result v0

    goto :goto_0
.end method

.method public setCreateObjectReceivedListener(Lcom/samsung/api/CreateObjectReceived;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/CreateObjectReceived;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 274
    :cond_0
    const/4 v0, 0x0

    .line 277
    :goto_0
    return v0

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->setCreateObjectReceivedListener(Lcom/samsung/api/CreateObjectReceived;)V

    .line 277
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDirectoryName(ILjava/lang/String;)V
    .locals 2
    .param p1, "category"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 355
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/api/DigitalMediaServerAPI;->getDirectory(I)Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    move-result-object v0

    .line 359
    .local v0, "dir":Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {v0, p2}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setFriendlyName(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 254
    :cond_0
    const/4 v0, 0x0

    .line 256
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {p0}, Lcom/samsung/api/DigitalMediaServerAPI;->isServerStarted()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/upnp/media/server/MediaServer;->setFriendlyName(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setIcon(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "small"    # Landroid/graphics/Bitmap;
    .param p2, "macro"    # Landroid/graphics/Bitmap;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/upnp/media/server/MediaServer;->setIcon(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 192
    return-void
.end method

.method public setServerIcon(Ljava/io/ByteArrayOutputStream;I)Z
    .locals 3
    .param p1, "icon"    # Ljava/io/ByteArrayOutputStream;
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v2, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v1

    .line 160
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 174
    const/4 v0, -0x1

    .line 177
    .local v0, "type2":I
    :goto_1
    if-ltz v0, :cond_0

    .line 180
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/upnp/media/server/MediaServer;->setIcon(Ljava/io/ByteArrayOutputStream;I)V

    .line 181
    const/4 v1, 0x1

    goto :goto_0

    .line 162
    .end local v0    # "type2":I
    :pswitch_0
    const/4 v0, 0x0

    .line 163
    .restart local v0    # "type2":I
    goto :goto_1

    .line 165
    .end local v0    # "type2":I
    :pswitch_1
    const/4 v0, 0x1

    .line 166
    .restart local v0    # "type2":I
    goto :goto_1

    .line 168
    .end local v0    # "type2":I
    :pswitch_2
    const/4 v0, 0x2

    .line 169
    .restart local v0    # "type2":I
    goto :goto_1

    .line 171
    .end local v0    # "type2":I
    :pswitch_3
    const/4 v0, 0x3

    .line 172
    .restart local v0    # "type2":I
    goto :goto_1

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setUploadFinishedListener(Lcom/samsung/api/UploadFinished;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/UploadFinished;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->setUploadFinishedListener(Lcom/samsung/api/UploadFinished;)V

    .line 207
    return-void
.end method

.method public setUploadPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 287
    :cond_0
    const/4 v0, 0x0

    .line 288
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->setUploadPath(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public startMediaServer(Landroid/content/Context;Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "desc"    # Ljava/io/InputStream;
    .param p3, "cds"    # Ljava/io/InputStream;
    .param p4, "cms"    # Ljava/io/InputStream;
    .param p5, "friendlyName"    # Ljava/lang/String;
    .param p6, "uuid"    # Ljava/lang/String;
    .param p7, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 132
    :goto_0
    return v0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/api/DigitalMediaServerAPI;->isServerStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    const/4 v0, 0x0

    goto :goto_0

    .line 122
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/upnp/media/server/MediaServer;->initailize(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    new-instance v1, Lcom/samsung/upnp/media/server/object/format/DefaultFormat;

    invoke-direct {v1}, Lcom/samsung/upnp/media/server/object/format/DefaultFormat;-><init>()V

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/MediaServer;->addPlugIn(Lcom/samsung/upnp/media/server/object/Format;)Z
    :try_end_0
    .catch Lcom/samsung/upnp/device/InvalidDescriptionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    invoke-static {}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getInstance()Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->setContext(Landroid/content/Context;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->start()Z

    move-result v0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v7

    .line 125
    .local v7, "e":Lcom/samsung/upnp/device/InvalidDescriptionException;
    invoke-virtual {v7}, Lcom/samsung/upnp/device/InvalidDescriptionException;->printStackTrace()V

    .line 126
    const-string v0, "Samsung DMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "there is some error in initialize "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/samsung/upnp/device/InvalidDescriptionException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopMediaServer()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 140
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return v0

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/api/DigitalMediaServerAPI;->isServerStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->stop()Z

    move-result v0

    goto :goto_0
.end method

.method public updateDirectory(I)V
    .locals 2
    .param p1, "category"    # I

    .prologue
    .line 339
    iget-object v1, p0, Lcom/samsung/api/DigitalMediaServerAPI;->mServer:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v1, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/api/DigitalMediaServerAPI;->getDirectory(I)Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    move-result-object v0

    .line 343
    .local v0, "dir":Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->update()V

    goto :goto_0
.end method
