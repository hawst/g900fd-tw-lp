.class public Lcom/samsung/api/ProtocolInfo;
.super Ljava/lang/Object;
.source "ProtocolInfo.java"


# static fields
.field public static final DLNA_ORG_FLAGS:Ljava/lang/String; = "DLNA.ORG_FLAGS"

.field public static final DLNA_ORG_OP:Ljava/lang/String; = "DLNA.ORG_OP"

.field public static final DLNA_ORG_PN:Ljava/lang/String; = "DLNA.ORG_PN"


# instance fields
.field private mDlnaFeatures:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFirst:Ljava/lang/String;

.field private mFourth:Ljava/lang/String;

.field private mProtocol:Ljava/lang/String;

.field private mSecond:Ljava/lang/String;

.field private mThird:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mDlnaFeatures:Ljava/util/HashMap;

    .line 75
    return-void
.end method

.method public static ParseProtocolInfo(Ljava/lang/String;)Lcom/samsung/api/ProtocolInfo;
    .locals 8
    .param p0, "protocol"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 49
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, ":"

    invoke-direct {v3, p0, v4, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 50
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_1

    .line 51
    const/4 v2, 0x0

    .line 70
    :cond_0
    return-object v2

    .line 53
    :cond_1
    new-instance v2, Lcom/samsung/api/ProtocolInfo;

    invoke-direct {v2}, Lcom/samsung/api/ProtocolInfo;-><init>()V

    .line 55
    .local v2, "pinfo":Lcom/samsung/api/ProtocolInfo;
    iput-object p0, v2, Lcom/samsung/api/ProtocolInfo;->mProtocol:Ljava/lang/String;

    .line 56
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/api/ProtocolInfo;->mFirst:Ljava/lang/String;

    .line 57
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/api/ProtocolInfo;->mSecond:Ljava/lang/String;

    .line 58
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/api/ProtocolInfo;->mThird:Ljava/lang/String;

    .line 59
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/api/ProtocolInfo;->mFourth:Ljava/lang/String;

    .line 61
    new-instance v3, Ljava/util/StringTokenizer;

    .end local v3    # "st":Ljava/util/StringTokenizer;
    iget-object v4, v2, Lcom/samsung/api/ProtocolInfo;->mFourth:Ljava/lang/String;

    const-string v5, ";"

    invoke-direct {v3, v4, v5, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 63
    .restart local v3    # "st":Ljava/util/StringTokenizer;
    :cond_2
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 64
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "feature":Ljava/lang/String;
    const-string v4, "="

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "features":[Ljava/lang/String;
    array-length v4, v1

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 67
    iget-object v4, v2, Lcom/samsung/api/ProtocolInfo;->mDlnaFeatures:Ljava/util/HashMap;

    aget-object v5, v1, v7

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public get1stField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mFirst:Ljava/lang/String;

    return-object v0
.end method

.method public get2ndField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mSecond:Ljava/lang/String;

    return-object v0
.end method

.method public get3rdField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mThird:Ljava/lang/String;

    return-object v0
.end method

.method public get4thField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mFourth:Ljava/lang/String;

    return-object v0
.end method

.method public getDlnaFlags()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mDlnaFeatures:Ljava/util/HashMap;

    const-string v1, "DLNA.ORG_FLAGS"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDlnaOP()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mDlnaFeatures:Ljava/util/HashMap;

    const-string v1, "DLNA.ORG_OP"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDlnaPN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mDlnaFeatures:Ljava/util/HashMap;

    const-string v1, "DLNA.ORG_PN"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/api/ProtocolInfo;->mProtocol:Ljava/lang/String;

    return-object v0
.end method
