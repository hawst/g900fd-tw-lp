.class public final Lcom/samsung/api/Debugs;
.super Ljava/lang/Object;
.source "Debugs.java"


# static fields
.field static final DEBUG:I = 0x2

.field static final ERROR:I = 0x3

.field static final INFO:I = 0x0

.field static final WARN:I = 0x1

.field public static enabled:Z

.field private static mTag:Ljava/lang/String;

.field private static outputList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/api/DebugOutputHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    .line 64
    const-string v0, "samsung"

    sput-object v0, Lcom/samsung/api/Debugs;->mTag:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/api/Debugs;->outputList:Ljava/util/ArrayList;

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final addDebugOutputHandler(Lcom/samsung/api/DebugOutputHandler;)V
    .locals 1
    .param p0, "handler"    # Lcom/samsung/api/DebugOutputHandler;

    .prologue
    .line 77
    if-eqz p0, :cond_0

    .line 78
    sget-object v0, Lcom/samsung/api/Debugs;->outputList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_0
    return-void
.end method

.method public static final declared-synchronized debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 135
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_0
    monitor-exit v1

    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized error(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 182
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x3

    sget-object v2, Lcom/samsung/api/Debugs;->mTag:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :cond_0
    monitor-exit v1

    return-void

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized error(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 192
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x3

    invoke-static {v0, p0, p1}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    monitor-exit v1

    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized info(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 122
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x0

    sget-object v2, Lcom/samsung/api/Debugs;->mTag:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :cond_0
    monitor-exit v1

    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isOn()Z
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    return v0
.end method

.method public static final off()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    .line 54
    return-void
.end method

.method public static final on()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    .line 47
    return-void
.end method

.method private static final declared-synchronized printDebug(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "stat"    # I
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 99
    const-class v2, Lcom/samsung/api/Debugs;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/samsung/api/Debugs;->outputList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 115
    monitor-exit v2

    return-void

    .line 99
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/api/DebugOutputHandler;

    .line 100
    .local v0, "out":Lcom/samsung/api/DebugOutputHandler;
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    invoke-interface {v0, p1, p2}, Lcom/samsung/api/DebugOutputHandler;->info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 99
    .end local v0    # "out":Lcom/samsung/api/DebugOutputHandler;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 105
    .restart local v0    # "out":Lcom/samsung/api/DebugOutputHandler;
    :pswitch_1
    :try_start_2
    invoke-interface {v0, p1, p2}, Lcom/samsung/api/DebugOutputHandler;->warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :pswitch_2
    invoke-interface {v0, p1, p2}, Lcom/samsung/api/DebugOutputHandler;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :pswitch_3
    invoke-interface {v0, p1, p2}, Lcom/samsung/api/DebugOutputHandler;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static final removeDebugOutputHandler(Lcom/samsung/api/DebugOutputHandler;)V
    .locals 1
    .param p0, "handler"    # Lcom/samsung/api/DebugOutputHandler;

    .prologue
    .line 86
    sget-object v0, Lcom/samsung/api/Debugs;->outputList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 87
    return-void
.end method

.method public static final declared-synchronized warning(Ljava/lang/Exception;)V
    .locals 4
    .param p0, "e"    # Ljava/lang/Exception;

    .prologue
    .line 173
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x1

    sget-object v2, Lcom/samsung/api/Debugs;->mTag:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_0
    monitor-exit v1

    return-void

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized warning(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 144
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x1

    sget-object v2, Lcom/samsung/api/Debugs;->mTag:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_0
    monitor-exit v1

    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized warning(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 164
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p0, v2}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    :cond_0
    monitor-exit v1

    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized warning(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 154
    const-class v1, Lcom/samsung/api/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/api/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x1

    invoke-static {v0, p0, p1}, Lcom/samsung/api/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :cond_0
    monitor-exit v1

    return-void

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
