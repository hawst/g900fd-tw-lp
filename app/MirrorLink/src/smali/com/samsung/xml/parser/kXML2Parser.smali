.class public Lcom/samsung/xml/parser/kXML2Parser;
.super Lcom/samsung/xml/Parser;
.source "kXML2Parser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/xml/Parser;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;
    .locals 17
    .param p1, "inStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 44
    const/4 v13, 0x0

    .line 45
    .local v13, "rootNode":Lcom/samsung/xml/Node;
    const/4 v4, 0x0

    .line 46
    .local v4, "currNode":Lcom/samsung/xml/Node;
    const/4 v8, 0x0

    .line 49
    .local v8, "inReader":Ljava/io/InputStreamReader;
    :try_start_0
    new-instance v9, Ljava/io/InputStreamReader;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 50
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .local v9, "inReader":Ljava/io/InputStreamReader;
    :try_start_1
    new-instance v15, Lorg/kxml2/io/KXmlParser;

    invoke-direct {v15}, Lorg/kxml2/io/KXmlParser;-><init>()V

    .line 51
    .local v15, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v15, v9}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 52
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    .line 53
    .local v7, "eventType":I
    :goto_0
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v7, v0, :cond_0

    .line 104
    if-eqz v9, :cond_5

    .line 107
    :try_start_2
    invoke-virtual {v9}, Ljava/io/InputStreamReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 108
    const/4 v8, 0x0

    .line 114
    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    :goto_1
    return-object v13

    .line 54
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .restart local v9    # "inReader":Ljava/io/InputStreamReader;
    :cond_0
    packed-switch v7, :pswitch_data_0

    .line 88
    :cond_1
    :goto_2
    :try_start_3
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    goto :goto_0

    .line 57
    :pswitch_0
    new-instance v11, Lcom/samsung/xml/Node;

    invoke-direct {v11}, Lcom/samsung/xml/Node;-><init>()V

    .line 58
    .local v11, "node":Lcom/samsung/xml/Node;
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 59
    .local v12, "nodeName":Ljava/lang/String;
    invoke-virtual {v11, v12}, Lcom/samsung/xml/Node;->setName(Ljava/lang/String;)V

    .line 60
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v3

    .line 61
    .local v3, "attrsLen":I
    const/4 v10, 0x0

    .local v10, "n":I
    :goto_3
    if-lt v10, v3, :cond_3

    .line 67
    if-eqz v4, :cond_2

    .line 68
    invoke-virtual {v4, v11}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 69
    :cond_2
    move-object v4, v11

    .line 70
    if-nez v13, :cond_1

    .line 71
    move-object v13, v11

    .line 73
    goto :goto_2

    .line 62
    :cond_3
    invoke-interface {v15, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "attrName":Ljava/lang/String;
    invoke-interface {v15, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "attrValue":Ljava/lang/String;
    invoke-virtual {v11, v1, v2}, Lcom/samsung/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 76
    .end local v1    # "attrName":Ljava/lang/String;
    .end local v2    # "attrValue":Ljava/lang/String;
    .end local v3    # "attrsLen":I
    .end local v10    # "n":I
    .end local v11    # "node":Lcom/samsung/xml/Node;
    .end local v12    # "nodeName":Ljava/lang/String;
    :pswitch_1
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 77
    .local v14, "value":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 78
    invoke-virtual {v4, v14}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 91
    .end local v7    # "eventType":I
    .end local v14    # "value":Ljava/lang/String;
    .end local v15    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v5

    move-object v8, v9

    .line 92
    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .local v5, "e":Ljava/lang/Exception;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    :goto_4
    if-eqz v8, :cond_4

    .line 95
    :try_start_4
    invoke-virtual {v8}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 96
    const/4 v8, 0x0

    .line 102
    :cond_4
    :goto_5
    new-instance v16, Lcom/samsung/xml/ParserException;

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Lcom/samsung/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v16

    .line 83
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .restart local v7    # "eventType":I
    .restart local v9    # "inReader":Ljava/io/InputStreamReader;
    .restart local v15    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_2
    if-eqz v4, :cond_1

    .line 84
    :try_start_5
    invoke-virtual {v4}, Lcom/samsung/xml/Node;->getParentNode()Lcom/samsung/xml/Node;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v4

    goto :goto_2

    .line 97
    .end local v7    # "eventType":I
    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .end local v15    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5    # "e":Ljava/lang/Exception;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    :catch_1
    move-exception v6

    .line 99
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 109
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/io/IOException;
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .restart local v7    # "eventType":I
    .restart local v9    # "inReader":Ljava/io/InputStreamReader;
    .restart local v15    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v6

    .line 111
    .restart local v6    # "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .end local v6    # "e1":Ljava/io/IOException;
    :cond_5
    move-object v8, v9

    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    goto :goto_1

    .line 91
    .end local v7    # "eventType":I
    .end local v15    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_3
    move-exception v5

    goto :goto_4

    .line 54
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public parse(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 2
    .param p1, "info"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 120
    .local v0, "is":Ljava/io/ByteArrayInputStream;
    invoke-virtual {p0, v0}, Lcom/samsung/xml/parser/kXML2Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v1

    return-object v1
.end method
