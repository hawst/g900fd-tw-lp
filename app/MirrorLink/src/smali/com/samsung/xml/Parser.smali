.class public abstract Lcom/samsung/xml/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method


# virtual methods
.method public parse(Ljava/io/File;)Lcom/samsung/xml/Node;
    .locals 5
    .param p1, "descriptionFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 102
    const/4 v1, 0x0

    .line 104
    .local v1, "fileIn":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    .end local v1    # "fileIn":Ljava/io/InputStream;
    .local v2, "fileIn":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 112
    .local v3, "root":Lcom/samsung/xml/Node;
    if-eqz v2, :cond_0

    .line 113
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 106
    :cond_0
    :goto_0
    return-object v3

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 108
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fileIn":Ljava/io/InputStream;
    .end local v3    # "root":Lcom/samsung/xml/Node;
    .restart local v1    # "fileIn":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    new-instance v4, Lcom/samsung/xml/ParserException;

    invoke-direct {v4, v0}, Lcom/samsung/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 110
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 112
    :goto_2
    if-eqz v1, :cond_1

    .line 113
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 117
    :cond_1
    :goto_3
    throw v4

    .line 114
    :catch_2
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 110
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fileIn":Ljava/io/InputStream;
    .restart local v2    # "fileIn":Ljava/io/InputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fileIn":Ljava/io/InputStream;
    .restart local v1    # "fileIn":Ljava/io/InputStream;
    goto :goto_2

    .line 108
    .end local v1    # "fileIn":Ljava/io/InputStream;
    .restart local v2    # "fileIn":Ljava/io/InputStream;
    :catch_3
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileIn":Ljava/io/InputStream;
    .restart local v1    # "fileIn":Ljava/io/InputStream;
    goto :goto_1
.end method

.method public abstract parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation
.end method

.method public abstract parse(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation
.end method

.method public parse(Ljava/net/URL;)Lcom/samsung/xml/Node;
    .locals 6
    .param p1, "locationURL"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v3, 0x0

    .line 73
    .local v3, "urlIn":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 75
    .local v2, "urlCon":Ljava/net/HttpURLConnection;
    const/16 v4, 0xbb8

    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 76
    const-string v4, "GET"

    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 77
    const-string v4, "USER-AGENT"

    sget-object v5, Lcom/samsung/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 79
    invoke-virtual {p0, v3}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v1

    .line 80
    .local v1, "rootElem":Lcom/samsung/xml/Node;
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    if-eqz v3, :cond_0

    .line 88
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 81
    :cond_0
    :goto_0
    return-object v1

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "rootElem":Lcom/samsung/xml/Node;
    .end local v2    # "urlCon":Ljava/net/HttpURLConnection;
    :catch_1
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v4, Lcom/samsung/xml/ParserException;

    invoke-direct {v4, v0}, Lcom/samsung/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 85
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 86
    if-eqz v3, :cond_1

    .line 88
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 92
    :cond_1
    :goto_1
    throw v4

    .line 89
    :catch_2
    move-exception v0

    .line 90
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
