.class public Lcom/samsung/xml/NodeList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "NodeList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/xml/Node;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public getEndsWith(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 64
    if-nez p1, :cond_0

    move-object v2, v4

    .line 76
    :goto_0
    return-object v2

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/xml/NodeList;->size()I

    move-result v1

    .line 68
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-lt v0, v1, :cond_1

    move-object v2, v4

    .line 76
    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/xml/NodeList;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 70
    .local v2, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "nodeName":Ljava/lang/String;
    if-nez v3, :cond_3

    .line 68
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 73
    :cond_3
    invoke-virtual {v3, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0
.end method

.method public getNode(I)Lcom/samsung/xml/Node;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 41
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/xml/NodeList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/xml/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 49
    if-nez p1, :cond_1

    move-object v2, v4

    .line 59
    :cond_0
    :goto_0
    return-object v2

    .line 52
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/xml/NodeList;->size()I

    move-result v1

    .line 53
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-lt v0, v1, :cond_2

    move-object v2, v4

    .line 59
    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/xml/NodeList;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 55
    .local v2, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "nodeName":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
