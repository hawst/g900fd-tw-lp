.class public Lcom/samsung/http/HTTPPacket;
.super Ljava/lang/Object;
.source "HTTPPacket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/http/HTTPPacket$DualReader;
    }
.end annotation


# instance fields
.field private bFileContent:Z

.field private content:[B

.field private contentInput:Ljava/io/InputStream;

.field private fileContent:Ljava/io/File;

.field private firstLine:Ljava/lang/String;

.field private httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/http/HTTPHeader;",
            ">;"
        }
    .end annotation
.end field

.field private httpServer:Lcom/samsung/http/HTTPServer;

.field private httpSock:Lcom/samsung/http/HTTPSocket;

.field rand:Ljava/util/Random;

.field private reader:Lcom/samsung/http/HTTPPacket$DualReader;

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object v1, p0, Lcom/samsung/http/HTTPPacket;->httpServer:Lcom/samsung/http/HTTPServer;

    .line 221
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->rand:Ljava/util/Random;

    .line 354
    iput-boolean v2, p0, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    .line 379
    iput-object v1, p0, Lcom/samsung/http/HTTPPacket;->reader:Lcom/samsung/http/HTTPPacket$DualReader;

    .line 476
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->firstLine:Ljava/lang/String;

    .line 508
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 679
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    .line 791
    iput-object v1, p0, Lcom/samsung/http/HTTPPacket;->contentInput:Ljava/io/InputStream;

    .line 59
    const-string v0, "1.1"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->setVersion(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPPacket;->setContentInputStream(Ljava/io/InputStream;)V

    .line 61
    return-void
.end method

.method private setFirstLine(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->firstLine:Ljava/lang/String;

    .line 480
    return-void
.end method


# virtual methods
.method public addHeader(Lcom/samsung/http/HTTPHeader;)V
    .locals 1
    .param p1, "header"    # Lcom/samsung/http/HTTPHeader;

    .prologue
    .line 516
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    return-void
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 521
    new-instance v0, Lcom/samsung/http/HTTPHeader;

    invoke-direct {v0, p1, p2}, Lcom/samsung/http/HTTPHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    iget-object v1, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 523
    return-void
.end method

.method public clearHeaders()V
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 553
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 554
    return-void
.end method

.method public getConnection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 875
    const-string v0, "Connection"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 753
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasFileContent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 754
    const/4 v1, 0x0

    .line 757
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getFileContent()Ljava/io/File;

    move-result-object v2

    .line 758
    .local v2, "tempFile":Ljava/io/File;
    if-nez v2, :cond_0

    move-object v1, v3

    .line 767
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "tempFile":Ljava/io/File;
    :goto_0
    return-object v1

    .line 760
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "tempFile":Ljava/io/File;
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 761
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 763
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "tempFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-object v1, v3

    .line 764
    goto :goto_0

    .line 767
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0
.end method

.method public getContentInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->contentInput:Ljava/io/InputStream;

    return-object v0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 856
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->getLongHeaderValue(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getContentRange()[J
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 919
    const/4 v7, 0x3

    new-array v4, v7, [J

    .line 920
    .local v4, "range":[J
    const-wide/16 v7, 0x0

    aput-wide v7, v4, v11

    aput-wide v7, v4, v10

    aput-wide v7, v4, v9

    .line 921
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasContentRange()Z

    move-result v7

    if-nez v7, :cond_1

    .line 968
    :cond_0
    :goto_0
    return-object v4

    .line 923
    :cond_1
    const-string v7, "Content-Range"

    invoke-virtual {p0, v7}, Lcom/samsung/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 925
    .local v5, "rangeLine":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-gtz v7, :cond_2

    .line 926
    const-string v7, "Range"

    invoke-virtual {p0, v7}, Lcom/samsung/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 927
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 931
    const-string v7, "bytes"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 932
    const-string v7, "-1"

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    aput-wide v7, v4, v9

    goto :goto_0

    .line 937
    :cond_3
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v7, "="

    invoke-direct {v6, v5, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    .local v6, "strToken":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 941
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 944
    .local v0, "bytesStr":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 947
    const-string v7, "=-"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 949
    .local v1, "firstPosStr":Ljava/lang/String;
    const/4 v7, 0x0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 953
    :goto_1
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 955
    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 957
    .local v2, "lastPosStr":Ljava/lang/String;
    const/4 v7, 0x1

    :try_start_1
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 961
    :goto_2
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 963
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 965
    .local v3, "lengthStr":Ljava/lang/String;
    const/4 v7, 0x2

    :try_start_2
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 967
    :catch_0
    move-exception v7

    goto :goto_0

    .line 960
    .end local v3    # "lengthStr":Ljava/lang/String;
    :catch_1
    move-exception v7

    goto :goto_2

    .line 952
    .end local v2    # "lastPosStr":Ljava/lang/String;
    :catch_2
    move-exception v7

    goto :goto_1
.end method

.method public getContentRangeFirstPosition()J
    .locals 3

    .prologue
    .line 973
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getContentRange()[J

    move-result-object v0

    .line 974
    .local v0, "range":[J
    const/4 v1, 0x0

    aget-wide v1, v0, v1

    return-wide v1
.end method

.method public getContentRangeLastPosition()J
    .locals 3

    .prologue
    .line 979
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getContentRange()[J

    move-result-object v0

    .line 980
    .local v0, "range":[J
    const/4 v1, 0x1

    aget-wide v1, v0, v1

    return-wide v1
.end method

.method getContentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 774
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    new-instance v0, Ljava/lang/String;

    const-string v1, "This Packet Contains a file content, so can not be converted to String"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 777
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public getFileContent()Ljava/io/File;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    return-object v0
.end method

.method protected getFirstLine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->firstLine:Ljava/lang/String;

    return-object v0
.end method

.method protected getFirstLineToken(I)Ljava/lang/String;
    .locals 5
    .param p1, "num"    # I

    .prologue
    .line 489
    new-instance v2, Ljava/util/StringTokenizer;

    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->firstLine:Ljava/lang/String;

    const-string v4, " "

    invoke-direct {v2, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    .local v2, "st":Ljava/util/StringTokenizer;
    const-string v0, ""

    .line 491
    .local v0, "lastToken":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-le v1, p1, :cond_0

    .line 496
    .end local v0    # "lastToken":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 492
    .restart local v0    # "lastToken":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-nez v3, :cond_1

    .line 493
    const-string v0, ""

    goto :goto_1

    .line 494
    :cond_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 491
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getHeader(I)Lcom/samsung/http/HTTPHeader;
    .locals 4
    .param p1, "n"    # I

    .prologue
    .line 527
    const/4 v2, 0x0

    .line 529
    .local v2, "header":Lcom/samsung/http/HTTPHeader;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/http/HTTPHeader;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_0
    return-object v2

    .line 530
    :catch_0
    move-exception v1

    .line 531
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getNHeaders()I

    move-result v3

    .line 539
    .local v3, "nHeaders":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 547
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 540
    :cond_0
    invoke-virtual {p0, v2}, Lcom/samsung/http/HTTPPacket;->getHeader(I)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 541
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    if-nez v0, :cond_2

    .line 539
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 543
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v1

    .line 544
    .local v1, "headerName":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1
.end method

.method public getHeaderString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 666
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 667
    .local v3, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getNHeaders()I

    move-result v2

    .line 668
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 672
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 669
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPPacket;->getHeader(I)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 670
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/samsung/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 668
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 590
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 591
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 592
    const-string v1, ""

    .line 593
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getHttpServer()Lcom/samsung/http/HTTPServer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->httpServer:Lcom/samsung/http/HTTPServer;

    return-object v0
.end method

.method public getLongHeaderValue(Ljava/lang/String;)J
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 654
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 655
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 656
    const-wide/16 v1, 0x0

    .line 657
    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/util/StringUtil;->toLong(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_0
.end method

.method public getNHeaders()I
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->httpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getStringHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 631
    const-string v0, "\""

    const-string v1, "\""

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/http/HTTPPacket;->getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "startWidth"    # Ljava/lang/String;
    .param p3, "endWidth"    # Ljava/lang/String;

    .prologue
    .line 619
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 620
    .local v0, "headerValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 621
    const/4 v1, 0x0

    .line 626
    :goto_0
    return-object v1

    .line 622
    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 623
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 624
    :cond_1
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 625
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    move-object v1, v0

    .line 626
    goto :goto_0
.end method

.method public getTransferEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1077
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->version:Ljava/lang/String;

    return-object v0
.end method

.method handleExpect100Continue()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 197
    const-string v3, "Expect"

    invoke-virtual {p0, v3}, Lcom/samsung/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 198
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {v0}, Lcom/samsung/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "headerValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 201
    const-string v3, "100-continue"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 202
    instance-of v3, p0, Lcom/samsung/http/HTTPRequest;

    if-eqz v3, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getContentLength()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/samsung/http/HTTPPacket;->hasEnoughStorage(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 204
    check-cast p0, Lcom/samsung/http/HTTPRequest;

    .end local p0    # "this":Lcom/samsung/http/HTTPPacket;
    const/16 v2, 0x1a1

    invoke-virtual {p0, v2}, Lcom/samsung/http/HTTPRequest;->returnResponse(I)Z

    .line 205
    const/4 v2, 0x0

    .line 214
    .end local v1    # "headerValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 207
    .restart local v1    # "headerValue":Ljava/lang/String;
    .restart local p0    # "this":Lcom/samsung/http/HTTPPacket;
    :cond_1
    check-cast p0, Lcom/samsung/http/HTTPRequest;

    .end local p0    # "this":Lcom/samsung/http/HTTPPacket;
    const/16 v3, 0x64

    invoke-virtual {p0, v3}, Lcom/samsung/http/HTTPRequest;->returnResponse(I)Z

    goto :goto_0
.end method

.method public hasConnection()Z
    .locals 1

    .prologue
    .line 865
    const-string v0, "Connection"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasContent()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 782
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasFileContent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 784
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    array-length v1, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContentInputStream()Z
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->contentInput:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContentRange()Z
    .locals 1

    .prologue
    .line 904
    const-string v0, "Content-Range"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Range"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasEnoughStorage(J)Z
    .locals 7
    .param p1, "size"    # J

    .prologue
    .line 834
    new-instance v2, Landroid/os/StatFs;

    invoke-static {}, Lcom/samsung/upnp/media/server/ContentDirectory;->getUploadPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 835
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    mul-long v0, v3, v5

    .line 836
    .local v0, "space":J
    cmp-long v3, v0, p1

    if-lez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public hasFileContent()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    return v0
.end method

.method public hasFirstLine()Z
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->firstLine:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeader(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 558
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransferEncoding()Z
    .locals 1

    .prologue
    .line 1067
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/samsung/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->clearHeaders()V

    .line 85
    new-array v0, v1, [B

    invoke-virtual {p0, v0, v1}, Lcom/samsung/http/HTTPPacket;->setContent([BZ)V

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->setContentInputStream(Ljava/io/InputStream;)V

    .line 87
    return-void
.end method

.method public isChunked()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1082
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasTransferEncoding()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1087
    :cond_0
    :goto_0
    return v1

    .line 1084
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getTransferEncoding()Ljava/lang/String;

    move-result-object v0

    .line 1085
    .local v0, "transEnc":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1087
    const-string v1, "Chunked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCloseConnection()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 880
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasConnection()Z

    move-result v2

    if-nez v2, :cond_1

    .line 885
    :cond_0
    :goto_0
    return v1

    .line 882
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getConnection()Ljava/lang/String;

    move-result-object v0

    .line 883
    .local v0, "connection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 885
    const-string v1, "close"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isFileStreamNeeded()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 824
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 825
    .local v0, "sdcard":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 829
    :cond_0
    :goto_0
    return v1

    .line 827
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->isChunked()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getContentLength()J

    move-result-wide v2

    const-wide/32 v4, 0x4b000

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 828
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isKeepAliveConnection()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 890
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasConnection()Z

    move-result v2

    if-nez v2, :cond_1

    .line 895
    :cond_0
    :goto_0
    return v1

    .line 892
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getConnection()Ljava/lang/String;

    move-result-object v0

    .line 893
    .local v0, "connection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 895
    const-string v1, "Keep-Alive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public performReceivedListener(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getHttpServer()Lcom/samsung/http/HTTPServer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getHttpServer()Lcom/samsung/http/HTTPServer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/http/HTTPServer;->performReceivedListener(Ljava/lang/String;)V

    .line 352
    :cond_0
    return-void
.end method

.method public read(Lcom/samsung/http/HTTPSocket;)Z
    .locals 1
    .param p1, "httpSock"    # Lcom/samsung/http/HTTPSocket;

    .prologue
    .line 465
    invoke-virtual {p1}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466
    :cond_0
    const/4 v0, 0x0

    .line 469
    :goto_0
    return v0

    .line 468
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->init()V

    .line 469
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->set(Lcom/samsung/http/HTTPSocket;)Z

    move-result v0

    goto :goto_0
.end method

.method readContentInFile(Lcom/samsung/http/HTTPPacket$DualReader;)V
    .locals 23
    .param p1, "reader"    # Lcom/samsung/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Ljava/net/SocketTimeoutException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPPacket;->isChunked()Z

    move-result v10

    .line 226
    .local v10, "isChunkedRequest":Z
    const-wide/16 v6, 0x0

    .line 228
    .local v6, "contentLen":J
    if-eqz v10, :cond_2

    .line 229
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x10

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v6

    .line 237
    :goto_0
    const/4 v9, 0x0

    .line 238
    .local v9, "fos":Ljava/io/FileOutputStream;
    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/samsung/upnp/media/server/ContentDirectory;->getUploadPath()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/http/HTTPPacket;->rand:Ljava/util/Random;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/Random;->nextLong()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ".tmp"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 239
    .local v19, "tempFile":Ljava/io/File;
    new-instance v9, Ljava/io/FileOutputStream;

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 241
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    .line 242
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    .line 244
    const v5, 0x4b000

    .line 245
    .local v5, "chunkSize":I
    :goto_1
    const-wide/16 v20, 0x0

    cmp-long v20, v20, v6

    if-gez v20, :cond_0

    if-nez v9, :cond_3

    .line 309
    :cond_0
    if-eqz v9, :cond_1

    .line 310
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 311
    const/4 v9, 0x0

    .line 314
    :cond_1
    return-void

    .line 231
    .end local v5    # "chunkSize":I
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v19    # "tempFile":Ljava/io/File;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPPacket;->getContentLength()J

    move-result-wide v6

    goto :goto_0

    .line 246
    .restart local v5    # "chunkSize":I
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "tempFile":Ljava/io/File;
    :cond_3
    new-array v11, v5, [B

    .line 247
    .local v11, "readBuf":[B
    const-wide/16 v12, 0x0

    .line 248
    .local v12, "readCnt":J
    :goto_2
    cmp-long v20, v12, v6

    if-ltz v20, :cond_6

    .line 270
    :cond_4
    if-eqz v10, :cond_d

    .line 272
    const-wide/16 v17, 0x0

    .line 274
    .local v17, "skipLen":J
    :cond_5
    const-wide/16 v15, 0x0

    .line 276
    .local v15, "skipCnt":J
    :try_start_0
    const-string v20, "\r\n"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    sub-long v20, v20, v17

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/http/HTTPPacket$DualReader;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v15

    .line 284
    const-wide/16 v20, 0x0

    cmp-long v20, v15, v20

    if-gez v20, :cond_a

    .line 289
    :goto_3
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x10

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-wide v6

    goto :goto_1

    .line 250
    .end local v15    # "skipCnt":J
    .end local v17    # "skipLen":J
    :cond_6
    sub-long v3, v6, v12

    .line 251
    .local v3, "bufReadLen":J
    int-to-long v0, v5

    move-wide/from16 v20, v0

    cmp-long v20, v20, v3

    if-gez v20, :cond_7

    .line 252
    int-to-long v3, v5

    .line 253
    :cond_7
    const/16 v20, 0x0

    long-to-int v0, v3

    move/from16 v21, v0

    :try_start_2
    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v11, v1, v2}, Lcom/samsung/http/HTTPPacket$DualReader;->read([BII)I

    move-result v14

    .line 254
    .local v14, "readLen":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V

    .line 255
    if-ltz v14, :cond_4

    .line 258
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v11, v0, v14}, Ljava/io/FileOutputStream;->write([BII)V

    .line 259
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 261
    int-to-long v0, v14

    move-wide/from16 v20, v0

    add-long v12, v12, v20

    goto :goto_2

    .line 262
    .end local v14    # "readLen":I
    :catch_0
    move-exception v8

    .line 263
    .local v8, "e":Ljava/io/IOException;
    if-eqz v9, :cond_8

    .line 264
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 265
    const/4 v9, 0x0

    .line 267
    :cond_8
    throw v8

    .line 277
    .end local v3    # "bufReadLen":J
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v15    # "skipCnt":J
    .restart local v17    # "skipLen":J
    :catch_1
    move-exception v8

    .line 278
    .restart local v8    # "e":Ljava/io/IOException;
    if-eqz v9, :cond_9

    .line 279
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 280
    const/4 v9, 0x0

    .line 282
    :cond_9
    throw v8

    .line 286
    .end local v8    # "e":Ljava/io/IOException;
    :cond_a
    add-long v17, v17, v15

    .line 287
    const-string v20, "\r\n"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 273
    cmp-long v20, v17, v20

    if-ltz v20, :cond_5

    goto :goto_3

    .line 290
    :catch_2
    move-exception v8

    .line 291
    .local v8, "e":Ljava/lang/NumberFormatException;
    if-eqz v9, :cond_b

    .line 292
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 293
    const/4 v9, 0x0

    .line 295
    :cond_b
    throw v8

    .line 296
    .end local v8    # "e":Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v8

    .line 297
    .local v8, "e":Ljava/io/IOException;
    if-eqz v9, :cond_c

    .line 298
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 299
    const/4 v9, 0x0

    .line 301
    :cond_c
    throw v8

    .line 306
    .end local v8    # "e":Ljava/io/IOException;
    .end local v15    # "skipCnt":J
    .end local v17    # "skipLen":J
    :cond_d
    const-wide/16 v6, 0x0

    goto/16 :goto_1
.end method

.method readContentInMemory(Lcom/samsung/http/HTTPPacket$DualReader;)V
    .locals 12
    .param p1, "reader"    # Lcom/samsung/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 319
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getContentLength()J

    move-result-wide v3

    .line 322
    .local v3, "contentLen":J
    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-gez v9, :cond_0

    .line 323
    new-instance v9, Ljava/lang/NegativeArraySizeException;

    const-string v10, "Fail to create content buffer"

    invoke-direct {v9, v10}, Ljava/lang/NegativeArraySizeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 324
    :cond_0
    long-to-int v9, v3

    new-array v2, v9, [B

    .line 325
    .local v2, "content":[B
    const-wide/16 v6, 0x0

    .line 326
    .local v6, "readCnt":J
    :goto_0
    cmp-long v9, v6, v3

    if-ltz v9, :cond_2

    .line 341
    :cond_1
    :goto_1
    cmp-long v9, v3, v6

    if-eqz v9, :cond_3

    .line 342
    new-instance v9, Ljava/io/IOException;

    const-string v10, "Fail to get All contents"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 328
    :cond_2
    sub-long v0, v3, v6

    .line 329
    .local v0, "bufReadLen":J
    long-to-int v9, v6

    long-to-int v10, v0

    :try_start_0
    invoke-virtual {p1, v2, v9, v10}, Lcom/samsung/http/HTTPPacket$DualReader;->read([BII)I

    move-result v8

    .line 330
    .local v8, "readLen":I
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    if-ltz v8, :cond_1

    .line 333
    int-to-long v9, v8

    add-long/2addr v6, v9

    goto :goto_0

    .line 335
    .end local v8    # "readLen":I
    :catch_0
    move-exception v5

    .line 337
    .local v5, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 344
    .end local v0    # "bufReadLen":J
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    iput-boolean v11, p0, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    .line 345
    invoke-virtual {p0, v2, v11}, Lcom/samsung/http/HTTPPacket;->setContent([BZ)V

    .line 346
    return-void
.end method

.method readHeaders(Lcom/samsung/http/HTTPPacket$DualReader;)Z
    .locals 4
    .param p1, "reader"    # Lcom/samsung/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/samsung/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "firstLineOfPacket":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_0

    .line 173
    const/4 v3, 0x0

    .line 185
    :goto_0
    return v3

    .line 175
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p1}, Lcom/samsung/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, "headerLine":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_1

    .line 185
    const/4 v3, 0x1

    goto :goto_0

    .line 179
    :cond_1
    new-instance v1, Lcom/samsung/http/HTTPHeader;

    invoke-direct {v1, v2}, Lcom/samsung/http/HTTPHeader;-><init>(Ljava/lang/String;)V

    .line 180
    .local v1, "header":Lcom/samsung/http/HTTPHeader;
    invoke-virtual {v1}, Lcom/samsung/http/HTTPHeader;->hasName()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 181
    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPPacket;->setHeader(Lcom/samsung/http/HTTPHeader;)V

    .line 182
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public removeContent()V
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->removeTempFile()V

    .line 371
    :goto_0
    return-void

    .line 370
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    goto :goto_0
.end method

.method removeTempFile()V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    .line 375
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 377
    :cond_0
    return-void
.end method

.method protected set(Lcom/samsung/http/HTTPPacket;)V
    .locals 4
    .param p1, "httpPacket"    # Lcom/samsung/http/HTTPPacket;

    .prologue
    .line 446
    invoke-virtual {p1}, Lcom/samsung/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 448
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->clearHeaders()V

    .line 449
    invoke-virtual {p1}, Lcom/samsung/http/HTTPPacket;->getNHeaders()I

    move-result v2

    .line 450
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 455
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->setContent(Lcom/samsung/http/HTTPPacket;)V

    .line 456
    return-void

    .line 451
    :cond_0
    invoke-virtual {p1, v1}, Lcom/samsung/http/HTTPPacket;->getHeader(I)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 452
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->addHeader(Lcom/samsung/http/HTTPHeader;)V

    .line 450
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected set(Lcom/samsung/http/HTTPSocket;)Z
    .locals 1
    .param p1, "httpSock"    # Lcom/samsung/http/HTTPSocket;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->httpSock:Lcom/samsung/http/HTTPSocket;

    .line 441
    invoke-virtual {p1}, Lcom/samsung/http/HTTPSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->set(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method protected set(Ljava/io/InputStream;)Z
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 435
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/http/HTTPPacket;->set(Ljava/io/InputStream;Z)Z

    move-result v0

    return v0
.end method

.method protected set(Ljava/io/InputStream;Z)Z
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "onlyHeaders"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 383
    :try_start_0
    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->reader:Lcom/samsung/http/HTTPPacket$DualReader;

    if-nez v3, :cond_1

    .line 384
    new-instance v3, Lcom/samsung/http/HTTPPacket$DualReader;

    invoke-direct {v3, p1}, Lcom/samsung/http/HTTPPacket$DualReader;-><init>(Ljava/io/InputStream;)V

    iput-object v3, p0, Lcom/samsung/http/HTTPPacket;->reader:Lcom/samsung/http/HTTPPacket$DualReader;

    .line 388
    :goto_0
    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->reader:Lcom/samsung/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/samsung/http/HTTPPacket;->readHeaders(Lcom/samsung/http/HTTPPacket$DualReader;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 430
    :cond_0
    :goto_1
    return v1

    .line 386
    :cond_1
    const-string v3, "Something Left. Read input stream again!!!"

    invoke-static {v3}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 409
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 411
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 412
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->removeTempFile()V

    goto :goto_1

    .line 391
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_2
    if-eqz p2, :cond_3

    .line 392
    :try_start_1
    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/samsung/http/HTTPPacket;->setContent(Ljava/lang/String;Z)V

    move v1, v2

    .line 393
    goto :goto_1

    .line 397
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V

    .line 400
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->handleExpect100Continue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->isFileStreamNeeded()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 405
    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->reader:Lcom/samsung/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/samsung/http/HTTPPacket;->readContentInFile(Lcom/samsung/http/HTTPPacket$DualReader;)V

    :goto_2
    move v1, v2

    .line 430
    goto :goto_1

    .line 407
    :cond_4
    iget-object v3, p0, Lcom/samsung/http/HTTPPacket;->reader:Lcom/samsung/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/samsung/http/HTTPPacket;->readContentInMemory(Lcom/samsung/http/HTTPPacket$DualReader;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    .line 414
    :catch_1
    move-exception v0

    .line 415
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 416
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 417
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->removeTempFile()V

    goto :goto_1

    .line 419
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 420
    .local v0, "e":Ljava/lang/NegativeArraySizeException;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 421
    invoke-virtual {v0}, Ljava/lang/NegativeArraySizeException;->printStackTrace()V

    .line 422
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->removeTempFile()V

    goto :goto_1

    .line 424
    .end local v0    # "e":Ljava/lang/NegativeArraySizeException;
    :catch_3
    move-exception v0

    .line 425
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 426
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 427
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->removeTempFile()V

    goto :goto_1
.end method

.method public setCacheControl(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1006
    const-string v0, "max-age"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/http/HTTPPacket;->setCacheControl(Ljava/lang/String;I)V

    .line 1007
    return-void
.end method

.method public setCacheControl(Ljava/lang/String;I)V
    .locals 3
    .param p1, "directive"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1000
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1001
    .local v0, "strVal":Ljava/lang/String;
    const-string v1, "Cache-Control"

    invoke-virtual {p0, v1, v0}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    return-void
.end method

.method public setConnection(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 870
    const-string v0, "Connection"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    return-void
.end method

.method public setContent(Lcom/samsung/http/HTTPPacket;)V
    .locals 2
    .param p1, "packet"    # Lcom/samsung/http/HTTPPacket;

    .prologue
    .line 690
    if-nez p1, :cond_0

    .line 696
    :goto_0
    return-void

    .line 692
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 693
    iget-object v0, p1, Lcom/samsung/http/HTTPPacket;->content:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/http/HTTPPacket;->setContent([BZ)V

    goto :goto_0

    .line 695
    :cond_1
    iget-object v0, p1, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPPacket;->setContent(Ljava/io/File;)V

    goto :goto_0
.end method

.method public setContent(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 704
    invoke-virtual {p0}, Lcom/samsung/http/HTTPPacket;->removeTempFile()V

    .line 705
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->fileContent:Ljava/io/File;

    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    .line 707
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    .line 708
    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 725
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/http/HTTPPacket;->setContent(Ljava/lang/String;Z)V

    .line 726
    return-void
.end method

.method public setContent(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "updateWithContentLength"    # Z

    .prologue
    .line 714
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/samsung/http/HTTPPacket;->setContent([BZ)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 721
    :goto_0
    return-void

    .line 715
    :catch_0
    move-exception v0

    .line 716
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/samsung/http/HTTPPacket;->setContent([BZ)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 718
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 719
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "HTTP Packet"

    const-string v2, "Set Content data NullPointerException"

    invoke-static {v1, v2}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setContent([B)V
    .locals 1
    .param p1, "content"    # [B

    .prologue
    .line 700
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/http/HTTPPacket;->setContent([BZ)V

    .line 701
    return-void
.end method

.method public setContent([BZ)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "updateWithContentLength"    # Z

    .prologue
    .line 682
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->content:[B

    .line 683
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/http/HTTPPacket;->bFileContent:Z

    .line 684
    if-eqz p2, :cond_0

    .line 685
    array-length v0, p1

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/http/HTTPPacket;->setContentLength(J)V

    .line 686
    :cond_0
    return-void
.end method

.method public setContentInputStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 794
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->contentInput:Ljava/io/InputStream;

    .line 795
    return-void
.end method

.method public setContentLength(J)V
    .locals 1
    .param p1, "len"    # J

    .prologue
    .line 851
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/http/HTTPPacket;->setLongHeader(Ljava/lang/String;J)V

    .line 852
    return-void
.end method

.method public setContentRange(JJJ)V
    .locals 5
    .param p1, "firstPos"    # J
    .param p3, "lastPos"    # J
    .param p5, "length"    # J

    .prologue
    .line 909
    const-string v0, ""

    .line 910
    .local v0, "rangeStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "bytes "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 911
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 912
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 913
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    cmp-long v1, v3, p5

    if-gez v1, :cond_0

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 914
    const-string v1, "Content-Range"

    invoke-virtual {p0, v1, v0}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    return-void

    .line 913
    :cond_0
    const-string v1, "*"

    goto :goto_0
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 813
    const-string v0, "Content-Type"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    return-void
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 3
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 1052
    new-instance v0, Lcom/samsung/http/Date;

    invoke-direct {v0, p1}, Lcom/samsung/http/Date;-><init>(Ljava/util/Calendar;)V

    .line 1053
    .local v0, "date":Lcom/samsung/http/Date;
    const-string v1, "Date"

    invoke-virtual {v0}, Lcom/samsung/http/Date;->getDateString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    return-void
.end method

.method public setHeader(Lcom/samsung/http/HTTPHeader;)V
    .locals 2
    .param p1, "header"    # Lcom/samsung/http/HTTPHeader;

    .prologue
    .line 583
    if-nez p1, :cond_0

    .line 586
    :goto_0
    return-void

    .line 585
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 563
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 564
    .local v0, "header":Lcom/samsung/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 565
    invoke-virtual {v0, p2}, Lcom/samsung/http/HTTPHeader;->setValue(Ljava/lang/String;)V

    .line 569
    :goto_0
    return-void

    .line 568
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/http/HTTPPacket;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHost(Ljava/lang/String;I)V
    .locals 4
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 1034
    move-object v0, p1

    .line 1035
    .local v0, "hostAddr":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/net/HostInterface;->isIPv6Address(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1036
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1037
    :cond_0
    const-string v1, "HOST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    return-void
.end method

.method public setHttpServer(Lcom/samsung/http/HTTPServer;)V
    .locals 0
    .param p1, "server"    # Lcom/samsung/http/HTTPServer;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->httpServer:Lcom/samsung/http/HTTPServer;

    .line 112
    return-void
.end method

.method public setLongHeader(Ljava/lang/String;J)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 641
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    return-void
.end method

.method public setServer(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1020
    const-string v0, "Server"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    return-void
.end method

.method public setStringHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 614
    const-string v0, "\""

    const-string v1, "\""

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/samsung/http/HTTPPacket;->setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    return-void
.end method

.method public setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "startWidth"    # Ljava/lang/String;
    .param p4, "endWidth"    # Ljava/lang/String;

    .prologue
    .line 602
    if-nez p2, :cond_0

    .line 610
    :goto_0
    return-void

    .line 604
    :cond_0
    move-object v0, p2

    .line 605
    .local v0, "headerValue":Ljava/lang/String;
    invoke-virtual {v0, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 606
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 607
    :cond_1
    invoke-virtual {v0, p4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 608
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 609
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/samsung/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "ver"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/http/HTTPPacket;->version:Ljava/lang/String;

    .line 98
    return-void
.end method
