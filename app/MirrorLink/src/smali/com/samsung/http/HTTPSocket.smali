.class public Lcom/samsung/http/HTTPSocket;
.super Ljava/lang/Object;
.source "HTTPSocket.java"


# instance fields
.field private sockIn:Ljava/io/InputStream;

.field private sockOut:Ljava/io/BufferedOutputStream;

.field private socket:Ljava/net/Socket;


# direct methods
.method public constructor <init>(Ljava/net/Socket;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v0, p0, Lcom/samsung/http/HTTPSocket;->socket:Ljava/net/Socket;

    .line 90
    iput-object v0, p0, Lcom/samsung/http/HTTPSocket;->sockIn:Ljava/io/InputStream;

    .line 91
    iput-object v0, p0, Lcom/samsung/http/HTTPSocket;->sockOut:Ljava/io/BufferedOutputStream;

    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/http/HTTPSocket;->setSocket(Ljava/net/Socket;)V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/http/HTTPSocket;->open()Z

    .line 48
    return-void
.end method

.method private getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/http/HTTPSocket;->sockOut:Ljava/io/BufferedOutputStream;

    return-object v0
.end method

.method private post(Lcom/samsung/http/HTTPResponse;Ljava/io/InputStream;JJZ)Z
    .locals 21
    .param p1, "httpRes"    # Lcom/samsung/http/HTTPResponse;
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "contentOffset"    # J
    .param p5, "contentLength"    # J
    .param p7, "isOnlyHeader"    # Z

    .prologue
    .line 206
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 207
    :cond_0
    const/16 v17, 0x0

    .line 279
    :goto_0
    return v17

    .line 208
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setDate(Ljava/util/Calendar;)V

    .line 209
    invoke-direct/range {p0 .. p0}, Lcom/samsung/http/HTTPSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    .line 211
    .local v8, "out":Ljava/io/OutputStream;
    const-wide/16 v10, 0x0

    .line 213
    .local v10, "readCnt":J
    :try_start_0
    move-object/from16 v0, p1

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 214
    const-string v17, "\r\n"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 216
    .local v4, "crlf":[B
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/http/HTTPResponse;->getHeader()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/io/OutputStream;->write([B)V

    .line 217
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 219
    if-eqz p7, :cond_2

    .line 220
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 273
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "input stream closed : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 221
    :goto_1
    const/16 v17, 0x1

    goto :goto_0

    .line 274
    :catch_0
    move-exception v5

    .line 275
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 224
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/http/HTTPResponse;->isChunked()Z

    move-result v7

    .line 226
    .local v7, "isChunkedResponse":Z
    const-wide/16 v17, 0x0

    cmp-long v17, v17, p3

    if-gez v17, :cond_4

    .line 227
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "content offset size : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 228
    const-wide/32 v17, 0x7fffffff

    cmp-long v17, p3, v17

    if-lez v17, :cond_3

    .line 272
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 273
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "input stream closed : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 229
    :goto_2
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 274
    :catch_1
    move-exception v5

    .line 275
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 231
    .end local v5    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_4
    invoke-virtual/range {p2 .. p4}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v15

    .line 232
    .local v15, "skipped":J
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "skipped : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    .end local v15    # "skipped":J
    :cond_4
    const v3, 0x4b000

    .line 237
    .local v3, "chunkSize":I
    new-array v9, v3, [B

    .line 238
    .local v9, "readBuf":[B
    const-wide/16 v10, 0x0

    .line 239
    int-to-long v0, v3

    move-wide/from16 v17, v0

    cmp-long v17, v17, p5

    if-gez v17, :cond_7

    int-to-long v13, v3

    .line 240
    .local v13, "readSize":J
    :goto_3
    const/16 v17, 0x0

    long-to-int v0, v13

    move/from16 v18, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v9, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v12

    .line 241
    .local v12, "readLen":I
    :goto_4
    if-lez v12, :cond_5

    cmp-long v17, v10, p5

    if-ltz v17, :cond_8

    .line 254
    :cond_5
    if-eqz v7, :cond_6

    .line 255
    const-string v17, "0"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/io/OutputStream;->write([B)V

    .line 256
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 258
    :cond_6
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 272
    :try_start_5
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 273
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "input stream closed : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    .line 279
    .end local v3    # "chunkSize":I
    .end local v4    # "crlf":[B
    .end local v7    # "isChunkedResponse":Z
    .end local v9    # "readBuf":[B
    .end local v12    # "readLen":I
    .end local v13    # "readSize":J
    :goto_5
    const/16 v17, 0x1

    goto/16 :goto_0

    .restart local v3    # "chunkSize":I
    .restart local v4    # "crlf":[B
    .restart local v7    # "isChunkedResponse":Z
    .restart local v9    # "readBuf":[B
    :cond_7
    move-wide/from16 v13, p5

    .line 239
    goto :goto_3

    .line 242
    .restart local v12    # "readLen":I
    .restart local v13    # "readSize":J
    :cond_8
    if-eqz v7, :cond_9

    .line 244
    int-to-long v0, v12

    move-wide/from16 v17, v0

    :try_start_6
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/io/OutputStream;->write([B)V

    .line 245
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 247
    :cond_9
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v8, v9, v0, v12}, Ljava/io/OutputStream;->write([BII)V

    .line 248
    if-eqz v7, :cond_a

    .line 249
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 250
    :cond_a
    int-to-long v0, v12

    move-wide/from16 v17, v0

    add-long v10, v10, v17

    .line 251
    int-to-long v0, v3

    move-wide/from16 v17, v0

    sub-long v19, p5, v10

    cmp-long v17, v17, v19

    if-gez v17, :cond_b

    int-to-long v13, v3

    .line 252
    :goto_6
    const/16 v17, 0x0

    long-to-int v0, v13

    move/from16 v18, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v9, v1, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v12

    goto :goto_4

    .line 251
    :cond_b
    sub-long v13, p5, v10

    goto :goto_6

    .line 260
    .end local v3    # "chunkSize":I
    .end local v4    # "crlf":[B
    .end local v7    # "isChunkedResponse":Z
    .end local v9    # "readBuf":[B
    .end local v12    # "readLen":I
    .end local v13    # "readSize":J
    :catch_2
    move-exception v5

    .line 261
    .local v5, "e":Ljava/lang/Exception;
    :try_start_7
    const-string v17, "HTTPSocket"

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 263
    :try_start_8
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 267
    :goto_7
    :try_start_9
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_c

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    const-string v18, "broken pipe"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v17

    if-eqz v17, :cond_c

    .line 272
    :try_start_a
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 273
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "input stream closed : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 268
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_8
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 264
    .restart local v5    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v6

    .line 265
    .local v6, "e1":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_7

    .line 270
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    .line 272
    :try_start_c
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 273
    const-string v18, "HTTPSocket"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "input stream closed : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 277
    :goto_9
    throw v17

    .line 274
    .restart local v5    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 275
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 272
    .local v5, "e":Ljava/lang/Exception;
    :cond_c
    :try_start_d
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 273
    const-string v17, "HTTPSocket"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "input stream closed : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5

    goto/16 :goto_5

    .line 274
    :catch_5
    move-exception v5

    .line 275
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 274
    .end local v5    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v5

    .line 275
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 274
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "chunkSize":I
    .restart local v4    # "crlf":[B
    .restart local v7    # "isChunkedResponse":Z
    .restart local v9    # "readBuf":[B
    .restart local v12    # "readLen":I
    .restart local v13    # "readSize":J
    :catch_7
    move-exception v5

    .line 275
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5
.end method

.method private post(Lcom/samsung/http/HTTPResponse;[BJJZ)Z
    .locals 8
    .param p1, "httpRes"    # Lcom/samsung/http/HTTPResponse;
    .param p2, "content"    # [B
    .param p3, "contentOffset"    # J
    .param p5, "contentLength"    # J
    .param p7, "isOnlyHeader"    # Z

    .prologue
    const/4 v5, 0x1

    .line 161
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/samsung/http/HTTPResponse;->setDate(Ljava/util/Calendar;)V

    .line 162
    invoke-direct {p0}, Lcom/samsung/http/HTTPSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 165
    .local v4, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p1, p5, p6}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 167
    invoke-virtual {p1}, Lcom/samsung/http/HTTPResponse;->getHeader()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 168
    const-string v6, "\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 169
    if-nez p7, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v6, p5, v6

    if-nez v6, :cond_1

    .line 170
    :cond_0
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 200
    :goto_0
    return v5

    .line 174
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPResponse;->isChunked()Z

    move-result v3

    .line 176
    .local v3, "isChunkedResponse":Z
    if-eqz v3, :cond_2

    .line 177
    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "chunSizeBuf":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 179
    const-string v6, "\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 182
    .end local v0    # "chunSizeBuf":Ljava/lang/String;
    :cond_2
    long-to-int v6, p3

    long-to-int v7, p5

    invoke-virtual {v4, p2, v6, v7}, Ljava/io/OutputStream;->write([BII)V

    .line 184
    if-eqz v3, :cond_3

    .line 185
    const-string v6, "\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 186
    const-string v6, "0"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 187
    const-string v6, "\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/OutputStream;->write([B)V

    .line 189
    :cond_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 191
    .end local v3    # "isChunkedResponse":Z
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 194
    :try_start_1
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 198
    :goto_1
    const/4 v5, 0x0

    goto :goto_0

    .line 195
    :catch_1
    move-exception v2

    .line 196
    .local v2, "e2":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private setSocket(Ljava/net/Socket;)V
    .locals 0
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/http/HTTPSocket;->socket:Ljava/net/Socket;

    .line 66
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 135
    iget-object v2, p0, Lcom/samsung/http/HTTPSocket;->socket:Ljava/net/Socket;

    if-nez v2, :cond_0

    .line 152
    :goto_0
    return v1

    .line 139
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/Socket;->shutdownOutput()V

    .line 140
    invoke-virtual {p0}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/Socket;->shutdownInput()V

    .line 141
    iget-object v2, p0, Lcom/samsung/http/HTTPSocket;->sockIn:Ljava/io/InputStream;

    if-eqz v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/samsung/http/HTTPSocket;->sockIn:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 143
    :cond_1
    iget-object v2, p0, Lcom/samsung/http/HTTPSocket;->sockOut:Ljava/io/BufferedOutputStream;

    if-eqz v2, :cond_2

    .line 144
    iget-object v2, p0, Lcom/samsung/http/HTTPSocket;->sockOut:Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 145
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/Socket;->close()V

    .line 146
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/http/HTTPSocket;->setSocket(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/http/HTTPSocket;->sockIn:Ljava/io/InputStream;

    return-object v0
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/http/HTTPSocket;->socket:Ljava/net/Socket;

    return-object v0
.end method

.method public open()Z
    .locals 5

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/samsung/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v1

    .line 124
    .local v1, "sock":Ljava/net/Socket;
    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/http/HTTPSocket;->sockIn:Ljava/io/InputStream;

    .line 125
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/Socket;->getSendBufferSize()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v2, p0, Lcom/samsung/http/HTTPSocket;->sockOut:Ljava/io/BufferedOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public post(Lcom/samsung/http/HTTPResponse;JJZ)Z
    .locals 8
    .param p1, "httpRes"    # Lcom/samsung/http/HTTPResponse;
    .param p2, "contentOffset"    # J
    .param p4, "contentLength"    # J
    .param p6, "isOnlyHeader"    # Z

    .prologue
    .line 360
    invoke-virtual {p1}, Lcom/samsung/http/HTTPResponse;->hasContentInputStream()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/samsung/http/HTTPResponse;->getContentInputStream()Ljava/io/InputStream;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-wide v5, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/http/HTTPSocket;->post(Lcom/samsung/http/HTTPResponse;Ljava/io/InputStream;JJZ)Z

    move-result v0

    .line 362
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPResponse;->getContent()Ljava/io/InputStream;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-wide v5, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/http/HTTPSocket;->post(Lcom/samsung/http/HTTPResponse;Ljava/io/InputStream;JJZ)Z

    move-result v0

    goto :goto_0
.end method
