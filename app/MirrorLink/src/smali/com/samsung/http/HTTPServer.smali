.class public Lcom/samsung/http/HTTPServer;
.super Ljava/lang/Object;
.source "HTTPServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final DEFAULT_PORT:I = 0x50

.field public static final NAME:Ljava/lang/String; = "DoaHTTP"

.field public static final VERSION:Ljava/lang/String; = "1.1"

.field private static faultCount:I


# instance fields
.field private bindAddr:Ljava/net/InetAddress;

.field private bindPort:I

.field private httpReceivedListenerList:Lcom/samsung/util/ListenerList;

.field private httpRequestListenerList:Lcom/samsung/util/ListenerList;

.field private httpServerThread:Ljava/lang/Thread;

.field private serverSock:Ljava/net/ServerSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    sput v0, Lcom/samsung/http/HTTPServer;->faultCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object v1, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    .line 83
    iput-object v1, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    .line 210
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/http/HTTPServer;->httpRequestListenerList:Lcom/samsung/util/ListenerList;

    .line 211
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/http/HTTPServer;->httpReceivedListenerList:Lcom/samsung/util/ListenerList;

    .line 271
    iput-object v1, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    .line 75
    iput-object v1, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    .line 76
    return-void
.end method

.method public static getName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 64
    const-string v2, "os.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "osName":Ljava/lang/String;
    const-string v2, "os.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "osVer":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "DoaHTTP"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public accept()Ljava/net/Socket;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 172
    const/4 v2, 0x0

    .line 173
    .local v2, "sock":Ljava/net/Socket;
    iget-object v4, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    if-nez v4, :cond_0

    .line 193
    :goto_0
    return-object v3

    .line 176
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    .line 177
    const v4, 0x88b8

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 178
    const v4, 0x4b000

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setReceiveBufferSize(I)V

    .line 179
    const v4, 0x4b000

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSendBufferSize(I)V

    .line 180
    const/4 v4, 0x1

    const/16 v5, 0x1e

    invoke-virtual {v2, v4, v5}, Ljava/net/Socket;->setSoLinger(ZI)V

    .line 181
    const-string v4, "HTTPServer"

    invoke-virtual {v2}, Ljava/net/Socket;->getSendBufferSize()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v2

    .line 182
    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    if-eqz v2, :cond_1

    .line 187
    :try_start_1
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 192
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :catch_1
    move-exception v1

    .line 190
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public addReceivedListener(Lcom/samsung/http/HTTPReceivedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/http/HTTPReceivedListener;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpReceivedListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 247
    return-void
.end method

.method public addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/http/HTTPRequestListener;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpRequestListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public close()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 150
    iget-object v3, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    if-nez v3, :cond_0

    .line 163
    :goto_0
    return v1

    .line 153
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    iput-object v4, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    .line 160
    iput-object v4, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    .line 161
    iput v2, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    iput-object v4, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    .line 160
    iput-object v4, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    .line 161
    iput v2, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    move v1, v2

    .line 157
    goto :goto_0

    .line 158
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    .line 159
    iput-object v4, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    .line 160
    iput-object v4, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    .line 161
    iput v2, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    .line 162
    throw v1
.end method

.method public getBindAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    if-nez v0, :cond_0

    .line 102
    const-string v0, ""

    .line 103
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public open(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    const/4 v1, 0x1

    .line 127
    iget-object v2, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    if-eqz v2, :cond_0

    .line 141
    :goto_0
    return v1

    .line 130
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    .line 131
    iput p2, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    .line 134
    new-instance v2, Ljava/net/ServerSocket;

    iget v3, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    const/16 v4, 0x200

    iget-object v5, p0, Lcom/samsung/http/HTTPServer;->bindAddr:Ljava/net/InetAddress;

    invoke-direct {v2, v3, v4, v5}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v2, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    .line 135
    iget-object v2, p0, Lcom/samsung/http/HTTPServer;->serverSock:Ljava/net/ServerSocket;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "HTTPServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performReceivedListener(Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 260
    iget-object v3, p0, Lcom/samsung/http/HTTPServer;->httpReceivedListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/util/ListenerList;->size()I

    move-result v1

    .line 261
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 265
    return-void

    .line 262
    :cond_0
    iget-object v3, p0, Lcom/samsung/http/HTTPServer;->httpReceivedListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/http/HTTPReceivedListener;

    .line 263
    .local v0, "listener":Lcom/samsung/http/HTTPReceivedListener;
    invoke-interface {v0, p1}, Lcom/samsung/http/HTTPReceivedListener;->httptRecieved(Ljava/lang/String;)V

    .line 261
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public performRequestListener(Lcom/samsung/http/HTTPRequest;)V
    .locals 4
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 233
    iget-object v3, p0, Lcom/samsung/http/HTTPServer;->httpRequestListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/util/ListenerList;->size()I

    move-result v1

    .line 234
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 238
    return-void

    .line 235
    :cond_0
    iget-object v3, p0, Lcom/samsung/http/HTTPServer;->httpRequestListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/http/HTTPRequestListener;

    .line 236
    .local v0, "listener":Lcom/samsung/http/HTTPRequestListener;
    invoke-interface {v0, p1}, Lcom/samsung/http/HTTPRequestListener;->httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    .line 234
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public removeReceivedListener(Lcom/samsung/http/HTTPReceivedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/http/HTTPReceivedListener;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpReceivedListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 252
    return-void
.end method

.method public removeRequestListener(Lcom/samsung/http/HTTPRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/http/HTTPRequestListener;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpRequestListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 225
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const/16 v1, 0x20

    .line 276
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServer;->isOpened()Z

    move-result v2

    if-nez v2, :cond_0

    .line 338
    :goto_0
    return-void

    .line 280
    :cond_0
    new-instance v6, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v2, 0xa

    invoke-direct {v6, v2}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    .line 281
    .local v6, "queue":Ljava/util/concurrent/ArrayBlockingQueue;, "Ljava/util/concurrent/ArrayBlockingQueue<Ljava/lang/Runnable;>;"
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x23

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move v2, v1

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 282
    .local v0, "exeutor":Ljava/util/concurrent/ThreadPoolExecutor;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 284
    .local v9, "socks":Ljava/util/List;, "Ljava/util/List<Ljava/net/Socket;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    .line 285
    .local v10, "thisThread":Ljava/lang/Thread;
    const-string v1, "http server started!!!!"

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 286
    const-string v1, "UPNP"

    const-string v2, "http server started!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    if-eq v1, v10, :cond_2

    .line 323
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 333
    const-string v1, "UPNP"

    const-string v2, "shutting down now"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 335
    const-string v1, "UPNP"

    const-string v2, "http server stopped!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const-string v1, "http server stopped!!!!"

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    goto :goto_0

    .line 289
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 293
    :try_start_0
    const-string v1, "accept ..."

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 294
    :goto_3
    sget v1, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    const/16 v2, 0x64

    if-gt v1, v2, :cond_4

    .line 298
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServer;->accept()Ljava/net/Socket;

    move-result-object v8

    .line 300
    .local v8, "sock":Ljava/net/Socket;
    if-eqz v8, :cond_3

    .line 301
    sget v1, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    .line 302
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Accept success : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 308
    new-instance v1, Lcom/samsung/http/HTTPServerThread;

    invoke-direct {v1, p0, v8}, Lcom/samsung/http/HTTPServerThread;-><init>(Lcom/samsung/http/HTTPServer;Ljava/net/Socket;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 309
    const-string v1, "httpServThread ..."

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 311
    :cond_3
    if-nez v8, :cond_1

    .line 312
    const-string v1, "HTTPServer class"

    const-string v2, "accept return socket is null!!!!"

    invoke-static {v1, v2}, Lcom/samsung/api/Debugs;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 316
    .end local v8    # "sock":Ljava/net/Socket;
    :catch_0
    move-exception v1

    goto :goto_1

    .line 295
    :cond_4
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 323
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/net/Socket;

    .line 326
    .restart local v8    # "sock":Ljava/net/Socket;
    :try_start_1
    invoke-virtual {v8}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 328
    :catch_1
    move-exception v7

    .line 330
    .local v7, "e":Ljava/lang/Exception;
    goto/16 :goto_0
.end method

.method public declared-synchronized start()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 342
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    .line 343
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTTP Accept Thread"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/http/HTTPServer;->getBindAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/http/HTTPServer;->bindPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 347
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 348
    iget-object v0, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    monitor-exit p0

    return v3

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()Z
    .locals 1

    .prologue
    .line 356
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/http/HTTPServer;->httpServerThread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
