.class public Lcom/samsung/http/HTTPServerThread;
.super Ljava/lang/Thread;
.source "HTTPServerThread.java"


# static fields
.field static final MAX_SERVER_SOCKET:I = 0x64

.field static usedServerSocket:I


# instance fields
.field private httpServer:Lcom/samsung/http/HTTPServer;

.field private sock:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput v0, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/samsung/http/HTTPServer;Ljava/net/Socket;)V
    .locals 2
    .param p1, "httpServer"    # Lcom/samsung/http/HTTPServer;
    .param p2, "sock"    # Ljava/net/Socket;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/samsung/http/HTTPServerThread;->httpServer:Lcom/samsung/http/HTTPServer;

    .line 56
    iput-object p2, p0, Lcom/samsung/http/HTTPServerThread;->sock:Ljava/net/Socket;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTTP Thread for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/net/Socket;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPServerThread;->setName(Ljava/lang/String;)V

    .line 58
    const-string v0, "HTTPServerThread Start"

    invoke-static {v0}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 59
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 69
    new-instance v2, Lcom/samsung/http/HTTPSocket;

    iget-object v4, p0, Lcom/samsung/http/HTTPServerThread;->sock:Ljava/net/Socket;

    invoke-direct {v2, v4}, Lcom/samsung/http/HTTPSocket;-><init>(Ljava/net/Socket;)V

    .line 70
    .local v2, "httpSock":Lcom/samsung/http/HTTPSocket;
    new-instance v1, Lcom/samsung/http/HTTPRequest;

    invoke-direct {v1}, Lcom/samsung/http/HTTPRequest;-><init>()V

    .line 71
    .local v1, "httpReq":Lcom/samsung/http/HTTPRequest;
    invoke-virtual {v1, v2}, Lcom/samsung/http/HTTPRequest;->setSocket(Lcom/samsung/http/HTTPSocket;)V

    .line 72
    iget-object v4, p0, Lcom/samsung/http/HTTPServerThread;->httpServer:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPRequest;->setHttpServer(Lcom/samsung/http/HTTPServer;)V

    .line 74
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/http/HTTPRequest;->read()Z

    move-result v4

    if-nez v4, :cond_2

    .line 81
    :goto_0
    invoke-virtual {v1}, Lcom/samsung/http/HTTPRequest;->hasFileContent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 82
    invoke-virtual {v1}, Lcom/samsung/http/HTTPRequest;->getFileContent()Ljava/io/File;

    move-result-object v3

    .line 83
    .local v3, "tempFile":Ljava/io/File;
    if-eqz v3, :cond_1

    .line 84
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 87
    .end local v3    # "tempFile":Ljava/io/File;
    :cond_1
    const-string v4, "HTTPSocket"

    const-string v5, "Server Thread : Socket Close"

    invoke-static {v4, v5}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-wide/16 v4, 0x14

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_1
    invoke-virtual {v2}, Lcom/samsung/http/HTTPSocket;->close()Z

    .line 94
    const/4 v2, 0x0

    .line 95
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/http/HTTPServerThread;->sock:Ljava/net/Socket;

    .line 97
    sget v4, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    add-int/lit8 v4, v4, -0x1

    sput v4, Lcom/samsung/http/HTTPServerThread;->usedServerSocket:I

    .line 98
    return-void

    .line 76
    :cond_2
    iget-object v4, p0, Lcom/samsung/http/HTTPServerThread;->httpServer:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v4, v1}, Lcom/samsung/http/HTTPServer;->performRequestListener(Lcom/samsung/http/HTTPRequest;)V

    .line 77
    invoke-virtual {v1}, Lcom/samsung/http/HTTPRequest;->isKeepAlive()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
