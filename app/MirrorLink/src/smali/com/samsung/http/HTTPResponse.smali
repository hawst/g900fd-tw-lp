.class public Lcom/samsung/http/HTTPResponse;
.super Lcom/samsung/http/HTTPPacket;
.source "HTTPResponse.java"


# instance fields
.field private statusCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/samsung/http/HTTPPacket;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/http/HTTPResponse;->statusCode:I

    .line 57
    const-string v0, "text/html; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/samsung/http/HTTPServer;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPResponse;->setServer(Ljava/lang/String;)V

    .line 59
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPResponse;->setContent(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/samsung/http/HTTPResponse;)V
    .locals 1
    .param p1, "httpRes"    # Lcom/samsung/http/HTTPResponse;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/http/HTTPPacket;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/http/HTTPResponse;->statusCode:I

    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPResponse;->set(Lcom/samsung/http/HTTPPacket;)V

    .line 65
    return-void
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 114
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getStatusLineString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 115
    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getHeaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 117
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getStatusCode()I
    .locals 2

    .prologue
    .line 90
    iget v1, p0, Lcom/samsung/http/HTTPResponse;->statusCode:I

    if-eqz v1, :cond_0

    .line 91
    iget v1, p0, Lcom/samsung/http/HTTPResponse;->statusCode:I

    .line 93
    :goto_0
    return v1

    .line 92
    :cond_0
    new-instance v0, Lcom/samsung/http/HTTPStatus;

    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getFirstLine()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/http/HTTPStatus;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "httpStatus":Lcom/samsung/http/HTTPStatus;
    invoke-virtual {v0}, Lcom/samsung/http/HTTPStatus;->getStatusCode()I

    move-result v1

    goto :goto_0
.end method

.method public getStatusLineString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTTP/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getStatusCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/http/HTTPResponse;->statusCode:I

    invoke-static {v1}, Lcom/samsung/http/HTTPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSuccessful()Z
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getStatusCode()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/http/HTTPStatus;->isSuccessful(I)Z

    move-result v0

    return v0
.end method

.method public print()V
    .locals 2

    .prologue
    .line 138
    const-string v0, "HTTPResponse"

    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/http/HTTPResponse;->statusCode:I

    .line 86
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 128
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getStatusLineString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getHeaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    invoke-virtual {p0}, Lcom/samsung/http/HTTPResponse;->getContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 133
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
