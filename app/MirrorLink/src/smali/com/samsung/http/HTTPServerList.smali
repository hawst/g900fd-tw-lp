.class public Lcom/samsung/http/HTTPServerList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "HTTPServerList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/http/HTTPServer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 55
    return-void
.end method

.method private getHttpServer()Lcom/samsung/http/HTTPServer;
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/samsung/http/HTTPServer;

    invoke-direct {v0}, Lcom/samsung/http/HTTPServer;-><init>()V

    return-object v0
.end method


# virtual methods
.method public addReceivedListener(Lcom/samsung/http/HTTPReceivedListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/http/HTTPReceivedListener;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->size()I

    move-result v1

    .line 73
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 77
    return-void

    .line 74
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/http/HTTPServer;

    move-result-object v2

    .line 75
    .local v2, "server":Lcom/samsung/http/HTTPServer;
    invoke-virtual {v2, p1}, Lcom/samsung/http/HTTPServer;->addReceivedListener(Lcom/samsung/http/HTTPReceivedListener;)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/http/HTTPRequestListener;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->size()I

    move-result v1

    .line 64
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 68
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/http/HTTPServer;

    move-result-object v2

    .line 66
    .local v2, "server":Lcom/samsung/http/HTTPServer;
    invoke-virtual {v2, p1}, Lcom/samsung/http/HTTPServer;->addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public close()Z
    .locals 5

    .prologue
    .line 90
    const/4 v0, 0x1

    .line 91
    .local v0, "isClosed":Z
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->size()I

    move-result v2

    .line 92
    .local v2, "nServers":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 96
    return v0

    .line 93
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/http/HTTPServer;

    move-result-object v3

    .line 94
    .local v3, "server":Lcom/samsung/http/HTTPServer;
    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/samsung/http/HTTPServer;->close()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    .line 92
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getHTTPServer(I)Lcom/samsung/http/HTTPServer;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPServerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/http/HTTPServer;

    return-object v0
.end method

.method public open(I)Z
    .locals 5
    .param p1, "port"    # I

    .prologue
    .line 101
    invoke-static {}, Lcom/samsung/net/HostInterface;->getNHostAddresses()I

    move-result v3

    .line 103
    .local v3, "nHostAddrs":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 116
    const/4 v4, 0x1

    :goto_1
    return v4

    .line 104
    :cond_0
    invoke-static {v2}, Lcom/samsung/net/HostInterface;->getHostAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "bindAddr":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/http/HTTPServerList;->getHttpServer()Lcom/samsung/http/HTTPServer;

    move-result-object v1

    .line 108
    .local v1, "httpServer":Lcom/samsung/http/HTTPServer;
    invoke-virtual {v1, v0, p1}, Lcom/samsung/http/HTTPServer;->open(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 109
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->close()Z

    .line 110
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->clear()V

    .line 111
    const/4 v4, 0x0

    goto :goto_1

    .line 113
    :cond_1
    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPServerList;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public open(ILcom/samsung/net/HostInterface$NWK_TYPE;)Z
    .locals 5
    .param p1, "port"    # I
    .param p2, "nwkType"    # Lcom/samsung/net/HostInterface$NWK_TYPE;

    .prologue
    .line 120
    invoke-static {p2}, Lcom/samsung/net/HostInterface;->getNHostAddresses(Lcom/samsung/net/HostInterface$NWK_TYPE;)I

    move-result v3

    .line 122
    .local v3, "nHostAddrs":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 134
    const/4 v4, 0x1

    :goto_1
    return v4

    .line 123
    :cond_0
    invoke-static {p2, v2}, Lcom/samsung/net/HostInterface;->getHostAddress(Lcom/samsung/net/HostInterface$NWK_TYPE;I)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "bindAddr":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/http/HTTPServerList;->getHttpServer()Lcom/samsung/http/HTTPServer;

    move-result-object v1

    .line 126
    .local v1, "httpServer":Lcom/samsung/http/HTTPServer;
    invoke-virtual {v1, v0, p1}, Lcom/samsung/http/HTTPServer;->open(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->close()Z

    .line 128
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->clear()V

    .line 129
    const/4 v4, 0x0

    goto :goto_1

    .line 131
    :cond_1
    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPServerList;->add(Ljava/lang/Object;)Z

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->size()I

    move-result v1

    .line 149
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 153
    return-void

    .line 150
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/http/HTTPServer;

    move-result-object v2

    .line 151
    .local v2, "server":Lcom/samsung/http/HTTPServer;
    invoke-virtual {v2}, Lcom/samsung/http/HTTPServer;->start()Z

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/samsung/http/HTTPServerList;->size()I

    move-result v1

    .line 158
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 162
    return-void

    .line 159
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/http/HTTPServer;

    move-result-object v2

    .line 160
    .local v2, "server":Lcom/samsung/http/HTTPServer;
    invoke-virtual {v2}, Lcom/samsung/http/HTTPServer;->stop()Z

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
