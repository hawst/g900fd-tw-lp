.class public Lcom/samsung/http/HTTPStatus;
.super Ljava/lang/Object;
.source "HTTPStatus.java"


# static fields
.field public static final BAD_REQUEST:I = 0x190

.field public static final CONTINUE:I = 0x64

.field public static final GONE:I = 0x19a

.field public static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final INVALID_RANGE:I = 0x1a0

.field public static final NOT_ACCEPTABLE:I = 0x196

.field public static final NOT_FOUND:I = 0x194

.field public static final OK:I = 0xc8

.field public static final PARTIAL_CONTENT:I = 0xce

.field public static final PRECONDITION_FAILED:I = 0x19c

.field public static final REQUEST_TIMEOUT:I = 0x198


# instance fields
.field private reasonPhrase:Ljava/lang/String;

.field private statusCode:I

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "lineStr"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/http/HTTPStatus;->version:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/http/HTTPStatus;->statusCode:I

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/http/HTTPStatus;->reasonPhrase:Ljava/lang/String;

    .line 89
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPStatus;->set(Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public static final code2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 56
    sparse-switch p0, :sswitch_data_0

    .line 66
    const-string v0, ""

    :goto_0
    return-object v0

    .line 57
    :sswitch_0
    const-string v0, "Continue"

    goto :goto_0

    .line 58
    :sswitch_1
    const-string v0, "OK"

    goto :goto_0

    .line 59
    :sswitch_2
    const-string v0, "Partial Content"

    goto :goto_0

    .line 60
    :sswitch_3
    const-string v0, "Bad Request"

    goto :goto_0

    .line 61
    :sswitch_4
    const-string v0, "Not Found"

    goto :goto_0

    .line 62
    :sswitch_5
    const-string v0, "Precondition Failed"

    goto :goto_0

    .line 63
    :sswitch_6
    const-string v0, "Invalid Range"

    goto :goto_0

    .line 64
    :sswitch_7
    const-string v0, "Internal Server Error"

    goto :goto_0

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xce -> :sswitch_2
        0x190 -> :sswitch_3
        0x194 -> :sswitch_4
        0x19c -> :sswitch_5
        0x1a0 -> :sswitch_6
        0x1f4 -> :sswitch_7
    .end sparse-switch
.end method

.method public static final isSuccessful(I)Z
    .locals 1
    .param p0, "statCode"    # I

    .prologue
    .line 136
    const/16 v0, 0xc8

    if-gt v0, p0, :cond_0

    const/16 v0, 0x12c

    if-ge p0, v0, :cond_0

    .line 137
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/samsung/http/HTTPStatus;->statusCode:I

    return v0
.end method

.method public set(Ljava/lang/String;)V
    .locals 8
    .param p1, "lineStr"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x1f4

    .line 152
    if-nez p1, :cond_1

    .line 153
    const-string v6, "1.1"

    invoke-virtual {p0, v6}, Lcom/samsung/http/HTTPStatus;->setVersion(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0, v7}, Lcom/samsung/http/HTTPStatus;->setStatusCode(I)V

    .line 155
    invoke-static {v7}, Lcom/samsung/http/HTTPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/http/HTTPStatus;->setReasonPhrase(Ljava/lang/String;)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    :try_start_0
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, " "

    invoke-direct {v4, p1, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .local v4, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 164
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 165
    .local v5, "ver":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/http/HTTPStatus;->setVersion(Ljava/lang/String;)V

    .line 167
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 169
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 170
    .local v1, "codeStr":Ljava/lang/String;
    const/4 v0, 0x0

    .line 172
    .local v0, "code":I
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 175
    :goto_1
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPStatus;->setStatusCode(I)V

    .line 177
    const-string v3, ""

    .line 178
    .local v3, "reason":Ljava/lang/String;
    :goto_2
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-nez v6, :cond_2

    .line 183
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/http/HTTPStatus;->setReasonPhrase(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 185
    .end local v0    # "code":I
    .end local v1    # "codeStr":Ljava/lang/String;
    .end local v3    # "reason":Ljava/lang/String;
    .end local v4    # "st":Ljava/util/StringTokenizer;
    .end local v5    # "ver":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 186
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    goto :goto_0

    .line 179
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "code":I
    .restart local v1    # "codeStr":Ljava/lang/String;
    .restart local v3    # "reason":Ljava/lang/String;
    .restart local v4    # "st":Ljava/util/StringTokenizer;
    .restart local v5    # "ver":Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-ltz v6, :cond_3

    .line 180
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 181
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v3

    goto :goto_2

    .line 174
    .end local v3    # "reason":Ljava/lang/String;
    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method public setReasonPhrase(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/http/HTTPStatus;->reasonPhrase:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/samsung/http/HTTPStatus;->statusCode:I

    .line 108
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/http/HTTPStatus;->version:Ljava/lang/String;

    .line 103
    return-void
.end method
