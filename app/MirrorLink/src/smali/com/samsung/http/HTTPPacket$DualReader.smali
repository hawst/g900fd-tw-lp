.class Lcom/samsung/http/HTTPPacket$DualReader;
.super Ljava/io/DataInputStream;
.source "HTTPPacket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/http/HTTPPacket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DualReader"
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "arg0"    # Ljava/io/InputStream;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 132
    return-void
.end method


# virtual methods
.method public readALine()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 140
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 143
    .local v2, "sb":Ljava/lang/StringBuffer;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/http/HTTPPacket$DualReader;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    .local v0, "d":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    .line 153
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "ret":Ljava/lang/String;
    return-object v1

    .line 144
    .end local v1    # "ret":Ljava/lang/String;
    :cond_2
    int-to-char v3, v0

    const/16 v4, 0xd

    if-eq v3, v4, :cond_0

    .line 146
    int-to-char v3, v0

    const/16 v4, 0xa

    if-eq v3, v4, :cond_1

    .line 149
    int-to-char v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
