.class public Lcom/samsung/http/HTTPRequest;
.super Lcom/samsung/http/HTTPPacket;
.source "HTTPRequest.java"


# instance fields
.field private httpSocket:Lcom/samsung/http/HTTPSocket;

.field private method:Ljava/lang/String;

.field private postSocket:Ljava/net/Socket;

.field private requestHost:Ljava/lang/String;

.field private requestPort:I

.field private uri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Lcom/samsung/http/HTTPPacket;-><init>()V

    .line 104
    iput-object v1, p0, Lcom/samsung/http/HTTPRequest;->method:Ljava/lang/String;

    .line 160
    iput-object v1, p0, Lcom/samsung/http/HTTPRequest;->uri:Ljava/lang/String;

    .line 239
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/http/HTTPRequest;->requestHost:Ljava/lang/String;

    .line 251
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/http/HTTPRequest;->requestPort:I

    .line 267
    iput-object v1, p0, Lcom/samsung/http/HTTPRequest;->httpSocket:Lcom/samsung/http/HTTPSocket;

    .line 415
    iput-object v1, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    .line 84
    const-string v0, "USER-AGENT"

    sget-object v1, Lcom/samsung/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/http/HTTPRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    return-void
.end method


# virtual methods
.method public getFirstLineString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getHTTPVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHTTPVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->hasFirstLine()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HTTP/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/samsung/http/HTTPPacket;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 3

    .prologue
    .line 334
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 336
    .local v1, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getFirstLineString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 338
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getHeaderString()Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, "headerString":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 341
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getSocket()Lcom/samsung/http/HTTPSocket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/http/HTTPSocket;->getLocalAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/http/HTTPRequest;->method:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/samsung/http/HTTPRequest;->method:Ljava/lang/String;

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getParameterList()Lcom/samsung/http/ParameterList;
    .locals 10

    .prologue
    .line 200
    new-instance v5, Lcom/samsung/http/ParameterList;

    invoke-direct {v5}, Lcom/samsung/http/ParameterList;-><init>()V

    .line 201
    .local v5, "paramList":Lcom/samsung/http/ParameterList;
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v6

    .line 202
    .local v6, "uri":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 217
    :cond_0
    return-object v5

    .line 205
    :cond_1
    const/16 v8, 0x3f

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 206
    .local v4, "paramIdx":I
    if-ltz v4, :cond_0

    .line 208
    :goto_0
    if-lez v4, :cond_0

    .line 209
    const/16 v8, 0x3d

    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 210
    .local v0, "eqIdx":I
    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v6, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "name":Ljava/lang/String;
    const/16 v8, 0x26

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 212
    .local v2, "nextParamIdx":I
    add-int/lit8 v9, v0, 0x1

    if-lez v2, :cond_2

    move v8, v2

    :goto_1
    invoke-virtual {v6, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 213
    .local v7, "value":Ljava/lang/String;
    new-instance v3, Lcom/samsung/http/Parameter;

    invoke-direct {v3, v1, v7}, Lcom/samsung/http/Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .local v3, "param":Lcom/samsung/http/Parameter;
    invoke-virtual {v5, v3}, Lcom/samsung/http/ParameterList;->add(Ljava/lang/Object;)Z

    .line 215
    move v4, v2

    goto :goto_0

    .line 212
    .end local v3    # "param":Lcom/samsung/http/Parameter;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_1
.end method

.method public getRequestHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/http/HTTPRequest;->requestHost:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestPort()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/samsung/http/HTTPRequest;->requestPort:I

    return v0
.end method

.method public getSocket()Lcom/samsung/http/HTTPSocket;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/http/HTTPRequest;->httpSocket:Lcom/samsung/http/HTTPSocket;

    return-object v0
.end method

.method public getURI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 181
    iget-object v1, p0, Lcom/samsung/http/HTTPRequest;->uri:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 182
    iget-object v0, p0, Lcom/samsung/http/HTTPRequest;->uri:Ljava/lang/String;

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 185
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "uriTemp":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/http/HTTP;->isAbsoluteURL(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-static {v0}, Lcom/samsung/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 189
    goto :goto_0
.end method

.method public isGetRequest()Z
    .locals 1

    .prologue
    .line 128
    const-string v0, "GET"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isHeadRequest()Z
    .locals 1

    .prologue
    .line 138
    const-string v0, "HEAD"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isKeepAlive()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 350
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->isCloseConnection()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 358
    :cond_0
    :goto_0
    return v2

    .line 352
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->isKeepAliveConnection()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 353
    goto :goto_0

    .line 354
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getHTTPVersion()Ljava/lang/String;

    move-result-object v0

    .line 355
    .local v0, "httpVer":Ljava/lang/String;
    const-string v4, "1.0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_3

    move v1, v3

    .line 356
    .local v1, "isHTTP10":Z
    :goto_1
    if-nez v1, :cond_0

    move v2, v3

    .line 358
    goto :goto_0

    .end local v1    # "isHTTP10":Z
    :cond_3
    move v1, v2

    .line 355
    goto :goto_1
.end method

.method public isMethod(Ljava/lang/String;)Z
    .locals 2
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getMethod()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "headerMethod":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 122
    const/4 v1, 0x0

    .line 123
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isNotifyRequest()Z
    .locals 1

    .prologue
    .line 153
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPostRequest()Z
    .locals 1

    .prologue
    .line 133
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSOAPAction()Z
    .locals 1

    .prologue
    .line 232
    const-string v0, "SOAPACTION"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSubscribeRequest()Z
    .locals 1

    .prologue
    .line 143
    const-string v0, "SUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUnsubscribeRequest()Z
    .locals 1

    .prologue
    .line 148
    const-string v0, "UNSUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public post(Ljava/lang/String;I)Lcom/samsung/http/HTTPResponse;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 499
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/http/HTTPRequest;->post(Ljava/lang/String;IZ)Lcom/samsung/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;IZ)Lcom/samsung/http/HTTPResponse;
    .locals 12
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "isKeepAlive"    # Z

    .prologue
    .line 426
    new-instance v4, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v4}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 428
    .local v4, "httpRes":Lcom/samsung/http/HTTPResponse;
    if-eqz p3, :cond_7

    const-string v10, "Keep-Alive"

    :goto_0
    invoke-virtual {p0, v10}, Lcom/samsung/http/HTTPRequest;->setConnection(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->isHeadRequest()Z

    move-result v7

    .line 432
    .local v7, "isHeaderRequest":Z
    const/4 v8, 0x0

    .line 433
    .local v8, "out":Ljava/io/OutputStream;
    const/4 v5, 0x0

    .line 436
    .local v5, "in":Ljava/io/InputStream;
    :try_start_0
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    if-nez v10, :cond_0

    .line 437
    new-instance v10, Ljava/net/Socket;

    invoke-direct {v10, p1, p2}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    .line 438
    :cond_0
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    const/16 v11, 0x1b58

    invoke-virtual {v10, v11}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 439
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/net/Socket;->setReuseAddress(Z)V

    .line 440
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    .line 441
    new-instance v9, Ljava/io/PrintStream;

    invoke-direct {v9, v8}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    .line 442
    .local v9, "pout":Ljava/io/PrintStream;
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 443
    const-string v10, "\r\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 445
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->isChunked()Z

    move-result v6

    .line 447
    .local v6, "isChunkedRequest":Z
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getContentString()Ljava/lang/String;

    move-result-object v1

    .line 448
    .local v1, "content":Ljava/lang/String;
    const/4 v2, 0x0

    .line 449
    .local v2, "contentLength":I
    if-eqz v1, :cond_1

    .line 450
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 452
    :cond_1
    if-lez v2, :cond_3

    .line 453
    if-eqz v6, :cond_2

    .line 454
    int-to-long v10, v2

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, "chunSizeBuf":Ljava/lang/String;
    invoke-virtual {v9, v0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 456
    const-string v10, "\r\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 458
    .end local v0    # "chunSizeBuf":Ljava/lang/String;
    :cond_2
    invoke-virtual {v9, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 459
    if-eqz v6, :cond_3

    .line 460
    const-string v10, "\r\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 463
    :cond_3
    if-eqz v6, :cond_4

    .line 464
    const-string v10, "0"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 465
    const-string v10, "\r\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 468
    :cond_4
    invoke-virtual {v9}, Ljava/io/PrintStream;->flush()V

    .line 469
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 470
    invoke-virtual {v4, v5, v7}, Lcom/samsung/http/HTTPResponse;->set(Ljava/io/InputStream;Z)Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478
    if-nez p3, :cond_6

    .line 479
    if-eqz v5, :cond_5

    .line 481
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 484
    :cond_5
    :goto_1
    if-eqz v8, :cond_6

    .line 486
    :try_start_2
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 487
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    .line 489
    :goto_2
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    .line 494
    .end local v1    # "content":Ljava/lang/String;
    .end local v2    # "contentLength":I
    .end local v6    # "isChunkedRequest":Z
    .end local v9    # "pout":Ljava/io/PrintStream;
    :cond_6
    :goto_3
    return-object v4

    .line 428
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "isHeaderRequest":Z
    .end local v8    # "out":Ljava/io/OutputStream;
    :cond_7
    const-string v10, "close"

    goto/16 :goto_0

    .line 472
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v7    # "isHeaderRequest":Z
    .restart local v8    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v3

    .line 473
    .local v3, "e":Ljava/net/SocketTimeoutException;
    const/16 v10, 0x19a

    :try_start_3
    invoke-virtual {v4, v10}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 478
    if-nez p3, :cond_6

    .line 479
    if-eqz v5, :cond_8

    .line 481
    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 484
    :cond_8
    :goto_4
    if-eqz v8, :cond_6

    .line 486
    :try_start_5
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 487
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9

    .line 489
    :goto_5
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    goto :goto_3

    .line 475
    .end local v3    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v3

    .line 476
    .local v3, "e":Ljava/lang/Exception;
    const/16 v10, 0x1f4

    :try_start_6
    invoke-virtual {v4, v10}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 478
    if-nez p3, :cond_6

    .line 479
    if-eqz v5, :cond_9

    .line 481
    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .line 484
    :cond_9
    :goto_6
    if-eqz v8, :cond_6

    .line 486
    :try_start_8
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 487
    iget-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    .line 489
    :goto_7
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    goto :goto_3

    .line 477
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    .line 478
    if-nez p3, :cond_b

    .line 479
    if-eqz v5, :cond_a

    .line 481
    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    .line 484
    :cond_a
    :goto_8
    if-eqz v8, :cond_b

    .line 486
    :try_start_a
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 487
    iget-object v11, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    invoke-virtual {v11}, Ljava/net/Socket;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    .line 489
    :goto_9
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/samsung/http/HTTPRequest;->postSocket:Ljava/net/Socket;

    .line 492
    :cond_b
    throw v10

    .line 482
    .local v3, "e":Ljava/net/SocketTimeoutException;
    :catch_2
    move-exception v10

    goto :goto_4

    .local v3, "e":Ljava/lang/Exception;
    :catch_3
    move-exception v10

    goto :goto_6

    .end local v3    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v11

    goto :goto_8

    .restart local v1    # "content":Ljava/lang/String;
    .restart local v2    # "contentLength":I
    .restart local v6    # "isChunkedRequest":Z
    .restart local v9    # "pout":Ljava/io/PrintStream;
    :catch_5
    move-exception v10

    goto :goto_1

    .line 488
    :catch_6
    move-exception v10

    goto :goto_2

    .end local v1    # "content":Ljava/lang/String;
    .end local v2    # "contentLength":I
    .end local v6    # "isChunkedRequest":Z
    .end local v9    # "pout":Ljava/io/PrintStream;
    :catch_7
    move-exception v11

    goto :goto_9

    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v10

    goto :goto_7

    .local v3, "e":Ljava/net/SocketTimeoutException;
    :catch_9
    move-exception v10

    goto :goto_5
.end method

.method public post(Lcom/samsung/http/HTTPResponse;)Z
    .locals 16
    .param p1, "httpRes"    # Lcom/samsung/http/HTTPResponse;

    .prologue
    .line 382
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->getSocket()Lcom/samsung/http/HTTPSocket;

    move-result-object v8

    .line 383
    .local v8, "httpSock":Lcom/samsung/http/HTTPSocket;
    const-wide/16 v10, 0x0

    .line 384
    .local v10, "offset":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/http/HTTPResponse;->getContentLength()J

    move-result-wide v6

    .line 385
    .local v6, "length":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->hasContentRange()Z

    move-result v1

    if-eqz v1, :cond_5

    const-wide/16 v12, 0x0

    cmp-long v1, v6, v12

    if-eqz v1, :cond_5

    .line 386
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->getContentRangeFirstPosition()J

    move-result-wide v2

    .line 387
    .local v2, "firstPos":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->getContentRangeLastPosition()J

    move-result-wide v4

    .line 389
    .local v4, "lastPos":J
    const-wide/16 v12, 0x0

    cmp-long v1, v4, v12

    if-gtz v1, :cond_0

    .line 390
    const-wide/16 v12, 0x1

    sub-long v4, v6, v12

    .line 393
    :cond_0
    const-wide/16 v12, 0x0

    cmp-long v1, v2, v12

    if-gez v1, :cond_1

    .line 394
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    move-result v1

    .line 408
    .end local v2    # "firstPos":J
    .end local v4    # "lastPos":J
    :goto_0
    return v1

    .line 396
    .restart local v2    # "firstPos":J
    .restart local v4    # "lastPos":J
    :cond_1
    cmp-long v1, v2, v6

    if-gtz v1, :cond_2

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    .line 397
    :cond_2
    const/16 v1, 0x1a0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPRequest;->returnResponse(I)Z

    move-result v1

    goto :goto_0

    .line 399
    :cond_3
    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    .line 400
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    move-result v1

    goto :goto_0

    :cond_4
    move-object/from16 v1, p1

    .line 402
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/http/HTTPResponse;->setContentRange(JJJ)V

    .line 403
    const/16 v1, 0xce

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 405
    move-wide v10, v2

    .line 406
    sub-long v12, v4, v2

    const-wide/16 v14, 0x1

    add-long v6, v12, v14

    .line 408
    .end local v2    # "firstPos":J
    .end local v4    # "lastPos":J
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/http/HTTPRequest;->isHeadRequest()Z

    move-result v14

    move-object/from16 v9, p1

    move-wide v12, v6

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/http/HTTPSocket;->post(Lcom/samsung/http/HTTPResponse;JJZ)Z

    move-result v1

    goto :goto_0
.end method

.method public print()V
    .locals 0

    .prologue
    .line 554
    return-void
.end method

.method public read()Z
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getSocket()Lcom/samsung/http/HTTPSocket;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/http/HTTPPacket;->read(Lcom/samsung/http/HTTPSocket;)Z

    move-result v0

    return v0
.end method

.method public returnBadRequest()Z
    .locals 1

    .prologue
    .line 531
    const/16 v0, 0x190

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnOK()Z
    .locals 1

    .prologue
    .line 526
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnResponse(I)Z
    .locals 3
    .param p1, "statusCode"    # I

    .prologue
    .line 518
    new-instance v0, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v0}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 519
    .local v0, "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {v0, p1}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 520
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 521
    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    move-result v1

    return v1
.end method

.method public set(Lcom/samsung/http/HTTPRequest;)V
    .locals 1
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 508
    invoke-virtual {p0, p1}, Lcom/samsung/http/HTTPRequest;->set(Lcom/samsung/http/HTTPPacket;)V

    .line 509
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getSocket()Lcom/samsung/http/HTTPSocket;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/http/HTTPRequest;->setSocket(Lcom/samsung/http/HTTPSocket;)V

    .line 510
    return-void
.end method

.method public setMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/http/HTTPRequest;->method:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setRequestHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/samsung/http/HTTPRequest;->requestHost:Ljava/lang/String;

    .line 244
    return-void
.end method

.method public setRequestPort(I)V
    .locals 0
    .param p1, "host"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/samsung/http/HTTPRequest;->requestPort:I

    .line 256
    return-void
.end method

.method public setSocket(Lcom/samsung/http/HTTPSocket;)V
    .locals 0
    .param p1, "value"    # Lcom/samsung/http/HTTPSocket;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/samsung/http/HTTPRequest;->httpSocket:Lcom/samsung/http/HTTPSocket;

    .line 272
    return-void
.end method

.method public setURI(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/http/HTTPRequest;->setURI(Ljava/lang/String;Z)V

    .line 174
    return-void
.end method

.method public setURI(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "isCheckRelativeURL"    # Z

    .prologue
    .line 164
    iput-object p1, p0, Lcom/samsung/http/HTTPRequest;->uri:Ljava/lang/String;

    .line 165
    if-nez p2, :cond_0

    .line 169
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/samsung/http/HTTPRequest;->uri:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/http/HTTPRequest;->uri:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 540
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/http/HTTPRequest;->getContentString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 541
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method
