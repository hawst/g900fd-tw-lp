.class public Lcom/samsung/soap/SOAPResponse;
.super Lcom/samsung/http/HTTPResponse;
.source "SOAPResponse.java"


# instance fields
.field private rootNode:Lcom/samsung/xml/Node;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 48
    invoke-static {}, Lcom/samsung/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/xml/Node;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/soap/SOAPResponse;->setRootNode(Lcom/samsung/xml/Node;)V

    .line 49
    const-string v0, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPResponse;->setContentType(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/samsung/http/HTTPResponse;)V
    .locals 3
    .param p1, "httpRes"    # Lcom/samsung/http/HTTPResponse;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/samsung/http/HTTPResponse;-><init>(Lcom/samsung/http/HTTPResponse;)V

    .line 56
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPResponse;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 57
    .local v1, "is":Ljava/io/InputStream;
    invoke-direct {p0, v1}, Lcom/samsung/soap/SOAPResponse;->setEnvelopeNode(Ljava/io/InputStream;)V

    .line 58
    const-string v2, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v2}, Lcom/samsung/soap/SOAPResponse;->setContentType(Ljava/lang/String;)V

    .line 59
    if-eqz v1, :cond_0

    .line 61
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/samsung/soap/SOAPResponse;)V
    .locals 1
    .param p1, "soapRes"    # Lcom/samsung/soap/SOAPResponse;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/http/HTTPResponse;-><init>(Lcom/samsung/http/HTTPResponse;)V

    .line 73
    invoke-virtual {p1}, Lcom/samsung/soap/SOAPResponse;->getEnvelopeNode()Lcom/samsung/xml/Node;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPResponse;->setEnvelopeNode(Lcom/samsung/xml/Node;)V

    .line 74
    const-string v0, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPResponse;->setContentType(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method private getRootNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/soap/SOAPResponse;->rootNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method private setEnvelopeNode(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 97
    if-nez p1, :cond_0

    .line 98
    invoke-static {}, Lcom/samsung/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/xml/Node;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/soap/SOAPResponse;->setRootNode(Lcom/samsung/xml/Node;)V

    .line 111
    :goto_0
    return-void

    .line 103
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/soap/SOAP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v2

    .line 104
    .local v2, "xmlParser":Lcom/samsung/xml/Parser;
    invoke-virtual {v2, p1}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v1

    .line 105
    .local v1, "rootNode":Lcom/samsung/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/soap/SOAPResponse;->setEnvelopeNode(Lcom/samsung/xml/Node;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    .end local v1    # "rootNode":Lcom/samsung/xml/Node;
    .end local v2    # "xmlParser":Lcom/samsung/xml/Parser;
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 109
    invoke-static {}, Lcom/samsung/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/xml/Node;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/soap/SOAPResponse;->setRootNode(Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method private setRootNode(Lcom/samsung/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/soap/SOAPResponse;->rootNode:Lcom/samsung/xml/Node;

    .line 86
    return-void
.end method


# virtual methods
.method public getBodyNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPResponse;->getEnvelopeNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 127
    .local v0, "envNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 128
    const/4 v1, 0x0

    .line 129
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "Body"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getEnvelopeNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/samsung/soap/SOAPResponse;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getFaultDetailNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPResponse;->getFaultNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 176
    .local v0, "faultNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 177
    const/4 v1, 0x0

    .line 178
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "detail"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getFaultNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPResponse;->getBodyNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 144
    .local v0, "bodyNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 145
    const/4 v1, 0x0

    .line 146
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "Fault"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public print()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public setContent(Lcom/samsung/xml/Node;)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 212
    const-string v0, ""

    .line 213
    .local v0, "conStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPResponse;->setContent(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public setEnvelopeNode(Lcom/samsung/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/samsung/soap/SOAPResponse;->setRootNode(Lcom/samsung/xml/Node;)V

    .line 117
    return-void
.end method
