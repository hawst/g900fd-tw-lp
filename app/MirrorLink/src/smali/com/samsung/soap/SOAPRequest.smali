.class public Lcom/samsung/soap/SOAPRequest;
.super Lcom/samsung/http/HTTPRequest;
.source "SOAPRequest.java"


# static fields
.field private static final SOAPACTION:Ljava/lang/String; = "SOAPACTION"


# instance fields
.field private rootNode:Lcom/samsung/xml/Node;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/http/HTTPRequest;-><init>()V

    .line 53
    const-string v0, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPRequest;->setContentType(Ljava/lang/String;)V

    .line 54
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPRequest;->setMethod(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method private declared-synchronized getRootNode()Lcom/samsung/xml/Node;
    .locals 5

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/soap/SOAPRequest;->rootNode:Lcom/samsung/xml/Node;

    if-eqz v3, :cond_0

    .line 131
    iget-object v3, p0, Lcom/samsung/soap/SOAPRequest;->rootNode:Lcom/samsung/xml/Node;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 154
    :goto_0
    monitor-exit p0

    return-object v3

    .line 133
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPRequest;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 138
    .local v1, "is":Ljava/io/InputStream;
    :try_start_2
    invoke-static {}, Lcom/samsung/soap/SOAP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v2

    .line 139
    .local v2, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v2, v1}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/soap/SOAPRequest;->rootNode:Lcom/samsung/xml/Node;
    :try_end_2
    .catch Lcom/samsung/xml/ParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 146
    if-eqz v1, :cond_1

    .line 147
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 154
    .end local v2    # "parser":Lcom/samsung/xml/Parser;
    :cond_1
    :goto_1
    :try_start_4
    iget-object v3, p0, Lcom/samsung/soap/SOAPRequest;->rootNode:Lcom/samsung/xml/Node;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Lcom/samsung/xml/ParserException;
    :try_start_5
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 146
    if-eqz v1, :cond_1

    .line 147
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 148
    :catch_1
    move-exception v3

    goto :goto_1

    .line 144
    .end local v0    # "e":Lcom/samsung/xml/ParserException;
    :catchall_0
    move-exception v3

    .line 146
    if-eqz v1, :cond_2

    .line 147
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 150
    :cond_2
    :goto_2
    :try_start_8
    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 130
    .end local v1    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3

    .line 148
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v4

    goto :goto_2

    .restart local v2    # "parser":Lcom/samsung/xml/Parser;
    :catch_3
    move-exception v3

    goto :goto_1
.end method

.method private setRootNode(Lcom/samsung/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/soap/SOAPRequest;->rootNode:Lcom/samsung/xml/Node;

    .line 126
    return-void
.end method


# virtual methods
.method public getBodyNode()Lcom/samsung/xml/Node;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPRequest;->getEnvelopeNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 174
    .local v0, "envNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-object v1

    .line 176
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/xml/Node;->hasNodes()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getEnvelopeNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/samsung/soap/SOAPRequest;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getSOAPAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "SOAPACTION"

    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPRequest;->getStringHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSOAPAction(Ljava/lang/String;)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 78
    const-string v3, "SOAPACTION"

    invoke-virtual {p0, v3}, Lcom/samsung/soap/SOAPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "headerValue":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v2

    .line 81
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 82
    const/4 v2, 0x1

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPRequest;->getSOAPAction()Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "soapAction":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public postMessage(Ljava/lang/String;I)Lcom/samsung/soap/SOAPResponse;
    .locals 6
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 95
    const-string v2, "SOAPRequest postMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "post keepAlive"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0, p1, p2}, Lcom/samsung/soap/SOAPRequest;->post(Ljava/lang/String;I)Lcom/samsung/http/HTTPResponse;

    move-result-object v0

    .line 98
    .local v0, "httpRes":Lcom/samsung/http/HTTPResponse;
    new-instance v1, Lcom/samsung/soap/SOAPResponse;

    invoke-direct {v1, v0}, Lcom/samsung/soap/SOAPResponse;-><init>(Lcom/samsung/http/HTTPResponse;)V

    .line 114
    .local v1, "soapRes":Lcom/samsung/soap/SOAPResponse;
    return-object v1
.end method

.method public print()V
    .locals 3

    .prologue
    .line 202
    const-string v1, "SOAPRequest"

    invoke-virtual {p0}, Lcom/samsung/soap/SOAPRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/samsung/soap/SOAPRequest;->hasContent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    invoke-direct {p0}, Lcom/samsung/soap/SOAPRequest;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 206
    .local v0, "rootElem":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_0

    .line 208
    const-string v1, "SOAPRequest Root"

    invoke-virtual {v0}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setContent(Lcom/samsung/xml/Node;)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 188
    const-string v0, ""

    .line 189
    .local v0, "conStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 191
    if-eqz p1, :cond_0

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/soap/SOAPRequest;->setContent(Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method public setEnvelopeNode(Lcom/samsung/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/samsung/soap/SOAPRequest;->setRootNode(Lcom/samsung/xml/Node;)V

    .line 164
    return-void
.end method

.method public setSOAPAction(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v0, "SOAPACTION"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/soap/SOAPRequest;->setStringHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method
