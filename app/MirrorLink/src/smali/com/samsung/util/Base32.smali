.class public Lcom/samsung/util/Base32;
.super Ljava/lang/Object;
.source "Base32.java"


# static fields
.field static final base32chars:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/16 v0, 0x20

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/util/Base32;->base32chars:[C

    .line 28
    return-void

    .line 25
    :array_0
    .array-data 2
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
        0x47s
        0x48s
        0x49s
        0x4as
        0x4bs
        0x4cs
        0x4ds
        0x4es
        0x4fs
        0x50s
        0x51s
        0x52s
        0x53s
        0x54s
        0x55s
        0x56s
        0x57s
        0x58s
        0x59s
        0x5as
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generate(J)Ljava/lang/String;
    .locals 6
    .param p0, "crc"    # J

    .prologue
    const-wide/16 v4, 0x1f

    .line 31
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 33
    .local v0, "base32":Ljava/lang/StringBuffer;
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x3b

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 34
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x36

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 35
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x31

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 36
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x2c

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 37
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x27

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 38
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x22

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 39
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x1d

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 40
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x18

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 41
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x13

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 42
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0xe

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 43
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/16 v2, 0x9

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 44
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/4 v2, 0x4

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 45
    sget-object v1, Lcom/samsung/util/Base32;->base32chars:[C

    const/4 v2, 0x1

    shl-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 47
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
