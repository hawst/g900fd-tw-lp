.class public Lcom/samsung/util/DeviceID;
.super Ljava/lang/Object;
.source "DeviceID.java"


# static fields
.field static final deadbeef:J = -0x2152411021524111L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDeviceID(J)Ljava/lang/String;
    .locals 7
    .param p0, "macaddress"    # J

    .prologue
    .line 27
    const-wide v5, -0x2152411021524111L    # -1.1885959257070704E148

    xor-long v3, p0, v5

    .line 28
    .local v3, "v":J
    invoke-static {v3, v4}, Lcom/samsung/util/CRC64;->generate(J)J

    move-result-wide v0

    .line 29
    .local v0, "crc":J
    invoke-static {v0, v1}, Lcom/samsung/util/Base32;->generate(J)Ljava/lang/String;

    move-result-object v2

    .line 31
    .local v2, "deviceid":Ljava/lang/String;
    return-object v2
.end method
