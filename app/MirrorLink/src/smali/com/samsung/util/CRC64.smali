.class public Lcom/samsung/util/CRC64;
.super Ljava/lang/Object;
.source "CRC64.java"


# static fields
.field private static final LOOKUPTABLE:[J

.field private static final POLY64REV:J = -0x2800000000000000L


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1

    const/16 v9, 0x100

    const/4 v8, 0x1

    .line 28
    new-array v4, v9, [J

    sput-object v4, Lcom/samsung/util/CRC64;->LOOKUPTABLE:[J

    .line 29
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v9, :cond_0

    .line 40
    return-void

    .line 30
    :cond_0
    int-to-long v2, v0

    .line 31
    .local v2, "v":J
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/16 v4, 0x8

    if-lt v1, v4, :cond_1

    .line 38
    sget-object v4, Lcom/samsung/util/CRC64;->LOOKUPTABLE:[J

    aput-wide v2, v4, v0

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_1
    and-long v4, v2, v10

    cmp-long v4, v4, v10

    if-nez v4, :cond_2

    .line 33
    ushr-long v4, v2, v8

    const-wide/high16 v6, -0x2800000000000000L    # -7.880401239278896E115

    xor-long v2, v4, v6

    .line 31
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 35
    :cond_2
    ushr-long/2addr v2, v8

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generate(J)J
    .locals 5
    .param p0, "value"    # J

    .prologue
    const/16 v4, 0x8

    .line 55
    new-array v1, v4, [B

    .line 56
    .local v1, "inputValue":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 61
    invoke-static {v1}, Lcom/samsung/util/CRC64;->generate([B)J

    move-result-wide v2

    return-wide v2

    .line 57
    :cond_0
    const-wide/16 v2, 0xff

    and-long/2addr v2, p0

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 58
    ushr-long/2addr p0, v4

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static generate([B)J
    .locals 4
    .param p0, "data"    # [B

    .prologue
    .line 47
    const-wide/16 v0, 0x0

    .line 48
    .local v0, "crc":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 51
    return-wide v0

    .line 49
    :cond_0
    aget-byte v3, p0, v2

    invoke-static {v0, v1, v3}, Lcom/samsung/util/CRC64;->next(JI)J

    move-result-wide v0

    .line 48
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static next(JI)J
    .locals 4
    .param p0, "crc"    # J
    .param p2, "ch"    # I

    .prologue
    .line 43
    const/16 v0, 0x8

    ushr-long v0, p0, v0

    sget-object v2, Lcom/samsung/util/CRC64;->LOOKUPTABLE:[J

    long-to-int v3, p0

    xor-int/2addr v3, p2

    and-int/lit16 v3, v3, 0xff

    aget-wide v2, v2, v3

    xor-long/2addr v0, v2

    return-wide v0
.end method
