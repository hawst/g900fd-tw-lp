.class public final Lcom/samsung/util/Debugs;
.super Ljava/lang/Object;
.source "Debugs.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final DEBUG:I = 0x2

.field static final ERROR:I = 0x3

.field static final INFO:I = 0x0

.field static final WARN:I = 0x1

.field public static enabled:Z

.field private static mTag:Ljava/lang/String;

.field private static outputList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/util/DebugOutputHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    .line 46
    const-string v0, "samsung"

    sput-object v0, Lcom/samsung/util/Debugs;->mTag:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/util/Debugs;->outputList:Ljava/util/ArrayList;

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final declared-synchronized addDebugOutputHandler(Lcom/samsung/util/DebugOutputHandler;)V
    .locals 2
    .param p0, "handler"    # Lcom/samsung/util/DebugOutputHandler;

    .prologue
    .line 55
    const-class v1, Lcom/samsung/util/Debugs;

    monitor-enter v1

    if-eqz p0, :cond_0

    .line 56
    :try_start_0
    sget-object v0, Lcom/samsung/util/Debugs;->outputList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    monitor-exit v1

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 102
    const-class v1, Lcom/samsung/util/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Lcom/samsung/util/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :cond_0
    monitor-exit v1

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized error(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 127
    const-class v1, Lcom/samsung/util/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 128
    const/4 v0, 0x3

    sget-object v2, Lcom/samsung/util/Debugs;->mTag:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/samsung/util/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_0
    monitor-exit v1

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized info(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 93
    const-class v1, Lcom/samsung/util/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x0

    sget-object v2, Lcom/samsung/util/Debugs;->mTag:Ljava/lang/String;

    invoke-static {v0, v2, p0}, Lcom/samsung/util/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_0
    monitor-exit v1

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isOn()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    return v0
.end method

.method public static final off()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    .line 41
    return-void
.end method

.method public static final on()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    .line 38
    return-void
.end method

.method private static final declared-synchronized printDebug(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "stat"    # I
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 73
    const-class v2, Lcom/samsung/util/Debugs;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/samsung/util/Debugs;->outputList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 89
    monitor-exit v2

    return-void

    .line 73
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/util/DebugOutputHandler;

    .line 74
    .local v0, "out":Lcom/samsung/util/DebugOutputHandler;
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 76
    :pswitch_0
    invoke-interface {v0, p1, p2}, Lcom/samsung/util/DebugOutputHandler;->info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 73
    .end local v0    # "out":Lcom/samsung/util/DebugOutputHandler;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 79
    .restart local v0    # "out":Lcom/samsung/util/DebugOutputHandler;
    :pswitch_1
    :try_start_2
    invoke-interface {v0, p1, p2}, Lcom/samsung/util/DebugOutputHandler;->warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :pswitch_2
    invoke-interface {v0, p1, p2}, Lcom/samsung/util/DebugOutputHandler;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :pswitch_3
    invoke-interface {v0, p1, p2}, Lcom/samsung/util/DebugOutputHandler;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static final declared-synchronized removeDebugOutputHandler(Lcom/samsung/util/DebugOutputHandler;)V
    .locals 2
    .param p0, "handler"    # Lcom/samsung/util/DebugOutputHandler;

    .prologue
    .line 60
    const-class v1, Lcom/samsung/util/Debugs;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/util/Debugs;->outputList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit v1

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final declared-synchronized warning(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 112
    const-class v1, Lcom/samsung/util/Debugs;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/util/Debugs;->enabled:Z

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x1

    invoke-static {v0, p0, p1}, Lcom/samsung/util/Debugs;->printDebug(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :cond_0
    monitor-exit v1

    return-void

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
