.class public Lcom/samsung/upnp/Device;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Lcom/samsung/http/HTTPReceivedListener;
.implements Lcom/samsung/http/HTTPRequestListener;
.implements Lcom/samsung/upnp/device/SearchListener;


# static fields
.field public static final DEFAULT_DESCRIPTION_URI:Ljava/lang/String; = "/description.xml"

.field public static final DEFAULT_DISCOVERY_WAIT_TIME:I = 0x64

.field public static final DEFAULT_LEASE_TIME:I = 0x708

.field public static final DEFAULT_STARTUP_WAIT_TIME:I = 0x3e8

.field private static final DEVICE_TYPE:Ljava/lang/String; = "deviceType"

.field public static final ELEM_NAME:Ljava/lang/String; = "device"

.field private static final FRIENDLY_NAME:Ljava/lang/String; = "friendlyName"

.field public static final HTTP_DEFAULT_PORT:I = 0xfa4

.field private static final MANUFACTURE:Ljava/lang/String; = "manufacturer"

.field private static final MANUFACTURE_URL:Ljava/lang/String; = "manufactureURL"

.field public static final MAX_FRIENDLY_NAME_LENGTH:I = 0x40

.field private static final MODEL_DESCRIPTION:Ljava/lang/String; = "modelDescription"

.field private static final MODEL_NAME:Ljava/lang/String; = "modelName"

.field private static final MODEL_NUMBER:Ljava/lang/String; = "modelNumber"

.field private static final MODEL_URL:Ljava/lang/String; = "modelURL"

.field private static final UDN:Ljava/lang/String; = "UDN"

.field public static final UPNP_ROOTDEVICE:Ljava/lang/String; = "upnp:rootdevice"

.field private static final URLBASE_NAME:Ljava/lang/String; = "URLBase"

.field private static final presentationURL:Ljava/lang/String; = "presentationURL"


# instance fields
.field descResoponse:Lcom/samsung/http/HTTPResponse;

.field private descriptionCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private devUUID:Ljava/lang/String;

.field private deviceNode:Lcom/samsung/xml/Node;

.field private rootNode:Lcom/samsung/xml/Node;

.field private wirelessMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 215
    invoke-static {}, Lcom/samsung/upnp/UPnP;->initialize()V

    .line 860
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 233
    invoke-direct {p0, v0, v0}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    .line 234
    return-void
.end method

.method public constructor <init>(Lcom/samsung/xml/Node;)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/xml/Node;

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    .line 239
    return-void
.end method

.method public constructor <init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V
    .locals 1
    .param p1, "root"    # Lcom/samsung/xml/Node;
    .param p2, "device"    # Lcom/samsung/xml/Node;

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1633
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/Device;->descriptionCache:Ljava/util/LinkedHashMap;

    .line 1672
    new-instance v0, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v0}, Lcom/samsung/http/HTTPResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/Device;->descResoponse:Lcom/samsung/http/HTTPResponse;

    .line 225
    iput-object p1, p0, Lcom/samsung/upnp/Device;->rootNode:Lcom/samsung/xml/Node;

    .line 226
    iput-object p2, p0, Lcom/samsung/upnp/Device;->deviceNode:Lcom/samsung/xml/Node;

    .line 227
    invoke-static {}, Lcom/samsung/upnp/UPnP;->createUUID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/upnp/Device;->setUUID(Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->setWirelessMode(Z)V

    .line 229
    return-void
.end method

.method private clearDescriptionCache()V
    .locals 2

    .prologue
    .line 1635
    iget-object v1, p0, Lcom/samsung/upnp/Device;->descriptionCache:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 1636
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/Device;->descriptionCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1635
    monitor-exit v1

    .line 1638
    return-void

    .line 1635
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private deviceActionControlRecieved(Lcom/samsung/upnp/control/ActionRequest;Lcom/samsung/upnp/Service;)V
    .locals 5
    .param p1, "ctlReq"    # Lcom/samsung/upnp/control/ActionRequest;
    .param p2, "service"    # Lcom/samsung/upnp/Service;

    .prologue
    .line 1784
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1800
    :cond_0
    :goto_0
    return-void

    .line 1786
    :cond_1
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1787
    invoke-virtual {p1}, Lcom/samsung/upnp/control/ActionRequest;->print()V

    .line 1788
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/upnp/control/ActionRequest;->getActionName()Ljava/lang/String;

    move-result-object v2

    .line 1789
    .local v2, "actionName":Ljava/lang/String;
    invoke-virtual {p2, v2}, Lcom/samsung/upnp/Service;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1790
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_3

    .line 1791
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/upnp/control/ControlRequest;)V

    goto :goto_0

    .line 1795
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v1

    .line 1796
    .local v1, "actionArgList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {p1}, Lcom/samsung/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v3

    .line 1797
    .local v3, "reqArgList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {v1, v3}, Lcom/samsung/upnp/ArgumentList;->set(Lcom/samsung/upnp/ArgumentList;)V

    .line 1798
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Action;->performActionListener(Lcom/samsung/upnp/control/ActionRequest;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1799
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/upnp/control/ControlRequest;)V

    goto :goto_0
.end method

.method private deviceControlRequestRecieved(Lcom/samsung/upnp/control/ControlRequest;Lcom/samsung/upnp/Service;)V
    .locals 1
    .param p1, "ctlReq"    # Lcom/samsung/upnp/control/ControlRequest;
    .param p2, "service"    # Lcom/samsung/upnp/Service;

    .prologue
    .line 1765
    if-nez p1, :cond_0

    .line 1771
    :goto_0
    return-void

    .line 1767
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/control/ControlRequest;->isQueryControl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1768
    new-instance v0, Lcom/samsung/upnp/control/QueryRequest;

    invoke-direct {v0, p1}, Lcom/samsung/upnp/control/QueryRequest;-><init>(Lcom/samsung/http/HTTPRequest;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/upnp/Device;->deviceQueryControlRecieved(Lcom/samsung/upnp/control/QueryRequest;Lcom/samsung/upnp/Service;)V

    goto :goto_0

    .line 1770
    :cond_1
    new-instance v0, Lcom/samsung/upnp/control/ActionRequest;

    invoke-direct {v0, p1}, Lcom/samsung/upnp/control/ActionRequest;-><init>(Lcom/samsung/http/HTTPRequest;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/upnp/Device;->deviceActionControlRecieved(Lcom/samsung/upnp/control/ActionRequest;Lcom/samsung/upnp/Service;)V

    goto :goto_0
.end method

.method private deviceEventNewSubscriptionRecieved(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/SubscriptionRequest;)V
    .locals 9
    .param p1, "service"    # Lcom/samsung/upnp/Service;
    .param p2, "subReq"    # Lcom/samsung/upnp/event/SubscriptionRequest;

    .prologue
    .line 1883
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1925
    :cond_0
    :goto_0
    return-void

    .line 1885
    :cond_1
    const-string v7, "Device"

    const-string v8, "Subscription : deviceEventNewSubscriptionRecieved"

    invoke-static {v7, v8}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1887
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->getCallback()Ljava/lang/String;

    move-result-object v0

    .line 1889
    .local v0, "callback":Ljava/lang/String;
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1896
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->getTimeout()J

    move-result-wide v5

    .line 1897
    .local v5, "timeOut":J
    invoke-static {}, Lcom/samsung/upnp/event/Subscription;->createSID()Ljava/lang/String;

    move-result-object v2

    .line 1899
    .local v2, "sid":Ljava/lang/String;
    new-instance v3, Lcom/samsung/upnp/event/Subscriber;

    invoke-direct {v3}, Lcom/samsung/upnp/event/Subscriber;-><init>()V

    .line 1900
    .local v3, "sub":Lcom/samsung/upnp/event/Subscriber;
    invoke-virtual {v3, v0}, Lcom/samsung/upnp/event/Subscriber;->setDeliveryURL(Ljava/lang/String;)V

    .line 1901
    invoke-virtual {v3, v5, v6}, Lcom/samsung/upnp/event/Subscriber;->setTimeOut(J)V

    .line 1902
    invoke-virtual {v3, v2}, Lcom/samsung/upnp/event/Subscriber;->setSID(Ljava/lang/String;)V

    .line 1903
    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Service;->addSubscriber(Lcom/samsung/upnp/event/Subscriber;)V

    .line 1905
    new-instance v4, Lcom/samsung/upnp/event/SubscriptionResponse;

    invoke-direct {v4}, Lcom/samsung/upnp/event/SubscriptionResponse;-><init>()V

    .line 1906
    .local v4, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    const/16 v7, 0xc8

    invoke-virtual {v4, v7}, Lcom/samsung/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 1907
    invoke-virtual {v4, v2}, Lcom/samsung/upnp/event/SubscriptionResponse;->setSID(Ljava/lang/String;)V

    .line 1908
    invoke-virtual {v4, v5, v6}, Lcom/samsung/upnp/event/SubscriptionResponse;->setTimeout(J)V

    .line 1909
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1910
    invoke-virtual {v4}, Lcom/samsung/upnp/event/SubscriptionResponse;->print()V

    .line 1911
    :cond_2
    invoke-virtual {p2, v4}, Lcom/samsung/upnp/event/SubscriptionRequest;->post(Lcom/samsung/upnp/event/SubscriptionResponse;)V

    .line 1913
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->isKeepAliveConnection()Z

    move-result v7

    if-nez v7, :cond_3

    .line 1914
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->getSocket()Lcom/samsung/http/HTTPSocket;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/http/HTTPSocket;->close()Z

    .line 1920
    :cond_3
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1921
    invoke-virtual {v4}, Lcom/samsung/upnp/event/SubscriptionResponse;->print()V

    .line 1922
    :cond_4
    const-string v7, "Device"

    const-string v8, "Subscription : Notify To Subscriber"

    invoke-static {v7, v8}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1923
    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Service;->notifyAllStateVariablesToSubscriber(Lcom/samsung/upnp/event/Subscriber;)V

    goto :goto_0

    .line 1891
    .end local v2    # "sid":Ljava/lang/String;
    .end local v3    # "sub":Lcom/samsung/upnp/event/Subscriber;
    .end local v4    # "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    .end local v5    # "timeOut":J
    :catch_0
    move-exception v1

    .line 1892
    .local v1, "e":Ljava/lang/Exception;
    const/16 v7, 0x19c

    invoke-direct {p0, p2, v7}, Lcom/samsung/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;I)V

    goto :goto_0
.end method

.method private deviceEventRenewSubscriptionRecieved(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/SubscriptionRequest;)V
    .locals 6
    .param p1, "service"    # Lcom/samsung/upnp/Service;
    .param p2, "subReq"    # Lcom/samsung/upnp/event/SubscriptionRequest;

    .prologue
    .line 1930
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1952
    :cond_0
    :goto_0
    return-void

    .line 1932
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->getSID()Ljava/lang/String;

    move-result-object v0

    .line 1933
    .local v0, "sid":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/samsung/upnp/Service;->getSubscriber(Ljava/lang/String;)Lcom/samsung/upnp/event/Subscriber;

    move-result-object v1

    .line 1935
    .local v1, "sub":Lcom/samsung/upnp/event/Subscriber;
    if-nez v1, :cond_2

    .line 1936
    const/16 v5, 0x19c

    invoke-direct {p0, p2, v5}, Lcom/samsung/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1940
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->getTimeout()J

    move-result-wide v3

    .line 1941
    .local v3, "timeOut":J
    invoke-virtual {v1, v3, v4}, Lcom/samsung/upnp/event/Subscriber;->setTimeOut(J)V

    .line 1942
    invoke-virtual {v1}, Lcom/samsung/upnp/event/Subscriber;->renew()V

    .line 1944
    new-instance v2, Lcom/samsung/upnp/event/SubscriptionResponse;

    invoke-direct {v2}, Lcom/samsung/upnp/event/SubscriptionResponse;-><init>()V

    .line 1945
    .local v2, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    const/16 v5, 0xc8

    invoke-virtual {v2, v5}, Lcom/samsung/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 1946
    invoke-virtual {v2, v0}, Lcom/samsung/upnp/event/SubscriptionResponse;->setSID(Ljava/lang/String;)V

    .line 1947
    invoke-virtual {v2, v3, v4}, Lcom/samsung/upnp/event/SubscriptionResponse;->setTimeout(J)V

    .line 1948
    invoke-virtual {p2, v2}, Lcom/samsung/upnp/event/SubscriptionRequest;->post(Lcom/samsung/upnp/event/SubscriptionResponse;)V

    .line 1950
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1951
    invoke-virtual {v2}, Lcom/samsung/upnp/event/SubscriptionResponse;->print()V

    goto :goto_0
.end method

.method private deviceEventSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;)V
    .locals 4
    .param p1, "subReq"    # Lcom/samsung/upnp/event/SubscriptionRequest;

    .prologue
    const/16 v3, 0x19c

    .line 1834
    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    .line 1835
    .local v1, "uri":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/Device;->getServiceByEventSubURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v0

    .line 1836
    .local v0, "service":Lcom/samsung/upnp/Service;
    if-nez v0, :cond_0

    .line 1837
    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->returnBadRequest()Z

    .line 1871
    :goto_0
    return-void

    .line 1840
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->hasCallback()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->hasSID()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1841
    invoke-direct {p0, p1, v3}, Lcom/samsung/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1846
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->isUnsubscribeRequest()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1847
    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Device;->deviceEventUnsubscriptionRecieved(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1852
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->hasCallback()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1853
    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Device;->deviceEventNewSubscriptionRecieved(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1865
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->hasSID()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1866
    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Device;->deviceEventRenewSubscriptionRecieved(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1870
    :cond_4
    invoke-direct {p0, p1, v3}, Lcom/samsung/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;I)V

    goto :goto_0
.end method

.method private deviceEventUnsubscriptionRecieved(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/SubscriptionRequest;)V
    .locals 4
    .param p1, "service"    # Lcom/samsung/upnp/Service;
    .param p2, "subReq"    # Lcom/samsung/upnp/event/SubscriptionRequest;

    .prologue
    .line 1956
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1978
    :cond_0
    :goto_0
    return-void

    .line 1958
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/upnp/event/SubscriptionRequest;->getSID()Ljava/lang/String;

    move-result-object v0

    .line 1959
    .local v0, "sid":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/samsung/upnp/Service;->getSubscriber(Ljava/lang/String;)Lcom/samsung/upnp/event/Subscriber;

    move-result-object v1

    .line 1961
    .local v1, "sub":Lcom/samsung/upnp/event/Subscriber;
    if-nez v1, :cond_2

    .line 1962
    const/16 v3, 0x19c

    invoke-direct {p0, p2, v3}, Lcom/samsung/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1967
    :cond_2
    monitor-enter p0

    .line 1968
    :try_start_0
    invoke-virtual {p1, v1}, Lcom/samsung/upnp/Service;->removeSubscriber(Lcom/samsung/upnp/event/Subscriber;)V

    .line 1967
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1972
    new-instance v2, Lcom/samsung/upnp/event/SubscriptionResponse;

    invoke-direct {v2}, Lcom/samsung/upnp/event/SubscriptionResponse;-><init>()V

    .line 1973
    .local v2, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    const/16 v3, 0xc8

    invoke-virtual {v2, v3}, Lcom/samsung/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 1974
    invoke-virtual {p2, v2}, Lcom/samsung/upnp/event/SubscriptionRequest;->post(Lcom/samsung/upnp/event/SubscriptionResponse;)V

    .line 1976
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1977
    invoke-virtual {v2}, Lcom/samsung/upnp/event/SubscriptionResponse;->print()V

    goto :goto_0

    .line 1967
    .end local v2    # "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private deviceQueryControlRecieved(Lcom/samsung/upnp/control/QueryRequest;Lcom/samsung/upnp/Service;)V
    .locals 3
    .param p1, "ctlReq"    # Lcom/samsung/upnp/control/QueryRequest;
    .param p2, "service"    # Lcom/samsung/upnp/Service;

    .prologue
    .line 1804
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1816
    :cond_0
    :goto_0
    return-void

    .line 1806
    :cond_1
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1807
    invoke-virtual {p1}, Lcom/samsung/upnp/control/QueryRequest;->print()V

    .line 1808
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/upnp/control/QueryRequest;->getVarName()Ljava/lang/String;

    move-result-object v1

    .line 1809
    .local v1, "varName":Ljava/lang/String;
    invoke-virtual {p2, v1}, Lcom/samsung/upnp/Service;->hasStateVariable(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1810
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/upnp/control/ControlRequest;)V

    goto :goto_0

    .line 1813
    :cond_3
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v0

    .line 1814
    .local v0, "stateVar":Lcom/samsung/upnp/StateVariable;
    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/StateVariable;->performQueryListener(Lcom/samsung/upnp/control/QueryRequest;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1815
    :cond_4
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/upnp/control/ControlRequest;)V

    goto :goto_0
.end method

.method private getAdvertiser()Lcom/samsung/upnp/device/Advertiser;
    .locals 1

    .prologue
    .line 2001
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getAdvertiser()Lcom/samsung/upnp/device/Advertiser;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized getDescriptionData(Ljava/lang/String;)[B
    .locals 4
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 1620
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v1

    .line 1621
    .local v1, "rootNode":Lcom/samsung/xml/Node;
    if-nez v1, :cond_0

    .line 1622
    const/4 v2, 0x0

    new-array v2, v2, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1630
    :goto_0
    monitor-exit p0

    return-object v2

    .line 1625
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 1626
    .local v0, "desc":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "<?xml version=\"1.0\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1627
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1628
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1630
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 1620
    .end local v0    # "desc":Ljava/lang/String;
    .end local v1    # "rootNode":Lcom/samsung/xml/Node;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private getDescriptionURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getDescriptionURI()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceData()Lcom/samsung/upnp/xml/DeviceData;
    .locals 2

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 376
    .local v0, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/xml/DeviceData;

    .line 377
    .local v1, "userData":Lcom/samsung/upnp/xml/DeviceData;
    if-nez v1, :cond_0

    .line 378
    new-instance v1, Lcom/samsung/upnp/xml/DeviceData;

    .end local v1    # "userData":Lcom/samsung/upnp/xml/DeviceData;
    invoke-direct {v1}, Lcom/samsung/upnp/xml/DeviceData;-><init>()V

    .line 379
    .restart local v1    # "userData":Lcom/samsung/upnp/xml/DeviceData;
    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 380
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/xml/DeviceData;->setNode(Lcom/samsung/xml/Node;)V

    .line 382
    :cond_0
    return-object v1
.end method

.method private getHTTPServerList()Lcom/samsung/http/HTTPServerList;
    .locals 1

    .prologue
    .line 1986
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getHTTPServerList()Lcom/samsung/http/HTTPServerList;

    move-result-object v0

    return-object v0
.end method

.method private getNotifyDeviceNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1182
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1183
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v0

    .line 1184
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "upnp:rootdevice"

    goto :goto_0
.end method

.method private getNotifyDeviceTypeNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1196
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getNotifyDeviceTypeUSN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSSDPSearchSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    .locals 1

    .prologue
    .line 1991
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getSSDPSearchSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;

    move-result-object v0

    return-object v0
.end method

.method private getUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/upnp/Device;->devUUID:Ljava/lang/String;

    return-object v0
.end method

.method private httpGetRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 5
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1675
    if-nez p1, :cond_0

    .line 1699
    :goto_0
    return-void

    .line 1677
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v2

    .line 1679
    .local v2, "uri":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 1680
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0

    .line 1684
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getLocalAddress()Ljava/lang/String;

    move-result-object v1

    .line 1686
    .local v1, "localAddr":Ljava/lang/String;
    invoke-direct {p0, v1, v2}, Lcom/samsung/upnp/Device;->requestDescriptionData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 1688
    .local v0, "fileByte":[B
    if-nez v0, :cond_2

    .line 1689
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0

    .line 1693
    :cond_2
    iget-object v3, p0, Lcom/samsung/upnp/Device;->descResoponse:Lcom/samsung/http/HTTPResponse;

    invoke-virtual {v3}, Lcom/samsung/http/HTTPResponse;->clearHeaders()V

    .line 1694
    invoke-static {v2}, Lcom/samsung/util/FileUtil;->isXMLFileName(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1695
    iget-object v3, p0, Lcom/samsung/upnp/Device;->descResoponse:Lcom/samsung/http/HTTPResponse;

    const-string v4, "text/xml; charset=\"utf-8\""

    invoke-virtual {v3, v4}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 1696
    :cond_3
    iget-object v3, p0, Lcom/samsung/upnp/Device;->descResoponse:Lcom/samsung/http/HTTPResponse;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 1697
    iget-object v3, p0, Lcom/samsung/upnp/Device;->descResoponse:Lcom/samsung/http/HTTPResponse;

    invoke-virtual {v3, v0}, Lcom/samsung/http/HTTPResponse;->setContent([B)V

    .line 1698
    iget-object v3, p0, Lcom/samsung/upnp/Device;->descResoponse:Lcom/samsung/http/HTTPResponse;

    invoke-virtual {p1, v3}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private httpHostRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 4
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1595
    if-nez p1, :cond_0

    .line 1609
    :goto_0
    return-void

    .line 1597
    :cond_0
    new-instance v1, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v1}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 1598
    .local v1, "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v0

    .line 1599
    .local v0, "header":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1600
    const-string v2, "contentFeatures.dlna.org"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1601
    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 1604
    :cond_1
    const-string v2, "Device_Header"

    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    const-string v2, "Device_Header"

    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getHeaderString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    invoke-virtual {p1, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private httpPostRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 1
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1709
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isSOAPAction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->soapActionRecieved(Lcom/samsung/http/HTTPRequest;)V

    .line 1716
    :goto_0
    return-void

    .line 1715
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0
.end method

.method private initializeLoadedDescription()Z
    .locals 1

    .prologue
    .line 498
    const-string v0, "/description.xml"

    invoke-direct {p0, v0}, Lcom/samsung/upnp/Device;->setDescriptionURI(Ljava/lang/String;)V

    .line 499
    const/16 v0, 0x708

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->setLeaseTime(I)V

    .line 500
    const/16 v0, 0xfa4

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->setHTTPPort(I)V

    .line 503
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->hasUDN()Z

    move-result v0

    if-nez v0, :cond_0

    .line 504
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->updateUDN()V

    .line 506
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private invalidActionControlRecieved(Lcom/samsung/upnp/control/ControlRequest;)V
    .locals 2
    .param p1, "ctlReq"    # Lcom/samsung/upnp/control/ControlRequest;

    .prologue
    .line 1775
    if-nez p1, :cond_0

    .line 1780
    :goto_0
    return-void

    .line 1777
    :cond_0
    new-instance v0, Lcom/samsung/upnp/control/ActionResponse;

    invoke-direct {v0}, Lcom/samsung/upnp/control/ActionResponse;-><init>()V

    .line 1778
    .local v0, "actRes":Lcom/samsung/upnp/control/ControlResponse;
    const/16 v1, 0x191

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/control/ControlResponse;->setFaultResponse(I)V

    .line 1779
    invoke-virtual {p1, v0}, Lcom/samsung/upnp/control/ControlRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private isDescriptionURI(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 411
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDescriptionURI()Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, "descriptionURI":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 413
    :cond_0
    const/4 v1, 0x0

    .line 414
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isDeviceNode(Lcom/samsung/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 515
    const-string v0, "device"

    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static final notifyWait()V
    .locals 1

    .prologue
    .line 1206
    const/16 v0, 0x64

    invoke-static {v0}, Lcom/samsung/util/TimerUtil;->waitRandom(I)V

    .line 1207
    return-void
.end method

.method private requestDescriptionData(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 6
    .param p1, "localAddr"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 1641
    iget-object v5, p0, Lcom/samsung/upnp/Device;->descriptionCache:Ljava/util/LinkedHashMap;

    monitor-enter v5

    .line 1642
    :try_start_0
    iget-object v4, p0, Lcom/samsung/upnp/Device;->descriptionCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 1644
    .local v0, "buffer":[B
    if-eqz v0, :cond_0

    .line 1645
    monitor-exit v5

    move-object v1, v0

    .line 1661
    .end local v0    # "buffer":[B
    .local v1, "buffer":[B
    :goto_0
    return-object v1

    .line 1650
    .end local v1    # "buffer":[B
    .restart local v0    # "buffer":[B
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/upnp/Device;->isDescriptionURI(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1651
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->getDescriptionData(Ljava/lang/String;)[B

    move-result-object v0

    .line 1659
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 1660
    iget-object v4, p0, Lcom/samsung/upnp/Device;->descriptionCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1661
    :cond_2
    monitor-exit v5

    move-object v1, v0

    .end local v0    # "buffer":[B
    .restart local v1    # "buffer":[B
    goto :goto_0

    .line 1653
    .end local v1    # "buffer":[B
    .restart local v0    # "buffer":[B
    :cond_3
    invoke-virtual {p0, p2}, Lcom/samsung/upnp/Device;->getDeviceByDescriptionURI(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v2

    .local v2, "embDev":Lcom/samsung/upnp/Device;
    if-eqz v2, :cond_4

    .line 1654
    invoke-direct {v2, p1}, Lcom/samsung/upnp/Device;->getDescriptionData(Ljava/lang/String;)[B

    move-result-object v0

    .line 1655
    goto :goto_1

    .line 1656
    :cond_4
    invoke-virtual {p0, p2}, Lcom/samsung/upnp/Device;->getServiceBySCPDURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v3

    .local v3, "embService":Lcom/samsung/upnp/Service;
    if-eqz v3, :cond_1

    .line 1657
    invoke-virtual {v3}, Lcom/samsung/upnp/Service;->getSCPDData()[B

    move-result-object v0

    goto :goto_1

    .line 1641
    .end local v0    # "buffer":[B
    .end local v2    # "embDev":Lcom/samsung/upnp/Device;
    .end local v3    # "embService":Lcom/samsung/upnp/Service;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method private setAdvertiser(Lcom/samsung/upnp/device/Advertiser;)V
    .locals 1
    .param p1, "adv"    # Lcom/samsung/upnp/device/Advertiser;

    .prologue
    .line 1996
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/DeviceData;->setAdvertiser(Lcom/samsung/upnp/device/Advertiser;)V

    .line 1997
    return-void
.end method

.method private setDescriptionFile(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 391
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/DeviceData;->setDescriptionFile(Ljava/io/File;)V

    .line 392
    return-void
.end method

.method private setDescriptionURI(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/DeviceData;->setDescriptionURI(Ljava/lang/String;)V

    .line 402
    return-void
.end method

.method private setUUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/samsung/upnp/Device;->devUUID:Ljava/lang/String;

    .line 326
    return-void
.end method

.method private soapActionRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 3
    .param p1, "soapReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1739
    if-nez p1, :cond_0

    .line 1757
    :goto_0
    return-void

    .line 1741
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v2

    .line 1742
    .local v2, "uri":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/samsung/upnp/Device;->getServiceByControlURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v1

    .line 1743
    .local v1, "ctlService":Lcom/samsung/upnp/Service;
    if-eqz v1, :cond_1

    .line 1744
    new-instance v0, Lcom/samsung/upnp/control/ActionRequest;

    invoke-direct {v0, p1}, Lcom/samsung/upnp/control/ActionRequest;-><init>(Lcom/samsung/http/HTTPRequest;)V

    .line 1745
    .local v0, "crlReq":Lcom/samsung/upnp/control/ActionRequest;
    invoke-direct {p0, v0, v1}, Lcom/samsung/upnp/Device;->deviceControlRequestRecieved(Lcom/samsung/upnp/control/ControlRequest;Lcom/samsung/upnp/Service;)V

    goto :goto_0

    .line 1756
    .end local v0    # "crlReq":Lcom/samsung/upnp/control/ActionRequest;
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->soapBadActionRecieved(Lcom/samsung/http/HTTPRequest;)V

    goto :goto_0
.end method

.method private soapBadActionRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 2
    .param p1, "soapReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1727
    if-nez p1, :cond_0

    .line 1732
    :goto_0
    return-void

    .line 1729
    :cond_0
    new-instance v0, Lcom/samsung/soap/SOAPResponse;

    invoke-direct {v0}, Lcom/samsung/soap/SOAPResponse;-><init>()V

    .line 1730
    .local v0, "soapRes":Lcom/samsung/soap/SOAPResponse;
    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/samsung/soap/SOAPResponse;->setStatusCode(I)V

    .line 1731
    invoke-virtual {p1, v0}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private stop(Z)Z
    .locals 6
    .param p1, "doByeBye"    # Z

    .prologue
    .line 2106
    const/4 v2, 0x0

    .line 2107
    .local v2, "isHttpClosed":Z
    const/4 v3, 0x0

    .line 2109
    .local v3, "isSsdpClosed":Z
    if-eqz p1, :cond_0

    .line 2110
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->byebye()V

    .line 2112
    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getHTTPServerList()Lcom/samsung/http/HTTPServerList;

    move-result-object v1

    .line 2113
    .local v1, "httpServerList":Lcom/samsung/http/HTTPServerList;
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServerList;->stop()V

    .line 2114
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServerList;->close()Z

    move-result v2

    .line 2115
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServerList;->clear()V

    .line 2117
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getSSDPSearchSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;

    move-result-object v4

    .line 2118
    .local v4, "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->stop()V

    .line 2119
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->close()Z

    move-result v3

    .line 2120
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->clear()V

    .line 2122
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getAdvertiser()Lcom/samsung/upnp/device/Advertiser;

    move-result-object v0

    .line 2123
    .local v0, "adv":Lcom/samsung/upnp/device/Advertiser;
    if-eqz v0, :cond_1

    .line 2124
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Advertiser;->stop()V

    .line 2125
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/samsung/upnp/Device;->setAdvertiser(Lcom/samsung/upnp/device/Advertiser;)V

    .line 2128
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private updateUDN()V
    .locals 2

    .prologue
    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "uuid:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getUUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->setUDN(Ljava/lang/String;)V

    .line 336
    return-void
.end method

.method private upnpBadSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;I)V
    .locals 1
    .param p1, "subReq"    # Lcom/samsung/upnp/event/SubscriptionRequest;
    .param p2, "code"    # I

    .prologue
    .line 1824
    if-nez p1, :cond_0

    .line 1829
    :goto_0
    return-void

    .line 1826
    :cond_0
    new-instance v0, Lcom/samsung/upnp/event/SubscriptionResponse;

    invoke-direct {v0}, Lcom/samsung/upnp/event/SubscriptionResponse;-><init>()V

    .line 1827
    .local v0, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    invoke-virtual {v0, p2}, Lcom/samsung/upnp/event/SubscriptionResponse;->setErrorResponse(I)V

    .line 1828
    invoke-virtual {p1, v0}, Lcom/samsung/upnp/event/SubscriptionRequest;->post(Lcom/samsung/upnp/event/SubscriptionResponse;)V

    goto :goto_0
.end method


# virtual methods
.method public announce()V
    .locals 8

    .prologue
    .line 1280
    invoke-static {}, Lcom/samsung/upnp/Device;->notifyWait()V

    .line 1282
    invoke-static {}, Lcom/samsung/net/HostInterface;->getNHostAddresses()I

    move-result v4

    .line 1283
    .local v4, "nHostAddrs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v4, :cond_0

    .line 1298
    return-void

    .line 1284
    :cond_0
    invoke-static {v3}, Lcom/samsung/net/HostInterface;->getHostAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 1285
    .local v0, "bindAddr":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_2

    .line 1283
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1287
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPAnnounceCount()I

    move-result v5

    .line 1288
    .local v5, "ssdpCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_1

    .line 1289
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->announce(Ljava/lang/String;)V

    .line 1291
    const-wide/16 v6, 0xc8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1288
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1292
    :catch_0
    move-exception v1

    .line 1294
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method public announce(Ljava/lang/String;)V
    .locals 16
    .param p1, "bindAddr"    # Ljava/lang/String;

    .prologue
    .line 1220
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/upnp/Device;->getLocationURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1222
    .local v4, "devLocation":Ljava/lang/String;
    new-instance v13, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;-><init>(Ljava/lang/String;)V

    .line 1224
    .local v13, "ssdpSock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    new-instance v12, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;

    invoke-direct {v12}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;-><init>()V

    .line 1225
    .local v12, "ssdpReq":Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setServer(Ljava/lang/String;)V

    .line 1226
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getLeaseTime()I

    move-result v14

    invoke-virtual {v12, v14}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setLeaseTime(I)V

    .line 1227
    invoke-virtual {v12, v4}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setLocation(Ljava/lang/String;)V

    .line 1228
    const-string v14, "ssdp:alive"

    invoke-virtual {v12, v14}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 1231
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v6

    .line 1232
    .local v6, "devUDN":Ljava/lang/String;
    invoke-virtual {v12, v6}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1234
    invoke-virtual {v12, v6}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1235
    invoke-virtual {v13, v12}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1239
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1240
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceNT()Ljava/lang/String;

    move-result-object v5

    .line 1241
    .local v5, "devNT":Ljava/lang/String;
    invoke-virtual {v12, v5}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1242
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, "::upnp:rootdevice"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1243
    invoke-virtual {v13, v12}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1247
    .end local v5    # "devNT":Ljava/lang/String;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceTypeUSN()Ljava/lang/String;

    move-result-object v7

    .line 1248
    .local v7, "devUSN":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceTypeNT()Ljava/lang/String;

    move-result-object v5

    .line 1249
    .restart local v5    # "devNT":Ljava/lang/String;
    invoke-virtual {v12, v5}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1250
    invoke-virtual {v12, v7}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1251
    invoke-virtual {v13, v12}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1254
    invoke-virtual {v13}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->close()Z

    .line 1256
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v11

    .line 1257
    .local v11, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v11}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v10

    .line 1258
    .local v10, "serviceCnt":I
    const/4 v8, 0x0

    .local v8, "n":I
    :goto_0
    if-lt v8, v10, :cond_1

    .line 1263
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v3

    .line 1264
    .local v3, "childDeviceList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v2

    .line 1265
    .local v2, "childDeviceCnt":I
    const/4 v8, 0x0

    :goto_1
    if-lt v8, v2, :cond_2

    .line 1270
    return-void

    .line 1259
    .end local v2    # "childDeviceCnt":I
    .end local v3    # "childDeviceList":Lcom/samsung/upnp/DeviceList;
    :cond_1
    invoke-virtual {v11, v8}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v9

    .line 1260
    .local v9, "service":Lcom/samsung/upnp/Service;
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/samsung/upnp/Service;->announce(Ljava/lang/String;)V

    .line 1258
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1266
    .end local v9    # "service":Lcom/samsung/upnp/Service;
    .restart local v2    # "childDeviceCnt":I
    .restart local v3    # "childDeviceList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v3, v8}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 1267
    .local v1, "childDevice":Lcom/samsung/upnp/Device;
    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/samsung/upnp/Device;->announce(Ljava/lang/String;)V

    .line 1265
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public byebye()V
    .locals 6

    .prologue
    .line 1385
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->clearDescriptionCache()V

    .line 1386
    invoke-static {}, Lcom/samsung/net/HostInterface;->getNHostAddresses()I

    move-result v3

    .line 1387
    .local v3, "nHostAddrs":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 1395
    return-void

    .line 1388
    :cond_0
    invoke-static {v2}, Lcom/samsung/net/HostInterface;->getHostAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 1389
    .local v0, "bindAddr":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 1387
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1391
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPAnnounceCount()I

    move-result v4

    .line 1392
    .local v4, "ssdpCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_1

    .line 1393
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->byebye(Ljava/lang/String;)V

    .line 1392
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public byebye(Ljava/lang/String;)V
    .locals 12
    .param p1, "bindAddr"    # Ljava/lang/String;

    .prologue
    .line 1335
    new-instance v10, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    invoke-direct {v10, p1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;-><init>(Ljava/lang/String;)V

    .line 1337
    .local v10, "ssdpSock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    new-instance v9, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;

    invoke-direct {v9}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;-><init>()V

    .line 1338
    .local v9, "ssdpReq":Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;
    const-string v11, "ssdp:byebye"

    invoke-virtual {v9, v11}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 1341
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceTypeUSN()Ljava/lang/String;

    move-result-object v4

    .line 1342
    .local v4, "devUSN":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1343
    invoke-virtual {v9, v4}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1344
    invoke-virtual {v10, v9}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1347
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1348
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceNT()Ljava/lang/String;

    move-result-object v3

    .line 1349
    .local v3, "devNT":Ljava/lang/String;
    invoke-virtual {v9, v3}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1350
    invoke-virtual {v9, v4}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1351
    invoke-virtual {v10, v9}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1355
    .end local v3    # "devNT":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceTypeNT()Ljava/lang/String;

    move-result-object v3

    .line 1356
    .restart local v3    # "devNT":Ljava/lang/String;
    invoke-virtual {v9, v3}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1357
    invoke-virtual {v9, v4}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1358
    invoke-virtual {v10, v9}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1361
    invoke-virtual {v10}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->close()Z

    .line 1363
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v8

    .line 1364
    .local v8, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v8}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v7

    .line 1365
    .local v7, "serviceCnt":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-lt v5, v7, :cond_1

    .line 1370
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 1371
    .local v2, "childDeviceList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 1372
    .local v1, "childDeviceCnt":I
    const/4 v5, 0x0

    :goto_1
    if-lt v5, v1, :cond_2

    .line 1376
    return-void

    .line 1366
    .end local v1    # "childDeviceCnt":I
    .end local v2    # "childDeviceList":Lcom/samsung/upnp/DeviceList;
    :cond_1
    invoke-virtual {v8, v5}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v6

    .line 1367
    .local v6, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v6, p1}, Lcom/samsung/upnp/Service;->byebye(Ljava/lang/String;)V

    .line 1365
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1373
    .end local v6    # "service":Lcom/samsung/upnp/Service;
    .restart local v1    # "childDeviceCnt":I
    .restart local v2    # "childDeviceList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v5}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1374
    .local v0, "childDevice":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->byebye(Ljava/lang/String;)V

    .line 1372
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public deviceSearchReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 0
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 1530
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/Device;->deviceSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 1531
    return-void
.end method

.method public deviceSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 18
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 1455
    const-string v15, "Device"

    const-string v16, "SSDP : Device Search Response"

    invoke-static/range {v15 .. v16}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isValid()Z

    move-result v15

    if-nez v15, :cond_1

    .line 1523
    :cond_0
    return-void

    .line 1460
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v14

    .line 1462
    .local v14, "ssdpST":Ljava/lang/String;
    if-eqz v14, :cond_0

    .line 1465
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v9

    .line 1467
    .local v9, "isRootDevice":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v8

    .line 1468
    .local v8, "devUSN":Ljava/lang/String;
    if-eqz v9, :cond_2

    .line 1469
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, "::upnp:rootdevice"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1471
    :cond_2
    invoke-static {v14}, Lcom/samsung/upnp/device/ST;->isAllDevice(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1472
    const-string v15, "Device"

    const-string v16, "SSDP : All Device"

    invoke-static/range {v15 .. v16}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "DeviceSSDP : All Device"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1474
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v7

    .line 1475
    .local v7, "devUDN":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v7, v7}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1476
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceNT()Ljava/lang/String;

    move-result-object v5

    .line 1477
    .local v5, "devNT":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v8}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1478
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceTypeNT()Ljava/lang/String;

    move-result-object v5

    .line 1479
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getNotifyDeviceTypeUSN()Ljava/lang/String;

    move-result-object v8

    .line 1480
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v8}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1509
    .end local v5    # "devNT":Ljava/lang/String;
    .end local v7    # "devUDN":Ljava/lang/String;
    :cond_3
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v13

    .line 1510
    .local v13, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v13}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v12

    .line 1511
    .local v12, "serviceCnt":I
    const/4 v10, 0x0

    .local v10, "n":I
    :goto_1
    if-lt v10, v12, :cond_7

    .line 1517
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v4

    .line 1518
    .local v4, "childDeviceList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v4}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v3

    .line 1519
    .local v3, "childDeviceCnt":I
    const/4 v10, 0x0

    :goto_2
    if-ge v10, v3, :cond_0

    .line 1520
    invoke-virtual {v4, v10}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 1521
    .local v2, "childDevice":Lcom/samsung/upnp/Device;
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/upnp/Device;->deviceSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 1519
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 1482
    .end local v2    # "childDevice":Lcom/samsung/upnp/Device;
    .end local v3    # "childDeviceCnt":I
    .end local v4    # "childDeviceList":Lcom/samsung/upnp/DeviceList;
    .end local v10    # "n":I
    .end local v12    # "serviceCnt":I
    .end local v13    # "serviceList":Lcom/samsung/upnp/ServiceList;
    :cond_4
    invoke-static {v14}, Lcom/samsung/upnp/device/ST;->isRootDevice(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1483
    if-eqz v9, :cond_3

    .line 1484
    const-string v15, "Device"

    const-string v16, "SSDP : Root Device"

    invoke-static/range {v15 .. v16}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "DeviceSSDP : Root Device"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1486
    const-string v15, "upnp:rootdevice"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v15, v8}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 1489
    :cond_5
    invoke-static {v14}, Lcom/samsung/upnp/device/ST;->isUUIDDevice(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1490
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v7

    .line 1491
    .restart local v7    # "devUDN":Ljava/lang/String;
    invoke-virtual {v14, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1492
    const-string v15, "Device"

    const-string v16, "SSDP : UUID Device"

    invoke-static/range {v15 .. v16}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "DeviceSSDP : UUID Device"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1494
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v7, v7}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 1498
    .end local v7    # "devUDN":Ljava/lang/String;
    :cond_6
    invoke-static {v14}, Lcom/samsung/upnp/device/ST;->isURNDevice(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1499
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v6

    .line 1500
    .local v6, "devType":Ljava/lang/String;
    invoke-virtual {v14, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1502
    const-string v15, "Device"

    const-string v16, "SSDP : URN Device"

    invoke-static/range {v15 .. v16}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1503
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "DeviceSSDP : URN Device"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1504
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, "::"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1505
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v8}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 1512
    .end local v6    # "devType":Ljava/lang/String;
    .restart local v10    # "n":I
    .restart local v12    # "serviceCnt":I
    .restart local v13    # "serviceList":Lcom/samsung/upnp/ServiceList;
    :cond_7
    invoke-virtual {v13, v10}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v11

    .line 1513
    .local v11, "service":Lcom/samsung/upnp/Service;
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v17, " of service "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1514
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/Service;->serviceSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z

    .line 1511
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1
.end method

.method public forceSendAnnounce()V
    .locals 8

    .prologue
    .line 1308
    invoke-static {}, Lcom/samsung/net/HostInterface;->getNHostAddresses()I

    move-result v4

    .line 1309
    .local v4, "nHostAddrs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v4, :cond_0

    .line 1324
    return-void

    .line 1310
    :cond_0
    invoke-static {v3}, Lcom/samsung/net/HostInterface;->getHostAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 1311
    .local v0, "bindAddr":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_2

    .line 1309
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1313
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPAnnounceCount()I

    move-result v5

    .line 1314
    .local v5, "ssdpCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_1

    .line 1315
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Device;->announce(Ljava/lang/String;)V

    .line 1317
    const-wide/16 v6, 0xc8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1314
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1318
    :catch_0
    move-exception v1

    .line 1320
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method public getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;
    .locals 13
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1114
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v11

    .line 1115
    .local v11, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v11}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v10

    .line 1116
    .local v10, "serviceCnt":I
    const/4 v8, 0x0

    .local v8, "n":I
    :goto_0
    if-lt v8, v10, :cond_1

    .line 1130
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v6

    .line 1131
    .local v6, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v5

    .line 1132
    .local v5, "devCnt":I
    const/4 v8, 0x0

    :goto_1
    if-lt v8, v5, :cond_5

    .line 1139
    const/4 v0, 0x0

    .end local v5    # "devCnt":I
    .end local v6    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    :goto_2
    return-object v0

    .line 1117
    :cond_1
    invoke-virtual {v11, v8}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v9

    .line 1118
    .local v9, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v9}, Lcom/samsung/upnp/Service;->getActionList()Lcom/samsung/upnp/ActionList;

    move-result-object v2

    .line 1119
    .local v2, "actionList":Lcom/samsung/upnp/ActionList;
    invoke-virtual {v2}, Lcom/samsung/upnp/ActionList;->size()I

    move-result v1

    .line 1120
    .local v1, "actionCnt":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    if-lt v7, v1, :cond_2

    .line 1116
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1121
    :cond_2
    invoke-virtual {v2, v7}, Lcom/samsung/upnp/ActionList;->getAction(I)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1122
    .local v0, "action":Lcom/samsung/upnp/Action;
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1123
    .local v3, "actionName":Ljava/lang/String;
    if-nez v3, :cond_4

    .line 1120
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1125
    :cond_4
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    goto :goto_2

    .line 1133
    .end local v0    # "action":Lcom/samsung/upnp/Action;
    .end local v1    # "actionCnt":I
    .end local v2    # "actionList":Lcom/samsung/upnp/ActionList;
    .end local v3    # "actionName":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v9    # "service":Lcom/samsung/upnp/Service;
    .restart local v5    # "devCnt":I
    .restart local v6    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_5
    invoke-virtual {v6, v8}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v4

    .line 1134
    .local v4, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v4, p1}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 1135
    .restart local v0    # "action":Lcom/samsung/upnp/Action;
    if-nez v0, :cond_0

    .line 1132
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public getDescriptionFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getDescriptionFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 419
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDescriptionFile()Ljava/io/File;

    move-result-object v0

    .line 420
    .local v0, "descriptionFile":Ljava/io/File;
    if-nez v0, :cond_0

    .line 421
    const-string v1, ""

    .line 422
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 908
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v3

    .line 909
    .local v3, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v2

    .line 910
    .local v2, "devCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-lt v4, v2, :cond_1

    .line 918
    const/4 v1, 0x0

    :cond_0
    :goto_1
    return-object v1

    .line 911
    :cond_1
    invoke-virtual {v3, v4}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 912
    .local v1, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->isDevice(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 914
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 915
    .local v0, "cdev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 916
    goto :goto_1

    .line 910
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getDeviceByDescriptionURI(Ljava/lang/String;)Lcom/samsung/upnp/Device;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 923
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v3

    .line 924
    .local v3, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v2

    .line 925
    .local v2, "devCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-lt v4, v2, :cond_1

    .line 933
    const/4 v1, 0x0

    :cond_0
    :goto_1
    return-object v1

    .line 926
    :cond_1
    invoke-virtual {v3, v4}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 927
    .local v1, "dev":Lcom/samsung/upnp/Device;
    invoke-direct {v1, p1}, Lcom/samsung/upnp/Device;->isDescriptionURI(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 929
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->getDeviceByDescriptionURI(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 930
    .local v0, "cdev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 931
    goto :goto_1

    .line 925
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getDeviceList()Lcom/samsung/upnp/DeviceList;
    .locals 8

    .prologue
    .line 878
    new-instance v1, Lcom/samsung/upnp/DeviceList;

    invoke-direct {v1}, Lcom/samsung/upnp/DeviceList;-><init>()V

    .line 879
    .local v1, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v6

    const-string v7, "deviceList"

    invoke-virtual {v6, v7}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 880
    .local v2, "devListNode":Lcom/samsung/xml/Node;
    if-nez v2, :cond_1

    .line 890
    :cond_0
    return-object v1

    .line 882
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v4

    .line 883
    .local v4, "nNode":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 884
    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v5

    .line 885
    .local v5, "node":Lcom/samsung/xml/Node;
    invoke-static {v5}, Lcom/samsung/upnp/Device;->isDeviceNode(Lcom/samsung/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 883
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 887
    :cond_2
    new-instance v0, Lcom/samsung/upnp/Device;

    invoke-direct {v0, v5}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;)V

    .line 888
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getDeviceNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/upnp/Device;->deviceNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "deviceType"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getElapsedTime()J
    .locals 4

    .prologue
    .line 596
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getTimeStamp()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 695
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 696
    .local v0, "node":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 697
    const-string v1, ""

    .line 698
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "friendlyName"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getHTTPPort()I
    .locals 1

    .prologue
    .line 1544
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getHTTPPort()I

    move-result v0

    return v0
.end method

.method public getIcon(I)Lcom/samsung/upnp/Icon;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 1165
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getIconList()Lcom/samsung/upnp/IconList;

    move-result-object v0

    .line 1166
    .local v0, "iconList":Lcom/samsung/upnp/IconList;
    if-gez p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/upnp/IconList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v1, p1, :cond_0

    .line 1167
    const/4 v1, 0x0

    .line 1168
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/IconList;->getIcon(I)Lcom/samsung/upnp/Icon;

    move-result-object v1

    goto :goto_0
.end method

.method public getIconList()Lcom/samsung/upnp/IconList;
    .locals 8

    .prologue
    .line 1148
    new-instance v1, Lcom/samsung/upnp/IconList;

    invoke-direct {v1}, Lcom/samsung/upnp/IconList;-><init>()V

    .line 1149
    .local v1, "iconList":Lcom/samsung/upnp/IconList;
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v6

    const-string v7, "iconList"

    invoke-virtual {v6, v7}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 1150
    .local v2, "iconListNode":Lcom/samsung/xml/Node;
    if-nez v2, :cond_1

    .line 1160
    :cond_0
    return-object v1

    .line 1152
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v4

    .line 1153
    .local v4, "nNode":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 1154
    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v5

    .line 1155
    .local v5, "node":Lcom/samsung/xml/Node;
    invoke-static {v5}, Lcom/samsung/upnp/Icon;->isIconNode(Lcom/samsung/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1153
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1157
    :cond_2
    new-instance v0, Lcom/samsung/upnp/Icon;

    invoke-direct {v0, v5}, Lcom/samsung/upnp/Icon;-><init>(Lcom/samsung/xml/Node;)V

    .line 1158
    .local v0, "icon":Lcom/samsung/upnp/Icon;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/IconList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getInterfaceAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2142
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 2143
    .local v0, "ssdpPacket":Lcom/samsung/upnp/ssdp/SSDPPacket;
    if-nez v0, :cond_0

    .line 2144
    const-string v1, ""

    .line 2145
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocalAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLeaseTime()I
    .locals 2

    .prologue
    .line 576
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 577
    .local v0, "packet":Lcom/samsung/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 578
    invoke-virtual {v0}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLeaseTime()I

    move-result v1

    .line 579
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/upnp/xml/DeviceData;->getLeaseTime()I

    move-result v1

    goto :goto_0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 2

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 555
    .local v0, "packet":Lcom/samsung/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 556
    invoke-virtual {v0}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 557
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/upnp/xml/DeviceData;->getLocation()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLocationURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 1177
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getHTTPPort()I

    move-result v0

    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDescriptionURI()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/samsung/net/HostInterface;->getHostURL(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getManufacture()Ljava/lang/String;
    .locals 2

    .prologue
    .line 714
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "manufacturer"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 762
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "modelName"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 778
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "modelNumber"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteInterfaceAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2149
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 2150
    .local v0, "ssdpPacket":Lcom/samsung/upnp/ssdp/SSDPPacket;
    if-nez v0, :cond_0

    .line 2151
    const-string v1, ""

    .line 2152
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getRootDevice()Lcom/samsung/upnp/Device;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 344
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v1

    .line 345
    .local v1, "rootNode":Lcom/samsung/xml/Node;
    if-nez v1, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-object v2

    .line 347
    :cond_1
    const-string v3, "device"

    invoke-virtual {v1, v3}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 348
    .local v0, "devNode":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_0

    .line 350
    new-instance v2, Lcom/samsung/upnp/Device;

    invoke-direct {v2, v1, v0}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method public getRootNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/upnp/Device;->rootNode:Lcom/samsung/xml/Node;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/samsung/upnp/Device;->rootNode:Lcom/samsung/xml/Node;

    .line 191
    :goto_0
    return-object v0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/Device;->deviceNode:Lcom/samsung/xml/Node;

    if-nez v0, :cond_1

    .line 190
    const/4 v0, 0x0

    goto :goto_0

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/samsung/upnp/Device;->deviceNode:Lcom/samsung/xml/Node;

    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    goto :goto_0
.end method

.method public getSSDPAnnounceCount()I
    .locals 1

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->isNMPRMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->isWirelessMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const/4 v0, 0x4

    .line 314
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;
    .locals 1

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    const/4 v0, 0x0

    .line 540
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/DeviceData;->getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v0

    goto :goto_0
.end method

.method public getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 959
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 960
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 961
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v5, :cond_1

    .line 967
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 968
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 969
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_2

    .line 976
    const/4 v4, 0x0

    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    return-object v4

    .line 962
    :cond_1
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 963
    .local v4, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/upnp/Service;->isService(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 961
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 970
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 971
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 972
    .restart local v4    # "service":Lcom/samsung/upnp/Service;
    if-nez v4, :cond_0

    .line 969
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getServiceByControlURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;
    .locals 8
    .param p1, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 1003
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 1004
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 1005
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v5, :cond_1

    .line 1011
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 1012
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 1013
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_2

    .line 1020
    const/4 v4, 0x0

    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    return-object v4

    .line 1006
    :cond_1
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1007
    .local v4, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/upnp/Service;->isControlURL(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1005
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1014
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1015
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->getServiceByControlURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1016
    .restart local v4    # "service":Lcom/samsung/upnp/Service;
    if-nez v4, :cond_0

    .line 1013
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getServiceByEventSubURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;
    .locals 8
    .param p1, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 1025
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 1026
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 1027
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v5, :cond_1

    .line 1033
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 1034
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 1035
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_2

    .line 1042
    const/4 v4, 0x0

    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    return-object v4

    .line 1028
    :cond_1
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1029
    .local v4, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/upnp/Service;->isEventSubURL(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1027
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1036
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1037
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->getServiceByEventSubURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1038
    .restart local v4    # "service":Lcom/samsung/upnp/Service;
    if-nez v4, :cond_0

    .line 1035
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getServiceBySCPDURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;
    .locals 8
    .param p1, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 981
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 982
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 983
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v5, :cond_1

    .line 989
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 990
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 991
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_2

    .line 998
    const/4 v4, 0x0

    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    return-object v4

    .line 984
    :cond_1
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 985
    .local v4, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/upnp/Service;->isSCPDURL(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 983
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 992
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 993
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->getServiceBySCPDURL(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 994
    .restart local v4    # "service":Lcom/samsung/upnp/Service;
    if-nez v4, :cond_0

    .line 991
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getServiceList()Lcom/samsung/upnp/ServiceList;
    .locals 8

    .prologue
    .line 942
    new-instance v4, Lcom/samsung/upnp/ServiceList;

    invoke-direct {v4}, Lcom/samsung/upnp/ServiceList;-><init>()V

    .line 943
    .local v4, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v6

    const-string v7, "serviceList"

    invoke-virtual {v6, v7}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v5

    .line 944
    .local v5, "serviceListNode":Lcom/samsung/xml/Node;
    if-nez v5, :cond_1

    .line 954
    :cond_0
    return-object v4

    .line 946
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v1

    .line 947
    .local v1, "nNode":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 948
    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 949
    .local v2, "node":Lcom/samsung/xml/Node;
    invoke-static {v2}, Lcom/samsung/upnp/Service;->isServiceNode(Lcom/samsung/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 947
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 951
    :cond_2
    new-instance v3, Lcom/samsung/upnp/Service;

    invoke-direct {v3, v2}, Lcom/samsung/upnp/Service;-><init>(Lcom/samsung/xml/Node;)V

    .line 952
    .local v3, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4, v3}, Lcom/samsung/upnp/ServiceList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1105
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v0

    return-object v0
.end method

.method public getStateVariable(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;
    .locals 10
    .param p1, "serviceType"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 1074
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    move-object v7, v8

    .line 1100
    :cond_0
    :goto_0
    return-object v7

    .line 1077
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 1078
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 1079
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    if-lt v3, v5, :cond_2

    .line 1091
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 1092
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 1093
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-lt v3, v1, :cond_5

    move-object v7, v8

    .line 1100
    goto :goto_0

    .line 1080
    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1082
    .local v4, "service":Lcom/samsung/upnp/Service;
    if-eqz p1, :cond_4

    .line 1083
    invoke-virtual {v4}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1079
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1086
    :cond_4
    invoke-virtual {v4, p2}, Lcom/samsung/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v7

    .line 1087
    .local v7, "stateVar":Lcom/samsung/upnp/StateVariable;
    if-eqz v7, :cond_3

    goto :goto_0

    .line 1094
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .end local v7    # "stateVar":Lcom/samsung/upnp/StateVariable;
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_5
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1095
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1, p2}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v7

    .line 1096
    .restart local v7    # "stateVar":Lcom/samsung/upnp/StateVariable;
    if-nez v7, :cond_0

    .line 1093
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public getSubscriberService(Ljava/lang/String;)Lcom/samsung/upnp/Service;
    .locals 9
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 1047
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 1048
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 1049
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v5, :cond_1

    .line 1056
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 1057
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 1058
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_2

    .line 1065
    const/4 v4, 0x0

    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    return-object v4

    .line 1050
    :cond_1
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1051
    .local v4, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4}, Lcom/samsung/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v7

    .line 1052
    .local v7, "sid":Ljava/lang/String;
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1049
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1059
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .end local v7    # "sid":Ljava/lang/String;
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1060
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->getSubscriberService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 1061
    .restart local v4    # "service":Lcom/samsung/upnp/Service;
    if-nez v4, :cond_0

    .line 1058
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getTimeStamp()J
    .locals 3

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPPacket()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 589
    .local v0, "packet":Lcom/samsung/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 590
    invoke-virtual {v0}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getTimeStamp()J

    move-result-wide v1

    .line 591
    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method public getUDN()Ljava/lang/String;
    .locals 3

    .prologue
    .line 826
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 827
    .local v0, "deviceNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 828
    const-string v1, ""

    .line 829
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v1

    const-string v2, "UDN"

    invoke-virtual {v1, v2}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getURLBase()Ljava/lang/String;
    .locals 2

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 640
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 641
    .local v0, "rootNode":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_0

    .line 642
    const-string v1, "URLBase"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 644
    .end local v0    # "rootNode":Lcom/samsung/xml/Node;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public hasUDN()Z
    .locals 2

    .prologue
    .line 834
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v0

    .line 835
    .local v0, "udn":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 836
    :cond_0
    const/4 v1, 0x0

    .line 837
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 2
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1555
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1556
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->print()V

    .line 1559
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isHeadRequest()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1560
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->httpHostRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    .line 1580
    :goto_0
    return-void

    .line 1564
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isGetRequest()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1565
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->httpGetRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    goto :goto_0

    .line 1568
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isPostRequest()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1569
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Device;->httpPostRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    goto :goto_0

    .line 1573
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isSubscribeRequest()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isUnsubscribeRequest()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1574
    :cond_4
    new-instance v0, Lcom/samsung/upnp/event/SubscriptionRequest;

    invoke-direct {v0, p1}, Lcom/samsung/upnp/event/SubscriptionRequest;-><init>(Lcom/samsung/http/HTTPRequest;)V

    .line 1575
    .local v0, "subReq":Lcom/samsung/upnp/event/SubscriptionRequest;
    invoke-direct {p0, v0}, Lcom/samsung/upnp/Device;->deviceEventSubscriptionRecieved(Lcom/samsung/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1579
    .end local v0    # "subReq":Lcom/samsung/upnp/event/SubscriptionRequest;
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0
.end method

.method public httptRecieved(Ljava/lang/String;)V
    .locals 0
    .param p1, "firstLine"    # Ljava/lang/String;

    .prologue
    .line 1584
    return-void
.end method

.method public isDevice(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 895
    if-nez p1, :cond_1

    .line 903
    :cond_0
    :goto_0
    return v0

    .line 897
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 898
    goto :goto_0

    .line 899
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 900
    goto :goto_0
.end method

.method public isExpired()Z
    .locals 5

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getElapsedTime()J

    move-result-wide v0

    .line 602
    .local v0, "elipsedTime":J
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getLeaseTime()I

    move-result v4

    add-int/lit8 v4, v4, 0x3c

    int-to-long v2, v4

    .line 603
    .local v2, "leaseTime":J
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 604
    const/4 v4, 0x1

    .line 605
    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isNMPRMode()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 288
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 289
    .local v0, "devNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_1

    .line 291
    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "INMPR03"

    invoke-virtual {v0, v2}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isRootDevice()Z
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWirelessMode()Z
    .locals 1

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/samsung/upnp/Device;->wirelessMode:Z

    return v0
.end method

.method public loadDescription(Ljava/io/InputStream;)Z
    .locals 4
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/upnp/device/InvalidDescriptionException;
        }
    .end annotation

    .prologue
    .line 475
    :try_start_0
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v1

    .line 476
    .local v1, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v1, p1}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/upnp/Device;->rootNode:Lcom/samsung/xml/Node;

    .line 477
    iget-object v2, p0, Lcom/samsung/upnp/Device;->rootNode:Lcom/samsung/xml/Node;

    if-nez v2, :cond_0

    .line 478
    new-instance v2, Lcom/samsung/upnp/device/InvalidDescriptionException;

    const-string v3, "Couldn\'t find a root node"

    invoke-direct {v2, v3, p1}, Lcom/samsung/upnp/device/InvalidDescriptionException;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    throw v2
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 483
    .end local v1    # "parser":Lcom/samsung/xml/Parser;
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Lcom/samsung/xml/ParserException;
    new-instance v2, Lcom/samsung/upnp/device/InvalidDescriptionException;

    invoke-direct {v2, v0}, Lcom/samsung/upnp/device/InvalidDescriptionException;-><init>(Ljava/lang/Exception;)V

    throw v2

    .line 479
    .end local v0    # "e":Lcom/samsung/xml/ParserException;
    .restart local v1    # "parser":Lcom/samsung/xml/Parser;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/upnp/Device;->rootNode:Lcom/samsung/xml/Node;

    const-string v3, "device"

    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/upnp/Device;->deviceNode:Lcom/samsung/xml/Node;

    .line 480
    iget-object v2, p0, Lcom/samsung/upnp/Device;->deviceNode:Lcom/samsung/xml/Node;

    if-nez v2, :cond_1

    .line 481
    new-instance v2, Lcom/samsung/upnp/device/InvalidDescriptionException;

    const-string v3, "Couldn\'t find a root device node"

    invoke-direct {v2, v3, p1}, Lcom/samsung/upnp/device/InvalidDescriptionException;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    throw v2
    :try_end_1
    .catch Lcom/samsung/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_0

    .line 487
    :cond_1
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->initializeLoadedDescription()Z

    move-result v2

    if-nez v2, :cond_2

    .line 488
    const/4 v2, 0x0

    .line 493
    :goto_0
    return v2

    .line 491
    :cond_2
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/upnp/Device;->setDescriptionFile(Ljava/io/File;)V

    .line 493
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;
    .param p2, "st"    # Ljava/lang/String;
    .param p3, "usn"    # Ljava/lang/String;

    .prologue
    .line 1411
    if-nez p1, :cond_0

    .line 1412
    const/4 v10, 0x0

    .line 1443
    :goto_0
    return v10

    .line 1413
    :cond_0
    const-string v10, "[yjkim] SSDP :"

    const-string v11, "postSearchResponse"

    invoke-static {v10, v11}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocalAddress()Ljava/lang/String;

    move-result-object v1

    .line 1415
    .local v1, "localAddr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getRootDevice()Lcom/samsung/upnp/Device;

    move-result-object v5

    .line 1416
    .local v5, "rootDev":Lcom/samsung/upnp/Device;
    const-string v6, ""

    .line 1417
    .local v6, "rootDevLocation":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 1418
    invoke-virtual {v5, v1}, Lcom/samsung/upnp/Device;->getLocationURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1422
    new-instance v8, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;

    invoke-direct {v8}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;-><init>()V

    .line 1423
    .local v8, "ssdpRes":Lcom/samsung/upnp/ssdp/SSDPSearchResponse;
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getLeaseTime()I

    move-result v10

    invoke-virtual {v8, v10}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->setLeaseTime(I)V

    .line 1424
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->setDate(Ljava/util/Calendar;)V

    .line 1425
    invoke-virtual {v8, p2}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->setST(Ljava/lang/String;)V

    .line 1426
    invoke-virtual {v8, p3}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->setUSN(Ljava/lang/String;)V

    .line 1427
    invoke-virtual {v8, v6}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->setLocation(Ljava/lang/String;)V

    .line 1431
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getMX()I

    move-result v2

    .line 1432
    .local v2, "mx":I
    mul-int/lit16 v10, v2, 0xc8

    invoke-static {v10}, Lcom/samsung/util/TimerUtil;->waitRandom(I)V

    .line 1434
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/lang/String;

    move-result-object v3

    .line 1435
    .local v3, "remoteAddr":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getRemotePort()I

    move-result v4

    .line 1436
    .local v4, "remotePort":I
    new-instance v9, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    invoke-direct {v9}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;-><init>()V

    .line 1437
    .local v9, "ssdpResSock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1438
    invoke-virtual {v8}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->print()V

    .line 1439
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getSSDPAnnounceCount()I

    move-result v7

    .line 1440
    .local v7, "ssdpCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v7, :cond_3

    .line 1443
    const/4 v10, 0x1

    goto :goto_0

    .line 1420
    .end local v0    # "i":I
    .end local v2    # "mx":I
    .end local v3    # "remoteAddr":Ljava/lang/String;
    .end local v4    # "remotePort":I
    .end local v7    # "ssdpCount":I
    .end local v8    # "ssdpRes":Lcom/samsung/upnp/ssdp/SSDPSearchResponse;
    .end local v9    # "ssdpResSock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 1441
    .restart local v0    # "i":I
    .restart local v2    # "mx":I
    .restart local v3    # "remoteAddr":Ljava/lang/String;
    .restart local v4    # "remotePort":I
    .restart local v7    # "ssdpCount":I
    .restart local v8    # "ssdpRes":Lcom/samsung/upnp/ssdp/SSDPSearchResponse;
    .restart local v9    # "ssdpResSock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    :cond_3
    invoke-virtual {v9, v3, v4, v8}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/lang/String;ILcom/samsung/upnp/ssdp/SSDPSearchResponse;)Z

    .line 1440
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setActionListener(Lcom/samsung/upnp/control/ActionListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/upnp/control/ActionListener;

    .prologue
    .line 2161
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v3

    .line 2162
    .local v3, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v1

    .line 2163
    .local v1, "nServices":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 2167
    return-void

    .line 2164
    :cond_0
    invoke-virtual {v3, v0}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v2

    .line 2165
    .local v2, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 2163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setActionListener(Lcom/samsung/upnp/control/ActionListener;Z)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/upnp/control/ActionListener;
    .param p2, "includeSubDevices"    # Z

    .prologue
    .line 2186
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/Device;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 2187
    if-eqz p2, :cond_0

    .line 2188
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 2189
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 2190
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v1, :cond_1

    .line 2195
    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    .end local v3    # "n":I
    :cond_0
    return-void

    .line 2191
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    .restart local v3    # "n":I
    :cond_1
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 2192
    .local v0, "dev":Lcom/samsung/upnp/Device;
    const/4 v4, 0x1

    invoke-virtual {v0, p1, v4}, Lcom/samsung/upnp/Device;->setActionListener(Lcom/samsung/upnp/control/ActionListener;Z)V

    .line 2190
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public setFriendlyName(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "needAnnounce"    # Z

    .prologue
    const/4 v1, 0x0

    .line 679
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x40

    if-ge v2, v3, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 690
    :cond_0
    :goto_0
    return v1

    .line 682
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 683
    .local v0, "node":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_0

    .line 685
    const-string v1, "friendlyName"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    if-eqz p2, :cond_2

    .line 687
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->byebye()V

    .line 688
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->announce()V

    .line 690
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setHTTPPort(I)V
    .locals 1
    .param p1, "port"    # I

    .prologue
    .line 1539
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/DeviceData;->setHTTPPort(I)V

    .line 1540
    return-void
.end method

.method public setLeaseTime(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/upnp/xml/DeviceData;->setLeaseTime(I)V

    .line 567
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getAdvertiser()Lcom/samsung/upnp/device/Advertiser;

    move-result-object v0

    .line 568
    .local v0, "adv":Lcom/samsung/upnp/device/Advertiser;
    if-eqz v0, :cond_0

    .line 569
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->announce()V

    .line 570
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Advertiser;->restart()V

    .line 572
    :cond_0
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 549
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/DeviceData;->setLocation(Ljava/lang/String;)V

    .line 550
    return-void
.end method

.method public setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/upnp/control/QueryListener;

    .prologue
    .line 2171
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v3

    .line 2172
    .local v3, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v1

    .line 2173
    .local v1, "nServices":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 2177
    return-void

    .line 2174
    :cond_0
    invoke-virtual {v3, v0}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v2

    .line 2175
    .local v2, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/Service;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V

    .line 2173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setQueryListener(Lcom/samsung/upnp/control/QueryListener;Z)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/upnp/control/QueryListener;
    .param p2, "includeSubDevices"    # Z

    .prologue
    .line 2200
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/Device;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V

    .line 2201
    if-eqz p2, :cond_0

    .line 2202
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 2203
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 2204
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v1, :cond_1

    .line 2209
    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    .end local v3    # "n":I
    :cond_0
    return-void

    .line 2205
    .restart local v1    # "devCnt":I
    .restart local v2    # "devList":Lcom/samsung/upnp/DeviceList;
    .restart local v3    # "n":I
    :cond_1
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 2206
    .local v0, "dev":Lcom/samsung/upnp/Device;
    const/4 v4, 0x1

    invoke-virtual {v0, p1, v4}, Lcom/samsung/upnp/Device;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;Z)V

    .line 2204
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 1
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 533
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getDeviceData()Lcom/samsung/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/DeviceData;->setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 534
    return-void
.end method

.method public setUDN(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 821
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "UDN"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    return-void
.end method

.method public setWirelessMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/samsung/upnp/Device;->wirelessMode:Z

    .line 303
    return-void
.end method

.method public start()Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2006
    invoke-direct {p0, v6}, Lcom/samsung/upnp/Device;->stop(Z)Z

    .line 2012
    const/4 v3, 0x0

    .line 2013
    .local v3, "retryCnt":I
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getHTTPPort()I

    move-result v1

    .line 2014
    .local v1, "bindPort":I
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getHTTPServerList()Lcom/samsung/http/HTTPServerList;

    move-result-object v2

    .line 2015
    .local v2, "httpServerList":Lcom/samsung/http/HTTPServerList;
    :goto_0
    invoke-virtual {v2, v1}, Lcom/samsung/http/HTTPServerList;->open(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2022
    invoke-virtual {v2, p0}, Lcom/samsung/http/HTTPServerList;->addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V

    .line 2023
    invoke-virtual {v2, p0}, Lcom/samsung/http/HTTPServerList;->addReceivedListener(Lcom/samsung/http/HTTPReceivedListener;)V

    .line 2024
    invoke-virtual {v2}, Lcom/samsung/http/HTTPServerList;->start()V

    .line 2030
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getSSDPSearchSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;

    move-result-object v4

    .line 2031
    .local v4, "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->open()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2051
    .end local v4    # "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    :cond_0
    :goto_1
    return v5

    .line 2016
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 2017
    const/16 v7, 0x64

    if-lt v7, v3, :cond_0

    .line 2019
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p0, v7}, Lcom/samsung/upnp/Device;->setHTTPPort(I)V

    .line 2020
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getHTTPPort()I

    move-result v1

    goto :goto_0

    .line 2033
    .restart local v4    # "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    :cond_2
    invoke-virtual {v4, p0}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->addSearchListener(Lcom/samsung/upnp/device/SearchListener;)V

    .line 2034
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->start()V

    .line 2040
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->announce()V

    .line 2046
    new-instance v0, Lcom/samsung/upnp/device/Advertiser;

    invoke-direct {v0, p0}, Lcom/samsung/upnp/device/Advertiser;-><init>(Lcom/samsung/upnp/Device;)V

    .line 2047
    .local v0, "adv":Lcom/samsung/upnp/device/Advertiser;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "ADVERTISER :"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/device/Advertiser;->setName(Ljava/lang/String;)V

    .line 2048
    invoke-direct {p0, v0}, Lcom/samsung/upnp/Device;->setAdvertiser(Lcom/samsung/upnp/device/Advertiser;)V

    .line 2049
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Advertiser;->start()V

    move v5, v6

    .line 2051
    goto :goto_1
.end method

.method public start(Lcom/samsung/net/HostInterface$NWK_TYPE;)Z
    .locals 8
    .param p1, "nwkType"    # Lcom/samsung/net/HostInterface$NWK_TYPE;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2055
    invoke-direct {p0, v6}, Lcom/samsung/upnp/Device;->stop(Z)Z

    .line 2061
    const/4 v3, 0x0

    .line 2062
    .local v3, "retryCnt":I
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getHTTPPort()I

    move-result v1

    .line 2063
    .local v1, "bindPort":I
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getHTTPServerList()Lcom/samsung/http/HTTPServerList;

    move-result-object v2

    .line 2065
    .local v2, "httpServerList":Lcom/samsung/http/HTTPServerList;
    :goto_0
    invoke-virtual {v2, v1, p1}, Lcom/samsung/http/HTTPServerList;->open(ILcom/samsung/net/HostInterface$NWK_TYPE;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2072
    invoke-virtual {v2, p0}, Lcom/samsung/http/HTTPServerList;->addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V

    .line 2073
    invoke-virtual {v2, p0}, Lcom/samsung/http/HTTPServerList;->addReceivedListener(Lcom/samsung/http/HTTPReceivedListener;)V

    .line 2074
    invoke-virtual {v2}, Lcom/samsung/http/HTTPServerList;->start()V

    .line 2080
    invoke-direct {p0}, Lcom/samsung/upnp/Device;->getSSDPSearchSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;

    move-result-object v4

    .line 2081
    .local v4, "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->open()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2101
    .end local v4    # "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    :cond_0
    :goto_1
    return v5

    .line 2066
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 2067
    const/16 v7, 0x64

    if-lt v7, v3, :cond_0

    .line 2069
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p0, v7}, Lcom/samsung/upnp/Device;->setHTTPPort(I)V

    .line 2070
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getHTTPPort()I

    move-result v1

    goto :goto_0

    .line 2083
    .restart local v4    # "ssdpSearchSockList":Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;
    :cond_2
    invoke-virtual {v4, p0}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->addSearchListener(Lcom/samsung/upnp/device/SearchListener;)V

    .line 2084
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchSocketList;->start()V

    .line 2090
    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->announce()V

    .line 2096
    new-instance v0, Lcom/samsung/upnp/device/Advertiser;

    invoke-direct {v0, p0}, Lcom/samsung/upnp/device/Advertiser;-><init>(Lcom/samsung/upnp/Device;)V

    .line 2097
    .local v0, "adv":Lcom/samsung/upnp/device/Advertiser;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "ADVERTISER :"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/device/Advertiser;->setName(Ljava/lang/String;)V

    .line 2098
    invoke-direct {p0, v0}, Lcom/samsung/upnp/Device;->setAdvertiser(Lcom/samsung/upnp/device/Advertiser;)V

    .line 2099
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Advertiser;->start()V

    move v5, v6

    .line 2101
    goto :goto_1
.end method

.method public stop()Z
    .locals 1

    .prologue
    .line 2133
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/upnp/Device;->stop(Z)Z

    move-result v0

    return v0
.end method
