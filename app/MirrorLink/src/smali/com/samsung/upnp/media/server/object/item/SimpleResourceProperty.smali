.class public Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;
.super Lcom/samsung/upnp/media/server/object/ResourceProperty;
.source "SimpleResourceProperty.java"


# instance fields
.field private contentFile:Ljava/io/File;

.field private contentInputStream:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Ljava/io/File;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "attrs"    # Lcom/samsung/xml/AttributeList;
    .param p4, "content"    # Ljava/io/File;

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/upnp/media/server/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;)V

    .line 18
    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;->contentFile:Ljava/io/File;

    .line 19
    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;->contentInputStream:Ljava/io/InputStream;

    .line 97
    iput-object p4, p0, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;->contentFile:Ljava/io/File;

    .line 98
    return-void
.end method

.method public static createSimpleResourceProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;
    .locals 8
    .param p0, "serverUri"    # Ljava/lang/String;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "size"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "duration"    # Ljava/lang/String;

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 25
    .local v0, "attr":Lcom/samsung/xml/Attribute;
    new-instance v1, Lcom/samsung/xml/AttributeList;

    invoke-direct {v1}, Lcom/samsung/xml/AttributeList;-><init>()V

    .line 26
    .local v1, "attrs":Lcom/samsung/xml/AttributeList;
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v2

    .line 29
    .local v2, "parser":Lcom/samsung/upnp/media/server/object/DLNAProfileParser;
    if-eqz p6, :cond_0

    invoke-virtual {p6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 30
    :cond_0
    const-string p6, "0"

    .line 32
    :cond_1
    new-instance v5, Ljava/sql/Time;

    invoke-static {p6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/sql/Time;-><init>(J)V

    .line 34
    .local v5, "t":Ljava/sql/Time;
    sget v6, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    invoke-virtual {v2, p2, p4, p5, v6}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v3

    .line 36
    .local v3, "protocol":Ljava/lang/String;
    if-eqz p4, :cond_2

    if-eqz p5, :cond_2

    .line 37
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 38
    .local v4, "resolution":Ljava/lang/String;
    new-instance v0, Lcom/samsung/xml/Attribute;

    .end local v0    # "attr":Lcom/samsung/xml/Attribute;
    const-string v6, "resolution"

    invoke-direct {v0, v6, v4}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .restart local v0    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v1, v0}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 42
    .end local v4    # "resolution":Ljava/lang/String;
    :cond_2
    new-instance v0, Lcom/samsung/xml/Attribute;

    .end local v0    # "attr":Lcom/samsung/xml/Attribute;
    const-string v6, "size"

    invoke-direct {v0, v6, p3}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .restart local v0    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v1, v0}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v0, Lcom/samsung/xml/Attribute;

    .end local v0    # "attr":Lcom/samsung/xml/Attribute;
    const-string v6, "duration"

    invoke-virtual {v5}, Ljava/sql/Time;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v6, v7}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .restart local v0    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v1, v0}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v6, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, p0, v3, v1, v7}, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Ljava/io/File;)V

    return-object v6
.end method

.method public static createSimpleResourceProperty(Ljava/lang/String;Ljava/util/HashMap;)Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;
    .locals 19
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 53
    .local v3, "attr":Lcom/samsung/xml/Attribute;
    new-instance v4, Lcom/samsung/xml/AttributeList;

    invoke-direct {v4}, Lcom/samsung/xml/AttributeList;-><init>()V

    .line 54
    .local v4, "attrs":Lcom/samsung/xml/AttributeList;
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v9

    .line 56
    .local v9, "parser":Lcom/samsung/upnp/media/server/object/DLNAProfileParser;
    const-string v16, "mime_type"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 57
    .local v8, "mimeType":Ljava/lang/String;
    const-string v16, "_size"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 58
    .local v12, "size":Ljava/lang/String;
    const-string v16, "width"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 59
    .local v15, "width":Ljava/lang/String;
    const-string v16, "height"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 60
    .local v7, "height":Ljava/lang/String;
    const-string v16, "resolution"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 61
    .local v11, "resolution":Ljava/lang/String;
    const-string v16, "duration"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 63
    .local v5, "durationStr":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 64
    const-string v5, "0"

    .line 66
    :cond_0
    new-instance v13, Ljava/sql/Time;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v13, v0, v1}, Ljava/sql/Time;-><init>(J)V

    .line 67
    .local v13, "t":Ljava/sql/Time;
    invoke-virtual {v13}, Ljava/sql/Time;->getHours()I

    move-result v16

    const/16 v17, 0x3

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_1

    .line 68
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/sql/Time;->setHours(I)V

    .line 71
    :cond_1
    const/4 v14, 0x0

    .line 72
    .local v14, "w":I
    const/4 v6, 0x0

    .line 75
    .local v6, "h":I
    :try_start_0
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v14

    .line 80
    :goto_0
    :try_start_1
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .line 84
    :goto_1
    sget v16, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    move/from16 v0, v16

    invoke-virtual {v9, v8, v14, v6, v0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v10

    .line 85
    .local v10, "protocol":Ljava/lang/String;
    new-instance v3, Lcom/samsung/xml/Attribute;

    .end local v3    # "attr":Lcom/samsung/xml/Attribute;
    const-string v16, "resolution"

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v11}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .restart local v3    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v4, v3}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v3, Lcom/samsung/xml/Attribute;

    .end local v3    # "attr":Lcom/samsung/xml/Attribute;
    const-string v16, "size"

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v12}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .restart local v3    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v4, v3}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 89
    new-instance v3, Lcom/samsung/xml/Attribute;

    .end local v3    # "attr":Lcom/samsung/xml/Attribute;
    const-string v16, "duration"

    invoke-virtual {v13}, Ljava/sql/Time;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v3, v0, v1}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .restart local v3    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v4, v3}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v17, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;

    new-instance v18, Ljava/io/File;

    const-string v16, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v10, v4, v2}, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Ljava/io/File;)V

    return-object v17

    .line 76
    .end local v10    # "protocol":Ljava/lang/String;
    :catch_0
    move-exception v16

    goto :goto_0

    .line 81
    :catch_1
    move-exception v16

    goto :goto_1
.end method


# virtual methods
.method public deleteContent()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 107
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;->contentFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-object v1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/io/FileNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method
