.class public Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;
.super Lcom/samsung/upnp/media/server/object/item/ItemNode;
.source "DOARecordedItemNode.java"


# direct methods
.method public constructor <init>(Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;-><init>()V

    .line 37
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getArtist()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setArtist(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setTitle(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getCreator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setCreator(Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setUPnPClass(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getAlbumArtUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "upnp:albumArtURI"

    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getAlbumArtUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    return-void
.end method
