.class public Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;
.super Lcom/samsung/upnp/media/server/object/ResourceProperty;
.source "DOAResourceProperty.java"


# static fields
.field static parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;


# instance fields
.field private attrs:Lcom/samsung/xml/AttributeList;

.field private inputStreamUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v0

    sput-object v0, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Landroid/net/Uri;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "attrs"    # Lcom/samsung/xml/AttributeList;
    .param p4, "inputStreamUri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/upnp/media/server/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;)V

    .line 21
    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->inputStreamUri:Landroid/net/Uri;

    .line 22
    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->attrs:Lcom/samsung/xml/AttributeList;

    .line 27
    invoke-virtual {p4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->setInputstreamUriString(Ljava/lang/String;)V

    .line 28
    iput-object p4, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->inputStreamUri:Landroid/net/Uri;

    .line 29
    iput-object p3, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->attrs:Lcom/samsung/xml/AttributeList;

    .line 30
    return-void
.end method

.method public static buildDOAResourceProperty(Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;)V
    .locals 17
    .param p0, "parent"    # Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;
    .param p1, "info"    # Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    .prologue
    .line 43
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    const-string v6, ""

    .line 46
    .local v6, "protocol":Ljava/lang/String;
    const-string v11, ""

    .line 47
    .local v11, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 48
    .local v2, "attr":Lcom/samsung/xml/Attribute;
    new-instance v3, Lcom/samsung/xml/AttributeList;

    invoke-direct {v3}, Lcom/samsung/xml/AttributeList;-><init>()V

    .line 49
    .local v3, "attrs":Lcom/samsung/xml/AttributeList;
    const/4 v7, 0x0

    .line 52
    .local v7, "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    sget-object v12, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getMimeType()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getWidth()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getHeight()I

    move-result v15

    sget v16, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    invoke-virtual/range {v12 .. v16}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v6

    .line 53
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getResolution()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getResolution()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_2

    .line 54
    new-instance v2, Lcom/samsung/xml/Attribute;

    .end local v2    # "attr":Lcom/samsung/xml/Attribute;
    const-string v12, "resolution"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getResolution()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .restart local v2    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v3, v2}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_2
    new-instance v2, Lcom/samsung/xml/Attribute;

    .end local v2    # "attr":Lcom/samsung/xml/Attribute;
    const-string v12, "size"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getSize()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .restart local v2    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v3, v2}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getDurationTime()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_3

    .line 60
    new-instance v2, Lcom/samsung/xml/Attribute;

    .end local v2    # "attr":Lcom/samsung/xml/Attribute;
    const-string v12, "duration"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getDurationTime()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .restart local v2    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v3, v2}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v4

    .line 64
    .local v4, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v4, :cond_0

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->getID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 67
    new-instance v7, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;

    .end local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getUri()Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v7, v11, v6, v3, v12}, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Landroid/net/Uri;)V

    .line 68
    .restart local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 69
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getImageDB_ID()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->imageDB_id:I

    .line 70
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getAudioDB_ID()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setAudioAlbum_id(Ljava/lang/String;)V

    .line 73
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getTYPE()Lcom/samsung/upnp/media/server/directory/doa/FileType;

    move-result-object v12

    sget-object v13, Lcom/samsung/upnp/media/server/directory/doa/FileType;->Video:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    if-ne v12, v13, :cond_4

    .line 74
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getSubTitle()Ljava/lang/String;

    move-result-object v5

    .line 75
    .local v5, "filePath":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 76
    invoke-static {v5}, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->getSubTitleFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 78
    .local v9, "subtitle":Ljava/lang/String;
    if-eqz v9, :cond_4

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->getID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentSubtitleExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 80
    new-instance v7, Lcom/samsung/upnp/media/server/object/item/sat/DOASubtitleProperty;

    .end local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    invoke-direct {v7, v11, v6, v9}, Lcom/samsung/upnp/media/server/object/item/sat/DOASubtitleProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .restart local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 87
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v9    # "subtitle":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getAlbumArtUri()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getAlbumArtUri()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_5

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getAlbumArtUri()Ljava/lang/String;

    move-result-object v5

    .line 89
    .restart local v5    # "filePath":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->getID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 90
    sget-object v12, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    .line 91
    const-string v13, "image/jpeg"

    .line 92
    const/4 v14, 0x0

    .line 93
    const/4 v15, 0x0

    .line 94
    sget v16, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    .line 90
    invoke-virtual/range {v12 .. v16}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "albumartProtocol":Ljava/lang/String;
    new-instance v7, Lcom/samsung/upnp/media/server/object/item/sat/DOAAlbumartProperty;

    .end local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    invoke-direct {v7, v11, v1, v5}, Lcom/samsung/upnp/media/server/object/item/sat/DOAAlbumartProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .restart local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 98
    const-string v12, "upnp:albumArtURI"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .end local v1    # "albumartProtocol":Ljava/lang/String;
    .end local v5    # "filePath":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getTYPE()Lcom/samsung/upnp/media/server/directory/doa/FileType;

    move-result-object v12

    sget-object v13, Lcom/samsung/upnp/media/server/directory/doa/FileType;->Image:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    if-ne v12, v13, :cond_6

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->getID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentMicroThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 105
    sget-object v12, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    .line 106
    const-string v13, "image/jpeg"

    .line 107
    const/16 v14, 0x78

    .line 108
    const/16 v15, 0x78

    .line 109
    sget v16, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_MICRO:I

    .line 105
    invoke-virtual/range {v12 .. v16}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v6

    .line 110
    new-instance v8, Lcom/samsung/xml/AttributeList;

    invoke-direct {v8}, Lcom/samsung/xml/AttributeList;-><init>()V

    .line 111
    .local v8, "smallAttrs":Lcom/samsung/xml/AttributeList;
    new-instance v2, Lcom/samsung/xml/Attribute;

    .end local v2    # "attr":Lcom/samsung/xml/Attribute;
    const-string v12, "resolution"

    const-string v13, "120x120"

    invoke-direct {v2, v12, v13}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .restart local v2    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v8, v2}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v7, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;

    .end local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getUri()Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v7, v11, v6, v12, v8}, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/xml/AttributeList;)V

    .line 114
    .restart local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 118
    .end local v8    # "smallAttrs":Lcom/samsung/xml/AttributeList;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsUri()Landroid/net/Uri;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 119
    new-instance v10, Lcom/samsung/xml/AttributeList;

    invoke-direct {v10}, Lcom/samsung/xml/AttributeList;-><init>()V

    .line 120
    .local v10, "thumbAttrs":Lcom/samsung/xml/AttributeList;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->getID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentSmallThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 121
    sget-object v12, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    const-string v13, "image/jpeg"

    .line 122
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsWidth()J

    move-result-wide v14

    long-to-int v14, v14

    .line 123
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsHeight()J

    move-result-wide v15

    long-to-int v15, v15

    .line 124
    sget v16, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_SMALL:I

    .line 121
    invoke-virtual/range {v12 .. v16}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v6

    .line 126
    new-instance v2, Lcom/samsung/xml/Attribute;

    .end local v2    # "attr":Lcom/samsung/xml/Attribute;
    const-string v12, "size"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsSize()I

    move-result v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .restart local v2    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v10, v2}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v2, Lcom/samsung/xml/Attribute;

    .end local v2    # "attr":Lcom/samsung/xml/Attribute;
    const-string v12, "resolution"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsWidth()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "x"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsHeight()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .restart local v2    # "attr":Lcom/samsung/xml/Attribute;
    invoke-virtual {v10, v2}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 132
    new-instance v7, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;

    .end local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getThumbnailsUri()Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v7, v11, v6, v10, v12}, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Landroid/net/Uri;)V

    .line 134
    .restart local v7    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public deleteContent()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 157
    invoke-static {}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getInstance()Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->inputStreamUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
