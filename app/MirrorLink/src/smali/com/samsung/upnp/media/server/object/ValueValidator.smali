.class public Lcom/samsung/upnp/media/server/object/ValueValidator;
.super Ljava/lang/Object;
.source "ValueValidator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isValidAttribute(Lcom/samsung/xml/Attribute;)Z
    .locals 3
    .param p0, "attr"    # Lcom/samsung/xml/Attribute;

    .prologue
    const/4 v0, 0x0

    .line 13
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25
    :cond_0
    :goto_0
    return v0

    .line 17
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidDate(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duration"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidTime(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "restricted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "searchable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isValidBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 100
    if-eqz p0, :cond_0

    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    :cond_0
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isValidContentProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)Z
    .locals 6
    .param p0, "cp"    # Lcom/samsung/upnp/media/server/object/ContentProperty;

    .prologue
    const/4 v3, 0x0

    .line 42
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v3

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getNAttributes()I

    move-result v2

    .line 46
    .local v2, "nAttr":I
    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 50
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "date"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidDate(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 52
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "duration"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidTime(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "upnp:storageMedium"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 58
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_5

    .line 64
    const/4 v3, 0x1

    goto :goto_0

    .line 59
    :cond_5
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttribute(I)Lcom/samsung/xml/Attribute;

    move-result-object v0

    .line 60
    .local v0, "attr":Lcom/samsung/xml/Attribute;
    invoke-static {v0}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidAttribute(Lcom/samsung/xml/Attribute;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 61
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->removeAttribute(Lcom/samsung/xml/Attribute;)Z

    .line 62
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getNAttributes()I

    move-result v2

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static isValidDate(Ljava/lang/String;)Z
    .locals 3
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    .line 80
    .local v0, "date":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 81
    :catch_0
    move-exception v1

    .line 82
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isValidTime(Ljava/lang/String;)Z
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x9

    if-le v3, v4, :cond_0

    .line 96
    :goto_0
    return v2

    .line 90
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "hh:mm:ss"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 92
    .local v0, "date":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    const/4 v2, 0x1

    goto :goto_0

    .line 93
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method
