.class public Lcom/samsung/upnp/media/server/action/BrowseAction;
.super Lcom/samsung/upnp/Action;
.source "BrowseAction.java"


# static fields
.field public static final BROWSE_DIRECT_CHILDREN:Ljava/lang/String; = "BrowseDirectChildren"

.field public static final BROWSE_FLAG:Ljava/lang/String; = "BrowseFlag"

.field public static final BROWSE_METADATA:Ljava/lang/String; = "BrowseMetadata"

.field public static final FILTER:Ljava/lang/String; = "Filter"

.field public static final NUMBER_RETURNED:Ljava/lang/String; = "NumberReturned"

.field public static final OBJECT_ID:Ljava/lang/String; = "ObjectID"

.field public static final REQUESTED_COUNT:Ljava/lang/String; = "RequestedCount"

.field public static final RESULT:Ljava/lang/String; = "Result"

.field public static final SORT_CRITERIA:Ljava/lang/String; = "SortCriteria"

.field public static final STARTING_INDEX:Ljava/lang/String; = "StartingIndex"

.field public static final TOTAL_MACHES:Ljava/lang/String; = "TotalMatches"

.field public static final UPDATE_ID:Ljava/lang/String; = "UpdateID"

.field private static browseActionNode:Lcom/samsung/xml/Node;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/upnp/media/server/action/BrowseAction;->browseActionNode:Lcom/samsung/xml/Node;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/upnp/Action;)V
    .locals 0
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/upnp/Action;-><init>(Lcom/samsung/upnp/Action;)V

    .line 59
    return-void
.end method

.method public static createDefaultBrowseAction(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Action;
    .locals 3
    .param p0, "serviceNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 62
    sget-object v2, Lcom/samsung/upnp/media/server/action/BrowseAction;->browseActionNode:Lcom/samsung/xml/Node;

    if-nez v2, :cond_0

    .line 66
    :try_start_0
    const-string v1, "<action><name>Browse</name><argumentList><argument><name>ObjectID</name><direction>in</direction><relatedStateVariable>A_ARG_TYPE_ObjectID</relatedStateVariable></argument><argument><name>BrowseFlag</name><direction>in</direction><relatedStateVariable>A_ARG_TYPE_BrowseFlag</relatedStateVariable></argument><argument><name>Filter</name><direction>in</direction><relatedStateVariable>A_ARG_TYPE_Filter</relatedStateVariable></argument><argument><name>StartingIndex</name><direction>in</direction><relatedStateVariable>A_ARG_TYPE_Index</relatedStateVariable></argument><argument><name>RequestedCount</name><direction>in</direction><relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable></argument><argument><name>SortCriteria</name><direction>in</direction><relatedStateVariable>A_ARG_TYPE_SortCriteria</relatedStateVariable></argument><argument><name>Result</name><direction>out</direction><relatedStateVariable>A_ARG_TYPE_Result</relatedStateVariable></argument><argument><name>NumberReturned</name><direction>out</direction><relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable></argument><argument><name>TotalMatches</name><direction>out</direction><relatedStateVariable>A_ARG_TYPE_Count</relatedStateVariable></argument><argument><name>UpdateID</name><direction>out</direction><relatedStateVariable>A_ARG_TYPE_UpdateID</relatedStateVariable></argument></argumentList></action>"

    .line 122
    .local v1, "browseNode":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    sput-object v2, Lcom/samsung/upnp/media/server/action/BrowseAction;->browseActionNode:Lcom/samsung/xml/Node;
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    .end local v1    # "browseNode":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 126
    .local v0, "action":Lcom/samsung/upnp/Action;
    sget-object v2, Lcom/samsung/upnp/media/server/action/BrowseAction;->browseActionNode:Lcom/samsung/xml/Node;

    if-eqz v2, :cond_1

    .line 127
    new-instance v0, Lcom/samsung/upnp/Action;

    .end local v0    # "action":Lcom/samsung/upnp/Action;
    sget-object v2, Lcom/samsung/upnp/media/server/action/BrowseAction;->browseActionNode:Lcom/samsung/xml/Node;

    invoke-direct {v0, p0, v2}, Lcom/samsung/upnp/Action;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    .line 129
    .restart local v0    # "action":Lcom/samsung/upnp/Action;
    :cond_1
    return-object v0

    .line 123
    .end local v0    # "action":Lcom/samsung/upnp/Action;
    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public getBrowseFlag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    const-string v0, "BrowseFlag"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const-string v0, "Filter"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    const-string v0, "ObjectID"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestedCount()J
    .locals 3

    .prologue
    .line 164
    const-string v1, "RequestedCount"

    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    return-wide v1
.end method

.method public getSortCriteria()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    const-string v0, "SortCriteria"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartingIndex()J
    .locals 3

    .prologue
    .line 158
    const-string v1, "StartingIndex"

    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    return-wide v1
.end method

.method public isDirectChildren()Z
    .locals 2

    .prologue
    .line 148
    const-string v0, "BrowseDirectChildren"

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getBrowseFlag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMetadata()Z
    .locals 2

    .prologue
    .line 143
    const-string v0, "BrowseMetadata"

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getBrowseFlag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setNumberReturned(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 189
    const-string v0, "NumberReturned"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 190
    return-void
.end method

.method public setResult(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 184
    const-string v0, "Result"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public setTotalMaches(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 194
    const-string v0, "TotalMatches"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 195
    return-void
.end method

.method public setUpdateID(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 199
    const-string v0, "UpdateID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 200
    return-void
.end method
