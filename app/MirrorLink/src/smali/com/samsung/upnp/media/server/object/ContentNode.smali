.class public abstract Lcom/samsung/upnp/media/server/object/ContentNode;
.super Lcom/samsung/xml/Node;
.source "ContentNode.java"


# static fields
.field public static final ID:Ljava/lang/String; = "id"

.field public static final PARENT_ID:Ljava/lang/String; = "parentID"

.field public static final RESTRICTED:Ljava/lang/String; = "restricted"

.field public static final UNKNOWN:Ljava/lang/String; = "UNKNOWN"


# instance fields
.field private contentDir:Lcom/samsung/upnp/media/server/ContentDirectory;

.field private propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/xml/Node;-><init>()V

    .line 148
    new-instance v0, Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setID(I)V

    .line 68
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setParentID(I)V

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setRestricted(I)V

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setContentDirectory(Lcom/samsung/upnp/media/server/ContentDirectory;)V

    .line 71
    return-void
.end method

.method private outputPropertyAttributes(Ljava/io/PrintWriter;Lcom/samsung/upnp/media/server/object/ContentProperty;)V
    .locals 5
    .param p1, "ps"    # Ljava/io/PrintWriter;
    .param p2, "prop"    # Lcom/samsung/upnp/media/server/object/ContentProperty;

    .prologue
    .line 432
    invoke-virtual {p2}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getNAttributes()I

    move-result v2

    .line 433
    .local v2, "nAttributes":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 437
    return-void

    .line 434
    :cond_0
    invoke-virtual {p2, v1}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttribute(I)Lcom/samsung/xml/Attribute;

    move-result-object v0

    .line 435
    .local v0, "attr":Lcom/samsung/xml/Attribute;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 433
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public abstract addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V
.end method

.method public addProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)V
    .locals 1
    .param p1, "prop"    # Lcom/samsung/upnp/media/server/object/ContentProperty;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->add(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 293
    if-nez p1, :cond_1

    move-object p0, v5

    .line 310
    .end local p0    # "this":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_0
    :goto_0
    return-object p0

    .line 296
    .restart local p0    # "this":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v4

    .line 297
    .local v4, "nodeID":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNContentNodes()I

    move-result v3

    .line 301
    .local v3, "nodeCnt":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_1
    if-lt v2, v3, :cond_2

    move-object p0, v5

    .line 310
    goto :goto_0

    .line 302
    :cond_2
    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/object/ContentNode;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v0

    .line 303
    .local v0, "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v0, :cond_4

    .line 301
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 305
    :cond_4
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v1

    .line 306
    .local v1, "fnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-eqz v1, :cond_3

    move-object p0, v1

    .line 307
    goto :goto_0
.end method

.method public getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->contentDir:Lcom/samsung/upnp/media/server/ContentDirectory;

    return-object v0
.end method

.method public getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/media/server/object/ContentNode;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329
    const-string v0, "id"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNContentNodes()I
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNNodes()I

    move-result v0

    return v0
.end method

.method public getNProperties()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->size()I

    move-result v0

    return v0
.end method

.method public getParentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    const-string v0, "parentID"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProperties(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentPropertyList;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 164
    new-instance v0, Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;-><init>()V

    .line 165
    .local v0, "list":Lcom/samsung/upnp/media/server/object/ContentPropertyList;
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 169
    return-object v0

    .line 165
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/media/server/object/ContentProperty;

    .line 166
    .local v1, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 167
    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->getContentProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentProperty;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->getContentProperty(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getProperty(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 232
    .local v0, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 234
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getRestricted()I
    .locals 1

    .prologue
    .line 362
    const-string v0, "restricted"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getAttributeIntegerValue(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    const-string v0, "dc:title"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUPnPClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    const-string v0, "upnp:class"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasProperties()Z
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNProperties()I

    move-result v0

    if-lez v0, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 197
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContaierNode()Z
    .locals 1

    .prologue
    .line 100
    instance-of v0, p0, Lcom/samsung/upnp/media/server/object/container/ContainerNode;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isItemNode()Z
    .locals 1

    .prologue
    .line 107
    instance-of v0, p0, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    if-eqz v0, :cond_0

    .line 108
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public output(Ljava/io/PrintWriter;IZ)V
    .locals 13
    .param p1, "ps"    # Ljava/io/PrintWriter;
    .param p2, "indentLevel"    # I
    .param p3, "hasChildNode"    # Z

    .prologue
    .line 441
    invoke-virtual {p0, p2}, Lcom/samsung/upnp/media/server/object/ContentNode;->getIndentLevelString(I)Ljava/lang/String;

    move-result-object v1

    .line 443
    .local v1, "indentString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getName()Ljava/lang/String;

    move-result-object v5

    .line 444
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getValue()Ljava/lang/String;

    move-result-object v10

    .line 446
    .local v10, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->hasNodes()Z

    move-result v11

    if-nez v11, :cond_0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->hasProperties()Z

    move-result v11

    if-nez v11, :cond_0

    .line 447
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "<"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 448
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->outputAttributes(Ljava/io/PrintWriter;)V

    .line 449
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, ">"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "</"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 484
    :goto_0
    return-void

    .line 453
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "<"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->outputAttributes(Ljava/io/PrintWriter;)V

    .line 455
    const-string v11, ">"

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 457
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNProperties()I

    move-result v4

    .line 458
    .local v4, "nProps":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_1
    if-lt v2, v4, :cond_2

    .line 475
    if-eqz p3, :cond_1

    .line 476
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNNodes()I

    move-result v3

    .line 477
    .local v3, "nChildNodes":I
    const/4 v2, 0x0

    :goto_2
    if-lt v2, v3, :cond_5

    .line 483
    .end local v3    # "nChildNodes":I
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "</"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_2
    add-int/lit8 v11, p2, 0x1

    invoke-virtual {p0, v11}, Lcom/samsung/upnp/media/server/object/ContentNode;->getIndentLevelString(I)Ljava/lang/String;

    move-result-object v7

    .line 460
    .local v7, "propIndentString":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/object/ContentNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v6

    .line 461
    .local v6, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    instance-of v11, v6, Lcom/samsung/upnp/media/server/object/item/IHiddenProperty;

    if-eqz v11, :cond_3

    .line 458
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 464
    :cond_3
    invoke-virtual {v6}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v8

    .line 465
    .local v8, "propName":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 466
    .local v9, "propValue":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/samsung/upnp/media/server/object/ContentProperty;->hasAttributes()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 467
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "<"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 468
    invoke-direct {p0, p1, v6}, Lcom/samsung/upnp/media/server/object/ContentNode;->outputPropertyAttributes(Ljava/io/PrintWriter;Lcom/samsung/upnp/media/server/object/ContentProperty;)V

    .line 469
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, ">"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "</"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 472
    :cond_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "<"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "</"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 478
    .end local v6    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    .end local v7    # "propIndentString":Ljava/lang/String;
    .end local v8    # "propName":Ljava/lang/String;
    .end local v9    # "propValue":Ljava/lang/String;
    .restart local v3    # "nChildNodes":I
    :cond_5
    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 479
    .local v0, "cnode":Lcom/samsung/xml/Node;
    add-int/lit8 v11, p2, 0x1

    const/4 v12, 0x1

    invoke-virtual {v0, p1, v11, v12}, Lcom/samsung/xml/Node;->output(Ljava/io/PrintWriter;IZ)V

    .line 477
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2
.end method

.method public removeAllProperty()V
    .locals 1

    .prologue
    .line 202
    new-instance v0, Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    .line 203
    return-void
.end method

.method public abstract removeContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)Z
.end method

.method public removeProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)Z
    .locals 1
    .param p1, "prop"    # Lcom/samsung/upnp/media/server/object/ContentProperty;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->propList:Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setContentDirectory(Lcom/samsung/upnp/media/server/ContentDirectory;)V
    .locals 0
    .param p1, "cdir"    # Lcom/samsung/upnp/media/server/ContentDirectory;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/upnp/media/server/object/ContentNode;->contentDir:Lcom/samsung/upnp/media/server/ContentDirectory;

    .line 82
    return-void
.end method

.method public setID(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 319
    const-string v0, "id"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setAttribute(Ljava/lang/String;I)V

    .line 320
    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 324
    const-string v0, "id"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method public setParentID(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 338
    const-string v0, "parentID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setAttribute(Ljava/lang/String;I)V

    .line 339
    return-void
.end method

.method public setParentID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 343
    const-string v0, "parentID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 210
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p2, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getProperty(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 214
    .local v0, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    if-eqz v0, :cond_2

    .line 215
    invoke-virtual {v0, p2}, Lcom/samsung/upnp/media/server/object/ContentProperty;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :cond_2
    new-instance v0, Lcom/samsung/upnp/media/server/object/ContentProperty;

    .end local v0    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-direct {v0, p1, p2}, Lcom/samsung/upnp/media/server/object/ContentProperty;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .restart local v0    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->addProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)V

    goto :goto_0
.end method

.method public setPropertyAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "propName"    # Ljava/lang/String;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 261
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getProperty(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 262
    .local v0, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/samsung/upnp/media/server/object/ContentProperty;

    .end local v0    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/samsung/upnp/media/server/object/ContentProperty;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .restart local v0    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->addProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)V

    .line 266
    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/samsung/upnp/media/server/object/ContentProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    return-void
.end method

.method public setRestricted(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 357
    const-string v0, "restricted"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setAttribute(Ljava/lang/String;I)V

    .line 358
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 371
    const-string v0, "dc:title"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    return-void
.end method

.method public setUPnPClass(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 399
    const-string v0, "upnp:class"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    return-void
.end method

.method public setWriteStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 418
    const-string v0, "upnp:writeStatus"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    return-void
.end method
