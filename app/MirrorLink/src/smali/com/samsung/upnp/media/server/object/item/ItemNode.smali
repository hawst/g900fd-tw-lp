.class public Lcom/samsung/upnp/media/server/object/item/ItemNode;
.super Lcom/samsung/upnp/media/server/object/ContentNode;
.source "ItemNode.java"


# static fields
.field public static final COLOR_DEPTH:Ljava/lang/String; = "colorDepth"

.field public static final CONTENTFEATURES:Ljava/lang/String; = "contentFeatures"

.field private static final DATE_FORMAT:Ljava/lang/String; = "yyyy-MM-dd"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final IMPORT_URI:Ljava/lang/String; = "importUri"

.field public static final NAME:Ljava/lang/String; = "item"

.field public static final RES:Ljava/lang/String; = "res"

.field public static final RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final SIZE:Ljava/lang/String; = "size"


# instance fields
.field public TIMEOUT:Ljava/util/Date;

.field public audioAlbum_id:Ljava/lang/String;

.field public imageDB_id:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 67
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/ContentNode;-><init>()V

    .line 60
    iput v1, p0, Lcom/samsung/upnp/media/server/object/item/ItemNode;->imageDB_id:I

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/ItemNode;->audioAlbum_id:Ljava/lang/String;

    .line 69
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setID(I)V

    .line 70
    const-string v0, "item"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setName(Ljava/lang/String;)V

    .line 71
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setStorageMedium(Ljava/lang/String;)V

    .line 72
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setWriteStatus(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public static createItemNode(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .locals 12
    .param p0, "node"    # Lcom/samsung/xml/Node;

    .prologue
    const/4 v9, 0x0

    .line 330
    :try_start_0
    new-instance v2, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-direct {v2}, Lcom/samsung/upnp/media/server/object/item/ItemNode;-><init>()V

    .line 332
    .local v2, "item":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setName(Ljava/lang/String;)V

    .line 335
    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getNAttributes()I

    move-result v3

    .line 336
    .local v3, "nAttr":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_1

    .line 340
    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v4

    .line 341
    .local v4, "nProp":I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v4, :cond_2

    .line 348
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getTitle()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_4

    :cond_0
    move-object v2, v9

    .line 381
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .end local v3    # "nAttr":I
    .end local v4    # "nProp":I
    :goto_2
    return-object v2

    .line 337
    .restart local v1    # "i":I
    .restart local v2    # "item":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .restart local v3    # "nAttr":I
    :cond_1
    invoke-virtual {p0, v1}, Lcom/samsung/xml/Node;->getAttribute(I)Lcom/samsung/xml/Attribute;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v1}, Lcom/samsung/xml/Node;->getAttribute(I)Lcom/samsung/xml/Attribute;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 342
    .restart local v4    # "nProp":I
    :cond_2
    invoke-virtual {p0, v1}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/upnp/media/server/object/ContentProperty;->CreateContentProperty(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v6

    .line 343
    .local v6, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    if-eqz v6, :cond_3

    .line 344
    invoke-virtual {v2, v6}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)V

    .line 341
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 352
    .end local v6    # "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    :cond_4
    const/4 v0, 0x0

    .line 353
    .local v0, "ctRes":I
    const/4 v8, 0x0

    .line 354
    .local v8, "res":Lcom/samsung/upnp/media/server/object/ContentProperty;
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getNProperties()I

    move-result v10

    if-lt v1, v10, :cond_5

    .line 362
    if-gtz v0, :cond_7

    move-object v2, v9

    .line 363
    goto :goto_2

    .line 355
    :cond_5
    invoke-virtual {v2, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "res"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 356
    add-int/lit8 v0, v0, 0x1

    .line 357
    invoke-virtual {v2, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v8

    .line 354
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 366
    :cond_7
    const-string v10, "protocolInfo"

    invoke-virtual {v8, v10}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 367
    .local v7, "protocol":Ljava/lang/String;
    const-string v5, ""

    .line 368
    .local v5, "newProtocol":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v10

    invoke-virtual {v10, v7}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->convertToValidateProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 369
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->isSupportedProtocol(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 370
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->convertToValidateProtocolWithout4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 371
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->isSupportedProtocol(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    move-object v2, v9

    .line 372
    goto :goto_2

    .line 376
    :cond_8
    const-string v10, "protocolInfo"

    invoke-virtual {v8, v10, v5}, Lcom/samsung/upnp/media/server/object/ContentProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 379
    .end local v0    # "ctRes":I
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .end local v3    # "nAttr":I
    .end local v4    # "nProp":I
    .end local v5    # "newProtocol":Ljava/lang/String;
    .end local v7    # "protocol":Ljava/lang/String;
    .end local v8    # "res":Lcom/samsung/upnp/media/server/object/ContentProperty;
    :catch_0
    move-exception v10

    move-object v2, v9

    .line 381
    goto/16 :goto_2
.end method


# virtual methods
.method public addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V
    .locals 1
    .param p1, "node"    # Lcom/samsung/upnp/media/server/object/ContentNode;

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addNode(Lcom/samsung/xml/Node;)V

    .line 112
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setParentID(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setContentDirectory(Lcom/samsung/upnp/media/server/ContentDirectory;)V

    .line 114
    return-void
.end method

.method public addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V
    .locals 1
    .param p1, "resProp"    # Lcom/samsung/upnp/media/server/object/ResourceProperty;

    .prologue
    .line 255
    invoke-static {p1}, Lcom/samsung/upnp/media/server/object/ValueValidator;->isValidContentProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)V

    .line 257
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 85
    instance-of v4, p1, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    if-eqz v4, :cond_2

    move-object v0, p1

    .line 86
    check-cast v0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    .line 87
    .local v0, "object":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getDefaultResource()Lcom/samsung/upnp/media/server/object/ResourceProperty;

    move-result-object v1

    .line 89
    .local v1, "resource":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    if-nez v1, :cond_1

    .line 102
    .end local v0    # "object":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .end local v1    # "resource":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    :cond_0
    :goto_0
    return v3

    .line 92
    .restart local v0    # "object":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .restart local v1    # "resource":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->getInputstreamUriString()Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "resourceurl":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 96
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 97
    const/4 v3, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "object":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .end local v1    # "resource":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v2    # "resourceurl":Ljava/lang/String;
    :cond_2
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method public findResPropertyByURL(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .locals 5
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getNProperties()I

    move-result v2

    .line 261
    .local v2, "nProp":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 266
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 262
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 263
    .local v0, "cp":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "res"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    instance-of v3, v0, Lcom/samsung/upnp/media/server/object/ResourceProperty;

    if-eqz v3, :cond_1

    .line 264
    check-cast v0, Lcom/samsung/upnp/media/server/object/ResourceProperty;

    goto :goto_1

    .line 261
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getAudioAlbum_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/ItemNode;->audioAlbum_id:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string v0, "dc:date"

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDateTime()J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 151
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getDate()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "dateStr":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xa

    if-ge v6, v7, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-wide v4

    .line 154
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 156
    .local v2, "df":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 157
    .local v0, "date":Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 161
    .end local v0    # "date":Ljava/util/Date;
    :catch_0
    move-exception v3

    .line 162
    .local v3, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getDefaultResource()Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .locals 5

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getNProperties()I

    move-result v2

    .line 272
    .local v2, "nProp":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 277
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 273
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 274
    .local v0, "cp":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "res"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    instance-of v3, v0, Lcom/samsung/upnp/media/server/object/ResourceProperty;

    if-eqz v3, :cond_1

    .line 275
    check-cast v0, Lcom/samsung/upnp/media/server/object/ResourceProperty;

    goto :goto_1

    .line 272
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getResources()Lcom/samsung/upnp/media/server/object/ContentPropertyList;
    .locals 6

    .prologue
    .line 294
    new-instance v2, Lcom/samsung/upnp/media/server/object/ContentPropertyList;

    invoke-direct {v2}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;-><init>()V

    .line 295
    .local v2, "list":Lcom/samsung/upnp/media/server/object/ContentPropertyList;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getNProperties()I

    move-result v3

    .line 296
    .local v3, "nProp":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_0

    .line 301
    return-object v2

    .line 297
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 298
    .local v0, "cp":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "res"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    instance-of v4, v0, Lcom/samsung/upnp/media/server/object/ResourceProperty;

    if-eqz v4, :cond_1

    .line 299
    invoke-virtual {v2, v0}, Lcom/samsung/upnp/media/server/object/ContentPropertyList;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getThumbnailResource()Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;
    .locals 5

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getNProperties()I

    move-result v2

    .line 282
    .local v2, "nProp":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 289
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 283
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getProperty(I)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v0

    .line 284
    .local v0, "cp":Lcom/samsung/upnp/media/server/object/ContentProperty;
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "res"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    instance-of v3, v0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;

    if-eqz v3, :cond_1

    .line 285
    check-cast v0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;

    goto :goto_1

    .line 282
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public removeContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)Z
    .locals 1
    .param p1, "node"    # Lcom/samsung/upnp/media/server/object/ContentNode;

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->removeNode(Lcom/samsung/xml/Node;)Z

    move-result v0

    return v0
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 185
    const-string v0, "upnp:artist"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public setAudioAlbum_id(Ljava/lang/String;)V
    .locals 0
    .param p1, "audioAlbumId"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/upnp/media/server/object/item/ItemNode;->audioAlbum_id:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setCreator(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 172
    const-string v0, "dc:creator"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 126
    const-string v0, "dc:date"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public setStorageMedium(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 226
    const-string v0, "upnp:storageMedium"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public updateResourceUri(Ljava/lang/String;)V
    .locals 1
    .param p1, "interfaceAddress"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getDefaultResource()Lcom/samsung/upnp/media/server/object/ResourceProperty;

    move-result-object v0

    .line 306
    .local v0, "property":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    if-eqz v0, :cond_0

    .line 307
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->setUri(Ljava/lang/String;)V

    .line 308
    :cond_0
    return-void
.end method
