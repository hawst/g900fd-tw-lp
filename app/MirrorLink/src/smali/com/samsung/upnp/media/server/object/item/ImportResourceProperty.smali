.class public Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
.super Lcom/samsung/upnp/media/server/object/ResourceProperty;
.source "ImportResourceProperty.java"


# static fields
.field private static final parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;


# instance fields
.field mediaFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v0

    sput-object v0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/samsung/upnp/media/server/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->mediaFile:Ljava/io/File;

    .line 97
    iput-object p3, p0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->mediaFile:Ljava/io/File;

    .line 98
    return-void
.end method

.method private static createDestFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "mime"    # Ljava/lang/String;

    .prologue
    .line 23
    if-eqz p0, :cond_0

    const-string v3, ""

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 24
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 25
    :cond_1
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getPostfixByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 26
    .local v2, "postFix":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "destFile":Ljava/io/File;
    const/4 v1, 0x0

    .line 29
    .local v1, "index":I
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 32
    return-object v0

    .line 30
    :cond_2
    new-instance v0, Ljava/io/File;

    .end local v0    # "destFile":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v0    # "destFile":Ljava/io/File;
    goto :goto_0
.end method

.method public static createImportResourceProperty(Ljava/lang/String;Lcom/samsung/upnp/media/server/object/item/ImportItemNode;Lcom/samsung/upnp/media/server/object/ContentProperty;Ljava/io/File;)Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "parent"    # Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    .param p2, "prop"    # Lcom/samsung/upnp/media/server/object/ContentProperty;
    .param p3, "tempFile"    # Ljava/io/File;

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "uri":Ljava/lang/String;
    const-string v1, ""

    .line 79
    .local v1, "protocol":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 80
    const-string v4, "protocolInfo"

    invoke-virtual {p2, v4}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    invoke-virtual {v5, v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->createDestFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 85
    .local v0, "destFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 86
    invoke-virtual {p3, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 87
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 89
    sget-object v4, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    sget v5, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    invoke-virtual {v4, v1, v5}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 90
    new-instance v2, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;

    invoke-direct {v2, v3, v1, v0}, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 91
    .local v2, "res":Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
    return-object v2
.end method

.method public static createImportResourceProperty(Ljava/lang/String;Lcom/samsung/upnp/media/server/object/item/ImportItemNode;Lcom/samsung/upnp/media/server/object/ContentProperty;Ljava/io/InputStream;)Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
    .locals 10
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "parent"    # Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    .param p2, "prop"    # Lcom/samsung/upnp/media/server/object/ContentProperty;
    .param p3, "is"    # Ljava/io/InputStream;

    .prologue
    const/4 v6, 0x0

    .line 38
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v8

    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 39
    .local v7, "uri":Ljava/lang/String;
    const-string v4, ""

    .line 40
    .local v4, "protocol":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 41
    const-string v8, "protocolInfo"

    invoke-virtual {p2, v8}, Lcom/samsung/upnp/media/server/object/ContentProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    invoke-virtual {v9, v4}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v8, v9}, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->createDestFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 44
    .local v0, "destFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 46
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 48
    .local v5, "read":I
    if-eqz p3, :cond_1

    .line 49
    :goto_0
    :try_start_1
    invoke-virtual {p3}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    const/4 v8, -0x1

    if-ne v5, v8, :cond_5

    .line 60
    :cond_1
    if-eqz p3, :cond_2

    .line 61
    :try_start_2
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V

    .line 62
    :cond_2
    if-eqz v3, :cond_3

    .line 63
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 69
    :cond_3
    :goto_1
    new-instance v6, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;

    invoke-direct {v6, v7, v4, v0}, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .local v6, "res":Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
    move-object v2, v3

    .line 70
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "read":I
    .end local v6    # "res":Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_4
    :goto_2
    return-object v6

    .line 50
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "read":I
    :cond_5
    :try_start_3
    invoke-virtual {v3, v5}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 52
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 53
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "read":I
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :goto_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 60
    if-eqz p3, :cond_6

    .line 61
    :try_start_5
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V

    .line 62
    :cond_6
    if-eqz v2, :cond_4

    .line 63
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 64
    :catch_1
    move-exception v1

    .line 65
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 55
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 56
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 60
    if-eqz p3, :cond_7

    .line 61
    :try_start_7
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V

    .line 62
    :cond_7
    if-eqz v2, :cond_4

    .line 63
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 64
    :catch_3
    move-exception v1

    .line 65
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 58
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 60
    :goto_5
    if-eqz p3, :cond_8

    .line 61
    :try_start_8
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V

    .line 62
    :cond_8
    if-eqz v2, :cond_9

    .line 63
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 67
    :cond_9
    :goto_6
    throw v8

    .line 64
    :catch_4
    move-exception v1

    .line 65
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 64
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "read":I
    :catch_5
    move-exception v1

    .line 65
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 58
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 55
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 52
    .end local v5    # "read":I
    :catch_7
    move-exception v1

    goto :goto_3
.end method


# virtual methods
.method public deleteContent()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->mediaFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->mediaFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 117
    :cond_0
    return-void
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->mediaFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 107
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->mediaFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-object v1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/io/FileNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method
