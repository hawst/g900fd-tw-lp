.class public Lcom/samsung/upnp/media/server/ConnectionManager;
.super Ljava/lang/Object;
.source "ConnectionManager.java"

# interfaces
.implements Lcom/samsung/upnp/control/ActionListener;
.implements Lcom/samsung/upnp/control/QueryListener;


# static fields
.field public static final AV_TRNSPORT_ID:Ljava/lang/String; = "AVTransportID"

.field public static final CONNECTION_COMPLETE:Ljava/lang/String; = "ConnectionComplete"

.field public static final CONNECTION_ID:Ljava/lang/String; = "ConnectionID"

.field public static final CONNECTION_IDS:Ljava/lang/String; = "ConnectionIDs"

.field public static final DIRECTION:Ljava/lang/String; = "Direction"

.field public static final GET_CURRENT_CONNECTION_IDS:Ljava/lang/String; = "GetCurrentConnectionIDs"

.field public static final GET_CURRENT_CONNECTION_INFO:Ljava/lang/String; = "GetCurrentConnectionInfo"

.field public static final GET_PROTOCOL_INFO:Ljava/lang/String; = "GetProtocolInfo"

.field public static final HTTP_GET:Ljava/lang/String; = "http-get"

.field public static final PEER_CONNECTION_ID:Ljava/lang/String; = "PeerConnectionID"

.field public static final PEER_CONNECTION_MANAGER:Ljava/lang/String; = "PeerConnectionManager"

.field public static final PREPARE_FOR_CONNECTION:Ljava/lang/String; = "PrepareForConnection"

.field public static final PROTOCOL_INFO:Ljava/lang/String; = "ProtocolInfo"

.field public static final RCS_ID:Ljava/lang/String; = "RcsID"

.field public static final REMOTE_PROTOCOL_INFO:Ljava/lang/String; = "RemoteProtocolInfo"

.field public static final SERVICE_TYPE:Ljava/lang/String; = "urn:schemas-upnp-org:service:ConnectionManager:1"

.field public static final SINK:Ljava/lang/String; = "Sink"

.field public static final SOURCE:Ljava/lang/String; = "Source"

.field public static final STATUS:Ljava/lang/String; = "Status"


# instance fields
.field private conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

.field private maxConnectionID:I

.field private mediaServer:Lcom/samsung/upnp/media/server/MediaServer;

.field private mutex:Lcom/samsung/util/Mutex;


# direct methods
.method public constructor <init>(Lcom/samsung/upnp/media/server/MediaServer;)V
    .locals 1
    .param p1, "mserver"    # Lcom/samsung/upnp/media/server/MediaServer;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Lcom/samsung/util/Mutex;

    invoke-direct {v0}, Lcom/samsung/util/Mutex;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->mutex:Lcom/samsung/util/Mutex;

    .line 140
    new-instance v0, Lcom/samsung/upnp/media/server/ConnectionInfoList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/ConnectionInfoList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

    .line 80
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/ConnectionManager;->setMediaServer(Lcom/samsung/upnp/media/server/MediaServer;)V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->maxConnectionID:I

    .line 82
    return-void
.end method

.method private getCurrentConnectionIDs(Lcom/samsung/upnp/Action;)Z
    .locals 7
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 242
    if-nez p1, :cond_0

    .line 243
    const/4 v5, 0x0

    .line 262
    :goto_0
    return v5

    .line 244
    :cond_0
    const-string v1, ""

    .line 245
    .local v1, "conIDs":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ConnectionManager;->lock()V

    .line 246
    iget-object v5, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/ConnectionInfoList;->size()I

    move-result v4

    .line 247
    .local v4, "size":I
    const-string v5, "ConnectionManager "

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    if-nez v4, :cond_3

    .line 249
    const-string v1, "0"

    .line 258
    :cond_1
    const-string v5, "ConnectionIDs"

    invoke-virtual {p1, v5}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 259
    .local v0, "argument":Lcom/samsung/upnp/Argument;
    if-eqz v0, :cond_2

    .line 260
    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 261
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ConnectionManager;->unlock()V

    .line 262
    const/4 v5, 0x1

    goto :goto_0

    .line 251
    .end local v0    # "argument":Lcom/samsung/upnp/Argument;
    :cond_3
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    if-ge v3, v4, :cond_1

    .line 252
    iget-object v5, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

    invoke-virtual {v5, v3}, Lcom/samsung/upnp/media/server/ConnectionInfoList;->getConnectionInfo(I)Lcom/samsung/upnp/media/server/ConnectionInfo;

    move-result-object v2

    .line 253
    .local v2, "info":Lcom/samsung/upnp/media/server/ConnectionInfo;
    if-lez v3, :cond_4

    .line 254
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 255
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getID()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 251
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private getCurrentConnectionInfo(Lcom/samsung/upnp/Action;)Z
    .locals 7
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v6, -0x1

    .line 271
    if-nez p1, :cond_0

    .line 323
    :goto_0
    return v3

    .line 273
    :cond_0
    const-string v5, "ConnectionID"

    invoke-virtual {p1, v5}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 274
    .local v0, "argument":Lcom/samsung/upnp/Argument;
    const/4 v1, -0x1

    .line 275
    .local v1, "id":I
    if-eqz v0, :cond_1

    .line 276
    invoke-virtual {v0}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v1

    .line 277
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ConnectionManager;->lock()V

    .line 278
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/ConnectionManager;->getConnectionInfo(I)Lcom/samsung/upnp/media/server/ConnectionInfo;

    move-result-object v2

    .line 279
    .local v2, "info":Lcom/samsung/upnp/media/server/ConnectionInfo;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ConnectionManager;->unlock()V

    .line 281
    iget-object v5, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/ConnectionInfoList;->size()I

    move-result v5

    if-nez v5, :cond_8

    if-nez v1, :cond_8

    .line 282
    const-string v3, "RcsID"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_2

    .line 284
    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 285
    :cond_2
    const-string v3, "AVTransportID"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_3

    .line 287
    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 288
    :cond_3
    const-string v3, "PeerConnectionManager"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_4

    .line 290
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 291
    :cond_4
    const-string v3, "PeerConnectionID"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_5

    .line 293
    invoke-virtual {v0, v6}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 294
    :cond_5
    const-string v3, "Direction"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_6

    .line 296
    const-string v3, "Output"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 297
    :cond_6
    const-string v3, "Status"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_7

    .line 299
    const-string v3, "Unknown"

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    :cond_7
    move v3, v4

    .line 300
    goto :goto_0

    .line 301
    :cond_8
    if-eqz v2, :cond_f

    .line 302
    const-string v3, "RcsID"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_9

    .line 304
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getRcsID()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 305
    :cond_9
    const-string v3, "AVTransportID"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_a

    .line 307
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getAVTransportID()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 308
    :cond_a
    const-string v3, "PeerConnectionManager"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 309
    if-eqz v0, :cond_b

    .line 310
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getPeerConnectionManager()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 311
    :cond_b
    const-string v3, "PeerConnectionID"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_c

    .line 313
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getPeerConnectionID()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 314
    :cond_c
    const-string v3, "Direction"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_d

    .line 316
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getDirection()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 317
    :cond_d
    const-string v3, "Status"

    invoke-virtual {p1, v3}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_e

    .line 319
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    :cond_e
    move v3, v4

    .line 320
    goto/16 :goto_0

    .line 322
    :cond_f
    const/16 v4, 0x191

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid connection reference ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setMediaServer(Lcom/samsung/upnp/media/server/MediaServer;)V
    .locals 0
    .param p1, "mserver"    # Lcom/samsung/upnp/media/server/MediaServer;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->mediaServer:Lcom/samsung/upnp/media/server/MediaServer;

    .line 93
    return-void
.end method


# virtual methods
.method public actionControlReceived(Lcom/samsung/upnp/Action;)Z
    .locals 6
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, -0x1

    .line 193
    if-nez p1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v2

    .line 195
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "actionName":Ljava/lang/String;
    const-string v4, "DLNA"

    invoke-static {v4, v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const/4 v1, 0x0

    .line 198
    .local v1, "argument":Lcom/samsung/upnp/Argument;
    const-string v4, "GetProtocolInfo"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 199
    const-string v2, "Source"

    invoke-virtual {p1, v2}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_2

    .line 201
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getCurrentProtocolInfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 203
    :cond_2
    const-string v2, "Sink"

    invoke-virtual {p1, v2}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 204
    if-eqz v1, :cond_3

    .line 205
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    :cond_3
    move v2, v3

    .line 207
    goto :goto_0

    .line 210
    :cond_4
    const-string v4, "PrepareForConnection"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 211
    const-string v2, "ConnectionID"

    invoke-virtual {p1, v2}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 212
    if-eqz v1, :cond_5

    .line 213
    invoke-virtual {v1, v5}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 214
    :cond_5
    const-string v2, "AVTransportID"

    invoke-virtual {p1, v2}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 215
    if-eqz v1, :cond_6

    .line 216
    invoke-virtual {v1, v5}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 217
    :cond_6
    const-string v2, "RcsID"

    invoke-virtual {p1, v2}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_7

    .line 219
    invoke-virtual {v1, v5}, Lcom/samsung/upnp/Argument;->setValue(I)V

    :cond_7
    move v2, v3

    .line 220
    goto :goto_0

    .line 223
    :cond_8
    const-string v4, "ConnectionComplete"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    move v2, v3

    .line 224
    goto :goto_0

    .line 227
    :cond_9
    const-string v3, "GetCurrentConnectionInfo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 228
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/ConnectionManager;->getCurrentConnectionInfo(Lcom/samsung/upnp/Action;)Z

    move-result v2

    goto :goto_0

    .line 230
    :cond_a
    const-string v3, "GetCurrentConnectionIDs"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 231
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/ConnectionManager;->getCurrentConnectionIDs(Lcom/samsung/upnp/Action;)Z

    move-result v2

    goto/16 :goto_0
.end method

.method public getConnectionInfo(I)Lcom/samsung/upnp/media/server/ConnectionInfo;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 149
    iget-object v3, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

    invoke-virtual {v3}, Lcom/samsung/upnp/media/server/ConnectionInfoList;->size()I

    move-result v2

    .line 150
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 155
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 151
    :cond_1
    iget-object v3, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->conInfoList:Lcom/samsung/upnp/media/server/ConnectionInfoList;

    invoke-virtual {v3, v1}, Lcom/samsung/upnp/media/server/ConnectionInfoList;->getConnectionInfo(I)Lcom/samsung/upnp/media/server/ConnectionInfo;

    move-result-object v0

    .line 152
    .local v0, "info":Lcom/samsung/upnp/media/server/ConnectionInfo;
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/ConnectionInfo;->getID()I

    move-result v3

    if-eq v3, p1, :cond_0

    .line 150
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public lock()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->mutex:Lcom/samsung/util/Mutex;

    invoke-virtual {v0}, Lcom/samsung/util/Mutex;->lock()V

    .line 114
    return-void
.end method

.method public queryControlReceived(Lcom/samsung/upnp/StateVariable;)Z
    .locals 1
    .param p1, "stateVar"    # Lcom/samsung/upnp/StateVariable;

    .prologue
    .line 333
    const/4 v0, 0x0

    return v0
.end method

.method public unlock()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionManager;->mutex:Lcom/samsung/util/Mutex;

    invoke-virtual {v0}, Lcom/samsung/util/Mutex;->unlock()V

    .line 119
    return-void
.end method
