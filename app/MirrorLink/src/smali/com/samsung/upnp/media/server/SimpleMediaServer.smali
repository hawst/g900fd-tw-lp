.class public Lcom/samsung/upnp/media/server/SimpleMediaServer;
.super Ljava/lang/Object;
.source "SimpleMediaServer.java"

# interfaces
.implements Lcom/samsung/http/HTTPRequestListener;


# static fields
.field public static final DEFAULT_HTTP_PORT:I = 0x5c16


# instance fields
.field playItem:Lcom/samsung/upnp/media/server/object/item/ItemNode;

.field private server:Lcom/samsung/http/HTTPServer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/samsung/http/HTTPServer;

    invoke-direct {v0}, Lcom/samsung/http/HTTPServer;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    .line 50
    return-void
.end method


# virtual methods
.method public getRegisteredContents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/upnp/media/server/object/item/ItemNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 4
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 97
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "uri":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "uri = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->playItem:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-static {p1, v2}, Lcom/samsung/upnp/media/server/CDSResponseBuilder;->buildResponse(Lcom/samsung/http/HTTPRequest;Lcom/samsung/upnp/media/server/object/item/ItemNode;)Lcom/samsung/http/HTTPResponse;

    move-result-object v0

    .line 100
    .local v0, "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {p1, v0}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 101
    return-void
.end method

.method public isServerStarted()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v0}, Lcom/samsung/http/HTTPServer;->isOpened()Z

    move-result v0

    goto :goto_0
.end method

.method public registerContent(Lcom/samsung/upnp/media/server/object/item/ItemNode;)V
    .locals 0
    .param p1, "item"    # Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->playItem:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .line 86
    return-void
.end method

.method public startServer(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v2, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v2}, Lcom/samsung/http/HTTPServer;->isOpened()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const-string v2, "server is already started"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 71
    :goto_0
    return-object v1

    .line 59
    :cond_0
    const/16 v0, 0x5c16

    .line 61
    .local v0, "httpPort":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v2, p1, v0}, Lcom/samsung/http/HTTPServer;->open(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Simple media server starts!!! - Address: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Port: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v1, p0}, Lcom/samsung/http/HTTPServer;->addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V

    .line 70
    iget-object v1, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v1}, Lcom/samsung/http/HTTPServer;->start()Z

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 62
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 63
    add-int/lit16 v2, v0, -0x5c16

    const/16 v3, 0x64

    if-le v2, v3, :cond_1

    .line 64
    const-string v2, "fail to start a simple media server"

    invoke-static {v2}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopServer()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v0}, Lcom/samsung/http/HTTPServer;->stop()Z

    .line 76
    iget-object v0, p0, Lcom/samsung/upnp/media/server/SimpleMediaServer;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v0}, Lcom/samsung/http/HTTPServer;->close()Z

    .line 77
    return-void
.end method

.method public unregisterContent(Lcom/samsung/upnp/media/server/object/item/ItemNode;)V
    .locals 0
    .param p1, "item"    # Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .prologue
    .line 89
    return-void
.end method
