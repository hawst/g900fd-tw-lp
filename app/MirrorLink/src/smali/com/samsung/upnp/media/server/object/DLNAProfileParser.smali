.class public Lcom/samsung/upnp/media/server/object/DLNAProfileParser;
.super Ljava/lang/Object;
.source "DLNAProfileParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE:[I = null

.field public static final AAC_ADTS:Ljava/lang/String; = "AAC_ADTS"

.field public static final AAC_ADTS_320:Ljava/lang/String; = "AAC_ADTS_320"

.field public static final AAC_ISO:Ljava/lang/String; = "AAC_ISO"

.field public static final AAC_ISO_320:Ljava/lang/String; = "AAC_ISO_320"

.field public static final AAC_LTP_ISO:Ljava/lang/String; = "AAC_LTP_ISO"

.field public static final AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AAC_LTP_MULT5_ISO"

.field public static final AAC_LTP_MULT7_ISO:Ljava/lang/String; = "AAC_LTP_MULT7_ISO"

.field public static final AAC_MULT5_ADTS:Ljava/lang/String; = "AAC_MULT5_ADTS"

.field public static final AAC_MULT5_ISO:Ljava/lang/String; = "AAC_MULT5_ISO"

.field public static final AC3:Ljava/lang/String; = "AC3"

.field public static final AMR_3GPP:Ljava/lang/String; = "AMR_3GPP"

.field public static final AMR_WBplus:Ljava/lang/String; = "AMR_WBplus"

.field public static final ATRAC3plus:Ljava/lang/String; = "ATRAC3plus"

.field public static final AVC_3GPP_BL_CIF15_AMR_WBplus:Ljava/lang/String; = "AVC_3GPP_BL_CIF15_AMR_WBplus"

.field public static final AVC_3GPP_BL_CIF30_AMR_WBplus:Ljava/lang/String; = "AVC_3GPP_BL_CIF30_AMR_WBplus"

.field public static final AVC_3GPP_BL_QCIF15_AAC:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AAC"

.field public static final AVC_3GPP_BL_QCIF15_AAC_LTP:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AAC_LTP"

.field public static final AVC_3GPP_BL_QCIF15_AMR:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AMR"

.field public static final AVC_3GPP_BL_QCIF15_AMR_WBplus:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_AMR_WBplus"

.field public static final AVC_3GPP_BL_QCIF15_HEAAC:Ljava/lang/String; = "AVC_3GPP_BL_QCIF15_HEAAC"

.field public static final AVC_MP4_BL_CIF15_AAC:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC"

.field public static final AVC_MP4_BL_CIF15_AAC_520:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_520"

.field public static final AVC_MP4_BL_CIF15_AAC_LTP:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_LTP"

.field public static final AVC_MP4_BL_CIF15_AAC_LTP_520:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_LTP_520"

.field public static final AVC_MP4_BL_CIF15_AMR:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AMR"

.field public static final AVC_MP4_BL_CIF15_BSAC:Ljava/lang/String; = "AVC_MP4_BL_CIF15_BSAC"

.field public static final AVC_MP4_BL_CIF15_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_CIF15_HEAAC"

.field public static final AVC_MP4_BL_CIF30_AAC_LTP:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AAC_LTP"

.field public static final AVC_MP4_BL_CIF30_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AAC_LTP_MULT5"

.field public static final AVC_MP4_BL_CIF30_AAC_MULT5:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AAC_MULT5"

.field public static final AVC_MP4_BL_CIF30_AC3:Ljava/lang/String; = "AVC_MP4_BL_CIF30_AC3"

.field public static final AVC_MP4_BL_CIF30_BSAC:Ljava/lang/String; = "AVC_MP4_BL_CIF30_BSAC"

.field public static final AVC_MP4_BL_CIF30_BSAC_MULT5:Ljava/lang/String; = "AVC_MP4_BL_CIF30_BSAC_MULT5"

.field public static final AVC_MP4_BL_CIF30_HEAAC_L2:Ljava/lang/String; = "AVC_MP4_BL_CIF30_HEAAC_L2"

.field public static final AVC_MP4_BL_CIF30_MPEG1_L3:Ljava/lang/String; = "AVC_MP4_BL_CIF30_MPEG1_L3"

.field public static final AVC_MP4_BL_L12_CIF15_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_L12_CIF15_HEAAC"

.field public static final AVC_MP4_BL_L1B_QCIF15_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_L1B_QCIF15_HEAAC"

.field public static final AVC_MP4_BL_L2_CIF30_AAC:Ljava/lang/String; = "AVC_MP4_BL_L2_CIF30_AAC"

.field public static final AVC_MP4_BL_L3L_SD_AAC:Ljava/lang/String; = "AVC_MP4_BL_L3L_SD_AAC"

.field public static final AVC_MP4_BL_L3L_SD_HEAAC:Ljava/lang/String; = "AVC_MP4_BL_L3L_SD_HEAAC"

.field public static final AVC_MP4_BL_L3_SD_AAC:Ljava/lang/String; = "AVC_MP4_BL_L3_SD_AAC"

.field public static final AVC_MP4_MP_SD_AAC_LTP:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_LTP"

.field public static final AVC_MP4_MP_SD_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_LTP_MULT5"

.field public static final AVC_MP4_MP_SD_AAC_LTP_MULT7:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_LTP_MULT7"

.field public static final AVC_MP4_MP_SD_AAC_MULT5:Ljava/lang/String; = "AVC_MP4_MP_SD_AAC_MULT5"

.field public static final AVC_MP4_MP_SD_AC3:Ljava/lang/String; = "AVC_MP4_MP_SD_AC3"

.field public static final AVC_MP4_MP_SD_ATRAC3plus:Ljava/lang/String; = "AVC_MP4_MP_SD_ATRAC3plus"

.field public static final AVC_MP4_MP_SD_BSAC:Ljava/lang/String; = "AVC_MP4_MP_SD_BSAC"

.field public static final AVC_MP4_MP_SD_HEAAC_L2:Ljava/lang/String; = "AVC_MP4_MP_SD_HEAAC_L2"

.field public static final AVC_MP4_MP_SD_MPEG1_L3:Ljava/lang/String; = "AVC_MP4_MP_SD_MPEG1_L3"

.field public static final AVC_TS_BL_CIF15_AAC:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC"

.field public static final AVC_TS_BL_CIF15_AAC_540:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_540"

.field public static final AVC_TS_BL_CIF15_AAC_540_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_540_ISO"

.field public static final AVC_TS_BL_CIF15_AAC_540_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_540_T"

.field public static final AVC_TS_BL_CIF15_AAC_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_ISO"

.field public static final AVC_TS_BL_CIF15_AAC_LTP:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_LTP"

.field public static final AVC_TS_BL_CIF15_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_LTP_ISO"

.field public static final AVC_TS_BL_CIF15_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_LTP_T"

.field public static final AVC_TS_BL_CIF15_AAC_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_AAC_T"

.field public static final AVC_TS_BL_CIF15_BSAC:Ljava/lang/String; = "AVC_TS_BL_CIF15_BSAC"

.field public static final AVC_TS_BL_CIF15_BSAC_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF15_BSAC_ISO"

.field public static final AVC_TS_BL_CIF15_BSAC_T:Ljava/lang/String; = "AVC_TS_BL_CIF15_BSAC_T"

.field public static final AVC_TS_BL_CIF30_AAC_940:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_940"

.field public static final AVC_TS_BL_CIF30_AAC_940_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_940_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_940_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_940_T"

.field public static final AVC_TS_BL_CIF30_AAC_LTP:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_MULT5"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_MULT5_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_MULT5_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_MULT5_T"

.field public static final AVC_TS_BL_CIF30_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_LTP_T"

.field public static final AVC_TS_BL_CIF30_AAC_MULT5:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_MULT5"

.field public static final AVC_TS_BL_CIF30_AAC_MULT5_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_MULT5_ISO"

.field public static final AVC_TS_BL_CIF30_AAC_MULT5_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AAC_MULT5_T"

.field public static final AVC_TS_BL_CIF30_AC3:Ljava/lang/String; = "AVC_TS_BL_CIF30_AC3"

.field public static final AVC_TS_BL_CIF30_AC3_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_AC3_ISO"

.field public static final AVC_TS_BL_CIF30_AC3_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_AC3_T"

.field public static final AVC_TS_BL_CIF30_HEAAC_L2:Ljava/lang/String; = "AVC_TS_BL_CIF30_HEAAC_L2"

.field public static final AVC_TS_BL_CIF30_HEAAC_L2_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_HEAAC_L2_ISO"

.field public static final AVC_TS_BL_CIF30_HEAAC_L2_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_HEAAC_L2_T"

.field public static final AVC_TS_BL_CIF30_MPEG1_L3:Ljava/lang/String; = "AVC_TS_BL_CIF30_MPEG1_L3"

.field public static final AVC_TS_BL_CIF30_MPEG1_L3_ISO:Ljava/lang/String; = "AVC_TS_BL_CIF30_MPEG1_L3_ISO"

.field public static final AVC_TS_BL_CIF30_MPEG1_L3_T:Ljava/lang/String; = "AVC_TS_BL_CIF30_MPEG1_L3_T"

.field public static final AVC_TS_MP_HD_AAC:Ljava/lang/String; = "AVC_TS_MP_HD_AAC"

.field public static final AVC_TS_MP_HD_AAC_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP"

.field public static final AVC_TS_MP_HD_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT5"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT5_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT5_T"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT7:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT7"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT7_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT7_ISO"

.field public static final AVC_TS_MP_HD_AAC_LTP_MULT7_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_MULT7_T"

.field public static final AVC_TS_MP_HD_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_LTP_T"

.field public static final AVC_TS_MP_HD_AAC_MULT5:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_MULT5"

.field public static final AVC_TS_MP_HD_AAC_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_MULT5_ISO"

.field public static final AVC_TS_MP_HD_AAC_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_MULT5_T"

.field public static final AVC_TS_MP_HD_AAC_T:Ljava/lang/String; = "AVC_TS_MP_HD_AAC_T"

.field public static final AVC_TS_MP_HD_AC3:Ljava/lang/String; = "AVC_TS_MP_HD_AC3"

.field public static final AVC_TS_MP_HD_AC3_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_AC3_ISO"

.field public static final AVC_TS_MP_HD_AC3_T:Ljava/lang/String; = "AVC_TS_MP_HD_AC3_T"

.field public static final AVC_TS_MP_HD_HEAAC_L2:Ljava/lang/String; = "AVC_TS_MP_HD_HEAAC_L2"

.field public static final AVC_TS_MP_HD_HEAAC_L2_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_HEAAC_L2_ISO"

.field public static final AVC_TS_MP_HD_HEAAC_L2_T:Ljava/lang/String; = "AVC_TS_MP_HD_HEAAC_L2_T"

.field public static final AVC_TS_MP_HD_MPEG1_L3:Ljava/lang/String; = "AVC_TS_MP_HD_MPEG1_L3"

.field public static final AVC_TS_MP_HD_MPEG1_L3_ISO:Ljava/lang/String; = "AVC_TS_MP_HD_MPEG1_L3_ISO"

.field public static final AVC_TS_MP_HD_MPEG1_L3_T:Ljava/lang/String; = "AVC_TS_MP_HD_MPEG1_L3_T"

.field public static final AVC_TS_MP_SD_AAC_LTP:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP"

.field public static final AVC_TS_MP_SD_AAC_LTP_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_ISO"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT5:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT5"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT5_ISO"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT5_T"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT7:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT7"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT7_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT7_ISO"

.field public static final AVC_TS_MP_SD_AAC_LTP_MULT7_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_MULT7_T"

.field public static final AVC_TS_MP_SD_AAC_LTP_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_LTP_T"

.field public static final AVC_TS_MP_SD_AAC_MULT5:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_MULT5"

.field public static final AVC_TS_MP_SD_AAC_MULT5_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_MULT5_ISO"

.field public static final AVC_TS_MP_SD_AAC_MULT5_T:Ljava/lang/String; = "AVC_TS_MP_SD_AAC_MULT5_T"

.field public static final AVC_TS_MP_SD_AC3:Ljava/lang/String; = "AVC_TS_MP_SD_AC3"

.field public static final AVC_TS_MP_SD_AC3_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_AC3_ISO"

.field public static final AVC_TS_MP_SD_AC3_T:Ljava/lang/String; = "AVC_TS_MP_SD_AC3_T"

.field public static final AVC_TS_MP_SD_BSAC:Ljava/lang/String; = "AVC_TS_MP_SD_BSAC"

.field public static final AVC_TS_MP_SD_BSAC_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_BSAC_ISO"

.field public static final AVC_TS_MP_SD_BSAC_T:Ljava/lang/String; = "AVC_TS_MP_SD_BSAC_T"

.field public static final AVC_TS_MP_SD_HEAAC_L2:Ljava/lang/String; = "AVC_TS_MP_SD_HEAAC_L2"

.field public static final AVC_TS_MP_SD_HEAAC_L2_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_HEAAC_L2_ISO"

.field public static final AVC_TS_MP_SD_HEAAC_L2_T:Ljava/lang/String; = "AVC_TS_MP_SD_HEAAC_L2_T"

.field public static final AVC_TS_MP_SD_MPEG1_L3:Ljava/lang/String; = "AVC_TS_MP_SD_MPEG1_L3"

.field public static final AVC_TS_MP_SD_MPEG1_L3_ISO:Ljava/lang/String; = "AVC_TS_MP_SD_MPEG1_L3_ISO"

.field public static final AVC_TS_MP_SD_MPEG1_L3_T:Ljava/lang/String; = "AVC_TS_MP_SD_MPEG1_L3_T"

.field public static final BSAC_ISO:Ljava/lang/String; = "BSAC_ISO"

.field public static final BSAC_MULT5_ISO:Ljava/lang/String; = "BSAC_MULT5_ISO"

.field public static final HEAAC_L2_ADTS:Ljava/lang/String; = "HEAAC_L2_ADTS"

.field public static final HEAAC_L2_ADTS_320:Ljava/lang/String; = "HEAAC_L2_ADTS_320"

.field public static final HEAAC_L2_ISO:Ljava/lang/String; = "HEAAC_L2_ISO"

.field public static final HEAAC_L2_ISO_320:Ljava/lang/String; = "HEAAC_L2_ISO_320"

.field public static final HEAAC_L3_ADTS:Ljava/lang/String; = "HEAAC_L3_ADTS"

.field public static final HEAAC_L3_ISO:Ljava/lang/String; = "HEAAC_L3_ISO"

.field public static final HEAAC_MULT5_ADTS:Ljava/lang/String; = "HEAAC_MULT5_ADTS"

.field public static final HEAAC_MULT5_ISO:Ljava/lang/String; = "HEAAC_MULT5_ISO"

.field public static final JPEG_LRG:Ljava/lang/String; = "JPEG_LRG"

.field public static final JPEG_LRG_ICO:Ljava/lang/String; = "JPEG_LRG_ICO"

.field public static final JPEG_MED:Ljava/lang/String; = "JPEG_MED"

.field public static final JPEG_SM:Ljava/lang/String; = "JPEG_SM"

.field public static final JPEG_SM_ICO:Ljava/lang/String; = "JPEG_SM_ICO"

.field public static final JPEG_TN:Ljava/lang/String; = "JPEG_TN"

.field public static final LPCM:Ljava/lang/String; = "LPCM"

.field public static final MIME_AUDIO_3GPP:Ljava/lang/String; = "audio/3gpp"

.field public static final MIME_AUDIO_ADTS:Ljava/lang/String; = "audio/vnd.dlna.adts"

.field public static final MIME_AUDIO_L16:Ljava/lang/String; = "audio/L16"

.field public static final MIME_AUDIO_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIME_AUDIO_MPEG:Ljava/lang/String; = "audio/mpeg"

.field public static final MIME_AUDIO_RAW:Ljava/lang/String; = "audio/vnd.dolby.dd-raw"

.field public static final MIME_AUDIO_SONY_OMA:Ljava/lang/String; = "audio/x-sony-oma"

.field public static final MIME_AUDIO_WMA:Ljava/lang/String; = "audio/x-ms-wma"

.field public static final MIME_IMAGE_JPEG:Ljava/lang/String; = "image/jpeg"

.field public static final MIME_IMAGE_PNG:Ljava/lang/String; = "image/png"

.field public static final MIME_VIDEO_3GPP:Ljava/lang/String; = "video/3gpp"

.field public static final MIME_VIDEO_ASF:Ljava/lang/String; = "video/x-ms-asf"

.field public static final MIME_VIDEO_AVI:Ljava/lang/String; = "video/avi"

.field public static final MIME_VIDEO_MP4:Ljava/lang/String; = "video/mp4"

.field public static final MIME_VIDEO_MPEG:Ljava/lang/String; = "video/mpeg"

.field public static final MIME_VIDEO_TTS:Ljava/lang/String; = "video/vnd.dlna.mpeg-tts"

.field public static final MIME_VIDEO_WMV:Ljava/lang/String; = "video/x-ms-wmv"

.field public static final MP3:Ljava/lang/String; = "MP3"

.field public static final MP3X:Ljava/lang/String; = "MP3X"

.field public static final MPEG1:Ljava/lang/String; = "MPEG1"

.field public static final MPEG4_H263_3GPP_P0_L10_AMR_WBplus:Ljava/lang/String; = "MPEG4_H263_3GPP_P0_L10_AMR_WBplus"

.field public static final MPEG4_H263_3GPP_P3_L10_AMR:Ljava/lang/String; = "MPEG4_H263_3GPP_P3_L10_AMR"

.field public static final MPEG4_H263_MP4_P0_L10_AAC:Ljava/lang/String; = "MPEG4_H263_MP4_P0_L10_AAC"

.field public static final MPEG4_H263_MP4_P0_L10_AAC_LTP:Ljava/lang/String; = "MPEG4_H263_MP4_P0_L10_AAC_LTP"

.field public static final MPEG4_P2_3GPP_SP_L0B_AAC:Ljava/lang/String; = "MPEG4_P2_3GPP_SP_L0B_AAC"

.field public static final MPEG4_P2_3GPP_SP_L0B_AMR:Ljava/lang/String; = "MPEG4_P2_3GPP_SP_L0B_AMR"

.field public static final MPEG4_P2_ASF_ASP_L4_SO_G726:Ljava/lang/String; = "MPEG4_P2_ASF_ASP_L4_SO_G726"

.field public static final MPEG4_P2_ASF_ASP_L5_SO_G726:Ljava/lang/String; = "MPEG4_P2_ASF_ASP_L5_SO_G726"

.field public static final MPEG4_P2_ASF_SP_G726:Ljava/lang/String; = "MPEG4_P2_ASF_SP_G726"

.field public static final MPEG4_P2_MP4_ASP_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_AAC"

.field public static final MPEG4_P2_MP4_ASP_ATRAC3plus:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_ATRAC3plus"

.field public static final MPEG4_P2_MP4_ASP_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_HEAAC"

.field public static final MPEG4_P2_MP4_ASP_HEAAC_MULT5:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_HEAAC_MULT5"

.field public static final MPEG4_P2_MP4_ASP_L4_SO_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L4_SO_AAC"

.field public static final MPEG4_P2_MP4_ASP_L4_SO_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L4_SO_HEAAC"

.field public static final MPEG4_P2_MP4_ASP_L4_SO_HEAAC_MULT5:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L4_SO_HEAAC_MULT5"

.field public static final MPEG4_P2_MP4_ASP_L5_SO_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L5_SO_AAC"

.field public static final MPEG4_P2_MP4_ASP_L5_SO_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L5_SO_HEAAC"

.field public static final MPEG4_P2_MP4_ASP_L5_SO_HEAAC_MULT5:Ljava/lang/String; = "MPEG4_P2_MP4_ASP_L5_SO_HEAAC_MULT5"

.field public static final MPEG4_P2_MP4_SP_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_AAC"

.field public static final MPEG4_P2_MP4_SP_AAC_LTP:Ljava/lang/String; = "MPEG4_P2_MP4_SP_AAC_LTP"

.field public static final MPEG4_P2_MP4_SP_ATRAC3plus:Ljava/lang/String; = "MPEG4_P2_MP4_SP_ATRAC3plus"

.field public static final MPEG4_P2_MP4_SP_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_HEAAC"

.field public static final MPEG4_P2_MP4_SP_L2_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_L2_AAC"

.field public static final MPEG4_P2_MP4_SP_L2_AMR:Ljava/lang/String; = "MPEG4_P2_MP4_SP_L2_AMR"

.field public static final MPEG4_P2_MP4_SP_VGA_AAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_VGA_AAC"

.field public static final MPEG4_P2_MP4_SP_VGA_HEAAC:Ljava/lang/String; = "MPEG4_P2_MP4_SP_VGA_HEAAC"

.field public static final MPEG4_P2_TS_ASP_AAC:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AAC"

.field public static final MPEG4_P2_TS_ASP_AAC_ISO:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AAC_ISO"

.field public static final MPEG4_P2_TS_ASP_AAC_T:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AAC_T"

.field public static final MPEG4_P2_TS_ASP_AC3:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AC3"

.field public static final MPEG4_P2_TS_ASP_AC3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AC3_ISO"

.field public static final MPEG4_P2_TS_ASP_AC3_T:Ljava/lang/String; = "MPEG4_P2_TS_ASP_AC3_T"

.field public static final MPEG4_P2_TS_ASP_MPEG1_L3:Ljava/lang/String; = "MPEG4_P2_TS_ASP_MPEG1_L3"

.field public static final MPEG4_P2_TS_ASP_MPEG1_L3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_ASP_MPEG1_L3_ISO"

.field public static final MPEG4_P2_TS_ASP_MPEG1_L3_T:Ljava/lang/String; = "MPEG4_P2_TS_ASP_MPEG1_L3_T"

.field public static final MPEG4_P2_TS_CO_AC3:Ljava/lang/String; = "MPEG4_P2_TS_CO_AC3"

.field public static final MPEG4_P2_TS_CO_AC3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_CO_AC3_ISO"

.field public static final MPEG4_P2_TS_CO_AC3_T:Ljava/lang/String; = "MPEG4_P2_TS_CO_AC3_T"

.field public static final MPEG4_P2_TS_CO_MPEG2_L2:Ljava/lang/String; = "MPEG4_P2_TS_CO_MPEG2_L2"

.field public static final MPEG4_P2_TS_CO_MPEG2_L2_ISO:Ljava/lang/String; = "MPEG4_P2_TS_CO_MPEG2_L2_ISO"

.field public static final MPEG4_P2_TS_CO_MPEG2_L2_T:Ljava/lang/String; = "MPEG4_P2_TS_CO_MPEG2_L2_T"

.field public static final MPEG4_P2_TS_SP_AAC:Ljava/lang/String; = "MPEG4_P2_TS_SP_AAC"

.field public static final MPEG4_P2_TS_SP_AAC_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_AAC_ISO"

.field public static final MPEG4_P2_TS_SP_AAC_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_AAC_T"

.field public static final MPEG4_P2_TS_SP_AC3:Ljava/lang/String; = "MPEG4_P2_TS_SP_AC3"

.field public static final MPEG4_P2_TS_SP_AC3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_AC3_ISO"

.field public static final MPEG4_P2_TS_SP_AC3_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_AC3_T"

.field public static final MPEG4_P2_TS_SP_MEPG1_L3:Ljava/lang/String; = "MPEG4_P2_TS_SP_MEPG1_L3"

.field public static final MPEG4_P2_TS_SP_MPEG1_L3_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG1_L3_ISO"

.field public static final MPEG4_P2_TS_SP_MPEG1_L3_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG1_L3_T"

.field public static final MPEG4_P2_TS_SP_MPEG2_L2:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG2_L2"

.field public static final MPEG4_P2_TS_SP_MPEG2_L2_ISO:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG2_L2_ISO"

.field public static final MPEG4_P2_TS_SP_MPEG2_L2_T:Ljava/lang/String; = "MPEG4_P2_TS_SP_MPEG2_L2_T"

.field public static final MPEG_ES_NTSC:Ljava/lang/String; = "MPEG_ES_NTSC"

.field public static final MPEG_ES_NTSC_XAC3:Ljava/lang/String; = "MPEG_ES_NTSC_XAC3"

.field public static final MPEG_ES_PAL:Ljava/lang/String; = "MPEG_ES_PAL"

.field public static final MPEG_ES_PAL_XAC3:Ljava/lang/String; = "MPEG_ES_PAL_XAC3"

.field public static final MPEG_PS_NTSC:Ljava/lang/String; = "MPEG_PS_NTSC"

.field public static final MPEG_PS_NTSC_X_AC3:Ljava/lang/String; = "MPEG_PS_NTSC_X_AC3"

.field public static final MPEG_PS_PAL:Ljava/lang/String; = "MPEG_PS_PAL"

.field public static final MPEG_PS_PAL_XAC3:Ljava/lang/String; = "MPEG_PS_PAL_XAC3"

.field public static final MPEG_TS_HD_KO:Ljava/lang/String; = "MPEG_TS_HD_KO"

.field public static final MPEG_TS_HD_KO_ISO:Ljava/lang/String; = "MPEG_TS_HD_KO_ISO"

.field public static final MPEG_TS_HD_KO_T:Ljava/lang/String; = "MPEG_TS_HD_KO_T"

.field public static final MPEG_TS_HD_KO_XAC3:Ljava/lang/String; = "MPEG_TS_HD_KO_XAC3"

.field public static final MPEG_TS_HD_KO_XAC3_ISO:Ljava/lang/String; = "MPEG_TS_HD_KO_XAC3_ISO"

.field public static final MPEG_TS_HD_KO_XAC3_T:Ljava/lang/String; = "MPEG_TS_HD_KO_XAC3_T"

.field public static final MPEG_TS_HD_NA:Ljava/lang/String; = "MPEG_TS_HD_NA"

.field public static final MPEG_TS_HD_NA_ISO:Ljava/lang/String; = "MPEG_TS_HD_NA_ISO"

.field public static final MPEG_TS_HD_NA_T:Ljava/lang/String; = "MPEG_TS_HD_NA_T"

.field public static final MPEG_TS_HD_NA_XAC3:Ljava/lang/String; = "MPEG_TS_HD_NA_XAC3"

.field public static final MPEG_TS_HD_NA_XAC3_ISO:Ljava/lang/String; = "MPEG_TS_HD_NA_XAC3_ISO"

.field public static final MPEG_TS_HD_NA_XAC3_T:Ljava/lang/String; = "MPEG_TS_HD_NA_XAC3_T"

.field public static final MPEG_TS_MP_LL_AAC:Ljava/lang/String; = "MPEG_TS_MP_LL_AAC"

.field public static final MPEG_TS_MP_LL_AAC_ISO:Ljava/lang/String; = "MPEG_TS_MP_LL_AAC_ISO"

.field public static final MPEG_TS_MP_LL_AAC_T:Ljava/lang/String; = "MPEG_TS_MP_LL_AAC_T"

.field public static final MPEG_TS_SD_EU:Ljava/lang/String; = "MPEG_TS_SD_EU"

.field public static final MPEG_TS_SD_EU_ISO:Ljava/lang/String; = "MPEG_TS_SD_EU_ISO"

.field public static final MPEG_TS_SD_EU_T:Ljava/lang/String; = "MPEG_TS_SD_EU_T"

.field public static final MPEG_TS_SD_KO:Ljava/lang/String; = "MPEG_TS_SD_KO"

.field public static final MPEG_TS_SD_KO_ISO:Ljava/lang/String; = "MPEG_TS_SD_KO_ISO"

.field public static final MPEG_TS_SD_KO_T:Ljava/lang/String; = "MPEG_TS_SD_KO_T"

.field public static final MPEG_TS_SD_KO_XAC3_T:Ljava/lang/String; = "MPEG_TS_SD_KO_XAC3_T"

.field public static final MPEG_TS_SD_KO_X_AC3:Ljava/lang/String; = "MPEG_TS_SD_KO_X_AC3"

.field public static final MPEG_TS_SD_KO_X_AC3_ISO:Ljava/lang/String; = "MPEG_TS_SD_KO_X_AC3_ISO"

.field public static final MPEG_TS_SD_NA:Ljava/lang/String; = "MPEG_TS_SD_NA"

.field public static final MPEG_TS_SD_NA_ISO:Ljava/lang/String; = "MPEG_TS_SD_NA_ISO"

.field public static final MPEG_TS_SD_NA_T:Ljava/lang/String; = "MPEG_TS_SD_NA_T"

.field public static final MPEG_TS_SD_NA_XAC3:Ljava/lang/String; = "MPEG_TS_SD_NA_XAC3"

.field public static final MPEG_TS_SD_NA_XAC3_ISO:Ljava/lang/String; = "MPEG_TS_SD_NA_XAC3_ISO"

.field public static final MPEG_TS_SD_NA_XAC3_T:Ljava/lang/String; = "MPEG_TS_SD_NA_XAC3_T"

.field public static final PNG_LRG:Ljava/lang/String; = "PNG_LRG"

.field public static final PNG_LRG_ICO:Ljava/lang/String; = "PNG_LRG_ICO"

.field public static final PNG_SM_ICO:Ljava/lang/String; = "PNG_SM_ICO"

.field public static final PNG_TN:Ljava/lang/String; = "PNG_TN"

.field public static final POSTFIX_3GP:Ljava/lang/String; = ".3gp"

.field public static final POSTFIX_3GPP:Ljava/lang/String; = ".3gpp"

.field public static final POSTFIX_ADTS:Ljava/lang/String; = ".adts"

.field public static final POSTFIX_ASF:Ljava/lang/String; = ".asf"

.field public static final POSTFIX_AVI:Ljava/lang/String; = ".avi"

.field public static final POSTFIX_JPEG:Ljava/lang/String; = ".jpeg"

.field public static final POSTFIX_LPCM:Ljava/lang/String; = ".lpcm"

.field public static final POSTFIX_MP3:Ljava/lang/String; = ".mp3"

.field public static final POSTFIX_MP4:Ljava/lang/String; = ".mp4"

.field public static final POSTFIX_MPEG:Ljava/lang/String; = ".mpeg"

.field public static final POSTFIX_PNG:Ljava/lang/String; = ".png"

.field public static final POSTFIX_RAW:Ljava/lang/String; = ".raw"

.field public static final POSTFIX_WMA:Ljava/lang/String; = ".wma"

.field public static final POSTFIX_WMV:Ljava/lang/String; = ".wmv"

.field public static PROTOCOL_TYPE_MICRO:I = 0x0

.field public static PROTOCOL_TYPE_ORIGIN:I = 0x0

.field public static PROTOCOL_TYPE_SMALL:I = 0x0

.field public static final WMABASE:Ljava/lang/String; = "WMABASE"

.field public static final WMAFULL:Ljava/lang/String; = "WMAFULL"

.field public static final WMAPRO:Ljava/lang/String; = "WMAPRO"

.field public static final WMVHIGH_FULL:Ljava/lang/String; = "WMVHIGH_FULL"

.field public static final WMVHIGH_PRO:Ljava/lang/String; = "WMVHIGH_PRO"

.field public static final WMVHM_BASE:Ljava/lang/String; = "WMVHM_BASE"

.field public static final WMVMED_BASE:Ljava/lang/String; = "WMVMED_BASE"

.field public static final WMVMED_FULL:Ljava/lang/String; = "WMVMED_FULL"

.field public static final WMVMED_PRO:Ljava/lang/String; = "WMVMED_PRO"

.field public static final WMVSPLL_BASE:Ljava/lang/String; = "WMVSPLL_BASE"

.field public static final WMVSPML_BASE:Ljava/lang/String; = "WMVSPML_BASE"

.field public static final WMVSPML_MP3:Ljava/lang/String; = "WMVSPML_MP3"

.field static parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;


# instance fields
.field postfixDictionary:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field protocols:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE()[I
    .locals 3

    .prologue
    .line 11
    sget-object v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->$SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->values()[Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ADTS:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ASF:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->AVI:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->GP3:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->GPP3:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->JPEG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->LPCM:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MP3:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MP4:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MPEG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->PNG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_b
    :try_start_b
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->RAW:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_c
    :try_start_c
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->WMA:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_d
    :try_start_d
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->WMV:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_e
    sput-object v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->$SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_e

    :catch_1
    move-exception v1

    goto :goto_d

    :catch_2
    move-exception v1

    goto :goto_c

    :catch_3
    move-exception v1

    goto :goto_b

    :catch_4
    move-exception v1

    goto :goto_a

    :catch_5
    move-exception v1

    goto :goto_9

    :catch_6
    move-exception v1

    goto :goto_8

    :catch_7
    move-exception v1

    goto :goto_7

    :catch_8
    move-exception v1

    goto :goto_6

    :catch_9
    move-exception v1

    goto :goto_5

    :catch_a
    move-exception v1

    goto :goto_4

    :catch_b
    move-exception v1

    goto/16 :goto_3

    :catch_c
    move-exception v1

    goto/16 :goto_2

    :catch_d
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 323
    new-instance v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;-><init>()V

    sput-object v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    .line 526
    const/4 v0, 0x0

    sput v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    .line 527
    const/4 v0, 0x1

    sput v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_SMALL:I

    .line 528
    const/4 v0, 0x2

    sput v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_MICRO:I

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    .line 530
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 331
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/jpeg"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->JPEG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/png"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->PNG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/vnd.dolby.dd-raw"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->RAW:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/vnd.dlna.adts"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ADTS:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-ms-asf"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ASF:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/3gpp"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->GPP3:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/3gpp"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->GPP3:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/L16"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->LPCM:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/mp4"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MP4:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mp4"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MP4:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/mpeg"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MP3:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mpeg"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MPEG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/vnd.dlna.mpeg-tts"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->MPEG:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/x-ms-wma"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->WMA:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-ms-wmv"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->WMV:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/avi"

    sget-object v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->AVI:Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_SM"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_MED"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_LRG"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mpeg:DLNA.ORG_PN=MP3"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/3gpp:DLNA.ORG_PN=AAC_ISO_320"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mp4:DLNA.ORG_PN=AAC_ISO_320"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/mp4:DLNA.ORG_PN=AVC_MP4_BL_CIF15_AAC_520"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/mp4:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/3gpp:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/avi:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mp4:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mpeg:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/png:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    return-void
.end method

.method private build1stField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    const-string v0, "http-get"

    return-object v0
.end method

.method private build3rdField(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 457
    return-object p1
.end method

.method private build4thField(Ljava/lang/String;III)Ljava/lang/String;
    .locals 6
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "proto_type"    # I

    .prologue
    const/16 v5, 0x1000

    const/16 v4, 0xa0

    .line 460
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 461
    .local v1, "upnpClass":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    .line 463
    .local v0, "type":Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;
    if-nez v0, :cond_0

    .line 464
    const-string v2, ""

    .line 519
    :goto_0
    return-object v2

    .line 466
    :cond_0
    const-string v2, "object.item.videoItem.movie"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 467
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->$SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 477
    :pswitch_0
    const-string v2, ""

    goto :goto_0

    .line 469
    :pswitch_1
    const-string v2, "AVC_MP4_BL_CIF15_AAC_520"

    goto :goto_0

    .line 471
    :pswitch_2
    const-string v2, "AVC_3GPP_BL_QCIF15_AMR"

    goto :goto_0

    .line 473
    :pswitch_3
    const-string v2, "WMVMED_FULL"

    goto :goto_0

    .line 475
    :pswitch_4
    const-string v2, "MPEG4_P2_TS_SP_AAC"

    goto :goto_0

    .line 479
    :cond_1
    const-string v2, "object.item.audioItem.musicTrack"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 480
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->$SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 492
    :pswitch_5
    const-string v2, ""

    goto :goto_0

    .line 482
    :pswitch_6
    const-string v2, "MP3"

    goto :goto_0

    .line 484
    :pswitch_7
    const-string v2, "AAC_ISO_320"

    goto :goto_0

    .line 486
    :pswitch_8
    const-string v2, "LPCM"

    goto :goto_0

    .line 488
    :pswitch_9
    const-string v2, "WMABASE"

    goto :goto_0

    .line 490
    :pswitch_a
    const-string v2, "AAC_ADTS_320"

    goto :goto_0

    .line 495
    :cond_2
    const-string v2, "object.item.imageItem.photo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 496
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->$SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    .line 517
    :cond_3
    const-string v2, ""

    goto :goto_0

    .line 498
    :pswitch_b
    if-eqz p2, :cond_4

    if-nez p3, :cond_5

    .line 499
    :cond_4
    const-string v2, ""

    goto :goto_0

    .line 500
    :cond_5
    if-lt v4, p2, :cond_7

    if-lt v4, p3, :cond_7

    .line 501
    sget v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_MICRO:I

    if-ne p4, v2, :cond_6

    const-string v2, "JPEG_TN"

    goto :goto_0

    :cond_6
    const-string v2, "JPEG_SM"

    goto :goto_0

    .line 502
    :cond_7
    const/16 v2, 0x280

    if-lt v2, p2, :cond_8

    const/16 v2, 0x1e0

    if-lt v2, p3, :cond_8

    .line 503
    const-string v2, "JPEG_SM"

    goto/16 :goto_0

    .line 504
    :cond_8
    const/16 v2, 0x400

    if-lt v2, p2, :cond_9

    const/16 v2, 0x300

    if-lt v2, p3, :cond_9

    .line 505
    const-string v2, "JPEG_MED"

    goto/16 :goto_0

    .line 506
    :cond_9
    if-lt v5, p2, :cond_3

    if-lt v5, p3, :cond_3

    .line 507
    const-string v2, "JPEG_LRG"

    goto/16 :goto_0

    .line 510
    :pswitch_c
    if-eqz p2, :cond_a

    if-nez p3, :cond_b

    .line 511
    :cond_a
    const-string v2, ""

    goto/16 :goto_0

    .line 512
    :cond_b
    if-lez p2, :cond_d

    if-lt v4, p2, :cond_d

    if-lez p3, :cond_d

    if-lt v4, p3, :cond_d

    .line 513
    sget v2, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_MICRO:I

    if-ne p4, v2, :cond_c

    const-string v2, "PNG_TN"

    goto/16 :goto_0

    :cond_c
    const-string v2, "PNG_LRG"

    goto/16 :goto_0

    .line 515
    :cond_d
    const-string v2, "PNG_LRG"

    goto/16 :goto_0

    .line 519
    :cond_e
    const-string v2, ""

    goto/16 :goto_0

    .line 467
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 480
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_8
        :pswitch_5
        :pswitch_a
        :pswitch_9
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 496
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private bulid2ndField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 454
    const-string v0, "*"

    return-object v0
.end method

.method public static declared-synchronized getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;
    .locals 2

    .prologue
    .line 325
    const-class v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->parser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 618
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->get4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 619
    const-string v0, "jpeg"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "png"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 626
    :goto_0
    return-object v0

    .line 621
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "DLNA.ORG_OP=01"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 623
    :cond_2
    sget v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    if-ne p2, v0, :cond_3

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ";DLNA.ORG_OP=01;DLNA.ORG_CI=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 626
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ";DLNA.ORG_OP=01;DLNA.ORG_CI=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public buildProtocol(Ljava/lang/String;III)Ljava/lang/String;
    .locals 5
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "type"    # I

    .prologue
    .line 532
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 533
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->build1stField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 534
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->bulid2ndField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 537
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->build3rdField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->build4thField(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    .line 540
    .local v0, "feature":Ljava/lang/String;
    const-string v1, ""

    .line 542
    .local v1, "protocol":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 543
    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 545
    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 546
    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, p4}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 549
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DLNA.ORG_PN="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->build4thField(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 551
    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 552
    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public convertToValidateProtocol(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 558
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 559
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->build1stField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->bulid2ndField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->get4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "feature":Ljava/lang/String;
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 568
    .local v1, "pos":I
    if-lez v1, :cond_0

    .line 569
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 570
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    invoke-virtual {p0, v3, v4}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public convertToValidateProtocolWithout4thField(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 576
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->build1stField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->bulid2ndField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 582
    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public get4thField(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 430
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ":"

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    .local v0, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 433
    :cond_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 434
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 435
    :cond_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 436
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 437
    :cond_2
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 438
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 439
    :goto_0
    return-object v1

    :cond_3
    const-string v1, ""

    goto :goto_0
.end method

.method public getCurrentProtocolInfo()Ljava/lang/String;
    .locals 4

    .prologue
    .line 588
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 589
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 593
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 594
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 589
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 590
    .local v0, "protocol":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 444
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ":"

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    .local v0, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 446
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 447
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getPostfixByMimetype(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 407
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 408
    :cond_0
    const-string v1, ""

    .line 413
    :goto_0
    return-object v1

    .line 410
    :cond_1
    iget-object v1, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    .line 411
    .local v0, "type":Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;
    if-nez v0, :cond_2

    .line 412
    const-string v1, ".tmp"

    goto :goto_0

    .line 413
    :cond_2
    iget-object v1, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->postfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    invoke-virtual {p0, v1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getPostfixByPostfixEnum(Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method getPostfixByPostfixEnum(Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;

    .prologue
    .line 369
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->$SWITCH_TABLE$com$samsung$upnp$media$server$object$DLNAProfileParser$POSTFIX_TYPE()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser$POSTFIX_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 399
    const-string v0, ""

    :goto_0
    return-object v0

    .line 371
    :pswitch_0
    const-string v0, ".adts"

    goto :goto_0

    .line 373
    :pswitch_1
    const-string v0, ".asf"

    goto :goto_0

    .line 375
    :pswitch_2
    const-string v0, ".3gpp"

    goto :goto_0

    .line 377
    :pswitch_3
    const-string v0, ".3gp"

    goto :goto_0

    .line 379
    :pswitch_4
    const-string v0, ".jpeg"

    goto :goto_0

    .line 381
    :pswitch_5
    const-string v0, ".lpcm"

    goto :goto_0

    .line 383
    :pswitch_6
    const-string v0, ".mp3"

    goto :goto_0

    .line 385
    :pswitch_7
    const-string v0, ".mp4"

    goto :goto_0

    .line 387
    :pswitch_8
    const-string v0, ".mpeg"

    goto :goto_0

    .line 389
    :pswitch_9
    const-string v0, ".png"

    goto :goto_0

    .line 391
    :pswitch_a
    const-string v0, ".raw"

    goto :goto_0

    .line 393
    :pswitch_b
    const-string v0, ".wma"

    goto :goto_0

    .line 395
    :pswitch_c
    const-string v0, ".wmv"

    goto :goto_0

    .line 397
    :pswitch_d
    const-string v0, ".avi"

    goto :goto_0

    .line 369
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_9
        :pswitch_a
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_b
        :pswitch_1
        :pswitch_c
        :pswitch_6
        :pswitch_d
    .end packed-switch
.end method

.method public getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 417
    if-nez p1, :cond_0

    .line 418
    const-string v0, "object.item"

    .line 425
    :goto_0
    return-object v0

    .line 419
    :cond_0
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    const-string v0, "object.item.audioItem.musicTrack"

    goto :goto_0

    .line 421
    :cond_1
    const-string v0, "video"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 422
    const-string v0, "object.item.videoItem.movie"

    goto :goto_0

    .line 423
    :cond_2
    const-string v0, "image"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 424
    const-string v0, "object.item.imageItem.photo"

    goto :goto_0

    .line 425
    :cond_3
    const-string v0, "object.item"

    goto :goto_0
.end method

.method public isSupportedProtocol(Ljava/lang/String;)Z
    .locals 3
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 602
    iget-object v1, p0, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->protocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 606
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 602
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 603
    .local v0, "prot":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 604
    const/4 v1, 0x1

    goto :goto_0
.end method
