.class public Lcom/samsung/upnp/media/server/UPnP;
.super Ljava/lang/Object;
.source "UPnP.java"


# static fields
.field public static final ALBUM:Ljava/lang/String; = "upnp:album"

.field public static final ALBUMART:Ljava/lang/String; = "upnp:albumArtURI"

.field public static final ARTIST:Ljava/lang/String; = "upnp:artist"

.field public static final CLASS:Ljava/lang/String; = "upnp:class"

.field public static final OBJECT_CONTAINER:Ljava/lang/String; = "object.container"

.field public static final OBJECT_ITEM:Ljava/lang/String; = "object.item"

.field public static final OBJECT_ITEM_AUDIOITEM:Ljava/lang/String; = "object.item.audioItem"

.field public static final OBJECT_ITEM_AUDIOITEM_MUSICTRACK:Ljava/lang/String; = "object.item.audioItem.musicTrack"

.field public static final OBJECT_ITEM_IMAGEITEM:Ljava/lang/String; = "object.item.imageItem"

.field public static final OBJECT_ITEM_IMAGEITEM_PHOTO:Ljava/lang/String; = "object.item.imageItem.photo"

.field public static final OBJECT_ITEM_VIDEOITEM:Ljava/lang/String; = "object.item.videoItem"

.field public static final OBJECT_ITEM_VIDEOITEM_MOVIE:Ljava/lang/String; = "object.item.videoItem.movie"

.field public static final STORAGE_MEDIUM:Ljava/lang/String; = "upnp:storageMedium"

.field public static final STORAGE_USED:Ljava/lang/String; = "upnp:storageUsed"

.field public static final WRITE_STATUS:Ljava/lang/String; = "upnp:writeStatus"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
