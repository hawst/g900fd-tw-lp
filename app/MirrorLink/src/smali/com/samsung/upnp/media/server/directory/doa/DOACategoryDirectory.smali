.class public Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;
.super Lcom/samsung/upnp/media/server/Directory;
.source "DOACategoryDirectory.java"


# static fields
.field public static final AUDIO:I = 0x1

.field public static final IMAGE:I = 0x0

.field private static final NAME:Ljava/lang/String; = "DOA"

.field public static final VIDEO:I = 0x2

.field private static doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;


# instance fields
.field private cduListener:Lcom/samsung/api/ContentsDirectoryUpdatedListener;

.field private prepared:Z

.field private type:I

.field updateHandler:Landroid/os/Handler;

.field updateThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/Directory;-><init>(Ljava/lang/String;)V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->type:I

    .line 31
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Contents Updater"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateThread:Landroid/os/HandlerThread;

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateHandler:Landroid/os/Handler;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->prepared:Z

    .line 40
    invoke-static {}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getInstance()Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    move-result-object v0

    sput-object v0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    .line 41
    iput p2, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->type:I

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->setRestricted(I)V

    .line 44
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->startThread()V

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)[Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getRecordedItemNodes()[Lcom/samsung/upnp/media/server/object/item/ItemNode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getCurrentRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Lcom/samsung/api/ContentsDirectoryUpdatedListener;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->cduListener:Lcom/samsung/api/ContentsDirectoryUpdatedListener;

    return-object v0
.end method

.method private getCurrentRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .locals 2

    .prologue
    .line 182
    const/4 v0, 0x0

    .line 183
    .local v0, "recInfos":[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    sget-object v1, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->open()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    iget v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->type:I

    packed-switch v1, :pswitch_data_0

    .line 195
    :goto_0
    sget-object v1, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->close()V

    .line 198
    :cond_0
    return-object v0

    .line 186
    :pswitch_0
    sget-object v1, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getVideoRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v0

    .line 187
    goto :goto_0

    .line 189
    :pswitch_1
    sget-object v1, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getAudioRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v0

    .line 190
    goto :goto_0

    .line 192
    :pswitch_2
    sget-object v1, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->doaDatabase:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getImageRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v0

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getRecordedItemNodes()[Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .locals 4

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getNContentNodes()I

    move-result v1

    .line 173
    .local v1, "nContents":I
    new-array v2, v1, [Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .line 174
    .local v2, "recNode":[Lcom/samsung/upnp/media/server/object/item/ItemNode;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 178
    return-object v2

    .line 175
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v3

    instance-of v3, v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    if-eqz v3, :cond_1

    .line 176
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v3

    check-cast v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    aput-object v3, v2, v0

    .line 174
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private startThread()V
    .locals 2

    .prologue
    .line 48
    iget-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 49
    iget-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 50
    .local v0, "looper":Landroid/os/Looper;
    if-nez v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateHandler:Landroid/os/Handler;

    goto :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 64
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 65
    return-void
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->type:I

    return v0
.end method

.method public setContentsDirectoryUpdatedListener(Lcom/samsung/api/ContentsDirectoryUpdatedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/api/ContentsDirectoryUpdatedListener;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->cduListener:Lcom/samsung/api/ContentsDirectoryUpdatedListener;

    .line 59
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->updateHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;

    invoke-direct {v1, p0}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;-><init>(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 169
    return-void
.end method
