.class Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;
.super Ljava/lang/Object;
.source "DOACategoryDirectory.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->update()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;


# direct methods
.method constructor <init>(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 107
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    # invokes: Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getRecordedItemNodes()[Lcom/samsung/upnp/media/server/object/item/ItemNode;
    invoke-static {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->access$0(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)[Lcom/samsung/upnp/media/server/object/item/ItemNode;

    move-result-object v1

    .line 108
    .local v1, "currRecNode":[Lcom/samsung/upnp/media/server/object/item/ItemNode;
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    # invokes: Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getCurrentRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    invoke-static {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->access$1(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v8

    .line 110
    .local v8, "recInfoOnDB":[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    if-nez v8, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    array-length v6, v1

    .line 113
    .local v6, "nCurrRecNodes":I
    array-length v7, v8

    .line 114
    .local v7, "nRecInfoOnDB":I
    const/4 v10, 0x0

    .line 117
    .local v10, "updateFlag":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v6, :cond_3

    .line 136
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    if-lt v4, v7, :cond_8

    .line 156
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    invoke-virtual {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getNContentNodes()I

    move-result v5

    .line 157
    .local v5, "nContents":I
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    invoke-virtual {v11, v5}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->setChildCount(I)V

    .line 159
    if-eqz v10, :cond_2

    .line 160
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    invoke-virtual {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 161
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_2

    .line 162
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->updateSystemUpdateID()V

    .line 164
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    :cond_2
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    # getter for: Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->cduListener:Lcom/samsung/api/ContentsDirectoryUpdatedListener;
    invoke-static {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->access$2(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Lcom/samsung/api/ContentsDirectoryUpdatedListener;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 165
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    # getter for: Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->cduListener:Lcom/samsung/api/ContentsDirectoryUpdatedListener;
    invoke-static {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->access$2(Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;)Lcom/samsung/api/ContentsDirectoryUpdatedListener;

    move-result-object v11

    invoke-interface {v11, v10, v5}, Lcom/samsung/api/ContentsDirectoryUpdatedListener;->updatedDirectory(ZI)V

    goto :goto_0

    .line 118
    .end local v4    # "j":I
    .end local v5    # "nContents":I
    :cond_3
    const/4 v2, 0x0

    .line 119
    .local v2, "hasRecItem":Z
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_3
    if-lt v4, v7, :cond_5

    .line 127
    :goto_4
    if-eqz v2, :cond_7

    .line 117
    :cond_4
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 120
    :cond_5
    aget-object v11, v1, v3

    if-eqz v11, :cond_6

    .line 121
    aget-object v11, v1, v3

    aget-object v12, v8, v4

    invoke-virtual {v11, v12}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 122
    const/4 v2, 0x1

    .line 123
    goto :goto_4

    .line 119
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 129
    :cond_7
    aget-object v11, v1, v3

    instance-of v11, v11, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;

    if-nez v11, :cond_4

    .line 131
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    aget-object v12, v1, v3

    invoke-virtual {v11, v12}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->removeContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)Z

    .line 132
    const/4 v10, 0x1

    goto :goto_5

    .line 137
    .end local v2    # "hasRecItem":Z
    :cond_8
    const/4 v2, 0x0

    .line 138
    .restart local v2    # "hasRecItem":Z
    const/4 v3, 0x0

    :goto_6
    if-lt v3, v6, :cond_9

    .line 146
    :goto_7
    if-eqz v2, :cond_b

    .line 136
    :goto_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 139
    :cond_9
    aget-object v11, v1, v3

    if-eqz v11, :cond_a

    .line 140
    aget-object v11, v1, v3

    aget-object v12, v8, v4

    invoke-virtual {v11, v12}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 141
    const/4 v2, 0x1

    .line 142
    goto :goto_7

    .line 138
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 150
    :cond_b
    new-instance v9, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;

    aget-object v11, v8, v4

    invoke-direct {v9, v11}, Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;-><init>(Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;)V

    .line 151
    .local v9, "recItem":Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;
    iget-object v11, p0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory$1;->this$0:Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    invoke-virtual {v11, v9}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 152
    aget-object v11, v8, v4

    invoke-static {v9, v11}, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;->buildDOAResourceProperty(Lcom/samsung/upnp/media/server/object/item/sat/DOARecordedItemNode;Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;)V

    .line 153
    const/4 v10, 0x1

    goto :goto_8
.end method
