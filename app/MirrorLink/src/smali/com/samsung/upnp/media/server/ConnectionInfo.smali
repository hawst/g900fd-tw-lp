.class public Lcom/samsung/upnp/media/server/ConnectionInfo;
.super Ljava/lang/Object;
.source "ConnectionInfo.java"


# static fields
.field public static final INPUT:Ljava/lang/String; = "Input"

.field public static final OK:Ljava/lang/String; = "OK"

.field public static final OUTPUT:Ljava/lang/String; = "Output"

.field public static final UNKNOWN:Ljava/lang/String; = "Unknown"


# instance fields
.field private direction:Ljava/lang/String;

.field private id:I

.field private peerConnectionID:I

.field private peerConnectionManager:Ljava/lang/String;

.field private rcsId:I

.field private status:Ljava/lang/String;

.field private transId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAVTransportID()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->transId:I

    return v0
.end method

.method public getDirection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->direction:Ljava/lang/String;

    return-object v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->id:I

    return v0
.end method

.method public getPeerConnectionID()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->peerConnectionID:I

    return v0
.end method

.method public getPeerConnectionManager()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->peerConnectionManager:Ljava/lang/String;

    return-object v0
.end method

.method public getRcsID()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->rcsId:I

    return v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ConnectionInfo;->status:Ljava/lang/String;

    return-object v0
.end method
