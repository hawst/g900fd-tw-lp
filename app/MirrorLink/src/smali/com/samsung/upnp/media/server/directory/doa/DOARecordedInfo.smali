.class public Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
.super Ljava/lang/Object;
.source "DOARecordedInfo.java"


# instance fields
.field private TYPE:Lcom/samsung/upnp/media/server/directory/doa/FileType;

.field private album:Ljava/lang/String;

.field private albumArtUri:Ljava/lang/String;

.field private artist:Ljava/lang/String;

.field private audioAlbum_id:Ljava/lang/String;

.field private category:Ljava/lang/String;

.field private creator:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private durationTime:Ljava/lang/String;

.field private endTime:J

.field private height:I

.field private imageDB_id:I

.field private mimeType:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private recGroup:Ljava/lang/String;

.field private recordID:I

.field private resolution:Ljava/lang/String;

.field private size:J

.field private startTime:J

.field private subTitle:Ljava/lang/String;

.field private thumbnailHeight:J

.field private thumbnailSize:I

.field private thumbnailUri:Landroid/net/Uri;

.field private thumbnailWidth:J

.field private title:Ljava/lang/String;

.field private uri:Landroid/net/Uri;

.field private width:I


# direct methods
.method public constructor <init>(Lcom/samsung/upnp/media/server/directory/doa/FileType;)V
    .locals 8
    .param p1, "type"    # Lcom/samsung/upnp/media/server/directory/doa/FileType;

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, -0x1

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->mimeType:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->durationTime:Ljava/lang/String;

    .line 46
    iput v2, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->width:I

    .line 47
    iput v2, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->height:I

    .line 48
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->resolution:Ljava/lang/String;

    .line 49
    iput-wide v3, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->size:J

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->path:Ljava/lang/String;

    .line 52
    iput v5, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->imageDB_id:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->audioAlbum_id:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->date:Ljava/lang/String;

    .line 133
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->category:Ljava/lang/String;

    .line 161
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->description:Ljava/lang/String;

    .line 175
    iput-wide v3, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->endTime:J

    .line 189
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->recGroup:Ljava/lang/String;

    .line 203
    iput v5, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->recordID:I

    .line 217
    iput-wide v3, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->startTime:J

    .line 231
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->title:Ljava/lang/String;

    .line 245
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->creator:Ljava/lang/String;

    .line 260
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->subTitle:Ljava/lang/String;

    .line 273
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->artist:Ljava/lang/String;

    .line 285
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->album:Ljava/lang/String;

    .line 297
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->albumArtUri:Ljava/lang/String;

    .line 314
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailUri:Landroid/net/Uri;

    .line 315
    iput-wide v6, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailWidth:J

    .line 316
    iput-wide v6, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailHeight:J

    .line 317
    iput v2, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailSize:I

    .line 39
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->TYPE:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    .line 40
    return-void
.end method


# virtual methods
.method public getAlbumArtUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->albumArtUri:Ljava/lang/String;

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioDB_ID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->audioAlbum_id:Ljava/lang/String;

    return-object v0
.end method

.method public getCreator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->creator:Ljava/lang/String;

    return-object v0
.end method

.method public getDataPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getDurationTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->durationTime:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->height:I

    return v0
.end method

.method public getImageDB_ID()I
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->imageDB_id:I

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->size:J

    return-wide v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->subTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTYPE()Lcom/samsung/upnp/media/server/directory/doa/FileType;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->TYPE:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    return-object v0
.end method

.method public getThumbnailsHeight()J
    .locals 2

    .prologue
    .line 344
    iget-wide v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailHeight:J

    return-wide v0
.end method

.method public getThumbnailsSize()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailSize:I

    return v0
.end method

.method public getThumbnailsUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getThumbnailsWidth()J
    .locals 2

    .prologue
    .line 340
    iget-wide v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->thumbnailWidth:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->width:I

    return v0
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 1
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 291
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->album:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public setAlbumArtUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumArtUri"    # Ljava/lang/String;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->albumArtUri:Ljava/lang/String;

    .line 305
    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "artist"    # Ljava/lang/String;

    .prologue
    .line 279
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->artist:Ljava/lang/String;

    .line 280
    return-void
.end method

.method public setAudioAlbum_ID(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 360
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->audioAlbum_id:Ljava/lang/String;

    .line 361
    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->category:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setCreator(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 248
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->creator:Ljava/lang/String;

    .line 249
    return-void
.end method

.method public setDataPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->path:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setDurationTime(Ljava/lang/String;)V
    .locals 1
    .param p1, "durationTime"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->durationTime:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setImageDB_ID(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 356
    iput p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->imageDB_id:I

    .line 357
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->mimeType:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 1
    .param p1, "resolution"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->resolution:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1, "size"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->size:J

    .line 62
    return-void
.end method

.method public setSubTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 263
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->subTitle:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 234
    invoke-static {p1}, Lcom/samsung/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->title:Ljava/lang/String;

    .line 235
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->uri:Landroid/net/Uri;

    .line 122
    return-void
.end method
