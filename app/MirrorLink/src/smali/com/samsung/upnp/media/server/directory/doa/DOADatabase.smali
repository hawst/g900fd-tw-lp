.class public Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;
.super Ljava/lang/Object;
.source "DOADatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;,
        Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;
    }
.end annotation


# static fields
.field private static final DB_NAME:Ljava/lang/String; = "sat_DMS"

.field private static instance:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;


# instance fields
.field private androidContext:Landroid/content/Context;

.field private audioNumOnDB:I

.field private imageNumOnDB:I

.field isCache:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;

.field private videoNumOnDB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->instance:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    .line 110
    new-instance v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;

    invoke-direct {v0, p0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;-><init>(Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;)V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->isCache:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;

    .line 80
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    return-object v0
.end method

.method private arrayListToRecordedInfo(Ljava/util/concurrent/CopyOnWriteArrayList;)[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;",
            ">;)[",
            "Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;"
        }
    .end annotation

    .prologue
    .line 873
    .local p1, "recVec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    .line 874
    .local v2, "recCnt":I
    new-array v1, v2, [Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    .line 875
    .local v1, "recArray":[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 879
    return-object v1

    .line 876
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    aput-object v3, v1, v0

    .line 875
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static formatTime(I)Ljava/lang/String;
    .locals 5
    .param p0, "time"    # I

    .prologue
    .line 908
    if-nez p0, :cond_0

    const-string v3, "00:00:00"

    .line 912
    :goto_0
    return-object v3

    .line 909
    :cond_0
    rem-int/lit8 v3, p0, 0x3c

    int-to-long v3, v3

    invoke-static {v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->toDateString(J)Ljava/lang/String;

    move-result-object v2

    .line 910
    .local v2, "seconds":Ljava/lang/String;
    rem-int/lit16 v3, p0, 0xe10

    div-int/lit8 v3, v3, 0x3c

    int-to-long v3, v3

    invoke-static {v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->toDateString(J)Ljava/lang/String;

    move-result-object v1

    .line 911
    .local v1, "minutes":Ljava/lang/String;
    div-int/lit16 v3, p0, 0xe10

    int-to-long v3, v3

    invoke-static {v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->toDateString(J)Ljava/lang/String;

    move-result-object v0

    .line 912
    .local v0, "hours":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getAudioMediFromDB(Ljava/util/concurrent/CopyOnWriteArrayList;)V
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351
    .local p1, "vec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 352
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 353
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 352
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 354
    .local v29, "tempCur":Landroid/database/Cursor;
    new-instance v16, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;-><init>(Landroid/database/Cursor;)V

    .line 355
    .local v16, "cur":Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;
    if-nez v16, :cond_0

    .line 454
    :goto_0
    return-void

    .line 357
    :cond_0
    const-string v3, "mime_type"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 358
    .local v25, "mimeIndex":I
    const-string v3, "_data"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 359
    .local v18, "dataIndex":I
    const-string v3, "title"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 360
    .local v31, "titleIndex":I
    const-string v3, "_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 361
    .local v23, "idIndex":I
    const-string v3, "duration"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 362
    .local v21, "durationIndex":I
    const-string v3, "_size"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 363
    .local v28, "sizeIndex":I
    const-string v3, "artist"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 364
    .local v13, "artistIndex":I
    const-string v3, "composer"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 365
    .local v14, "composerIndex":I
    const-string v3, "album"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 366
    .local v11, "albumIndex":I
    const-string v3, "album_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 368
    .local v10, "albumIdIndex":I
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getCount()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->setAudioNumOnDB(I)V

    .line 370
    :cond_1
    :goto_1
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 453
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->close()V

    goto :goto_0

    .line 371
    :cond_2
    new-instance v26, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    sget-object v3, Lcom/samsung/upnp/media/server/directory/doa/FileType;->Audio:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;-><init>(Lcom/samsung/upnp/media/server/directory/doa/FileType;)V

    .line 373
    .local v26, "satInfo":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 374
    .local v24, "mime":Ljava/lang/String;
    if-eqz v24, :cond_1

    const-string v3, "pyv"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "dcf"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "imelody"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 375
    const-string v3, "dm"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "pvv"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "midi"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 376
    const-string v3, "amr"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ogg"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "application"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 378
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setMimeType(Ljava/lang/String;)V

    .line 380
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 381
    .local v17, "data":Ljava/lang/String;
    if-eqz v17, :cond_1

    const-string v3, "pyv"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "dcf"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "imelody"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 382
    const-string v3, "dm"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "pvv"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "midi"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 383
    const-string v3, "amr"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ogg"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "application"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 385
    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDataPath(Ljava/lang/String;)V

    .line 387
    move-object/from16 v0, v16

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 388
    .local v30, "title":Ljava/lang/String;
    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setTitle(Ljava/lang/String;)V

    .line 389
    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSubTitle(Ljava/lang/String;)V

    .line 394
    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getInt(I)I

    move-result v19

    .line 395
    .local v19, "db_id":I
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move/from16 v0, v19

    int-to-long v4, v0

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v22

    .line 396
    .local v22, "id":Landroid/net/Uri;
    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setUri(Landroid/net/Uri;)V

    .line 398
    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 399
    .local v20, "duration":Ljava/lang/String;
    if-eqz v20, :cond_3

    const-string v3, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 400
    :cond_3
    const-string v3, "00:00:00"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDurationTime(Ljava/lang/String;)V

    .line 404
    :goto_2
    move-object/from16 v0, v16

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getInt(I)I

    move-result v27

    .line 405
    .local v27, "size":I
    move/from16 v0, v27

    int-to-long v3, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSize(J)V

    .line 407
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 408
    .local v12, "artist":Ljava/lang/String;
    if-eqz v12, :cond_4

    const-string v3, "<unknown>"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 409
    :cond_4
    const-string v12, "unknown"

    .line 411
    :cond_5
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setArtist(Ljava/lang/String;)V

    .line 415
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 416
    .local v15, "creator":Ljava/lang/String;
    if-eqz v15, :cond_6

    const-string v3, "<unknown>"

    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 417
    :cond_6
    const-string v15, "unknown"

    .line 419
    :cond_7
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setCreator(Ljava/lang/String;)V

    .line 421
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 422
    .local v8, "album":Ljava/lang/String;
    if-eqz v8, :cond_8

    const-string v3, "<unknown>"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 423
    :cond_8
    const-string v8, "unknown"

    .line 425
    :cond_9
    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setAlbum(Ljava/lang/String;)V

    .line 427
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 428
    .local v9, "albumId":Ljava/lang/String;
    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setAudioAlbum_ID(Ljava/lang/String;)V

    .line 451
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 402
    .end local v8    # "album":Ljava/lang/String;
    .end local v9    # "albumId":Ljava/lang/String;
    .end local v12    # "artist":Ljava/lang/String;
    .end local v15    # "creator":Ljava/lang/String;
    .end local v27    # "size":I
    :cond_a
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    div-int/lit16 v3, v3, 0x3e8

    invoke-static {v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->formatTime(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDurationTime(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getBitmap(J)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "id"    # J

    .prologue
    const/16 v5, 0xa0

    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 138
    .local v1, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x3

    .line 141
    .local v2, "sampleSize":I
    :cond_0
    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 142
    iget-object v3, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    if-eqz v3, :cond_1

    .line 144
    iget-object v3, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x1

    .line 143
    invoke-static {v3, p1, p2, v4, v1}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 154
    :cond_1
    if-nez v0, :cond_3

    .line 155
    const/4 v0, 0x0

    .line 170
    :cond_2
    :goto_0
    return-object v0

    .line 157
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-gt v3, v5, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v3, v5, :cond_2

    .line 160
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 162
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 166
    const/16 v3, 0xa

    if-le v2, v3, :cond_0

    goto :goto_0
.end method

.method private getImageMediFromDB(Ljava/util/concurrent/CopyOnWriteArrayList;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 457
    .local p1, "vec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 458
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 459
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 458
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 461
    .local v21, "tempCur":Landroid/database/Cursor;
    new-instance v8, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;-><init>(Landroid/database/Cursor;)V

    .line 462
    .local v8, "cur":Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;
    if-nez v8, :cond_0

    .line 573
    :goto_0
    return-void

    .line 465
    :cond_0
    const-string v3, "_data"

    invoke-virtual {v8, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 466
    .local v10, "dataIndex":I
    const-string v3, "mime_type"

    invoke-virtual {v8, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 467
    .local v17, "mimeIndex":I
    const-string v3, "title"

    invoke-virtual {v8, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 468
    .local v23, "titleIndex":I
    const-string v3, "_id"

    invoke-virtual {v8, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 469
    .local v15, "idIndex":I
    const-string v3, "_size"

    invoke-virtual {v8, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 471
    .local v20, "sizeIndex":I
    invoke-virtual {v8}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getCount()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->setImageNumOnDB(I)V

    .line 474
    :cond_1
    :goto_1
    invoke-virtual {v8}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 572
    invoke-virtual {v8}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->close()V

    goto :goto_0

    .line 475
    :cond_2
    new-instance v18, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    sget-object v3, Lcom/samsung/upnp/media/server/directory/doa/FileType;->Image:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;-><init>(Lcom/samsung/upnp/media/server/directory/doa/FileType;)V

    .line 477
    .local v18, "satInfo":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    invoke-virtual {v8, v10}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 481
    .local v9, "data":Ljava/lang/String;
    if-eqz v9, :cond_1

    const-string v3, "application"

    invoke-virtual {v9, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "gif"

    invoke-virtual {v9, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "bmp"

    invoke-virtual {v9, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 483
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDataPath(Ljava/lang/String;)V

    .line 485
    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 489
    .local v16, "mime":Ljava/lang/String;
    if-eqz v16, :cond_1

    const-string v3, "application"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "gif"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "bmp"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 491
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setMimeType(Ljava/lang/String;)V

    .line 493
    move/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 494
    .local v22, "title":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setTitle(Ljava/lang/String;)V

    .line 495
    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSubTitle(Ljava/lang/String;)V

    .line 496
    const-string v3, "category"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setCategory(Ljava/lang/String;)V

    .line 498
    invoke-virtual {v8, v15}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getInt(I)I

    move-result v11

    .line 499
    .local v11, "db_id":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setImageDB_ID(I)V

    .line 504
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, v11

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v14

    .line 505
    .local v14, "id":Landroid/net/Uri;
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setUri(Landroid/net/Uri;)V

    .line 507
    move/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getLong(I)J

    move-result-wide v3

    long-to-int v0, v3

    move/from16 v19, v0

    .line 508
    .local v19, "size":I
    if-nez v19, :cond_4

    .line 510
    :try_start_0
    const-string v3, "r"

    invoke-virtual {v2, v14, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v13

    .line 511
    .local v13, "file":Landroid/content/res/AssetFileDescriptor;
    if-eqz v13, :cond_3

    .line 512
    invoke-virtual {v13}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v3

    long-to-int v3, v3

    int-to-long v3, v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSize(J)V

    .line 513
    invoke-virtual {v13}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 570
    .end local v13    # "file":Landroid/content/res/AssetFileDescriptor;
    :cond_3
    :goto_2
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 515
    :catch_0
    move-exception v12

    .line 516
    .local v12, "e":Ljava/io/FileNotFoundException;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 517
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 518
    .end local v12    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v12

    .line 519
    .local v12, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 520
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 523
    .end local v12    # "e":Ljava/io/IOException;
    :cond_4
    move/from16 v0, v19

    int-to-long v3, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSize(J)V

    goto :goto_2
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;
    .locals 2

    .prologue
    .line 73
    const-class v1, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->instance:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;-><init>()V

    sput-object v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->instance:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    .line 75
    :cond_0
    sget-object v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->instance:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getVideoMediFromDB(Ljava/util/concurrent/CopyOnWriteArrayList;)V
    .locals 39
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "vec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    new-instance v26, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 216
    .local v26, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    move-object/from16 v0, v26

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 218
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 219
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 220
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 219
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v33

    .line 221
    .local v33, "tempCur":Landroid/database/Cursor;
    new-instance v11, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;

    move-object/from16 v0, v33

    invoke-direct {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;-><init>(Landroid/database/Cursor;)V

    .line 222
    .local v11, "cur":Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;
    if-nez v11, :cond_0

    .line 348
    :goto_0
    return-void

    .line 224
    :cond_0
    const-string v3, "mime_type"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 225
    .local v25, "mimeIndex":I
    const-string v3, "_data"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 226
    .local v13, "dataIndex":I
    const-string v3, "title"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 227
    .local v38, "titleIndex":I
    const-string v3, "_id"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 228
    .local v22, "idIndex":I
    const-string v3, "resolution"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 229
    .local v28, "resolutionIndex":I
    const-string v3, "duration"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 230
    .local v16, "durationIndex":I
    const-string v3, "_size"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 231
    .local v32, "sizeIndex":I
    const-string v3, "album"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 232
    .local v9, "albumIndex":I
    const-string v3, "mini_thumb_magic"

    invoke-virtual {v11, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 234
    .local v36, "thumbmagicIndex":I
    invoke-virtual {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getCount()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->setVideoNumOnDB(I)V

    .line 236
    const/16 v23, 0x0

    .line 238
    .local v23, "isDirectory":Z
    :cond_1
    :goto_1
    invoke-virtual {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 347
    invoke-virtual {v11}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->close()V

    goto :goto_0

    .line 240
    :cond_2
    new-instance v29, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    sget-object v3, Lcom/samsung/upnp/media/server/directory/doa/FileType;->Video:Lcom/samsung/upnp/media/server/directory/doa/FileType;

    move-object/from16 v0, v29

    invoke-direct {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;-><init>(Lcom/samsung/upnp/media/server/directory/doa/FileType;)V

    .line 242
    .local v29, "satInfo":Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    move/from16 v0, v25

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 243
    .local v24, "mime":Ljava/lang/String;
    if-eqz v24, :cond_1

    const-string v3, "pyv"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "dcf"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 244
    const-string v3, "dm"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "pvv"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 245
    const-string v3, "ogg"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "application"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 247
    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setMimeType(Ljava/lang/String;)V

    .line 250
    invoke-virtual {v11, v13}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 251
    .local v12, "data":Ljava/lang/String;
    if-eqz v12, :cond_1

    const-string v3, "pyv"

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "dcf"

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "dm"

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 252
    const-string v3, "pvv"

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ogg"

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 253
    const-string v3, "application"

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 255
    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDataPath(Ljava/lang/String;)V

    .line 256
    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSubTitle(Ljava/lang/String;)V

    .line 258
    move/from16 v0, v38

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 259
    .local v37, "title":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setTitle(Ljava/lang/String;)V

    .line 260
    const-string v3, "category"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setCategory(Ljava/lang/String;)V

    .line 262
    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getInt(I)I

    move-result v14

    .line 263
    .local v14, "db_id":I
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, v14

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v21

    .line 264
    .local v21, "id":Landroid/net/Uri;
    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setUri(Landroid/net/Uri;)V

    .line 266
    move/from16 v0, v28

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 267
    .local v27, "resolution":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setResolution(Ljava/lang/String;)V

    .line 269
    move/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 270
    .local v15, "duration":Ljava/lang/String;
    if-eqz v15, :cond_3

    const-string v3, ""

    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 271
    :cond_3
    const-string v3, "00:00:00"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDurationTime(Ljava/lang/String;)V

    .line 278
    :goto_2
    move/from16 v0, v32

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getLong(I)J

    move-result-wide v30

    .line 279
    .local v30, "size":J
    const-wide/16 v3, 0x0

    cmp-long v3, v30, v3

    if-nez v3, :cond_9

    .line 280
    const/16 v18, 0x0

    .line 282
    .local v18, "file":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    const-string v3, "r"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v18

    .line 283
    if-eqz v18, :cond_4

    .line 284
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v3

    long-to-int v3, v3

    int-to-long v3, v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSize(J)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :cond_4
    if-eqz v18, :cond_5

    .line 291
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 300
    .end local v18    # "file":Landroid/content/res/AssetFileDescriptor;
    :cond_5
    :goto_3
    invoke-virtual {v11, v9}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 301
    .local v8, "album":Ljava/lang/String;
    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setAlbum(Ljava/lang/String;)V

    .line 302
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    move/from16 v0, v36

    invoke-virtual {v11, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->getInt(I)I

    move-result v35

    .line 315
    .local v35, "thumb_magic":I
    const/16 v19, 0x0

    .line 316
    .local v19, "fos":Ljava/io/FileOutputStream;
    if-nez v23, :cond_6

    .line 317
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/samsung/upnp/media/server/ContentDirectory;->CONTENT_THUMBNAIL_LOCATION:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v23

    .line 319
    :cond_6
    new-instance v34, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/upnp/media/server/ContentDirectory;->CONTENT_THUMBNAIL_LOCATION:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".thumb"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    .local v34, "tempFile":Ljava/io/File;
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 322
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setAlbumArtUri(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 273
    .end local v8    # "album":Ljava/lang/String;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .end local v30    # "size":J
    .end local v34    # "tempFile":Ljava/io/File;
    .end local v35    # "thumb_magic":I
    :cond_7
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    div-int/lit16 v3, v3, 0x3e8

    invoke-static {v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->formatTime(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setDurationTime(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 286
    .restart local v18    # "file":Landroid/content/res/AssetFileDescriptor;
    .restart local v30    # "size":J
    :catch_0
    move-exception v17

    .line 287
    .local v17, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 289
    if-eqz v18, :cond_5

    .line 291
    :try_start_3
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 292
    :catch_1
    move-exception v3

    goto :goto_3

    .line 288
    .end local v17    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    .line 289
    if-eqz v18, :cond_8

    .line 291
    :try_start_4
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 295
    :cond_8
    :goto_4
    throw v3

    .line 297
    .end local v18    # "file":Landroid/content/res/AssetFileDescriptor;
    :cond_9
    invoke-virtual/range {v29 .. v31}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setSize(J)V

    goto/16 :goto_3

    .line 325
    .restart local v8    # "album":Ljava/lang/String;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v34    # "tempFile":Ljava/io/File;
    .restart local v35    # "thumb_magic":I
    :cond_a
    int-to-long v3, v14

    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-static {v2, v3, v4, v5, v0}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 326
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_1

    .line 328
    :try_start_5
    new-instance v20, Ljava/io/FileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 329
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .local v20, "fos":Ljava/io/FileOutputStream;
    :try_start_6
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x50

    move-object/from16 v0, v20

    invoke-virtual {v10, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 330
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 331
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->flush()V

    .line 332
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;->setAlbumArtUri(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    move-object/from16 v19, v20

    .line 339
    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    :goto_5
    if-eqz v19, :cond_1

    .line 340
    :try_start_7
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_1

    .line 341
    :catch_2
    move-exception v3

    goto/16 :goto_1

    .line 292
    .end local v8    # "album":Ljava/lang/String;
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .end local v34    # "tempFile":Ljava/io/File;
    .end local v35    # "thumb_magic":I
    .restart local v18    # "file":Landroid/content/res/AssetFileDescriptor;
    :catch_3
    move-exception v4

    goto :goto_4

    :catch_4
    move-exception v3

    goto/16 :goto_3

    .line 335
    .end local v18    # "file":Landroid/content/res/AssetFileDescriptor;
    .restart local v8    # "album":Ljava/lang/String;
    .restart local v10    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v34    # "tempFile":Ljava/io/File;
    .restart local v35    # "thumb_magic":I
    :catch_5
    move-exception v3

    goto :goto_5

    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v3

    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 333
    :catch_7
    move-exception v3

    goto :goto_5

    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v3

    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5
.end method

.method private static toDateString(J)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 917
    const-wide/16 v0, 0xa

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 918
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 919
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public getAudioRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .locals 2

    .prologue
    .line 858
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 859
    .local v0, "recVec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    invoke-direct {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getAudioMediFromDB(Ljava/util/concurrent/CopyOnWriteArrayList;)V

    .line 861
    invoke-direct {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->arrayListToRecordedInfo(Ljava/util/concurrent/CopyOnWriteArrayList;)[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v1

    return-object v1
.end method

.method public getImageNumOnDB()I
    .locals 1

    .prologue
    .line 884
    iget v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->imageNumOnDB:I

    return v0
.end method

.method public getImageRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .locals 2

    .prologue
    .line 865
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 866
    .local v0, "recVec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    invoke-direct {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getImageMediFromDB(Ljava/util/concurrent/CopyOnWriteArrayList;)V

    .line 868
    invoke-direct {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->arrayListToRecordedInfo(Ljava/util/concurrent/CopyOnWriteArrayList;)[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v1

    return-object v1
.end method

.method public getInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 119
    :try_start_0
    iget-object v4, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->isCache:Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;

    invoke-virtual {v4, p1}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$InputStreamCache;->get(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v1

    .line 120
    .local v1, "f":Ljava/io/File;
    if-nez v1, :cond_0

    move-object v2, v3

    .line 125
    .end local v1    # "f":Ljava/io/File;
    :goto_0
    return-object v2

    .line 122
    .restart local v1    # "f":Ljava/io/File;
    :cond_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .local v2, "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 124
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    move-object v2, v3

    .line 125
    goto :goto_0
.end method

.method public getThumbnail(Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "media_store_uri"    # Landroid/net/Uri;
    .param p2, "kind"    # I

    .prologue
    .line 177
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 178
    .local v2, "id":J
    const/4 v0, 0x0

    .line 180
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move-object v1, v0

    .line 191
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v1

    .line 182
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    .line 184
    iget-object v4, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 183
    invoke-static {v4, v2, v3, v5, v6}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    move-object v1, v0

    .line 186
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 188
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_2
    invoke-direct {p0, v2, v3}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getBitmap(J)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 189
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getVideoRecordedInfos()[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;
    .locals 2

    .prologue
    .line 851
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 852
    .local v0, "recVec":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;>;"
    invoke-direct {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getVideoMediFromDB(Ljava/util/concurrent/CopyOnWriteArrayList;)V

    .line 854
    invoke-direct {p0, v0}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->arrayListToRecordedInfo(Ljava/util/concurrent/CopyOnWriteArrayList;)[Lcom/samsung/upnp/media/server/directory/doa/DOARecordedInfo;

    move-result-object v1

    return-object v1
.end method

.method public open()Z
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x1

    return v0
.end method

.method public setAudioNumOnDB(I)V
    .locals 0
    .param p1, "audioNumOnDB"    # I

    .prologue
    .line 896
    iput p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->audioNumOnDB:I

    .line 897
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->androidContext:Landroid/content/Context;

    .line 85
    return-void
.end method

.method public setImageNumOnDB(I)V
    .locals 0
    .param p1, "imageNumOnDB"    # I

    .prologue
    .line 888
    iput p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->imageNumOnDB:I

    .line 889
    return-void
.end method

.method public setVideoNumOnDB(I)V
    .locals 0
    .param p1, "videoNumOnDB"    # I

    .prologue
    .line 904
    iput p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->videoNumOnDB:I

    .line 905
    return-void
.end method
