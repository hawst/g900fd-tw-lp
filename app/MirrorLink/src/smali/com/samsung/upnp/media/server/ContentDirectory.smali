.class public Lcom/samsung/upnp/media/server/ContentDirectory;
.super Ljava/lang/Object;
.source "ContentDirectory.java"

# interfaces
.implements Lcom/samsung/upnp/control/ActionListener;
.implements Lcom/samsung/upnp/control/QueryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/upnp/media/server/ContentDirectory$ICreateObjectReceived;,
        Lcom/samsung/upnp/media/server/ContentDirectory$IUploadFinished;
    }
.end annotation


# static fields
.field public static final BAD_GET_CONTENTS_FEATURES:Ljava/lang/String; = "getcontentFeatures.dlna.org: 2"

.field static final BAD_METADATA:I = 0x2c8

.field public static final BROWSE:Ljava/lang/String; = "Browse"

.field private static final CHECK_CONTENTS:I = 0x3ea

.field private static final CHECK_SYSTEM_ID:I = 0x3e9

.field public static final CONTENT_ALBUM_URI:Ljava/lang/String; = "/MediaStore/AlbumArt"

.field public static final CONTENT_EXPORT_URI:Ljava/lang/String; = "/MediaStore/Out"

.field public static final CONTENT_ID:Ljava/lang/String; = "id"

.field public static final CONTENT_IMPORT_URI:Ljava/lang/String; = "/MediaStore/In"

.field public static final CONTENT_MICRO_THUMBNAILS_URI:Ljava/lang/String; = "/MediaStore/Micro"

.field public static final CONTENT_SMALL_THUMBNAILS_URI:Ljava/lang/String; = "/MediaStore/Small"

.field public static final CONTENT_SUBTITLE_URI:Ljava/lang/String; = "/subtitle"

.field public static final CONTENT_THUMBNAIL_LOCATION:Ljava/lang/String;

.field public static final CREATEOBJECT:Ljava/lang/String; = "CreateObject"

.field private static final DEFAULT_CONTENTUPDATE_INTERVAL:I = 0xea60

.field private static final DEFAULT_PATH:Ljava/lang/String;

.field private static final DEFAULT_SYSTEMUPDATEID_INTERVAL:I = 0x7d0

.field public static final DESTROYOBJECT:Ljava/lang/String; = "DestroyObject"

.field public static final GET_CONTENTS_FEATURES:Ljava/lang/String; = "getcontentFeatures.dlna.org: 1"

.field public static final GET_SEARCH_CAPABILITIES:Ljava/lang/String; = "GetSearchCapabilities"

.field public static final GET_SORT_CAPABILITIES:Ljava/lang/String; = "GetSortCapabilities"

.field public static final GET_SYSTEM_UPDATE_ID:Ljava/lang/String; = "GetSystemUpdateID"

.field public static final ID:Ljava/lang/String; = "Id"

.field public static final REAL_TIME_INFO:Ljava/lang/String; = "realTimeInfo.dlna.org"

.field public static final RESULT:Ljava/lang/String; = "Result"

.field public static final SEARCH:Ljava/lang/String; = "Search"

.field public static final SEARCH_CAPS:Ljava/lang/String; = "SearchCaps"

.field public static final SERVICE_TYPE:Ljava/lang/String; = "urn:schemas-upnp-org:service:ContentDirectory"

.field public static final SORT_CAPS:Ljava/lang/String; = "SortCaps"

.field public static final SYSTEM_UPDATE_ID:Ljava/lang/String; = "SystemUpdateID"

.field public static final TIME_SEEK:Ljava/lang/String; = "TimeSeekRange.dlna.org"

.field public static final TRANSFER_MODE_BACKGROUND:Ljava/lang/String; = "transferMode.dlna.org: Background"

.field public static final TRANSFER_MODE_INTERACTIVE:Ljava/lang/String; = "transferMode.dlna.org: Interactive"

.field public static final TRANSFER_MODE_STREAMING:Ljava/lang/String; = "transferMode.dlna.org: Streaming"

.field private static uploadPath:Ljava/lang/String;


# instance fields
.field private cdsEventHandler:Landroid/os/Handler;

.field private cdsEventThread:Landroid/os/HandlerThread;

.field private contentUpdateInterval:J

.field private createObjectListener:Ljava/lang/Object;

.field private dirList:Lcom/samsung/upnp/media/server/DirectoryList;

.field dlnaParser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

.field private filter:Lcom/samsung/upnp/media/server/object/Filter;

.field private formatList:Lcom/samsung/upnp/media/server/object/FormatList;

.field importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/upnp/media/server/object/item/ImportItemNode;",
            ">;"
        }
    .end annotation
.end field

.field private lastSystemUpdateID:I

.field private lastSystemUpdateTime:J

.field private maxContentID:I

.field private mediaServer:Lcom/samsung/upnp/media/server/MediaServer;

.field private mutex:Lcom/samsung/util/Mutex;

.field private rootNode:Lcom/samsung/upnp/media/server/object/container/RootNode;

.field private searchCapList:Lcom/samsung/upnp/media/server/object/SearchCapList;

.field private sortCapList:Lcom/samsung/upnp/media/server/object/SortCapList;

.field private systemUpdateID:I

.field private systemUpdateIDInterval:J

.field private uploadFinishedListener:Ljava/lang/Object;

.field private varSystemUpdateID:Lcom/samsung/upnp/StateVariable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/android/data/com.sec.android.app.dlna/thumbs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/upnp/media/server/ContentDirectory;->CONTENT_THUMBNAIL_LOCATION:Ljava/lang/String;

    .line 1320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/upnp/media/server/ContentDirectory;->DEFAULT_PATH:Ljava/lang/String;

    .line 1321
    sget-object v0, Lcom/samsung/upnp/media/server/ContentDirectory;->DEFAULT_PATH:Ljava/lang/String;

    sput-object v0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    .line 1530
    return-void
.end method

.method public constructor <init>(Lcom/samsung/upnp/media/server/MediaServer;)V
    .locals 4
    .param p1, "mserver"    # Lcom/samsung/upnp/media/server/MediaServer;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    new-instance v0, Lcom/samsung/util/Mutex;

    invoke-direct {v0}, Lcom/samsung/util/Mutex;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->mutex:Lcom/samsung/util/Mutex;

    .line 305
    new-instance v0, Lcom/samsung/upnp/media/server/object/FormatList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/FormatList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->formatList:Lcom/samsung/upnp/media/server/object/FormatList;

    .line 342
    new-instance v0, Lcom/samsung/upnp/media/server/object/SortCapList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/SortCapList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->sortCapList:Lcom/samsung/upnp/media/server/object/SortCapList;

    .line 395
    new-instance v0, Lcom/samsung/upnp/media/server/object/SearchCapList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/SearchCapList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->searchCapList:Lcom/samsung/upnp/media/server/object/SearchCapList;

    .line 455
    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->filter:Lcom/samsung/upnp/media/server/object/Filter;

    .line 467
    new-instance v0, Lcom/samsung/upnp/media/server/DirectoryList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/DirectoryList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dirList:Lcom/samsung/upnp/media/server/DirectoryList;

    .line 647
    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    .line 680
    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    .line 701
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1420
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dlnaParser:Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    .line 1555
    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    .line 1556
    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventHandler:Landroid/os/Handler;

    .line 1560
    iput v3, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateID:I

    .line 1561
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateTime:J

    .line 1562
    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->varSystemUpdateID:Lcom/samsung/upnp/StateVariable;

    .line 174
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setMediaServer(Lcom/samsung/upnp/media/server/MediaServer;)V

    .line 176
    iput v3, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->systemUpdateID:I

    .line 177
    iput v3, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->maxContentID:I

    .line 179
    const-wide/16 v0, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setSystemUpdateInterval(J)V

    .line 180
    const-wide/32 v0, 0xea60

    invoke-virtual {p0, v0, v1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setContentUpdateInterval(J)V

    .line 182
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->initRootNode()V

    .line 183
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->initSortCaps()V

    .line 184
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->initSearchCaps()V

    .line 185
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->initFilter()V

    .line 186
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/upnp/media/server/ContentDirectory;)J
    .locals 2

    .prologue
    .line 1561
    iget-wide v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$1(Lcom/samsung/upnp/media/server/ContentDirectory;)I
    .locals 1

    .prologue
    .line 1560
    iget v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateID:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/upnp/media/server/ContentDirectory;)Lcom/samsung/upnp/StateVariable;
    .locals 1

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->varSystemUpdateID:Lcom/samsung/upnp/StateVariable;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/upnp/media/server/ContentDirectory;I)V
    .locals 0

    .prologue
    .line 1560
    iput p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateID:I

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/upnp/media/server/ContentDirectory;J)V
    .locals 0

    .prologue
    .line 1561
    iput-wide p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateTime:J

    return-void
.end method

.method private browseActionReceived(Lcom/samsung/upnp/media/server/action/BrowseAction;)Z
    .locals 1
    .param p1, "action"    # Lcom/samsung/upnp/media/server/action/BrowseAction;

    .prologue
    .line 745
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->isMetadata()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 746
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->browseMetadataActionReceived(Lcom/samsung/upnp/media/server/action/BrowseAction;)Z

    move-result v0

    .line 749
    :goto_0
    return v0

    .line 747
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->isDirectChildren()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 748
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->browseDirectChildrenActionReceived(Lcom/samsung/upnp/media/server/action/BrowseAction;)Z

    move-result v0

    goto :goto_0

    .line 749
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private browseDirectChildrenActionReceived(Lcom/samsung/upnp/media/server/action/BrowseAction;)Z
    .locals 56
    .param p1, "action"    # Lcom/samsung/upnp/media/server/action/BrowseAction;

    .prologue
    .line 869
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getObjectID()Ljava/lang/String;

    move-result-object v33

    .line 870
    .local v33, "objID":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/ContentDirectory;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v31

    .line 871
    .local v31, "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v31, :cond_0

    .line 872
    const/16 v5, 0x2bd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setStatus(I)V

    .line 873
    const/4 v5, 0x0

    .line 1084
    :goto_0
    return v5

    .line 876
    :cond_0
    new-instance v18, Lcom/samsung/upnp/media/server/object/ContentNodeList;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/upnp/media/server/object/ContentNodeList;-><init>()V

    .line 877
    .local v18, "contentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNContentNodes()I

    move-result v30

    .line 878
    .local v30, "nChildNodes":I
    const/16 v29, 0x0

    .local v29, "n":I
    :goto_1
    move/from16 v0, v29

    move/from16 v1, v30

    if-lt v0, v1, :cond_1

    .line 884
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getSortCriteria()Ljava/lang/String;

    move-result-object v44

    .line 885
    .local v44, "sortCriteria":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v2}, Lcom/samsung/upnp/media/server/ContentDirectory;->sortContentNodeList(Lcom/samsung/upnp/media/server/object/ContentNodeList;Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNodeList;

    move-result-object v45

    .line 887
    .local v45, "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    if-nez v45, :cond_2

    .line 889
    const/16 v5, 0x2c5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setStatus(I)V

    .line 890
    const/4 v5, 0x0

    goto :goto_0

    .line 879
    .end local v44    # "sortCriteria":Ljava/lang/String;
    .end local v45    # "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    :cond_1
    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v17

    .line 880
    .local v17, "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->add(Ljava/lang/Object;)Z

    .line 878
    add-int/lit8 v29, v29, 0x1

    goto :goto_1

    .line 893
    .end local v17    # "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    .restart local v44    # "sortCriteria":Ljava/lang/String;
    .restart local v45    # "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    :cond_2
    const-wide/16 v46, 0x0

    .line 894
    .local v46, "startingIndex":J
    const-wide/16 v38, 0x0

    .line 897
    .local v38, "requestedCount":J
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getStartingIndex()J

    move-result-wide v46

    .line 898
    const-wide/16 v5, 0x0

    cmp-long v5, v46, v5

    if-gtz v5, :cond_3

    .line 899
    const-wide/16 v46, 0x0

    .line 900
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getRequestedCount()J

    move-result-wide v38

    .line 901
    const-wide v5, 0x100000000L

    cmp-long v5, v46, v5

    if-gtz v5, :cond_4

    const-wide v5, 0x100000000L

    cmp-long v5, v38, v5

    if-lez v5, :cond_5

    .line 902
    :cond_4
    const/16 v5, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setStatus(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    const/4 v5, 0x0

    goto :goto_0

    .line 905
    :catch_0
    move-exception v21

    .line 906
    .local v21, "e":Ljava/lang/Exception;
    const/16 v5, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setStatus(I)V

    .line 907
    const/4 v5, 0x0

    goto :goto_0

    .line 910
    .end local v21    # "e":Ljava/lang/Exception;
    :cond_5
    const-wide/16 v5, 0x0

    cmp-long v5, v38, v5

    if-nez v5, :cond_6

    .line 911
    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v38, v0

    .line 913
    :cond_6
    new-instance v20, Lcom/samsung/upnp/media/server/object/DIDLLite;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/upnp/media/server/object/DIDLLite;-><init>()V

    .line 914
    .local v20, "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    const/16 v32, 0x0

    .line 916
    .local v32, "numberReturned":I
    const/16 v48, 0x0

    .line 917
    .local v48, "startingIndexInt":I
    move/from16 v0, v30

    int-to-long v5, v0

    cmp-long v5, v46, v5

    if-lez v5, :cond_a

    .line 918
    move/from16 v48, v30

    .line 922
    :goto_2
    move/from16 v0, v30

    int-to-long v5, v0

    cmp-long v5, v38, v5

    if-lez v5, :cond_7

    .line 923
    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v38, v0

    .line 924
    :cond_7
    const-wide/16 v5, 0x64

    cmp-long v5, v38, v5

    if-lez v5, :cond_8

    .line 925
    const-wide/16 v38, 0x64

    .line 927
    :cond_8
    move/from16 v29, v48

    :goto_3
    move/from16 v0, v29

    move/from16 v1, v30

    if-ge v0, v1, :cond_9

    move/from16 v0, v32

    int-to-long v5, v0

    cmp-long v5, v5, v38

    if-ltz v5, :cond_b

    .line 1078
    :cond_9
    :goto_4
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/upnp/media/server/object/DIDLLite;->toString()Ljava/lang/String;

    move-result-object v43

    .line 1079
    .local v43, "result":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setResult(Ljava/lang/String;)V

    .line 1080
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setNumberReturned(I)V

    .line 1081
    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setTotalMaches(I)V

    .line 1082
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSystemUpdateID()I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setUpdateID(I)V

    .line 1084
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 920
    .end local v43    # "result":Ljava/lang/String;
    :cond_a
    move-wide/from16 v0, v46

    long-to-int v0, v0

    move/from16 v48, v0

    goto :goto_2

    .line 930
    :cond_b
    :try_start_1
    move-object/from16 v0, v45

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v17

    .line 934
    .restart local v17    # "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v17, :cond_d

    .line 927
    :cond_c
    :goto_5
    add-int/lit8 v29, v29, 0x1

    goto :goto_3

    .line 931
    .end local v17    # "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    :catch_1
    move-exception v21

    .line 932
    .restart local v21    # "e":Ljava/lang/Exception;
    goto :goto_4

    .line 939
    .end local v21    # "e":Ljava/lang/Exception;
    .restart local v17    # "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_d
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/upnp/media/server/object/ContentNode;->isItemNode()Z

    move-result v5

    if-eqz v5, :cond_14

    move-object/from16 v5, v17

    .line 940
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->updateResourceUri(Ljava/lang/String;)V

    .line 941
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/upnp/media/server/object/ContentNode;->getUPnPClass()Ljava/lang/String;

    move-result-object v5

    const-string v6, "object.item.imageItem"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_12

    move-object/from16 v5, v17

    .line 943
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getDefaultResource()Lcom/samsung/upnp/media/server/object/ResourceProperty;

    move-result-object v40

    .line 945
    .local v40, "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    if-eqz v40, :cond_12

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->get4thfield()Ljava/lang/String;

    move-result-object v5

    const-string v6, "*"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 946
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/upnp/media/server/MediaServer;->androidContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v5, v17

    .line 947
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    iget v5, v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;->imageDB_id:I

    int-to-long v7, v5

    .line 948
    const/4 v5, 0x1

    .line 949
    const/4 v9, 0x0

    .line 946
    invoke-static {v6, v7, v8, v5, v9}, Landroid/provider/MediaStore$Images$Thumbnails;->queryMiniThumbnail(Landroid/content/ContentResolver;JI[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v52

    .line 951
    .local v52, "thumbCur":Landroid/database/Cursor;
    if-eqz v52, :cond_e

    invoke-interface/range {v52 .. v52}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 952
    const-string v5, "_id"

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 953
    .local v19, "db_id":I
    sget-object v5, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move/from16 v0, v19

    int-to-long v6, v0

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v53

    .line 956
    .local v53, "thumbUri2":Landroid/net/Uri;
    const-string v5, "width"

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v55

    .line 957
    .local v55, "width2":I
    const-string v5, "height"

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 958
    .local v26, "height2":I
    const-string v5, "_data"

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v52

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 961
    .local v24, "filepath":Ljava/lang/String;
    :try_start_2
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 962
    .local v22, "file":Ljava/io/File;
    if-eqz v22, :cond_16

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 964
    new-instance v51, Lcom/samsung/xml/AttributeList;

    invoke-direct/range {v51 .. v51}, Lcom/samsung/xml/AttributeList;-><init>()V

    .line 965
    .local v51, "thumbAttrs":Lcom/samsung/xml/AttributeList;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentSmallThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 966
    .local v54, "url":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v5

    const-string v6, "image/jpeg"

    .line 967
    sget v7, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_SMALL:I

    .line 966
    move/from16 v0, v55

    move/from16 v1, v26

    invoke-virtual {v5, v6, v0, v1, v7}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v37

    .line 968
    .local v37, "protocol2":Ljava/lang/String;
    new-instance v16, Lcom/samsung/xml/Attribute;

    const-string v5, "size"

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-direct {v0, v5, v6}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    .local v16, "attr2":Lcom/samsung/xml/Attribute;
    move-object/from16 v0, v51

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 971
    new-instance v16, Lcom/samsung/xml/Attribute;

    .end local v16    # "attr2":Lcom/samsung/xml/Attribute;
    const-string v5, "resolution"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v55 .. v55}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-direct {v0, v5, v6}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    .restart local v16    # "attr2":Lcom/samsung/xml/Attribute;
    move-object/from16 v0, v51

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 974
    new-instance v41, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;

    move-object/from16 v0, v41

    move-object/from16 v1, v54

    move-object/from16 v2, v37

    move-object/from16 v3, v51

    move-object/from16 v4, v53

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;Landroid/net/Uri;)V

    .line 975
    .local v41, "res2":Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;
    move-object/from16 v0, v17

    check-cast v0, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    move-object v5, v0

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 994
    .end local v16    # "attr2":Lcom/samsung/xml/Attribute;
    .end local v37    # "protocol2":Ljava/lang/String;
    .end local v41    # "res2":Lcom/samsung/upnp/media/server/object/item/sat/DOAResourceProperty;
    .end local v51    # "thumbAttrs":Lcom/samsung/xml/AttributeList;
    .end local v54    # "url":Ljava/lang/String;
    :goto_6
    if-eqz v52, :cond_e

    .line 995
    invoke-interface/range {v52 .. v52}, Landroid/database/Cursor;->close()V

    .line 996
    const/16 v52, 0x0

    .line 1001
    .end local v19    # "db_id":I
    .end local v22    # "file":Ljava/io/File;
    .end local v24    # "filepath":Ljava/lang/String;
    .end local v26    # "height2":I
    .end local v53    # "thumbUri2":Landroid/net/Uri;
    .end local v55    # "width2":I
    :cond_e
    :goto_7
    if-eqz v52, :cond_f

    .line 1002
    invoke-interface/range {v52 .. v52}, Landroid/database/Cursor;->close()V

    .line 1003
    const/16 v52, 0x0

    .line 1006
    :cond_f
    new-instance v34, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v34 .. v34}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1007
    .local v34, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    move-object/from16 v0, v34

    iput-boolean v5, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1008
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->getMimeType()Ljava/lang/String;

    move-result-object v28

    .line 1009
    .local v28, "mime":Ljava/lang/String;
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->getInputStream()Ljava/io/InputStream;

    move-result-object v27

    .line 1010
    .local v27, "is":Ljava/io/InputStream;
    if-eqz v27, :cond_12

    .line 1011
    const/4 v5, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-static {v0, v5, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1014
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v5

    move-object/from16 v0, v34

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v34

    iget v7, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sget v8, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    move-object/from16 v0, v28

    invoke-virtual {v5, v0, v6, v7, v8}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v36

    .line 1015
    .local v36, "protocol":Ljava/lang/String;
    move-object/from16 v0, v40

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->updateProtocol(Ljava/lang/String;)V

    .line 1017
    new-instance v5, Ljava/lang/StringBuilder;

    move-object/from16 v0, v34

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v34

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    .line 1018
    .local v49, "tempStr":Ljava/lang/String;
    const-string v5, "resolution"

    move-object/from16 v0, v40

    move-object/from16 v1, v49

    invoke-virtual {v0, v5, v1}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/16 v6, 0x78

    if-lt v5, v6, :cond_10

    move-object/from16 v0, v34

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v6, 0x78

    if-ge v5, v6, :cond_11

    :cond_10
    move-object/from16 v5, v17

    .line 1021
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getThumbnailResource()Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;

    move-result-object v35

    .line 1022
    .local v35, "prop":Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;
    if-eqz v35, :cond_11

    .line 1023
    const-string v5, "resolution"

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->getAttribute(Ljava/lang/String;)Lcom/samsung/xml/Attribute;

    move-result-object v15

    .line 1024
    .local v15, "attr":Lcom/samsung/xml/Attribute;
    move-object/from16 v0, v35

    invoke-virtual {v0, v15}, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->removeAttribute(Lcom/samsung/xml/Attribute;)Z

    .line 1026
    new-instance v50, Lcom/samsung/xml/Attribute;

    const-string v5, "resolution"

    new-instance v6, Ljava/lang/StringBuilder;

    move-object/from16 v0, v34

    iget v7, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v34

    iget v7, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v50

    invoke-direct {v0, v5, v6}, Lcom/samsung/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    .local v50, "tempattr":Lcom/samsung/xml/Attribute;
    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->addAttribute(Lcom/samsung/xml/Attribute;)V

    .line 1032
    .end local v15    # "attr":Lcom/samsung/xml/Attribute;
    .end local v35    # "prop":Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;
    .end local v50    # "tempattr":Lcom/samsung/xml/Attribute;
    :cond_11
    :try_start_3
    invoke-virtual/range {v27 .. v27}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1039
    .end local v27    # "is":Ljava/io/InputStream;
    .end local v28    # "mime":Ljava/lang/String;
    .end local v34    # "option":Landroid/graphics/BitmapFactory$Options;
    .end local v36    # "protocol":Ljava/lang/String;
    .end local v40    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v49    # "tempStr":Ljava/lang/String;
    .end local v52    # "thumbCur":Landroid/database/Cursor;
    :cond_12
    :goto_8
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/upnp/media/server/object/ContentNode;->getUPnPClass()Ljava/lang/String;

    move-result-object v5

    const-string v6, "object.item.audioItem"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_14

    move-object/from16 v5, v17

    .line 1041
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getAudioAlbum_id()Ljava/lang/String;

    move-result-object v13

    .line 1043
    .local v13, "albumId":Ljava/lang/String;
    if-eqz v13, :cond_14

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_14

    .line 1044
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/upnp/media/server/MediaServer;->androidContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1045
    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "_id="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 1044
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1046
    .local v11, "albumCur":Landroid/database/Cursor;
    if-eqz v11, :cond_14

    .line 1048
    :cond_13
    :goto_9
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_18

    .line 1060
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1067
    .end local v11    # "albumCur":Landroid/database/Cursor;
    .end local v13    # "albumId":Ljava/lang/String;
    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getFilter()Ljava/lang/String;

    move-result-object v25

    .line 1068
    .local v25, "filterCondition":Ljava/lang/String;
    const-string v5, "*"

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_15

    const-string v5, ""

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 1070
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/upnp/media/server/ContentDirectory;->filter:Lcom/samsung/upnp/media/server/object/Filter;

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v5, v0, v1}, Lcom/samsung/upnp/media/server/object/Filter;->getContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v17

    .line 1072
    :cond_15
    if-eqz v17, :cond_c

    .line 1074
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/DIDLLite;->addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 1075
    add-int/lit8 v32, v32, 0x1

    goto/16 :goto_5

    .line 979
    .end local v25    # "filterCondition":Ljava/lang/String;
    .restart local v19    # "db_id":I
    .restart local v22    # "file":Ljava/io/File;
    .restart local v24    # "filepath":Ljava/lang/String;
    .restart local v26    # "height2":I
    .restart local v40    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .restart local v52    # "thumbCur":Landroid/database/Cursor;
    .restart local v53    # "thumbUri2":Landroid/net/Uri;
    .restart local v55    # "width2":I
    :cond_16
    :try_start_4
    const-string v5, "DLNA"

    const-string v6, "No thumb file!!!!"

    invoke-static {v5, v6}, Lcom/samsung/util/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_6

    .line 986
    .end local v22    # "file":Ljava/io/File;
    :catch_2
    move-exception v21

    .line 987
    .restart local v21    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string v5, "DLNA"

    const-string v6, "No thumb file!!!!"

    invoke-static {v5, v6}, Lcom/samsung/util/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 994
    if-eqz v52, :cond_e

    .line 995
    invoke-interface/range {v52 .. v52}, Landroid/database/Cursor;->close()V

    .line 996
    const/16 v52, 0x0

    goto/16 :goto_7

    .line 993
    .end local v21    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 994
    if-eqz v52, :cond_17

    .line 995
    invoke-interface/range {v52 .. v52}, Landroid/database/Cursor;->close()V

    .line 996
    const/16 v52, 0x0

    .line 998
    :cond_17
    throw v5

    .line 1049
    .end local v19    # "db_id":I
    .end local v24    # "filepath":Ljava/lang/String;
    .end local v26    # "height2":I
    .end local v40    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v52    # "thumbCur":Landroid/database/Cursor;
    .end local v53    # "thumbUri2":Landroid/net/Uri;
    .end local v55    # "width2":I
    .restart local v11    # "albumCur":Landroid/database/Cursor;
    .restart local v13    # "albumId":Ljava/lang/String;
    :cond_18
    const-string v5, "album_art"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1050
    .local v12, "albumData":Ljava/lang/String;
    if-eqz v12, :cond_13

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_13

    .line 1051
    move-object/from16 v23, v12

    .line 1052
    .local v23, "filePath":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 1053
    .restart local v54    # "url":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v5

    const-string v6, "image/jpeg"

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget v9, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v14

    .line 1054
    .local v14, "albumartProtocol":Ljava/lang/String;
    new-instance v42, Lcom/samsung/upnp/media/server/object/item/sat/DOAAlbumartProperty;

    move-object/from16 v0, v42

    move-object/from16 v1, v54

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/upnp/media/server/object/item/sat/DOAAlbumartProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .local v42, "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object/from16 v5, v17

    .line 1055
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    move-object/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 1057
    const-string v5, "upnp:albumArtURI"

    move-object/from16 v0, v17

    move-object/from16 v1, v54

    invoke-virtual {v0, v5, v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 1033
    .end local v11    # "albumCur":Landroid/database/Cursor;
    .end local v12    # "albumData":Ljava/lang/String;
    .end local v13    # "albumId":Ljava/lang/String;
    .end local v14    # "albumartProtocol":Ljava/lang/String;
    .end local v23    # "filePath":Ljava/lang/String;
    .end local v42    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v54    # "url":Ljava/lang/String;
    .restart local v27    # "is":Ljava/io/InputStream;
    .restart local v28    # "mime":Ljava/lang/String;
    .restart local v34    # "option":Landroid/graphics/BitmapFactory$Options;
    .restart local v36    # "protocol":Ljava/lang/String;
    .restart local v40    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .restart local v49    # "tempStr":Ljava/lang/String;
    .restart local v52    # "thumbCur":Landroid/database/Cursor;
    :catch_3
    move-exception v5

    goto/16 :goto_8
.end method

.method private browseMetadataActionReceived(Lcom/samsung/upnp/media/server/action/BrowseAction;)Z
    .locals 9
    .param p1, "action"    # Lcom/samsung/upnp/media/server/action/BrowseAction;

    .prologue
    const/16 v8, 0x2bd

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 758
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getObjectID()Ljava/lang/String;

    move-result-object v3

    .line 759
    .local v3, "objID":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/media/server/ContentDirectory;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v0

    .line 760
    .local v0, "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v0, :cond_0

    .line 761
    invoke-virtual {p1, v8}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setStatus(I)V

    .line 791
    :goto_0
    return v5

    .line 765
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->getFilter()Ljava/lang/String;

    move-result-object v2

    .line 766
    .local v2, "filterCondition":Ljava/lang/String;
    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 768
    iget-object v7, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->filter:Lcom/samsung/upnp/media/server/object/Filter;

    invoke-virtual {v7, v0, v2}, Lcom/samsung/upnp/media/server/object/Filter;->getContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v0

    .line 770
    :cond_1
    if-nez v0, :cond_2

    .line 771
    invoke-virtual {p1, v8}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setStatus(I)V

    goto :goto_0

    .line 775
    :cond_2
    const-string v5, "Filter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "browseMetadata "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/util/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    new-instance v1, Lcom/samsung/upnp/media/server/object/DIDLLite;

    invoke-direct {v1}, Lcom/samsung/upnp/media/server/object/DIDLLite;-><init>()V

    .line 780
    .local v1, "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/media/server/object/DIDLLite;->setContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 781
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/DIDLLite;->toString()Ljava/lang/String;

    move-result-object v4

    .line 783
    .local v4, "result":Ljava/lang/String;
    const-string v5, "Result"

    invoke-virtual {p1, v5, v4}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    const-string v5, "NumberReturned"

    invoke-virtual {p1, v5, v6}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 785
    const-string v5, "TotalMatches"

    invoke-virtual {p1, v5, v6}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 786
    const-string v5, "UpdateID"

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSystemUpdateID()I

    move-result v7

    invoke-virtual {p1, v5, v7}, Lcom/samsung/upnp/media/server/action/BrowseAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 788
    invoke-static {}, Lcom/samsung/util/Debugs;->isOn()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 789
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;->print()V

    :cond_3
    move v5, v6

    .line 791
    goto :goto_0
.end method

.method private createActionReceived(Lcom/samsung/upnp/media/server/action/CreateObjectAction;)Z
    .locals 11
    .param p1, "action"    # Lcom/samsung/upnp/media/server/action/CreateObjectAction;

    .prologue
    const/4 v8, 0x0

    .line 703
    if-nez p1, :cond_0

    .line 736
    :goto_0
    return v8

    .line 705
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;->isAnyContainer()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 706
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v6

    .line 708
    .local v6, "parser":Lcom/samsung/xml/Parser;
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;->getElements()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v4

    .line 709
    .local v4, "node":Lcom/samsung/xml/Node;
    const-string v9, "item"

    invoke-virtual {v4, v9}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v3

    .line 710
    .local v3, "itemNode":Lcom/samsung/xml/Node;
    invoke-static {v3}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->createImportItemNode(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/media/server/object/item/ImportItemNode;

    move-result-object v2

    .line 711
    .local v2, "importItemNode":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    if-nez v2, :cond_1

    .line 712
    const/16 v9, 0x2c8

    const-string v10, "Bad Metadata"

    invoke-virtual {p1, v9, v10}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;->setStatus(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 728
    .end local v2    # "importItemNode":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    .end local v3    # "itemNode":Lcom/samsung/xml/Node;
    .end local v4    # "node":Lcom/samsung/xml/Node;
    :catch_0
    move-exception v1

    .line 729
    .local v1, "e":Lcom/samsung/xml/ParserException;
    invoke-virtual {v1}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    goto :goto_0

    .line 716
    .end local v1    # "e":Lcom/samsung/xml/ParserException;
    .restart local v2    # "importItemNode":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    .restart local v3    # "itemNode":Lcom/samsung/xml/Node;
    .restart local v4    # "node":Lcom/samsung/xml/Node;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getUPnPClass()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/upnp/media/server/ContentDirectory;->findContainerNodeByUPnPClass(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v5

    .line 717
    .local v5, "parentNode":Lcom/samsung/upnp/media/server/object/ContentNode;
    iget-object v9, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v9, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    if-eqz v5, :cond_2

    .line 719
    invoke-virtual {v5, v2}, Lcom/samsung/upnp/media/server/object/ContentNode;->addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 721
    :cond_2
    new-instance v0, Lcom/samsung/upnp/media/server/object/DIDLLite;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/DIDLLite;-><init>()V

    .line 722
    .local v0, "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    invoke-virtual {v0, v2}, Lcom/samsung/upnp/media/server/object/DIDLLite;->addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 723
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/DIDLLite;->toString()Ljava/lang/String;

    move-result-object v7

    .line 724
    .local v7, "result":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;->setObjectID(Ljava/lang/String;)V

    .line 725
    invoke-virtual {p1, v7}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;->setResult(Ljava/lang/String;)V

    .line 726
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->updateSystemUpdateID()V
    :try_end_1
    .catch Lcom/samsung/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 727
    const/4 v8, 0x1

    goto :goto_0

    .line 730
    .end local v0    # "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    .end local v2    # "importItemNode":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    .end local v3    # "itemNode":Lcom/samsung/xml/Node;
    .end local v4    # "node":Lcom/samsung/xml/Node;
    .end local v5    # "parentNode":Lcom/samsung/upnp/media/server/object/ContentNode;
    .end local v7    # "result":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 731
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 734
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v6    # "parser":Lcom/samsung/xml/Parser;
    :cond_3
    const/16 v9, 0x2c9

    const-string v10, "Restricted Parent Object"

    invoke-virtual {p1, v9, v10}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;->setStatus(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized getNextContentID()I
    .locals 1

    .prologue
    .line 248
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->maxContentID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->maxContentID:I

    .line 249
    iget v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->maxContentID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getSearchCapabilities()Ljava/lang/String;
    .locals 6

    .prologue
    .line 439
    const-string v3, ""

    .line 440
    .local v3, "searchCapsStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getNSearchCaps()I

    move-result v2

    .line 441
    .local v2, "nSearchCaps":I
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->searchCapList:Lcom/samsung/upnp/media/server/object/SearchCapList;

    .line 442
    .local v0, "localSearchCapList":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/samsung/upnp/media/server/object/SearchCap;>;"
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 448
    return-object v3

    .line 443
    :cond_0
    if-lez v1, :cond_1

    .line 444
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 445
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/upnp/media/server/object/SearchCap;

    invoke-interface {v4}, Lcom/samsung/upnp/media/server/object/SearchCap;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 442
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getSearchContentList(Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/SearchCriteriaList;Lcom/samsung/upnp/media/server/object/SearchCapList;Lcom/samsung/upnp/media/server/object/ContentNodeList;)I
    .locals 4
    .param p1, "node"    # Lcom/samsung/upnp/media/server/object/ContentNode;
    .param p2, "searchCriList"    # Lcom/samsung/upnp/media/server/object/SearchCriteriaList;
    .param p3, "searchCapList"    # Lcom/samsung/upnp/media/server/object/SearchCapList;
    .param p4, "contentNodeList"    # Lcom/samsung/upnp/media/server/object/ContentNodeList;

    .prologue
    .line 1134
    if-nez p1, :cond_0

    .line 1135
    const/4 v3, 0x0

    .line 1144
    :goto_0
    return v3

    .line 1137
    :cond_0
    invoke-virtual {p2, p1, p3}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;->compare(Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/SearchCapList;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1138
    invoke-virtual {p4, p1}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->add(Ljava/lang/Object;)Z

    .line 1139
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNContentNodes()I

    move-result v2

    .line 1140
    .local v2, "nChildNodes":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    if-lt v1, v2, :cond_2

    .line 1144
    invoke-virtual {p4}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->size()I

    move-result v3

    goto :goto_0

    .line 1141
    :cond_2
    invoke-virtual {p1, v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v0

    .line 1142
    .local v0, "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSearchContentList(Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/SearchCriteriaList;Lcom/samsung/upnp/media/server/object/SearchCapList;Lcom/samsung/upnp/media/server/object/ContentNodeList;)I

    .line 1140
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getSearchCriteriaList(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SearchCriteriaList;
    .locals 8
    .param p1, "searchStr"    # Ljava/lang/String;

    .prologue
    .line 1093
    new-instance v5, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;

    invoke-direct {v5}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;-><init>()V

    .line 1095
    .local v5, "searchList":Lcom/samsung/upnp/media/server/object/SearchCriteriaList;
    if-nez p1, :cond_1

    .line 1129
    :cond_0
    :goto_0
    return-object v5

    .line 1097
    :cond_1
    const-string v7, "*"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_3

    .line 1099
    :cond_2
    new-instance v3, Lcom/samsung/upnp/media/server/object/SearchCriteria;

    invoke-direct {v3}, Lcom/samsung/upnp/media/server/object/SearchCriteria;-><init>()V

    .line 1100
    .local v3, "searchCri":Lcom/samsung/upnp/media/server/object/SearchCriteria;
    const-string v7, "*"

    invoke-virtual {v3, v7}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 1101
    const-string v7, "exists"

    invoke-virtual {v3, v7}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 1102
    const-string v7, "true"

    invoke-virtual {v3, v7}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setValue(Ljava/lang/String;)V

    .line 1103
    const-string v7, "or"

    invoke-virtual {v3, v7}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setLogic(Ljava/lang/String;)V

    .line 1104
    invoke-virtual {v5, v3}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1109
    .end local v3    # "searchCri":Lcom/samsung/upnp/media/server/object/SearchCriteria;
    :cond_3
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v7, " \t\n\u000c\r"

    invoke-direct {v4, p1, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    .local v4, "searchCriTokenizer":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1111
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 1112
    .local v2, "prop":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1114
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 1115
    .local v0, "binOp":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1117
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 1118
    .local v6, "value":Ljava/lang/String;
    const-string v7, "\""

    invoke-static {v6, v7}, Lcom/samsung/util/StringUtil;->trim(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1119
    const-string v1, ""

    .line 1120
    .local v1, "logOp":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1121
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 1122
    :cond_4
    new-instance v3, Lcom/samsung/upnp/media/server/object/SearchCriteria;

    invoke-direct {v3}, Lcom/samsung/upnp/media/server/object/SearchCriteria;-><init>()V

    .line 1123
    .restart local v3    # "searchCri":Lcom/samsung/upnp/media/server/object/SearchCriteria;
    invoke-virtual {v3, v2}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 1124
    invoke-virtual {v3, v0}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 1125
    invoke-virtual {v3, v6}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setValue(Ljava/lang/String;)V

    .line 1126
    invoke-virtual {v3, v1}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->setLogic(Ljava/lang/String;)V

    .line 1127
    invoke-virtual {v5, v3}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private getSortCapabilities()Ljava/lang/String;
    .locals 7

    .prologue
    .line 378
    const-string v3, ""

    .line 379
    .local v3, "sortCapsStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getNSortCaps()I

    move-result v1

    .line 380
    .local v1, "nSortCaps":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 387
    return-object v3

    .line 381
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSortCap(I)Lcom/samsung/upnp/media/server/object/SortCap;

    move-result-object v2

    .line 382
    .local v2, "sortCap":Lcom/samsung/upnp/media/server/object/SortCap;
    invoke-interface {v2}, Lcom/samsung/upnp/media/server/object/SortCap;->getType()Ljava/lang/String;

    move-result-object v4

    .line 383
    .local v4, "type":Ljava/lang/String;
    if-lez v0, :cond_1

    .line 384
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 385
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 380
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getSortCriteriaArray(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SortCriterionList;
    .locals 4
    .param p1, "sortCriteria"    # Ljava/lang/String;

    .prologue
    .line 819
    new-instance v0, Lcom/samsung/upnp/media/server/object/SortCriterionList;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/SortCriterionList;-><init>()V

    .line 820
    .local v0, "sortCriList":Lcom/samsung/upnp/media/server/object/SortCriterionList;
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v3, ", "

    invoke-direct {v1, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    .local v1, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-nez v3, :cond_0

    .line 825
    return-object v0

    .line 822
    :cond_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 823
    .local v2, "token":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/samsung/upnp/media/server/object/SortCriterionList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getUploadPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1342
    sget-object v0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    return-object v0
.end method

.method private initFilter()V
    .locals 1

    .prologue
    .line 459
    new-instance v0, Lcom/samsung/upnp/media/server/object/Filter;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/Filter;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->filter:Lcom/samsung/upnp/media/server/object/Filter;

    .line 460
    return-void
.end method

.method private initRootNode()V
    .locals 1

    .prologue
    .line 278
    new-instance v0, Lcom/samsung/upnp/media/server/object/container/RootNode;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/container/RootNode;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->rootNode:Lcom/samsung/upnp/media/server/object/container/RootNode;

    .line 279
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->rootNode:Lcom/samsung/upnp/media/server/object/container/RootNode;

    invoke-virtual {v0, p0}, Lcom/samsung/upnp/media/server/object/container/RootNode;->setContentDirectory(Lcom/samsung/upnp/media/server/ContentDirectory;)V

    .line 280
    return-void
.end method

.method private initSearchCaps()V
    .locals 1

    .prologue
    .line 425
    new-instance v0, Lcom/samsung/upnp/media/server/object/search/AllSearchCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/search/AllSearchCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSearchCap(Lcom/samsung/upnp/media/server/object/SearchCap;)Z

    .line 426
    new-instance v0, Lcom/samsung/upnp/media/server/object/search/IdSearchCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/search/IdSearchCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSearchCap(Lcom/samsung/upnp/media/server/object/SearchCap;)Z

    .line 427
    new-instance v0, Lcom/samsung/upnp/media/server/object/search/TitleSearchCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/search/TitleSearchCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSearchCap(Lcom/samsung/upnp/media/server/object/SearchCap;)Z

    .line 429
    new-instance v0, Lcom/samsung/upnp/media/server/object/search/UPnPClassSearchCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/search/UPnPClassSearchCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSearchCap(Lcom/samsung/upnp/media/server/object/SearchCap;)Z

    .line 430
    return-void
.end method

.method private initSortCaps()V
    .locals 1

    .prologue
    .line 367
    new-instance v0, Lcom/samsung/upnp/media/server/object/sort/UPnPClassSortCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/sort/UPnPClassSortCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSortCap(Lcom/samsung/upnp/media/server/object/SortCap;)Z

    .line 368
    new-instance v0, Lcom/samsung/upnp/media/server/object/sort/DCTitleSortCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/sort/DCTitleSortCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSortCap(Lcom/samsung/upnp/media/server/object/SortCap;)Z

    .line 369
    new-instance v0, Lcom/samsung/upnp/media/server/object/sort/DCDateSortCap;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/sort/DCDateSortCap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->addSortCap(Lcom/samsung/upnp/media/server/object/SortCap;)Z

    .line 370
    return-void
.end method

.method private notifiyUploadContentFinished(Lcom/samsung/upnp/media/server/object/item/ItemNode;Z)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/upnp/media/server/object/item/ItemNode;
    .param p2, "bSuccess"    # Z

    .prologue
    .line 686
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 688
    :try_start_0
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    instance-of v1, v1, Lcom/samsung/upnp/media/server/ContentDirectory$IUploadFinished;

    if-eqz v1, :cond_1

    .line 689
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/upnp/media/server/ContentDirectory$IUploadFinished;

    invoke-interface {v1, p1, p2}, Lcom/samsung/upnp/media/server/ContentDirectory$IUploadFinished;->onUploadFinished(Lcom/samsung/upnp/media/server/object/item/ItemNode;Z)V

    .line 698
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    instance-of v1, v1, Lcom/samsung/api/UploadFinished;

    if-eqz v1, :cond_0

    .line 691
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/api/UploadFinished;

    new-instance v2, Lcom/samsung/api/ContentItem;

    invoke-direct {v2, p1}, Lcom/samsung/api/ContentItem;-><init>(Lcom/samsung/xml/Node;)V

    invoke-interface {v1, v2, p2}, Lcom/samsung/api/UploadFinished;->onUploadFinished(Lcom/samsung/api/ContentItem;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 692
    :catch_0
    move-exception v0

    .line 693
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 694
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 695
    .local v0, "e":Ljava/lang/Error;
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method

.method private notifyCreateObjectReceived(Lcom/samsung/upnp/control/ActionRequest;)Z
    .locals 3
    .param p1, "actionRequest"    # Lcom/samsung/upnp/control/ActionRequest;

    .prologue
    const/4 v2, 0x0

    .line 658
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 660
    :try_start_0
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    instance-of v1, v1, Lcom/samsung/upnp/media/server/ContentDirectory$ICreateObjectReceived;

    if-eqz v1, :cond_0

    .line 661
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/upnp/media/server/ContentDirectory$ICreateObjectReceived;

    invoke-interface {v1, p1}, Lcom/samsung/upnp/media/server/ContentDirectory$ICreateObjectReceived;->onCreateObjectReceived(Lcom/samsung/upnp/control/ActionRequest;)Z

    move-result v1

    .line 672
    :goto_0
    return v1

    .line 662
    :cond_0
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    instance-of v1, v1, Lcom/samsung/api/CreateObjectReceived;

    if-eqz v1, :cond_1

    .line 663
    iget-object v1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/api/CreateObjectReceived;

    invoke-interface {v1}, Lcom/samsung/api/CreateObjectReceived;->onCreateObjectReceived()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    goto :goto_0

    .line 664
    :catch_0
    move-exception v0

    .line 665
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v2

    .line 666
    goto :goto_0

    .line 667
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 668
    .local v0, "e":Ljava/lang/Error;
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    move v1, v2

    .line 669
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Error;
    :cond_1
    move v1, v2

    .line 672
    goto :goto_0
.end method

.method private searchActionReceived(Lcom/samsung/upnp/media/server/action/SearchAction;)Z
    .locals 41
    .param p1, "action"    # Lcom/samsung/upnp/media/server/action/SearchAction;

    .prologue
    .line 1150
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/SearchAction;->getContainerID()Ljava/lang/String;

    move-result-object v14

    .line 1151
    .local v14, "contaierID":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/upnp/media/server/ContentDirectory;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v24

    .line 1152
    .local v24, "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v24, :cond_0

    .line 1153
    const/16 v3, 0x2c6

    const-string v4, "No such container"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/action/SearchAction;->setStatus(ILjava/lang/String;)V

    .line 1154
    const/4 v3, 0x0

    .line 1304
    :goto_0
    return v3

    .line 1157
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/SearchAction;->getSearchCriteria()Ljava/lang/String;

    move-result-object v35

    .line 1158
    .local v35, "searchCriteria":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSearchCriteriaList(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SearchCriteriaList;

    move-result-object v34

    .line 1159
    .local v34, "searchCriList":Lcom/samsung/upnp/media/server/object/SearchCriteriaList;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSearchCapList()Lcom/samsung/upnp/media/server/object/SearchCapList;

    move-result-object v33

    .line 1161
    .local v33, "searchCapList":Lcom/samsung/upnp/media/server/object/SearchCapList;
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 1164
    const/16 v3, 0x2c4

    const-string v4, "invalid search criteria"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/action/SearchAction;->setStatus(ILjava/lang/String;)V

    .line 1165
    const/4 v3, 0x0

    goto :goto_0

    .line 1169
    :cond_1
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_1
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;->size()I

    move-result v3

    move/from16 v0, v20

    if-lt v0, v3, :cond_2

    .line 1178
    new-instance v15, Lcom/samsung/upnp/media/server/object/ContentNodeList;

    invoke-direct {v15}, Lcom/samsung/upnp/media/server/object/ContentNodeList;-><init>()V

    .line 1179
    .local v15, "contentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/upnp/media/server/object/ContentNode;->getNContentNodes()I

    move-result v23

    .line 1180
    .local v23, "nChildNodes":I
    const/16 v20, 0x0

    :goto_2
    move/from16 v0, v20

    move/from16 v1, v23

    if-lt v0, v1, :cond_4

    .line 1185
    invoke-virtual {v15}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->size()I

    move-result v23

    .line 1188
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/SearchAction;->getSortCriteria()Ljava/lang/String;

    move-result-object v36

    .line 1189
    .local v36, "sortCriteria":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v15, v1}, Lcom/samsung/upnp/media/server/ContentDirectory;->sortContentNodeList(Lcom/samsung/upnp/media/server/object/ContentNodeList;Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNodeList;

    move-result-object v37

    .line 1190
    .local v37, "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    if-nez v37, :cond_6

    .line 1192
    const/16 v3, 0x2c5

    const-string v4, "invalid sort criteria"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/action/SearchAction;->setStatus(ILjava/lang/String;)V

    .line 1193
    const/4 v3, 0x0

    goto :goto_0

    .line 1170
    .end local v15    # "contentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    .end local v23    # "nChildNodes":I
    .end local v36    # "sortCriteria":Ljava/lang/String;
    .end local v37    # "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    :cond_2
    move-object/from16 v0, v34

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/SearchCriteriaList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/upnp/media/server/object/SearchCriteria;

    invoke-virtual {v3}, Lcom/samsung/upnp/media/server/object/SearchCriteria;->getProperty()Ljava/lang/String;

    move-result-object v27

    .line 1171
    .local v27, "propName":Ljava/lang/String;
    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/SearchCapList;->getSearchCap(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SearchCap;

    move-result-object v3

    if-nez v3, :cond_3

    .line 1173
    const/16 v3, 0x2c4

    const-string v4, "invalid search criteria"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/media/server/action/SearchAction;->setStatus(ILjava/lang/String;)V

    .line 1174
    const/4 v3, 0x0

    goto :goto_0

    .line 1169
    :cond_3
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 1181
    .end local v27    # "propName":Ljava/lang/String;
    .restart local v15    # "contentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    .restart local v23    # "nChildNodes":I
    :cond_4
    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ContentNode;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v13

    .line 1182
    .local v13, "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-eqz v13, :cond_5

    .line 1183
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v33

    invoke-direct {v0, v13, v1, v2, v15}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSearchContentList(Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/SearchCriteriaList;Lcom/samsung/upnp/media/server/object/SearchCapList;Lcom/samsung/upnp/media/server/object/ContentNodeList;)I

    .line 1180
    :cond_5
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 1196
    .end local v13    # "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    .restart local v36    # "sortCriteria":Ljava/lang/String;
    .restart local v37    # "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    :cond_6
    const/16 v38, 0x0

    .line 1197
    .local v38, "startingIndex":I
    const/16 v29, 0x0

    .line 1200
    .local v29, "requestedCount":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/SearchAction;->getStartingIndex()I

    move-result v38

    .line 1201
    if-gtz v38, :cond_7

    .line 1202
    const/16 v38, 0x0

    .line 1203
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/SearchAction;->getRequestedCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v29

    .line 1204
    if-nez v29, :cond_8

    .line 1205
    move/from16 v29, v23

    .line 1211
    :cond_8
    new-instance v16, Lcom/samsung/upnp/media/server/object/DIDLLite;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/upnp/media/server/object/DIDLLite;-><init>()V

    .line 1212
    .local v16, "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    const/16 v25, 0x0

    .line 1213
    .local v25, "numberReturned":I
    move/from16 v20, v38

    :goto_3
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_9

    move/from16 v0, v25

    move/from16 v1, v29

    if-lt v0, v1, :cond_a

    .line 1298
    :cond_9
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/upnp/media/server/object/DIDLLite;->toString()Ljava/lang/String;

    move-result-object v32

    .line 1299
    .local v32, "result":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/action/SearchAction;->setResult(Ljava/lang/String;)V

    .line 1300
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/action/SearchAction;->setNumberReturned(I)V

    .line 1301
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/action/SearchAction;->setTotalMaches(I)V

    .line 1302
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSystemUpdateID()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/action/SearchAction;->setUpdateID(I)V

    .line 1304
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1206
    .end local v16    # "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    .end local v25    # "numberReturned":I
    .end local v32    # "result":Ljava/lang/String;
    :catch_0
    move-exception v17

    .line 1207
    .local v17, "e":Ljava/lang/Exception;
    const/16 v3, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/action/SearchAction;->setStatus(I)V

    .line 1208
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1214
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v16    # "didlLite":Lcom/samsung/upnp/media/server/object/DIDLLite;
    .restart local v25    # "numberReturned":I
    :cond_a
    move-object/from16 v0, v37

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v13

    .line 1215
    .restart local v13    # "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v13, :cond_c

    .line 1213
    :cond_b
    :goto_4
    add-int/lit8 v20, v20, 0x1

    goto :goto_3

    .line 1220
    :cond_c
    invoke-virtual {v13}, Lcom/samsung/upnp/media/server/object/ContentNode;->isItemNode()Z

    move-result v3

    if-eqz v3, :cond_f

    move-object v3, v13

    .line 1221
    check-cast v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v13}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->updateResourceUri(Ljava/lang/String;)V

    .line 1222
    invoke-virtual {v13}, Lcom/samsung/upnp/media/server/object/ContentNode;->getUPnPClass()Ljava/lang/String;

    move-result-object v3

    const-string v4, "object.item.imageItem"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    move-object v3, v13

    .line 1224
    check-cast v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v3}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getDefaultResource()Lcom/samsung/upnp/media/server/object/ResourceProperty;

    move-result-object v30

    .line 1225
    .local v30, "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    if-eqz v30, :cond_d

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->get4thfield()Ljava/lang/String;

    move-result-object v3

    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1226
    new-instance v26, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1227
    .local v26, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    move-object/from16 v0, v26

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1228
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->getMimeType()Ljava/lang/String;

    move-result-object v22

    .line 1229
    .local v22, "mime":Ljava/lang/String;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->getInputStream()Ljava/io/InputStream;

    move-result-object v21

    .line 1230
    .local v21, "is":Ljava/io/InputStream;
    if-eqz v21, :cond_d

    .line 1231
    const/4 v3, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1234
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v3

    move-object/from16 v0, v26

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v26

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sget v6, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4, v5, v6}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v28

    .line 1235
    .local v28, "protocol":Ljava/lang/String;
    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->updateProtocol(Ljava/lang/String;)V

    .line 1237
    new-instance v3, Ljava/lang/StringBuilder;

    move-object/from16 v0, v26

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 1238
    .local v39, "tempStr":Ljava/lang/String;
    const-string v3, "resolution"

    move-object/from16 v0, v30

    move-object/from16 v1, v39

    invoke-virtual {v0, v3, v1}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    :try_start_1
    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1248
    .end local v21    # "is":Ljava/io/InputStream;
    .end local v22    # "mime":Ljava/lang/String;
    .end local v26    # "option":Landroid/graphics/BitmapFactory$Options;
    .end local v28    # "protocol":Ljava/lang/String;
    .end local v30    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v39    # "tempStr":Ljava/lang/String;
    :cond_d
    :goto_5
    invoke-virtual {v13}, Lcom/samsung/upnp/media/server/object/ContentNode;->getUPnPClass()Ljava/lang/String;

    move-result-object v3

    const-string v4, "object.item.audioItem"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    move-object v3, v13

    .line 1252
    check-cast v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v3}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getAudioAlbum_id()Ljava/lang/String;

    move-result-object v11

    .line 1254
    .local v11, "albumId":Ljava/lang/String;
    if-eqz v11, :cond_f

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_f

    .line 1255
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/upnp/media/server/MediaServer;->androidContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1256
    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1255
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1257
    .local v9, "albumCur":Landroid/database/Cursor;
    if-eqz v9, :cond_f

    .line 1259
    :cond_e
    :goto_6
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_11

    .line 1271
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1280
    .end local v9    # "albumCur":Landroid/database/Cursor;
    .end local v11    # "albumId":Ljava/lang/String;
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/media/server/action/SearchAction;->getFilter()Ljava/lang/String;

    move-result-object v19

    .line 1281
    .local v19, "filterCondition":Ljava/lang/String;
    const-string v3, "*"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string v3, ""

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 1283
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/upnp/media/server/ContentDirectory;->filter:Lcom/samsung/upnp/media/server/object/Filter;

    move-object/from16 v0, v19

    invoke-virtual {v3, v13, v0}, Lcom/samsung/upnp/media/server/object/Filter;->getContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v13

    .line 1284
    if-eqz v13, :cond_b

    .line 1288
    :cond_10
    const-string v3, "Filter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "search "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/util/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lcom/samsung/upnp/media/server/object/DIDLLite;->addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 1290
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_4

    .line 1260
    .end local v19    # "filterCondition":Ljava/lang/String;
    .restart local v9    # "albumCur":Landroid/database/Cursor;
    .restart local v11    # "albumId":Ljava/lang/String;
    :cond_11
    const-string v3, "album_art"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1261
    .local v10, "albumData":Ljava/lang/String;
    if-eqz v10, :cond_e

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_e

    .line 1262
    move-object/from16 v18, v10

    .line 1263
    .local v18, "filePath":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/upnp/media/server/object/ContentNode;->getID()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/upnp/media/server/ContentDirectory;->getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 1264
    .local v40, "url":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v3

    const-string v4, "image/jpeg"

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget v7, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_ORIGIN:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v12

    .line 1265
    .local v12, "albumartProtocol":Ljava/lang/String;
    new-instance v31, Lcom/samsung/upnp/media/server/object/item/sat/DOAAlbumartProperty;

    move-object/from16 v0, v31

    move-object/from16 v1, v40

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v12, v2}, Lcom/samsung/upnp/media/server/object/item/sat/DOAAlbumartProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .local v31, "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    move-object v3, v13

    .line 1266
    check-cast v3, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    move-object/from16 v0, v31

    invoke-virtual {v3, v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 1268
    const-string v3, "upnp:albumArtURI"

    move-object/from16 v0, v40

    invoke-virtual {v13, v3, v0}, Lcom/samsung/upnp/media/server/object/ContentNode;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1241
    .end local v9    # "albumCur":Landroid/database/Cursor;
    .end local v10    # "albumData":Ljava/lang/String;
    .end local v11    # "albumId":Ljava/lang/String;
    .end local v12    # "albumartProtocol":Ljava/lang/String;
    .end local v18    # "filePath":Ljava/lang/String;
    .end local v31    # "resProp":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v40    # "url":Ljava/lang/String;
    .restart local v21    # "is":Ljava/io/InputStream;
    .restart local v22    # "mime":Ljava/lang/String;
    .restart local v26    # "option":Landroid/graphics/BitmapFactory$Options;
    .restart local v28    # "protocol":Ljava/lang/String;
    .restart local v30    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .restart local v39    # "tempStr":Ljava/lang/String;
    :catch_1
    move-exception v3

    goto/16 :goto_5
.end method

.method private setMediaServer(Lcom/samsung/upnp/media/server/MediaServer;)V
    .locals 0
    .param p1, "mserver"    # Lcom/samsung/upnp/media/server/MediaServer;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->mediaServer:Lcom/samsung/upnp/media/server/MediaServer;

    .line 197
    return-void
.end method

.method public static setUploadPath(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1324
    if-eqz p0, :cond_0

    const-string v2, ""

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1325
    :cond_0
    sget-object v2, Lcom/samsung/upnp/media/server/ContentDirectory;->DEFAULT_PATH:Ljava/lang/String;

    sput-object v2, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    .line 1338
    :goto_0
    return v1

    .line 1329
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1330
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1331
    :cond_2
    sget-object v2, Lcom/samsung/upnp/media/server/ContentDirectory;->DEFAULT_PATH:Ljava/lang/String;

    sput-object v2, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    goto :goto_0

    .line 1335
    :cond_3
    sput-object p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    .line 1336
    sget-object v1, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1337
    sget-object v1, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    .line 1338
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private sortContentNodeList(Lcom/samsung/upnp/media/server/object/ContentNodeList;Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNodeList;
    .locals 13
    .param p1, "contentNodeList"    # Lcom/samsung/upnp/media/server/object/ContentNodeList;
    .param p2, "sortCriteria"    # Ljava/lang/String;

    .prologue
    const/16 v12, 0x2d

    .line 830
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    if-gtz v10, :cond_2

    :cond_0
    move-object v9, p1

    .line 860
    :cond_1
    :goto_0
    return-object v9

    .line 833
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->size()I

    move-result v4

    .line 834
    .local v4, "nChildNodes":I
    new-array v1, v4, [Lcom/samsung/upnp/media/server/object/ContentNode;

    .line 835
    .local v1, "conNode":[Lcom/samsung/upnp/media/server/object/ContentNode;
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    if-lt v3, v4, :cond_3

    .line 838
    invoke-direct {p0, p2}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSortCriteriaArray(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SortCriterionList;

    move-result-object v7

    .line 839
    .local v7, "sortCritList":Lcom/samsung/upnp/media/server/object/SortCriterionList;
    invoke-virtual {v7}, Lcom/samsung/upnp/media/server/object/SortCriterionList;->size()I

    move-result v5

    .line 840
    .local v5, "nSortCrit":I
    const/4 v3, 0x0

    :goto_2
    if-lt v3, v5, :cond_4

    .line 857
    new-instance v9, Lcom/samsung/upnp/media/server/object/ContentNodeList;

    invoke-direct {v9}, Lcom/samsung/upnp/media/server/object/ContentNodeList;-><init>()V

    .line 858
    .local v9, "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v4, :cond_1

    .line 859
    aget-object v10, v1, v3

    invoke-virtual {v9, v10}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->add(Ljava/lang/Object;)Z

    .line 858
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 836
    .end local v5    # "nSortCrit":I
    .end local v7    # "sortCritList":Lcom/samsung/upnp/media/server/object/SortCriterionList;
    .end local v9    # "sortedContentNodeList":Lcom/samsung/upnp/media/server/object/ContentNodeList;
    :cond_3
    invoke-virtual {p1, v3}, Lcom/samsung/upnp/media/server/object/ContentNodeList;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v10

    aput-object v10, v1, v3

    .line 835
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 841
    .restart local v5    # "nSortCrit":I
    .restart local v7    # "sortCritList":Lcom/samsung/upnp/media/server/object/SortCriterionList;
    :cond_4
    invoke-virtual {v7, v3}, Lcom/samsung/upnp/media/server/object/SortCriterionList;->getSortCriterion(I)Ljava/lang/String;

    move-result-object v8

    .line 842
    .local v8, "sortStr":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/util/Debugs;->info(Ljava/lang/String;)V

    .line 843
    const/4 v0, 0x1

    .line 844
    .local v0, "ascSeq":Z
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 845
    .local v2, "firstSortChar":C
    if-ne v2, v12, :cond_5

    .line 846
    const/4 v0, 0x0

    .line 847
    :cond_5
    const/16 v10, 0x2b

    if-eq v2, v10, :cond_6

    if-ne v2, v12, :cond_7

    .line 848
    :cond_6
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 849
    :cond_7
    invoke-virtual {p0, v8}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSortCap(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SortCap;

    move-result-object v6

    .line 850
    .local v6, "sortCap":Lcom/samsung/upnp/media/server/object/SortCap;
    if-nez v6, :cond_8

    .line 851
    const/4 v9, 0x0

    goto :goto_0

    .line 852
    :cond_8
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "  ascSeq = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/util/Debugs;->info(Ljava/lang/String;)V

    .line 853
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "  sortCap = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Lcom/samsung/upnp/media/server/object/SortCap;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/util/Debugs;->info(Ljava/lang/String;)V

    .line 854
    invoke-direct {p0, v1, v6, v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->sortContentNodeList([Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/SortCap;Z)V

    .line 840
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2
.end method

.method private sortContentNodeList([Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/SortCap;Z)V
    .locals 8
    .param p1, "conNode"    # [Lcom/samsung/upnp/media/server/object/ContentNode;
    .param p2, "sortCap"    # Lcom/samsung/upnp/media/server/object/SortCap;
    .param p3, "ascSeq"    # Z

    .prologue
    .line 801
    array-length v4, p1

    .line 802
    .local v4, "nConNode":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    add-int/lit8 v6, v4, -0x1

    if-lt v2, v6, :cond_0

    .line 815
    return-void

    .line 803
    :cond_0
    move v5, v2

    .line 804
    .local v5, "selIdx":I
    add-int/lit8 v3, v2, 0x1

    .local v3, "j":I
    :goto_1
    if-lt v3, v4, :cond_1

    .line 811
    aget-object v1, p1, v2

    .line 812
    .local v1, "conTmp":Lcom/samsung/upnp/media/server/object/ContentNode;
    aget-object v6, p1, v5

    aput-object v6, p1, v2

    .line 813
    aput-object v1, p1, v5

    .line 802
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 805
    .end local v1    # "conTmp":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_1
    aget-object v6, p1, v5

    aget-object v7, p1, v3

    invoke-interface {p2, v6, v7}, Lcom/samsung/upnp/media/server/object/SortCap;->compare(Lcom/samsung/upnp/media/server/object/ContentNode;Lcom/samsung/upnp/media/server/object/ContentNode;)I

    move-result v0

    .line 806
    .local v0, "cmpRet":I
    if-eqz p3, :cond_2

    if-gez v0, :cond_2

    .line 807
    move v5, v3

    .line 808
    :cond_2
    if-nez p3, :cond_3

    if-lez v0, :cond_3

    .line 809
    move v5, v3

    .line 804
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public actionControlReceived(Lcom/samsung/upnp/Action;)Z
    .locals 14
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 572
    invoke-virtual {p1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 574
    .local v0, "actionName":Ljava/lang/String;
    const-string v12, "Browse"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 575
    new-instance v1, Lcom/samsung/upnp/media/server/action/BrowseAction;

    invoke-direct {v1, p1}, Lcom/samsung/upnp/media/server/action/BrowseAction;-><init>(Lcom/samsung/upnp/Action;)V

    .line 576
    .local v1, "browseAct":Lcom/samsung/upnp/media/server/action/BrowseAction;
    invoke-direct {p0, v1}, Lcom/samsung/upnp/media/server/ContentDirectory;->browseActionReceived(Lcom/samsung/upnp/media/server/action/BrowseAction;)Z

    move-result v12

    .line 640
    .end local v1    # "browseAct":Lcom/samsung/upnp/media/server/action/BrowseAction;
    :goto_0
    return v12

    .line 579
    :cond_0
    const-string v12, "Search"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 580
    new-instance v7, Lcom/samsung/upnp/media/server/action/SearchAction;

    invoke-direct {v7, p1}, Lcom/samsung/upnp/media/server/action/SearchAction;-><init>(Lcom/samsung/upnp/Action;)V

    .line 581
    .local v7, "searchAct":Lcom/samsung/upnp/media/server/action/SearchAction;
    invoke-direct {p0, v7}, Lcom/samsung/upnp/media/server/ContentDirectory;->searchActionReceived(Lcom/samsung/upnp/media/server/action/SearchAction;)Z

    move-result v12

    goto :goto_0

    .line 585
    .end local v7    # "searchAct":Lcom/samsung/upnp/media/server/action/SearchAction;
    :cond_1
    const-string v12, "GetSearchCapabilities"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 586
    const-string v12, "SearchCaps"

    invoke-virtual {p1, v12}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v8

    .line 587
    .local v8, "searchCapsArg":Lcom/samsung/upnp/Argument;
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSearchCapabilities()Ljava/lang/String;

    move-result-object v9

    .line 588
    .local v9, "searchCapsStr":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 589
    invoke-virtual {v8, v9}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 590
    :cond_2
    const/4 v12, 0x1

    goto :goto_0

    .line 594
    .end local v8    # "searchCapsArg":Lcom/samsung/upnp/Argument;
    .end local v9    # "searchCapsStr":Ljava/lang/String;
    :cond_3
    const-string v12, "GetSortCapabilities"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 595
    const-string v12, "SortCaps"

    invoke-virtual {p1, v12}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 596
    .local v10, "sortCapsArg":Lcom/samsung/upnp/Argument;
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSortCapabilities()Ljava/lang/String;

    move-result-object v11

    .line 597
    .local v11, "sortCapsStr":Ljava/lang/String;
    if-eqz v10, :cond_4

    .line 598
    invoke-virtual {v10, v11}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 599
    :cond_4
    const/4 v12, 0x1

    goto :goto_0

    .line 602
    .end local v10    # "sortCapsArg":Lcom/samsung/upnp/Argument;
    .end local v11    # "sortCapsStr":Ljava/lang/String;
    :cond_5
    const-string v12, "GetSystemUpdateID"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 603
    const-string v12, "Id"

    invoke-virtual {p1, v12}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v4

    .line 604
    .local v4, "idArg":Lcom/samsung/upnp/Argument;
    if-eqz v4, :cond_6

    .line 605
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getSystemUpdateID()I

    move-result v12

    invoke-virtual {v4, v12}, Lcom/samsung/upnp/Argument;->setValue(I)V

    .line 606
    :cond_6
    const/4 v12, 0x1

    goto :goto_0

    .line 609
    .end local v4    # "idArg":Lcom/samsung/upnp/Argument;
    :cond_7
    const-string v12, "CreateObject"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 610
    invoke-virtual {p1}, Lcom/samsung/upnp/Action;->getActionRequest()Lcom/samsung/upnp/control/ActionRequest;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/samsung/upnp/media/server/ContentDirectory;->notifyCreateObjectReceived(Lcom/samsung/upnp/control/ActionRequest;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 611
    const/4 v12, 0x0

    goto :goto_0

    .line 613
    :cond_8
    new-instance v2, Lcom/samsung/upnp/media/server/action/CreateObjectAction;

    invoke-direct {v2, p1}, Lcom/samsung/upnp/media/server/action/CreateObjectAction;-><init>(Lcom/samsung/upnp/Action;)V

    .line 614
    .local v2, "createObjAct":Lcom/samsung/upnp/media/server/action/CreateObjectAction;
    invoke-direct {p0, v2}, Lcom/samsung/upnp/media/server/ContentDirectory;->createActionReceived(Lcom/samsung/upnp/media/server/action/CreateObjectAction;)Z

    move-result v12

    goto :goto_0

    .line 618
    .end local v2    # "createObjAct":Lcom/samsung/upnp/media/server/action/CreateObjectAction;
    :cond_9
    const-string v12, "DestroyObject"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 619
    const-string v12, "ObjectID"

    invoke-virtual {p1, v12}, Lcom/samsung/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 620
    .local v3, "id":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/media/server/ContentDirectory;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v5

    .line 621
    .local v5, "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v5, :cond_a

    .line 622
    const/16 v12, 0x2bd

    const-string v13, "No such object"

    invoke-virtual {p1, v12, v13}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 623
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 625
    :cond_a
    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/object/ContentNode;->getRestricted()I

    move-result v12

    if-eqz v12, :cond_b

    .line 626
    const/16 v12, 0x2c7

    const-string v13, "Restricted object"

    invoke-virtual {p1, v12, v13}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 627
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 630
    :cond_b
    iget-object v12, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v12, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 632
    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/object/ContentNode;->getParentNode()Lcom/samsung/xml/Node;

    move-result-object v12

    invoke-virtual {v12, v5}, Lcom/samsung/xml/Node;->removeNode(Lcom/samsung/xml/Node;)Z

    .line 633
    instance-of v12, v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    if-eqz v12, :cond_c

    .line 634
    check-cast v5, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .end local v5    # "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    invoke-virtual {v5}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->getDefaultResource()Lcom/samsung/upnp/media/server/object/ResourceProperty;

    move-result-object v6

    .line 635
    .local v6, "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    if-eqz v6, :cond_c

    .line 636
    invoke-virtual {v6}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->deleteContent()V

    .line 638
    .end local v6    # "res":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    :cond_c
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 640
    .end local v3    # "id":Ljava/lang/String;
    :cond_d
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public addDirectory(Lcom/samsung/upnp/media/server/Directory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/Directory;

    .prologue
    .line 476
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dirList:Lcom/samsung/upnp/media/server/DirectoryList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/DirectoryList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const/4 v0, 0x0

    .line 487
    :goto_0
    return v0

    .line 479
    :cond_0
    invoke-virtual {p1, p0}, Lcom/samsung/upnp/media/server/Directory;->setContentDirectory(Lcom/samsung/upnp/media/server/ContentDirectory;)V

    .line 480
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dirList:Lcom/samsung/upnp/media/server/DirectoryList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/DirectoryList;->add(Ljava/lang/Object;)Z

    .line 481
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->rootNode:Lcom/samsung/upnp/media/server/object/container/RootNode;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/container/RootNode;->addContentNode(Lcom/samsung/upnp/media/server/object/ContentNode;)V

    .line 482
    invoke-virtual {p1}, Lcom/samsung/upnp/media/server/Directory;->update()V

    .line 485
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->updateSystemUpdateID()V

    .line 487
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addPlugIn(Lcom/samsung/upnp/media/server/object/Format;)Z
    .locals 1
    .param p1, "format"    # Lcom/samsung/upnp/media/server/object/Format;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->formatList:Lcom/samsung/upnp/media/server/object/FormatList;

    if-nez v0, :cond_0

    .line 310
    const/4 v0, 0x0

    .line 314
    :goto_0
    return v0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->formatList:Lcom/samsung/upnp/media/server/object/FormatList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/FormatList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->formatList:Lcom/samsung/upnp/media/server/object/FormatList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/FormatList;->add(Ljava/lang/Object;)Z

    .line 314
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addSearchCap(Lcom/samsung/upnp/media/server/object/SearchCap;)Z
    .locals 1
    .param p1, "searchCap"    # Lcom/samsung/upnp/media/server/object/SearchCap;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->searchCapList:Lcom/samsung/upnp/media/server/object/SearchCapList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/SearchCapList;->add(Ljava/lang/Object;)Z

    .line 400
    const/4 v0, 0x1

    return v0
.end method

.method public addSortCap(Lcom/samsung/upnp/media/server/object/SortCap;)Z
    .locals 1
    .param p1, "sortCap"    # Lcom/samsung/upnp/media/server/object/SortCap;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->sortCapList:Lcom/samsung/upnp/media/server/object/SortCapList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/SortCapList;->add(Ljava/lang/Object;)Z

    .line 347
    const/4 v0, 0x1

    return v0
.end method

.method public contentExportRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 14
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 1423
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v11

    .line 1425
    .local v11, "uri":Ljava/lang/String;
    const-string v12, "getCaptionInfo.sec"

    invoke-virtual {p1, v12}, Lcom/samsung/http/HTTPRequest;->getHeader(Ljava/lang/String;)Lcom/samsung/http/HTTPHeader;

    move-result-object v0

    .line 1426
    .local v0, "capHeader":Lcom/samsung/http/HTTPHeader;
    if-nez v0, :cond_1

    .line 1428
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v1

    .line 1429
    .local v1, "header":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1430
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v12

    const-string v13, "HOST"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 1431
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    .line 1482
    .end local v1    # "header":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1437
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getParameterList()Lcom/samsung/http/ParameterList;

    move-result-object v8

    .line 1438
    .local v8, "paramList":Lcom/samsung/http/ParameterList;
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_1
    invoke-virtual {v8}, Lcom/samsung/http/ParameterList;->size()I

    move-result v12

    if-lt v5, v12, :cond_3

    .line 1447
    const-string v12, "id"

    invoke-virtual {v8, v12}, Lcom/samsung/http/ParameterList;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1448
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 1449
    new-instance v10, Ljava/util/StringTokenizer;

    const-string v12, "."

    invoke-direct {v10, v3, v12}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    .local v10, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1451
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 1458
    .end local v10    # "st":Ljava/util/StringTokenizer;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/media/server/ContentDirectory;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v6

    .line 1459
    .local v6, "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    if-nez v6, :cond_4

    .line 1460
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0

    .line 1439
    .end local v3    # "id":Ljava/lang/String;
    .end local v6    # "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_3
    invoke-virtual {v8, v5}, Lcom/samsung/http/ParameterList;->getParameter(I)Lcom/samsung/http/Parameter;

    move-result-object v7

    .line 1440
    .local v7, "param":Lcom/samsung/http/Parameter;
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "["

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/samsung/http/Parameter;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Lcom/samsung/http/Parameter;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/util/Debugs;->info(Ljava/lang/String;)V

    .line 1438
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1463
    .end local v7    # "param":Lcom/samsung/http/Parameter;
    .restart local v3    # "id":Ljava/lang/String;
    .restart local v6    # "node":Lcom/samsung/upnp/media/server/object/ContentNode;
    :cond_4
    instance-of v12, v6, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    if-nez v12, :cond_5

    .line 1464
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0

    :cond_5
    move-object v4, v6

    .line 1472
    check-cast v4, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .line 1473
    .local v4, "itemNode":Lcom/samsung/upnp/media/server/object/item/ItemNode;
    invoke-static {p1, v4}, Lcom/samsung/upnp/media/server/CDSResponseBuilder;->buildResponse(Lcom/samsung/http/HTTPRequest;Lcom/samsung/upnp/media/server/object/item/ItemNode;)Lcom/samsung/http/HTTPResponse;

    move-result-object v2

    .line 1474
    .local v2, "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {p1, v2}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    move-result v9

    .line 1475
    .local v9, "result":Z
    if-nez v9, :cond_0

    .line 1476
    const-string v12, "HTTPSocket"

    const-string v13, "CDS : Socket close"

    invoke-static {v12, v13}, Lcom/samsung/util/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getSocket()Lcom/samsung/http/HTTPSocket;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/http/HTTPSocket;->close()Z

    goto :goto_0
.end method

.method public contentImportProgressRecieved(Ljava/lang/String;)V
    .locals 6
    .param p1, "firstLine"    # Ljava/lang/String;

    .prologue
    .line 1403
    const-string v4, "id="

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1404
    .local v2, "pos":I
    add-int/lit8 v4, v2, 0x3

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 1405
    const/4 v3, 0x0

    .line 1406
    .local v3, "st":Ljava/util/StringTokenizer;
    const/16 v4, 0x3f

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-lez v4, :cond_2

    .line 1407
    new-instance v3, Ljava/util/StringTokenizer;

    .end local v3    # "st":Ljava/util/StringTokenizer;
    const-string v4, "&"

    invoke-direct {v3, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    .restart local v3    # "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1412
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 1413
    .local v0, "id":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1418
    .end local v0    # "id":Ljava/lang/String;
    :cond_1
    return-void

    .line 1409
    :cond_2
    new-instance v3, Ljava/util/StringTokenizer;

    .end local v3    # "st":Ljava/util/StringTokenizer;
    invoke-direct {v3, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .restart local v3    # "st":Ljava/util/StringTokenizer;
    goto :goto_0

    .line 1413
    .restart local v0    # "id":Ljava/lang/String;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;

    .line 1414
    .local v1, "item":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1415
    invoke-virtual {v1}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->rebirth()V

    goto :goto_1
.end method

.method public contentImportRequestRecieved(Lcom/samsung/http/HTTPRequest;)Z
    .locals 11
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    const/4 v8, 0x0

    .line 1346
    if-nez p1, :cond_0

    .line 1399
    :goto_0
    return v8

    .line 1349
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getParameterList()Lcom/samsung/http/ParameterList;

    move-result-object v4

    .line 1351
    .local v4, "paramList":Lcom/samsung/http/ParameterList;
    const-string v9, "id"

    invoke-virtual {v4, v9}, Lcom/samsung/http/ParameterList;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1353
    .local v0, "id":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1354
    .local v1, "importItem":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    iget-object v9, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v9}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_3

    .line 1364
    :goto_1
    if-nez v1, :cond_4

    .line 1365
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->hasFileContent()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1366
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getFileContent()Ljava/io/File;

    move-result-object v7

    .line 1367
    .local v7, "tempFile":Ljava/io/File;
    if-eqz v7, :cond_2

    .line 1368
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 1371
    .end local v7    # "tempFile":Ljava/io/File;
    :cond_2
    const/4 v9, 0x0

    invoke-direct {p0, v9, v8}, Lcom/samsung/upnp/media/server/ContentDirectory;->notifiyUploadContentFinished(Lcom/samsung/upnp/media/server/object/item/ItemNode;Z)V

    .line 1372
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    move-result v8

    goto :goto_0

    .line 1354
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;

    .line 1355
    .local v2, "item":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    invoke-virtual {v2}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getID()Ljava/lang/String;

    move-result-object v3

    .line 1356
    .local v3, "itemID":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 1357
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1358
    move-object v1, v2

    .line 1359
    goto :goto_1

    .line 1375
    .end local v2    # "item":Lcom/samsung/upnp/media/server/object/item/ImportItemNode;
    .end local v3    # "itemID":Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->importItemNodeList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v8, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1376
    const-string v8, "res"

    invoke-virtual {v1, v8}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->getProperty(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentProperty;

    move-result-object v5

    .line 1377
    .local v5, "prop":Lcom/samsung/upnp/media/server/object/ContentProperty;
    const/4 v6, 0x0

    .line 1389
    .local v6, "res":Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->hasFileContent()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1390
    sget-object v8, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getFileContent()Ljava/io/File;

    move-result-object v9

    invoke-static {v8, v1, v5, v9}, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->createImportResourceProperty(Ljava/lang/String;Lcom/samsung/upnp/media/server/object/item/ImportItemNode;Lcom/samsung/upnp/media/server/object/ContentProperty;Ljava/io/File;)Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;

    move-result-object v6

    .line 1395
    :goto_2
    invoke-virtual {v1, v5}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->removeProperty(Lcom/samsung/upnp/media/server/object/ContentProperty;)Z

    .line 1396
    invoke-virtual {v1, v6}, Lcom/samsung/upnp/media/server/object/item/ImportItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 1398
    const/4 v8, 0x1

    invoke-direct {p0, v1, v8}, Lcom/samsung/upnp/media/server/ContentDirectory;->notifiyUploadContentFinished(Lcom/samsung/upnp/media/server/object/item/ItemNode;Z)V

    .line 1399
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnOK()Z

    move-result v8

    goto :goto_0

    .line 1392
    :cond_5
    sget-object v8, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadPath:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getContent()Ljava/io/InputStream;

    move-result-object v9

    invoke-static {v8, v1, v5, v9}, Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;->createImportResourceProperty(Ljava/lang/String;Lcom/samsung/upnp/media/server/object/item/ImportItemNode;Lcom/samsung/upnp/media/server/object/ContentProperty;Ljava/io/InputStream;)Lcom/samsung/upnp/media/server/object/item/ImportResourceProperty;

    move-result-object v6

    goto :goto_2
.end method

.method public findContainerNodeByUPnPClass(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;
    .locals 5
    .param p1, "upnpClass"    # Ljava/lang/String;

    .prologue
    .line 544
    const/4 v3, -0x1

    .line 545
    .local v3, "target":I
    const-string v4, "object.item.video"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 546
    const/4 v3, 0x2

    .line 547
    :cond_0
    const-string v4, "object.item.image"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 548
    const/4 v3, 0x0

    .line 549
    :cond_1
    const-string v4, "object.item.audio"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 550
    const/4 v3, 0x1

    .line 552
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getRootNode()Lcom/samsung/upnp/media/server/object/container/RootNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/upnp/media/server/object/container/RootNode;->getNContentNodes()I

    move-result v2

    .line 553
    .local v2, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_4

    .line 561
    const/4 v0, 0x0

    :cond_3
    return-object v0

    .line 554
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getRootNode()Lcom/samsung/upnp/media/server/object/container/RootNode;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/samsung/upnp/media/server/object/container/RootNode;->getContentNode(I)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v0

    .line 555
    .local v0, "cnode":Lcom/samsung/upnp/media/server/object/ContentNode;
    instance-of v4, v0, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    if-eqz v4, :cond_5

    move-object v4, v0

    .line 556
    check-cast v4, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;

    invoke-virtual {v4}, Lcom/samsung/upnp/media/server/directory/doa/DOACategoryDirectory;->getType()I

    move-result v4

    if-eq v4, v3, :cond_3

    .line 553
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getRootNode()Lcom/samsung/upnp/media/server/object/container/RootNode;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/container/RootNode;->findContentNodeByID(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/ContentNode;

    move-result-object v0

    return-object v0
.end method

.method public getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1522
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getHTTPPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/MediaStore/AlbumArt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1500
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getHTTPPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/MediaStore/Out"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentImportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1514
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getHTTPPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/MediaStore/In"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentMicroThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1509
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getHTTPPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/MediaStore/Micro"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentSmallThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1505
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getHTTPPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/MediaStore/Small"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentSubtitleExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1518
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getHTTPPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/subtitle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".smi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHTTPPort()I
    .locals 1

    .prologue
    .line 1495
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->getHTTPPort()I

    move-result v0

    return v0
.end method

.method public getInterfaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1490
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/MediaServer;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->mediaServer:Lcom/samsung/upnp/media/server/MediaServer;

    return-object v0
.end method

.method public getNSearchCaps()I
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->searchCapList:Lcom/samsung/upnp/media/server/object/SearchCapList;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/SearchCapList;->size()I

    move-result v0

    return v0
.end method

.method public getNSortCaps()I
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->sortCapList:Lcom/samsung/upnp/media/server/object/SortCapList;

    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/object/SortCapList;->size()I

    move-result v0

    return v0
.end method

.method public getNextContainerID()I
    .locals 1

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getNextContentID()I

    move-result v0

    return v0
.end method

.method public getNextItemID()I
    .locals 1

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getNextContentID()I

    move-result v0

    return v0
.end method

.method public getRootNode()Lcom/samsung/upnp/media/server/object/container/RootNode;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->rootNode:Lcom/samsung/upnp/media/server/object/container/RootNode;

    return-object v0
.end method

.method public getSearchCapList()Lcom/samsung/upnp/media/server/object/SearchCapList;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->searchCapList:Lcom/samsung/upnp/media/server/object/SearchCapList;

    return-object v0
.end method

.method public getSortCap(I)Lcom/samsung/upnp/media/server/object/SortCap;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 357
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->sortCapList:Lcom/samsung/upnp/media/server/object/SortCapList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/SortCapList;->getSortCap(I)Lcom/samsung/upnp/media/server/object/SortCap;

    move-result-object v0

    return-object v0
.end method

.method public getSortCap(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SortCap;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 362
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->sortCapList:Lcom/samsung/upnp/media/server/object/SortCapList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/SortCapList;->getSortCap(Ljava/lang/String;)Lcom/samsung/upnp/media/server/object/SortCap;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getSystemUpdateID()I
    .locals 1

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->systemUpdateID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSystemUpdateIDInterval()J
    .locals 2

    .prologue
    .line 1542
    iget-wide v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->systemUpdateIDInterval:J

    return-wide v0
.end method

.method public isContainedDirectory(Lcom/samsung/upnp/media/server/Directory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/Directory;

    .prologue
    .line 527
    if-nez p1, :cond_0

    .line 528
    const/4 v0, 0x0

    .line 530
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dirList:Lcom/samsung/upnp/media/server/DirectoryList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/DirectoryList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public queryControlReceived(Lcom/samsung/upnp/StateVariable;)Z
    .locals 1
    .param p1, "stateVar"    # Lcom/samsung/upnp/StateVariable;

    .prologue
    .line 1313
    const/4 v0, 0x0

    return v0
.end method

.method public removeDirectory(Lcom/samsung/upnp/media/server/Directory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/Directory;

    .prologue
    .line 491
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dirList:Lcom/samsung/upnp/media/server/DirectoryList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/DirectoryList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->dirList:Lcom/samsung/upnp/media/server/DirectoryList;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/DirectoryList;->remove(Ljava/lang/Object;)Z

    .line 494
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->rootNode:Lcom/samsung/upnp/media/server/object/container/RootNode;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/object/container/RootNode;->removeNode(Lcom/samsung/xml/Node;)Z

    .line 495
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->updateSystemUpdateID()V

    .line 496
    const/4 v0, 0x1

    .line 499
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentUpdateInterval(J)V
    .locals 0
    .param p1, "itime"    # J

    .prologue
    .line 1547
    iput-wide p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->contentUpdateInterval:J

    .line 1548
    return-void
.end method

.method public setCreateObjectReceivedListener(Ljava/lang/Object;)V
    .locals 0
    .param p1, "listener"    # Ljava/lang/Object;

    .prologue
    .line 654
    iput-object p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->createObjectListener:Ljava/lang/Object;

    .line 655
    return-void
.end method

.method public setSystemUpdateInterval(J)V
    .locals 0
    .param p1, "itime"    # J

    .prologue
    .line 1537
    iput-wide p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->systemUpdateIDInterval:J

    .line 1538
    return-void
.end method

.method public setUploadFinishedListener(Ljava/lang/Object;)V
    .locals 0
    .param p1, "listener"    # Ljava/lang/Object;

    .prologue
    .line 682
    iput-object p1, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->uploadFinishedListener:Ljava/lang/Object;

    .line 683
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1565
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "CDS Eventing"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    .line 1567
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/ContentDirectory;->getMediaServer()Lcom/samsung/upnp/media/server/MediaServer;

    move-result-object v1

    .line 1568
    .local v1, "mserver":Lcom/samsung/upnp/media/server/MediaServer;
    if-eqz v1, :cond_0

    .line 1569
    const-string v2, "SystemUpdateID"

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/media/server/MediaServer;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->varSystemUpdateID:Lcom/samsung/upnp/StateVariable;

    .line 1571
    :cond_0
    iget-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->varSystemUpdateID:Lcom/samsung/upnp/StateVariable;

    if-eqz v2, :cond_1

    .line 1572
    iget-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->varSystemUpdateID:Lcom/samsung/upnp/StateVariable;

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/StateVariable;->setValue(I)V

    .line 1578
    iput v4, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->lastSystemUpdateID:I

    .line 1579
    iget-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 1580
    iget-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 1581
    .local v0, "looper":Landroid/os/Looper;
    if-nez v0, :cond_2

    .line 1633
    .end local v0    # "looper":Landroid/os/Looper;
    :cond_1
    :goto_0
    return-void

    .line 1585
    .restart local v0    # "looper":Landroid/os/Looper;
    :cond_2
    new-instance v2, Lcom/samsung/upnp/media/server/ContentDirectory$1;

    invoke-direct {v2, p0, v0}, Lcom/samsung/upnp/media/server/ContentDirectory$1;-><init>(Lcom/samsung/upnp/media/server/ContentDirectory;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventHandler:Landroid/os/Handler;

    .line 1632
    iget-object v2, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 1636
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1637
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 1638
    :cond_0
    return-void
.end method

.method public declared-synchronized updateSystemUpdateID()V
    .locals 2

    .prologue
    .line 228
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->systemUpdateID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->systemUpdateID:I

    .line 229
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 231
    iget-object v0, p0, Lcom/samsung/upnp/media/server/ContentDirectory;->cdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    :cond_0
    monitor-exit p0

    return-void

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
