.class public Lcom/samsung/upnp/media/server/MediaServer;
.super Lcom/samsung/upnp/Device;
.source "MediaServer.java"


# static fields
.field public static final DEFAULT_HTTP_PORT:I = 0x80eb

.field public static final DEVICE_MICRO_ICON_JPEG:Ljava/lang/String; = "icon/icon.jpeg"

.field public static final DEVICE_MICRO_ICON_PNG:Ljava/lang/String; = "icon/icon.png"

.field public static final DEVICE_SMALL_ICON_JPEG:Ljava/lang/String; = "icon/icon2.jpeg"

.field public static final DEVICE_SMALL_ICON_PNG:Ljava/lang/String; = "icon/icon2.png"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "urn:schemas-upnp-org:device:MediaServer:1"

.field public static final MACROICON_JPEG:I = 0x0

.field public static final MACROICON_PNG:I = 0x1

.field public static final SMALLICON_JPEG:I = 0x2

.field public static final SMALLICON_PNG:I = 0x3

.field private static volatile mediaServerSingleton:Lcom/samsung/upnp/media/server/MediaServer;


# instance fields
.field public androidContext:Landroid/content/Context;

.field private conDir:Lcom/samsung/upnp/media/server/ContentDirectory;

.field private conMan:Lcom/samsung/upnp/media/server/ConnectionManager;

.field private ipAddress:Ljava/lang/String;

.field private isStart:Z

.field private macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

.field private macroIcon_png:Ljava/io/ByteArrayOutputStream;

.field private mutex:Ljava/lang/Object;

.field private smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

.field private smallIcon_png:Ljava/io/ByteArrayOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/upnp/media/server/MediaServer;->mediaServerSingleton:Lcom/samsung/upnp/media/server/MediaServer;

    .line 311
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Lcom/samsung/upnp/Device;-><init>()V

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/media/server/MediaServer;->mutex:Ljava/lang/Object;

    .line 72
    const-string v0, "localhost"

    iput-object v0, p0, Lcom/samsung/upnp/media/server/MediaServer;->ipAddress:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->androidContext:Landroid/content/Context;

    .line 304
    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    .line 305
    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    .line 306
    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    .line 307
    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    .line 518
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/upnp/media/server/MediaServer;->isStart:Z

    .line 78
    iput-object p1, p0, Lcom/samsung/upnp/media/server/MediaServer;->androidContext:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/samsung/upnp/media/server/object/format/DefaultFormat;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/format/DefaultFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/MediaServer;->addPlugIn(Lcom/samsung/upnp/media/server/object/Format;)Z

    .line 80
    return-void
.end method

.method public static getMediaServerInstatnce(Landroid/content/Context;)Lcom/samsung/upnp/media/server/MediaServer;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    sget-object v0, Lcom/samsung/upnp/media/server/MediaServer;->mediaServerSingleton:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v0, :cond_1

    .line 84
    const-class v1, Lcom/samsung/upnp/media/server/MediaServer;

    monitor-enter v1

    .line 85
    :try_start_0
    sget-object v0, Lcom/samsung/upnp/media/server/MediaServer;->mediaServerSingleton:Lcom/samsung/upnp/media/server/MediaServer;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/samsung/upnp/media/server/MediaServer;

    invoke-direct {v0, p0}, Lcom/samsung/upnp/media/server/MediaServer;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/upnp/media/server/MediaServer;->mediaServerSingleton:Lcom/samsung/upnp/media/server/MediaServer;

    .line 84
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_1
    sget-object v0, Lcom/samsung/upnp/media/server/MediaServer;->mediaServerSingleton:Lcom/samsung/upnp/media/server/MediaServer;

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private httpGetRequestReceived(Lcom/samsung/http/HTTPRequest;)Z
    .locals 8
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    const/16 v7, 0xc8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 409
    if-nez p1, :cond_1

    .line 511
    :cond_0
    :goto_0
    return v4

    .line 411
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v3

    .line 414
    .local v3, "uri":Ljava/lang/String;
    const-string v6, "icon/icon.png"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 415
    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    if-eqz v6, :cond_0

    .line 417
    invoke-virtual {p0, v4}, Lcom/samsung/upnp/media/server/MediaServer;->getIcon(I)Lcom/samsung/upnp/Icon;

    move-result-object v2

    .line 418
    .local v2, "icon":Lcom/samsung/upnp/Icon;
    new-instance v1, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v1}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 419
    .local v1, "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {v1, v7}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 420
    invoke-virtual {v2}, Lcom/samsung/upnp/Icon;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 421
    iget-object v4, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    int-to-long v6, v4

    invoke-virtual {v1, v6, v7}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 422
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 423
    invoke-virtual {p1, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    move v4, v5

    .line 424
    goto :goto_0

    .line 425
    .end local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v2    # "icon":Lcom/samsung/upnp/Icon;
    :cond_2
    const-string v6, "icon/icon2.png"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 426
    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    if-eqz v6, :cond_0

    .line 428
    invoke-virtual {p0, v5}, Lcom/samsung/upnp/media/server/MediaServer;->getIcon(I)Lcom/samsung/upnp/Icon;

    move-result-object v2

    .line 429
    .restart local v2    # "icon":Lcom/samsung/upnp/Icon;
    new-instance v1, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v1}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 430
    .restart local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {v1, v7}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 431
    invoke-virtual {v2}, Lcom/samsung/upnp/Icon;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 432
    iget-object v4, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    int-to-long v6, v4

    invoke-virtual {v1, v6, v7}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 433
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 434
    invoke-virtual {p1, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    move v4, v5

    .line 438
    goto :goto_0

    .line 439
    .end local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v2    # "icon":Lcom/samsung/upnp/Icon;
    :cond_3
    const-string v6, "icon/icon.jpeg"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 440
    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    if-eqz v6, :cond_0

    .line 442
    invoke-virtual {p0, v4}, Lcom/samsung/upnp/media/server/MediaServer;->getIcon(I)Lcom/samsung/upnp/Icon;

    move-result-object v2

    .line 443
    .restart local v2    # "icon":Lcom/samsung/upnp/Icon;
    new-instance v1, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v1}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 444
    .restart local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {v1, v7}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 445
    invoke-virtual {v2}, Lcom/samsung/upnp/Icon;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 446
    iget-object v4, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    int-to-long v6, v4

    invoke-virtual {v1, v6, v7}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 447
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 448
    invoke-virtual {p1, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    move v4, v5

    .line 449
    goto/16 :goto_0

    .line 450
    .end local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v2    # "icon":Lcom/samsung/upnp/Icon;
    :cond_4
    const-string v6, "icon/icon2.jpeg"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 451
    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    if-eqz v6, :cond_0

    .line 453
    invoke-virtual {p0, v5}, Lcom/samsung/upnp/media/server/MediaServer;->getIcon(I)Lcom/samsung/upnp/Icon;

    move-result-object v2

    .line 454
    .restart local v2    # "icon":Lcom/samsung/upnp/Icon;
    new-instance v1, Lcom/samsung/http/HTTPResponse;

    invoke-direct {v1}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 455
    .restart local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {v1, v7}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 456
    invoke-virtual {v2}, Lcom/samsung/upnp/Icon;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 457
    iget-object v4, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    int-to-long v6, v4

    invoke-virtual {v1, v6, v7}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 458
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v6, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v4}, Lcom/samsung/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 459
    invoke-virtual {p1, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    move v4, v5

    .line 463
    goto/16 :goto_0

    .line 464
    .end local v1    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v2    # "icon":Lcom/samsung/upnp/Icon;
    :cond_5
    const-string v6, "/MediaStore/Out"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 465
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 466
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    .line 468
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentExportRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    move v4, v5

    .line 472
    goto/16 :goto_0

    .line 473
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    :cond_6
    const-string v6, "/MediaStore/Micro"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 474
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 475
    .restart local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    .line 477
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentExportRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    move v4, v5

    .line 483
    goto/16 :goto_0

    .line 484
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    :cond_7
    const-string v6, "/MediaStore/Small"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 485
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 486
    .restart local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    .line 488
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentExportRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    move v4, v5

    .line 494
    goto/16 :goto_0

    .line 495
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    :cond_8
    const-string v6, "/subtitle"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 496
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 497
    .restart local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    .line 499
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentExportRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    move v4, v5

    .line 503
    goto/16 :goto_0

    .line 504
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    :cond_9
    const-string v6, "/MediaStore/AlbumArt"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 505
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 506
    .restart local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentExportRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    move v4, v5

    .line 509
    goto/16 :goto_0
.end method

.method private httpPostRequestReceived(Lcom/samsung/http/HTTPRequest;)Z
    .locals 5
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    const/4 v2, 0x0

    .line 394
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 395
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 405
    :cond_0
    :goto_0
    return v2

    .line 397
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "uri":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "uri = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 400
    const-string v3, "/MediaStore/In"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 401
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentImportRequestRecieved(Lcom/samsung/http/HTTPRequest;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 402
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private initializeServer()V
    .locals 3

    .prologue
    .line 158
    const/16 v2, 0x9

    invoke-static {v2}, Lcom/samsung/upnp/UPnP;->setEnable(I)V

    .line 160
    iget-object v2, p0, Lcom/samsung/upnp/media/server/MediaServer;->ipAddress:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/MediaServer;->setInterfaceAddress(Ljava/lang/String;)V

    .line 161
    const v2, 0x80eb

    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/MediaServer;->setHTTPPort(I)V

    .line 163
    new-instance v2, Lcom/samsung/upnp/media/server/ContentDirectory;

    invoke-direct {v2, p0}, Lcom/samsung/upnp/media/server/ContentDirectory;-><init>(Lcom/samsung/upnp/media/server/MediaServer;)V

    iput-object v2, p0, Lcom/samsung/upnp/media/server/MediaServer;->conDir:Lcom/samsung/upnp/media/server/ContentDirectory;

    .line 164
    new-instance v2, Lcom/samsung/upnp/media/server/ConnectionManager;

    invoke-direct {v2, p0}, Lcom/samsung/upnp/media/server/ConnectionManager;-><init>(Lcom/samsung/upnp/media/server/MediaServer;)V

    iput-object v2, p0, Lcom/samsung/upnp/media/server/MediaServer;->conMan:Lcom/samsung/upnp/media/server/ConnectionManager;

    .line 166
    const-string v2, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v0

    .line 167
    .local v0, "servConDir":Lcom/samsung/upnp/Service;
    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 169
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Service;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V

    .line 171
    :cond_0
    const-string v2, "urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual {p0, v2}, Lcom/samsung/upnp/media/server/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v1

    .line 172
    .local v1, "servConMan":Lcom/samsung/upnp/Service;
    if-eqz v1, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getConnectionManager()Lcom/samsung/upnp/media/server/ConnectionManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 174
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getConnectionManager()Lcom/samsung/upnp/media/server/ConnectionManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/upnp/Service;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V

    .line 176
    :cond_1
    return-void
.end method


# virtual methods
.method public addContentDirectory(Lcom/samsung/upnp/media/server/Directory;)Z
    .locals 2
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/Directory;

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 212
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 213
    :cond_0
    const/4 v1, 0x0

    .line 214
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->addDirectory(Lcom/samsung/upnp/media/server/Directory;)Z

    move-result v1

    goto :goto_0
.end method

.method public addPlugIn(Lcom/samsung/upnp/media/server/object/Format;)Z
    .locals 2
    .param p1, "format"    # Lcom/samsung/upnp/media/server/object/Format;

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 265
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 266
    :cond_0
    const/4 v1, 0x0

    .line 267
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->addPlugIn(Lcom/samsung/upnp/media/server/object/Format;)Z

    move-result v1

    goto :goto_0
.end method

.method public getConnectionManager()Lcom/samsung/upnp/media/server/ConnectionManager;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/upnp/media/server/MediaServer;->conMan:Lcom/samsung/upnp/media/server/ConnectionManager;

    return-object v0
.end method

.method public getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/upnp/media/server/MediaServer;->conDir:Lcom/samsung/upnp/media/server/ContentDirectory;

    return-object v0
.end method

.method public getInterfaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/samsung/net/HostInterface;->getInterface()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUploadPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 296
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_0

    .line 297
    const-string v1, ""

    .line 298
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/samsung/upnp/media/server/ContentDirectory;->getUploadPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 1
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 370
    if-nez p1, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isPostRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->httpPostRequestReceived(Lcom/samsung/http/HTTPRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->httpGetRequestReceived(Lcom/samsung/http/HTTPRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    invoke-super {p0, p1}, Lcom/samsung/upnp/Device;->httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    goto :goto_0
.end method

.method public httptRecieved(Ljava/lang/String;)V
    .locals 2
    .param p1, "firstLine"    # Ljava/lang/String;

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 385
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    const-string v1, "/MediaStore/In"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->contentImportProgressRecieved(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initailize(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "description"    # Ljava/io/InputStream;
    .param p2, "contentDirectorySCPD"    # Ljava/io/InputStream;
    .param p3, "connectionManagerSCPD"    # Ljava/io/InputStream;
    .param p4, "friendlyName"    # Ljava/lang/String;
    .param p5, "udn"    # Ljava/lang/String;
    .param p6, "ipAddr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/upnp/device/InvalidDescriptionException;
        }
    .end annotation

    .prologue
    .line 98
    iput-object p6, p0, Lcom/samsung/upnp/media/server/MediaServer;->ipAddress:Ljava/lang/String;

    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/media/server/MediaServer;->loadDescription(Ljava/io/InputStream;)Z

    .line 101
    if-eqz p4, :cond_0

    .line 102
    const/4 v3, 0x0

    invoke-virtual {p0, p4, v3}, Lcom/samsung/upnp/media/server/MediaServer;->setFriendlyName(Ljava/lang/String;Z)Z

    .line 104
    :cond_0
    if-eqz p5, :cond_1

    .line 105
    invoke-virtual {p0, p5}, Lcom/samsung/upnp/media/server/MediaServer;->setUDN(Ljava/lang/String;)V

    .line 107
    :cond_1
    const-string v3, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual {p0, v3}, Lcom/samsung/upnp/media/server/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v1

    .line 108
    .local v1, "servConDir":Lcom/samsung/upnp/Service;
    const-string v3, "urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual {p0, v3}, Lcom/samsung/upnp/media/server/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v2

    .line 112
    .local v2, "servConMan":Lcom/samsung/upnp/Service;
    if-eqz v1, :cond_2

    .line 113
    :try_start_0
    invoke-virtual {v1, p2}, Lcom/samsung/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z

    .line 114
    :cond_2
    if-eqz v2, :cond_3

    .line 115
    invoke-virtual {v2, p3}, Lcom/samsung/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/MediaServer;->initializeServer()V

    .line 122
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Lcom/samsung/xml/ParserException;
    invoke-virtual {v0}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    goto :goto_0
.end method

.method public isServerStarted()Z
    .locals 2

    .prologue
    .line 555
    iget-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 556
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/upnp/media/server/MediaServer;->isStart:Z

    monitor-exit v1

    return v0

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeContentDirectory(Lcom/samsung/upnp/media/server/Directory;)Z
    .locals 2
    .param p1, "dir"    # Lcom/samsung/upnp/media/server/Directory;

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 223
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 224
    :cond_0
    const/4 v1, 0x0

    .line 225
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->removeDirectory(Lcom/samsung/upnp/media/server/Directory;)Z

    move-result v1

    goto :goto_0
.end method

.method public setCreateObjectReceivedListener(Lcom/samsung/api/CreateObjectReceived;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/CreateObjectReceived;

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 230
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 232
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setCreateObjectReceivedListener(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setCreateObjectReceivedListener(Lcom/samsung/upnp/media/server/ContentDirectory$ICreateObjectReceived;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/media/server/ContentDirectory$ICreateObjectReceived;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 245
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_0

    .line 248
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setCreateObjectReceivedListener(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setFriendlyName(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "needAnnounce"    # Z

    .prologue
    .line 198
    invoke-super {p0, p1, p2}, Lcom/samsung/upnp/Device;->setFriendlyName(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-static {p1}, Lcom/samsung/http/HTTP;->setDeviceName(Ljava/lang/String;)V

    .line 200
    const/4 v0, 0x1

    .line 202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIcon(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "small"    # Landroid/graphics/Bitmap;
    .param p2, "macro"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 335
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x4e20

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    .line 336
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    iget-object v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p2, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 337
    iget-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_0
    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x4e20

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    .line 343
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    iget-object v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 344
    iget-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 349
    :goto_1
    :try_start_2
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x4e20

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    .line 350
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    iget-object v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p2, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 351
    iget-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 356
    :goto_2
    :try_start_3
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x4e20

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    .line 357
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    iget-object v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 358
    iget-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 362
    :goto_3
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    goto :goto_0

    .line 345
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 346
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    goto :goto_1

    .line 352
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 353
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    goto :goto_2

    .line 359
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 360
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    goto :goto_3
.end method

.method public setIcon(Ljava/io/ByteArrayOutputStream;I)V
    .locals 0
    .param p1, "icon"    # Ljava/io/ByteArrayOutputStream;
    .param p2, "type"    # I

    .prologue
    .line 314
    packed-switch p2, :pswitch_data_0

    .line 331
    :goto_0
    return-void

    .line 316
    :pswitch_0
    iput-object p1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    goto :goto_0

    .line 319
    :pswitch_1
    iput-object p1, p0, Lcom/samsung/upnp/media/server/MediaServer;->macroIcon_png:Ljava/io/ByteArrayOutputStream;

    goto :goto_0

    .line 322
    :pswitch_2
    iput-object p1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_jpeg:Ljava/io/ByteArrayOutputStream;

    goto :goto_0

    .line 325
    :pswitch_3
    iput-object p1, p0, Lcom/samsung/upnp/media/server/MediaServer;->smallIcon_png:Ljava/io/ByteArrayOutputStream;

    goto :goto_0

    .line 314
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setInterfaceAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "ifaddr"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-static {p1}, Lcom/samsung/net/HostInterface;->setInterface(Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public setUploadFinishedListener(Lcom/samsung/api/UploadFinished;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/UploadFinished;

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 237
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setUploadFinishedListener(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setUploadFinishedListener(Lcom/samsung/upnp/media/server/ContentDirectory$IUploadFinished;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/media/server/ContentDirectory$IUploadFinished;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 253
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_0

    .line 256
    :goto_0
    return-void

    .line 255
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setUploadFinishedListener(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setUploadPath(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 289
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-nez v0, :cond_0

    .line 290
    const/4 v1, 0x0

    .line 291
    :goto_0
    return v1

    :cond_0
    invoke-static {p1}, Lcom/samsung/upnp/media/server/ContentDirectory;->setUploadPath(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public start()Z
    .locals 1

    .prologue
    .line 521
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/media/server/MediaServer;->start(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public start(Ljava/lang/String;)Z
    .locals 4
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 526
    iget-object v2, p0, Lcom/samsung/upnp/media/server/MediaServer;->mutex:Ljava/lang/Object;

    monitor-enter v2

    .line 527
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 528
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getConnectionManager()Lcom/samsung/upnp/media/server/ConnectionManager;

    move-result-object v3

    if-nez v3, :cond_1

    .line 529
    :cond_0
    monitor-exit v2

    const/4 v1, 0x0

    .line 538
    :goto_0
    return v1

    .line 532
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->isStart:Z

    if-nez v3, :cond_2

    .line 533
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->start()V

    .line 535
    invoke-super {p0}, Lcom/samsung/upnp/Device;->start()Z

    .line 536
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->isStart:Z

    .line 538
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 526
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public stop()Z
    .locals 4

    .prologue
    .line 544
    iget-object v3, p0, Lcom/samsung/upnp/media/server/MediaServer;->mutex:Ljava/lang/Object;

    monitor-enter v3

    .line 545
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/upnp/media/server/MediaServer;->getContentDirectory()Lcom/samsung/upnp/media/server/ContentDirectory;

    move-result-object v0

    .line 546
    .local v0, "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {v0}, Lcom/samsung/upnp/media/server/ContentDirectory;->stop()V

    .line 548
    :cond_0
    invoke-super {p0}, Lcom/samsung/upnp/Device;->stop()Z

    move-result v1

    .line 549
    .local v1, "result":Z
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/upnp/media/server/MediaServer;->isStart:Z

    .line 550
    monitor-exit v3

    return v1

    .line 544
    .end local v0    # "cds":Lcom/samsung/upnp/media/server/ContentDirectory;
    .end local v1    # "result":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
