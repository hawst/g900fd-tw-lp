.class public Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;
.super Lcom/samsung/upnp/media/server/object/ResourceProperty;
.source "DOAThumbnailProperty.java"


# instance fields
.field private array:Ljava/io/ByteArrayOutputStream;

.field private isThumbnailCreated:Z

.field private media_store_uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/xml/AttributeList;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "media_store_url"    # Landroid/net/Uri;
    .param p4, "attrList"    # Lcom/samsung/xml/AttributeList;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1, p2, p4}, Lcom/samsung/upnp/media/server/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/xml/AttributeList;)V

    .line 19
    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->media_store_uri:Landroid/net/Uri;

    .line 20
    iput-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->isThumbnailCreated:Z

    .line 26
    iput-object p3, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->media_store_uri:Landroid/net/Uri;

    .line 28
    return-void
.end method

.method private declared-synchronized buildThumbnail()V
    .locals 5

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->isThumbnailCreated:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 71
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 53
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getInstance()Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->media_store_uri:Landroid/net/Uri;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;->getThumbnail(Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 54
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 57
    :try_start_2
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x2710

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    .line 58
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x50

    iget-object v4, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 59
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 64
    :try_start_3
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    if-eqz v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :cond_2
    :goto_1
    const/4 v2, 0x1

    :try_start_4
    iput-boolean v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->isThumbnailCreated:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 51
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 60
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_5
    iput-object v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 64
    :try_start_6
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    if-eqz v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 66
    :catch_1
    move-exception v1

    .line 67
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 62
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    .line 64
    :try_start_8
    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    if-eqz v3, :cond_3

    .line 65
    iget-object v3, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 69
    :cond_3
    :goto_2
    :try_start_9
    throw v2

    .line 66
    :catch_2
    move-exception v1

    .line 67
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 66
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 67
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public deleteContent()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public declared-synchronized getContentLength()J
    .locals 2

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->isThumbnailCreated:Z

    if-nez v0, :cond_0

    .line 32
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->buildThumbnail()V

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 34
    const-wide/16 v0, 0x0

    .line 35
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->isThumbnailCreated:Z

    if-nez v0, :cond_0

    .line 40
    invoke-direct {p0}, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->buildThumbnail()V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 42
    const/4 v0, 0x0

    .line 43
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/samsung/upnp/media/server/object/item/sat/DOAThumbnailProperty;->array:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
