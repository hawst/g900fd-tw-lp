.class Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;
.super Ljava/lang/Object;
.source "DOADatabase.java"

# interfaces
.implements Landroid/database/Cursor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/upnp/media/server/directory/doa/DOADatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DoaCursor"
.end annotation


# instance fields
.field private cursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579
    iput-object p1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    .line 580
    return-void
.end method


# virtual methods
.method public abortUpdates()V
    .locals 0

    .prologue
    .line 651
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 586
    :cond_0
    return-void
.end method

.method public commitUpdates()Z
    .locals 1

    .prologue
    .line 662
    const/4 v0, 0x1

    return v0
.end method

.method public commitUpdates(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 663
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<+Ljava/lang/Long;+Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "buffer"    # Landroid/database/CharArrayBuffer;

    .prologue
    .line 596
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 599
    :cond_0
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 604
    :cond_0
    return-void
.end method

.method public deleteRow()Z
    .locals 1

    .prologue
    .line 650
    const/4 v0, 0x1

    return v0
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 607
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 609
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    .line 615
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 619
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 621
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 627
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 629
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 633
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 635
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 641
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 647
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 668
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 670
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 676
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 680
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 682
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 686
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 688
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 692
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 694
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 664
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 700
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 704
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    .line 706
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 710
    iget-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 711
    iget-object v1, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 712
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 714
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 712
    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 714
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 591
    const/4 v0, 0x0

    return v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    .line 721
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpdates()Z
    .locals 1

    .prologue
    .line 653
    const/4 v0, 0x1

    return v0
.end method

.method public isAfterLast()Z
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    .line 727
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBeforeFirst()Z
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    .line 733
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 738
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    .line 739
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirst()Z
    .locals 1

    .prologue
    .line 743
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 744
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isFirst()Z

    move-result v0

    .line 745
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLast()Z
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    .line 751
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 755
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    .line 757
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public move(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 761
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->move(I)Z

    move-result v0

    .line 763
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 769
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    .line 775
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 781
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 785
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 787
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    .line 793
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 797
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 799
    :cond_0
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 802
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 805
    :cond_0
    return-void
.end method

.method public requery()Z
    .locals 1

    .prologue
    .line 808
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    .line 810
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 814
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 816
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 820
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 822
    :cond_0
    return-void
.end method

.method public supportsUpdates()Z
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x1

    return v0
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 825
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 827
    :cond_0
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 830
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/samsung/upnp/media/server/directory/doa/DOADatabase$DoaCursor;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 832
    :cond_0
    return-void
.end method

.method public updateBlob(I[B)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # [B

    .prologue
    .line 654
    const/4 v0, 0x1

    return v0
.end method

.method public updateDouble(ID)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # D

    .prologue
    .line 660
    const/4 v0, 0x1

    return v0
.end method

.method public updateFloat(IF)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # F

    .prologue
    .line 659
    const/4 v0, 0x1

    return v0
.end method

.method public updateInt(II)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # I

    .prologue
    .line 657
    const/4 v0, 0x1

    return v0
.end method

.method public updateLong(IJ)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # J

    .prologue
    .line 658
    const/4 v0, 0x1

    return v0
.end method

.method public updateShort(IS)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # S

    .prologue
    .line 656
    const/4 v0, 0x1

    return v0
.end method

.method public updateString(ILjava/lang/String;)Z
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 655
    const/4 v0, 0x1

    return v0
.end method

.method public updateToNull(I)Z
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 661
    const/4 v0, 0x1

    return v0
.end method
