.class public Lcom/samsung/upnp/SimpleDevice;
.super Ljava/lang/Object;
.source "SimpleDevice.java"

# interfaces
.implements Lcom/samsung/http/HTTPRequestListener;


# static fields
.field public static final DEFAULT_HTTP_PORT:I = 0x5c16


# instance fields
.field private contentInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private httpPort:I

.field private ipAddress:Ljava/lang/String;

.field itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

.field private server:Lcom/samsung/http/HTTPServer;


# direct methods
.method public constructor <init>(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contentInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "localhost"

    iput-object v0, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    .line 39
    const/16 v0, 0x5c16

    iput v0, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    .line 40
    iput-object v1, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    .line 185
    iput-object v1, p0, Lcom/samsung/upnp/SimpleDevice;->server:Lcom/samsung/http/HTTPServer;

    .line 199
    iput-object v1, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .line 43
    new-instance v0, Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-direct {v0}, Lcom/samsung/upnp/media/server/object/item/ItemNode;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    .line 44
    iput-object p1, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    .line 45
    return-void
.end method

.method private checkItemNode()Z
    .locals 5

    .prologue
    .line 121
    iget-object v3, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v4, "title"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 122
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v4, "mime_type"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123
    .local v0, "mimeType":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v4, "_size"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 125
    .local v1, "size":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 126
    :cond_0
    const/4 v3, 0x0

    .line 128
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private getServerAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSubtitleAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/subtitle/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private httpAddInResponse(Lcom/samsung/http/HTTPRequest;)V
    .locals 2
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-static {p1, v1}, Lcom/samsung/upnp/media/server/CDSResponseBuilder;->buildResponse(Lcom/samsung/http/HTTPRequest;Lcom/samsung/upnp/media/server/object/item/ItemNode;)Lcom/samsung/http/HTTPResponse;

    move-result-object v0

    .line 91
    .local v0, "httpRes":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {p1, v0}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 92
    return-void
.end method

.method private initializeItemNode()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 132
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 133
    .local v10, "title":Ljava/lang/String;
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "mime_type"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 134
    .local v3, "mimeType":Ljava/lang/String;
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "_size"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 135
    .local v8, "size":Ljava/lang/String;
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "_data"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 137
    .local v0, "data":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    const-string v11, "image"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 140
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    const-string v12, "object.item.imageItem.photo"

    invoke-virtual {v11, v12}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setUPnPClass(Ljava/lang/String;)V

    .line 143
    if-eqz v0, :cond_2

    .line 144
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 145
    .local v2, "fis":Ljava/io/FileInputStream;
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 146
    .local v4, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v11, 0x1

    iput-boolean v11, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 147
    const/4 v11, 0x0

    invoke-static {v2, v11, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 148
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 149
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "width"

    iget v13, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "height"

    iget v13, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 166
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "option":Landroid/graphics/BitmapFactory$Options;
    :cond_2
    :goto_1
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v11, v10}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setTitle(Ljava/lang/String;)V

    .line 170
    invoke-direct {p0}, Lcom/samsung/upnp/SimpleDevice;->getServerAddress()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    invoke-static {v11, v12}, Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;->createSimpleResourceProperty(Ljava/lang/String;Ljava/util/HashMap;)Lcom/samsung/upnp/media/server/object/item/SimpleResourceProperty;

    move-result-object v7

    .line 171
    .local v7, "resProperty":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v11, v7}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    .line 175
    if-eqz v0, :cond_0

    .line 176
    invoke-static {v0}, Lcom/samsung/upnp/media/server/object/ResourceProperty;->getSubTitleFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 177
    .local v9, "subtitle":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 178
    invoke-static {}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->getParser()Lcom/samsung/upnp/media/server/object/DLNAProfileParser;

    move-result-object v11

    sget v12, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->PROTOCOL_TYPE_SMALL:I

    invoke-virtual {v11, v3, v14, v14, v12}, Lcom/samsung/upnp/media/server/object/DLNAProfileParser;->buildProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v5

    .line 179
    .local v5, "protocol":Ljava/lang/String;
    new-instance v6, Lcom/samsung/upnp/media/server/object/item/sat/DOASubtitleProperty;

    invoke-direct {p0}, Lcom/samsung/upnp/SimpleDevice;->getSubtitleAddress()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11, v5, v9}, Lcom/samsung/upnp/media/server/object/item/sat/DOASubtitleProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .local v6, "resProp":Lcom/samsung/upnp/media/server/object/item/sat/DOASubtitleProperty;
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    invoke-virtual {v11, v6}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->addResource(Lcom/samsung/upnp/media/server/object/ResourceProperty;)V

    goto :goto_0

    .line 152
    .end local v5    # "protocol":Ljava/lang/String;
    .end local v6    # "resProp":Lcom/samsung/upnp/media/server/object/item/sat/DOASubtitleProperty;
    .end local v7    # "resProperty":Lcom/samsung/upnp/media/server/object/ResourceProperty;
    .end local v9    # "subtitle":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 153
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 154
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 155
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 157
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    const-string v11, "video"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 158
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    const-string v12, "object.item.videoItem.movie"

    invoke-virtual {v11, v12}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setUPnPClass(Ljava/lang/String;)V

    .line 159
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "width"

    const-string v13, "0"

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "height"

    const-string v13, "0"

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 161
    :cond_4
    const-string v11, "audio"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 162
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    const-string v12, "object.item.audioItem.musicTrack"

    invoke-virtual {v11, v12}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->setUPnPClass(Ljava/lang/String;)V

    .line 163
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "width"

    const-string v13, "0"

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v11, p0, Lcom/samsung/upnp/SimpleDevice;->contentInfo:Ljava/util/HashMap;

    const-string v12, "height"

    const-string v13, "0"

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method private initializeServer()V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x9

    invoke-static {v0}, Lcom/samsung/upnp/UPnP;->setEnable(I)V

    .line 51
    iget-object v0, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/SimpleDevice;->setInterfaceAddress(Ljava/lang/String;)V

    .line 52
    const/16 v0, 0x5c16

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/SimpleDevice;->setHTTPPort(I)V

    .line 53
    return-void
.end method

.method private setHTTPServer(Lcom/samsung/http/HTTPServer;)V
    .locals 0
    .param p1, "server"    # Lcom/samsung/http/HTTPServer;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/upnp/SimpleDevice;->server:Lcom/samsung/http/HTTPServer;

    .line 188
    return-void
.end method


# virtual methods
.method public getItemNode()Lcom/samsung/xml/Node;
    .locals 5

    .prologue
    .line 203
    :try_start_0
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/upnp/SimpleDevice;->itemNode:Lcom/samsung/upnp/media/server/object/item/ItemNode;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/upnp/media/server/object/item/ItemNode;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 207
    :goto_0
    return-object v1

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Lcom/samsung/xml/ParserException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 3
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "uri":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "uri = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/samsung/upnp/SimpleDevice;->httpAddInResponse(Lcom/samsung/http/HTTPRequest;)V

    .line 85
    return-void
.end method

.method public setHTTPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    .line 58
    return-void
.end method

.method public setInterfaceAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "ifaddr"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {p1}, Lcom/samsung/net/HostInterface;->setInterface(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public start()Z
    .locals 5

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/samsung/upnp/SimpleDevice;->checkItemNode()Z

    move-result v0

    .line 101
    .local v0, "isValidItem":Z
    if-nez v0, :cond_0

    .line 102
    const/4 v2, 0x0

    .line 116
    :goto_0
    return v2

    .line 104
    :cond_0
    new-instance v1, Lcom/samsung/http/HTTPServer;

    invoke-direct {v1}, Lcom/samsung/http/HTTPServer;-><init>()V

    .line 105
    .local v1, "server":Lcom/samsung/http/HTTPServer;
    invoke-static {}, Lcom/samsung/net/HostInterface;->getInterface()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    .line 106
    invoke-direct {p0}, Lcom/samsung/upnp/SimpleDevice;->initializeServer()V

    .line 107
    :goto_1
    iget-object v2, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/http/HTTPServer;->open(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ipAddress "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/upnp/SimpleDevice;->ipAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " httpPort "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 111
    invoke-direct {p0, v1}, Lcom/samsung/upnp/SimpleDevice;->setHTTPServer(Lcom/samsung/http/HTTPServer;)V

    .line 113
    invoke-virtual {v1, p0}, Lcom/samsung/http/HTTPServer;->addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V

    .line 114
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServer;->start()Z

    .line 115
    invoke-direct {p0}, Lcom/samsung/upnp/SimpleDevice;->initializeItemNode()V

    .line 116
    const/4 v2, 0x1

    goto :goto_0

    .line 108
    :cond_1
    iget v2, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/upnp/SimpleDevice;->httpPort:I

    goto :goto_1
.end method

.method public stop()Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/upnp/SimpleDevice;->server:Lcom/samsung/http/HTTPServer;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/samsung/upnp/SimpleDevice;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v0}, Lcom/samsung/http/HTTPServer;->stop()Z

    .line 194
    iget-object v0, p0, Lcom/samsung/upnp/SimpleDevice;->server:Lcom/samsung/http/HTTPServer;

    invoke-virtual {v0}, Lcom/samsung/http/HTTPServer;->close()Z

    move-result v0

    .line 196
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
