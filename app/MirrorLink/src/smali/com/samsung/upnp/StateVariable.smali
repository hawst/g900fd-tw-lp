.class public Lcom/samsung/upnp/StateVariable;
.super Lcom/samsung/upnp/xml/NodeData;
.source "StateVariable.java"


# static fields
.field private static final DATATYPE:Ljava/lang/String; = "dataType"

.field public static final ELEM_NAME:Ljava/lang/String; = "stateVariable"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final SENDEVENTS:Ljava/lang/String; = "sendEvents"

.field private static final SENDEVENTS_NO:Ljava/lang/String; = "no"

.field private static final SENDEVENTS_YES:Ljava/lang/String; = "yes"


# instance fields
.field private serviceNode:Lcom/samsung/xml/Node;

.field private stateVariableNode:Lcom/samsung/xml/Node;

.field private upnpStatus:Lcom/samsung/upnp/UPnPStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/samsung/upnp/xml/NodeData;-><init>()V

    .line 376
    new-instance v0, Lcom/samsung/upnp/UPnPStatus;

    invoke-direct {v0}, Lcom/samsung/upnp/UPnPStatus;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/StateVariable;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/upnp/StateVariable;->serviceNode:Lcom/samsung/xml/Node;

    .line 111
    new-instance v0, Lcom/samsung/xml/Node;

    invoke-direct {v0}, Lcom/samsung/xml/Node;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/StateVariable;->stateVariableNode:Lcom/samsung/xml/Node;

    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V
    .locals 1
    .param p1, "serviceNode"    # Lcom/samsung/xml/Node;
    .param p2, "stateVarNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/samsung/upnp/xml/NodeData;-><init>()V

    .line 376
    new-instance v0, Lcom/samsung/upnp/UPnPStatus;

    invoke-direct {v0}, Lcom/samsung/upnp/UPnPStatus;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/StateVariable;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    .line 116
    iput-object p1, p0, Lcom/samsung/upnp/StateVariable;->serviceNode:Lcom/samsung/xml/Node;

    .line 117
    iput-object p2, p0, Lcom/samsung/upnp/StateVariable;->stateVariableNode:Lcom/samsung/xml/Node;

    .line 118
    return-void
.end method

.method public static isStateVariableNode(Lcom/samsung/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 126
    const-string v0, "stateVariable"

    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getAllowedValueList()Lcom/samsung/upnp/AllowedValueList;
    .locals 8

    .prologue
    .line 256
    new-instance v4, Lcom/samsung/upnp/AllowedValueList;

    invoke-direct {v4}, Lcom/samsung/upnp/AllowedValueList;-><init>()V

    .line 257
    .local v4, "valueList":Lcom/samsung/upnp/AllowedValueList;
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v6

    const-string v7, "allowedValueList"

    invoke-virtual {v6, v7}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v5

    .line 258
    .local v5, "valueListNode":Lcom/samsung/xml/Node;
    if-nez v5, :cond_1

    .line 269
    :cond_0
    return-object v4

    .line 261
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v2

    .line 262
    .local v2, "nNode":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 263
    invoke-virtual {v5, v1}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v3

    .line 264
    .local v3, "node":Lcom/samsung/xml/Node;
    invoke-static {v3}, Lcom/samsung/upnp/AllowedValue;->isAllowedValueNode(Lcom/samsung/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 262
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    :cond_2
    new-instance v0, Lcom/samsung/upnp/AllowedValue;

    invoke-direct {v0, v3}, Lcom/samsung/upnp/AllowedValue;-><init>(Lcom/samsung/xml/Node;)V

    .line 267
    .local v0, "allowedVal":Lcom/samsung/upnp/AllowedValue;
    invoke-virtual {v4, v0}, Lcom/samsung/upnp/AllowedValueList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getAllowedValueRange()Lcom/samsung/upnp/AllowedValueRange;
    .locals 3

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v1

    const-string v2, "allowedValueRange"

    invoke-virtual {v1, v2}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 285
    .local v0, "valueRangeNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 286
    const/4 v1, 0x0

    .line 287
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/upnp/AllowedValueRange;

    invoke-direct {v1, v0}, Lcom/samsung/upnp/AllowedValueRange;-><init>(Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "dataType"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQueryListener()Lcom/samsung/upnp/control/QueryListener;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableData()Lcom/samsung/upnp/xml/StateVariableData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/StateVariableData;->getQueryListener()Lcom/samsung/upnp/control/QueryListener;

    move-result-object v0

    return-object v0
.end method

.method public getService()Lcom/samsung/upnp/Service;
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 94
    .local v0, "serviceNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 95
    const/4 v1, 0x0

    .line 96
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/upnp/Service;

    invoke-direct {v1, v0}, Lcom/samsung/upnp/Service;-><init>(Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method public getServiceNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/upnp/StateVariable;->serviceNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method public getStateVariableData()Lcom/samsung/upnp/xml/StateVariableData;
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 203
    .local v0, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/xml/StateVariableData;

    .line 204
    .local v1, "userData":Lcom/samsung/upnp/xml/StateVariableData;
    if-nez v1, :cond_0

    .line 205
    new-instance v1, Lcom/samsung/upnp/xml/StateVariableData;

    .end local v1    # "userData":Lcom/samsung/upnp/xml/StateVariableData;
    invoke-direct {v1}, Lcom/samsung/upnp/xml/StateVariableData;-><init>()V

    .line 206
    .restart local v1    # "userData":Lcom/samsung/upnp/xml/StateVariableData;
    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 207
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/xml/StateVariableData;->setNode(Lcom/samsung/xml/Node;)V

    .line 209
    :cond_0
    return-object v1
.end method

.method public getStateVariableNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/upnp/StateVariable;->stateVariableNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method public getStatus()Lcom/samsung/upnp/UPnPStatus;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/samsung/upnp/StateVariable;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableData()Lcom/samsung/upnp/xml/StateVariableData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/StateVariableData;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAllowedValueList()Z
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getAllowedValueList()Lcom/samsung/upnp/AllowedValueList;

    move-result-object v0

    .line 275
    .local v0, "valueList":Lcom/samsung/upnp/AllowedValueList;
    invoke-virtual {v0}, Lcom/samsung/upnp/AllowedValueList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasAllowedValueRange()Z
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getAllowedValueRange()Lcom/samsung/upnp/AllowedValueRange;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSendEvents()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v2

    const-string v3, "sendEvents"

    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "state":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v1

    .line 179
    :cond_1
    const-string v2, "yes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 180
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public performQueryListener(Lcom/samsung/upnp/control/QueryRequest;)Z
    .locals 6
    .param p1, "queryReq"    # Lcom/samsung/upnp/control/QueryRequest;

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getQueryListener()Lcom/samsung/upnp/control/QueryListener;

    move-result-object v0

    .line 312
    .local v0, "listener":Lcom/samsung/upnp/control/QueryListener;
    if-nez v0, :cond_0

    .line 313
    const/4 v4, 0x0

    .line 327
    :goto_0
    return v4

    .line 314
    :cond_0
    new-instance v1, Lcom/samsung/upnp/control/QueryResponse;

    invoke-direct {v1}, Lcom/samsung/upnp/control/QueryResponse;-><init>()V

    .line 315
    .local v1, "queryRes":Lcom/samsung/upnp/control/QueryResponse;
    new-instance v2, Lcom/samsung/upnp/StateVariable;

    invoke-direct {v2}, Lcom/samsung/upnp/StateVariable;-><init>()V

    .line 316
    .local v2, "retVar":Lcom/samsung/upnp/StateVariable;
    invoke-virtual {v2, p0}, Lcom/samsung/upnp/StateVariable;->set(Lcom/samsung/upnp/StateVariable;)V

    .line 317
    const-string v4, ""

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 318
    const/16 v4, 0x194

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/StateVariable;->setStatus(I)V

    .line 319
    invoke-interface {v0, v2}, Lcom/samsung/upnp/control/QueryListener;->queryControlReceived(Lcom/samsung/upnp/StateVariable;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 320
    invoke-virtual {v1, v2}, Lcom/samsung/upnp/control/QueryResponse;->setResponse(Lcom/samsung/upnp/StateVariable;)V

    .line 326
    :goto_1
    invoke-virtual {p1, v1}, Lcom/samsung/upnp/control/QueryRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 327
    const/4 v4, 0x1

    goto :goto_0

    .line 323
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/upnp/StateVariable;->getStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v3

    .line 324
    .local v3, "upnpStatus":Lcom/samsung/upnp/UPnPStatus;
    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/samsung/upnp/control/QueryResponse;->setFaultResponse(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public set(Lcom/samsung/upnp/StateVariable;)V
    .locals 1
    .param p1, "stateVar"    # Lcom/samsung/upnp/StateVariable;

    .prologue
    .line 190
    invoke-virtual {p1}, Lcom/samsung/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/StateVariable;->setName(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p1}, Lcom/samsung/upnp/StateVariable;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p1}, Lcom/samsung/upnp/StateVariable;->getDataType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/StateVariable;->setDataType(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p1}, Lcom/samsung/upnp/StateVariable;->isSendEvents()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/StateVariable;->setSendEvents(Z)V

    .line 194
    return-void
.end method

.method public setDataType(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "dataType"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/control/QueryListener;

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableData()Lcom/samsung/upnp/xml/StateVariableData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/StateVariableData;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V

    .line 307
    return-void
.end method

.method public setSendEvents(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/xml/Node;

    move-result-object v1

    const-string v2, "sendEvents"

    if-eqz p1, :cond_0

    const-string v0, "yes"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/samsung/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-void

    .line 171
    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method

.method public setStatus(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 386
    invoke-static {p1}, Lcom/samsung/upnp/UPnPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/upnp/StateVariable;->setStatus(ILjava/lang/String;)V

    .line 387
    return-void
.end method

.method public setStatus(ILjava/lang/String;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "descr"    # Ljava/lang/String;

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/upnp/StateVariable;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/UPnPStatus;->setCode(I)V

    .line 381
    iget-object v0, p0, Lcom/samsung/upnp/StateVariable;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    invoke-virtual {v0, p2}, Lcom/samsung/upnp/UPnPStatus;->setDescription(Ljava/lang/String;)V

    .line 382
    return-void
.end method

.method public setValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 237
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 238
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableData()Lcom/samsung/upnp/xml/StateVariableData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/upnp/xml/StateVariableData;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "currValue":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getStateVariableData()Lcom/samsung/upnp/xml/StateVariableData;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/upnp/xml/StateVariableData;->setValue(Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->getService()Lcom/samsung/upnp/Service;

    move-result-object v1

    .line 228
    .local v1, "service":Lcom/samsung/upnp/Service;
    if-nez v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/StateVariable;->isSendEvents()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    invoke-virtual {v1, p0}, Lcom/samsung/upnp/Service;->notify(Lcom/samsung/upnp/StateVariable;)V

    goto :goto_0
.end method
