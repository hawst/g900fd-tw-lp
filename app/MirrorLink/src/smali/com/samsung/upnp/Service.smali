.class public Lcom/samsung/upnp/Service;
.super Ljava/lang/Object;
.source "Service.java"


# static fields
.field private static final CONTROL_URL:Ljava/lang/String; = "controlURL"

.field public static final ELEM_NAME:Ljava/lang/String; = "service"

.field private static final EVENT_SUB_URL:Ljava/lang/String; = "eventSubURL"

.field private static final SCPDURL:Ljava/lang/String; = "SCPDURL"

.field private static final SERVICE_ID:Ljava/lang/String; = "serviceId"

.field private static final SERVICE_TYPE:Ljava/lang/String; = "serviceType"


# instance fields
.field private serviceNode:Lcom/samsung/xml/Node;


# direct methods
.method public constructor <init>(Lcom/samsung/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/samsung/upnp/Service;->serviceNode:Lcom/samsung/xml/Node;

    .line 128
    return-void
.end method

.method private getDeviceNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/xml/Node;->getParentNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 162
    .local v0, "node":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 163
    const/4 v1, 0x0

    .line 164
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getParentNode()Lcom/samsung/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method private getNotifyServiceTypeNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getNotifyServiceTypeUSN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 660
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRootNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method private getSCPDNode()Lcom/samsung/xml/Node;
    .locals 19

    .prologue
    .line 452
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v15

    .line 453
    .local v15, "serviceNode":Lcom/samsung/xml/Node;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/upnp/Service;->getServiceData()Lcom/samsung/upnp/xml/ServiceData;

    move-result-object v2

    .line 454
    .local v2, "data":Lcom/samsung/upnp/xml/ServiceData;
    invoke-virtual {v2}, Lcom/samsung/upnp/xml/ServiceData;->getSCPDNode()Lcom/samsung/xml/Node;

    move-result-object v11

    .line 455
    .local v11, "scpdNode":Lcom/samsung/xml/Node;
    if-eqz v11, :cond_0

    move-object v12, v11

    .line 512
    .end local v11    # "scpdNode":Lcom/samsung/xml/Node;
    .local v12, "scpdNode":Lcom/samsung/xml/Node;
    :goto_0
    return-object v12

    .line 458
    .end local v12    # "scpdNode":Lcom/samsung/xml/Node;
    .restart local v11    # "scpdNode":Lcom/samsung/xml/Node;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Service;->getSCPDURL()Ljava/lang/String;

    move-result-object v13

    .line 461
    .local v13, "scpdURLStr":Ljava/lang/String;
    :try_start_0
    new-instance v14, Ljava/net/URL;

    invoke-direct {v14, v13}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 462
    .local v14, "scpdUrl":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/xml/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 510
    .end local v14    # "scpdUrl":Ljava/net/URL;
    :goto_1
    invoke-virtual {v2, v11}, Lcom/samsung/upnp/xml/ServiceData;->setSCPDNode(Lcom/samsung/xml/Node;)V

    move-object v12, v11

    .line 512
    .end local v11    # "scpdNode":Lcom/samsung/xml/Node;
    .restart local v12    # "scpdNode":Lcom/samsung/xml/Node;
    goto :goto_0

    .line 464
    .end local v12    # "scpdNode":Lcom/samsung/xml/Node;
    .restart local v11    # "scpdNode":Lcom/samsung/xml/Node;
    :catch_0
    move-exception v3

    .line 465
    .local v3, "e1":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/upnp/Service;->getRootDevice()Lcom/samsung/upnp/Device;

    move-result-object v10

    .line 466
    .local v10, "rootDev":Lcom/samsung/upnp/Device;
    const/16 v16, 0x0

    .line 467
    .local v16, "urlBaseStr":Ljava/lang/String;
    if-eqz v10, :cond_2

    .line 468
    invoke-virtual {v10}, Lcom/samsung/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v16

    .line 470
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    if-gtz v17, :cond_2

    .line 471
    :cond_1
    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 473
    invoke-virtual {v10}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v7

    .line 474
    .local v7, "location":Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/http/HTTP;->getBaseUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 484
    .end local v7    # "location":Ljava/lang/String;
    :cond_2
    :goto_2
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 486
    .local v9, "newScpdURLStr":Ljava/lang/String;
    :try_start_1
    new-instance v8, Ljava/net/URL;

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 487
    .local v8, "newScpdURL":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/xml/Node;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    goto :goto_1

    .line 479
    .end local v8    # "newScpdURL":Ljava/net/URL;
    .end local v9    # "newScpdURLStr":Ljava/lang/String;
    :cond_3
    invoke-virtual {v10}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/upnp/Service;->removeTillLastSlash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 480
    .restart local v7    # "location":Ljava/lang/String;
    move-object/from16 v16, v7

    goto :goto_2

    .line 489
    .end local v7    # "location":Ljava/lang/String;
    .restart local v9    # "newScpdURLStr":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 490
    .local v4, "e2":Ljava/lang/Exception;
    move-object/from16 v0, v16

    invoke-static {v0, v13}, Lcom/samsung/http/HTTP;->getAbsoluteURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 492
    :try_start_2
    new-instance v8, Ljava/net/URL;

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 493
    .restart local v8    # "newScpdURL":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/xml/Node;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v11

    goto :goto_1

    .line 495
    .end local v8    # "newScpdURL":Ljava/net/URL;
    :catch_2
    move-exception v5

    .line 496
    .local v5, "e3":Ljava/lang/Exception;
    if-eqz v10, :cond_4

    .line 497
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Lcom/samsung/upnp/Device;->getDescriptionFilePath()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 499
    :cond_4
    :try_start_3
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/upnp/Service;->getSCPDNode(Ljava/io/File;)Lcom/samsung/xml/Node;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v11

    goto/16 :goto_1

    .line 501
    :catch_3
    move-exception v6

    .line 502
    .local v6, "e4":Ljava/lang/Exception;
    invoke-static {v6}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    goto/16 :goto_1
.end method

.method private getSCPDNode(Ljava/io/File;)Lcom/samsung/xml/Node;
    .locals 2
    .param p1, "scpdFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 410
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v0

    .line 411
    .local v0, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v0, p1}, Lcom/samsung/xml/Parser;->parse(Ljava/io/File;)Lcom/samsung/xml/Node;

    move-result-object v1

    return-object v1
.end method

.method private getSCPDNode(Ljava/net/URL;)Lcom/samsung/xml/Node;
    .locals 2
    .param p1, "scpdUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 404
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v0

    .line 405
    .local v0, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v0, p1}, Lcom/samsung/xml/Parser;->parse(Ljava/net/URL;)Lcom/samsung/xml/Node;

    move-result-object v1

    return-object v1
.end method

.method private getServiceData()Lcom/samsung/upnp/xml/ServiceData;
    .locals 2

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 640
    .local v0, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/xml/ServiceData;

    .line 641
    .local v1, "userData":Lcom/samsung/upnp/xml/ServiceData;
    if-nez v1, :cond_0

    .line 642
    new-instance v1, Lcom/samsung/upnp/xml/ServiceData;

    .end local v1    # "userData":Lcom/samsung/upnp/xml/ServiceData;
    invoke-direct {v1}, Lcom/samsung/upnp/xml/ServiceData;-><init>()V

    .line 643
    .restart local v1    # "userData":Lcom/samsung/upnp/xml/ServiceData;
    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 644
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/xml/ServiceData;->setNode(Lcom/samsung/xml/Node;)V

    .line 646
    :cond_0
    return-object v1
.end method

.method public static isServiceNode(Lcom/samsung/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 152
    const-string v0, "service"

    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isURL(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "referenceUrl"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 228
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v2, v3

    .line 237
    :cond_1
    :goto_0
    return v2

    .line 230
    :cond_2
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 231
    .local v1, "ret":Z
    if-nez v1, :cond_1

    .line 233
    invoke-static {p1, v3}, Lcom/samsung/http/HTTP;->toRelativeURL(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 234
    .local v0, "relativeRefUrl":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 235
    if-nez v1, :cond_1

    move v2, v3

    .line 237
    goto :goto_0
.end method

.method private notify(Lcom/samsung/upnp/event/Subscriber;Lcom/samsung/upnp/ServiceStateTable;)Z
    .locals 7
    .param p1, "sub"    # Lcom/samsung/upnp/event/Subscriber;
    .param p2, "stateTable"    # Lcom/samsung/upnp/ServiceStateTable;

    .prologue
    .line 905
    invoke-virtual {p1}, Lcom/samsung/upnp/event/Subscriber;->getDeliveryHost()Ljava/lang/String;

    move-result-object v0

    .line 906
    .local v0, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/event/Subscriber;->getDeliveryPort()I

    move-result v2

    .line 909
    .local v2, "port":I
    new-instance v1, Lcom/samsung/upnp/event/NotifyRequest;

    invoke-direct {v1}, Lcom/samsung/upnp/event/NotifyRequest;-><init>()V

    .line 910
    .local v1, "notifyReq":Lcom/samsung/upnp/event/NotifyRequest;
    invoke-virtual {v1, p1, p2}, Lcom/samsung/upnp/event/NotifyRequest;->setRequest(Lcom/samsung/upnp/event/Subscriber;Lcom/samsung/upnp/ServiceStateTable;)Z

    .line 912
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ServiceNotify"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;)V

    .line 914
    invoke-virtual {v1, v0, v2}, Lcom/samsung/upnp/event/NotifyRequest;->post(Ljava/lang/String;I)Lcom/samsung/http/HTTPResponse;

    move-result-object v3

    .line 915
    .local v3, "res":Lcom/samsung/http/HTTPResponse;
    invoke-virtual {v3}, Lcom/samsung/http/HTTPResponse;->isSuccessful()Z

    move-result v4

    if-nez v4, :cond_0

    .line 916
    const/4 v4, 0x0

    .line 920
    :goto_0
    return v4

    .line 918
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/event/Subscriber;->incrementNotifyCount()V

    .line 920
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private notify(Lcom/samsung/upnp/event/Subscriber;Lcom/samsung/upnp/StateVariable;)Z
    .locals 8
    .param p1, "sub"    # Lcom/samsung/upnp/event/Subscriber;
    .param p2, "stateVar"    # Lcom/samsung/upnp/StateVariable;

    .prologue
    .line 781
    if-nez p1, :cond_0

    .line 782
    const/4 v0, 0x0

    .line 809
    :goto_0
    return v0

    .line 783
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v3

    .line 784
    .local v3, "varName":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/upnp/StateVariable;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 786
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/event/Subscriber;->getDeliveryHost()Ljava/lang/String;

    move-result-object v5

    .line 787
    .local v5, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/event/Subscriber;->getDeliveryPort()I

    move-result v6

    .line 790
    .local v6, "port":I
    const-string v0, "UPNP/Service"

    const-string v1, "Modified notify"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Lcom/samsung/upnp/Service$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/upnp/Service$1;-><init>(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/Subscriber;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 802
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 807
    invoke-virtual {p1}, Lcom/samsung/upnp/event/Subscriber;->incrementNotifyCount()V

    .line 809
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private removeTillLastSlash(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 415
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 416
    .local v3, "results":[Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    .line 417
    .local v1, "l":Z
    if-eqz v1, :cond_0

    .line 429
    .end local p1    # "location":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 421
    .restart local p1    # "location":Ljava/lang/String;
    :cond_0
    array-length v4, v3

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 422
    const-string p1, ""

    goto :goto_0

    .line 424
    :cond_1
    const-string v2, ""

    .line 425
    .local v2, "resultLocation":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_2

    move-object p1, v2

    .line 429
    goto :goto_0

    .line 426
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addSubscriber(Lcom/samsung/upnp/event/Subscriber;)V
    .locals 1
    .param p1, "sub"    # Lcom/samsung/upnp/event/Subscriber;

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getSubscriberList()Lcom/samsung/upnp/event/SubscriberList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/event/SubscriberList;->add(Ljava/lang/Object;)Z

    .line 755
    return-void
.end method

.method public announce(Ljava/lang/String;)V
    .locals 8
    .param p1, "bindAddr"    # Ljava/lang/String;

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getRootDevice()Lcom/samsung/upnp/Device;

    move-result-object v2

    .line 667
    .local v2, "rootDev":Lcom/samsung/upnp/Device;
    const-string v1, ""

    .line 668
    .local v1, "devLocation":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 669
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/Device;->getLocationURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 670
    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getNotifyServiceTypeNT()Ljava/lang/String;

    move-result-object v3

    .line 671
    .local v3, "serviceNT":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getNotifyServiceTypeUSN()Ljava/lang/String;

    move-result-object v4

    .line 673
    .local v4, "serviceUSN":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 675
    .local v0, "dev":Lcom/samsung/upnp/Device;
    new-instance v5, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;

    invoke-direct {v5}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;-><init>()V

    .line 676
    .local v5, "ssdpReq":Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setServer(Ljava/lang/String;)V

    .line 677
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getLeaseTime()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setLeaseTime(I)V

    .line 678
    invoke-virtual {v5, v1}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setLocation(Ljava/lang/String;)V

    .line 679
    const-string v7, "ssdp:alive"

    invoke-virtual {v5, v7}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 680
    invoke-virtual {v5, v3}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 681
    invoke-virtual {v5, v4}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 683
    new-instance v6, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    invoke-direct {v6, p1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;-><init>(Ljava/lang/String;)V

    .line 684
    .local v6, "ssdpSock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    invoke-static {}, Lcom/samsung/upnp/Device;->notifyWait()V

    .line 685
    invoke-virtual {v6, v5}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 686
    return-void
.end method

.method public byebye(Ljava/lang/String;)V
    .locals 5
    .param p1, "bindAddr"    # Ljava/lang/String;

    .prologue
    .line 692
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getNotifyServiceTypeNT()Ljava/lang/String;

    move-result-object v0

    .line 693
    .local v0, "devNT":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getNotifyServiceTypeUSN()Ljava/lang/String;

    move-result-object v1

    .line 695
    .local v1, "devUSN":Ljava/lang/String;
    new-instance v2, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;

    invoke-direct {v2}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;-><init>()V

    .line 696
    .local v2, "ssdpReq":Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;
    const-string v4, "ssdp:byebye"

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 697
    invoke-virtual {v2, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 698
    invoke-virtual {v2, v1}, Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 700
    new-instance v3, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    invoke-direct {v3, p1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;-><init>(Ljava/lang/String;)V

    .line 701
    .local v3, "ssdpSock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    invoke-static {}, Lcom/samsung/upnp/Device;->notifyWait()V

    .line 702
    invoke-virtual {v3, v2}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->post(Lcom/samsung/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 703
    return-void
.end method

.method public clearSID()V
    .locals 2

    .prologue
    .line 945
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/Service;->setSID(Ljava/lang/String;)V

    .line 946
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/upnp/Service;->setTimeout(J)V

    .line 947
    return-void
.end method

.method public getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getActionList()Lcom/samsung/upnp/ActionList;

    move-result-object v1

    .line 556
    .local v1, "actionList":Lcom/samsung/upnp/ActionList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ActionList;->size()I

    move-result v3

    .line 557
    .local v3, "nActions":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 565
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 558
    :cond_0
    invoke-virtual {v1, v2}, Lcom/samsung/upnp/ActionList;->getAction(I)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 559
    .local v0, "action":Lcom/samsung/upnp/Action;
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v4

    .line 560
    .local v4, "name":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 557
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 562
    :cond_2
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1
.end method

.method public getActionList()Lcom/samsung/upnp/ActionList;
    .locals 9

    .prologue
    .line 534
    new-instance v1, Lcom/samsung/upnp/ActionList;

    invoke-direct {v1}, Lcom/samsung/upnp/ActionList;-><init>()V

    .line 535
    .local v1, "actionList":Lcom/samsung/upnp/ActionList;
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getSCPDNode()Lcom/samsung/xml/Node;

    move-result-object v6

    .line 536
    .local v6, "scdpNode":Lcom/samsung/xml/Node;
    if-nez v6, :cond_1

    .line 550
    :cond_0
    return-object v1

    .line 538
    :cond_1
    const-string v8, "actionList"

    invoke-virtual {v6, v8}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 539
    .local v2, "actionListNode":Lcom/samsung/xml/Node;
    if-eqz v2, :cond_0

    .line 541
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v7

    .line 542
    .local v7, "serviceNode":Lcom/samsung/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v4

    .line 543
    .local v4, "nNode":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 544
    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v5

    .line 545
    .local v5, "node":Lcom/samsung/xml/Node;
    invoke-static {v5}, Lcom/samsung/upnp/Action;->isActionNode(Lcom/samsung/xml/Node;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 543
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 547
    :cond_2
    new-instance v0, Lcom/samsung/upnp/Action;

    invoke-direct {v0, v7, v5}, Lcom/samsung/upnp/Action;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    .line 548
    .local v0, "action":Lcom/samsung/upnp/Action;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ActionList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getControlURL()Ljava/lang/String;
    .locals 6

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 307
    .local v0, "d":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_0

    .line 308
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "controlURL"

    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 317
    :goto_0
    return-object v4

    .line 311
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 312
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 314
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "controlURL"

    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "path":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    .line 317
    invoke-static {v3, v2}, Lcom/samsung/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getDevice()Lcom/samsung/upnp/Device;
    .locals 3

    .prologue
    .line 178
    new-instance v0, Lcom/samsung/upnp/Device;

    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    return-object v0
.end method

.method public getEventSubURL()Ljava/lang/String;
    .locals 6

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 341
    .local v0, "d":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "eventSubURL"

    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 351
    :goto_0
    return-object v4

    .line 345
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 348
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "eventSubURL"

    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 349
    .local v2, "path":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    .line 351
    invoke-static {v3, v2}, Lcom/samsung/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getRootDevice()Lcom/samsung/upnp/Device;
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getRootDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    return-object v0
.end method

.method public getSCPDData()[B
    .locals 4

    .prologue
    .line 517
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getSCPDNode()Lcom/samsung/xml/Node;

    move-result-object v1

    .line 518
    .local v1, "scpdNode":Lcom/samsung/xml/Node;
    if-nez v1, :cond_0

    .line 519
    const/4 v2, 0x0

    new-array v2, v2, [B

    .line 525
    :goto_0
    return-object v2

    .line 521
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 522
    .local v0, "desc":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "<?xml version=\"1.0\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 523
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 525
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    goto :goto_0
.end method

.method public getSCPDURL()Ljava/lang/String;
    .locals 6

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 254
    .local v0, "d":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "SCPDURL"

    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 264
    :goto_0
    return-object v4

    .line 258
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 261
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "SCPDURL"

    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 262
    .local v2, "path":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    .line 264
    invoke-static {v3, v2}, Lcom/samsung/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 935
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getServiceData()Lcom/samsung/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/ServiceData;->getSID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "serviceId"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/upnp/Service;->serviceNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method public getServiceStateTable()Lcom/samsung/upnp/ServiceStateTable;
    .locals 9

    .prologue
    .line 574
    new-instance v6, Lcom/samsung/upnp/ServiceStateTable;

    invoke-direct {v6}, Lcom/samsung/upnp/ServiceStateTable;-><init>()V

    .line 575
    .local v6, "stateTable":Lcom/samsung/upnp/ServiceStateTable;
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getSCPDNode()Lcom/samsung/xml/Node;

    move-result-object v3

    .line 576
    .local v3, "scpdNode":Lcom/samsung/xml/Node;
    if-nez v3, :cond_1

    .line 593
    :cond_0
    return-object v6

    .line 580
    :cond_1
    const-string v8, "serviceStateTable"

    invoke-virtual {v3, v8}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v7

    .line 581
    .local v7, "stateTableNode":Lcom/samsung/xml/Node;
    if-eqz v7, :cond_0

    .line 584
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    .line 585
    .local v4, "serviceNode":Lcom/samsung/xml/Node;
    invoke-virtual {v7}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v1

    .line 586
    .local v1, "nNode":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 587
    invoke-virtual {v7, v0}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 588
    .local v2, "node":Lcom/samsung/xml/Node;
    invoke-static {v2}, Lcom/samsung/upnp/StateVariable;->isStateVariableNode(Lcom/samsung/xml/Node;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 586
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 590
    :cond_2
    new-instance v5, Lcom/samsung/upnp/StateVariable;

    invoke-direct {v5, v4, v2}, Lcom/samsung/upnp/StateVariable;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    .line 591
    .local v5, "serviceVar":Lcom/samsung/upnp/StateVariable;
    invoke-virtual {v6, v5}, Lcom/samsung/upnp/ServiceStateTable;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getServiceType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 200
    .local v0, "serviceNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_0

    .line 201
    const-string v1, ""

    .line 202
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v1

    const-string v2, "serviceType"

    invoke-virtual {v1, v2}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 598
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceStateTable()Lcom/samsung/upnp/ServiceStateTable;

    move-result-object v1

    .line 599
    .local v1, "stateTable":Lcom/samsung/upnp/ServiceStateTable;
    invoke-virtual {v1}, Lcom/samsung/upnp/ServiceStateTable;->size()I

    move-result v2

    .line 600
    .local v2, "tableSize":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 608
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 601
    :cond_0
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ServiceStateTable;->getStateVariable(I)Lcom/samsung/upnp/StateVariable;

    move-result-object v3

    .line 602
    .local v3, "var":Lcom/samsung/upnp/StateVariable;
    invoke-virtual {v3}, Lcom/samsung/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v4

    .line 603
    .local v4, "varName":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 600
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 605
    :cond_2
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1
.end method

.method public getSubscriber(Ljava/lang/String;)Lcom/samsung/upnp/event/Subscriber;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 764
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getSubscriberList()Lcom/samsung/upnp/event/SubscriberList;

    move-result-object v3

    .line 765
    .local v3, "subList":Lcom/samsung/upnp/event/SubscriberList;
    invoke-virtual {v3}, Lcom/samsung/upnp/event/SubscriberList;->size()I

    move-result v4

    .line 766
    .local v4, "subListCnt":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 776
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 767
    :cond_0
    invoke-virtual {v3, v0}, Lcom/samsung/upnp/event/SubscriberList;->getSubscriber(I)Lcom/samsung/upnp/event/Subscriber;

    move-result-object v2

    .line 768
    .local v2, "sub":Lcom/samsung/upnp/event/Subscriber;
    if-nez v2, :cond_2

    .line 766
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 770
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/upnp/event/Subscriber;->getSID()Ljava/lang/String;

    move-result-object v1

    .line 771
    .local v1, "sid":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 773
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1
.end method

.method public getSubscriberList()Lcom/samsung/upnp/event/SubscriberList;
    .locals 1

    .prologue
    .line 749
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getServiceData()Lcom/samsung/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/ServiceData;->getSubscriberList()Lcom/samsung/upnp/event/SubscriberList;

    move-result-object v0

    return-object v0
.end method

.method public hasSID()Z
    .locals 1

    .prologue
    .line 951
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/util/StringUtil;->hasData(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasStateVariable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 613
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isControlURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getControlURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Service;->isURL(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isEventSubURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getEventSubURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Service;->isURL(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSCPDURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getSCPDURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/upnp/Service;->isURL(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isService(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 622
    if-nez p1, :cond_1

    .line 630
    :cond_0
    :goto_0
    return v0

    .line 626
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 627
    goto :goto_0

    .line 628
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 629
    goto :goto_0
.end method

.method public isSubscribed()Z
    .locals 1

    .prologue
    .line 956
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->hasSID()Z

    move-result v0

    return v0
.end method

.method public loadSCPD(Ljava/io/InputStream;)Z
    .locals 4
    .param p1, "fis"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 393
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v1

    .line 394
    .local v1, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v1, p1}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v2

    .line 395
    .local v2, "scpdNode":Lcom/samsung/xml/Node;
    if-nez v2, :cond_0

    .line 396
    const/4 v3, 0x0

    .line 399
    :goto_0
    return v3

    .line 397
    :cond_0
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getServiceData()Lcom/samsung/upnp/xml/ServiceData;

    move-result-object v0

    .line 398
    .local v0, "data":Lcom/samsung/upnp/xml/ServiceData;
    invoke-virtual {v0, v2}, Lcom/samsung/upnp/xml/ServiceData;->setSCPDNode(Lcom/samsung/xml/Node;)V

    .line 399
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public notify(Lcom/samsung/upnp/StateVariable;)V
    .locals 6
    .param p1, "stateVar"    # Lcom/samsung/upnp/StateVariable;

    .prologue
    .line 814
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getSubscriberList()Lcom/samsung/upnp/event/SubscriberList;

    move-result-object v2

    .line 819
    .local v2, "subList":Lcom/samsung/upnp/event/SubscriberList;
    invoke-virtual {v2}, Lcom/samsung/upnp/event/SubscriberList;->size()I

    move-result v3

    .line 820
    .local v3, "subListCnt":I
    new-array v4, v3, [Lcom/samsung/upnp/event/Subscriber;

    .line 821
    .local v4, "subs":[Lcom/samsung/upnp/event/Subscriber;
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 823
    const/4 v0, 0x0

    :goto_1
    if-lt v0, v3, :cond_1

    .line 830
    invoke-virtual {v2}, Lcom/samsung/upnp/event/SubscriberList;->size()I

    move-result v3

    .line 831
    new-array v4, v3, [Lcom/samsung/upnp/event/Subscriber;

    .line 832
    const/4 v0, 0x0

    :goto_2
    if-lt v0, v3, :cond_3

    .line 834
    const/4 v0, 0x0

    :goto_3
    if-lt v0, v3, :cond_4

    .line 842
    return-void

    .line 822
    :cond_0
    invoke-virtual {v2, v0}, Lcom/samsung/upnp/event/SubscriberList;->getSubscriber(I)Lcom/samsung/upnp/event/Subscriber;

    move-result-object v5

    aput-object v5, v4, v0

    .line 821
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 824
    :cond_1
    aget-object v1, v4, v0

    .line 825
    .local v1, "sub":Lcom/samsung/upnp/event/Subscriber;
    invoke-virtual {v1}, Lcom/samsung/upnp/event/Subscriber;->isExpired()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 826
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/Service;->removeSubscriber(Lcom/samsung/upnp/event/Subscriber;)V

    .line 823
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 833
    .end local v1    # "sub":Lcom/samsung/upnp/event/Subscriber;
    :cond_3
    invoke-virtual {v2, v0}, Lcom/samsung/upnp/event/SubscriberList;->getSubscriber(I)Lcom/samsung/upnp/event/Subscriber;

    move-result-object v5

    aput-object v5, v4, v0

    .line 832
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 835
    :cond_4
    aget-object v1, v4, v0

    .line 836
    .restart local v1    # "sub":Lcom/samsung/upnp/event/Subscriber;
    invoke-direct {p0, v1, p1}, Lcom/samsung/upnp/Service;->notify(Lcom/samsung/upnp/event/Subscriber;Lcom/samsung/upnp/StateVariable;)Z

    .line 834
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public notifyAllStateVariablesToSubscriber(Lcom/samsung/upnp/event/Subscriber;)V
    .locals 1
    .param p1, "sub"    # Lcom/samsung/upnp/event/Subscriber;

    .prologue
    .line 845
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceStateTable()Lcom/samsung/upnp/ServiceStateTable;

    move-result-object v0

    .line 846
    .local v0, "stateTable":Lcom/samsung/upnp/ServiceStateTable;
    invoke-direct {p0, p1, v0}, Lcom/samsung/upnp/Service;->notify(Lcom/samsung/upnp/event/Subscriber;Lcom/samsung/upnp/ServiceStateTable;)Z

    .line 847
    return-void
.end method

.method public removeSubscriber(Lcom/samsung/upnp/event/Subscriber;)V
    .locals 1
    .param p1, "sub"    # Lcom/samsung/upnp/event/Subscriber;

    .prologue
    .line 759
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getSubscriberList()Lcom/samsung/upnp/event/SubscriberList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/event/SubscriberList;->remove(Ljava/lang/Object;)Z

    .line 760
    return-void
.end method

.method public serviceSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z
    .locals 6
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 707
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v4

    .line 709
    .local v4, "ssdpST":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 710
    const/4 v5, 0x0

    .line 726
    :goto_0
    return v5

    .line 712
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 714
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getNotifyServiceTypeNT()Ljava/lang/String;

    move-result-object v1

    .line 715
    .local v1, "serviceNT":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getNotifyServiceTypeUSN()Ljava/lang/String;

    move-result-object v3

    .line 717
    .local v3, "serviceUSN":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/upnp/device/ST;->isAllDevice(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 718
    invoke-virtual {v0, p1, v1, v3}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 726
    :cond_1
    :goto_1
    const/4 v5, 0x1

    goto :goto_0

    .line 720
    :cond_2
    invoke-static {v4}, Lcom/samsung/upnp/device/ST;->isURNService(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 721
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v2

    .line 722
    .local v2, "serviceType":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 723
    invoke-virtual {v0, p1, v2, v3}, Lcom/samsung/upnp/Device;->postSearchResponse(Lcom/samsung/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public setActionListener(Lcom/samsung/upnp/control/ActionListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/upnp/control/ActionListener;

    .prologue
    .line 979
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getActionList()Lcom/samsung/upnp/ActionList;

    move-result-object v1

    .line 980
    .local v1, "actionList":Lcom/samsung/upnp/ActionList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ActionList;->size()I

    move-result v3

    .line 981
    .local v3, "nActions":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 985
    return-void

    .line 982
    :cond_0
    invoke-virtual {v1, v2}, Lcom/samsung/upnp/ActionList;->getAction(I)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 983
    .local v0, "action":Lcom/samsung/upnp/Action;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Action;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 981
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V
    .locals 4
    .param p1, "queryListener"    # Lcom/samsung/upnp/control/QueryListener;

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/samsung/upnp/Service;->getServiceStateTable()Lcom/samsung/upnp/ServiceStateTable;

    move-result-object v1

    .line 736
    .local v1, "stateTable":Lcom/samsung/upnp/ServiceStateTable;
    invoke-virtual {v1}, Lcom/samsung/upnp/ServiceStateTable;->size()I

    move-result v2

    .line 737
    .local v2, "tableSize":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 741
    return-void

    .line 738
    :cond_0
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ServiceStateTable;->getStateVariable(I)Lcom/samsung/upnp/StateVariable;

    move-result-object v3

    .line 739
    .local v3, "var":Lcom/samsung/upnp/StateVariable;
    invoke-virtual {v3, p1}, Lcom/samsung/upnp/StateVariable;->setQueryListener(Lcom/samsung/upnp/control/QueryListener;)V

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 940
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getServiceData()Lcom/samsung/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/ServiceData;->setSID(Ljava/lang/String;)V

    .line 941
    return-void
.end method

.method public setTimeout(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 970
    invoke-direct {p0}, Lcom/samsung/upnp/Service;->getServiceData()Lcom/samsung/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/upnp/xml/ServiceData;->setTimeout(J)V

    .line 971
    return-void
.end method
