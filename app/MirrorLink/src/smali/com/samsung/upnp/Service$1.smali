.class Lcom/samsung/upnp/Service$1;
.super Ljava/lang/Object;
.source "Service.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/upnp/Service;->notify(Lcom/samsung/upnp/event/Subscriber;Lcom/samsung/upnp/StateVariable;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/upnp/Service;

.field private final synthetic val$host:Ljava/lang/String;

.field private final synthetic val$port:I

.field private final synthetic val$sub:Lcom/samsung/upnp/event/Subscriber;

.field private final synthetic val$value:Ljava/lang/String;

.field private final synthetic val$varName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/upnp/Service;Lcom/samsung/upnp/event/Subscriber;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/upnp/Service$1;->this$0:Lcom/samsung/upnp/Service;

    iput-object p2, p0, Lcom/samsung/upnp/Service$1;->val$sub:Lcom/samsung/upnp/event/Subscriber;

    iput-object p3, p0, Lcom/samsung/upnp/Service$1;->val$varName:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/upnp/Service$1;->val$value:Ljava/lang/String;

    iput-object p5, p0, Lcom/samsung/upnp/Service$1;->val$host:Ljava/lang/String;

    iput p6, p0, Lcom/samsung/upnp/Service$1;->val$port:I

    .line 792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 797
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "UPNP CHECK NEW THREAD"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 798
    new-instance v0, Lcom/samsung/upnp/event/NotifyRequest;

    invoke-direct {v0}, Lcom/samsung/upnp/event/NotifyRequest;-><init>()V

    .line 799
    .local v0, "notifyReq":Lcom/samsung/upnp/event/NotifyRequest;
    iget-object v1, p0, Lcom/samsung/upnp/Service$1;->val$sub:Lcom/samsung/upnp/event/Subscriber;

    iget-object v2, p0, Lcom/samsung/upnp/Service$1;->val$varName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/upnp/Service$1;->val$value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/upnp/event/NotifyRequest;->setRequest(Lcom/samsung/upnp/event/Subscriber;Ljava/lang/String;Ljava/lang/String;)Z

    .line 800
    iget-object v1, p0, Lcom/samsung/upnp/Service$1;->val$host:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/upnp/Service$1;->val$port:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/event/NotifyRequest;->post(Ljava/lang/String;I)Lcom/samsung/http/HTTPResponse;

    .line 801
    return-void
.end method
