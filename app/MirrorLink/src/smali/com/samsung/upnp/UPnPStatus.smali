.class public Lcom/samsung/upnp/UPnPStatus;
.super Ljava/lang/Object;
.source "UPnPStatus.java"


# static fields
.field public static final ACTION_FAILED:I = 0x1f5

.field public static final INVALID_ACTION:I = 0x191

.field public static final INVALID_ARGS:I = 0x192

.field public static final INVALID_CONNECTION_REFERENCE:I = 0x2c2

.field public static final INVALID_SEARCH_CRITERIA:I = 0x2c4

.field public static final INVALID_SORT_CRITERIA:I = 0x2c5

.field public static final INVALID_VAR:I = 0x194

.field public static final NO_SUCH_CONTAINER:I = 0x2c6

.field public static final NO_SUCH_OBJECT:I = 0x2bd

.field public static final OUT_OF_SYNC:I = 0x193

.field public static final PRECONDITION_FAILED:I = 0x19c

.field public static final RESTRICTED_OBJECT:I = 0x2c7

.field public static final RESTRICTED_PARENT_OBJECT:I = 0x2c9


# instance fields
.field private code:I

.field private description:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/UPnPStatus;->setCode(I)V

    .line 76
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/UPnPStatus;->setDescription(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public static final code2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 48
    sparse-switch p0, :sswitch_data_0

    .line 63
    const-string v0, ""

    :goto_0
    return-object v0

    .line 49
    :sswitch_0
    const-string v0, "Invalid Action"

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "Invalid Args"

    goto :goto_0

    .line 51
    :sswitch_2
    const-string v0, "Out of Sync"

    goto :goto_0

    .line 52
    :sswitch_3
    const-string v0, "Invalid Var"

    goto :goto_0

    .line 53
    :sswitch_4
    const-string v0, "Precondition Failed"

    goto :goto_0

    .line 54
    :sswitch_5
    const-string v0, "Action Failed"

    goto :goto_0

    .line 55
    :sswitch_6
    const-string v0, "No such object"

    goto :goto_0

    .line 56
    :sswitch_7
    const-string v0, "Invalid Connection Reference"

    goto :goto_0

    .line 57
    :sswitch_8
    const-string v0, "Invalid Search Criteria"

    goto :goto_0

    .line 58
    :sswitch_9
    const-string v0, "No Such Container"

    goto :goto_0

    .line 59
    :sswitch_a
    const-string v0, "Invalid Sort Criteria"

    goto :goto_0

    .line 60
    :sswitch_b
    const-string v0, "Restricted Object"

    goto :goto_0

    .line 61
    :sswitch_c
    const-string v0, "Restricted Parent Object"

    goto :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_0
        0x192 -> :sswitch_1
        0x193 -> :sswitch_2
        0x194 -> :sswitch_3
        0x19c -> :sswitch_4
        0x1f5 -> :sswitch_5
        0x2bd -> :sswitch_6
        0x2c2 -> :sswitch_7
        0x2c4 -> :sswitch_8
        0x2c5 -> :sswitch_a
        0x2c6 -> :sswitch_9
        0x2c7 -> :sswitch_b
        0x2c9 -> :sswitch_c
    .end sparse-switch
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/upnp/UPnPStatus;->code:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/upnp/UPnPStatus;->description:Ljava/lang/String;

    return-object v0
.end method

.method public setCode(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/upnp/UPnPStatus;->code:I

    .line 91
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/upnp/UPnPStatus;->description:Ljava/lang/String;

    .line 99
    return-void
.end method
