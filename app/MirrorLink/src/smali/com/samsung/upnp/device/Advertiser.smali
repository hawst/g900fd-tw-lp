.class public Lcom/samsung/upnp/device/Advertiser;
.super Lcom/samsung/util/ThreadCore;
.source "Advertiser.java"


# instance fields
.field private device:Lcom/samsung/upnp/Device;


# direct methods
.method public constructor <init>(Lcom/samsung/upnp/Device;)V
    .locals 0
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/util/ThreadCore;-><init>()V

    .line 38
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/device/Advertiser;->setDevice(Lcom/samsung/upnp/Device;)V

    .line 39
    return-void
.end method


# virtual methods
.method public getDevice()Lcom/samsung/upnp/Device;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/upnp/device/Advertiser;->device:Lcom/samsung/upnp/Device;

    return-object v0
.end method

.method public run()V
    .locals 15

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/samsung/upnp/device/Advertiser;->getDevice()Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 64
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getLeaseTime()I

    move-result v7

    int-to-long v3, v7

    .line 66
    .local v3, "leaseTime":J
    const-wide/16 v7, 0x4

    div-long v7, v3, v7

    long-to-float v9, v3

    float-to-double v9, v9

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v11

    const-wide/high16 v13, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v11, v13

    mul-double/2addr v9, v11

    double-to-long v9, v9

    add-long v5, v7, v9

    .line 68
    .local v5, "notifyInterval":J
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/upnp/device/Advertiser;->isRunnable()Z

    move-result v7

    if-nez v7, :cond_1

    .line 86
    return-void

    .line 72
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    int-to-long v7, v2

    cmp-long v7, v7, v5

    if-ltz v7, :cond_3

    .line 81
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/device/Advertiser;->isRunnable()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 83
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->announce()V

    goto :goto_0

    .line 74
    :cond_3
    const-wide/16 v7, 0x3e8

    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V

    .line 75
    invoke-virtual {p0}, Lcom/samsung/upnp/device/Advertiser;->isRunnable()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_2

    .line 72
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method public setDevice(Lcom/samsung/upnp/Device;)V
    .locals 0
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/upnp/device/Advertiser;->device:Lcom/samsung/upnp/Device;

    .line 50
    return-void
.end method
