.class public Lcom/samsung/upnp/ServiceList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "ServiceList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "serviceList"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 43
    return-void
.end method


# virtual methods
.method public getService(I)Lcom/samsung/upnp/Service;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 53
    .local v0, "obj":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ServiceList;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 56
    .end local v0    # "obj":Ljava/lang/Object;
    :goto_0
    check-cast v0, Lcom/samsung/upnp/Service;

    return-object v0

    .line 55
    .restart local v0    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    goto :goto_0
.end method
