.class public Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "SSDPNotifySocketList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/upnp/ssdp/SSDPNotifySocket;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->size()I

    move-result v1

    .line 103
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->clear()V

    .line 110
    return-void

    .line 104
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->getSSDPNotifySocket(I)Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-result-object v2

    .line 105
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    if-nez v2, :cond_1

    .line 103
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->close()Z

    goto :goto_1
.end method

.method public getSSDPNotifySocket(I)Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    .locals 4
    .param p1, "n"    # I

    .prologue
    .line 47
    const/4 v2, 0x0

    .line 49
    .local v2, "ssdpSoc":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-object v2

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public open()Z
    .locals 5

    .prologue
    .line 91
    invoke-static {}, Lcom/samsung/net/HostInterface;->getNHostAddresses()I

    move-result v2

    .line 92
    .local v2, "nHostAddrs":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 97
    const/4 v4, 0x1

    return v4

    .line 93
    :cond_0
    invoke-static {v1}, Lcom/samsung/net/HostInterface;->getHostAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "bindAddr":Ljava/lang/String;
    new-instance v3, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    invoke-direct {v3, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;-><init>(Ljava/lang/String;)V

    .line 95
    .local v3, "ssdpNotifySocket":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->add(Ljava/lang/Object;)Z

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setControlPoint(Lcom/samsung/upnp/ControlPoint;)V
    .locals 3
    .param p1, "ctrlPoint"    # Lcom/samsung/upnp/ControlPoint;

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->size()I

    move-result v1

    .line 63
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 69
    return-void

    .line 64
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->getSSDPNotifySocket(I)Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-result-object v2

    .line 65
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    if-nez v2, :cond_1

    .line 63
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->setControlPoint(Lcom/samsung/upnp/ControlPoint;)V

    goto :goto_1
.end method

.method public setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V
    .locals 3
    .param p1, "pmcp"    # Lcom/samsung/pmr/PersonalMessageControlPoint;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->size()I

    move-result v1

    .line 77
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 83
    return-void

    .line 78
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->getSSDPNotifySocket(I)Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-result-object v2

    .line 79
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    if-nez v2, :cond_1

    .line 77
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V

    goto :goto_1
.end method

.method public start()V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->size()I

    move-result v1

    .line 119
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 125
    return-void

    .line 120
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->getSSDPNotifySocket(I)Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-result-object v2

    .line 121
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    if-nez v2, :cond_1

    .line 119
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->start()V

    goto :goto_1
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->size()I

    move-result v1

    .line 130
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 136
    return-void

    .line 131
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->getSSDPNotifySocket(I)Lcom/samsung/upnp/ssdp/SSDPNotifySocket;

    move-result-object v2

    .line 132
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPNotifySocket;
    if-nez v2, :cond_1

    .line 130
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPNotifySocket;->stop()V

    goto :goto_1
.end method
