.class public Lcom/samsung/upnp/ssdp/HTTPMUSocket;
.super Ljava/lang/Object;
.source "HTTPMUSocket.java"


# instance fields
.field protected bindAddress:Ljava/lang/String;

.field private ssdpMultiGroup:Ljava/net/InetSocketAddress;

.field private ssdpMultiIf:Ljava/net/NetworkInterface;

.field private ssdpMultiSock:Ljava/net/MulticastSocket;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiGroup:Ljava/net/InetSocketAddress;

    .line 52
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    .line 53
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiIf:Ljava/net/NetworkInterface;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->bindAddress:Ljava/lang/String;

    .line 63
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 164
    iget-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    if-nez v2, :cond_0

    .line 177
    :goto_0
    return v1

    .line 168
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiGroup:Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiIf:Ljava/net/NetworkInterface;

    invoke-virtual {v2, v3, v4}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 169
    iget-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 170
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMulticastInetAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiGroup:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public open(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 6
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "bindAddr"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 118
    :try_start_0
    new-instance v3, Ljava/net/MulticastSocket;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/net/MulticastSocket;-><init>(Ljava/net/SocketAddress;)V

    iput-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    .line 119
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/net/MulticastSocket;->setReuseAddress(Z)V

    .line 120
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p2}, Ljava/net/InetSocketAddress;-><init>(I)V

    .line 121
    .local v0, "bindSockAddr":Ljava/net/InetSocketAddress;
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v3, v0}, Ljava/net/MulticastSocket;->bind(Ljava/net/SocketAddress;)V

    .line 122
    new-instance v3, Ljava/net/InetSocketAddress;

    invoke-static {}, Lcom/samsung/upnp/UPnP;->getMultiGroupAddr()Ljava/net/InetAddress;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiGroup:Ljava/net/InetSocketAddress;

    .line 124
    invoke-static {p3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    invoke-static {v3}, Ljava/net/NetworkInterface;->getByInetAddress(Ljava/net/InetAddress;)Ljava/net/NetworkInterface;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiIf:Ljava/net/NetworkInterface;

    .line 125
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    iget-object v4, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiGroup:Ljava/net/InetSocketAddress;

    iget-object v5, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiIf:Ljava/net/NetworkInterface;

    invoke-virtual {v3, v4, v5}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 126
    iput-object p3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->bindAddress:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .end local v0    # "bindSockAddr":Ljava/net/InetSocketAddress;
    :goto_0
    return v2

    .line 128
    :catch_0
    move-exception v1

    .line 129
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 130
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public post(Lcom/samsung/http/HTTPRequest;)Z
    .locals 3
    .param p1, "req"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 226
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->send(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public receive()Lcom/samsung/upnp/ssdp/SSDPPacket;
    .locals 5

    .prologue
    .line 239
    const/16 v3, 0x400

    new-array v2, v3, [B

    .line 240
    .local v2, "ssdvRecvBuf":[B
    new-instance v1, Lcom/samsung/upnp/ssdp/SSDPPacket;

    array-length v3, v2

    invoke-direct {v1, v2, v3}, Lcom/samsung/upnp/ssdp/SSDPPacket;-><init>([BI)V

    .line 241
    .local v1, "recvPacket":Lcom/samsung/upnp/ssdp/SSDPPacket;
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->bindAddress:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/ssdp/SSDPPacket;->setLocalAddress(Ljava/lang/String;)V

    .line 244
    :try_start_0
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 245
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/samsung/upnp/ssdp/SSDPPacket;->setTimeStamp(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :goto_0
    return-object v1

    .line 248
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "HTTPMUSocket"

    const-string v4, "Exception"

    invoke-static {v3, v4}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public send(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 7
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "bindAddr"    # Ljava/lang/String;
    .param p3, "bindPort"    # I

    .prologue
    .line 186
    const/4 v2, 0x0

    .line 188
    .local v2, "msock":Ljava/net/MulticastSocket;
    if-eqz p2, :cond_0

    if-lez p3, :cond_0

    .line 189
    :try_start_0
    new-instance v3, Ljava/net/MulticastSocket;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/net/MulticastSocket;-><init>(Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .local v3, "msock":Ljava/net/MulticastSocket;
    :try_start_1
    new-instance v4, Ljava/net/InetSocketAddress;

    invoke-direct {v4, p2, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/net/MulticastSocket;->bind(Ljava/net/SocketAddress;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 194
    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiIf:Ljava/net/NetworkInterface;

    invoke-virtual {v2, v4}, Ljava/net/MulticastSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 195
    new-instance v0, Ljava/net/DatagramPacket;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/upnp/ssdp/HTTPMUSocket;->ssdpMultiGroup:Ljava/net/InetSocketAddress;

    invoke-direct {v0, v4, v5, v6}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    .line 197
    .local v0, "dgmPacket":Ljava/net/DatagramPacket;
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Ljava/net/MulticastSocket;->setTimeToLive(I)V

    .line 198
    invoke-virtual {v2, v0}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V

    .line 199
    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 207
    const/4 v4, 0x1

    .end local v0    # "dgmPacket":Ljava/net/DatagramPacket;
    :goto_1
    return v4

    .line 193
    :cond_0
    new-instance v3, Ljava/net/MulticastSocket;

    invoke-direct {v3}, Ljava/net/MulticastSocket;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .restart local v3    # "msock":Ljava/net/MulticastSocket;
    move-object v2, v3

    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    goto :goto_0

    .line 201
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    if-eqz v2, :cond_1

    .line 203
    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 204
    :cond_1
    invoke-static {v1}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 205
    const/4 v4, 0x0

    goto :goto_1

    .line 201
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .restart local v3    # "msock":Ljava/net/MulticastSocket;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    goto :goto_2
.end method
