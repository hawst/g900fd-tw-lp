.class public Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "SSDPSearchResponseSocketList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 42
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->size()I

    move-result v1

    .line 118
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->clear()V

    .line 123
    return-void

    .line 119
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-result-object v2

    .line 120
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->close()Z

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    .locals 4
    .param p1, "n"    # I

    .prologue
    .line 78
    monitor-enter p0

    const/4 v2, 0x0

    .line 80
    .local v2, "ssdpSoc":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :goto_0
    monitor-exit p0

    return-object v2

    .line 81
    :catch_0
    move-exception v1

    .line 82
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 78
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public open(I)Z
    .locals 6
    .param p1, "port"    # I

    .prologue
    .line 94
    :try_start_0
    invoke-static {}, Lcom/samsung/net/HostInterface;->getNHostAddresses()I

    move-result v3

    .line 95
    .local v3, "nHostAddrs":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 107
    const/4 v5, 0x1

    .end local v2    # "n":I
    .end local v3    # "nHostAddrs":I
    :goto_1
    return v5

    .line 96
    .restart local v2    # "n":I
    .restart local v3    # "nHostAddrs":I
    :cond_0
    invoke-static {v2}, Lcom/samsung/net/HostInterface;->getHostAddress(I)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "bindAddr":Ljava/lang/String;
    new-instance v4, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    invoke-direct {v4, v0, p1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;-><init>(Ljava/lang/String;I)V

    .line 98
    .local v4, "socket":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {p0, v4}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "bindAddr":Ljava/lang/String;
    .end local v2    # "n":I
    .end local v3    # "nHostAddrs":I
    .end local v4    # "socket":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->stop()V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->close()V

    .line 104
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->clear()V

    .line 105
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public post(Lcom/samsung/upnp/ssdp/SSDPSearchRequest;)Z
    .locals 7
    .param p1, "req"    # Lcom/samsung/upnp/ssdp/SSDPSearchRequest;

    .prologue
    .line 157
    if-nez p1, :cond_1

    .line 158
    const/4 v3, 0x0

    .line 173
    :cond_0
    return v3

    .line 159
    :cond_1
    const/4 v3, 0x1

    .line 160
    .local v3, "ret":Z
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->size()I

    move-result v2

    .line 161
    .local v2, "nSockets":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 162
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-result-object v4

    .line 163
    .local v4, "sock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    if-nez v4, :cond_3

    .line 161
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->getLocalAddress()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "bindAddr":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;->setLocalAddress(Ljava/lang/String;)V

    .line 167
    const-string v5, "239.255.255.250"

    .line 168
    .local v5, "ssdpAddr":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/net/HostInterface;->isIPv6Address(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 169
    invoke-static {}, Lcom/samsung/upnp/ssdp/SSDP;->getIPv6Address()Ljava/lang/String;

    move-result-object v5

    .line 170
    :cond_4
    const/16 v6, 0x76c

    invoke-virtual {v4, v5, v6, p1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/lang/String;ILcom/samsung/upnp/ssdp/SSDPSearchRequest;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 171
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public setControlPoint(Lcom/samsung/upnp/ControlPoint;)V
    .locals 3
    .param p1, "ctrlPoint"    # Lcom/samsung/upnp/ControlPoint;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->size()I

    move-result v1

    .line 51
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 55
    return-void

    .line 52
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-result-object v2

    .line 53
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->setControlPoint(Lcom/samsung/upnp/ControlPoint;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V
    .locals 3
    .param p1, "pmcp"    # Lcom/samsung/pmr/PersonalMessageControlPoint;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->size()I

    move-result v1

    .line 64
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 69
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-result-object v2

    .line 67
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v2, p1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->size()I

    move-result v2

    .line 132
    .local v2, "nSockets":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 140
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-result-object v3

    .line 135
    .local v3, "sock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    :try_start_0
    invoke-virtual {v3}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->start()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/NullPointerException;
    throw v0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->size()I

    move-result v1

    .line 145
    .local v1, "nSockets":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 149
    return-void

    .line 146
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->getSSDPSearchResponseSocket(I)Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;

    move-result-object v2

    .line 147
    .local v2, "sock":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->stop()V

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
