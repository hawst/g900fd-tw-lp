.class public Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;
.super Lcom/samsung/upnp/ssdp/HTTPUSocket;
.source "SSDPSearchResponseSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket$Worker;
    }
.end annotation


# instance fields
.field private controlPoint:Lcom/samsung/upnp/ControlPoint;

.field private deviceSearchResponseThread:Ljava/lang/Thread;

.field private pmcp:Lcom/samsung/pmr/PersonalMessageControlPoint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/samsung/upnp/ssdp/HTTPUSocket;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->controlPoint:Lcom/samsung/upnp/ControlPoint;

    .line 69
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->pmcp:Lcom/samsung/pmr/PersonalMessageControlPoint;

    .line 86
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    .line 38
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->setControlPoint(Lcom/samsung/upnp/ControlPoint;)V

    .line 39
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "bindAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/upnp/ssdp/HTTPUSocket;-><init>(Ljava/lang/String;I)V

    .line 53
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->controlPoint:Lcom/samsung/upnp/ControlPoint;

    .line 69
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->pmcp:Lcom/samsung/pmr/PersonalMessageControlPoint;

    .line 86
    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    .line 45
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->setControlPoint(Lcom/samsung/upnp/ControlPoint;)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V

    .line 47
    return-void
.end method


# virtual methods
.method public getControlPoint()Lcom/samsung/upnp/ControlPoint;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->controlPoint:Lcom/samsung/upnp/ControlPoint;

    return-object v0
.end method

.method public getPersonalMessageCP()Lcom/samsung/pmr/PersonalMessageControlPoint;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->pmcp:Lcom/samsung/pmr/PersonalMessageControlPoint;

    return-object v0
.end method

.method public post(Ljava/lang/String;ILcom/samsung/upnp/ssdp/SSDPSearchRequest;)Z
    .locals 1
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "req"    # Lcom/samsung/upnp/ssdp/SSDPSearchRequest;

    .prologue
    .line 164
    invoke-virtual {p3}, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public post(Ljava/lang/String;ILcom/samsung/upnp/ssdp/SSDPSearchResponse;)Z
    .locals 1
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "res"    # Lcom/samsung/upnp/ssdp/SSDPSearchResponse;

    .prologue
    .line 155
    invoke-virtual {p3}, Lcom/samsung/upnp/ssdp/SSDPSearchResponse;->getHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 90
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    .line 92
    .local v3, "thisThread":Ljava/lang/Thread;
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->getControlPoint()Lcom/samsung/upnp/ControlPoint;

    move-result-object v0

    .line 93
    .local v0, "ctrlPoint":Lcom/samsung/upnp/ControlPoint;
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->getPersonalMessageCP()Lcom/samsung/pmr/PersonalMessageControlPoint;

    move-result-object v2

    .line 94
    .local v2, "pmcp":Lcom/samsung/pmr/PersonalMessageControlPoint;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    if-eq v5, v3, :cond_2

    .line 116
    :cond_1
    return-void

    .line 95
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 96
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->receive()Lcom/samsung/upnp/ssdp/SSDPPacket;

    move-result-object v1

    .line 97
    .local v1, "packet":Lcom/samsung/upnp/ssdp/SSDPPacket;
    if-eqz v1, :cond_1

    .line 99
    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/ControlPoint;->isValidResponsePacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 100
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket$Worker;

    invoke-direct {v5, p0, v1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket$Worker;-><init>(Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 101
    .local v4, "thread":Ljava/lang/Thread;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 102
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SEARCH : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 103
    :cond_3
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 106
    .end local v4    # "thread":Ljava/lang/Thread;
    :cond_4
    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->isValidAlivePacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket$Worker;

    invoke-direct {v5, p0, v1}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket$Worker;-><init>(Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 110
    .restart local v4    # "thread":Ljava/lang/Thread;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 111
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SEARCH : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 112
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public setControlPoint(Lcom/samsung/upnp/ControlPoint;)V
    .locals 0
    .param p1, "ctrlp"    # Lcom/samsung/upnp/ControlPoint;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->controlPoint:Lcom/samsung/upnp/ControlPoint;

    .line 58
    return-void
.end method

.method public setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V
    .locals 0
    .param p1, "pmcp"    # Lcom/samsung/pmr/PersonalMessageControlPoint;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->pmcp:Lcom/samsung/pmr/PersonalMessageControlPoint;

    .line 74
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 138
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    .line 139
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SEARCH MAIN"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->getLocalAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 142
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocket;->deviceSearchResponseThread:Ljava/lang/Thread;

    .line 147
    return-void
.end method
