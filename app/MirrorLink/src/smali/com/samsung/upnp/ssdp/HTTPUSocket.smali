.class public Lcom/samsung/upnp/ssdp/HTTPUSocket;
.super Ljava/lang/Object;
.source "HTTPUSocket.java"


# instance fields
.field private localAddr:Ljava/lang/String;

.field private ssdpUniSock:Ljava/net/DatagramSocket;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->localAddr:Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/HTTPUSocket;->open()Z

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "bindAddr"    # Ljava/lang/String;
    .param p2, "bindPort"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->localAddr:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, p1, p2}, Lcom/samsung/upnp/ssdp/HTTPUSocket;->open(Ljava/lang/String;I)Z

    .line 64
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 158
    iget-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    if-nez v2, :cond_0

    .line 170
    :goto_0
    return v1

    .line 162
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v2}, Ljava/net/DatagramSocket;->close()V

    .line 163
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 167
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    iget-object v1, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->localAddr:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->localAddr:Ljava/lang/String;

    .line 91
    :goto_0
    return-object v1

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v1}, Ljava/net/DatagramSocket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 88
    .local v0, "localAddrTemp":Ljava/net/InetAddress;
    if-eqz v0, :cond_1

    .line 89
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 91
    .end local v0    # "localAddrTemp":Ljava/net/InetAddress;
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public open()Z
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/HTTPUSocket;->close()Z

    .line 103
    :try_start_0
    new-instance v1, Ljava/net/DatagramSocket;

    invoke-direct {v1}, Ljava/net/DatagramSocket;-><init>()V

    iput-object v1, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 107
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public open(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "bindAddr"    # Ljava/lang/String;
    .param p2, "bindPort"    # I

    .prologue
    const/4 v2, 0x1

    .line 115
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/HTTPUSocket;->close()Z

    .line 119
    :try_start_0
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p2}, Ljava/net/InetSocketAddress;-><init>(I)V

    .line 120
    .local v0, "bindSock":Ljava/net/InetSocketAddress;
    new-instance v3, Ljava/net/DatagramSocket;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/net/DatagramSocket;-><init>(Ljava/net/SocketAddress;)V

    iput-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    .line 121
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/net/DatagramSocket;->setReuseAddress(Z)V

    .line 122
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v3, v0}, Ljava/net/DatagramSocket;->bind(Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ssdp/HTTPUSocket;->setLocalAddress(Ljava/lang/String;)V

    .line 131
    .end local v0    # "bindSock":Ljava/net/InetSocketAddress;
    :goto_0
    return v2

    .line 124
    :catch_0
    move-exception v1

    .line 125
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V

    .line 126
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public post(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 5
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 180
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    .line 181
    .local v2, "inetAddr":Ljava/net/InetAddress;
    new-instance v0, Ljava/net/DatagramPacket;

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v0, v3, v4, v2, p2}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 182
    .local v0, "dgmPacket":Ljava/net/DatagramPacket;
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    if-eqz v3, :cond_0

    .line 183
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v3, v0}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :cond_0
    const/4 v3, 0x1

    .end local v0    # "dgmPacket":Ljava/net/DatagramPacket;
    .end local v2    # "inetAddr":Ljava/net/InetAddress;
    :goto_0
    return v3

    .line 185
    :catch_0
    move-exception v1

    .line 190
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public receive()Lcom/samsung/upnp/ssdp/SSDPPacket;
    .locals 5

    .prologue
    .line 201
    const/16 v3, 0x400

    new-array v2, v3, [B

    .line 202
    .local v2, "ssdvRecvBuf":[B
    new-instance v1, Lcom/samsung/upnp/ssdp/SSDPPacket;

    array-length v3, v2

    invoke-direct {v1, v2, v3}, Lcom/samsung/upnp/ssdp/SSDPPacket;-><init>([BI)V

    .line 203
    .local v1, "recvPacket":Lcom/samsung/upnp/ssdp/SSDPPacket;
    invoke-virtual {p0}, Lcom/samsung/upnp/ssdp/HTTPUSocket;->getLocalAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/upnp/ssdp/SSDPPacket;->setLocalAddress(Ljava/lang/String;)V

    .line 205
    :try_start_0
    iget-object v3, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->ssdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/samsung/upnp/ssdp/SSDPPacket;->setTimeStamp(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    .end local v1    # "recvPacket":Lcom/samsung/upnp/ssdp/SSDPPacket;
    :goto_0
    return-object v1

    .line 208
    .restart local v1    # "recvPacket":Lcom/samsung/upnp/ssdp/SSDPPacket;
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLocalAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/upnp/ssdp/HTTPUSocket;->localAddr:Ljava/lang/String;

    .line 80
    return-void
.end method
