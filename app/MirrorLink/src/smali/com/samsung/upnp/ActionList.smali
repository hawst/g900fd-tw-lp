.class public Lcom/samsung/upnp/ActionList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "ActionList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/upnp/Action;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "actionList"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method public getAction(I)Lcom/samsung/upnp/Action;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ActionList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/Action;

    return-object v0
.end method
