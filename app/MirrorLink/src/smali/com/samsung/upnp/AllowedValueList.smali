.class public Lcom/samsung/upnp/AllowedValueList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "AllowedValueList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/upnp/AllowedValue;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "allowedValueList"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public getAllowedValue(I)Lcom/samsung/upnp/AllowedValue;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/AllowedValueList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/AllowedValue;

    return-object v0
.end method
