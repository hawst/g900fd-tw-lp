.class public Lcom/samsung/upnp/ControlPoint;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Lcom/samsung/http/HTTPRequestListener;
.implements Lcom/samsung/upnp/IControlPoint;


# static fields
.field private static final DEFAULT_EVENTSUB_PORT:I = 0x1f7a

.field private static final DEFAULT_EVENTSUB_URI:Ljava/lang/String; = "/evetSub"

.field private static final DEFAULT_EXPIRED_DEVICE_MONITORING_INTERVAL:I = 0x3c

.field private static final DEFAULT_SSDP_PORT:I = 0x1f48


# instance fields
.field private activityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private devNodeList:Lcom/samsung/xml/NodeList;

.field deviceChangeListenerList:Lcom/samsung/util/ListenerList;

.field private deviceDisposer:Lcom/samsung/upnp/device/Disposer;

.field private deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

.field private deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

.field private eventListenerList:Lcom/samsung/util/ListenerList;

.field private eventSubURI:Ljava/lang/String;

.field private expiredDeviceMonitoringInterval:J

.field private httpPort:I

.field private httpServerList:Lcom/samsung/http/HTTPServerList;

.field private nmprMode:Z

.field private renewSubscriber:Lcom/samsung/upnp/control/RenewSubscriber;

.field private searchMx:I

.field private searchTargetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ssdpNotifySocketList:Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

.field private ssdpPort:I

.field private ssdpSearchResponseSocketList:Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 123
    invoke-static {}, Lcom/samsung/upnp/UPnP;->initialize()V

    .line 124
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide v5, 0x40c3880000000000L    # 10000.0

    const-wide v3, 0x40bf7a0000000000L    # 8058.0

    .line 148
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    mul-double/2addr v0, v5

    add-double/2addr v0, v3

    double-to-int v0, v0

    add-int/lit16 v0, v0, 0x1f48

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    mul-double/2addr v1, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/upnp/ControlPoint;-><init>(II)V

    .line 149
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4
    .param p1, "ssdpPort"    # I
    .param p2, "httpPort"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput v2, p0, Lcom/samsung/upnp/ControlPoint;->ssdpPort:I

    .line 185
    iput v2, p0, Lcom/samsung/upnp/ControlPoint;->httpPort:I

    .line 214
    new-instance v0, Lcom/samsung/xml/NodeList;

    invoke-direct {v0}, Lcom/samsung/xml/NodeList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    .line 452
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    .line 483
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    .line 516
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    .line 651
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/upnp/ControlPoint;->searchMx:I

    .line 652
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    .line 738
    new-instance v0, Lcom/samsung/http/HTTPServerList;

    invoke-direct {v0}, Lcom/samsung/http/HTTPServerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->httpServerList:Lcom/samsung/http/HTTPServerList;

    .line 772
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    .line 869
    const-string v0, "/evetSub"

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventSubURI:Ljava/lang/String;

    .line 131
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    invoke-direct {v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->ssdpNotifySocketList:Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    .line 132
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    invoke-direct {v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;-><init>()V

    iput-object v0, p0, Lcom/samsung/upnp/ControlPoint;->ssdpSearchResponseSocketList:Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    .line 134
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ControlPoint;->setSSDPPort(I)V

    .line 135
    invoke-virtual {p0, p2}, Lcom/samsung/upnp/ControlPoint;->setHTTPPort(I)V

    .line 137
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/ControlPoint;->setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V

    .line 138
    const-wide/16 v0, 0x3c

    invoke-virtual {p0, v0, v1}, Lcom/samsung/upnp/ControlPoint;->setExpiredDeviceMonitoringInterval(J)V

    .line 140
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/upnp/control/RenewSubscriber;)V

    .line 142
    invoke-virtual {p0, v2}, Lcom/samsung/upnp/ControlPoint;->setNMPRMode(Z)V

    .line 143
    invoke-virtual {p0, v3}, Lcom/samsung/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/upnp/control/RenewSubscriber;)V

    .line 144
    return-void
.end method

.method private addActivityDevice(Ljava/lang/String;)V
    .locals 3
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 229
    const-string v0, "ControlPoint"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "add activit list udn :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    monitor-exit v1

    .line 233
    return-void

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private addDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 11
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 243
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v9

    .line 244
    .local v9, "usn":Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 246
    .local v8, "udn":Ljava/lang/String;
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->addActivityDevice(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0, v8}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 251
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 253
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    .line 296
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "location":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 260
    .local v2, "locationUrl":Ljava/net/URL;
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v4

    .line 261
    .local v4, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v4, v2}, Lcom/samsung/xml/Parser;->parse(Ljava/net/URL;)Lcom/samsung/xml/Node;

    move-result-object v7

    .line 263
    .local v7, "rootNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v7}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v6

    .line 264
    .local v6, "rootDev":Lcom/samsung/upnp/Device;
    if-nez v6, :cond_1

    .line 265
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    :goto_1
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_1
    :try_start_1
    invoke-virtual {v6, p1}, Lcom/samsung/upnp/Device;->setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 269
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    .line 271
    invoke-virtual {p0, v8}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 273
    if-eqz v0, :cond_2

    .line 274
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 287
    .end local v2    # "locationUrl":Ljava/net/URL;
    .end local v4    # "parser":Lcom/samsung/xml/Parser;
    .end local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .end local v7    # "rootNode":Lcom/samsung/xml/Node;
    :catch_0
    move-exception v3

    .line 288
    .local v3, "me":Ljava/net/MalformedURLException;
    :try_start_2
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;)V

    .line 289
    invoke-static {v3}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 294
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 278
    .end local v3    # "me":Ljava/net/MalformedURLException;
    .restart local v2    # "locationUrl":Ljava/net/URL;
    .restart local v4    # "parser":Lcom/samsung/xml/Parser;
    .restart local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .restart local v7    # "rootNode":Lcom/samsung/xml/Node;
    :cond_2
    :try_start_3
    invoke-direct {p0, v7}, Lcom/samsung/upnp/ControlPoint;->addDevice(Lcom/samsung/xml/Node;)V

    .line 286
    invoke-virtual {p0, v6}, Lcom/samsung/upnp/ControlPoint;->performAddDeviceListener(Lcom/samsung/upnp/Device;)V
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 294
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 290
    .end local v2    # "locationUrl":Ljava/net/URL;
    .end local v4    # "parser":Lcom/samsung/xml/Parser;
    .end local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .end local v7    # "rootNode":Lcom/samsung/xml/Node;
    :catch_1
    move-exception v5

    .line 291
    .local v5, "pe":Lcom/samsung/xml/ParserException;
    :try_start_4
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;)V

    .line 292
    invoke-static {v5}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 294
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 293
    .end local v5    # "pe":Lcom/samsung/xml/ParserException;
    :catchall_0
    move-exception v10

    .line 294
    invoke-direct {p0, v8}, Lcom/samsung/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    .line 295
    throw v10
.end method

.method private addDevice(Lcom/samsung/xml/Node;)V
    .locals 1
    .param p1, "rootNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/xml/NodeList;->add(Ljava/lang/Object;)Z

    .line 218
    return-void
.end method

.method private getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;
    .locals 3
    .param p1, "rootNode"    # Lcom/samsung/xml/Node;

    .prologue
    const/4 v1, 0x0

    .line 299
    if-nez p1, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-object v1

    .line 301
    :cond_1
    const-string v2, "device"

    invoke-virtual {p1, v2}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 302
    .local v0, "devNode":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_0

    .line 304
    new-instance v1, Lcom/samsung/upnp/Device;

    invoke-direct {v1, p1, v0}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method private getEventSubCallbackURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 881
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getHTTPPort()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getEventSubURI()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/samsung/net/HostInterface;->getHostURL(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHTTPServerList()Lcom/samsung/http/HTTPServerList;
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->httpServerList:Lcom/samsung/http/HTTPServerList;

    return-object v0
.end method

.method private getSSDPNotifySocketList()Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->ssdpNotifySocketList:Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    return-object v0
.end method

.method private getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->ssdpSearchResponseSocketList:Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    return-object v0
.end method

.method private isActivityDevice(Ljava/lang/String;)Z
    .locals 2
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 223
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private isValidTarget(Ljava/lang/String;)Z
    .locals 1
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 676
    const-string v0, "upnp:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "urn:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private parseLastChangEvent(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 852
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v2

    .line 853
    .local v2, "parser":Lcom/samsung/xml/Parser;
    const/4 v1, 0x0

    .line 855
    .local v1, "instanceID":Lcom/samsung/xml/Node;
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v3

    .line 856
    .local v3, "rootNode":Lcom/samsung/xml/Node;
    if-nez v3, :cond_0

    .line 857
    const/4 v4, 0x0

    .line 862
    .end local v3    # "rootNode":Lcom/samsung/xml/Node;
    :goto_0
    return-object v4

    .line 858
    .restart local v3    # "rootNode":Lcom/samsung/xml/Node;
    :cond_0
    const-string v4, "InstanceID"

    invoke-virtual {v3, v4}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .end local v3    # "rootNode":Lcom/samsung/xml/Node;
    :goto_1
    move-object v4, v1

    .line 862
    goto :goto_0

    .line 859
    :catch_0
    move-exception v0

    .line 860
    .local v0, "e":Lcom/samsung/xml/ParserException;
    invoke-virtual {v0}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    goto :goto_1
.end method

.method private removeActivityDevice(Ljava/lang/String;)V
    .locals 3
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 236
    const-string v0, "ControlPoint"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "remove activit list udn :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 238
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->activityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 237
    monitor-exit v1

    .line 240
    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private removeDevice(Lcom/samsung/upnp/Device;)V
    .locals 1
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 388
    if-nez p1, :cond_0

    .line 391
    :goto_0
    return-void

    .line 390
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->removeDevice(Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method private removeDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 399
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isByeBye()Z

    move-result v2

    if-nez v2, :cond_0

    .line 404
    :goto_0
    return-void

    .line 401
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 402
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 403
    .local v0, "udn":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->removeDevice(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeDevice(Lcom/samsung/xml/Node;)V
    .locals 2
    .param p1, "rootNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 380
    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 381
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ControlPoint;->performRemoveDeviceListener(Lcom/samsung/upnp/Device;)V

    .line 384
    :cond_0
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v1, p1}, Lcom/samsung/xml/NodeList;->remove(Ljava/lang/Object;)Z

    .line 385
    return-void
.end method

.method private removeDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 394
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 395
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->removeDevice(Lcom/samsung/upnp/Device;)V

    .line 396
    return-void
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 520
    if-nez p1, :cond_0

    .line 521
    const/4 v0, 0x0

    .line 524
    :goto_0
    return v0

    .line 523
    :cond_0
    monitor-enter p0

    .line 524
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    goto :goto_0

    .line 523
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addDeviceChangeListener(Lcom/samsung/upnp/device/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/DeviceChangeListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 538
    if-nez p1, :cond_0

    .line 539
    const/4 v0, 0x0

    .line 542
    :goto_0
    return v0

    .line 541
    :cond_0
    monitor-enter p0

    .line 542
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    goto :goto_0

    .line 541
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addEventListener(Lcom/samsung/api/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/EventListener;

    .prologue
    .line 776
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addEventListener(Lcom/samsung/upnp/event/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/event/EventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 782
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addNotifyListener(Lcom/samsung/upnp/device/NotifyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/NotifyListener;

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 456
    return-void
.end method

.method public addSearchResponseListener(Lcom/samsung/upnp/device/SearchResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/SearchResponseListener;

    .prologue
    .line 486
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 487
    return-void
.end method

.method public addSearchTarget(Ljava/lang/String;)V
    .locals 3
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 665
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->isValidTarget(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 673
    :cond_0
    :goto_0
    return-void

    .line 668
    :cond_1
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 672
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 668
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 669
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0
.end method

.method public getDevice(Lcom/samsung/api/DeviceItem;)Lcom/samsung/upnp/Device;
    .locals 1
    .param p1, "device"    # Lcom/samsung/api/DeviceItem;

    .prologue
    .line 348
    if-nez p1, :cond_0

    .line 349
    const/4 v0, 0x0

    .line 350
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/api/DeviceItem;->getUdn()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    goto :goto_0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 354
    iget-object v5, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5}, Lcom/samsung/xml/NodeList;->size()I

    move-result v3

    .line 356
    .local v3, "nRoots":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    add-int/lit8 v5, v3, 0x1

    if-lt v2, v5, :cond_1

    .line 367
    const/4 v1, 0x0

    :cond_0
    :goto_1
    return-object v1

    .line 357
    :cond_1
    iget-object v5, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5, v2}, Lcom/samsung/xml/NodeList;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v4

    .line 358
    .local v4, "rootNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v4}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 359
    .local v1, "dev":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_3

    .line 356
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 361
    :cond_3
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->isDevice(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 363
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 364
    .local v0, "cdev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 365
    goto :goto_1
.end method

.method public getDeviceDisposer()Lcom/samsung/upnp/device/Disposer;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceDisposer:Lcom/samsung/upnp/device/Disposer;

    return-object v0
.end method

.method public getDeviceList()Lcom/samsung/upnp/DeviceList;
    .locals 9

    .prologue
    .line 308
    new-instance v1, Lcom/samsung/upnp/DeviceList;

    invoke-direct {v1}, Lcom/samsung/upnp/DeviceList;-><init>()V

    .line 309
    .local v1, "devList":Lcom/samsung/upnp/DeviceList;
    iget-object v7, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v7}, Lcom/samsung/xml/NodeList;->size()I

    move-result v5

    .line 310
    .local v5, "nRoots":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-lt v4, v5, :cond_0

    .line 330
    return-object v1

    .line 311
    :cond_0
    iget-object v7, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v7, v4}, Lcom/samsung/xml/NodeList;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v6

    .line 312
    .local v6, "rootNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v6}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 313
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_2

    .line 310
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 315
    :cond_2
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    .line 317
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v3

    .line 318
    .local v3, "emDeviceList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/upnp/Device;

    .line 319
    .local v2, "emDevice":Lcom/samsung/upnp/Device;
    if-eqz v2, :cond_3

    .line 320
    invoke-virtual {v1, v2}, Lcom/samsung/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getEventSubURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventSubURI:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiredDeviceMonitoringInterval()J
    .locals 2

    .prologue
    .line 437
    iget-wide v0, p0, Lcom/samsung/upnp/ControlPoint;->expiredDeviceMonitoringInterval:J

    return-wide v0
.end method

.method public getHTTPPort()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/samsung/upnp/ControlPoint;->httpPort:I

    return v0
.end method

.method public getRenewSubscriber()Lcom/samsung/upnp/control/RenewSubscriber;
    .locals 1

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->renewSubscriber:Lcom/samsung/upnp/control/RenewSubscriber;

    return-object v0
.end method

.method public getSSDPPort()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/samsung/upnp/ControlPoint;->ssdpPort:I

    return v0
.end method

.method public getSearchMx()I
    .locals 1

    .prologue
    .line 655
    iget v0, p0, Lcom/samsung/upnp/ControlPoint;->searchMx:I

    return v0
.end method

.method public httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 11
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 745
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    .line 749
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->isNotifyRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 750
    new-instance v7, Lcom/samsung/upnp/event/NotifyRequest;

    invoke-direct {v7, p1}, Lcom/samsung/upnp/event/NotifyRequest;-><init>(Lcom/samsung/http/HTTPRequest;)V

    .line 751
    .local v7, "notifyReq":Lcom/samsung/upnp/event/NotifyRequest;
    invoke-virtual {v7}, Lcom/samsung/upnp/event/NotifyRequest;->getSID()Ljava/lang/String;

    move-result-object v1

    .line 752
    .local v1, "uuid":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/upnp/event/NotifyRequest;->getSEQ()J

    move-result-wide v2

    .line 753
    .local v2, "seq":J
    invoke-virtual {v7}, Lcom/samsung/upnp/event/NotifyRequest;->getPropertyList()Lcom/samsung/upnp/event/PropertyList;

    move-result-object v10

    .line 754
    .local v10, "props":Lcom/samsung/upnp/event/PropertyList;
    invoke-virtual {v10}, Lcom/samsung/upnp/event/PropertyList;->size()I

    move-result v9

    .line 755
    .local v9, "propCnt":I
    const/4 v6, 0x0

    .local v6, "n":I
    :goto_0
    if-lt v6, v9, :cond_0

    .line 761
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnOK()Z

    .line 766
    .end local v1    # "uuid":Ljava/lang/String;
    .end local v2    # "seq":J
    .end local v6    # "n":I
    .end local v7    # "notifyReq":Lcom/samsung/upnp/event/NotifyRequest;
    .end local v9    # "propCnt":I
    .end local v10    # "props":Lcom/samsung/upnp/event/PropertyList;
    :goto_1
    return-void

    .line 756
    .restart local v1    # "uuid":Ljava/lang/String;
    .restart local v2    # "seq":J
    .restart local v6    # "n":I
    .restart local v7    # "notifyReq":Lcom/samsung/upnp/event/NotifyRequest;
    .restart local v9    # "propCnt":I
    .restart local v10    # "props":Lcom/samsung/upnp/event/PropertyList;
    :cond_0
    invoke-virtual {v10, v6}, Lcom/samsung/upnp/event/PropertyList;->getProperty(I)Lcom/samsung/upnp/event/Property;

    move-result-object v8

    .line 757
    .local v8, "prop":Lcom/samsung/upnp/event/Property;
    invoke-virtual {v8}, Lcom/samsung/upnp/event/Property;->getName()Ljava/lang/String;

    move-result-object v4

    .line 758
    .local v4, "varName":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/upnp/event/Property;->getValue()Ljava/lang/String;

    move-result-object v5

    .local v5, "varValue":Ljava/lang/String;
    move-object v0, p0

    .line 759
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/upnp/ControlPoint;->performEventListener(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 755
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 765
    .end local v1    # "uuid":Ljava/lang/String;
    .end local v2    # "seq":J
    .end local v4    # "varName":Ljava/lang/String;
    .end local v5    # "varValue":Ljava/lang/String;
    .end local v6    # "n":I
    .end local v7    # "notifyReq":Lcom/samsung/upnp/event/NotifyRequest;
    .end local v8    # "prop":Lcom/samsung/upnp/event/Property;
    .end local v9    # "propCnt":I
    .end local v10    # "props":Lcom/samsung/upnp/event/PropertyList;
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_1
.end method

.method public isTarget(Ljava/lang/String;)Z
    .locals 5
    .param p1, "candinate"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 689
    if-nez p1, :cond_1

    .line 697
    :cond_0
    :goto_0
    return v1

    .line 691
    :cond_1
    const-string v3, "upnp:rootdevice"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 692
    goto :goto_0

    .line 693
    :cond_2
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 694
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 695
    goto :goto_0
.end method

.method public isValidNotifyPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z
    .locals 1
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 606
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getNT()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ControlPoint;->isTarget(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isValidResponsePacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z
    .locals 1
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 610
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ControlPoint;->isTarget(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public notifyReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 614
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 615
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 617
    .local v0, "udn":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->isActivityDevice(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 618
    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->addDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 619
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isByeBye()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 620
    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 630
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ControlPoint;->performNotifyListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 631
    return-void
.end method

.method public performAddDeviceListener(Lcom/samsung/upnp/Device;)V
    .locals 5
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 556
    monitor-enter p0

    .line 557
    :try_start_0
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/util/ListenerList;->size()I

    move-result v2

    .line 558
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v2, :cond_0

    .line 556
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    return-void

    .line 560
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 561
    .local v1, "listener":Ljava/lang/Object;
    instance-of v4, v1, Lcom/samsung/api/DeviceChangeListener;

    if-eqz v4, :cond_2

    .line 562
    check-cast v1, Lcom/samsung/api/DeviceChangeListener;

    .end local v1    # "listener":Ljava/lang/Object;
    new-instance v4, Lcom/samsung/api/DeviceItem;

    invoke-direct {v4, p1}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    invoke-interface {v1, v4}, Lcom/samsung/api/DeviceChangeListener;->deviceAdded(Lcom/samsung/api/DeviceItem;)V

    .line 558
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 563
    .restart local v1    # "listener":Ljava/lang/Object;
    :cond_2
    instance-of v4, v1, Lcom/samsung/upnp/device/DeviceChangeListener;

    if-eqz v4, :cond_1

    .line 564
    check-cast v1, Lcom/samsung/upnp/device/DeviceChangeListener;

    .end local v1    # "listener":Ljava/lang/Object;
    invoke-interface {v1, p1}, Lcom/samsung/upnp/device/DeviceChangeListener;->deviceAdded(Lcom/samsung/upnp/Device;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 565
    :catch_0
    move-exception v0

    .line 566
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 556
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "listenerSize":I
    .end local v3    # "n":I
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 568
    .restart local v2    # "listenerSize":I
    .restart local v3    # "n":I
    :catch_1
    move-exception v0

    .line 569
    .local v0, "e":Ljava/lang/Error;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public performEventListener(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "seq"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "value"    # Ljava/lang/String;

    .prologue
    .line 798
    const/4 v9, 0x0

    .line 799
    .local v9, "lastChangeNode":Lcom/samsung/xml/Node;
    const-string v1, "LastChange"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 800
    move-object/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->parseLastChangEvent(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v9

    .line 801
    if-nez v9, :cond_1

    .line 849
    :cond_0
    return-void

    .line 804
    :cond_1
    invoke-virtual {v9}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object p4

    .line 805
    const-string v1, "val"

    invoke-virtual {v9, v1}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 806
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v1}, Lcom/samsung/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    .line 814
    invoke-virtual {v9}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v12

    .line 815
    .local v12, "nodeCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v12, :cond_0

    .line 817
    :try_start_0
    invoke-virtual {v9, v8}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v11

    .line 818
    .local v11, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v11}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object p4

    .line 819
    const-string v1, "val"

    invoke-virtual {v11, v1}, Lcom/samsung/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 820
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v1}, Lcom/samsung/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-nez v1, :cond_6

    .line 815
    .end local v11    # "node":Lcom/samsung/xml/Node;
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 806
    .end local v8    # "i":I
    .end local v12    # "nodeCount":I
    :cond_4
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 808
    .local v10, "listener":Ljava/lang/Object;
    instance-of v1, v10, Lcom/samsung/api/EventListener;

    if-eqz v1, :cond_5

    move-object v1, v10

    .line 809
    check-cast v1, Lcom/samsung/api/EventListener;

    move-object v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/samsung/api/EventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 810
    :cond_5
    instance-of v1, v10, Lcom/samsung/upnp/event/EventListener;

    if-eqz v1, :cond_2

    move-object v1, v10

    .line 811
    check-cast v1, Lcom/samsung/upnp/event/EventListener;

    move-object v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/samsung/upnp/event/EventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 820
    .end local v10    # "listener":Ljava/lang/Object;
    .restart local v8    # "i":I
    .restart local v11    # "node":Lcom/samsung/xml/Node;
    .restart local v12    # "nodeCount":I
    :cond_6
    :try_start_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v10

    .line 823
    .restart local v10    # "listener":Ljava/lang/Object;
    :try_start_2
    instance-of v1, v10, Lcom/samsung/api/EventListener;

    if-eqz v1, :cond_7

    .line 824
    move-object v0, v10

    check-cast v0, Lcom/samsung/api/EventListener;

    move-object v1, v0

    move-object v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/samsung/api/EventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 827
    :catch_0
    move-exception v7

    .line 828
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 836
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v10    # "listener":Ljava/lang/Object;
    .end local v11    # "node":Lcom/samsung/xml/Node;
    :catch_1
    move-exception v1

    goto :goto_3

    .line 825
    .restart local v10    # "listener":Ljava/lang/Object;
    .restart local v11    # "node":Lcom/samsung/xml/Node;
    :cond_7
    :try_start_4
    instance-of v1, v10, Lcom/samsung/upnp/event/EventListener;

    if-eqz v1, :cond_3

    .line 826
    move-object v0, v10

    check-cast v0, Lcom/samsung/upnp/event/EventListener;

    move-object v1, v0

    move-object v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/samsung/upnp/event/EventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 830
    :catch_2
    move-exception v7

    .line 831
    .local v7, "e":Ljava/lang/Error;
    :try_start_5
    invoke-virtual {v7}, Ljava/lang/Error;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 841
    .end local v7    # "e":Ljava/lang/Error;
    .end local v8    # "i":I
    .end local v10    # "listener":Ljava/lang/Object;
    .end local v11    # "node":Lcom/samsung/xml/Node;
    .end local v12    # "nodeCount":I
    :cond_8
    iget-object v1, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v1}, Lcom/samsung/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_9
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 843
    .restart local v10    # "listener":Ljava/lang/Object;
    instance-of v1, v10, Lcom/samsung/api/EventListener;

    if-eqz v1, :cond_a

    move-object v1, v10

    .line 844
    check-cast v1, Lcom/samsung/api/EventListener;

    move-object v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/samsung/api/EventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 845
    :cond_a
    instance-of v1, v10, Lcom/samsung/upnp/event/EventListener;

    if-eqz v1, :cond_9

    move-object v1, v10

    .line 846
    check-cast v1, Lcom/samsung/upnp/event/EventListener;

    move-object v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/samsung/upnp/event/EventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public performNotifyListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 5
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 464
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/util/ListenerList;->size()I

    move-result v2

    .line 465
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v2, :cond_0

    .line 477
    return-void

    .line 467
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/device/NotifyListener;

    .line 468
    .local v1, "listener":Lcom/samsung/upnp/device/NotifyListener;
    invoke-interface {v1, p1}, Lcom/samsung/upnp/device/NotifyListener;->deviceNotifyReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 465
    .end local v1    # "listener":Lcom/samsung/upnp/device/NotifyListener;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 472
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 473
    .local v0, "e":Ljava/lang/Error;
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_1
.end method

.method public performRemoveDeviceListener(Lcom/samsung/upnp/Device;)V
    .locals 5
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 578
    monitor-enter p0

    .line 579
    :try_start_0
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/util/ListenerList;->size()I

    move-result v2

    .line 580
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v2, :cond_0

    .line 578
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 599
    return-void

    .line 584
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 585
    .local v1, "listener":Ljava/lang/Object;
    instance-of v4, v1, Lcom/samsung/api/DeviceChangeListener;

    if-eqz v4, :cond_2

    .line 586
    check-cast v1, Lcom/samsung/api/DeviceChangeListener;

    .end local v1    # "listener":Ljava/lang/Object;
    new-instance v4, Lcom/samsung/api/DeviceItem;

    invoke-direct {v4, p1}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    invoke-interface {v1, v4}, Lcom/samsung/api/DeviceChangeListener;->deviceRemoved(Lcom/samsung/api/DeviceItem;)V

    .line 580
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 587
    .restart local v1    # "listener":Ljava/lang/Object;
    :cond_2
    instance-of v4, v1, Lcom/samsung/upnp/device/DeviceChangeListener;

    if-eqz v4, :cond_1

    .line 588
    check-cast v1, Lcom/samsung/upnp/device/DeviceChangeListener;

    .end local v1    # "listener":Ljava/lang/Object;
    invoke-interface {v1, p1}, Lcom/samsung/upnp/device/DeviceChangeListener;->deviceRemoved(Lcom/samsung/upnp/Device;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 578
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "listenerSize":I
    .end local v3    # "n":I
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 593
    .restart local v2    # "listenerSize":I
    .restart local v3    # "n":I
    :catch_1
    move-exception v0

    .line 594
    .local v0, "e":Ljava/lang/Error;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public performSearchResponseListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 5
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 494
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/util/ListenerList;->size()I

    move-result v2

    .line 495
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v2, :cond_0

    .line 507
    return-void

    .line 497
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/upnp/ControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/device/SearchResponseListener;

    .line 498
    .local v1, "listener":Lcom/samsung/upnp/device/SearchResponseListener;
    invoke-interface {v1, p1}, Lcom/samsung/upnp/device/SearchResponseListener;->deviceSearchResponseReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 495
    .end local v1    # "listener":Lcom/samsung/upnp/device/SearchResponseListener;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 502
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 503
    .local v0, "e":Ljava/lang/Error;
    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_1
.end method

.method public removeDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 530
    monitor-enter p0

    .line 531
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    return v0

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeDeviceChangeListener(Lcom/samsung/upnp/device/DeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/DeviceChangeListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 549
    monitor-enter p0

    .line 550
    :try_start_0
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    return v0

    .line 549
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeDeviceItem(Ljava/lang/String;)V
    .locals 0
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 407
    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->removeDevice(Ljava/lang/String;)V

    .line 408
    return-void
.end method

.method public removeDeviceList(Ljava/lang/String;)V
    .locals 4
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 334
    if-nez p1, :cond_1

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v3}, Lcom/samsung/xml/NodeList;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_0

    .line 338
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v3, v2}, Lcom/samsung/xml/NodeList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/xml/Node;

    invoke-direct {p0, v3}, Lcom/samsung/upnp/ControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 339
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 340
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v3, v2}, Lcom/samsung/xml/NodeList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 342
    .end local v0    # "dev":Lcom/samsung/upnp/Device;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 343
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public removeEventListener(Lcom/samsung/api/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/EventListener;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeEventListener(Lcom/samsung/upnp/event/EventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/event/EventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 793
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->eventListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeSearchResponseListener(Lcom/samsung/upnp/device/SearchResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/SearchResponseListener;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/samsung/upnp/ControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 491
    return-void
.end method

.method public renewSubscriberService()V
    .locals 2

    .prologue
    .line 1013
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/upnp/ControlPoint;->renewSubscriberService(J)V

    .line 1014
    return-void
.end method

.method public renewSubscriberService(J)V
    .locals 4
    .param p1, "timeout"    # J

    .prologue
    .line 1004
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 1005
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 1006
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 1010
    return-void

    .line 1007
    :cond_0
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 1008
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/upnp/ControlPoint;->renewSubscriberService(Lcom/samsung/upnp/Device;J)V

    .line 1006
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public renewSubscriberService(Lcom/samsung/upnp/Device;J)V
    .locals 10
    .param p1, "dev"    # Lcom/samsung/upnp/Device;
    .param p2, "timeout"    # J

    .prologue
    .line 983
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v7

    .line 984
    .local v7, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v7}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v6

    .line 985
    .local v6, "serviceCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-lt v4, v6, :cond_0

    .line 995
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 996
    .local v2, "cdevList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 997
    .local v1, "cdevCnt":I
    const/4 v4, 0x0

    :goto_1
    if-lt v4, v1, :cond_3

    .line 1001
    return-void

    .line 986
    .end local v1    # "cdevCnt":I
    .end local v2    # "cdevList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    invoke-virtual {v7, v4}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v5

    .line 987
    .local v5, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v5}, Lcom/samsung/upnp/Service;->isSubscribed()Z

    move-result v9

    if-nez v9, :cond_2

    .line 985
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 989
    :cond_2
    invoke-virtual {v5}, Lcom/samsung/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v8

    .line 990
    .local v8, "sid":Ljava/lang/String;
    invoke-virtual {p0, v5, v8, p2, p3}, Lcom/samsung/upnp/ControlPoint;->subscribe(Lcom/samsung/upnp/Service;Ljava/lang/String;J)Z

    move-result v3

    .line 991
    .local v3, "isRenewed":Z
    if-nez v3, :cond_1

    .line 992
    invoke-virtual {p0, v5, p2, p3}, Lcom/samsung/upnp/ControlPoint;->subscribe(Lcom/samsung/upnp/Service;J)Z

    goto :goto_2

    .line 998
    .end local v3    # "isRenewed":Z
    .end local v5    # "service":Lcom/samsung/upnp/Service;
    .end local v8    # "sid":Ljava/lang/String;
    .restart local v1    # "cdevCnt":I
    .restart local v2    # "cdevList":Lcom/samsung/upnp/DeviceList;
    :cond_3
    invoke-virtual {v2, v4}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 999
    .local v0, "cdev":Lcom/samsung/upnp/Device;
    invoke-virtual {p0, v0, p2, p3}, Lcom/samsung/upnp/ControlPoint;->renewSubscriberService(Lcom/samsung/upnp/Device;J)V

    .line 997
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public search()V
    .locals 5

    .prologue
    .line 714
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 715
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    const-string v4, "upnp:rootdevice"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    :cond_0
    iget-object v3, p0, Lcom/samsung/upnp/ControlPoint;->searchTargetList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 722
    return-void

    .line 717
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 718
    .local v2, "target":Ljava/lang/String;
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;

    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getSearchMx()I

    move-result v4

    invoke-direct {v0, v2, v4}, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 719
    .local v0, "msReq":Lcom/samsung/upnp/ssdp/SSDPSearchRequest;
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v1

    .line 720
    .local v1, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/upnp/ssdp/SSDPSearchRequest;)Z

    goto :goto_0
.end method

.method public searchResponseReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 634
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 635
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 637
    .local v0, "udn":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->isActivityDevice(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 638
    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->addDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 644
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/ControlPoint;->performSearchResponseListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 645
    return-void
.end method

.method public searchTarget(Ljava/lang/String;)V
    .locals 3
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 726
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/upnp/ControlPoint;->isValidTarget(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 732
    :cond_0
    :goto_0
    return-void

    .line 729
    :cond_1
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;

    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getSearchMx()I

    move-result v2

    invoke-direct {v0, p1, v2}, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 730
    .local v0, "msReq":Lcom/samsung/upnp/ssdp/SSDPSearchRequest;
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v1

    .line 731
    .local v1, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/upnp/ssdp/SSDPSearchRequest;)Z

    goto :goto_0
.end method

.method public setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V
    .locals 0
    .param p1, "disposer"    # Lcom/samsung/upnp/device/Disposer;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/samsung/upnp/ControlPoint;->deviceDisposer:Lcom/samsung/upnp/device/Disposer;

    .line 442
    return-void
.end method

.method public setExpiredDeviceMonitoringInterval(J)V
    .locals 0
    .param p1, "interval"    # J

    .prologue
    .line 433
    iput-wide p1, p0, Lcom/samsung/upnp/ControlPoint;->expiredDeviceMonitoringInterval:J

    .line 434
    return-void
.end method

.method public setHTTPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/samsung/upnp/ControlPoint;->httpPort:I

    .line 193
    return-void
.end method

.method public setNMPRMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/samsung/upnp/ControlPoint;->nmprMode:Z

    .line 203
    return-void
.end method

.method public setRenewSubscriber(Lcom/samsung/upnp/control/RenewSubscriber;)V
    .locals 0
    .param p1, "sub"    # Lcom/samsung/upnp/control/RenewSubscriber;

    .prologue
    .line 1023
    iput-object p1, p0, Lcom/samsung/upnp/ControlPoint;->renewSubscriber:Lcom/samsung/upnp/control/RenewSubscriber;

    .line 1024
    return-void
.end method

.method public setSSDPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/samsung/upnp/ControlPoint;->ssdpPort:I

    .line 179
    return-void
.end method

.method public start()Z
    .locals 12

    .prologue
    const/16 v11, 0x64

    const/4 v9, 0x0

    .line 1035
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->stop()Z

    .line 1041
    const/4 v5, 0x0

    .line 1042
    .local v5, "retryCnt":I
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getHTTPPort()I

    move-result v0

    .line 1043
    .local v0, "bindPort":I
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getHTTPServerList()Lcom/samsung/http/HTTPServerList;

    move-result-object v3

    .line 1045
    .local v3, "httpServerList":Lcom/samsung/http/HTTPServerList;
    :goto_0
    invoke-virtual {v3, v0}, Lcom/samsung/http/HTTPServerList;->open(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1053
    invoke-virtual {v3}, Lcom/samsung/http/HTTPServerList;->size()I

    move-result v10

    if-nez v10, :cond_2

    .line 1118
    :cond_0
    :goto_1
    return v9

    .line 1046
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 1047
    if-lt v11, v5, :cond_0

    .line 1049
    add-int/lit8 v10, v0, 0x1

    invoke-virtual {p0, v10}, Lcom/samsung/upnp/ControlPoint;->setHTTPPort(I)V

    .line 1050
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getHTTPPort()I

    move-result v0

    goto :goto_0

    .line 1056
    :cond_2
    invoke-virtual {v3, p0}, Lcom/samsung/http/HTTPServerList;->addRequestListener(Lcom/samsung/http/HTTPRequestListener;)V

    .line 1057
    invoke-virtual {v3}, Lcom/samsung/http/HTTPServerList;->start()V

    .line 1063
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPNotifySocketList()Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    move-result-object v6

    .line 1064
    .local v6, "ssdpNotifySocketList":Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->open()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1066
    invoke-virtual {v6, p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->setControlPoint(Lcom/samsung/upnp/ControlPoint;)V

    .line 1068
    :try_start_0
    invoke-virtual {v6}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPPort()I

    move-result v7

    .line 1078
    .local v7, "ssdpPort":I
    const/4 v5, 0x0

    .line 1079
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v8

    .line 1080
    .local v8, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    :goto_2
    invoke-virtual {v8, v7}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->open(I)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1087
    invoke-virtual {v8, p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->setControlPoint(Lcom/samsung/upnp/ControlPoint;)V

    .line 1089
    :try_start_1
    invoke-virtual {v8}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1097
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->search()V

    .line 1103
    new-instance v1, Lcom/samsung/upnp/device/Disposer;

    invoke-direct {v1, p0}, Lcom/samsung/upnp/device/Disposer;-><init>(Lcom/samsung/upnp/IControlPoint;)V

    .line 1104
    .local v1, "disposer":Lcom/samsung/upnp/device/Disposer;
    const-string v9, "Device Disposer"

    invoke-virtual {v1, v9}, Lcom/samsung/upnp/device/Disposer;->setName(Ljava/lang/String;)V

    .line 1105
    invoke-virtual {p0, v1}, Lcom/samsung/upnp/ControlPoint;->setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V

    .line 1106
    invoke-virtual {v1}, Lcom/samsung/upnp/device/Disposer;->start()V

    .line 1113
    new-instance v4, Lcom/samsung/upnp/control/RenewSubscriber;

    invoke-direct {v4, p0}, Lcom/samsung/upnp/control/RenewSubscriber;-><init>(Lcom/samsung/upnp/IControlPoint;)V

    .line 1114
    .local v4, "renewSub":Lcom/samsung/upnp/control/RenewSubscriber;
    invoke-virtual {p0, v4}, Lcom/samsung/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/upnp/control/RenewSubscriber;)V

    .line 1115
    invoke-virtual {v4}, Lcom/samsung/upnp/control/RenewSubscriber;->start()V

    .line 1118
    const/4 v9, 0x1

    goto :goto_1

    .line 1069
    .end local v1    # "disposer":Lcom/samsung/upnp/device/Disposer;
    .end local v4    # "renewSub":Lcom/samsung/upnp/control/RenewSubscriber;
    .end local v7    # "ssdpPort":I
    .end local v8    # "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    :catch_0
    move-exception v2

    .line 1070
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 1081
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v7    # "ssdpPort":I
    .restart local v8    # "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 1082
    if-lt v11, v5, :cond_0

    .line 1084
    add-int/lit8 v10, v7, 0x1

    invoke-virtual {p0, v10}, Lcom/samsung/upnp/ControlPoint;->setSSDPPort(I)V

    .line 1085
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPPort()I

    move-result v7

    goto :goto_2

    .line 1090
    :catch_1
    move-exception v2

    .line 1091
    .restart local v2    # "e":Ljava/lang/Exception;
    goto :goto_1
.end method

.method public stop()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1122
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->unsubscribe()V

    .line 1124
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPNotifySocketList()Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    move-result-object v3

    .line 1125
    .local v3, "ssdpNotifySocketList":Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
    invoke-virtual {v3}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->stop()V

    .line 1126
    invoke-virtual {v3}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->close()V

    .line 1127
    invoke-virtual {v3}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->clear()V

    .line 1129
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v4

    .line 1130
    .local v4, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->stop()V

    .line 1131
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->close()V

    .line 1132
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->clear()V

    .line 1134
    invoke-direct {p0}, Lcom/samsung/upnp/ControlPoint;->getHTTPServerList()Lcom/samsung/http/HTTPServerList;

    move-result-object v1

    .line 1135
    .local v1, "httpServerList":Lcom/samsung/http/HTTPServerList;
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServerList;->stop()V

    .line 1136
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServerList;->close()Z

    .line 1137
    invoke-virtual {v1}, Lcom/samsung/http/HTTPServerList;->clear()V

    .line 1140
    iget-object v5, p0, Lcom/samsung/upnp/ControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5}, Lcom/samsung/xml/NodeList;->clear()V

    .line 1146
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getDeviceDisposer()Lcom/samsung/upnp/device/Disposer;

    move-result-object v0

    .line 1147
    .local v0, "disposer":Lcom/samsung/upnp/device/Disposer;
    if-eqz v0, :cond_0

    .line 1148
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Disposer;->stop()V

    .line 1149
    invoke-virtual {p0, v6}, Lcom/samsung/upnp/ControlPoint;->setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V

    .line 1156
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getRenewSubscriber()Lcom/samsung/upnp/control/RenewSubscriber;

    move-result-object v2

    .line 1157
    .local v2, "renewSub":Lcom/samsung/upnp/control/RenewSubscriber;
    if-eqz v2, :cond_1

    .line 1158
    invoke-virtual {v2}, Lcom/samsung/upnp/control/RenewSubscriber;->stop()V

    .line 1159
    invoke-virtual {p0, v6}, Lcom/samsung/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/upnp/control/RenewSubscriber;)V

    .line 1162
    :cond_1
    const/4 v5, 0x1

    return v5
.end method

.method public subscribe(Lcom/samsung/upnp/Service;)Z
    .locals 2
    .param p1, "service"    # Lcom/samsung/upnp/Service;

    .prologue
    .line 908
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/upnp/ControlPoint;->subscribe(Lcom/samsung/upnp/Service;J)Z

    move-result v0

    return v0
.end method

.method public subscribe(Lcom/samsung/upnp/Service;J)Z
    .locals 7
    .param p1, "service"    # Lcom/samsung/upnp/Service;
    .param p2, "timeout"    # J

    .prologue
    const/4 v5, 0x0

    .line 885
    invoke-virtual {p1}, Lcom/samsung/upnp/Service;->isSubscribed()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 886
    invoke-virtual {p1}, Lcom/samsung/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v2

    .line 887
    .local v2, "sid":Ljava/lang/String;
    invoke-virtual {p0, p1, v2, p2, p3}, Lcom/samsung/upnp/ControlPoint;->subscribe(Lcom/samsung/upnp/Service;Ljava/lang/String;J)Z

    move-result v5

    .line 904
    .end local v2    # "sid":Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 890
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/upnp/Service;->getRootDevice()Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 891
    .local v1, "rootDev":Lcom/samsung/upnp/Device;
    if-eqz v1, :cond_0

    .line 893
    invoke-virtual {v1}, Lcom/samsung/upnp/Device;->getInterfaceAddress()Ljava/lang/String;

    move-result-object v0

    .line 894
    .local v0, "ifAddress":Ljava/lang/String;
    new-instance v3, Lcom/samsung/upnp/event/SubscriptionRequest;

    invoke-direct {v3}, Lcom/samsung/upnp/event/SubscriptionRequest;-><init>()V

    .line 895
    .local v3, "subReq":Lcom/samsung/upnp/event/SubscriptionRequest;
    invoke-direct {p0, v0}, Lcom/samsung/upnp/ControlPoint;->getEventSubCallbackURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, p1, v6, p2, p3}, Lcom/samsung/upnp/event/SubscriptionRequest;->setSubscribeRequest(Lcom/samsung/upnp/Service;Ljava/lang/String;J)V

    .line 896
    invoke-virtual {v3}, Lcom/samsung/upnp/event/SubscriptionRequest;->post()Lcom/samsung/upnp/event/SubscriptionResponse;

    move-result-object v4

    .line 897
    .local v4, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    invoke-virtual {v4}, Lcom/samsung/upnp/event/SubscriptionResponse;->isSuccessful()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 898
    invoke-virtual {v4}, Lcom/samsung/upnp/event/SubscriptionResponse;->getSID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/samsung/upnp/Service;->setSID(Ljava/lang/String;)V

    .line 899
    invoke-virtual {v4}, Lcom/samsung/upnp/event/SubscriptionResponse;->getTimeout()J

    move-result-wide v5

    invoke-virtual {p1, v5, v6}, Lcom/samsung/upnp/Service;->setTimeout(J)V

    .line 900
    const/4 v5, 0x1

    goto :goto_0

    .line 903
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/upnp/Service;->clearSID()V

    goto :goto_0
.end method

.method public subscribe(Lcom/samsung/upnp/Service;Ljava/lang/String;J)Z
    .locals 4
    .param p1, "service"    # Lcom/samsung/upnp/Service;
    .param p2, "uuid"    # Ljava/lang/String;
    .param p3, "timeout"    # J

    .prologue
    .line 913
    new-instance v0, Lcom/samsung/upnp/event/SubscriptionRequest;

    invoke-direct {v0}, Lcom/samsung/upnp/event/SubscriptionRequest;-><init>()V

    .line 914
    .local v0, "subReq":Lcom/samsung/upnp/event/SubscriptionRequest;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/upnp/event/SubscriptionRequest;->setRenewRequest(Lcom/samsung/upnp/Service;Ljava/lang/String;J)V

    .line 915
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 916
    invoke-virtual {v0}, Lcom/samsung/upnp/event/SubscriptionRequest;->print()V

    .line 917
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/event/SubscriptionRequest;->post()Lcom/samsung/upnp/event/SubscriptionResponse;

    move-result-object v1

    .line 918
    .local v1, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 919
    invoke-virtual {v1}, Lcom/samsung/upnp/event/SubscriptionResponse;->print()V

    .line 920
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/upnp/event/SubscriptionResponse;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 921
    invoke-virtual {v1}, Lcom/samsung/upnp/event/SubscriptionResponse;->getSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/upnp/Service;->setSID(Ljava/lang/String;)V

    .line 922
    invoke-virtual {v1}, Lcom/samsung/upnp/event/SubscriptionResponse;->getTimeout()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/samsung/upnp/Service;->setTimeout(J)V

    .line 923
    const/4 v2, 0x1

    .line 926
    :goto_0
    return v2

    .line 925
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/upnp/Service;->clearSID()V

    .line 926
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public unsubscribe()V
    .locals 4

    .prologue
    .line 970
    invoke-virtual {p0}, Lcom/samsung/upnp/ControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 971
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 972
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 976
    return-void

    .line 973
    :cond_0
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 974
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ControlPoint;->unsubscribe(Lcom/samsung/upnp/Device;)V

    .line 972
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public unsubscribe(Lcom/samsung/upnp/Device;)V
    .locals 8
    .param p1, "device"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 953
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getServiceList()Lcom/samsung/upnp/ServiceList;

    move-result-object v6

    .line 954
    .local v6, "serviceList":Lcom/samsung/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/upnp/ServiceList;->size()I

    move-result v5

    .line 955
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v5, :cond_0

    .line 961
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 962
    .local v2, "childDevList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 963
    .local v1, "childDevCnt":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_2

    .line 967
    return-void

    .line 956
    .end local v1    # "childDevCnt":I
    .end local v2    # "childDevList":Lcom/samsung/upnp/DeviceList;
    :cond_0
    invoke-virtual {v6, v3}, Lcom/samsung/upnp/ServiceList;->getService(I)Lcom/samsung/upnp/Service;

    move-result-object v4

    .line 957
    .local v4, "service":Lcom/samsung/upnp/Service;
    invoke-virtual {v4}, Lcom/samsung/upnp/Service;->hasSID()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 958
    invoke-virtual {p0, v4}, Lcom/samsung/upnp/ControlPoint;->unsubscribe(Lcom/samsung/upnp/Service;)Z

    .line 955
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 964
    .end local v4    # "service":Lcom/samsung/upnp/Service;
    .restart local v1    # "childDevCnt":I
    .restart local v2    # "childDevList":Lcom/samsung/upnp/DeviceList;
    :cond_2
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 965
    .local v0, "cdev":Lcom/samsung/upnp/Device;
    invoke-virtual {p0, v0}, Lcom/samsung/upnp/ControlPoint;->unsubscribe(Lcom/samsung/upnp/Device;)V

    .line 963
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public unsubscribe(Lcom/samsung/upnp/Service;)Z
    .locals 3
    .param p1, "service"    # Lcom/samsung/upnp/Service;

    .prologue
    .line 942
    new-instance v0, Lcom/samsung/upnp/event/SubscriptionRequest;

    invoke-direct {v0}, Lcom/samsung/upnp/event/SubscriptionRequest;-><init>()V

    .line 943
    .local v0, "subReq":Lcom/samsung/upnp/event/SubscriptionRequest;
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/event/SubscriptionRequest;->setUnsubscribeRequest(Lcom/samsung/upnp/Service;)V

    .line 944
    invoke-virtual {v0}, Lcom/samsung/upnp/event/SubscriptionRequest;->post()Lcom/samsung/upnp/event/SubscriptionResponse;

    move-result-object v1

    .line 945
    .local v1, "subRes":Lcom/samsung/upnp/event/SubscriptionResponse;
    invoke-virtual {v1}, Lcom/samsung/upnp/event/SubscriptionResponse;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 946
    invoke-virtual {p1}, Lcom/samsung/upnp/Service;->clearSID()V

    .line 947
    const/4 v2, 0x1

    .line 949
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
