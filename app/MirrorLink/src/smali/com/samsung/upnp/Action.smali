.class public Lcom/samsung/upnp/Action;
.super Ljava/lang/Object;
.source "Action.java"


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "action"

.field private static final NAME:Ljava/lang/String; = "name"


# instance fields
.field private actionNode:Lcom/samsung/xml/Node;

.field private actionReq:Lcom/samsung/upnp/control/ActionRequest;

.field argumentList:Lcom/samsung/upnp/ArgumentList;

.field private serviceNode:Lcom/samsung/xml/Node;

.field private upnpStatus:Lcom/samsung/upnp/UPnPStatus;


# direct methods
.method public constructor <init>(Lcom/samsung/upnp/Action;)V
    .locals 3
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 106
    invoke-direct {p1}, Lcom/samsung/upnp/Action;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v0

    invoke-direct {p1}, Lcom/samsung/upnp/Action;->getActionNode()Lcom/samsung/xml/Node;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/upnp/Action;->getStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/upnp/Action;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;Lcom/samsung/upnp/UPnPStatus;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/upnp/Action;->argumentList:Lcom/samsung/upnp/ArgumentList;

    invoke-virtual {v0}, Lcom/samsung/upnp/ArgumentList;->clear()V

    .line 108
    invoke-virtual {p1}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/ArgumentList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/ArgumentList;

    iput-object v0, p0, Lcom/samsung/upnp/Action;->argumentList:Lcom/samsung/upnp/ArgumentList;

    .line 109
    return-void
.end method

.method public constructor <init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V
    .locals 1
    .param p1, "serviceNode"    # Lcom/samsung/xml/Node;
    .param p2, "actionNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/upnp/Action;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;Lcom/samsung/upnp/UPnPStatus;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;Lcom/samsung/upnp/UPnPStatus;)V
    .locals 7
    .param p1, "serviceNode"    # Lcom/samsung/xml/Node;
    .param p2, "actionNode"    # Lcom/samsung/xml/Node;
    .param p3, "status"    # Lcom/samsung/upnp/UPnPStatus;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/upnp/Action;->actionReq:Lcom/samsung/upnp/control/ActionRequest;

    .line 366
    new-instance v5, Lcom/samsung/upnp/UPnPStatus;

    invoke-direct {v5}, Lcom/samsung/upnp/UPnPStatus;-><init>()V

    iput-object v5, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    .line 80
    iput-object p1, p0, Lcom/samsung/upnp/Action;->serviceNode:Lcom/samsung/xml/Node;

    .line 81
    iput-object p2, p0, Lcom/samsung/upnp/Action;->actionNode:Lcom/samsung/xml/Node;

    .line 82
    if-eqz p3, :cond_0

    .line 83
    iput-object p3, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    .line 85
    :cond_0
    new-instance v5, Lcom/samsung/upnp/ArgumentList;

    invoke-direct {v5}, Lcom/samsung/upnp/ArgumentList;-><init>()V

    iput-object v5, p0, Lcom/samsung/upnp/Action;->argumentList:Lcom/samsung/upnp/ArgumentList;

    .line 86
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionNode()Lcom/samsung/xml/Node;

    move-result-object v5

    const-string v6, "argumentList"

    invoke-virtual {v5, v6}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v1

    .line 87
    .local v1, "argumentListNode":Lcom/samsung/xml/Node;
    if-eqz v1, :cond_1

    .line 88
    invoke-virtual {v1}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v4

    .line 89
    .local v4, "nodeCnt":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v4, :cond_2

    .line 97
    .end local v2    # "n":I
    .end local v4    # "nodeCnt":I
    :cond_1
    return-void

    .line 90
    .restart local v2    # "n":I
    .restart local v4    # "nodeCnt":I
    :cond_2
    invoke-virtual {v1, v2}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v3

    .line 91
    .local v3, "node":Lcom/samsung/xml/Node;
    invoke-static {v3}, Lcom/samsung/upnp/Argument;->isArgumentNode(Lcom/samsung/xml/Node;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 89
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 93
    :cond_3
    new-instance v0, Lcom/samsung/upnp/Argument;

    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v5

    invoke-direct {v0, v3, v5}, Lcom/samsung/upnp/Argument;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    .line 94
    .local v0, "argument":Lcom/samsung/upnp/Argument;
    iget-object v5, p0, Lcom/samsung/upnp/Action;->argumentList:Lcom/samsung/upnp/ArgumentList;

    invoke-virtual {v5, v0}, Lcom/samsung/upnp/ArgumentList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private clearOutputAgumentValues()V
    .locals 5

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v1

    .line 211
    .local v1, "allArgList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ArgumentList;->size()I

    move-result v0

    .line 212
    .local v0, "allArgCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 218
    return-void

    .line 213
    :cond_0
    invoke-virtual {v1, v3}, Lcom/samsung/upnp/ArgumentList;->getArgument(I)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 214
    .local v2, "arg":Lcom/samsung/upnp/Argument;
    invoke-virtual {v2}, Lcom/samsung/upnp/Argument;->isOutDirection()Z

    move-result v4

    if-nez v4, :cond_1

    .line 212
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 216
    :cond_1
    const-string v4, ""

    invoke-virtual {v2, v4}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getActionData()Lcom/samsung/upnp/xml/ActionData;
    .locals 2

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionNode()Lcom/samsung/xml/Node;

    move-result-object v0

    .line 243
    .local v0, "node":Lcom/samsung/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/upnp/xml/ActionData;

    .line 244
    .local v1, "userData":Lcom/samsung/upnp/xml/ActionData;
    if-nez v1, :cond_0

    .line 245
    new-instance v1, Lcom/samsung/upnp/xml/ActionData;

    .end local v1    # "userData":Lcom/samsung/upnp/xml/ActionData;
    invoke-direct {v1}, Lcom/samsung/upnp/xml/ActionData;-><init>()V

    .line 246
    .restart local v1    # "userData":Lcom/samsung/upnp/xml/ActionData;
    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 247
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/xml/ActionData;->setNode(Lcom/samsung/xml/Node;)V

    .line 249
    :cond_0
    return-object v1
.end method

.method private getActionNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/upnp/Action;->actionNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method private getControlResponse()Lcom/samsung/upnp/control/ControlResponse;
    .locals 1

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionData()Lcom/samsung/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/ActionData;->getControlResponse()Lcom/samsung/upnp/control/ControlResponse;

    move-result-object v0

    return-object v0
.end method

.method private getServiceNode()Lcom/samsung/xml/Node;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/upnp/Action;->serviceNode:Lcom/samsung/xml/Node;

    return-object v0
.end method

.method public static isActionNode(Lcom/samsung/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/xml/Node;

    .prologue
    .line 117
    const-string v0, "action"

    invoke-virtual {p0}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setControlResponse(Lcom/samsung/upnp/control/ControlResponse;)V
    .locals 1
    .param p1, "res"    # Lcom/samsung/upnp/control/ControlResponse;

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionData()Lcom/samsung/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/ActionData;->setControlResponse(Lcom/samsung/upnp/control/ControlResponse;)V

    .line 306
    return-void
.end method


# virtual methods
.method public getActionListener()Lcom/samsung/upnp/control/ActionListener;
    .locals 1

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionData()Lcom/samsung/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/upnp/xml/ActionData;->getActionListener()Lcom/samsung/upnp/control/ActionListener;

    move-result-object v0

    return-object v0
.end method

.method public getActionRequest()Lcom/samsung/upnp/control/ActionRequest;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/upnp/Action;->actionReq:Lcom/samsung/upnp/control/ActionRequest;

    return-object v0
.end method

.method public getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v1

    .line 177
    .local v1, "argList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ArgumentList;->size()I

    move-result v4

    .line 178
    .local v4, "nArgs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v4, :cond_0

    .line 186
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 179
    :cond_0
    invoke-virtual {v1, v3}, Lcom/samsung/upnp/ArgumentList;->getArgument(I)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 180
    .local v0, "arg":Lcom/samsung/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v2

    .line 181
    .local v2, "argName":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 178
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 183
    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1
.end method

.method public getArgumentList()Lcom/samsung/upnp/ArgumentList;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/upnp/Action;->argumentList:Lcom/samsung/upnp/ArgumentList;

    return-object v0
.end method

.method public getArgumentValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 223
    .local v0, "arg":Lcom/samsung/upnp/Argument;
    if-nez v0, :cond_0

    .line 224
    const-string v1, ""

    .line 225
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getControlStatus()Lcom/samsung/upnp/UPnPStatus;
    .locals 2

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getControlResponse()Lcom/samsung/upnp/control/ControlResponse;

    move-result-object v0

    .line 311
    .local v0, "controlResponse":Lcom/samsung/upnp/control/ControlResponse;
    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {v0}, Lcom/samsung/upnp/control/ControlResponse;->getUPnPError()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v1

    .line 314
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/upnp/UPnPStatus;

    invoke-direct {v1}, Lcom/samsung/upnp/UPnPStatus;-><init>()V

    goto :goto_0
.end method

.method public getInputArgumentList()Lcom/samsung/upnp/ArgumentList;
    .locals 6

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v1

    .line 149
    .local v1, "allArgList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ArgumentList;->size()I

    move-result v0

    .line 150
    .local v0, "allArgCnt":I
    new-instance v3, Lcom/samsung/upnp/ArgumentList;

    invoke-direct {v3}, Lcom/samsung/upnp/ArgumentList;-><init>()V

    .line 151
    .local v3, "argList":Lcom/samsung/upnp/ArgumentList;
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-lt v4, v0, :cond_0

    .line 157
    return-object v3

    .line 152
    :cond_0
    invoke-virtual {v1, v4}, Lcom/samsung/upnp/ArgumentList;->getArgument(I)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 153
    .local v2, "arg":Lcom/samsung/upnp/Argument;
    invoke-virtual {v2}, Lcom/samsung/upnp/Argument;->isInDirection()Z

    move-result v5

    if-nez v5, :cond_1

    .line 151
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {v3, v2}, Lcom/samsung/upnp/ArgumentList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionNode()Lcom/samsung/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getService()Lcom/samsung/upnp/Service;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lcom/samsung/upnp/Service;

    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getServiceNode()Lcom/samsung/xml/Node;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/upnp/Service;-><init>(Lcom/samsung/xml/Node;)V

    return-object v0
.end method

.method public getStatus()Lcom/samsung/upnp/UPnPStatus;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    return-object v0
.end method

.method public getStatusByString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    invoke-virtual {v1}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    invoke-virtual {v1}, Lcom/samsung/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public performActionListener(Lcom/samsung/upnp/control/ActionRequest;)Z
    .locals 5
    .param p1, "actionReq"    # Lcom/samsung/upnp/control/ActionRequest;

    .prologue
    .line 268
    iput-object p1, p0, Lcom/samsung/upnp/Action;->actionReq:Lcom/samsung/upnp/control/ActionRequest;

    .line 269
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getActionListener()Lcom/samsung/upnp/control/ActionListener;

    move-result-object v1

    .line 270
    .local v1, "listener":Lcom/samsung/upnp/control/ActionListener;
    if-nez v1, :cond_0

    .line 271
    const/4 v3, 0x0

    .line 286
    :goto_0
    return v3

    .line 273
    :cond_0
    new-instance v0, Lcom/samsung/upnp/control/ActionResponse;

    invoke-direct {v0}, Lcom/samsung/upnp/control/ActionResponse;-><init>()V

    .line 274
    .local v0, "actionRes":Lcom/samsung/upnp/control/ActionResponse;
    const/16 v3, 0x191

    invoke-virtual {p0, v3}, Lcom/samsung/upnp/Action;->setStatus(I)V

    .line 275
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->clearOutputAgumentValues()V

    .line 276
    invoke-interface {v1, p0}, Lcom/samsung/upnp/control/ActionListener;->actionControlReceived(Lcom/samsung/upnp/Action;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 277
    invoke-virtual {v0, p0}, Lcom/samsung/upnp/control/ActionResponse;->setResponse(Lcom/samsung/upnp/Action;)V

    .line 283
    :goto_1
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 284
    invoke-virtual {v0}, Lcom/samsung/upnp/control/ActionResponse;->print()V

    .line 285
    :cond_1
    invoke-virtual {p1, v0}, Lcom/samsung/upnp/control/ActionRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 286
    const/4 v3, 0x1

    goto :goto_0

    .line 280
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getStatus()Lcom/samsung/upnp/UPnPStatus;

    move-result-object v2

    .line 281
    .local v2, "upnpStatus":Lcom/samsung/upnp/UPnPStatus;
    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getCode()I

    move-result v3

    invoke-virtual {v2}, Lcom/samsung/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/samsung/upnp/control/ActionResponse;->setFaultResponse(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public postControlAction()Z
    .locals 7

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v0

    .line 325
    .local v0, "actionArgList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getInputArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v1

    .line 326
    .local v1, "actionInputArgList":Lcom/samsung/upnp/ArgumentList;
    new-instance v2, Lcom/samsung/upnp/control/ActionRequest;

    invoke-direct {v2}, Lcom/samsung/upnp/control/ActionRequest;-><init>()V

    .line 327
    .local v2, "ctrlReq":Lcom/samsung/upnp/control/ActionRequest;
    invoke-virtual {v2, p0, v1}, Lcom/samsung/upnp/control/ActionRequest;->setRequest(Lcom/samsung/upnp/Action;Lcom/samsung/upnp/ArgumentList;)V

    .line 328
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 329
    invoke-virtual {v2}, Lcom/samsung/upnp/control/ActionRequest;->print()V

    .line 330
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/upnp/control/ActionRequest;->post()Lcom/samsung/upnp/control/ActionResponse;

    move-result-object v3

    .line 331
    .local v3, "ctrlRes":Lcom/samsung/upnp/control/ActionResponse;
    invoke-static {}, Lcom/samsung/api/Debugs;->isOn()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 332
    invoke-virtual {v3}, Lcom/samsung/upnp/control/ActionResponse;->print()V

    .line 333
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/upnp/Action;->setControlResponse(Lcom/samsung/upnp/control/ControlResponse;)V

    .line 335
    invoke-virtual {v3}, Lcom/samsung/upnp/control/ActionResponse;->getStatusCode()I

    move-result v5

    .line 336
    .local v5, "statCode":I
    invoke-virtual {p0, v5}, Lcom/samsung/upnp/Action;->setStatus(I)V

    .line 337
    invoke-virtual {v3}, Lcom/samsung/upnp/control/ActionResponse;->isSuccessful()Z

    move-result v6

    if-nez v6, :cond_2

    .line 338
    const/4 v6, 0x0

    .line 341
    :goto_0
    return v6

    .line 339
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/upnp/control/ActionResponse;->getResponse()Lcom/samsung/upnp/ArgumentList;

    move-result-object v4

    .line 340
    .local v4, "outArgList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {v0, v4}, Lcom/samsung/upnp/ArgumentList;->set(Lcom/samsung/upnp/ArgumentList;)V

    .line 341
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public print()V
    .locals 10

    .prologue
    .line 350
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Action : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 351
    invoke-virtual {p0}, Lcom/samsung/upnp/Action;->getArgumentList()Lcom/samsung/upnp/ArgumentList;

    move-result-object v1

    .line 352
    .local v1, "argList":Lcom/samsung/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ArgumentList;->size()I

    move-result v4

    .line 353
    .local v4, "nArgs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v4, :cond_0

    .line 360
    return-void

    .line 354
    :cond_0
    invoke-virtual {v1, v3}, Lcom/samsung/upnp/ArgumentList;->getArgument(I)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 355
    .local v0, "arg":Lcom/samsung/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    .line 356
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 357
    .local v6, "value":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/upnp/Argument;->getDirection()Ljava/lang/String;

    move-result-object v2

    .line 358
    .local v2, "dir":Ljava/lang/String;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, " ["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 353
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public setActionListener(Lcom/samsung/upnp/control/ActionListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/control/ActionListener;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/samsung/upnp/Action;->getActionData()Lcom/samsung/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/xml/ActionData;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 264
    return-void
.end method

.method public setArgumentValue(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 204
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method public setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v0

    .line 197
    .local v0, "arg":Lcom/samsung/upnp/Argument;
    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 199
    :cond_0
    invoke-virtual {v0, p2}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setStatus(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 376
    invoke-static {p1}, Lcom/samsung/upnp/UPnPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 377
    return-void
.end method

.method public setStatus(ILjava/lang/String;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "descr"    # Ljava/lang/String;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    invoke-virtual {v0, p1}, Lcom/samsung/upnp/UPnPStatus;->setCode(I)V

    .line 371
    iget-object v0, p0, Lcom/samsung/upnp/Action;->upnpStatus:Lcom/samsung/upnp/UPnPStatus;

    invoke-virtual {v0, p2}, Lcom/samsung/upnp/UPnPStatus;->setDescription(Ljava/lang/String;)V

    .line 372
    return-void
.end method
