.class public final Lcom/mirrorlink/android/service/MirrorlinkManagerDefs;
.super Ljava/lang/Object;
.source "MirrorlinkManagerDefs.java"


# static fields
.field public static final CCC_CERTIFIED:I = 0x1

.field public static final CCC_POLICY:I = 0x1

.field public static final ENTITY_NAME:Ljava/lang/String; = "EntitiesName"

.field public static final ERROR_NO_DATA_CONNECTIVITY:I = 0x0

.field public static final FAIL:I = 0x2

.field public static final MEMBER_CERTIFIED:I = 0x2

.field public static final MIRRORLINK_MANAGER_SERVICE:Ljava/lang/String; = "com.samsung.android.MIRRORLINK_SERVICE"

.field public static final ML_AWARE:I = 0x0

.field public static final NONCERTIFIED:Ljava/lang/String; = "NONCERTIFIED"

.field public static final NONRESTRICTED:Ljava/lang/String; = "NONRESTRICTED"

.field public static final NOT_PRESENT:I = 0x3

.field public static final NO_VALIDAPPS_TO_PERFORM_REVOCATION:I = 0x3

.field public static final PASS:I = 0x1

.field public static final PKG_NAME:Ljava/lang/String; = "pkgName"

.field public static final RESTRICTED:Ljava/lang/String; = "RESTRICTED"

.field public static final SAMSUNG_POLICY:I = 0x0

.field public static final STATUS:Ljava/lang/String; = "AppStatus"

.field public static final UNCHECKED:Ljava/lang/String; = "UNCHECKED"

.field public static final VALID:Ljava/lang/String; = "VALID"

.field public static final VALID_DATE:Ljava/lang/String; = "VALID_DATE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
