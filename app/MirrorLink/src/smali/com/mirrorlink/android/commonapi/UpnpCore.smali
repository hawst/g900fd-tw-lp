.class public Lcom/mirrorlink/android/commonapi/UpnpCore;
.super Ljava/lang/Object;
.source "UpnpCore.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSUpnpCore"


# instance fields
.field private mClientID:Ljava/lang/String;

.field private mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

.field private mClientProfileObtained:Z

.field private mFriendlyName:Ljava/lang/String;

.field private mManufacturer:Ljava/lang/String;

.field private mMirrorlinkVersionMajor:I

.field private mMirrorlinkVersionMinor:I

.field private mModelName:Ljava/lang/String;

.field private mModelNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "TMSUpnpCore"

    const-string v1, "Enter Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;-><init>()V

    iput-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientProfileObtained:Z

    .line 29
    const-string v0, "TMSUpnpCore"

    const-string v1, "Exit Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method public getClientIdentifier()Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;
    .locals 2

    .prologue
    .line 33
    const-string v0, "TMSUpnpCore"

    const-string v1, "UpnpCore.getClientIdentifier() "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iget-boolean v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientProfileObtained:Z

    if-nez v0, :cond_0

    .line 35
    const-string v0, "TMSUpnpCore"

    const-string v1, "UpnpCore.getClientIdentifier() No Client Profile "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    goto :goto_0
.end method

.method public getClientMajorVersion()I
    .locals 3

    .prologue
    .line 66
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.getClientMajorVersion() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMajor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMajor:I

    return v0
.end method

.method public getClientMinorVersion()I
    .locals 3

    .prologue
    .line 71
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.getClientMinorVersion() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMinor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMinor:I

    return v0
.end method

.method public isClientVersionValid()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 51
    const-string v1, "TMSUpnpCore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UpnpCore.getClientMajorVersion() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientProfileObtained:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-boolean v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientProfileObtained:Z

    if-nez v1, :cond_0

    .line 53
    const-string v1, "TMSUpnpCore"

    const-string v2, "UpnpCore.getClientMajorVersion() Client profile not arrived"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_0
    return v0

    .line 57
    :cond_0
    iget v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMajor:I

    if-eq v1, v4, :cond_1

    iget v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMinor:I

    if-ne v1, v4, :cond_2

    .line 58
    :cond_1
    const-string v1, "TMSUpnpCore"

    const-string v2, "UpnpCore.getClientMajorVersion() Client arrived but not ml version"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setUpnpClientProfile(Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .prologue
    .line 79
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.setUpnpClientProfileMlVersion() Version"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMajor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mMirrorlinkVersionMinor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    iput-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientID:Ljava/lang/String;

    .line 82
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.setUpnpClientProfileMlVersion() ClientID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v0, p1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mFriendlyName:Ljava/lang/String;

    .line 85
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.setUpnpClientProfileMlVersion() FriendlyName "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    iput-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mManufacturer:Ljava/lang/String;

    .line 88
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.setUpnpClientProfileMlVersion() Manufacturer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mModelName:Ljava/lang/String;

    .line 91
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.setUpnpClientProfileMlVersion() ModelName "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mModelName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mModelNumber:Ljava/lang/String;

    .line 94
    const-string v0, "TMSUpnpCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpCore.setUpnpClientProfileMlVersion() ModelNumber "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mModelNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    iget-object v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->setClientId(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    iget-object v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->setFriendlyName(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    iget-object v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->setManufacturer(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    iget-object v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->setModelName(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientIdentifier:Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;

    iget-object v1, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mModelNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->setModelNumber(Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mirrorlink/android/commonapi/UpnpCore;->mClientProfileObtained:Z

    .line 133
    return-void
.end method
