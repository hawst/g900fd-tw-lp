.class Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;
.super Ljava/lang/Object;
.source "EffectAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EffectLongClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;
    .param p2, "x1"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 286
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mParentEffectGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->access$000(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Lcom/sec/android/app/filtermanager/views/EffectGridView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->startDrag(Landroid/view/View;)Z

    move-result v0

    .line 287
    return v0
.end method
