.class public Lcom/sec/android/app/filtermanager/ManageEffectActivity;
.super Landroid/app/Activity;
.source "ManageEffectActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;
    }
.end annotation


# static fields
.field static final ACTION_FILTER_CHANGED:Ljava/lang/String; = "com.samsung.android.action.DOWNFILTER_CHANGED"

.field public static final CONTENT_LOADER_FRAGMENT_TAG:Ljava/lang/String; = "LoaderFragment"

.field private static final REQUEST_SAMSUNG_APPS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ManageEffectActivity"

.field private static enableDoneButtonEffects:Z


# instance fields
.field private mCustomView:Landroid/view/View;

.field private mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

.field private mHelpToast:Landroid/widget/Toast;

.field private mPackageChangeReceiver:Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mHelpToast:Landroid/widget/Toast;

    .line 202
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/filtermanager/ManageEffectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/ManageEffectActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->saveOrder()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/filtermanager/ManageEffectActivity;)Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/ManageEffectActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    return-object v0
.end method

.method private initLayout(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)V
    .locals 3
    .param p1, "loaderFragment"    # Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .prologue
    .line 159
    const v1, 0x7f070006

    invoke-virtual {p0, v1}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/filtermanager/ManageEffectActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity$1;-><init>(Lcom/sec/android/app/filtermanager/ManageEffectActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const v1, 0x7f070005

    invoke-virtual {p0, v1}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/filtermanager/ManageEffectActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity$2;-><init>(Lcom/sec/android/app/filtermanager/ManageEffectActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    const/high16 v1, 0x7f070000

    invoke-virtual {p0, v1}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/filtermanager/views/EffectGridView;

    iput-object v1, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .line 177
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/filtermanager/views/EffectGridView;Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)V

    .line 178
    .local v0, "adapter":Lcom/sec/android/app/filtermanager/views/EffectAdapter;
    invoke-virtual {p1, v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->setOwner(Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    sget-boolean v1, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->enableDoneButtonEffects:Z

    if-eqz v1, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setEnableItems()V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setDisableItems()V

    goto :goto_0
.end method

.method private saveOrder()V
    .locals 19

    .prologue
    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    .line 236
    .local v12, "fm":Landroid/app/FragmentManager;
    const-string v2, "LoaderFragment"

    invoke-virtual {v12, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v17

    check-cast v17, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .line 238
    .local v17, "loaderFragment":Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 239
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 241
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getFilterUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getFilterProjection()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 243
    if-nez v7, :cond_1

    .line 297
    if-eqz v7, :cond_0

    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-array v10, v2, [Ljava/lang/String;

    .line 249
    .local v10, "effectName":[Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-array v14, v2, [I

    .line 250
    .local v14, "id":[I
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-array v8, v2, [I

    .line 252
    .local v8, "deleted":[I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 253
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v13, v2, :cond_2

    .line 254
    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v13

    .line 255
    const/4 v2, 0x6

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aput v2, v8, v13

    .line 256
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aput v2, v14, v13

    .line 258
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    .line 253
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 260
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 262
    const/4 v13, 0x0

    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v13, v2, :cond_6

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-virtual {v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getEffectAdapter()Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getEffectInfo(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;

    move-result-object v15

    .line 264
    .local v15, "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/content/ContentValues;

    move-object/from16 v18, v0

    .line 266
    .local v18, "values":[Landroid/content/ContentValues;
    const/4 v2, 0x0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    aput-object v3, v18, v2

    .line 267
    const/4 v2, 0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    aput-object v3, v18, v2

    .line 269
    const/4 v2, 0x0

    aget-object v2, v18, v2

    const-string v3, "filter_order"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 271
    invoke-virtual {v15}, Lcom/sec/android/app/filtermanager/ExtEffectItem;->isHidden()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 272
    const/4 v2, 0x1

    aget-object v2, v18, v2

    const-string v3, "deleted"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 277
    :goto_3
    const/4 v11, 0x0

    .line 278
    .local v11, "findFilter":I
    const/16 v16, 0x0

    .local v16, "k":I
    :goto_4
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_3

    .line 279
    iget v2, v15, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mId:I

    aget v3, v14, v13

    if-ne v2, v3, :cond_5

    .line 280
    move/from16 v11, v16

    .line 285
    :cond_3
    invoke-interface {v7, v11}, Landroid/database/Cursor;->move(I)Z

    .line 287
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getBaseUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v18, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v15, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 288
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getBaseUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v18, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v15, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 290
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 262
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 274
    .end local v11    # "findFilter":I
    .end local v16    # "k":I
    :cond_4
    const/4 v2, 0x1

    aget-object v2, v18, v2

    const-string v3, "deleted"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 294
    .end local v8    # "deleted":[I
    .end local v10    # "effectName":[Ljava/lang/String;
    .end local v13    # "i":I
    .end local v14    # "id":[I
    .end local v15    # "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    .end local v18    # "values":[Landroid/content/ContentValues;
    :catch_0
    move-exception v9

    .line 295
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 297
    if-eqz v7, :cond_0

    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 278
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v8    # "deleted":[I
    .restart local v10    # "effectName":[Ljava/lang/String;
    .restart local v11    # "findFilter":I
    .restart local v13    # "i":I
    .restart local v14    # "id":[I
    .restart local v15    # "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    .restart local v16    # "k":I
    .restart local v18    # "values":[Landroid/content/ContentValues;
    :cond_5
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_4

    .line 297
    .end local v11    # "findFilter":I
    .end local v15    # "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    .end local v16    # "k":I
    .end local v18    # "values":[Landroid/content/ContentValues;
    :cond_6
    if-eqz v7, :cond_0

    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 297
    .end local v8    # "deleted":[I
    .end local v10    # "effectName":[Ljava/lang/String;
    .end local v13    # "i":I
    .end local v14    # "id":[I
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_7

    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 190
    packed-switch p1, :pswitch_data_0

    .line 196
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 197
    return-void

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->saveOrder()V

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->finish()V

    .line 317
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/high16 v3, 0x7f030000

    .line 210
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 212
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->onDestroy()V

    .line 214
    invoke-virtual {p0, v3}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setContentView(I)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "LoaderFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-direct {p0, v1}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->initLayout(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)V

    .line 227
    :goto_0
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 228
    const-string v1, "ManageEffectActivity"

    const-string v2, "!!!!!!!!!!!!!!!ORIENTATION_LANDSCAPE!!!!!!!!!!!!!!!!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_0
    :goto_1
    return-void

    .line 218
    :cond_1
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 219
    .local v0, "displaymetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->onDestroy()V

    .line 223
    invoke-virtual {p0, v3}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setContentView(I)V

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "LoaderFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-direct {p0, v1}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->initLayout(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)V

    goto :goto_0

    .line 229
    .end local v0    # "displaymetrics":Landroid/util/DisplayMetrics;
    :cond_2
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 230
    const-string v1, "ManageEffectActivity"

    const-string v2, "!!!!!!!!!!!!!!!ORIENTATION_PORTRAIT!!!!!!!!!!!!!!!!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x2

    const/4 v7, 0x0

    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    sput-boolean v7, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->enableDoneButtonEffects:Z

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 61
    .local v1, "fm":Landroid/app/FragmentManager;
    const-string v4, "LoaderFragment"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .line 65
    .local v2, "loaderFragment":Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    if-nez v2, :cond_0

    .line 66
    new-instance v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .end local v2    # "loaderFragment":Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    invoke-direct {v2}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;-><init>()V

    .line 67
    .restart local v2    # "loaderFragment":Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const-string v5, "LoaderFragment"

    invoke-virtual {v4, v2, v5}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 70
    :cond_0
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->requestWindowFeature(I)Z

    .line 71
    const/high16 v4, 0x7f060000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setTheme(I)V

    .line 73
    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setContentView(I)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030001

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mCustomView:Landroid/view/View;

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mCustomView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 80
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const v5, 0x7f050004

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setTitle(I)V

    .line 84
    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/4 v4, 0x5

    invoke-direct {v3, v8, v8, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    .line 87
    .local v3, "params":Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mCustomView:Landroid/view/View;

    invoke-virtual {v4, v5, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 100
    .end local v3    # "params":Landroid/app/ActionBar$LayoutParams;
    :cond_1
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->initLayout(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)V

    .line 101
    new-instance v4, Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;

    invoke-direct {v4, p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;-><init>(Lcom/sec/android/app/filtermanager/ManageEffectActivity;)V

    iput-object v4, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mPackageChangeReceiver:Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;

    .line 103
    iget-object v4, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mPackageChangeReceiver:Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "com.samsung.android.action.DOWNFILTER_CHANGED"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 104
    iget-object v4, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-virtual {v4}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->refreshView()V

    .line 107
    const v4, 0x7f050006

    invoke-static {p0, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mHelpToast:Landroid/widget/Toast;

    .line 108
    iget-object v4, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mHelpToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 109
    return-void

    .line 91
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 95
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 96
    .local v0, "displaymetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mPackageChangeReceiver:Lcom/sec/android/app/filtermanager/ManageEffectActivity$PackageChangeReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->onDestroy()V

    .line 117
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mHelpToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->mHelpToast:Landroid/widget/Toast;

    .line 131
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->invalidateOptionsMenu()V

    .line 124
    return-void
.end method

.method public setDisableItems()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 310
    sput-boolean v1, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->enableDoneButtonEffects:Z

    .line 311
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 312
    return-void
.end method

.method public setEnableItems()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 305
    sput-boolean v1, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->enableDoneButtonEffects:Z

    .line 306
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 307
    return-void
.end method
