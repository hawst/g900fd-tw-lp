.class Lcom/sec/android/app/filtermanager/views/EffectGridView$2;
.super Landroid/os/Handler;
.source "EffectGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x4

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 92
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    :pswitch_0
    return-void

    .line 94
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$600(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$700(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I
    invoke-static {v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$600(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v2

    # invokes: Lcom/sec/android/app/filtermanager/views/EffectGridView;->insert(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$800(Lcom/sec/android/app/filtermanager/views/EffectGridView;II)V

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I
    invoke-static {v0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$702(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I
    invoke-static {v0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$602(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I
    invoke-static {v0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$902(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I
    invoke-static {v0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1002(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1102(Lcom/sec/android/app/filtermanager/views/EffectGridView;Z)Z

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 108
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 114
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 120
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    const/16 v1, 0xc8

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I
    invoke-static {v0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1202(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I

    goto/16 :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
