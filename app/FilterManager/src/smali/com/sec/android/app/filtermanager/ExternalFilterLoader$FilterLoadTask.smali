.class Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;
.super Landroid/os/AsyncTask;
.source "ExternalFilterLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FilterLoadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mEffects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/filtermanager/ExtEffectItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mOrder:Landroid/util/SparseIntArray;

.field private final mParentLoaderRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/filtermanager/ExternalFilterLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final mReportUpdates:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;Landroid/content/Context;Z)V
    .locals 1
    .param p1, "loader"    # Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "reportUpdates"    # Z

    .prologue
    .line 324
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mEffects:Ljava/util/ArrayList;

    .line 311
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mOrder:Landroid/util/SparseIntArray;

    .line 325
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mParentLoaderRef:Ljava/lang/ref/WeakReference;

    .line 326
    iput-object p2, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mContext:Landroid/content/Context;

    .line 327
    iput-boolean p3, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mReportUpdates:Z

    .line 329
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 308
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 14
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 333
    const/4 v8, 0x0

    .line 336
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    # getter for: Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->access$200()Landroid/net/Uri;

    move-result-object v1

    # getter for: Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->access$300()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 338
    if-nez v8, :cond_1

    .line 339
    const/4 v0, 0x0

    .line 376
    if-eqz v8, :cond_0

    .line 377
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 381
    :cond_0
    :goto_0
    return-object v0

    .line 343
    :cond_1
    const/4 v7, 0x0

    .line 345
    .local v7, "count":I
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 346
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 347
    .local v1, "id":I
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 348
    .local v3, "effectName":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 349
    .local v2, "filterName":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 350
    .local v12, "version":Ljava/lang/String;
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 351
    .local v4, "title":Ljava/lang/String;
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 352
    .local v5, "packageName":Ljava/lang/String;
    const/4 v0, 0x6

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 353
    .local v9, "deleted":Ljava/lang/String;
    const/4 v0, 0x7

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 356
    .local v11, "order":I
    if-eqz v3, :cond_2

    const-string v0, "Unnamed filter"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v2, :cond_2

    const-string v0, "Unnamed filter"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v4, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 364
    iget-object v13, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mEffects:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/filtermanager/ExtEffectItem;

    const-string v6, "1"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/filtermanager/ExtEffectItem;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mOrder:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v11, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 366
    add-int/lit8 v7, v7, 0x1

    .line 369
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mReportUpdates:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xf

    if-ge v7, v0, :cond_2

    rem-int/lit8 v0, v7, 0x5

    if-nez v0, :cond_2

    .line 370
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 373
    .end local v1    # "id":I
    .end local v2    # "filterName":Ljava/lang/String;
    .end local v3    # "effectName":Ljava/lang/String;
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v7    # "count":I
    .end local v9    # "deleted":Ljava/lang/String;
    .end local v11    # "order":I
    .end local v12    # "version":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 374
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 376
    if-eqz v8, :cond_3

    .line 377
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 381
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 376
    .restart local v7    # "count":I
    :cond_4
    if-eqz v8, :cond_3

    .line 377
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 376
    .end local v7    # "count":I
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_5

    .line 377
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 308
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 395
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mParentLoaderRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .line 397
    .local v0, "loader":Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    if-eqz v0, :cond_0

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mEffects:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mOrder:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->setFilters(Ljava/util/ArrayList;Landroid/util/SparseIntArray;)V

    .line 400
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 308
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 386
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mParentLoaderRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .line 388
    .local v0, "loader":Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
    if-eqz v0, :cond_0

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mEffects:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->mOrder:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->setFilters(Ljava/util/ArrayList;Landroid/util/SparseIntArray;)V

    .line 391
    :cond_0
    return-void
.end method
