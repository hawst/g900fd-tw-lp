.class Lcom/sec/android/app/filtermanager/views/EffectGridView$1;
.super Ljava/lang/Object;
.source "EffectGridView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$000(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mX:F
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$100(Lcom/sec/android/app/filtermanager/views/EffectGridView;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mY:F
    invoke-static {v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$200(Lcom/sec/android/app/filtermanager/views/EffectGridView;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I
    invoke-static {v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$000(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v3

    # invokes: Lcom/sec/android/app/filtermanager/views/EffectGridView;->scroll(FFI)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$300(Lcom/sec/android/app/filtermanager/views/EffectGridView;FFI)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 80
    return-void
.end method
