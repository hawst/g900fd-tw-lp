.class Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;
.super Ljava/lang/Object;
.source "EffectGridView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationTimeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Lcom/sec/android/app/filtermanager/views/EffectGridView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p2, "x1"    # Lcom/sec/android/app/filtermanager/views/EffectGridView$1;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v1, 0xa

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1200(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v0

    if-le v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # -= operator for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I
    invoke-static {v0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1220(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1200(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I

    move-result v0

    if-le v0, v1, :cond_1

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeHandler:Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1300(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

    move-result-object v1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1102(Lcom/sec/android/app/filtermanager/views/EffectGridView;Z)Z

    goto :goto_0
.end method
