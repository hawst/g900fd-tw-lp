.class public Lcom/sec/android/app/filtermanager/EffectDragListener;
.super Ljava/lang/Object;
.source "EffectDragListener.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# instance fields
.field private mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

.field private mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

.field private mDropped:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/filtermanager/DragSource;Lcom/sec/android/app/filtermanager/DropTarget;)V
    .locals 1
    .param p1, "dragSource"    # Lcom/sec/android/app/filtermanager/DragSource;
    .param p2, "dropTarget"    # Lcom/sec/android/app/filtermanager/DropTarget;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropped:Z

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    .line 27
    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    .line 31
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v1

    .line 67
    :cond_1
    return v0

    .line 33
    :pswitch_1
    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropped:Z

    .line 34
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v4

    invoke-interface {v2, v3, v4, p1}, Lcom/sec/android/app/filtermanager/DragSource;->isDragAllowed(FFLjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-interface {v0}, Lcom/sec/android/app/filtermanager/DragSource;->initializeViewsForDrag()V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v3

    invoke-interface {v0, v2, v3, p1}, Lcom/sec/android/app/filtermanager/DragSource;->onDragStart(FFLjava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v4

    invoke-interface {v0, v2, v3, v4, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->onDragOver(Lcom/sec/android/app/filtermanager/DragSource;FFLjava/lang/Object;)V

    goto :goto_0

    .line 45
    :pswitch_3
    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropped:Z

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    invoke-interface {v0, v3, v3, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->isDropAccepted(FFLjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-interface {v0, v2, v3, v3, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->onDragOver(Lcom/sec/android/app/filtermanager/DragSource;FFLjava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_4
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropped:Z

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v3

    invoke-interface {v0, v2, v3, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->isDropAccepted(FFLjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v4

    invoke-interface {v0, v2, v3, v4, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->onDrop(Lcom/sec/android/app/filtermanager/DragSource;FFLjava/lang/Object;)V

    goto :goto_0

    .line 58
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropped:Z

    if-nez v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    invoke-interface {v0, v3, v3, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->isDropAccepted(FFLjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDropTarget:Lcom/sec/android/app/filtermanager/DropTarget;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-interface {v0, v2, v3, v3, p1}, Lcom/sec/android/app/filtermanager/DropTarget;->onDrop(Lcom/sec/android/app/filtermanager/DragSource;FFLjava/lang/Object;)V

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/EffectDragListener;->mDragSource:Lcom/sec/android/app/filtermanager/DragSource;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v3

    invoke-interface {v0, v2, v3, p1}, Lcom/sec/android/app/filtermanager/DragSource;->onDragEnded(FFLjava/lang/Object;)V

    goto/16 :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
