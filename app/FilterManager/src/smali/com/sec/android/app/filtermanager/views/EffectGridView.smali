.class public Lcom/sec/android/app/filtermanager/views/EffectGridView;
.super Landroid/widget/GridView;
.source "EffectGridView.java"

# interfaces
.implements Lcom/sec/android/app/filtermanager/DragSource;
.implements Lcom/sec/android/app/filtermanager/DropTarget;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;,
        Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;,
        Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;,
        Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;
    }
.end annotation


# static fields
.field private static final ACTION_DRAG_ENDED:I = 0x4

.field private static final ACTION_DROP_COMPLETED:I = 0x1

.field private static final ACTION_MAKE_DRAGVIEW_INVISIBLE:I = 0x3

.field private static final ACTION_RESET_ANIMATION_TIME:I = 0x5

.field private static final ANIMATION_DECREASE_OFFSET:I = 0xa

.field private static final ANIMATION_DURATION:I = 0xc8

.field private static final ANIMATION_OFFSET:I = 0x0

.field private static final DRAGVIEW_ALPHA:I = 0xff

.field private static final SCROLL_OFFSET:I = 0xa

.field private static final SCROLL_THRESHHOLD:I = 0x96

.field private static final TAG:Ljava/lang/String; = "EffectGridView"


# instance fields
.field private isScrolling:Z

.field private lastY:F

.field private mAnimating:Z

.field private mAnimationTimeHandler:Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

.field private mAnimationTimeLeft:I

.field private mBusy:Z

.field private mChildcount:I

.field private mContext:Landroid/content/Context;

.field private mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

.field private mCursorView:Landroid/widget/ImageView;

.field private mDragOverPosition:I

.field private mDragPosition:I

.field private mDragView:Landroid/view/View;

.field private mDraggedScroll:I

.field private mDraggedScrollTask:Ljava/lang/Runnable;

.field private mDropOnPosition:I

.field private mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

.field private mFirstVisiblePos:I

.field private mMainHandler:Landroid/os/Handler;

.field private mPrevDragOverposition:I

.field private mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

.field private mScrollAdjustment:Z

.field private mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 158
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    .line 54
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    .line 57
    iput v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    .line 58
    iput v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    .line 68
    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    .line 69
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mX:F

    .line 70
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mY:F

    .line 71
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->lastY:F

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isScrolling:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    .line 74
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    .line 159
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->initView()V

    .line 161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 148
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    .line 54
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    .line 57
    iput v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    .line 58
    iput v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    .line 68
    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    .line 69
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mX:F

    .line 70
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mY:F

    .line 71
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->lastY:F

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isScrolling:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    .line 74
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    .line 149
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->initView()V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "s"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 170
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    .line 54
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    .line 57
    iput v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    .line 58
    iput v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    .line 68
    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    .line 69
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mX:F

    .line 70
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mY:F

    .line 71
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->lastY:F

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isScrolling:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    .line 74
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$1;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$2;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    .line 171
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    .line 172
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->initView()V

    .line 173
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/filtermanager/views/EffectGridView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mX:F

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    return p1
.end method

.method static synthetic access$1102(Lcom/sec/android/app/filtermanager/views/EffectGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    return p1
.end method

.method static synthetic access$1220(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeHandler:Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/filtermanager/views/EffectGridView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mY:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/filtermanager/views/EffectGridView;FFI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F
    .param p3, "x3"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->scroll(FFI)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/filtermanager/views/EffectGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/filtermanager/views/EffectGridView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->insert(II)V

    return-void
.end method

.method static synthetic access$902(Lcom/sec/android/app/filtermanager/views/EffectGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    return p1
.end method

.method private declared-synchronized checkScrolling(FF)Z
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 367
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getMeasuredHeight()I

    move-result v0

    .line 369
    .local v0, "height":I
    iget-boolean v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    if-nez v3, :cond_3

    .line 370
    int-to-float v3, v0

    sub-float/2addr v3, p2

    const/high16 v4, 0x43160000    # 150.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 371
    const/16 v3, 0xa

    invoke-virtual {p0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->canScrollVertically(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 372
    const/16 v2, 0xa

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 375
    :cond_1
    int-to-float v3, v0

    sub-float/2addr v3, p2

    add-int/lit16 v4, v0, -0x96

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    .line 377
    const/16 v3, -0xa

    :try_start_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->canScrollVertically(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 378
    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->lastY:F

    shr-int/lit8 v4, v0, 0x1

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 379
    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->lastY:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 380
    const/16 v2, -0xa

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 367
    .end local v0    # "height":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 383
    .restart local v0    # "height":I
    :cond_2
    const/4 v1, 0x0

    :try_start_2
    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    move v1, v2

    .line 384
    goto :goto_0

    .line 390
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v2

    .line 391
    goto :goto_0
.end method

.method private getChildPosition(II)I
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, -0x1

    .line 266
    if-eq p1, v2, :cond_0

    if-ne p2, v2, :cond_1

    .line 279
    :cond_0
    :goto_0
    return v2

    .line 270
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 272
    .local v0, "hitRectChild":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 273
    invoke-virtual {p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 274
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_3

    iget v3, v0, Landroid/graphics/Rect;->right:I

    if-lt v3, p1, :cond_2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    if-gt v3, p2, :cond_3

    :cond_2
    iget v3, v0, Landroid/graphics/Rect;->left:I

    if-lt v3, p1, :cond_4

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    if-le v3, p2, :cond_4

    .line 276
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    add-int/2addr v2, v1

    goto :goto_0

    .line 272
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getChildPosition(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 253
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 254
    .local v1, "hitRectView":Landroid/graphics/Rect;
    invoke-virtual {p1, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 255
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 256
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 257
    .local v0, "hitRectChild":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 258
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v3

    add-int/2addr v3, v2

    .line 262
    .end local v0    # "hitRectChild":Landroid/graphics/Rect;
    :goto_1
    return v3

    .line 255
    .restart local v0    # "hitRectChild":Landroid/graphics/Rect;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 262
    .end local v0    # "hitRectChild":Landroid/graphics/Rect;
    :cond_1
    const/4 v3, -0x1

    goto :goto_1
.end method

.method private getDragShowView(Landroid/view/View;)Landroid/view/View;
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 236
    const v5, 0x7f070007

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 237
    .local v2, "framelayout":Landroid/widget/FrameLayout;
    iget-object v5, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/filtermanager/ManageEffectActivity;

    const v6, 0x7f070002

    invoke-virtual {v5, v6}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 239
    .local v1, "dragShowView":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f04000b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v4, v5

    .line 240
    .local v4, "width":I
    iget-object v5, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f04000a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .line 241
    .local v3, "height":I
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->destroyDrawingCache()V

    .line 244
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->buildDrawingCache()V

    .line 246
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 247
    .local v0, "bd":Landroid/graphics/drawable/BitmapDrawable;
    const/16 v5, 0xff

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 248
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 249
    return-object v1
.end method

.method private hideCursorView()V
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCursorView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCursorView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 750
    :cond_0
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 178
    new-instance v0, Lcom/sec/android/app/filtermanager/EffectDragListener;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/filtermanager/EffectDragListener;-><init>(Lcom/sec/android/app/filtermanager/DragSource;Lcom/sec/android/app/filtermanager/DropTarget;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 179
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Lcom/sec/android/app/filtermanager/views/EffectGridView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeHandler:Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

    .line 180
    return-void
.end method

.method private insert(II)V
    .locals 6
    .param p1, "dropOnPosition"    # I
    .param p2, "dragPosition"    # I

    .prologue
    .line 283
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getEffectInfo(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;

    move-result-object v2

    .line 284
    .local v2, "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    const-string v3, "EffectGridView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert dropOnPosition = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", dragPosition = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", getEffectInfo = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getAlignedPosition(I)I

    move-result v1

    .line 287
    .local v1, "alignedDropOnPosition":I
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getAlignedPosition(I)I

    move-result v0

    .line 289
    .local v0, "alignedDragPosition":I
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->insert(II)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->notifyDataSetChanged()V

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->reorderDB(II)V

    .line 292
    return-void
.end method

.method private moveBackward(III)V
    .locals 20
    .param p1, "fromPos"    # I
    .param p2, "toPos"    # I
    .param p3, "ignore"    # I

    .prologue
    .line 631
    const-string v1, "EffectGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "moveBackward fromPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  toPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v12

    .line 640
    .local v12, "firstVisiblePosition":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v10

    .line 641
    .local v10, "count":I
    new-array v15, v10, [Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    .line 644
    .local v15, "tempAnimation":[Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;
    sub-int v18, p2, p1

    .line 645
    .local v18, "total":I
    const/16 v16, 0xc8

    .line 646
    .local v16, "tempDuration":I
    const/16 v1, 0xc8

    div-int v11, v1, v18

    .line 647
    .local v11, "durationScale":I
    const/16 v17, 0x0

    .line 648
    .local v17, "tempOffset":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v14, v1, p1

    .line 650
    .local v14, "temp":Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;
    move/from16 v13, p1

    .local v13, "i":I
    :goto_0
    move/from16 v0, p2

    if-gt v13, v0, :cond_4

    .line 651
    move/from16 v0, p3

    if-ne v13, v0, :cond_0

    .line 650
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 655
    :cond_0
    mul-int v1, v17, v11

    sub-int v8, v16, v1

    .line 656
    .local v8, "duration":I
    mul-int v1, v17, v11

    add-int/lit8 v9, v1, 0x0

    .line 657
    .local v9, "offset":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->isMoved()Z

    move-result v1

    if-nez v1, :cond_1

    .line 658
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedBackWard()V

    .line 659
    const/4 v4, 0x0

    .line 660
    .local v4, "fromX":I
    const/4 v5, 0x0

    .line 661
    .local v5, "fromY":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v6, v1, v2

    .line 662
    .local v6, "toX":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v7, v1, v2

    .line 663
    .local v7, "toY":I
    sub-int v19, v13, v12

    new-instance v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v3, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V

    aput-object v1, v15, v19

    .line 664
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    add-int/lit8 v2, v13, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v3, v3, v13

    aput-object v3, v1, v2

    .line 683
    .end local v4    # "fromX":I
    .end local v5    # "fromY":I
    .end local v6    # "toX":I
    .end local v7    # "toY":I
    :goto_2
    add-int/lit8 v17, v17, 0x1

    .line 684
    sub-int v1, v13, v12

    aget-object v1, v15, v1

    # invokes: Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->startAnimation()V
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->access$1600(Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;)V

    goto :goto_1

    .line 665
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->isMovedForward()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 666
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedinPlace()V

    .line 667
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v4, v1, v2

    .line 668
    .restart local v4    # "fromX":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v5, v1, v2

    .line 669
    .restart local v5    # "fromY":I
    const/4 v6, 0x0

    .line 670
    .restart local v6    # "toX":I
    const/4 v7, 0x0

    .line 671
    .restart local v7    # "toY":I
    sub-int v19, v13, v12

    new-instance v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v3, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V

    aput-object v1, v15, v19

    .line 672
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    add-int/lit8 v2, v13, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v3, v3, v13

    aput-object v3, v1, v2

    goto :goto_2

    .line 673
    .end local v4    # "fromX":I
    .end local v5    # "fromY":I
    .end local v6    # "toX":I
    .end local v7    # "toY":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->isMovedBackword()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 674
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedinPlace()V

    .line 675
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v4, v1, v2

    .line 676
    .restart local v4    # "fromX":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v13

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v5, v1, v2

    .line 677
    .restart local v5    # "fromY":I
    const/4 v6, 0x0

    .line 678
    .restart local v6    # "toX":I
    const/4 v7, 0x0

    .line 679
    .restart local v7    # "toY":I
    sub-int v19, v13, v12

    new-instance v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v13

    iget-object v3, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V

    aput-object v1, v15, v19

    goto/16 :goto_2

    .line 681
    .end local v4    # "fromX":I
    .end local v5    # "fromY":I
    .end local v6    # "toX":I
    .end local v7    # "toY":I
    :cond_3
    const-string v1, "EffectGridView"

    const-string v2, "wrong position check....2"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 687
    .end local v8    # "duration":I
    .end local v9    # "offset":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aput-object v14, v1, p2

    .line 688
    return-void
.end method

.method private moveForward(III)V
    .locals 16
    .param p1, "fromPos"    # I
    .param p2, "toPos"    # I
    .param p3, "ignore"    # I

    .prologue
    .line 576
    const-string v1, "EffectGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "moveForward  fromPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  toPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v11

    .line 585
    .local v11, "firstVisiblePosition":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v10

    .line 587
    .local v10, "count":I
    new-array v14, v10, [Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    .line 588
    .local v14, "tempAnimation":[Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v13, v1, p2

    .line 590
    .local v13, "temp":Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;
    move/from16 v12, p2

    .local v12, "i":I
    :goto_0
    move/from16 v0, p1

    if-lt v12, v0, :cond_4

    .line 591
    move/from16 v0, p3

    if-ne v12, v0, :cond_0

    .line 590
    :goto_1
    add-int/lit8 v12, v12, -0x1

    goto :goto_0

    .line 595
    :cond_0
    const/16 v8, 0xc8

    .line 596
    .local v8, "duration":I
    const/4 v9, 0x0

    .line 597
    .local v9, "offset":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->isMoved()Z

    move-result v1

    if-nez v1, :cond_1

    .line 598
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedForward()V

    .line 599
    const/4 v4, 0x0

    .line 600
    .local v4, "fromX":I
    const/4 v5, 0x0

    .line 601
    .local v5, "fromY":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v6, v1, v2

    .line 602
    .local v6, "toX":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v7, v1, v2

    .line 603
    .local v7, "toY":I
    sub-int v15, v12, v11

    new-instance v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v3, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V

    aput-object v1, v14, v15

    .line 604
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    add-int/lit8 v2, v12, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v3, v3, v12

    aput-object v3, v1, v2

    .line 624
    .end local v4    # "fromX":I
    .end local v5    # "fromY":I
    .end local v6    # "toX":I
    .end local v7    # "toY":I
    :goto_2
    sub-int v1, v12, v11

    aget-object v1, v14, v1

    # invokes: Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->startAnimation()V
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->access$1600(Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;)V

    goto :goto_1

    .line 605
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->isMovedBackword()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 606
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedinPlace()V

    .line 607
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v4, v1, v2

    .line 608
    .restart local v4    # "fromX":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v5, v1, v2

    .line 609
    .restart local v5    # "fromY":I
    const/4 v6, 0x0

    .line 610
    .restart local v6    # "toX":I
    const/4 v7, 0x0

    .line 611
    .restart local v7    # "toY":I
    sub-int v15, v12, v11

    new-instance v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v3, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V

    aput-object v1, v14, v15

    .line 612
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    add-int/lit8 v2, v12, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v3, v3, v12

    aput-object v3, v1, v2

    goto :goto_2

    .line 613
    .end local v4    # "fromX":I
    .end local v5    # "fromY":I
    .end local v6    # "toX":I
    .end local v7    # "toY":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->isMovedForward()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 614
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedinPlace()V

    .line 615
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v4, v1, v2

    .line 616
    .restart local v4    # "fromX":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v1, v1, v12

    iget-object v1, v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v5, v1, v2

    .line 617
    .restart local v5    # "fromY":I
    const/4 v6, 0x0

    .line 618
    .restart local v6    # "toX":I
    const/4 v7, 0x0

    .line 619
    .restart local v7    # "toY":I
    sub-int v15, v12, v11

    new-instance v1, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v2, v2, v12

    iget-object v3, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V

    aput-object v1, v14, v15

    goto/16 :goto_2

    .line 621
    .end local v4    # "fromX":I
    .end local v5    # "fromY":I
    .end local v6    # "toX":I
    .end local v7    # "toY":I
    :cond_3
    const-string v1, "EffectGridView"

    const-string v2, "wrong position check....1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 627
    .end local v8    # "duration":I
    .end local v9    # "offset":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aput-object v13, v1, p1

    .line 628
    return-void
.end method

.method private performDragEnd()V
    .locals 6

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->hideCursorView()V

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    :goto_0
    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 305
    return-void

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized scroll(FFI)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "offset"    # I

    .prologue
    const/4 v4, -0x1

    .line 395
    monitor-enter p0

    float-to-int v2, p1

    float-to-int v3, p2

    :try_start_0
    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildPosition(II)I

    move-result v0

    .line 397
    .local v0, "dropPos":I
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    .line 398
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->checkScrolling(FF)Z

    move-result v2

    if-nez v2, :cond_0

    if-eq v0, v4, :cond_8

    .line 399
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isScrolling:Z

    .line 400
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    .line 401
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    if-eq v2, v3, :cond_2

    .line 402
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->initializeViewsForDrag()V

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    .line 407
    :cond_2
    const/16 v2, 0xa

    invoke-virtual {p0, p3, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->smoothScrollBy(II)V

    .line 408
    if-ne v0, v4, :cond_6

    .line 409
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    if-lez v2, :cond_5

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getLastVisiblePosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    .line 418
    :cond_3
    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 419
    .local v1, "hitRectChild":Landroid/graphics/Rect;
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 420
    invoke-direct {p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->showCursorView(Landroid/graphics/Rect;)V

    .line 422
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    if-eq v2, v3, :cond_4

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    if-eq v2, v4, :cond_4

    .line 423
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    if-ge v2, v3, :cond_7

    .line 424
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v4, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->moveForward(III)V

    .line 430
    :cond_4
    :goto_1
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    .line 431
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    .end local v1    # "hitRectChild":Landroid/graphics/Rect;
    :goto_2
    monitor-exit p0

    return-void

    .line 411
    :cond_5
    :try_start_1
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    if-gez v2, :cond_3

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395
    .end local v0    # "dropPos":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 415
    .restart local v0    # "dropPos":I
    :cond_6
    :try_start_2
    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    goto :goto_0

    .line 426
    .restart local v1    # "hitRectChild":Landroid/graphics/Rect;
    :cond_7
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iget v4, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->moveBackward(III)V

    goto :goto_1

    .line 433
    .end local v1    # "hitRectChild":Landroid/graphics/Rect;
    :cond_8
    const-string v2, "ANUJ"

    const-string v3, "removed mDraggedScrollTask called "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 435
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    .line 436
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private showCursorView(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "outRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v5, 0x0

    .line 733
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCursorView:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 734
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f040001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 735
    .local v2, "width":I
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f040000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 736
    .local v0, "height":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 737
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, p1, Landroid/graphics/Rect;->top:I

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 738
    iget v3, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v2, 0x4

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 740
    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v3, :cond_0

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 742
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCursorView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 743
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCursorView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 745
    .end local v0    # "height":I
    .end local v1    # "rp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "width":I
    :cond_1
    return-void
.end method


# virtual methods
.method public getEffectAdapter()Lcom/sec/android/app/filtermanager/views/EffectAdapter;
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    return-object v0
.end method

.method public initializeViewsForDrag()V
    .locals 9

    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v7

    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 330
    .local v1, "count":I
    new-array v7, v1, [Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    iput-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    .line 331
    new-array v7, v1, [Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    iput-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    .line 333
    .local v2, "firstVisible":I
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getLastVisiblePosition()I

    move-result v4

    .line 334
    .local v4, "lastVisible":I
    move v3, v2

    .local v3, "i":I
    :goto_0
    if-gt v3, v4, :cond_3

    .line 335
    sub-int v7, v3, v2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 336
    .local v0, "child":Landroid/view/View;
    sub-int v7, v3, v2

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p0, v7}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 337
    .local v6, "prevChild":Landroid/view/View;
    sub-int v7, v3, v2

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p0, v7}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 338
    .local v5, "nextChild":Landroid/view/View;
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    new-instance v8, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    invoke-direct {v8, p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V

    aput-object v8, v7, v3

    .line 339
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iput-object v0, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    .line 340
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 341
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    invoke-virtual {v7}, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->movedinPlace()V

    .line 342
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCurrentRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    iget-object v8, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v8, v8, v3

    aput-object v8, v7, v3

    .line 343
    if-ne v3, v2, :cond_0

    if-eq v3, v4, :cond_0

    .line 344
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 345
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 334
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 346
    :cond_0
    if-eq v3, v2, :cond_1

    if-ne v3, v4, :cond_1

    .line 347
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 348
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 349
    :cond_1
    if-ne v3, v2, :cond_2

    if-ne v3, v4, :cond_2

    .line 350
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 351
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 353
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 354
    iget-object v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mRectInfo:[Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 357
    .end local v0    # "child":Landroid/view/View;
    .end local v5    # "nextChild":Landroid/view/View;
    .end local v6    # "prevChild":Landroid/view/View;
    :cond_3
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    return v0
.end method

.method public isDragAllowed(FFLjava/lang/Object;)Z
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "info"    # Ljava/lang/Object;

    .prologue
    .line 310
    const/4 v0, 0x1

    return v0
.end method

.method public isDropAccepted(FFLjava/lang/Object;)Z
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "info"    # Ljava/lang/Object;

    .prologue
    .line 363
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->onDestroy()V

    .line 199
    return-void
.end method

.method public onDragEnded(FFLjava/lang/Object;)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "info"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 322
    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScroll:I

    .line 323
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->performDragEnd()V

    .line 324
    iput-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    .line 325
    return-void
.end method

.method public declared-synchronized onDragOver(Lcom/sec/android/app/filtermanager/DragSource;FFLjava/lang/Object;)V
    .locals 5
    .param p1, "source"    # Lcom/sec/android/app/filtermanager/DragSource;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "info"    # Ljava/lang/Object;

    .prologue
    const/4 v4, -0x1

    .line 442
    monitor-enter p0

    const/4 v1, 0x0

    cmpl-float v1, p3, v1

    if-lez v1, :cond_0

    .line 443
    :try_start_0
    iput p3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->lastY:F

    .line 445
    :cond_0
    iput p2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mX:F

    .line 446
    iput p3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mY:F

    .line 447
    iget-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mScrollAdjustment:Z

    if-nez v1, :cond_1

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->checkScrolling(FF)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 501
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 451
    :cond_2
    float-to-int v1, p2

    float-to-int v2, p3

    :try_start_1
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildPosition(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    .line 453
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    if-ne v1, v4, :cond_3

    .line 454
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    .line 457
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isScrolling:Z

    if-eqz v1, :cond_8

    .line 458
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    if-eq v1, v2, :cond_4

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->initializeViewsForDrag()V

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    .line 464
    :cond_4
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    if-eq v1, v4, :cond_7

    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    if-eq v1, v4, :cond_7

    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    if-eq v1, v2, :cond_7

    .line 465
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->hideCursorView()V

    .line 466
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 469
    const-string v1, "ANUJ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDragView visible position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildPosition(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 473
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    if-eqz v1, :cond_6

    .line 474
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    .line 476
    :cond_6
    const/16 v1, 0xc8

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    .line 477
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->insert(II)V

    .line 478
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    .line 480
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    if-eqz v1, :cond_7

    .line 482
    const-string v1, "ANUJ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDragView invisible position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildPosition(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 486
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isScrolling:Z

    .line 489
    :cond_8
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 490
    .local v0, "hitRectChild":Landroid/graphics/Rect;
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 491
    invoke-direct {p0, v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->showCursorView(Landroid/graphics/Rect;)V

    .line 492
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    if-eq v1, v4, :cond_1

    .line 495
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    if-le v1, v2, :cond_9

    .line 496
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->moveForward(III)V

    .line 500
    :goto_1
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 442
    .end local v0    # "hitRectChild":Landroid/graphics/Rect;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 498
    .restart local v0    # "hitRectChild":Landroid/graphics/Rect;
    :cond_9
    :try_start_2
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->moveBackward(III)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public onDragStart(FFLjava/lang/Object;)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "info"    # Ljava/lang/Object;

    .prologue
    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 317
    return-void
.end method

.method public onDrop(Lcom/sec/android/app/filtermanager/DragSource;FFLjava/lang/Object;)V
    .locals 5
    .param p1, "source"    # Lcom/sec/android/app/filtermanager/DragSource;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "info"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 505
    float-to-int v1, p2

    float-to-int v2, p3

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildPosition(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    .line 507
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 508
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    .line 511
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->hideCursorView()V

    .line 513
    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDropOnPosition:I

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    if-ne v1, v2, :cond_1

    .line 526
    :goto_0
    return-void

    .line 516
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setEnableItemsInParentView()V

    .line 517
    iget-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z

    if-eqz v1, :cond_2

    .line 518
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeLeft:I

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 520
    :cond_2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 523
    .local v0, "msg":Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->what:I

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public refreshView()V
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->refreshView()V

    .line 730
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 29
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 184
    instance-of v0, p1, Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    if-eqz v0, :cond_0

    .line 185
    check-cast p1, Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .end local p1    # "adapter":Landroid/widget/ListAdapter;
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-super {p0, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mEffectAdapter:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 192
    return-void

    .line 190
    .restart local p1    # "adapter":Landroid/widget/ListAdapter;
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only EffectAdapter is supported as adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setEnableItemsInParentView()V
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/filtermanager/ManageEffectActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/ManageEffectActivity;->setEnableItems()V

    .line 760
    return-void
.end method

.method public startDrag(Landroid/view/View;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 207
    invoke-direct {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildPosition(Landroid/view/View;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    .line 208
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragOverPosition:I

    .line 209
    iget v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragPosition:I

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mPrevDragOverposition:I

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getFirstVisiblePosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mFirstVisiblePos:I

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getChildCount()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mChildcount:I

    .line 212
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mCursorView:Landroid/widget/ImageView;

    .line 214
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 215
    .local v0, "outRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 216
    invoke-direct {p0, v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->showCursorView(Landroid/graphics/Rect;)V

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDragView:Landroid/view/View;

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isDragAllowed(FFLjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mBusy:Z

    if-nez v2, :cond_1

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    if-nez v2, :cond_0

    .line 219
    new-instance v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    invoke-direct {v2, v6}, Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;-><init>(Lcom/sec/android/app/filtermanager/views/EffectGridView$1;)V

    iput-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    .line 221
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    invoke-direct {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getDragShowView(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;->mDragShowView:Landroid/widget/ImageView;

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;->mDragShowView:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;->mDragShowView:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 226
    new-instance v1, Landroid/view/View$DragShadowBuilder;

    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mViewHolder:Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/filtermanager/views/EffectGridView$ViewHolder;->mDragShowView:Landroid/widget/ImageView;

    invoke-direct {v1, v2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 227
    .local v1, "shadowView":Landroid/view/View$DragShadowBuilder;
    const/4 v2, 0x0

    invoke-virtual {p1, v6, v1, p1, v2}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 229
    .end local v1    # "shadowView":Landroid/view/View$DragShadowBuilder;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView;->mDraggedScrollTask:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 232
    const/4 v2, 0x1

    return v2
.end method
