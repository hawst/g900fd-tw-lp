.class Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;
.super Ljava/lang/Object;
.source "EffectGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MovedInfo"
.end annotation


# instance fields
.field public mInitialRect:Landroid/graphics/Rect;

.field private mMovedBackWard:Z

.field private mMovedForward:Z

.field public mNextRect:Landroid/graphics/Rect;

.field public mPreviousIntialRect:Landroid/graphics/Rect;

.field public mView:Landroid/view/View;

.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 540
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mInitialRect:Landroid/graphics/Rect;

    .line 542
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mNextRect:Landroid/graphics/Rect;

    .line 543
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mPreviousIntialRect:Landroid/graphics/Rect;

    .line 544
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedForward:Z

    .line 545
    iput-boolean v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedBackWard:Z

    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mView:Landroid/view/View;

    .line 548
    return-void
.end method


# virtual methods
.method public isMoved()Z
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedForward:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedBackWard:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovedBackword()Z
    .locals 1

    .prologue
    .line 567
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedBackWard:Z

    return v0
.end method

.method public isMovedForward()Z
    .locals 1

    .prologue
    .line 563
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedForward:Z

    return v0
.end method

.method public movedBackWard()V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedForward:Z

    .line 555
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedBackWard:Z

    .line 556
    return-void
.end method

.method public movedForward()V
    .locals 1

    .prologue
    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedForward:Z

    .line 551
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedBackWard:Z

    .line 552
    return-void
.end method

.method public movedinPlace()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 558
    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedForward:Z

    .line 559
    iput-boolean v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$MovedInfo;->mMovedBackWard:Z

    .line 560
    return-void
.end method
