.class Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;
.super Ljava/lang/Object;
.source "EffectGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransformAnimation"
.end annotation


# instance fields
.field public animation:Landroid/view/animation/TranslateAnimation;

.field private mAnimationView:Landroid/view/View;

.field mDuration:I

.field mFromX:I

.field mFromY:I

.field mOffset:I

.field mToX:I

.field mToY:I

.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectGridView;Landroid/view/View;IIIIII)V
    .locals 10
    .param p2, "v"    # Landroid/view/View;
    .param p3, "x1"    # I
    .param p4, "y1"    # I
    .param p5, "x2"    # I
    .param p6, "y2"    # I
    .param p7, "duration"    # I
    .param p8, "offset"    # I

    .prologue
    .line 700
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 693
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mFromX:I

    .line 694
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mToX:I

    .line 695
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mFromY:I

    .line 696
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mToY:I

    .line 697
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mDuration:I

    .line 698
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mOffset:I

    .line 701
    iput-object p2, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mAnimationView:Landroid/view/View;

    .line 702
    iput p3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mFromX:I

    .line 703
    iput p4, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mFromY:I

    .line 704
    iput p5, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mToX:I

    .line 705
    move/from16 v0, p6

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mToY:I

    .line 706
    move/from16 v0, p7

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mDuration:I

    .line 707
    move/from16 v0, p8

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mOffset:I

    .line 708
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mFromX:I

    int-to-float v3, v3

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mToX:I

    int-to-float v5, v5

    const/4 v6, 0x0

    iget v7, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mFromY:I

    int-to-float v7, v7

    const/4 v8, 0x0

    iget v9, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mToY:I

    int-to-float v9, v9

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->animation:Landroid/view/animation/TranslateAnimation;

    .line 711
    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->startAnimation()V

    return-void
.end method

.method private startAnimation()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->animation:Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->animation:Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mOffset:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setStartOffset(J)V

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->animation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # setter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimating:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1102(Lcom/sec/android/app/filtermanager/views/EffectGridView;Z)Z

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeHandler:Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1300(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 720
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$400(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->this$0:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectGridView;->mAnimationTimeHandler:Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->access$1300(Lcom/sec/android/app/filtermanager/views/EffectGridView;)Lcom/sec/android/app/filtermanager/views/EffectGridView$AnimationTimeHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 721
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->mAnimationView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectGridView$TransformAnimation;->animation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 722
    return-void
.end method
