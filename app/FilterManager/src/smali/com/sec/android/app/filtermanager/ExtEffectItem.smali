.class public Lcom/sec/android/app/filtermanager/ExtEffectItem;
.super Ljava/lang/Object;
.source "ExtEffectItem.java"


# static fields
.field public static final EXT_RES_PATH:Ljava/lang/String; = "/data/DownFilters/Res/"

.field public static final PRE_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.filter"

.field public static final PRE_RES_PATH:Ljava/lang/String; = "/system/cameradata/preloadfilters/Res/"


# instance fields
.field public final mId:I

.field private mIsHidden:Z

.field public final mName:Ljava/lang/String;

.field public final mResourcePathName:Ljava/lang/String;

.field public final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "filterName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;
    .param p6, "hidden"    # Z

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mId:I

    .line 26
    iput-object p4, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mTitle:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mName:Ljava/lang/String;

    .line 28
    const-string v0, "com.sec.android.filter"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/system/cameradata/preloadfilters/Res/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".so"

    invoke-virtual {p2, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mResourcePathName:Ljava/lang/String;

    .line 33
    :goto_0
    iput-boolean p6, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mIsHidden:Z

    .line 34
    return-void

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/DownFilters/Res/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".so"

    invoke-virtual {p2, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mResourcePathName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public isHidden()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mIsHidden:Z

    return v0
.end method

.method public setHidden(Z)V
    .locals 0
    .param p1, "hidden"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mIsHidden:Z

    return-void
.end method
