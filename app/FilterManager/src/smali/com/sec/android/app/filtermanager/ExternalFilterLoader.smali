.class public Lcom/sec/android/app/filtermanager/ExternalFilterLoader;
.super Landroid/app/Fragment;
.source "ExternalFilterLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;,
        Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;
    }
.end annotation


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.filterprovider/filters"

.field private static final BASE_URI:Landroid/net/Uri;

.field private static final DELETED:Ljava/lang/String; = "deleted"

.field private static final EFFECT_NAME:Ljava/lang/String; = "name"

.field private static final FILE_NAME:Ljava/lang/String; = "filename"

.field private static final FILTER_AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.filterprovider/filters/include_deleted"

.field private static final FILTER_PROJECTION:[Ljava/lang/String;

.field private static final FILTER_URI:Landroid/net/Uri;

.field private static final INDEX_DELETED:I = 0x6

.field private static final INDEX_EFFECT_NAME:I = 0x1

.field private static final INDEX_FILE_NAME:I = 0x2

.field private static final INDEX_ID:I = 0x0

.field private static final INDEX_ORDER:I = 0x7

.field private static final INDEX_PACKAGE_NAME:I = 0x5

.field private static final INDEX_TITLE:I = 0x4

.field private static final INDEX_VERSION:I = 0x3

.field private static final ORDER:Ljava/lang/String; = "filter_order"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field private static final SCREEN_MAX_ITEMS:I = 0xf

.field private static final TAG:Ljava/lang/String;

.field private static final TITLE:Ljava/lang/String; = "title"

.field private static final TITLE_AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.filterprovider/titles"

.field private static final TITLE_URI:Landroid/net/Uri;

.field private static final UPDATE_REPORT_INTERVAL:I = 0x5

.field private static final VERSION:Ljava/lang/String; = "version"

.field private static final _ID:Ljava/lang/String; = "_ID"


# instance fields
.field private mExternalEffects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/filtermanager/ExtEffectItem;",
            ">;"
        }
    .end annotation
.end field

.field private mExternalEffectsOrder:Landroid/util/SparseIntArray;

.field private mFilterDBObserver:Landroid/database/ContentObserver;

.field private mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->TAG:Ljava/lang/String;

    .line 28
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    .line 32
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters/include_deleted"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    .line 36
    const-string v0, "content://com.samsung.android.provider.filterprovider/titles"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->TITLE_URI:Landroid/net/Uri;

    .line 82
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "filename"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "version"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "package_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "filter_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    .line 85
    new-instance v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$1;-><init>(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mFilterDBObserver:Landroid/database/ContentObserver;

    .line 308
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)Landroid/util/SparseIntArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    return-object v0
.end method

.method static synthetic access$200()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    .line 417
    return-void
.end method

.method public getAlignedPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    return v0
.end method

.method public getBaseUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 475
    sget-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getExternalEffectAt(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/filtermanager/ExtEffectItem;

    return-object v0
.end method

.method public getExternalEffectCount()I
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFilterProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 487
    sget-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getFilterUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitleUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 479
    sget-object v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->TITLE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public insert(II)V
    .locals 2
    .param p1, "dropOnPosition"    # I
    .param p2, "dragPosition"    # I

    .prologue
    .line 456
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/filtermanager/ExtEffectItem;

    .line 458
    .local v0, "temp":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 463
    :goto_0
    return-void

    .line 461
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public loadExternalFilters(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 409
    new-instance v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;

    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v2, p0, p1, v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;-><init>(Lcom/sec/android/app/filtermanager/ExternalFilterLoader;Landroid/content/Context;Z)V

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$FilterLoadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 410
    return-void

    :cond_0
    move v0, v1

    .line 409
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->setRetainInstance(Z)V

    .line 102
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    .line 109
    return-void
.end method

.method public reorder(Landroid/content/Context;II)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "firstIndex"    # I
    .param p3, "secondIndex"    # I

    .prologue
    .line 118
    move/from16 v0, p2

    move/from16 v1, p3

    if-eq v0, v1, :cond_0

    if-ltz p2, :cond_0

    if-gez p3, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    const/4 v10, 0x0

    .line 122
    .local v10, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 125
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v8, 0x0

    .line 128
    .local v8, "FILTER_ORDER":I
    :try_start_0
    sget-object v3, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "filter_order"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 130
    if-eqz v10, :cond_c

    .line 131
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 133
    sub-int v3, p2, p3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v15

    .line 134
    .local v15, "n":I
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 135
    .local v19, "temp":Landroid/content/ContentValues;
    move/from16 v0, p2

    move/from16 v1, p3

    if-ge v0, v1, :cond_2

    const/4 v13, 0x1

    .line 136
    .local v13, "flag":Z
    :goto_1
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 138
    .local v17, "startOrder":I
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 139
    .local v9, "count":I
    invoke-interface {v10}, Landroid/database/Cursor;->moveToLast()Z

    .line 140
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 142
    .local v12, "endOrder":I
    sub-int v3, v12, v17

    if-lt v3, v9, :cond_8

    .line 143
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 145
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move/from16 v0, p3

    if-eq v3, v0, :cond_3

    .line 146
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 206
    .end local v9    # "count":I
    .end local v12    # "endOrder":I
    .end local v13    # "flag":Z
    .end local v15    # "n":I
    .end local v17    # "startOrder":I
    .end local v19    # "temp":Landroid/content/ContentValues;
    :catch_0
    move-exception v11

    .line 207
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    if-eqz v10, :cond_0

    .line 210
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 135
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v15    # "n":I
    .restart local v19    # "temp":Landroid/content/ContentValues;
    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    .line 149
    .restart local v9    # "count":I
    .restart local v12    # "endOrder":I
    .restart local v13    # "flag":Z
    .restart local v17    # "startOrder":I
    :cond_3
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->getPosition()I

    move-result v18

    .line 152
    .local v18, "startPosition":I
    const/4 v15, 0x0

    :goto_3
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move/from16 v0, p2

    if-eq v3, v0, :cond_6

    .line 153
    if-eqz v13, :cond_4

    .line 154
    invoke-interface {v10}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 152
    :goto_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 156
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 209
    .end local v9    # "count":I
    .end local v12    # "endOrder":I
    .end local v13    # "flag":Z
    .end local v15    # "n":I
    .end local v17    # "startOrder":I
    .end local v18    # "startPosition":I
    .end local v19    # "temp":Landroid/content/ContentValues;
    :catchall_0
    move-exception v3

    if-eqz v10, :cond_5

    .line 210
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3

    .line 160
    .restart local v9    # "count":I
    .restart local v12    # "endOrder":I
    .restart local v13    # "flag":Z
    .restart local v15    # "n":I
    .restart local v17    # "startOrder":I
    .restart local v18    # "startPosition":I
    .restart local v19    # "temp":Landroid/content/ContentValues;
    :cond_6
    :try_start_3
    move/from16 v0, v18

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 165
    .end local v18    # "startPosition":I
    :goto_5
    const-string v3, "filter_order"

    const-string v4, "-1"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/16 v21, -0x1

    .line 169
    .local v21, "tempOrder":I
    if-lez v9, :cond_a

    .line 170
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_6
    if-gt v14, v15, :cond_a

    .line 172
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const v5, 0x7fffffff

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v20

    .line 174
    .local v20, "tempItemId":I
    const v3, 0x7fffffff

    move/from16 v0, v20

    if-eq v0, v3, :cond_7

    .line 175
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 176
    .local v16, "order":I
    sget-object v3, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filter_order="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 178
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->delete(I)V

    .line 179
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    move/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 181
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->clear()V

    .line 182
    const-string v3, "filter_order"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 183
    move/from16 v21, v16

    .line 186
    .end local v16    # "order":I
    :cond_7
    if-eqz v13, :cond_9

    .line 187
    invoke-interface {v10}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 170
    :goto_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 162
    .end local v14    # "i":I
    .end local v20    # "tempItemId":I
    .end local v21    # "tempOrder":I
    :cond_8
    sub-int v3, p3, v17

    invoke-interface {v10, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_5

    .line 189
    .restart local v14    # "i":I
    .restart local v20    # "tempItemId":I
    .restart local v21    # "tempOrder":I
    :cond_9
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_7

    .line 194
    .end local v14    # "i":I
    .end local v20    # "tempItemId":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    const/4 v4, -0x1

    const v5, 0x7fffffff

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v20

    .line 196
    .restart local v20    # "tempItemId":I
    const v3, 0x7fffffff

    move/from16 v0, v20

    if-eq v3, v0, :cond_b

    .line 197
    sget-object v3, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    const-string v4, "filter_order=-1"

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/util/SparseIntArray;->delete(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    move/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 203
    :cond_b
    sget-object v3, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mFilterDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 204
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->clear()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 209
    .end local v9    # "count":I
    .end local v12    # "endOrder":I
    .end local v13    # "flag":Z
    .end local v15    # "n":I
    .end local v17    # "startOrder":I
    .end local v19    # "temp":Landroid/content/ContentValues;
    .end local v20    # "tempItemId":I
    .end local v21    # "tempOrder":I
    :cond_c
    if-eqz v10, :cond_0

    .line 210
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method setFilters(Ljava/util/ArrayList;Landroid/util/SparseIntArray;)V
    .locals 1
    .param p2, "itemsOrder"    # Landroid/util/SparseIntArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/filtermanager/ExtEffectItem;",
            ">;",
            "Landroid/util/SparseIntArray;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/filtermanager/ExtEffectItem;>;"
    monitor-enter p0

    .line 296
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffects:Ljava/util/ArrayList;

    .line 297
    iput-object p2, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mExternalEffectsOrder:Landroid/util/SparseIntArray;

    .line 298
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    invoke-interface {v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;->viewsRefreshed()V

    .line 303
    :cond_0
    return-void

    .line 298
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setOwner(Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;)V
    .locals 0
    .param p1, "owner"    # Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    .prologue
    .line 471
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->mOwner:Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;

    .line 472
    return-void
.end method

.method public updateHide(Landroid/content/Context;ZLcom/sec/android/app/filtermanager/ExtEffectItem;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hidden"    # Z
    .param p3, "effectItem"    # Lcom/sec/android/app/filtermanager/ExtEffectItem;

    .prologue
    .line 222
    const/4 v8, 0x0

    .line 224
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 226
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "_ID"

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const-string v4, "deleted"

    aput-object v4, v3, v2

    .line 227
    .local v3, "filter_projection":[Ljava/lang/String;
    const/4 v7, 0x1

    .line 230
    .local v7, "FILTER_HIDDEN":I
    :try_start_0
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 232
    if-eqz v8, :cond_4

    .line 234
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 236
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v10

    .line 237
    .local v10, "hiddenColumnName":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hiddenColumnName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 239
    .local v14, "temp":Landroid/content/ContentValues;
    move-object/from16 v0, p3

    iget v12, v0, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mId:I

    .line 240
    .local v12, "id":I
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    if-eqz p2, :cond_6

    .line 243
    const-string v11, "0"

    .line 246
    .local v11, "hide":Ljava/lang/String;
    :goto_0
    invoke-virtual {v14, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v2, "ANUJ"

    const-string v4, "***************Before Update*****************"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 251
    .local v13, "print":Landroid/database/Cursor;
    if-eqz v13, :cond_1

    .line 252
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 254
    :cond_0
    const-string v2, "ANUJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", deleted = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 258
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 263
    :cond_1
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v14, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 264
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->BASE_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 267
    const-string v2, "ANUJ"

    const-string v4, "***************After Update*****************"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    sget-object v2, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->FILTER_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 269
    if-eqz v13, :cond_3

    .line 270
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 272
    :cond_2
    const-string v2, "ANUJ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", deleted = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 276
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_3
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    .end local v10    # "hiddenColumnName":Ljava/lang/String;
    .end local v11    # "hide":Ljava/lang/String;
    .end local v12    # "id":I
    .end local v13    # "print":Landroid/database/Cursor;
    .end local v14    # "temp":Landroid/content/ContentValues;
    :cond_4
    if-eqz v8, :cond_5

    .line 288
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 292
    :cond_5
    :goto_1
    return-void

    .line 245
    .restart local v10    # "hiddenColumnName":Ljava/lang/String;
    .restart local v12    # "id":I
    .restart local v14    # "temp":Landroid/content/ContentValues;
    :cond_6
    :try_start_1
    const-string v11, "1"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v11    # "hide":Ljava/lang/String;
    goto/16 :goto_0

    .line 283
    .end local v10    # "hiddenColumnName":Ljava/lang/String;
    .end local v11    # "hide":Ljava/lang/String;
    .end local v12    # "id":I
    .end local v14    # "temp":Landroid/content/ContentValues;
    :catch_0
    move-exception v9

    .line 284
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 287
    if-eqz v8, :cond_5

    .line 288
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 287
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_7

    .line 288
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2
.end method
