.class Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;
.super Landroid/os/Handler;
.source "EffectAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 78
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    return-void

    .line 80
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mParentEffectGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->access$000(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Lcom/sec/android/app/filtermanager/views/EffectGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    # invokes: Lcom/sec/android/app/filtermanager/views/EffectAdapter;->populateViews()V
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->access$100(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V

    goto :goto_0

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->access$200(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
