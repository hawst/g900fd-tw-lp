.class Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;
.super Ljava/lang/Object;
.source "EffectAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/filtermanager/views/EffectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckBoxHideListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;
    .param p2, "x1"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 294
    instance-of v1, p1, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    .line 295
    const v1, 0x7f07000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    .line 300
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    # getter for: Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mParentEffectGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;
    invoke-static {v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->access$000(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Lcom/sec/android/app/filtermanager/views/EffectGridView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->setEnableItemsInParentView()V

    .line 301
    return-void

    .line 296
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    instance-of v1, p1, Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;->this$0:Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;

    check-cast v1, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;

    iget v1, v1, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mPosition:I

    invoke-virtual {v2, v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getEffectInfo(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;

    move-result-object v0

    .line 298
    .local v0, "effectInfo":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/sec/android/app/filtermanager/ExtEffectItem;->setHidden(Z)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
