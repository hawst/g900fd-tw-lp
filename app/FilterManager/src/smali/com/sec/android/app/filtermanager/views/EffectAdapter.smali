.class public Lcom/sec/android/app/filtermanager/views/EffectAdapter;
.super Landroid/widget/BaseAdapter;
.source "EffectAdapter.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/android/app/filtermanager/ExternalFilterLoader$DBObserverListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectTouchListener;,
        Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;,
        Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;,
        Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ACTION_DATABASE_REORDER:I = 0x5

.field private static final ACTION_REFRESH_REQUESTED:I = 0x2

.field private static final IMAGES_CACHE_SIZE_MB:I = 0xa

.field private static final PACKAGE_UPDATE_OFFSET:I = 0x3e8

.field private static final SCALE_ANIMATION_DURATION:I = 0x32

.field private static final SCALE_ANIMATION_PIVOT_X:F = 0.5f

.field private static final SCALE_ANIMATION_PIVOT_Y:F = 0.5f

.field private static final SCALE_DOWN_ANIMATION_OFFSET:F = 0.95f

.field private static final TAG:Ljava/lang/String; = "EffectAdapter"


# instance fields
.field private mCheckBoxHideListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;

.field private mContext:Landroid/content/Context;

.field private final mEffectHeight:I

.field private mEffectLongClickListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;

.field private mEffectScaleDownAnimation:Landroid/view/animation/ScaleAnimation;

.field private mEffectScaleUpAnimation:Landroid/view/animation/ScaleAnimation;

.field private mEffectTouchListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectTouchListener;

.field private final mEffectWidth:I

.field private mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

.field private mFilterImagesCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMainHandler:Landroid/os/Handler;

.field private mParentEffectGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/filtermanager/views/EffectGridView;Lcom/sec/android/app/filtermanager/ExternalFilterLoader;)V
    .locals 9
    .param p1, "parentActivity"    # Landroid/content/Context;
    .param p2, "parentEffectGridView"    # Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .param p3, "loader"    # Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .prologue
    .line 109
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 68
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;

    const/high16 v1, 0xa00000

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;I)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mFilterImagesCache:Landroid/util/LruCache;

    .line 75
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$2;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mContext:Landroid/content/Context;

    .line 111
    iput-object p2, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mParentEffectGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 113
    iput-object p3, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    .line 114
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectLongClickListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;

    .line 115
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mCheckBoxHideListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;

    .line 116
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectTouchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectTouchListener;-><init>(Lcom/sec/android/app/filtermanager/views/EffectAdapter;Lcom/sec/android/app/filtermanager/views/EffectAdapter$1;)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectTouchListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectTouchListener;

    .line 117
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f733333    # 0.95f

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3f733333    # 0.95f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleDownAnimation:Landroid/view/animation/ScaleAnimation;

    .line 119
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const v1, 0x3f733333    # 0.95f

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3f733333    # 0.95f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleUpAnimation:Landroid/view/animation/ScaleAnimation;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleDownAnimation:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleUpAnimation:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleDownAnimation:Landroid/view/animation/ScaleAnimation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleUpAnimation:Landroid/view/animation/ScaleAnimation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectWidth:I

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectHeight:I

    .line 128
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Lcom/sec/android/app/filtermanager/views/EffectGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mParentEffectGridView:Lcom/sec/android/app/filtermanager/views/EffectGridView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->populateViews()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Landroid/view/animation/ScaleAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleDownAnimation:Landroid/view/animation/ScaleAnimation;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/filtermanager/views/EffectAdapter;)Landroid/view/animation/ScaleAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/filtermanager/views/EffectAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectScaleUpAnimation:Landroid/view/animation/ScaleAnimation;

    return-object v0
.end method

.method private getImage(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mFilterImagesCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 240
    .local v0, "item":Landroid/graphics/drawable/BitmapDrawable;
    if-nez v0, :cond_0

    .line 241
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "item":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    .line 243
    .restart local v0    # "item":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mFilterImagesCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    :cond_0
    return-object v0
.end method

.method private populateViews()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->loadExternalFilters(Landroid/content/Context;)V

    .line 94
    return-void
.end method


# virtual methods
.method public getAlignedPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getAlignedPosition(I)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getExternalEffectCount()I

    move-result v0

    return v0
.end method

.method public getEffectInfo(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->getExternalEffectAt(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getEffectInfo(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 161
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 189
    if-nez p2, :cond_0

    .line 190
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030002

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 191
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectLongClickListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectLongClickListener;

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 192
    new-instance v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;-><init>()V

    .line 196
    .local v0, "holder":Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;
    const v3, 0x7f070009

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    .line 197
    const v3, 0x7f07000a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mBox:Landroid/widget/CheckBox;

    .line 200
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 201
    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mBox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mCheckBoxHideListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mCheckBoxHideListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$CheckBoxHideListener;

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectTouchListener:Lcom/sec/android/app/filtermanager/views/EffectAdapter$EffectTouchListener;

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 209
    :goto_0
    invoke-virtual {p2, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 210
    iput p1, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mPosition:I

    .line 212
    invoke-virtual {p0, p1}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getEffectInfo(I)Lcom/sec/android/app/filtermanager/ExtEffectItem;

    move-result-object v2

    .line 214
    .local v2, "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v3, v2, Lcom/sec/android/app/filtermanager/ExtEffectItem;->mResourcePathName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->getImage(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 218
    .local v1, "image":Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_1

    .line 219
    const-string v3, "EffectAdapter"

    const-string v4, " image is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 p2, 0x0

    .line 228
    .end local p2    # "convertView":Landroid/view/View;
    :goto_1
    return-object p2

    .line 205
    .end local v0    # "holder":Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;
    .end local v1    # "image":Landroid/graphics/drawable/Drawable;
    .end local v2    # "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 206
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;
    goto :goto_0

    .line 223
    .restart local v1    # "image":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "infoItem":Lcom/sec/android/app/filtermanager/ExtEffectItem;
    :cond_1
    iget v3, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectWidth:I

    iget v5, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mEffectHeight:I

    invoke-virtual {v1, v4, v4, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 224
    iget-object v3, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 226
    iget-object v5, v0, Lcom/sec/android/app/filtermanager/views/EffectAdapter$ViewHolder;->mBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Lcom/sec/android/app/filtermanager/ExtEffectItem;->isHidden()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v5, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method public insert(II)V
    .locals 1
    .param p1, "dropOnPosition"    # I
    .param p2, "dragPosition"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->insert(II)V

    .line 172
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mExternalFilterLoader:Lcom/sec/android/app/filtermanager/ExternalFilterLoader;

    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/ExternalFilterLoader;->clear()V

    .line 135
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 340
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    instance-of v0, p2, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 341
    const v0, 0x7f07000a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 343
    :cond_0
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 264
    const/16 v3, 0x42

    if-eq p2, v3, :cond_0

    const/16 v3, 0xa0

    if-ne p2, v3, :cond_3

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v1, :cond_3

    .line 265
    instance-of v3, p1, Lcom/sec/android/app/filtermanager/views/EffectGridView;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 266
    check-cast v0, Lcom/sec/android/app/filtermanager/views/EffectGridView;

    .line 267
    .local v0, "gridView":Lcom/sec/android/app/filtermanager/views/EffectGridView;
    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getSelectedView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 268
    invoke-virtual {v0}, Lcom/sec/android/app/filtermanager/views/EffectGridView;->getSelectedView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->performClick()Z

    .line 276
    .end local v0    # "gridView":Lcom/sec/android/app/filtermanager/views/EffectGridView;
    :cond_1
    :goto_0
    return v1

    .line 272
    :cond_2
    const-string v1, "EffectAdapter"

    const-string v3, "view is not EffectGridView type"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 273
    goto :goto_0

    :cond_3
    move v1, v2

    .line 276
    goto :goto_0
.end method

.method public refreshView()V
    .locals 3

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 254
    return-void
.end method

.method reorderDB(II)V
    .locals 3
    .param p1, "dropOnPosition"    # I
    .param p2, "dragPosition"    # I

    .prologue
    .line 144
    const-string v0, "EffectAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " reorderDB dropOnPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  dragPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {v1, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 147
    return-void
.end method

.method public viewsRefreshed()V
    .locals 0

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/filtermanager/views/EffectAdapter;->notifyDataSetChanged()V

    .line 260
    return-void
.end method
