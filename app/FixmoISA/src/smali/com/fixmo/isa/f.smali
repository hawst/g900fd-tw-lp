.class final Lcom/fixmo/isa/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:I

.field private d:Lcom/a/a/a/a;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/fixmo/isa/f;

    invoke-static {v0}, Lcom/fixmo/isa/g;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/fixmo/isa/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/fixmo/isa/f;->b:I

    iput v1, p0, Lcom/fixmo/isa/f;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;

    iput v1, p0, Lcom/fixmo/isa/f;->e:I

    iput v1, p0, Lcom/fixmo/isa/f;->f:I

    iput v1, p0, Lcom/fixmo/isa/f;->g:I

    return-void
.end method

.method private declared-synchronized b()V
    .locals 8

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v2, p0, Lcom/fixmo/isa/f;->e:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_2

    sget-object v0, Lcom/fixmo/isa/f;->a:Ljava/lang/String;

    const-string v1, "Reporting platform scan progress: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/fixmo/isa/f;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;

    iget v1, p0, Lcom/fixmo/isa/f;->b:I

    iget v2, p0, Lcom/fixmo/isa/f;->f:I

    invoke-interface {v0, v1, v2}, Lcom/a/a/a/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget v2, p0, Lcom/fixmo/isa/f;->e:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_3

    sget-object v0, Lcom/fixmo/isa/f;->a:Ljava/lang/String;

    const-string v1, "Reporting package scan progress: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/fixmo/isa/f;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;

    iget v1, p0, Lcom/fixmo/isa/f;->c:I

    iget v2, p0, Lcom/fixmo/isa/f;->f:I

    invoke-interface {v0, v1, v2}, Lcom/a/a/a/a;->a(II)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/fixmo/isa/f;->e:I

    const/16 v3, 0xd

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/fixmo/isa/f;->g:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    mul-double/2addr v2, v0

    cmpl-double v4, v2, v0

    if-ltz v4, :cond_4

    :goto_1
    iget v2, p0, Lcom/fixmo/isa/f;->b:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v0

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/fixmo/isa/f;->c:I

    int-to-double v4, v4

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    sget-object v2, Lcom/fixmo/isa/f;->a:Ljava/lang/String;

    const-string v3, "Platform scan progress: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/fixmo/isa/f;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/fixmo/isa/f;->a:Ljava/lang/String;

    const-string v3, "Package scan progress: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/fixmo/isa/f;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/fixmo/isa/f;->a:Ljava/lang/String;

    const-string v3, "Reporting total scan progress: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    double-to-int v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;

    double-to-int v0, v0

    iget v1, p0, Lcom/fixmo/isa/f;->f:I

    invoke-interface {v2, v0, v1}, Lcom/a/a/a/a;->a(II)V

    goto/16 :goto_0

    :cond_4
    mul-double v0, v2, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v6

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/fixmo/isa/f;->b:I

    iput v1, p0, Lcom/fixmo/isa/f;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;

    iput v1, p0, Lcom/fixmo/isa/f;->g:I

    return-void
.end method

.method public declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/fixmo/isa/f;->b:I

    invoke-direct {p0}, Lcom/fixmo/isa/f;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(II)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/fixmo/isa/f;->g:I

    if-nez v0, :cond_0

    iput p2, p0, Lcom/fixmo/isa/f;->g:I

    :cond_0
    int-to-double v0, p1

    int-to-double v2, p2

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/fixmo/isa/f;->c:I

    invoke-direct {p0}, Lcom/fixmo/isa/f;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/a/a/a/a;II)V
    .locals 1

    iput-object p1, p0, Lcom/fixmo/isa/f;->d:Lcom/a/a/a/a;

    iput p2, p0, Lcom/fixmo/isa/f;->e:I

    iput p3, p0, Lcom/fixmo/isa/f;->f:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/fixmo/isa/f;->g:I

    return-void
.end method
