.class final Lcom/fixmo/isa/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/fixmo/isa/FixmoISAService;


# direct methods
.method private constructor <init>(Lcom/fixmo/isa/FixmoISAService;)V
    .locals 0

    iput-object p1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/fixmo/isa/FixmoISAService;Lcom/fixmo/isa/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/fixmo/isa/e;-><init>(Lcom/fixmo/isa/FixmoISAService;)V

    return-void
.end method

.method private a()V
    .locals 10

    const/4 v1, 0x0

    const/4 v5, 0x1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Processing clearBaseline request."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x4

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    const/16 v4, 0xc

    invoke-virtual {v0, v2, v4, v3}, Lcom/fixmo/isa/f;->a(Lcom/a/a/a/a;II)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    and-int v2, v5, v0

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Trying to delete platform baseline, but it does not already exist."

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-virtual {v0}, Lcom/fixmo/isa/FixmoISAService;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->c(Lcom/fixmo/isa/FixmoISAService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v4, 0x5040

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v6, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v6, :cond_8

    invoke-static {v0}, Lcom/fixmo/isa/g;->a(Landroid/content/pm/PackageInfo;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v6}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v6

    iget-object v7, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/a/a/a/a;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Trying to delete baseline for package %s, but it does not already exist."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v6

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v6, v0, v4}, Lcom/fixmo/isa/f;->a(II)V

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->j()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "error encountered while clearing 3rd party baseline"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/fixmo/isa/f;->a(II)V

    :cond_4
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->f()Z

    move-result v0

    and-int/2addr v0, v2

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/a/a/a/a;->a(I)V

    :goto_3
    return-void

    :cond_5
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to clear baseline"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to rollback before reporting error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-direct {p0, v3}, Lcom/fixmo/isa/e;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "clearBaselineFailed: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :try_start_1
    invoke-direct {p0, v3}, Lcom/fixmo/isa/e;->a(I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for an exception: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(I)V
    .locals 4

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/a/a/a/a;->a(IILjava/lang/String;I)V

    return-void
.end method

.method private a(ILjava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/a/a/a/a;->a(IILjava/lang/String;I)V

    return-void
.end method

.method private a(ILjava/util/List;)V
    .locals 12

    const/16 v11, 0xd

    const/16 v3, 0xc

    const/4 v7, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing performScanNow request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x3

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v6}, Lcom/fixmo/isa/f;->a(Lcom/a/a/a/a;II)V

    if-eq p1, v3, :cond_0

    if-ne p1, v11, :cond_10

    :cond_0
    if-ne p1, v3, :cond_7

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_5

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v7

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v8, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v8}, Lcom/fixmo/isa/FixmoISAService;->d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;

    move-result-object v8

    iget-object v9, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v9}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v9

    invoke-virtual {v8, v0, v9}, Lcom/fixmo/isa/b;->a(Ljava/lang/String;Lcom/a/a/a/a;)[B

    move-result-object v8

    if-nez v8, :cond_1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Package scan for %s returned null result."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, v0, v6}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    :goto_1
    return-void

    :cond_1
    invoke-interface {v1, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v8

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v8, v0, v4}, Lcom/fixmo/isa/f;->a(II)V

    move v3, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/a/a/a/a;->a(Ljava/lang/String;)[B

    move-result-object v4

    if-nez v4, :cond_4

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Baseline for package %s does not exist."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v3, v9, v10

    invoke-static {v2, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_f

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fingerprints for package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " do not match."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    const/4 v1, 0x1

    move v2, p1

    invoke-interface/range {v0 .. v6}, Lcom/a/a/a/a;->a(IILjava/lang/String;[B[BI)V

    move v0, v7

    :goto_4
    move v1, v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-virtual {v0}, Lcom/fixmo/isa/FixmoISAService;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Lcom/fixmo/isa/f;->a(II)V

    move-object v0, v1

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;

    move-result-object v0

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v1

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/fixmo/isa/b;->a(Lcom/fixmo/isa/f;Lcom/a/a/a/a;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Third party packages scan returned null result..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v6}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "performScanNow failed: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, p1, v0, v6}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for exception. "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_7
    :try_start_2
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;

    move-result-object v0

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v1

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/fixmo/isa/b;->a(Lcom/fixmo/isa/f;Lcom/a/a/a/a;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Third party packages scan returned null result."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v6}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    goto/16 :goto_1

    :cond_8
    move v0, v1

    :goto_5
    const/16 v1, 0xb

    if-eq p1, v1, :cond_9

    if-ne p1, v11, :cond_e

    :cond_9
    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/a;->d()[B

    move-result-object v4

    if-nez v4, :cond_a

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-virtual {v1}, Lcom/fixmo/isa/FixmoISAService;->a()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Platform baseline does not exist."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/a;->a()[B

    move-result-object v5

    if-nez v5, :cond_b

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Platform scan returned null result."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v6}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    :cond_b
    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v3, 0x0

    move v2, p1

    invoke-interface/range {v0 .. v6}, Lcom/a/a/a/a;->a(IILjava/lang/String;[B[BI)V

    :goto_6
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    if-nez v7, :cond_c

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Device did not pass the integrity check."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/a/a/a/a;->a(I)V

    goto/16 :goto_1

    :cond_d
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Platform baseline does not exist."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v6}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    :cond_e
    move v7, v0

    goto :goto_6

    :cond_f
    move v0, v1

    goto/16 :goto_4

    :cond_10
    move v0, v2

    goto/16 :goto_5
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    const/4 v3, 0x1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing putPackageToBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x5

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;

    move-result-object v0

    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/fixmo/isa/b;->a(Ljava/lang/String;Lcom/a/a/a/a;)[B

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Package scan returned null result."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/a;->g()Z

    move-result v2

    and-int/2addr v2, v3

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/a/a/a/a;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Baseline for package %s already exists. Will replace with the new one."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-interface {v3, p1, v0}, Lcom/a/a/a/a;->a(Ljava/lang/String;[B)Z

    move-result v0

    and-int/2addr v0, v2

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/a;->f()Z

    move-result v2

    and-int/2addr v0, v2

    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/a/a/a/a;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "putPackageToBaseline failed: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v0, v2, v1}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for exception. "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Failed to put package %s to baseline."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Failed to rollback before reporting error."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method private a(Z)V
    .locals 10

    const/4 v6, 0x1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing establishBaselineScan request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    const/16 v4, 0xd

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v3}, Lcom/fixmo/isa/f;->a(Lcom/a/a/a/a;II)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;

    move-result-object v0

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v1

    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/fixmo/isa/b;->a(Lcom/fixmo/isa/f;Lcom/a/a/a/a;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Third party package scan returned null result."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/a/a/a;->a()[B

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Platform scan returned null result."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "establishBaselineScan failed: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for an exception: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fixmo/isa/f;->a()V

    if-eqz p1, :cond_4

    invoke-direct {p0, v1, v0}, Lcom/fixmo/isa/e;->a([BLjava/util/Map;)[B

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to create pre-baseline fingerprint."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v5}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/a/a/a/a;->b()[B

    move-result-object v5

    if-nez v5, :cond_3

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Pre-baseline fingerprint does not already exist."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    goto :goto_0

    :cond_3
    invoke-static {v2, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Pre-baseline fingerprints does not match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/a;->g()Z

    move-result v2

    and-int/2addr v2, v6

    iget-object v5, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v5}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/a/a/a/a;->e()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Platform baseline already exists. Will replace with the new one."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v5, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v5}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v5

    invoke-interface {v5, v1}, Lcom/a/a/a/a;->b([B)Z

    move-result v1

    and-int/2addr v2, v1

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6, v1}, Lcom/a/a/a/a;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v6, "Baseline for package %s already exists. Will replace with the new one."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v1, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v1}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-interface {v6, v1, v0}, Lcom/a/a/a/a;->a(Ljava/lang/String;[B)Z

    move-result v0

    and-int/2addr v2, v0

    goto :goto_1

    :cond_7
    if-eqz v2, :cond_a

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->f()Z

    move-result v0

    and-int/2addr v0, v2

    :goto_2
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/a/a/a/a;->a(I)V

    :goto_3
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    goto/16 :goto_0

    :cond_8
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to establise baseline."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to rollback before reporting error."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const/4 v0, 0x0

    invoke-direct {p0, v4, v0, v3}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :cond_a
    move v0, v2

    goto :goto_2
.end method

.method private a([BLjava/util/Map;)[B
    .locals 7

    const/4 v2, 0x0

    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_1
    :try_start_2
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Error performing pre-baseline scan."

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_0
    :goto_2
    if-eqz v3, :cond_1

    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :cond_1
    :goto_3
    if-eqz v4, :cond_2

    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    :cond_2
    :goto_4
    move-object v0, v2

    :cond_3
    :goto_5
    return-object v0

    :cond_4
    :try_start_6
    new-instance v3, Ljava/io/SequenceInputStream;

    new-instance v0, Lcom/fixmo/isa/a;

    invoke-direct {v0, v5}, Lcom/fixmo/isa/a;-><init>(Ljava/util/List;)V

    invoke-direct {v3, v0}, Ljava/io/SequenceInputStream;-><init>(Ljava/util/Enumeration;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    new-instance v1, Ljava/io/SequenceInputStream;

    invoke-direct {v1, v4, v3}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    invoke-static {v1}, Lcom/fixmo/isa/g;->a(Ljava/io/InputStream;)[B
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_c
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-result-object v0

    if-eqz v1, :cond_5

    :try_start_9
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    :cond_5
    :goto_6
    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    :cond_6
    :goto_7
    if-eqz v4, :cond_3

    :try_start_b
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    goto :goto_5

    :catch_1
    move-exception v1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error closing platformStream."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catch_2
    move-exception v1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v5, "Error closing sequenceStream."

    invoke-static {v2, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :catch_3
    move-exception v1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error closing packageStream."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    :catch_4
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v5, "Error closing sequenceStream."

    invoke-static {v1, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_5
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Error closing packageStream."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_6
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Error closing platformStream."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    :goto_8
    if-eqz v2, :cond_7

    :try_start_c
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    :cond_7
    :goto_9
    if-eqz v3, :cond_8

    :try_start_d
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    :cond_8
    :goto_a
    if-eqz v4, :cond_9

    :try_start_e
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    :cond_9
    :goto_b
    throw v0

    :catch_7
    move-exception v1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v5, "Error closing sequenceStream."

    invoke-static {v2, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    :catch_8
    move-exception v1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error closing packageStream."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    :catch_9
    move-exception v1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error closing platformStream."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_b

    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_8

    :catchall_2
    move-exception v0

    goto :goto_8

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_8

    :catch_a
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    goto/16 :goto_1

    :catch_b
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    :catch_c
    move-exception v0

    goto/16 :goto_1
.end method

.method private b()V
    .locals 6

    const/4 v5, 0x1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing performPreBaselineScan request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xd

    const/4 v2, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lcom/fixmo/isa/f;->a(Lcom/a/a/a/a;II)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;

    move-result-object v0

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v3

    iget-object v4, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v4}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/fixmo/isa/b;->a(Lcom/fixmo/isa/f;Lcom/a/a/a/a;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Third-party package scan returned null result."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/a/a/a;->a()[B

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Platform scan returned null result."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "performPreBaselineScan failed:"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for exception. "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v4}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/fixmo/isa/f;->a()V

    invoke-direct {p0, v3, v0}, Lcom/fixmo/isa/e;->a([BLjava/util/Map;)[B

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Failed to create pre-baseline scan."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/a/a/a;->g()Z

    move-result v3

    and-int/2addr v3, v5

    iget-object v4, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v4}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/a/a/a;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "A prebaseline already exists. Will replace with the new one."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v4, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v4}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/a/a/a/a;->a([B)Z

    move-result v0

    and-int/2addr v0, v3

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/a/a/a;->f()Z

    move-result v3

    and-int/2addr v0, v3

    :cond_4
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/a/a/a/a;->a(I)V

    :goto_1
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    goto/16 :goto_0

    :cond_5
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Failed to perform pre-baseline scan."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Failed to rollback before reporting error"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing removePackageFromBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x6

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    and-int/2addr v0, v2

    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/a/a/a/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Trying to delete baseline for package %s, but it does not already exist."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v2}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/a/a/a;->f()Z

    move-result v2

    and-int/2addr v0, v2

    :cond_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/a/a/a/a;->a(I)V

    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Failed to remove package \'%s\' from baseline."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Failed to rollback before reporting error."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "removePackageFromBaseline failed: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v0, v2, v1}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for exception. "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private c()V
    .locals 5

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing updatePlatformBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xb

    const/4 v2, 0x7

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->d()[B

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "There does not already exist a platform baseline"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lcom/fixmo/isa/f;->a(Lcom/a/a/a/a;II)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->a()[B

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Platform scan returned null result."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "updatePlatformBaseline failed: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to report error for exception. "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/fixmo/isa/f;->a()V

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v4}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/a/a/a;->g()Z

    move-result v4

    and-int/2addr v3, v4

    iget-object v4, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v4}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/a/a/a/a;->c([B)Z

    move-result v0

    and-int/2addr v0, v3

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v3}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/a/a/a;->f()Z

    move-result v3

    and-int/2addr v0, v3

    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/a/a/a/a;->a(I)V

    :goto_1
    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fixmo/isa/f;->a()V

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Failed to update platform baseline"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/e;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/a/a/a;->g()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Failed to rollback before reporting error"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/fixmo/isa/e;->a(ILjava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received a message of unknown type."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :pswitch_0
    invoke-static {}, Ljava/lang/System;->gc()V

    return v5

    :pswitch_1
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Checking ISA Version: %d"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/fixmo/isa/e;->a()V

    goto :goto_0

    :pswitch_3
    const-string v1, "compareWithPreBaseline"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/fixmo/isa/e;->a(Z)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/fixmo/isa/e;->b()V

    goto :goto_0

    :pswitch_5
    const-string v1, "flags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "packageNames"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/fixmo/isa/e;->a(ILjava/util/List;)V

    goto :goto_0

    :pswitch_6
    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/fixmo/isa/e;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_7
    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/fixmo/isa/e;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/fixmo/isa/e;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
