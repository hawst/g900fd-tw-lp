.class Lcom/fixmo/isa/d;
.super Lcom/a/a/a/e;


# instance fields
.field final synthetic a:Lcom/fixmo/isa/FixmoISAService;


# direct methods
.method constructor <init>(Lcom/fixmo/isa/FixmoISAService;)V
    .locals 0

    iput-object p1, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-direct {p0}, Lcom/a/a/a/e;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/fixmo/isa/f;->a(I)V

    return-void
.end method

.method public a(Lcom/a/a/a/a;)V
    .locals 2

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Setting callback to the ISL"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0, p1}, Lcom/fixmo/isa/FixmoISAService;->a(Lcom/fixmo/isa/FixmoISAService;Lcom/a/a/a/a;)Lcom/a/a/a/a;

    return-void
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received updatePlatformBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public a(ILjava/util/List;)Z
    .locals 4

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received performScanNow request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_1

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "flags"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "packageNames"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received removePackageFromBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "packageName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Z)Z
    .locals 3

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "In establishBaselineScan request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "compareWithPreBaseline"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received performPreBaselineScan request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received putPackageToBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "packageName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    const-string v1, "android.permission.sec.MDM_ENTERPRISE_ISL"

    invoke-virtual {v0, v1}, Lcom/fixmo/isa/FixmoISAService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "permission denied"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/fixmo/isa/FixmoISAService;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received clearBaseline request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/fixmo/isa/d;->a:Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/FixmoISAService;->e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return v2
.end method
