.class public final Lcom/fixmo/isa/FixmoISAService;
.super Landroid/app/Service;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/fixmo/isa/f;

.field private d:Lcom/a/a/a/a;

.field private e:Lcom/fixmo/isa/b;

.field private f:Landroid/os/Handler;

.field private g:Landroid/os/Looper;

.field private final h:Lcom/a/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/fixmo/isa/FixmoISAService;

    invoke-static {v0}, Lcom/fixmo/isa/g;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/fixmo/isa/FixmoISAService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object p0, p0, Lcom/fixmo/isa/FixmoISAService;->b:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->d:Lcom/a/a/a/a;

    new-instance v0, Lcom/fixmo/isa/d;

    invoke-direct {v0, p0}, Lcom/fixmo/isa/d;-><init>(Lcom/fixmo/isa/FixmoISAService;)V

    iput-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->h:Lcom/a/a/a/e;

    return-void
.end method

.method static synthetic a(Lcom/fixmo/isa/FixmoISAService;Lcom/a/a/a/a;)Lcom/a/a/a/a;
    .locals 0

    iput-object p1, p0, Lcom/fixmo/isa/FixmoISAService;->d:Lcom/a/a/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/f;
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->c:Lcom/fixmo/isa/f;

    return-object v0
.end method

.method static synthetic b(Lcom/fixmo/isa/FixmoISAService;)Lcom/a/a/a/a;
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->d:Lcom/a/a/a/a;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/fixmo/isa/FixmoISAService;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/fixmo/isa/FixmoISAService;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->b:Landroid/content/Context;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->d:Lcom/a/a/a/a;

    invoke-interface {v0}, Lcom/a/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/fixmo/isa/FixmoISAService;->a:Ljava/lang/String;

    const-string v1, "ISL V1 detected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "islfromknoxv1"
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/fixmo/isa/FixmoISAService;->a:Ljava/lang/String;

    const-string v1, "detected ISL V1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "islfromknoxv1"

    goto :goto_0
.end method

.method static synthetic d(Lcom/fixmo/isa/FixmoISAService;)Lcom/fixmo/isa/b;
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->e:Lcom/fixmo/isa/b;

    return-object v0
.end method

.method static synthetic e(Lcom/fixmo/isa/FixmoISAService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->f:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected a()Z
    .locals 4

    invoke-direct {p0}, Lcom/fixmo/isa/FixmoISAService;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/fixmo/isa/FixmoISAService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "detected isl version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "islfromknoxv1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->h:Lcom/a/a/a/e;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/fixmo/isa/f;

    invoke-direct {v0}, Lcom/fixmo/isa/f;-><init>()V

    iput-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->c:Lcom/fixmo/isa/f;

    new-instance v0, Lcom/fixmo/isa/b;

    invoke-direct {v0, p0}, Lcom/fixmo/isa/b;-><init>(Lcom/fixmo/isa/FixmoISAService;)V

    iput-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->e:Lcom/fixmo/isa/b;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FixmoISAThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->g:Landroid/os/Looper;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/fixmo/isa/FixmoISAService;->g:Landroid/os/Looper;

    new-instance v2, Lcom/fixmo/isa/e;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/fixmo/isa/e;-><init>(Lcom/fixmo/isa/FixmoISAService;Lcom/fixmo/isa/d;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->f:Landroid/os/Handler;

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/fixmo/isa/FixmoISAService;->g:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method
