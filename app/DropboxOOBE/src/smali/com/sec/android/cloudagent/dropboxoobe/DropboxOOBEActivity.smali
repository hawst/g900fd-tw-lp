.class public Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;
.super Landroid/app/Activity;


# static fields
.field public static final a:Z

.field public static final b:Z

.field public static final c:Z

.field public static final d:Z

.field public static final e:Z

.field public static final f:Z

.field public static final g:Z

.field public static final h:Z

.field public static final i:Z

.field public static final j:Z

.field public static final k:Z

.field public static final l:Z

.field public static final m:Z

.field private static final o:Ljava/lang/String;

.field private static final p:Z


# instance fields
.field n:Landroid/widget/LinearLayout;

.field private q:Landroid/app/AlertDialog;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View$OnClickListener;

.field private w:Landroid/view/View$OnClickListener;

.field private x:Landroid/view/View$OnClickListener;

.field private y:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kona"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->p:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "hlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "ha3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "h3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "htdlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "s3ve"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ASH"

    sget-object v3, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SC-01F"

    sget-object v3, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Madrid"

    sget-object v3, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCL22"

    sget-object v3, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "JS01"

    sget-object v3, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SC-02F"

    sget-object v3, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "flte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->b:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "cane"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "heat"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "victor"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "vivalto"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "vastalte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "fortuna"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "afyon"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kanas"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->e:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "ms01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->f:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "hllte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "hl3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "fresco"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_2
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->g:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "k3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kalte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kq3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kqlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kmini"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kactivelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "m2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "lentis"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "SCL23"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "berluti"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "ks01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "patek"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "q7"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "slte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "hestia"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kcat6"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "kccat6"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "vlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "superior"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_3
    move v0, v2

    :goto_3
    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->h:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "mega2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "vasta3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_4
    move v0, v2

    :goto_4
    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->i:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "u0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->j:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "a5"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->k:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "pocket2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->l:Z

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "ja3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "jflte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "jalte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "jactive"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "jfvelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->o:Ljava/lang/String;

    const-string v3, "jftdd"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v1, v2

    :cond_6
    sput-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->m:Z

    return-void

    :cond_7
    move v0, v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto :goto_4
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/sec/android/cloudagent/dropboxoobe/a;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/dropboxoobe/a;-><init>(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->v:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/cloudagent/dropboxoobe/b;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/dropboxoobe/b;-><init>(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->w:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/cloudagent/dropboxoobe/c;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/dropboxoobe/c;-><init>(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->x:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/android/cloudagent/dropboxoobe/d;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/dropboxoobe/d;-><init>(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->y:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->q:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->q:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private a()V
    .locals 3

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->m:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    :goto_0
    invoke-direct {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->b()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, -0x7c000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method private a(ILjava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 4

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "android.app.StatusBarManager"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const-string v2, "setTransGradationMode"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "statusbar"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "CloudAgentOOBE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got exception in setTransGradationMode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;ILjava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(ILjava/lang/Exception;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "FULL_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "SamsungDark"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private a(Z)V
    .locals 7

    const/16 v3, 0x32

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->r:Landroid/widget/Button;

    const v0, 0x7f0a0004

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->s:Landroid/widget/Button;

    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->t:Landroid/widget/TextView;

    const v0, 0x7f0a0002

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->u:Landroid/widget/TextView;

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->u:Landroid/widget/TextView;

    const v1, 0x7f06000e

    invoke-virtual {p0, v1}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->r:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->s:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0006

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->n:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0007

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f060018

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->n:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->t:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->r:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->s:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->u:Landroid/widget/TextView;

    const v1, 0x7f060010

    invoke-virtual {p0, v1}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f06001a

    invoke-virtual {p0, v3}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->r:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->s:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1
.end method

.method private b()V
    .locals 8

    const v6, 0x7f050009

    const v5, 0x103012c

    const/16 v4, 0x13

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->h:Z

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v4, :cond_0

    invoke-virtual {p0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setTheme(I)V

    const v1, 0x7f030005

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(Landroid/view/View;)V

    :goto_1
    return-void

    :cond_0
    const v1, 0x7f030003

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    goto :goto_0

    :cond_1
    sget-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->k:Z

    if-eqz v1, :cond_2

    const v1, 0x7f030005

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    sget-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d:Z

    if-nez v1, :cond_3

    sget-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->e:Z

    if-eqz v1, :cond_4

    :cond_3
    const v1, 0x7f030004

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(Landroid/view/View;)V

    goto :goto_1

    :cond_4
    sget-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->i:Z

    if-eqz v1, :cond_5

    const v1, 0x7f030006

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(Landroid/view/View;)V

    goto :goto_1

    :cond_5
    sget-boolean v1, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->m:Z

    if-eqz v1, :cond_7

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v4, :cond_6

    invoke-virtual {p0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setTheme(I)V

    const v1, 0x7f030002

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_6
    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    goto :goto_2

    :cond_7
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v4, :cond_8

    invoke-virtual {p0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setTheme(I)V

    const v1, 0x7f030002

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_3
    invoke-virtual {v1, v3, v0, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {p0, v1}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_8
    const v1, 0x7f030001

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_3
.end method

.method private c()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.dropbox.android"

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.sec.android.cloudagent"

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x7

    const/4 v4, 0x1

    const/4 v3, -0x1

    const-string v0, "CloudAgentOOBE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v3, :cond_0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.cloudagent.DROPBOX_AUTH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "requestSignIn"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-ne p2, v3, :cond_2

    invoke-virtual {p0, v3}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setResult(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setResult(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(ILjava/lang/Exception;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v0, "CloudAgentOOBE"

    const-string v1, "Dropbox OOBE onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->m:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->f:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->g:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->h:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->k:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->e:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->i:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->l:Z

    if-eqz v0, :cond_3

    :cond_0
    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Landroid/content/Context;Z)V

    :cond_1
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Z)V

    return-void

    :cond_2
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(I)V

    goto :goto_0

    :cond_3
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->finish()V

    :cond_0
    const-string v0, "CloudAgentOOBE"

    const-string v1, "Dropbox OOBE onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/cloudagent/b;->a(Landroid/content/Context;)Z

    move-result v0

    const-string v1, "CloudAgentOOBE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCloudAvailable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    const-string v0, "CloudAgentOOBE"

    const-string v1, "Dropbox account already exists.. So set RESULT_OK and finish the activity"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->finish()V

    :cond_1
    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->m:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->b:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->f:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->g:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->h:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->k:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->e:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->i:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->l:Z

    if-eqz v0, :cond_5

    :cond_2
    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Landroid/content/Context;Z)V

    :cond_3
    :goto_0
    invoke-direct {p0, v5}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Z)V

    return-void

    :cond_4
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(I)V

    goto :goto_0

    :cond_5
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->setContentView(I)V

    goto :goto_0
.end method
