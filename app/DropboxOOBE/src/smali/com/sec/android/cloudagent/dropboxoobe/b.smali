.class Lcom/sec/android/cloudagent/dropboxoobe/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/dropboxoobe/b;->a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/b;->a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/b;->a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;

    const-string v1, "com.dropbox.intent.action.LOGIN"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->b:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->c:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->f:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->g:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->h:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->k:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->d:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->e:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->i:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/b;->a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;

    const-string v1, "com.dropbox.intent.action.LOGIN"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    sget-boolean v0, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->m:Z

    if-eqz v0, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/b;->a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;

    const-string v1, "com.dropbox.intent.action.LOGIN"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/cloudagent/dropboxoobe/b;->a:Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;

    const-string v1, "com.dropbox.intent.action.SAMSUNG_LOGIN"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;->a(Lcom/sec/android/cloudagent/dropboxoobe/DropboxOOBEActivity;Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
