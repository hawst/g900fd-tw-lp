.class public Lcom/sec/android/provider/emergencymode/EmergencyProvider;
.super Landroid/content/ContentProvider;
.source "EmergencyProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;,
        Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static final s_urlMatcher:Landroid/content/UriMatcher;


# instance fields
.field private appPositionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

.field private mSP:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    const-string v0, "EmergencyProvider"

    sput-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    .line 56
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    .line 78
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "whitelist"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "prefsettings"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 80
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "disabledpkg"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 81
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "alarm"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 82
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "ecclist"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 83
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "launcherdefault"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "launcheradd"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 85
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.emergencymode"

    const-string v2, "updatetable"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    .line 853
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 285
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v6, "bulkInsert start"

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v5, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {v5}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 287
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 289
    :try_start_0
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 290
    .local v2, "match":I
    array-length v3, p2

    .line 291
    .local v3, "numValues":I
    const/4 v4, 0x0

    .line 292
    .local v4, "table":Ljava/lang/String;
    packed-switch v2, :pswitch_data_0

    .line 315
    :goto_0
    if-eqz v4, :cond_1

    .line 316
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 317
    const/4 v5, 0x0

    aget-object v6, p2, v1

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 318
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v6, "bulkInsert, return 0"

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    const/4 v5, 0x0

    .line 325
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 329
    .end local v1    # "i":I
    :goto_2
    return v5

    .line 295
    :pswitch_0
    :try_start_1
    const-string v4, "whitelist"

    .line 296
    goto :goto_0

    .line 299
    :pswitch_1
    const-string v4, "prefsettings"

    .line 300
    goto :goto_0

    .line 303
    :pswitch_2
    const-string v4, "disabledpkg"

    .line 304
    goto :goto_0

    .line 307
    :pswitch_3
    const-string v4, "alarm"

    .line 308
    goto :goto_0

    .line 311
    :pswitch_4
    const-string v4, "ecclist"

    goto :goto_0

    .line 316
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 323
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 328
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bulkInsert, uri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ContentValues : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    array-length v5, p2

    goto :goto_2

    .line 325
    .end local v2    # "match":I
    .end local v3    # "numValues":I
    .end local v4    # "table":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 130
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 132
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    sget-object v4, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 133
    .local v3, "match":I
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 134
    packed-switch v3, :pswitch_data_0

    .line 176
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 189
    .end local v3    # "match":I
    :cond_0
    :goto_1
    sget-object v4, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " delete count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return v0

    .line 138
    .restart local v3    # "match":I
    :pswitch_0
    :try_start_1
    const-string v4, "whitelist"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 139
    goto :goto_0

    .line 143
    :pswitch_1
    const-string v4, "prefsettings"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 144
    goto :goto_0

    .line 148
    :pswitch_2
    const-string v4, "disabledpkg"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 149
    goto :goto_0

    .line 153
    :pswitch_3
    const-string v4, "alarm"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 154
    goto :goto_0

    .line 158
    :pswitch_4
    const-string v4, "ecclist"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 159
    goto :goto_0

    .line 163
    :pswitch_5
    const-string v4, "launcherdefault"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 164
    goto :goto_0

    .line 168
    :pswitch_6
    const-string v4, "launcheradd"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 169
    goto :goto_0

    .line 177
    .end local v3    # "match":I
    :catch_0
    move-exception v2

    .line 178
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v4, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete Exception e "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 180
    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .line 180
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_1

    .line 181
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_1
    throw v4

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected findAppInfo(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/sec/android/provider/emergencymode/AppInfo;
    .locals 4
    .param p2, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/provider/emergencymode/AppInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/provider/emergencymode/AppInfo;"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/4 v2, 0x0

    .line 578
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v1, v2

    .line 588
    :goto_0
    return-object v1

    .line 582
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 583
    .local v1, "thisAppInfo":Lcom/sec/android/provider/emergencymode/AppInfo;
    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v1    # "thisAppInfo":Lcom/sec/android/provider/emergencymode/AppInfo;
    :cond_3
    move-object v1, v2

    .line 588
    goto :goto_0
.end method

.method protected getBestAppPosition(ILjava/lang/String;)I
    .locals 7
    .param p1, "oldPosition"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 592
    sget-object v3, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBestAppPosition name["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] old position["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iget-object v3, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;

    .line 594
    .local v1, "posInfo":Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPosition()I

    move-result v3

    if-nez v3, :cond_0

    .line 596
    invoke-virtual {v1, v6}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->setPosition(I)V

    .line 597
    invoke-virtual {v1, p2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->setPackageName(Ljava/lang/String;)V

    .line 598
    sget-object v3, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v4, "It got same position"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    .end local p1    # "oldPosition":I
    :goto_0
    return p1

    .line 601
    .restart local p1    # "oldPosition":I
    :cond_0
    sget-object v3, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "State of same position : name["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] position State["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 604
    iget-object v3, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;

    .line 605
    .local v2, "thisPosInfo":Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
    invoke-virtual {v2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPosition()I

    move-result v3

    if-nez v3, :cond_1

    .line 606
    invoke-virtual {v2, v6}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->setPosition(I)V

    .line 607
    invoke-virtual {v2, p2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->setPackageName(Ljava/lang/String;)V

    .line 608
    sget-object v3, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "It got new position ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    move p1, v0

    .line 609
    goto :goto_0

    .line 603
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 614
    .end local v2    # "thisPosInfo":Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
    :cond_2
    sget-object v3, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v4, "Getting new position was failed"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const/4 p1, -0x1

    goto :goto_0
.end method

.method protected declared-synchronized getLastUpdateTime()J
    .locals 4

    .prologue
    .line 668
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mSP:Landroid/content/SharedPreferences;

    const-string v1, "LastUpdateTime"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "url"    # Landroid/net/Uri;

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 122
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 106
    :pswitch_0
    const-string v0, "com.sec.android.emergencymode/whitelist"

    goto :goto_0

    .line 108
    :pswitch_1
    const-string v0, "com.sec.android.emergencymode/prefsettings"

    goto :goto_0

    .line 110
    :pswitch_2
    const-string v0, "com.sec.android.emergencymode/disabledpkg"

    goto :goto_0

    .line 112
    :pswitch_3
    const-string v0, "com.sec.android.emergencymode/alarm"

    goto :goto_0

    .line 114
    :pswitch_4
    const-string v0, "com.sec.android.emergencymode/ecclist"

    goto :goto_0

    .line 116
    :pswitch_5
    const-string v0, "com.sec.android.emergencymode/launcherdefault"

    goto :goto_0

    .line 118
    :pswitch_6
    const-string v0, "com.sec.android.emergencymode/launcheradd"

    goto :goto_0

    .line 120
    :pswitch_7
    const-string v0, "com.sec.android.emergencymode/updatetable"

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected initAppPosition(Z)V
    .locals 7
    .param p1, "upsm"    # Z

    .prologue
    .line 619
    const/4 v3, 0x0

    .line 620
    .local v3, "i":I
    const-string v5, "ro.csc.countryiso_code"

    const-string v6, "unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 621
    .local v2, "countryIso":Ljava/lang/String;
    const-string v5, "jp"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 624
    .local v4, "isJapan":Z
    iget-object v5, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 625
    if-eqz v4, :cond_1

    .line 627
    if-eqz p1, :cond_0

    const/16 v1, 0x9

    .line 633
    .local v1, "availableAppNumber":I
    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_3

    .line 634
    new-instance v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;

    const-string v5, "EMPTY"

    const/4 v6, 0x0

    invoke-direct {v0, p0, v5, v6}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;-><init>(Lcom/sec/android/provider/emergencymode/EmergencyProvider;Ljava/lang/String;I)V

    .line 635
    .local v0, "appInfo":Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
    iget-object v5, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 627
    .end local v0    # "appInfo":Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
    .end local v1    # "availableAppNumber":I
    :cond_0
    const/4 v1, 0x7

    goto :goto_0

    .line 630
    :cond_1
    if-eqz p1, :cond_2

    const/4 v1, 0x6

    .restart local v1    # "availableAppNumber":I
    :goto_2
    goto :goto_0

    .end local v1    # "availableAppNumber":I
    :cond_2
    const/4 v1, 0x4

    goto :goto_2

    .line 637
    .restart local v1    # "availableAppNumber":I
    :cond_3
    return-void
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    .line 195
    const/4 v3, 0x0

    .line 196
    .local v3, "result":Landroid/net/Uri;
    sget-object v8, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v9, "insert start"

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v8, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {v8}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 199
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 200
    sget-object v8, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 202
    .local v2, "match":I
    const/4 v6, 0x0

    .line 204
    .local v6, "table":Ljava/lang/String;
    if-eqz p2, :cond_4

    .line 205
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 210
    .local v7, "values":Landroid/content/ContentValues;
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 263
    :cond_0
    :goto_1
    if-eqz v6, :cond_2

    .line 264
    const/4 v8, 0x0

    invoke-virtual {v0, v6, v8, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 265
    .local v4, "rowID":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_1

    .line 267
    invoke-static {p1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 269
    :cond_1
    sget-object v8, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "inserted rowID = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .end local v4    # "rowID":J
    :cond_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    if-eqz v0, :cond_3

    .line 276
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 280
    .end local v2    # "match":I
    .end local v6    # "table":Ljava/lang/String;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_3
    :goto_2
    return-object v3

    .line 207
    .restart local v2    # "match":I
    .restart local v6    # "table":Ljava/lang/String;
    :cond_4
    :try_start_1
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .restart local v7    # "values":Landroid/content/ContentValues;
    goto :goto_0

    .line 213
    :pswitch_0
    const-string v8, "pkg"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 215
    const-string v8, "allowflag"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 217
    const-string v6, "whitelist"

    .line 218
    goto :goto_1

    .line 221
    :pswitch_1
    const-string v8, "pref"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 223
    const-string v8, "value"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 225
    const-string v6, "prefsettings"

    .line 226
    goto :goto_1

    .line 229
    :pswitch_2
    const-string v8, "pkg"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 231
    const-string v8, "value"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 233
    const-string v6, "disabledpkg"

    .line 234
    goto :goto_1

    .line 237
    :pswitch_3
    const-string v8, "pkg"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 239
    const-string v8, "action"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 241
    const-string v6, "alarm"

    .line 242
    goto/16 :goto_1

    .line 245
    :pswitch_4
    const-string v8, "mcc"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 247
    const-string v8, "emcall"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 249
    const-string v8, "heatsensing"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 251
    const-string v6, "ecclist"

    .line 252
    goto/16 :goto_1

    .line 255
    :pswitch_5
    const-string v6, "launcherdefault"

    .line 256
    goto/16 :goto_1

    .line 259
    :pswitch_6
    const-string v6, "launcheradd"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 272
    .end local v2    # "match":I
    .end local v6    # "table":Ljava/lang/String;
    .end local v7    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 273
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v8, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "insert Exception e "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 275
    if-eqz v0, :cond_3

    .line 276
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_2

    .line 275
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_5

    .line 276
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_5
    throw v8

    .line 210
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected occupyAppPosition(ILjava/lang/String;)Z
    .locals 5
    .param p1, "position"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 640
    iget-object v3, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 641
    .local v0, "appPositionListSize":I
    if-gt v0, p1, :cond_0

    .line 642
    sget-object v2, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "occupyAppPosition too high position["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] max size["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 644
    :cond_0
    iget-object v3, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->appPositionList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;

    .line 645
    .local v1, "posInfo":Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPosition()I

    move-result v3

    if-eqz v3, :cond_1

    .line 646
    sget-object v2, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "occupyAppPosition already occupied : position state ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] pkgName["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 649
    :cond_1
    invoke-virtual {v1, v2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->setPosition(I)V

    .line 650
    invoke-virtual {v1, p2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->setPackageName(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mSP:Landroid/content/SharedPreferences;

    .line 96
    new-instance v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;-><init>(Lcom/sec/android/provider/emergencymode/EmergencyProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    .line 97
    sget-object v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v1, "onCreate Complete"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 335
    sget-object v2, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "query start "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 338
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v8

    .line 339
    .local v8, "match":I
    packed-switch v8, :pswitch_data_0

    .line 395
    :goto_0
    return-object v5

    .line 341
    :pswitch_0
    const-string v2, "whitelist"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 385
    :goto_1
    iget-object v2, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 386
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v9, 0x0

    .line 387
    .local v9, "ret":Landroid/database/Cursor;
    const/16 v2, 0x8

    if-eq v8, v2, :cond_0

    .line 388
    if-eqz v1, :cond_0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    .line 389
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 391
    if-eqz v9, :cond_0

    .line 392
    sget-object v2, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "query count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v5, v9

    .line 395
    goto :goto_0

    .line 346
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "ret":Landroid/database/Cursor;
    :pswitch_1
    const-string v2, "prefsettings"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 351
    :pswitch_2
    const-string v2, "disabledpkg"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 356
    :pswitch_3
    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 361
    :pswitch_4
    const-string v2, "ecclist"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 366
    :pswitch_5
    const-string v2, "launcherdefault"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 371
    :pswitch_6
    const-string v2, "launcheradd"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 377
    :pswitch_7
    invoke-virtual {p0}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->updateTable()V

    goto :goto_0

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected declared-synchronized setLastUpdateTime(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 658
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mSP:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 659
    iget-object v1, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mSP:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 660
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LastUpdateTime"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 661
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    :goto_0
    monitor-exit p0

    return-void

    .line 663
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v2, "setLastUpdateTime mSP is null!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 658
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 401
    const/4 v0, 0x0

    .line 403
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 405
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 406
    sget-object v4, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 407
    .local v3, "match":I
    packed-switch v3, :pswitch_data_0

    .line 455
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    if-eqz v1, :cond_0

    .line 460
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 469
    .end local v3    # "match":I
    :cond_0
    :goto_1
    return v0

    .line 411
    .restart local v3    # "match":I
    :pswitch_0
    :try_start_1
    const-string v4, "whitelist"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 412
    goto :goto_0

    .line 417
    :pswitch_1
    const-string v4, "prefsettings"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 418
    goto :goto_0

    .line 423
    :pswitch_2
    const-string v4, "disabledpkg"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 424
    goto :goto_0

    .line 429
    :pswitch_3
    const-string v4, "alarm"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 430
    goto :goto_0

    .line 435
    :pswitch_4
    const-string v4, "ecclist"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 436
    goto :goto_0

    .line 441
    :pswitch_5
    const-string v4, "launcherdefault"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 442
    goto :goto_0

    .line 447
    :pswitch_6
    const-string v4, "launcheradd"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 448
    goto :goto_0

    .line 456
    .end local v3    # "match":I
    :catch_0
    move-exception v2

    .line 457
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v4, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update Exception e "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 459
    if-eqz v1, :cond_0

    .line 460
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .line 459
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_1

    .line 460
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_1
    throw v4

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected updateTable()V
    .locals 10

    .prologue
    .line 473
    new-instance v4, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;

    invoke-virtual {p0}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;-><init>(Landroid/content/Context;)V

    .line 474
    .local v4, "listLoader":Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    const-wide/16 v2, 0x0

    .local v2, "lastUpdateTime":J
    const-wide/16 v6, 0x0

    .line 478
    .local v6, "updateTimeOfAppOrCSC":J
    invoke-virtual {v4}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getUpdateTimeOfAppOrCSC()J

    move-result-wide v6

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->getLastUpdateTime()J

    move-result-wide v2

    .line 481
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-eqz v5, :cond_0

    cmp-long v5, v2, v6

    if-eqz v5, :cond_1

    .line 483
    :cond_0
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v8, "updateTable start!"

    invoke-static {v5, v8}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v5, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    invoke-virtual {v5}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 487
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 488
    const/4 v5, 0x1

    invoke-virtual {p0, v0, v4, v5}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;Z)V

    .line 489
    const/4 v5, 0x0

    invoke-virtual {p0, v0, v4, v5}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->updateTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;Z)V

    .line 490
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 492
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->setLastUpdateTime(J)V

    .line 493
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v8, "updateTable finished!"

    invoke-static {v5, v8}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    if-eqz v0, :cond_1

    .line 499
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 503
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :goto_0
    return-void

    .line 494
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v1

    .line 495
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v5, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;

    const-string v8, "updateTable stopped!!"

    invoke-static {v5, v8}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498
    if-eqz v0, :cond_1

    .line 499
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .line 498
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_2

    .line 499
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_2
    throw v5
.end method

.method protected updateTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;Z)V
    .locals 22
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "listLoader"    # Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    .param p3, "isUpsm"    # Z

    .prologue
    .line 507
    if-eqz p3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    .line 508
    .local v12, "columnMode":Ljava/lang/String;
    const/4 v13, 0x0

    .line 509
    .local v13, "cursorOldDefault":Landroid/database/Cursor;
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v18, "newAppInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->initAppPosition(Z)V

    .line 513
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 514
    .local v5, "selection":Ljava/lang/String;
    const-string v2, "launcheradd"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 517
    invoke-virtual/range {p2 .. p3}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->loadAllowedXMLIntoDatabase(Z)Ljava/util/ArrayList;

    move-result-object v10

    .line 518
    .local v10, "addAppInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 519
    .local v17, "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v2, "launcheradd"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherAddList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1

    .line 507
    .end local v5    # "selection":Ljava/lang/String;
    .end local v10    # "addAppInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v12    # "columnMode":Ljava/lang/String;
    .end local v13    # "cursorOldDefault":Landroid/database/Cursor;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    .end local v18    # "newAppInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 523
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v10    # "addAppInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v12    # "columnMode":Ljava/lang/String;
    .restart local v13    # "cursorOldDefault":Landroid/database/Cursor;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v18    # "newAppInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' AND fixed=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 524
    const-string v2, "launcherdefault"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 526
    invoke-virtual/range {p2 .. p3}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->loadDefaultXMLIntoDatabase(Z)Ljava/util/ArrayList;

    move-result-object v14

    .line 527
    .local v14, "defaultAppInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 528
    .restart local v17    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v2, "launcherdefault"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherDefaultList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 529
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPosition()I

    move-result v2

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->occupyAppPosition(ILjava/lang/String;)Z

    goto :goto_2

    .line 533
    .end local v17    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' AND fixed=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 537
    :try_start_0
    const-string v3, "launcherdefault"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "package"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string v6, "position"

    aput-object v6, v4, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 546
    if-eqz v13, :cond_5

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 548
    :cond_3
    const-string v2, "package"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 549
    .local v21, "pkgName":Ljava/lang/String;
    const-string v2, "position"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 550
    .local v20, "oldPosition":I
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->findAppInfo(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/sec/android/provider/emergencymode/AppInfo;

    move-result-object v11

    .line 551
    .local v11, "appInfo":Lcom/sec/android/provider/emergencymode/AppInfo;
    if-eqz v11, :cond_4

    .line 553
    invoke-virtual {v11}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->getBestAppPosition(ILjava/lang/String;)I

    move-result v19

    .line 554
    .local v19, "newPosition":I
    const/4 v2, -0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_4

    .line 555
    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/sec/android/provider/emergencymode/AppInfo;->setPosition(I)V

    .line 556
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    .end local v19    # "newPosition":I
    :cond_4
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 563
    .end local v11    # "appInfo":Lcom/sec/android/provider/emergencymode/AppInfo;
    .end local v20    # "oldPosition":I
    .end local v21    # "pkgName":Ljava/lang/String;
    :cond_5
    const-string v2, "launcherdefault"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 565
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 566
    .restart local v17    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v2, "launcherdefault"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->mOpenHelper:Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherDefaultListInfoUpdate(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 568
    .end local v17    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    :catch_0
    move-exception v15

    .line 569
    .local v15, "ex":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571
    if-eqz v13, :cond_6

    .line 572
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 575
    .end local v15    # "ex":Ljava/lang/Exception;
    :cond_6
    :goto_4
    return-void

    .line 571
    :cond_7
    if-eqz v13, :cond_6

    .line 572
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 571
    :catchall_0
    move-exception v2

    if-eqz v13, :cond_8

    .line 572
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
.end method
