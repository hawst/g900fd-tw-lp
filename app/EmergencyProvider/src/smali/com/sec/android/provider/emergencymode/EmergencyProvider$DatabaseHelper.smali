.class Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "EmergencyProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/provider/emergencymode/EmergencyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseHelper"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/provider/emergencymode/EmergencyProvider;


# direct methods
.method constructor <init>(Lcom/sec/android/provider/emergencymode/EmergencyProvider;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 678
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->this$0:Lcom/sec/android/provider/emergencymode/EmergencyProvider;

    .line 679
    const-string v0, "emergency.db"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 680
    iput-object p2, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    .line 682
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DatabaseHelper"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    return-void
.end method

.method private createAlarmTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 825
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createAlarmTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    const-string v0, "DROP TABLE IF EXISTS alarm"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 828
    const-string v0, "CREATE TABLE alarm(pkg TEXT, action TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 829
    return-void
.end method

.method private createDisablePkgTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 818
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createDisablePkgTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const-string v0, "DROP TABLE IF EXISTS disabledpkg"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 821
    const-string v0, "CREATE TABLE disabledpkg(pkg TEXT, value TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 822
    return-void
.end method

.method private createEccTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 832
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createEccTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    const-string v0, "DROP TABLE IF EXISTS ecclist"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 835
    const-string v0, "CREATE TABLE ecclist(mcc TEXT, emcall TEXT, heatsensing TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 836
    return-void
.end method

.method private createLauncherAddTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 846
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createLauncherAddTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const-string v0, "DROP TABLE IF EXISTS launcheradd"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 849
    const-string v0, "CREATE TABLE launcheradd(package TEXT, class TEXT, permission TEXT, mode INTEGER)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 850
    return-void
.end method

.method private createLauncherDefaultTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 839
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createLauncherDefaultTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    const-string v0, "DROP TABLE IF EXISTS launcherdefault"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 842
    const-string v0, "CREATE TABLE launcherdefault(package TEXT, class TEXT, position INTEGER, fixed INTEGER, mode INTEGER)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 843
    return-void
.end method

.method private createPrefSettingTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 811
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createPrefSettingTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v0, "DROP TABLE IF EXISTS prefsettings"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 814
    const-string v0, "CREATE TABLE prefsettings(pref TEXT, value TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 815
    return-void
.end method

.method private createWhiteListTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 804
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "createWhiteListTable()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const-string v0, "DROP TABLE IF EXISTS whitelist"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 807
    const-string v0, "CREATE TABLE whitelist(_id INTEGER PRIMARY KEY, pkg TEXT, allowflag INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 808
    return-void
.end method

.method private initDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 22
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 701
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const/high16 v18, 0x7f050000

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 705
    .local v7, "eccList":[Ljava/lang/String;
    move-object v4, v7

    .local v4, "arr$":[Ljava/lang/String;
    array-length v13, v4

    .local v13, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v13, :cond_0

    aget-object v6, v4, v10

    .line 706
    .local v6, "ecc":Ljava/lang/String;
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 707
    .local v14, "splitList":[Ljava/lang/String;
    const-string v17, "ecclist"

    const/16 v18, 0x0

    const/16 v19, 0x0

    aget-object v19, v14, v19

    const/16 v20, 0x1

    aget-object v20, v14, v20

    const/16 v21, 0x2

    aget-object v21, v14, v21

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForEccList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 705
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 710
    .end local v6    # "ecc":Ljava/lang/String;
    .end local v14    # "splitList":[Ljava/lang/String;
    :cond_0
    new-instance v12, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;-><init>(Landroid/content/Context;)V

    .line 711
    .local v12, "launcherListLoader":Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->loadDefaultXMLIntoDatabase(Z)Ljava/util/ArrayList;

    move-result-object v16

    .line 712
    .local v16, "upsmDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->loadAllowedXMLIntoDatabase(Z)Ljava/util/ArrayList;

    move-result-object v15

    .line 713
    .local v15, "upsmAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->loadDefaultXMLIntoDatabase(Z)Ljava/util/ArrayList;

    move-result-object v9

    .line 714
    .local v9, "emDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->loadAllowedXMLIntoDatabase(Z)Ljava/util/ArrayList;

    move-result-object v8

    .line 716
    .local v8, "emAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 717
    .local v11, "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v17, "launcherdefault"

    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherDefaultList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 731
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v7    # "eccList":[Ljava/lang/String;
    .end local v8    # "emAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v9    # "emDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    .end local v12    # "launcherListLoader":Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    .end local v13    # "len$":I
    .end local v15    # "upsmAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v16    # "upsmDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    :catch_0
    move-exception v5

    .line 732
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "init Exception e "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 734
    if-eqz p1, :cond_1

    .line 735
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 738
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 719
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v7    # "eccList":[Ljava/lang/String;
    .restart local v8    # "emAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v9    # "emDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v12    # "launcherListLoader":Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    .restart local v13    # "len$":I
    .restart local v15    # "upsmAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v16    # "upsmDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    :cond_2
    :try_start_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 720
    .restart local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v17, "launcheradd"

    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherAddList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 734
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v7    # "eccList":[Ljava/lang/String;
    .end local v8    # "emAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v9    # "emDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    .end local v12    # "launcherListLoader":Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    .end local v13    # "len$":I
    .end local v15    # "upsmAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v16    # "upsmDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    :catchall_0
    move-exception v17

    if-eqz p1, :cond_3

    .line 735
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_3
    throw v17

    .line 722
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v7    # "eccList":[Ljava/lang/String;
    .restart local v8    # "emAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v9    # "emDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v12    # "launcherListLoader":Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
    .restart local v13    # "len$":I
    .restart local v15    # "upsmAddAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v16    # "upsmDefaultAppInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    :cond_4
    :try_start_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 723
    .restart local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v17, "launcherdefault"

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherDefaultList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_4

    .line 725
    .end local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    :cond_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 726
    .restart local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    const-string v17, "launcheradd"

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->mappingForLauncherAddList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_5

    .line 729
    .end local v11    # "info":Lcom/sec/android/provider/emergencymode/AppInfo;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 730
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->this$0:Lcom/sec/android/provider/emergencymode/EmergencyProvider;

    move-object/from16 v17, v0

    invoke-virtual {v12}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getUpdateTimeOfAppOrCSC()J

    move-result-wide v18

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->setLastUpdateTime(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 734
    if-eqz p1, :cond_1

    .line 735
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_2
.end method

.method private mappingForEccList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 2
    .param p1, "mcc"    # Ljava/lang/String;
    .param p2, "emcall"    # Ljava/lang/String;
    .param p3, "heatsensing"    # Ljava/lang/String;

    .prologue
    .line 757
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 759
    .local v0, "map":Landroid/content/ContentValues;
    const-string v1, "mcc"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const-string v1, "emcall"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    const-string v1, "heatsensing"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    return-object v0
.end method


# virtual methods
.method public mappingForLauncherAddList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;
    .locals 3
    .param p1, "info"    # Lcom/sec/android/provider/emergencymode/AppInfo;
    .param p2, "upsm"    # Z

    .prologue
    .line 788
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 790
    .local v0, "map":Landroid/content/ContentValues;
    const-string v1, "package"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    const-string v1, "class"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    const-string v1, "permission"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPermission()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    const-string v2, "mode"

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 794
    return-object v0

    .line 793
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public mappingForLauncherDefaultList(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;
    .locals 3
    .param p1, "info"    # Lcom/sec/android/provider/emergencymode/AppInfo;
    .param p2, "upsm"    # Z

    .prologue
    .line 766
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 768
    .local v0, "map":Landroid/content/ContentValues;
    const-string v1, "package"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    const-string v1, "class"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const-string v1, "position"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 771
    const-string v1, "fixed"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getFixed()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 772
    const-string v2, "mode"

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 773
    return-object v0

    .line 772
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public mappingForLauncherDefaultListInfoUpdate(Lcom/sec/android/provider/emergencymode/AppInfo;Z)Landroid/content/ContentValues;
    .locals 4
    .param p1, "info"    # Lcom/sec/android/provider/emergencymode/AppInfo;
    .param p2, "upsm"    # Z

    .prologue
    const/4 v2, 0x0

    .line 777
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 779
    .local v0, "map":Landroid/content/ContentValues;
    const-string v1, "package"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    const-string v1, "class"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    const-string v3, "mode"

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 782
    const-string v1, "position"

    invoke-virtual {p1}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 783
    const-string v1, "fixed"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 784
    return-object v0

    :cond_0
    move v1, v2

    .line 781
    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 687
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "creating new longlife database"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createWhiteListTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 689
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createPrefSettingTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 690
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createDisablePkgTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 691
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createAlarmTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 692
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createEccTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 693
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createLauncherDefaultTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 694
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->createLauncherAddTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 696
    invoke-direct {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->initDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 697
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 799
    # getter for: Lcom/sec/android/provider/emergencymode/EmergencyProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/provider/emergencymode/EmergencyProvider;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onUpgrade :: DROP & CREATE"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    invoke-virtual {p0, p1}, Lcom/sec/android/provider/emergencymode/EmergencyProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 801
    return-void
.end method
