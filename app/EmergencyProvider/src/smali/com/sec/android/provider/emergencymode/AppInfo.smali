.class public Lcom/sec/android/provider/emergencymode/AppInfo;
.super Ljava/lang/Object;
.source "AppInfo.java"


# instance fields
.field private fixed:I

.field private mClassName:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPermission:Ljava/lang/String;

.field private position:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getFixed()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->fixed:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPermission()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->mPermission:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->position:I

    return v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newClass"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->mClassName:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setFixed(I)V
    .locals 0
    .param p1, "fixed"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->fixed:I

    .line 83
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newPkg"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->mPackageName:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setPermission(Ljava/lang/String;)V
    .locals 0
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->mPermission:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/provider/emergencymode/AppInfo;->position:I

    .line 51
    return-void
.end method
