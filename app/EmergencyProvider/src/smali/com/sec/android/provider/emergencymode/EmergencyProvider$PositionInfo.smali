.class public Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;
.super Ljava/lang/Object;
.source "EmergencyProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/provider/emergencymode/EmergencyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PositionInfo"
.end annotation


# instance fields
.field private mPackageName:Ljava/lang/String;

.field private mPosition:I

.field final synthetic this$0:Lcom/sec/android/provider/emergencymode/EmergencyProvider;


# direct methods
.method public constructor <init>(Lcom/sec/android/provider/emergencymode/EmergencyProvider;Ljava/lang/String;I)V
    .locals 0
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "pos"    # I

    .prologue
    .line 857
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->this$0:Lcom/sec/android/provider/emergencymode/EmergencyProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 858
    iput-object p2, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->mPackageName:Ljava/lang/String;

    .line 859
    iput p3, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->mPosition:I

    .line 860
    return-void
.end method


# virtual methods
.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 863
    iget v0, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->mPosition:I

    return v0
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newPkg"    # Ljava/lang/String;

    .prologue
    .line 875
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->mPackageName:Ljava/lang/String;

    .line 876
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 867
    iput p1, p0, Lcom/sec/android/provider/emergencymode/EmergencyProvider$PositionInfo;->mPosition:I

    .line 868
    return-void
.end method
