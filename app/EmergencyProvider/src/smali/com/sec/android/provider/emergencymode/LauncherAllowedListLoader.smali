.class public Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;
.super Ljava/lang/Object;
.source "LauncherAllowedListLoader.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->mContext:Landroid/content/Context;

    .line 71
    return-void
.end method

.method private getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;
    .locals 10
    .param p1, "cscFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, "cscFileChk":Ljava/io/File;
    const/4 v0, 0x0

    .line 76
    .local v0, "cscFile":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 78
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v5, "LauncherAllowedListLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CSC File Path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 82
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    .end local v0    # "cscFile":Ljava/io/BufferedReader;
    new-instance v5, Ljava/io/InputStreamReader;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v7, "EUCKR"

    invoke-direct {v5, v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    .restart local v0    # "cscFile":Ljava/io/BufferedReader;
    :goto_0
    if-eqz v0, :cond_0

    .line 91
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 92
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 93
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 94
    invoke-interface {v4, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 95
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1

    .line 104
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_0
    :goto_1
    return-object v4

    .line 83
    .end local v0    # "cscFile":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 84
    .local v2, "e":Ljava/io/FileNotFoundException;
    const/4 v0, 0x0

    .line 85
    .restart local v0    # "cscFile":Ljava/io/BufferedReader;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 96
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 97
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 98
    const/4 v0, 0x0

    .line 99
    const/4 v4, 0x0

    .line 100
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_1
.end method

.method private getDate(J)Ljava/lang/String;
    .locals 5
    .param p1, "datetime"    # J

    .prologue
    .line 409
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 410
    .local v1, "formatter":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 411
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 412
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 413
    .local v2, "strDate":Ljava/lang/String;
    return-object v2
.end method


# virtual methods
.method protected getFileTimeStamp(Ljava/lang/String;)J
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 399
    const-wide/16 v2, 0x0

    .line 400
    .local v2, "fileTime":J
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, "cscFileChk":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 403
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 405
    :cond_0
    return-wide v2
.end method

.method public getUpdateTimeOfAppOrCSC()J
    .locals 9

    .prologue
    .line 365
    const-wide/16 v4, 0x0

    .line 366
    .local v4, "latestTime":J
    const-wide/16 v2, 0x0

    .line 367
    .local v2, "lastUpdTimeOfProvider":J
    const/4 v1, 0x0

    .line 370
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const-string v6, "ro.product.device"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "m2alte"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "ro.product.device"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "m2a3g"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 371
    :cond_0
    const-string v6, "/system/csc/default_apps_without_torch.xml"

    invoke-virtual {p0, v6}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getFileTimeStamp(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 377
    :goto_0
    const-string v6, "/system/csc/allowed_apps.xml"

    invoke-virtual {p0, v6}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getFileTimeStamp(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 380
    const-string v6, "/system/csc/default_apps_upsm.xml"

    invoke-virtual {p0, v6}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getFileTimeStamp(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 383
    const-string v6, "/system/csc/allowed_apps_upsm.xml"

    invoke-virtual {p0, v6}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getFileTimeStamp(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 386
    iget-object v6, p0, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 388
    :try_start_0
    const-string v6, "com.sec.android.provider.emergencymode"

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-wide v2, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :goto_1
    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 393
    const-string v6, "LauncherAllowedListLoader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getUpdateTimeOfAppOrCSC time["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {p0, v4, v5}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getDate(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    return-wide v4

    .line 373
    :cond_1
    const-string v6, "/system/csc/default_apps.xml"

    invoke-virtual {p0, v6}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getFileTimeStamp(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    goto :goto_0

    .line 389
    :catch_0
    move-exception v0

    .line 390
    .local v0, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public final loadAllowedXMLIntoDatabase(Z)Ljava/util/ArrayList;
    .locals 25
    .param p1, "upsm"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/provider/emergencymode/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    const-string v22, "LauncherAllowedListLoader"

    const-string v23, "loadAllowedXMLIntoDatabase"

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v2, "appInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/4 v10, 0x0

    .line 254
    .local v10, "fileReader":Ljava/io/FileReader;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v9

    .line 255
    .local v9, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 256
    const/4 v15, 0x0

    .local v15, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v6, 0x0

    .line 257
    .local v6, "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v19, 0x0

    .line 258
    .local v19, "resParser":Landroid/content/res/XmlResourceParser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 259
    .local v17, "res":Landroid/content/res/Resources;
    const/16 v18, 0x0

    .line 261
    .local v18, "resId":I
    const-string v22, "LauncherAllowedListLoader"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "loadDefaultXMLIntoDatabase() : isUPSM = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    if-eqz p1, :cond_4

    .line 264
    const v18, 0x7f040001

    .line 265
    const-string v22, "/system/csc/allowed_apps_upsm.xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 272
    :goto_0
    if-nez v6, :cond_5

    .line 273
    const-string v22, "LauncherAllowedListLoader"

    const-string v23, "CSC is not available"

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v19

    .line 276
    move-object/from16 v15, v19

    .line 284
    :goto_1
    const-string v22, "favorites"

    move-object/from16 v0, v22

    invoke-static {v15, v0}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 285
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    .line 287
    .local v7, "depth":I
    :cond_0
    :goto_2
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v21

    .local v21, "type":I
    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v22

    move/from16 v0, v22

    if-le v0, v7, :cond_2

    :cond_1
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_2

    .line 288
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 352
    :cond_2
    if-eqz v10, :cond_3

    .line 354
    :try_start_1
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 361
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v9    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v15    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "res":Landroid/content/res/Resources;
    .end local v18    # "resId":I
    .end local v19    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "type":I
    :cond_3
    :goto_3
    return-object v2

    .line 268
    .restart local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v9    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v15    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v17    # "res":Landroid/content/res/Resources;
    .restart local v18    # "resId":I
    .restart local v19    # "resParser":Landroid/content/res/XmlResourceParser;
    :cond_4
    const/high16 v18, 0x7f040000

    .line 269
    :try_start_2
    const-string v22, "/system/csc/allowed_apps.xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    goto :goto_0

    .line 279
    :cond_5
    const-string v22, "LauncherAllowedListLoader"

    const-string v23, "CSC is available"

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    move-object v15, v6

    goto :goto_1

    .line 292
    .restart local v7    # "depth":I
    .restart local v21    # "type":I
    :cond_6
    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 296
    const/4 v14, 0x0

    .line 297
    .local v14, "packageName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 298
    .local v5, "className":Ljava/lang/String;
    const/16 v16, 0x0

    .line 300
    .local v16, "permission":Ljava/lang/String;
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 302
    .local v12, "name":Ljava/lang/String;
    if-eqz v12, :cond_0

    .line 303
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v20

    .line 305
    .local v20, "size":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_4
    move/from16 v0, v20

    if-ge v11, v0, :cond_a

    .line 306
    invoke-interface {v15, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 307
    .local v3, "attrName":Ljava/lang/String;
    invoke-interface {v15, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v4

    .line 309
    .local v4, "attrValue":Ljava/lang/String;
    if-eqz v3, :cond_7

    const-string v22, "packageName"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 310
    move-object v14, v4

    .line 313
    :cond_7
    if-eqz v3, :cond_8

    const-string v22, "className"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 314
    move-object v5, v4

    .line 317
    :cond_8
    if-eqz v3, :cond_9

    const-string v22, "permission"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 318
    move-object/from16 v16, v4

    .line 305
    :cond_9
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 322
    .end local v3    # "attrName":Ljava/lang/String;
    .end local v4    # "attrValue":Ljava/lang/String;
    :cond_a
    const-string v22, "LauncherAllowedListLoader"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Package Name: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Class name: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string v22, "appshortcut"

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 332
    new-instance v13, Lcom/sec/android/provider/emergencymode/AppInfo;

    invoke-direct {v13}, Lcom/sec/android/provider/emergencymode/AppInfo;-><init>()V

    .line 333
    .local v13, "newApp":Lcom/sec/android/provider/emergencymode/AppInfo;
    invoke-virtual {v13, v14}, Lcom/sec/android/provider/emergencymode/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 334
    invoke-virtual {v13, v5}, Lcom/sec/android/provider/emergencymode/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 335
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/provider/emergencymode/AppInfo;->setPermission(Ljava/lang/String;)V

    .line 336
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 338
    .end local v13    # "newApp":Lcom/sec/android/provider/emergencymode/AppInfo;
    :cond_b
    const/4 v14, 0x0

    .line 339
    goto/16 :goto_2

    .line 355
    .end local v5    # "className":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "name":Ljava/lang/String;
    .end local v14    # "packageName":Ljava/lang/String;
    .end local v16    # "permission":Ljava/lang/String;
    .end local v20    # "size":I
    :catch_0
    move-exception v8

    .line 356
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 342
    .end local v6    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v7    # "depth":I
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v15    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "res":Landroid/content/res/Resources;
    .end local v18    # "resId":I
    .end local v19    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "type":I
    :catch_1
    move-exception v8

    .line 343
    .local v8, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    const-string v22, "LauncherAllowedListLoader"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Got exception parsing favorites."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 352
    if-eqz v10, :cond_3

    .line 354
    :try_start_4
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_3

    .line 355
    :catch_2
    move-exception v8

    .line 356
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 345
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 346
    .restart local v8    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v22, "LauncherAllowedListLoader"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Got exception parsing favorites."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 352
    if-eqz v10, :cond_3

    .line 354
    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_3

    .line 355
    :catch_4
    move-exception v8

    .line 356
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 348
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 349
    .local v8, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_7
    const-string v22, "LauncherAllowedListLoader"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Got exception parsing favorites."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    invoke-virtual {v8}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 352
    if-eqz v10, :cond_3

    .line 354
    :try_start_8
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_3

    .line 355
    :catch_6
    move-exception v8

    .line 356
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 352
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v22

    if-eqz v10, :cond_c

    .line 354
    :try_start_9
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 357
    :cond_c
    :goto_5
    throw v22

    .line 355
    :catch_7
    move-exception v8

    .line 356
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5
.end method

.method public final loadDefaultXMLIntoDatabase(Z)Ljava/util/ArrayList;
    .locals 31
    .param p1, "upsm"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/provider/emergencymode/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    const/4 v15, 0x0

    .line 110
    .local v15, "maxPosition":I
    const-string v28, "LauncherAllowedListLoader"

    const-string v29, "loadDefaultXMLIntoDatabase"

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v3, "appInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/4 v12, 0x0

    .line 114
    .local v12, "fileReader":Ljava/io/FileReader;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v11

    .line 115
    .local v11, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 116
    const/16 v19, 0x0

    .local v19, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v7, 0x0

    .line 117
    .local v7, "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v25, 0x0

    .line 118
    .local v25, "resParser":Landroid/content/res/XmlResourceParser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 119
    .local v23, "res":Landroid/content/res/Resources;
    const/16 v24, 0x0

    .line 121
    .local v24, "resId":I
    const-string v28, "LauncherAllowedListLoader"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "loadDefaultXMLIntoDatabase() : isUPSM = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    if-eqz p1, :cond_5

    .line 124
    const v24, 0x7f040003

    .line 125
    const-string v28, "/system/csc/default_apps_upsm.xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    .line 138
    :goto_0
    if-nez v7, :cond_8

    .line 139
    const-string v28, "LauncherAllowedListLoader"

    const-string v29, "CSC is not available"

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v25

    .line 142
    move-object/from16 v19, v25

    .line 149
    :goto_1
    const-string v28, "favorites"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 150
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v8

    .line 152
    .local v8, "depth":I
    :cond_0
    :goto_2
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v27

    .local v27, "type":I
    const/16 v28, 0x3

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1

    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v28

    move/from16 v0, v28

    if-le v0, v8, :cond_2

    :cond_1
    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_2

    .line 153
    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_9

    .line 224
    :cond_2
    if-eqz v12, :cond_3

    .line 226
    :try_start_1
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 233
    .end local v7    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v8    # "depth":I
    .end local v11    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v19    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v23    # "res":Landroid/content/res/Resources;
    .end local v24    # "resId":I
    .end local v25    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v27    # "type":I
    :cond_3
    :goto_3
    const/16 v21, 0x0

    .line 234
    .local v21, "positionIndex":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v2, "appFixedInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    const/4 v13, 0x0

    .local v13, "i":I
    move/from16 v22, v21

    .end local v21    # "positionIndex":I
    .local v22, "positionIndex":I
    :goto_4
    if-gt v13, v15, :cond_12

    .line 236
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_13

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/provider/emergencymode/AppInfo;

    .line 237
    .local v10, "element":Lcom/sec/android/provider/emergencymode/AppInfo;
    invoke-virtual {v10}, Lcom/sec/android/provider/emergencymode/AppInfo;->getPosition()I

    move-result v28

    move/from16 v0, v28

    if-ne v13, v0, :cond_4

    .line 238
    add-int/lit8 v21, v22, 0x1

    .end local v22    # "positionIndex":I
    .restart local v21    # "positionIndex":I
    move/from16 v0, v22

    invoke-virtual {v10, v0}, Lcom/sec/android/provider/emergencymode/AppInfo;->setPosition(I)V

    .line 239
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    .end local v10    # "element":Lcom/sec/android/provider/emergencymode/AppInfo;
    :goto_5
    add-int/lit8 v13, v13, 0x1

    move/from16 v22, v21

    .end local v21    # "positionIndex":I
    .restart local v22    # "positionIndex":I
    goto :goto_4

    .line 128
    .end local v2    # "appFixedInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .end local v13    # "i":I
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v22    # "positionIndex":I
    .restart local v7    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v19    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v23    # "res":Landroid/content/res/Resources;
    .restart local v24    # "resId":I
    .restart local v25    # "resParser":Landroid/content/res/XmlResourceParser;
    :cond_5
    :try_start_2
    const-string v28, "ro.product.device"

    invoke-static/range {v28 .. v28}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    const-string v29, "m2alte"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-nez v28, :cond_6

    const-string v28, "ro.product.device"

    invoke-static/range {v28 .. v28}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    const-string v29, "m2a3g"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 129
    :cond_6
    const v24, 0x7f040004

    .line 130
    const-string v28, "/system/csc/default_apps_without_torch.xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    goto/16 :goto_0

    .line 133
    :cond_7
    const v24, 0x7f040002

    .line 134
    const-string v28, "/system/csc/default_apps.xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->getCscResource(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    goto/16 :goto_0

    .line 144
    :cond_8
    const-string v28, "LauncherAllowedListLoader"

    const-string v29, "CSC is available"

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v19, v7

    goto/16 :goto_1

    .line 157
    .restart local v8    # "depth":I
    .restart local v27    # "type":I
    :cond_9
    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_0

    .line 161
    const/16 v18, 0x0

    .line 162
    .local v18, "packageName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 163
    .local v6, "className":Ljava/lang/String;
    const/16 v20, 0x0

    .line 165
    .local v20, "position":I
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 167
    .local v16, "name":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 168
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v26

    .line 170
    .local v26, "size":I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_6
    move/from16 v0, v26

    if-ge v13, v0, :cond_d

    .line 171
    move-object/from16 v0, v19

    invoke-interface {v0, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    .line 172
    .local v4, "attrName":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-interface {v0, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    .line 174
    .local v5, "attrValue":Ljava/lang/String;
    if-eqz v4, :cond_a

    const-string v28, "packageName"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 175
    move-object/from16 v18, v5

    .line 178
    :cond_a
    if-eqz v4, :cond_b

    const-string v28, "className"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 179
    move-object v6, v5

    .line 182
    :cond_b
    if-eqz v4, :cond_c

    const-string v28, "iconPosition"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 183
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 170
    :cond_c
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 187
    .end local v4    # "attrName":Ljava/lang/String;
    .end local v5    # "attrValue":Ljava/lang/String;
    :cond_d
    const-string v28, "LauncherAllowedListLoader"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Package Name: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", Class name: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", IconPosition: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v28, "appshortcut"

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 197
    const-string v28, "com.android.contacts"

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x1120044

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v28

    if-eqz v28, :cond_10

    :cond_e
    const-string v28, "com.android.mms"

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/provider/emergencymode/LauncherAllowedListLoader;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x1120045

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 199
    :cond_f
    new-instance v17, Lcom/sec/android/provider/emergencymode/AppInfo;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/provider/emergencymode/AppInfo;-><init>()V

    .line 200
    .local v17, "newApp":Lcom/sec/android/provider/emergencymode/AppInfo;
    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/provider/emergencymode/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 201
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/sec/android/provider/emergencymode/AppInfo;->setClassName(Ljava/lang/String;)V

    .line 202
    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/provider/emergencymode/AppInfo;->setPosition(I)V

    .line 203
    const/16 v28, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/sec/android/provider/emergencymode/AppInfo;->setFixed(I)V

    .line 204
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 206
    move/from16 v0, v20

    if-ge v15, v0, :cond_10

    move/from16 v15, v20

    .line 209
    .end local v17    # "newApp":Lcom/sec/android/provider/emergencymode/AppInfo;
    :cond_10
    const/16 v18, 0x0

    .line 210
    const/4 v6, 0x0

    .line 211
    goto/16 :goto_2

    .line 227
    .end local v6    # "className":Ljava/lang/String;
    .end local v13    # "i":I
    .end local v16    # "name":Ljava/lang/String;
    .end local v18    # "packageName":Ljava/lang/String;
    .end local v20    # "position":I
    .end local v26    # "size":I
    :catch_0
    move-exception v9

    .line 228
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 214
    .end local v7    # "cscParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v8    # "depth":I
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v19    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v23    # "res":Landroid/content/res/Resources;
    .end local v24    # "resId":I
    .end local v25    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v27    # "type":I
    :catch_1
    move-exception v9

    .line 215
    .local v9, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    const-string v28, "LauncherAllowedListLoader"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Got exception parsing favorites."

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {v9}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 224
    if-eqz v12, :cond_3

    .line 226
    :try_start_4
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_3

    .line 227
    :catch_2
    move-exception v9

    .line 228
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 217
    .end local v9    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v9

    .line 218
    .restart local v9    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v28, "LauncherAllowedListLoader"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Got exception parsing favorites."

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 224
    if-eqz v12, :cond_3

    .line 226
    :try_start_6
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_3

    .line 227
    :catch_4
    move-exception v9

    .line 228
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 220
    .end local v9    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v9

    .line 221
    .local v9, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_7
    const-string v28, "LauncherAllowedListLoader"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Got exception parsing favorites."

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v9}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 224
    if-eqz v12, :cond_3

    .line 226
    :try_start_8
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_3

    .line 227
    :catch_6
    move-exception v9

    .line 228
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 224
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v28

    if-eqz v12, :cond_11

    .line 226
    :try_start_9
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 229
    :cond_11
    :goto_7
    throw v28

    .line 227
    :catch_7
    move-exception v9

    .line 228
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 245
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v2    # "appFixedInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/provider/emergencymode/AppInfo;>;"
    .restart local v13    # "i":I
    .restart local v22    # "positionIndex":I
    :cond_12
    return-object v2

    .restart local v14    # "i$":Ljava/util/Iterator;
    :cond_13
    move/from16 v21, v22

    .end local v22    # "positionIndex":I
    .restart local v21    # "positionIndex":I
    goto/16 :goto_5
.end method
