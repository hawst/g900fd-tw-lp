.class public abstract Lcom/dropbox/sync/android/E;
.super Ljava/io/IOException;
.source "panda.py"


# static fields
.field public static final serialVersionUID:J


# direct methods
.method public constructor <init>(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0, p1}, Lcom/dropbox/sync/android/E;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 296
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/dropbox/sync/android/E;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    return-object v0
.end method
