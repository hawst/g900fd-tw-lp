.class final Lcom/dropbox/sync/android/n;
.super Lcom/dropbox/sync/android/j;
.source "panda.py"


# instance fields
.field final synthetic c:Lcom/dropbox/sync/android/i;

.field private final d:Lcom/dropbox/sync/android/V;


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/V;)V
    .locals 4

    .prologue
    .line 107
    iput-object p1, p0, Lcom/dropbox/sync/android/n;->c:Lcom/dropbox/sync/android/i;

    .line 108
    invoke-virtual {p2}, Lcom/dropbox/sync/android/V;->d()Lcom/dropbox/sync/android/G;

    move-result-object v0

    const/4 v1, 0x5

    const/16 v2, 0xe10

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/dropbox/sync/android/j;-><init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/G;II)V

    .line 109
    iput-object p2, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    .line 110
    iget-object v0, p0, Lcom/dropbox/sync/android/n;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scheduling task to notify server of unlink of uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' token=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/sync/android/V;->c()Lcom/dropbox/sync/android/be;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    .line 117
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/n;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifying server of unlink for uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' token=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/V;->c()Lcom/dropbox/sync/android/be;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->g()Lcom/dropbox/sync/android/NativeApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeApp;->c()V

    .line 120
    iget-object v0, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->f()Lcom/dropbox/sync/android/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/a;->a(Lcom/dropbox/sync/android/V;)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    iget-object v1, p0, Lcom/dropbox/sync/android/n;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to notify server of unlink for uid=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v4}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' token=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/sync/android/n;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v4}, Lcom/dropbox/sync/android/V;->c()Lcom/dropbox/sync/android/be;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    const/4 v0, 0x0

    goto :goto_0
.end method
