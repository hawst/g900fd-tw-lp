.class final Lcom/dropbox/sync/android/aY;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/sync/android/aZ;

.field private final b:Landroid/content/Context;

.field private c:Lcom/dropbox/sync/android/ba;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/dropbox/sync/android/aZ;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/sync/android/aZ;-><init>(Lcom/dropbox/sync/android/aY;Lcom/dropbox/sync/android/aX;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/aY;->a:Lcom/dropbox/sync/android/aZ;

    .line 66
    sget-object v0, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    iput-object v0, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;

    .line 100
    iput-object p1, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    .line 101
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/dropbox/sync/android/ba;)V
    .locals 4

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 155
    :goto_0
    monitor-exit p0

    return-void

    .line 123
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/dropbox/sync/android/ba;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/ba;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/DbxSyncService;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityTracker: Binding"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/sync/android/DbxSyncService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/sync/android/aY;->a:Lcom/dropbox/sync/android/aZ;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 127
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to bind to DbxSyncService.  Did you remember to include it in the application manifest?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 130
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Lcom/dropbox/sync/android/ba;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/ba;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/DbxSyncService;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityTracker: Unbinding"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/dropbox/sync/android/aY;->a:Lcom/dropbox/sync/android/aZ;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 139
    :cond_2
    invoke-virtual {p1}, Lcom/dropbox/sync/android/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/ba;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 140
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/DbxSyncService;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityTracker: Start(+1)"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/sync/android/DbxSyncService;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_4

    .line 143
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to start DbxSyncService.  Did you remember to include it in the application manifest?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_3
    invoke-virtual {p1}, Lcom/dropbox/sync/android/ba;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/DbxSyncService;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityTracker: Start(-1)"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/dropbox/sync/android/DbxSyncService;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/dropbox/sync/android/aY;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 154
    :cond_4
    iput-object p1, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method protected final finalize()V
    .locals 4

    .prologue
    .line 105
    sget-object v0, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    iget-object v1, p0, Lcom/dropbox/sync/android/aY;->c:Lcom/dropbox/sync/android/ba;

    if-eq v0, v1, :cond_0

    .line 106
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/DbxSyncService;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "ActivityTracker destroyed when still active."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 109
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 110
    return-void
.end method
