.class public Lcom/dropbox/sync/android/ak;
.super Lcom/dropbox/sync/android/p;
.source "panda.py"


# static fields
.field public static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static final d:Lcom/dropbox/sync/android/am;


# instance fields
.field private final e:Lcom/dropbox/sync/android/NativeDatastoreManager;

.field private final f:Lcom/dropbox/sync/android/aY;

.field private final g:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/dropbox/sync/android/an;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-class v0, Lcom/dropbox/sync/android/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/ak;->c:Ljava/lang/String;

    .line 38
    const-string v0, "default"

    sput-object v0, Lcom/dropbox/sync/android/ak;->b:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/dropbox/sync/android/am;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/am;-><init>(Lcom/dropbox/sync/android/al;)V

    sput-object v0, Lcom/dropbox/sync/android/ak;->d:Lcom/dropbox/sync/android/am;

    return-void
.end method

.method private constructor <init>(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 98
    invoke-direct {p0, p1, p3}, Lcom/dropbox/sync/android/p;-><init>(Lcom/dropbox/sync/android/V;Ljava/io/File;)V

    .line 82
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/ak;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/ak;->h:Z

    .line 99
    new-instance v0, Lcom/dropbox/sync/android/aY;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->f()Lcom/dropbox/sync/android/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/sync/android/a;->e()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/aY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/ak;->f:Lcom/dropbox/sync/android/aY;

    .line 100
    new-instance v0, Lcom/dropbox/sync/android/NativeDatastoreManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/datastoresv0.db"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, p0, v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;-><init>(Lcom/dropbox/sync/android/NativeApp;Lcom/dropbox/sync/android/ak;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/ak;->e:Lcom/dropbox/sync/android/NativeDatastoreManager;

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/ak;->h:Z

    .line 102
    invoke-virtual {p0}, Lcom/dropbox/sync/android/ak;->c()V

    .line 103
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;Lcom/dropbox/sync/android/al;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/ak;-><init>(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method final a(Z)V
    .locals 4

    .prologue
    .line 141
    monitor-enter p0

    .line 142
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/ak;->h:Z

    if-nez v0, :cond_0

    .line 143
    monitor-exit p0

    .line 155
    :goto_0
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/ak;->h:Z

    .line 146
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->a:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->d()Lcom/dropbox/sync/android/G;

    move-result-object v0

    sget-object v1, Lcom/dropbox/sync/android/ak;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing DbxDatastoreManager for uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/sync/android/ak;->a:Lcom/dropbox/sync/android/V;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->e:Lcom/dropbox/sync/android/NativeDatastoreManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->b(Z)V

    .line 148
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 149
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->e:Lcom/dropbox/sync/android/NativeDatastoreManager;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->a(Z)V

    .line 150
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->a:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0, p0}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/p;)V

    .line 151
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->f:Lcom/dropbox/sync/android/aY;

    sget-object v1, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aY;->a(Lcom/dropbox/sync/android/ba;)V

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final b()Lcom/dropbox/sync/android/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/sync/android/q",
            "<+",
            "Lcom/dropbox/sync/android/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345
    sget-object v0, Lcom/dropbox/sync/android/ak;->d:Lcom/dropbox/sync/android/am;

    return-object v0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->f:Lcom/dropbox/sync/android/aY;

    iget-object v1, p0, Lcom/dropbox/sync/android/ak;->e:Lcom/dropbox/sync/android/NativeDatastoreManager;

    invoke-virtual {v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->a()Lcom/dropbox/sync/android/ba;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aY;->a(Lcom/dropbox/sync/android/ba;)V

    .line 325
    return-void
.end method

.method final d()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/dropbox/sync/android/ak;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 333
    new-instance v1, Lcom/dropbox/sync/android/al;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/sync/android/al;-><init>(Lcom/dropbox/sync/android/ak;Ljava/util/Iterator;)V

    invoke-static {v1}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/Runnable;)V

    .line 341
    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/ak;->a(Z)V

    .line 108
    return-void
.end method
