.class Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/dropbox/sync/android/DbxFileInfo;

.field private e:Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;


# direct methods
.method private constructor <init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;)V
    .locals 1

    .prologue
    .line 216
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->b:Ljava/util/ArrayList;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->c:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;)V

    return-void
.end method


# virtual methods
.method public addInvitee(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 229
    new-instance v0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 231
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    return-void
.end method

.method public addMetadata(JZJJJZLjava/lang/String;)V
    .locals 8
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 247
    if-eqz p3, :cond_0

    .line 248
    :goto_0
    new-instance v0, Lcom/dropbox/sync/android/DbxFileInfo;

    new-instance v1, Lcom/dropbox/sync/android/aT;

    invoke-direct {v1, p1, p2}, Lcom/dropbox/sync/android/aT;-><init>(J)V

    new-instance v5, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p6

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    move v2, p3

    move-wide v3, p4

    move/from16 v6, p10

    move-object/from16 v7, p11

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/sync/android/DbxFileInfo;-><init>(Lcom/dropbox/sync/android/aT;ZJLjava/util/Date;ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->d:Lcom/dropbox/sync/android/DbxFileInfo;

    .line 250
    return-void

    :cond_0
    move-wide/from16 p6, p8

    .line 247
    goto :goto_0
.end method

.method public addUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    invoke-direct {v0, p1, p2, p3, p5}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 238
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    if-eqz p4, :cond_0

    .line 240
    iput-object v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->e:Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    .line 242
    :cond_0
    return-void
.end method

.method public createSharedFolderInfo(Ljava/lang/String;ZZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 9
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 255
    new-instance v0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    iget-object v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->c:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->e:Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    iget-object v6, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;->d:Lcom/dropbox/sync/android/DbxFileInfo;

    move v4, p2

    move v5, p3

    move-object v7, p1

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;-><init>(Ljava/util/List;Ljava/util/List;Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;ZZLcom/dropbox/sync/android/DbxFileInfo;Ljava/lang/String;Z)V

    return-object v0
.end method
