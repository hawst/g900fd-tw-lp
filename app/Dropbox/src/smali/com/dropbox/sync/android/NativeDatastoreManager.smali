.class Lcom/dropbox/sync/android/NativeDatastoreManager;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/dropbox/sync/android/NativeApp;

.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/sync/android/ak;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/dropbox/sync/android/G;

.field private final f:J

.field private final g:Lcom/dropbox/sync/android/NativeThreads;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/dropbox/sync/android/NativeDatastoreManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/NativeDatastoreManager;->a:Z

    .line 19
    const-class v0, Lcom/dropbox/sync/android/NativeDatastoreManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeDatastoreManager;->b:Ljava/lang/String;

    .line 219
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 220
    invoke-static {}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeClassInit()V

    .line 221
    return-void

    .line 18
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/dropbox/sync/android/NativeApp;Lcom/dropbox/sync/android/ak;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "App must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->c:Lcom/dropbox/sync/android/NativeApp;

    .line 42
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Owner must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->d:Ljava/lang/ref/WeakReference;

    .line 44
    new-instance v0, Lcom/dropbox/sync/android/G;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/NativeApp;->b()Lcom/dropbox/sync/android/NativeEnv;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    .line 47
    invoke-virtual {p1}, Lcom/dropbox/sync/android/NativeApp;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p3}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeInit(JLjava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    .line 48
    sget-boolean v0, Lcom/dropbox/sync/android/NativeDatastoreManager;->a:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid native DatastoreManager handle."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 50
    :cond_2
    new-instance v0, Lcom/dropbox/sync/android/NativeThreads;

    const-string v1, "NativeDatastoreManager"

    invoke-static {}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeGetRunFuncs()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    iget-object v6, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/NativeThreads;-><init>(Ljava/lang/String;JJLcom/dropbox/sync/android/G;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->h:Z

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->a()V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->a(Z)V

    .line 57
    throw v0
.end method

.method private static addStringToSet(Ljava/util/Set;Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 214
    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 215
    return-void
.end method

.method private listCallback()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ak;

    .line 188
    if-eqz v0, :cond_0

    .line 190
    :try_start_0
    invoke-virtual {v0}, Lcom/dropbox/sync/android/ak;->d()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeDatastoreManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :catch_1
    move-exception v0

    .line 194
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeDatastoreManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static native nativeClassInit()V
.end method

.method private static native nativeFree(J)V
.end method

.method private static native nativeGetRunFuncs()J
.end method

.method private static native nativeGetSyncStatus(J)I
.end method

.method private native nativeInit(JLjava/lang/String;)J
.end method

.method private static native nativeSetOrClearListCallback(JZ)V
.end method

.method private static native nativeShutDown(J)V
.end method

.method private statusCallback()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ak;

    .line 171
    if-eqz v0, :cond_0

    .line 173
    :try_start_0
    invoke-virtual {v0}, Lcom/dropbox/sync/android/ak;->c()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 175
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeDatastoreManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :catch_1
    move-exception v0

    .line 177
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeDatastoreManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/dropbox/sync/android/ba;
    .locals 2

    .prologue
    .line 155
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    invoke-static {v0, v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeGetSyncStatus(J)I

    move-result v0

    .line 156
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_0

    .line 157
    sget-object v0, Lcom/dropbox/sync/android/ba;->a:Lcom/dropbox/sync/android/ba;

    .line 161
    :goto_0
    return-object v0

    .line 158
    :cond_0
    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    .line 159
    sget-object v0, Lcom/dropbox/sync/android/ba;->b:Lcom/dropbox/sync/android/ba;

    goto :goto_0

    .line 161
    :cond_1
    sget-object v0, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 91
    monitor-enter p0

    .line 92
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->h:Z

    if-nez v0, :cond_0

    .line 93
    monitor-exit p0

    .line 101
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->h:Z

    .line 96
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->b()V

    .line 100
    :cond_1
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    invoke-static {v0, v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeShutDown(J)V

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    invoke-static {v0, v1, p1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeSetOrClearListCallback(JZ)V

    .line 149
    return-void
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->h:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->e:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeDatastoreManager;->b:Ljava/lang/String;

    const-string v2, "NativeDatastoreManager finalized without being deinitialized."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->c()V

    .line 81
    :cond_1
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeDatastoreManager;->f:J

    invoke-static {v0, v1}, Lcom/dropbox/sync/android/NativeDatastoreManager;->nativeFree(J)V

    goto :goto_0
.end method
