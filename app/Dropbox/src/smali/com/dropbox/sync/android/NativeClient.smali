.class Lcom/dropbox/sync/android/NativeClient;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:I

.field public static f:I

.field public static g:I

.field public static h:I

.field public static i:I

.field public static j:I

.field static final synthetic k:Z

.field private static final l:Ljava/lang/String;


# instance fields
.field private final m:Lcom/dropbox/sync/android/G;

.field private final n:J

.field private final o:Lcom/dropbox/sync/android/NativeThreads;

.field private p:Lcom/dropbox/sync/android/bl;

.field private final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/dropbox/sync/android/bk;",
            "Lcom/dropbox/sync/android/bj;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/dropbox/sync/android/bi;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x40

    const/4 v1, 0x0

    .line 18
    const-class v0, Lcom/dropbox/sync/android/NativeClient;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/NativeClient;->k:Z

    .line 19
    const-class v0, Lcom/dropbox/sync/android/NativeClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    .line 55
    const/4 v0, 0x3

    sput v0, Lcom/dropbox/sync/android/NativeClient;->a:I

    .line 60
    const/16 v0, 0x8

    sput v0, Lcom/dropbox/sync/android/NativeClient;->b:I

    .line 65
    const/16 v0, 0x10

    sput v0, Lcom/dropbox/sync/android/NativeClient;->c:I

    .line 70
    const/16 v0, 0x18

    sput v0, Lcom/dropbox/sync/android/NativeClient;->d:I

    .line 75
    const/16 v0, 0x20

    sput v0, Lcom/dropbox/sync/android/NativeClient;->e:I

    .line 80
    const/16 v0, 0x28

    sput v0, Lcom/dropbox/sync/android/NativeClient;->f:I

    .line 86
    const/16 v0, 0x38

    sput v0, Lcom/dropbox/sync/android/NativeClient;->g:I

    .line 91
    sput v1, Lcom/dropbox/sync/android/NativeClient;->h:I

    .line 96
    sput v2, Lcom/dropbox/sync/android/NativeClient;->i:I

    .line 102
    sput v2, Lcom/dropbox/sync/android/NativeClient;->j:I

    .line 898
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 899
    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->nativeClassInit()V

    .line 900
    return-void

    :cond_0
    move v0, v1

    .line 18
    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    return-object v0
.end method

.method private fileCallback(J)V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 853
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 854
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->r:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/bi;

    .line 855
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 856
    if-eqz v0, :cond_0

    .line 857
    :try_start_2
    invoke-interface {v0}, Lcom/dropbox/sync/android/bi;->a()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 855
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1

    .line 859
    :catch_0
    move-exception v0

    .line 860
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 861
    :catch_1
    move-exception v0

    .line 862
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeFree(J)V
.end method

.method private pathCallback(JI)V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 832
    :try_start_0
    new-instance v0, Lcom/dropbox/sync/android/aT;

    invoke-direct {v0, p1, p2}, Lcom/dropbox/sync/android/aT;-><init>(J)V

    .line 833
    new-instance v1, Lcom/dropbox/sync/android/bk;

    invoke-direct {v1, v0, p3}, Lcom/dropbox/sync/android/bk;-><init>(Lcom/dropbox/sync/android/aT;I)V

    .line 835
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 836
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->q:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/bj;

    .line 837
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 838
    if-eqz v0, :cond_0

    .line 839
    :try_start_2
    invoke-interface {v0, v1}, Lcom/dropbox/sync/android/bj;->a(Lcom/dropbox/sync/android/bk;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 837
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1

    .line 841
    :catch_0
    move-exception v0

    .line 842
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 843
    :catch_1
    move-exception v0

    .line 844
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncStatusCallback()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 815
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 816
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->p:Lcom/dropbox/sync/android/bl;

    .line 817
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 818
    if-eqz v0, :cond_0

    .line 819
    :try_start_2
    invoke-interface {v0}, Lcom/dropbox/sync/android/bl;->a()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 817
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1

    .line 821
    :catch_0
    move-exception v0

    .line 822
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 823
    :catch_1
    move-exception v0

    .line 824
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 3

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeClient;->s:Z

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->m:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeClient;->l:Ljava/lang/String;

    const-string v2, "NativeClient finalized without being deinitialized."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->o:Lcom/dropbox/sync/android/NativeThreads;

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient;->o:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->c()V

    .line 338
    :cond_1
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeClient;->n:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeClient;->nativeFree(J)V

    goto :goto_0
.end method
