.class public final Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->a:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->b:Ljava/lang/String;

    .line 35
    iput-boolean p3, p0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->c:Z

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->c:Z

    return v0
.end method
