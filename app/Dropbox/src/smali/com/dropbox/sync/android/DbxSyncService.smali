.class public Lcom/dropbox/sync/android/DbxSyncService;
.super Landroid/app/Service;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/sync/android/G;

.field private final c:Lcom/dropbox/sync/android/bb;

.field private final d:Lcom/dropbox/sync/android/bc;

.field private e:I

.field private f:I

.field private g:Z

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dropbox/sync/android/DbxSyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 28
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    .line 29
    new-instance v0, Lcom/dropbox/sync/android/bb;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/bb;-><init>(Lcom/dropbox/sync/android/DbxSyncService;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->c:Lcom/dropbox/sync/android/bb;

    .line 30
    new-instance v0, Lcom/dropbox/sync/android/bc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/sync/android/bc;-><init>(Lcom/dropbox/sync/android/DbxSyncService;Lcom/dropbox/sync/android/aX;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->d:Lcom/dropbox/sync/android/bc;

    .line 35
    iput v2, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    .line 36
    iput v2, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    .line 37
    iput-boolean v2, p0, Lcom/dropbox/sync/android/DbxSyncService;->g:Z

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->h:I

    .line 276
    return-void
.end method

.method private a(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    .line 226
    iput p3, p0, Lcom/dropbox/sync/android/DbxSyncService;->h:I

    .line 231
    if-eqz p1, :cond_0

    and-int/lit8 v0, p2, 0x3

    if-nez v0, :cond_0

    .line 233
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->g:Z

    .line 234
    const-string v0, "start-count"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 235
    iget v1, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    .line 236
    iget-object v1, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DbxSyncService.handleStartCommand("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): \tbc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \tsc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget v1, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    if-gez v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid start count "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " after handleStartCommand("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 244
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/sync/android/DbxSyncService;->b()V

    .line 248
    const/4 v0, 0x2

    return v0
.end method

.method static synthetic a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 22
    invoke-static {p0}, Lcom/dropbox/sync/android/DbxSyncService;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/dropbox/sync/android/DbxSyncService;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/sync/android/DbxSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private static b(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 271
    invoke-static {p0}, Lcom/dropbox/sync/android/DbxSyncService;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 272
    const-string v1, "start-count"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 273
    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->g:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    if-nez v0, :cond_0

    .line 260
    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->h:I

    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/DbxSyncService;->stopSelfResult(I)Z

    move-result v0

    .line 261
    iget-object v1, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DbxSyncService.stopSelfResult("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dropbox/sync/android/DbxSyncService;->h:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->g:Z

    .line 264
    :cond_0
    return-void

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    .prologue
    .line 188
    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    .line 189
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DbxSyncService.onBind: \t\t\t\t\tbc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \tsc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->c:Lcom/dropbox/sync/android/bb;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 172
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 173
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    const-string v2, "DbxSyncService starting."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->d:Lcom/dropbox/sync/android/bc;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/bc;->a()V

    .line 176
    invoke-static {}, Lcom/dropbox/sync/android/I;->a()Lcom/dropbox/sync/android/I;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/dropbox/sync/android/I;->a(Landroid/content/Context;)V

    .line 177
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    const-string v2, "DbxSyncService stopping."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->d:Lcom/dropbox/sync/android/bc;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/bc;->b()V

    .line 183
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 184
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 195
    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    .line 196
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DbxSyncService.onRebind: \t\t\t\tbc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \tsc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DbxSyncService.onStartCommand("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", no-flags, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/dropbox/sync/android/DbxSyncService;->a(Landroid/content/Intent;II)I

    .line 216
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DbxSyncService.onStartCommand("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/DbxSyncService;->a(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    .line 201
    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    .line 202
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DbxSyncService.onUnbind: \t\t\t\tbc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \tsc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/sync/android/DbxSyncService;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    if-gez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSyncService;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/DbxSyncService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid bind count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dropbox/sync/android/DbxSyncService;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " after onUnbind()."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 206
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/sync/android/DbxSyncService;->b()V

    .line 209
    const/4 v0, 0x1

    return v0
.end method
