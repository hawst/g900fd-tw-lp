.class Lcom/dropbox/sync/android/NativeClient$SyncStatusBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/NativeClient;


# virtual methods
.method public createStatus(ZZILjava/lang/String;ZILjava/lang/String;ZILjava/lang/String;)Lcom/dropbox/sync/android/DbxSyncStatus;
    .locals 5
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 964
    :try_start_0
    new-instance v2, Lcom/dropbox/sync/android/DbxSyncStatus;

    new-instance v3, Lcom/dropbox/sync/android/bd;

    if-nez p3, :cond_0

    move-object v1, v0

    :goto_0
    invoke-direct {v3, p2, v1}, Lcom/dropbox/sync/android/bd;-><init>(ZLcom/dropbox/sync/android/DbxException;)V

    new-instance v4, Lcom/dropbox/sync/android/bd;

    if-nez p6, :cond_1

    move-object v1, v0

    :goto_1
    invoke-direct {v4, p5, v1}, Lcom/dropbox/sync/android/bd;-><init>(ZLcom/dropbox/sync/android/DbxException;)V

    new-instance v1, Lcom/dropbox/sync/android/bd;

    if-nez p9, :cond_2

    :goto_2
    invoke-direct {v1, p8, v0}, Lcom/dropbox/sync/android/bd;-><init>(ZLcom/dropbox/sync/android/DbxException;)V

    invoke-direct {v2, p1, v3, v4, v1}, Lcom/dropbox/sync/android/DbxSyncStatus;-><init>(ZLcom/dropbox/sync/android/bd;Lcom/dropbox/sync/android/bd;Lcom/dropbox/sync/android/bd;)V

    return-object v2

    :cond_0
    const-string v1, "metadata sync status"

    invoke-static {v1, p3, p4}, Lcom/dropbox/sync/android/DbxException;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v1, "upload status"

    invoke-static {v1, p6, p7}, Lcom/dropbox/sync/android/DbxException;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;

    move-result-object v1

    goto :goto_1

    :cond_2
    const-string v0, "download status"

    invoke-static {v0, p9, p10}, Lcom/dropbox/sync/android/DbxException;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_2

    .line 974
    :catch_0
    move-exception v0

    .line 975
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$SyncStatusBuilder;->a:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 976
    throw v0

    .line 977
    :catch_1
    move-exception v0

    .line 978
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$SyncStatusBuilder;->a:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 979
    throw v0
.end method
