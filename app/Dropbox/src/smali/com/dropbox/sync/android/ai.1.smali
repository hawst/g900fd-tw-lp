.class public final Lcom/dropbox/sync/android/ai;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/sync/android/NativeContactManager;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/V;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/dropbox/sync/android/aH;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/aH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    new-instance v0, Lcom/dropbox/sync/android/NativeContactManager;

    invoke-direct {v0, p1}, Lcom/dropbox/sync/android/NativeContactManager;-><init>(Lcom/dropbox/sync/android/V;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/ai;->a:Lcom/dropbox/sync/android/NativeContactManager;

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/ai;->b:Z

    .line 42
    new-instance v0, Lcom/dropbox/sync/android/aj;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/aj;-><init>(Lcom/dropbox/sync/android/ai;)V

    invoke-virtual {p1, v0}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/Z;)V

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/dropbox/sync/android/ai;)Lcom/dropbox/sync/android/NativeContactManager;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/sync/android/ai;->a:Lcom/dropbox/sync/android/NativeContactManager;

    return-object v0
.end method

.method public static a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/ai;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/dropbox/sync/android/ai;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/ai;-><init>(Lcom/dropbox/sync/android/V;)V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/ai;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/dropbox/sync/android/ai;->b:Z

    return p1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dropbox/sync/android/ai;->a:Lcom/dropbox/sync/android/NativeContactManager;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeContactManager;->a(Ljava/lang/String;)[Lcom/dropbox/sync/android/DbxContact;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/dropbox/sync/android/ai;->a:Lcom/dropbox/sync/android/NativeContactManager;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/dropbox/sync/android/DbxContact;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/DbxContact;

    invoke-virtual {v1, v0}, Lcom/dropbox/sync/android/NativeContactManager;->a([Lcom/dropbox/sync/android/DbxContact;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
