.class public Lcom/dropbox/sync/android/V;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/dropbox/sync/android/a;

.field private final d:Lcom/dropbox/sync/android/r;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/dropbox/sync/android/be;

.field private final g:Lcom/dropbox/sync/android/NativeApp;

.field private final h:Lcom/dropbox/sync/android/NativeEnv;

.field private final i:Lcom/dropbox/sync/android/G;

.field private j:Z

.field private k:Z

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/dropbox/sync/android/p;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/dropbox/sync/android/Z;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/dropbox/sync/android/DbxAccountInfo;

.field private o:J

.field private p:Z

.field private q:Lcom/dropbox/sync/android/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/dropbox/sync/android/V;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/V;->a:Z

    .line 23
    const-class v0, Lcom/dropbox/sync/android/V;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    return-void

    .line 21
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/dropbox/sync/android/a;Lcom/dropbox/sync/android/r;Ljava/lang/String;Lcom/dropbox/sync/android/be;Lcom/dropbox/sync/android/DbxAccountInfo;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->l:Ljava/util/Map;

    .line 40
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->m:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 41
    iput-object v2, p0, Lcom/dropbox/sync/android/V;->n:Lcom/dropbox/sync/android/DbxAccountInfo;

    .line 42
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/sync/android/V;->o:J

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/V;->p:Z

    .line 44
    iput-object v2, p0, Lcom/dropbox/sync/android/V;->q:Lcom/dropbox/sync/android/J;

    .line 82
    sget-boolean v0, Lcom/dropbox/sync/android/V;->a:Z

    if-nez v0, :cond_0

    invoke-static {p2, p4}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/be;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_0
    iput-object p1, p0, Lcom/dropbox/sync/android/V;->c:Lcom/dropbox/sync/android/a;

    .line 84
    iput-object p2, p0, Lcom/dropbox/sync/android/V;->d:Lcom/dropbox/sync/android/r;

    .line 85
    iput-object p3, p0, Lcom/dropbox/sync/android/V;->e:Ljava/lang/String;

    .line 86
    iput-object p4, p0, Lcom/dropbox/sync/android/V;->f:Lcom/dropbox/sync/android/be;

    .line 87
    iput-boolean p6, p0, Lcom/dropbox/sync/android/V;->j:Z

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/V;->k:Z

    .line 89
    iput-object p5, p0, Lcom/dropbox/sync/android/V;->n:Lcom/dropbox/sync/android/DbxAccountInfo;

    .line 90
    invoke-virtual {p1}, Lcom/dropbox/sync/android/a;->f()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/sync/android/a;->d()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0, p2, v0, v1}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/NativeLib;Ljava/io/File;)Lcom/dropbox/sync/android/NativeEnv;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->h:Lcom/dropbox/sync/android/NativeEnv;

    .line 91
    new-instance v0, Lcom/dropbox/sync/android/G;

    iget-object v1, p0, Lcom/dropbox/sync/android/V;->h:Lcom/dropbox/sync/android/NativeEnv;

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    .line 92
    invoke-virtual {p1}, Lcom/dropbox/sync/android/a;->f()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v4

    invoke-virtual {p1}, Lcom/dropbox/sync/android/a;->d()Ljava/io/File;

    move-result-object v5

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/r;Ljava/lang/String;Lcom/dropbox/sync/android/be;Lcom/dropbox/sync/android/NativeLib;Ljava/io/File;)Lcom/dropbox/sync/android/NativeApp;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->g:Lcom/dropbox/sync/android/NativeApp;

    .line 94
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dropbox user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " linked."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-static {p3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;)V

    .line 100
    iget-boolean v0, p2, Lcom/dropbox/sync/android/r;->g:Z

    if-eqz v0, :cond_1

    .line 102
    invoke-static {}, Lcom/dropbox/sync/android/i;->a()Lcom/dropbox/sync/android/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/dropbox/sync/android/i;->c(Lcom/dropbox/sync/android/V;)V

    .line 109
    :cond_1
    :goto_0
    monitor-enter p0

    .line 110
    :try_start_0
    new-instance v0, Lcom/dropbox/sync/android/W;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/W;-><init>(Lcom/dropbox/sync/android/V;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->q:Lcom/dropbox/sync/android/J;

    .line 116
    invoke-static {}, Lcom/dropbox/sync/android/I;->a()Lcom/dropbox/sync/android/I;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/dropbox/sync/android/V;->q:Lcom/dropbox/sync/android/J;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/I;->a(Lcom/dropbox/sync/android/J;)V

    .line 118
    invoke-virtual {v0}, Lcom/dropbox/sync/android/I;->b()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/V;->c(Z)V

    .line 119
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    return-void

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unlinked dropbox user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " created."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/V;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/V;->c(Z)V

    return-void
.end method

.method private a(Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lcom/dropbox/sync/android/Z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403
    new-instance v0, Lcom/dropbox/sync/android/Y;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/sync/android/Y;-><init>(Lcom/dropbox/sync/android/V;Ljava/util/Iterator;)V

    invoke-static {v0}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/Runnable;)V

    .line 411
    return-void
.end method

.method static a(Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/be;)Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    iget-object v0, v0, Lcom/dropbox/sync/android/ag;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/dropbox/sync/android/V;->a(Ljava/lang/String;Lcom/dropbox/sync/android/be;)Z

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/String;Lcom/dropbox/sync/android/be;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 123
    instance-of v1, p1, Lcom/dropbox/sync/android/bf;

    if-eqz v1, :cond_2

    .line 124
    if-eqz p0, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_2
    instance-of v1, p1, Lcom/dropbox/sync/android/bg;

    if-nez v1, :cond_0

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unexpected token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    monitor-enter p0

    .line 344
    :try_start_0
    iget-boolean v1, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-eqz v1, :cond_0

    .line 345
    iget-object v1, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/dropbox/sync/android/V;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " unlinked ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_2

    const-string v0, "locally"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    .line 348
    iput-boolean p1, p0, Lcom/dropbox/sync/android/V;->k:Z

    .line 350
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->m:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 351
    iget-object v1, p0, Lcom/dropbox/sync/android/V;->m:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 353
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/dropbox/sync/android/V;->a(Z)V

    .line 359
    if-eqz v0, :cond_1

    .line 360
    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/V;->a(Ljava/util/Iterator;)V

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->c:Lcom/dropbox/sync/android/a;

    invoke-virtual {v0, p0, p1}, Lcom/dropbox/sync/android/a;->a(Lcom/dropbox/sync/android/V;Z)V

    .line 365
    return-void

    .line 345
    :cond_2
    :try_start_1
    const-string v0, "remotely"

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized c(Z)V
    .locals 5

    .prologue
    .line 545
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->h:Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeEnv;->b(Z)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    :goto_0
    monitor-exit p0

    return-void

    .line 546
    :catch_0
    move-exception v0

    .line 547
    :try_start_1
    iget-object v1, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to set network status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/sync/android/G;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 2

    .prologue
    .line 538
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-nez v0, :cond_1

    .line 539
    new-instance v1, Lcom/dropbox/sync/android/aH;

    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->k:Z

    if-eqz v0, :cond_0

    const-string v0, "Account unlinked."

    :goto_0
    invoke-direct {v1, v0}, Lcom/dropbox/sync/android/aH;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 539
    :cond_0
    :try_start_1
    const-string v0, "Account unlinked from server."
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 541
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method final a(Lcom/dropbox/sync/android/r;Ljava/lang/String;Lcom/dropbox/sync/android/be;Lcom/dropbox/sync/android/NativeLib;Ljava/io/File;)Lcom/dropbox/sync/android/NativeApp;
    .locals 6

    .prologue
    .line 142
    sget-boolean v0, Lcom/dropbox/sync/android/V;->a:Z

    if-nez v0, :cond_0

    invoke-static {p1, p3}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/be;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 143
    :cond_0
    new-instance v0, Lcom/dropbox/sync/android/NativeApp;

    iget-object v2, p0, Lcom/dropbox/sync/android/V;->h:Lcom/dropbox/sync/android/NativeEnv;

    new-instance v5, Lcom/dropbox/sync/android/X;

    invoke-direct {v5, p0}, Lcom/dropbox/sync/android/X;-><init>(Lcom/dropbox/sync/android/V;)V

    move-object v1, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeApp;-><init>(Lcom/dropbox/sync/android/NativeLib;Lcom/dropbox/sync/android/NativeEnv;Ljava/lang/String;Lcom/dropbox/sync/android/be;Lcom/dropbox/sync/android/bh;)V

    return-object v0
.end method

.method final a(Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/NativeLib;Ljava/io/File;)Lcom/dropbox/sync/android/NativeEnv;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/dropbox/sync/android/NativeEnv;

    invoke-direct {v0, p2, p1, p3}, Lcom/dropbox/sync/android/NativeEnv;-><init>(Lcom/dropbox/sync/android/NativeLib;Lcom/dropbox/sync/android/r;Ljava/io/File;)V

    return-object v0
.end method

.method final declared-synchronized a(Lcom/dropbox/sync/android/q;)Lcom/dropbox/sync/android/p;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dropbox/sync/android/p;",
            ">(",
            "Lcom/dropbox/sync/android/q",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/dropbox/sync/android/V;->k()V

    .line 268
    invoke-virtual {p1}, Lcom/dropbox/sync/android/q;->b()Ljava/lang/String;

    move-result-object v1

    .line 271
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    if-nez v0, :cond_0

    .line 280
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->c:Lcom/dropbox/sync/android/a;

    iget-object v2, p0, Lcom/dropbox/sync/android/V;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/q;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/sync/android/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 281
    invoke-static {v0}, Lcom/dropbox/sync/android/t;->b(Ljava/io/File;)V

    .line 282
    iget-object v2, p0, Lcom/dropbox/sync/android/V;->g:Lcom/dropbox/sync/android/NativeApp;

    invoke-virtual {p1, p0, v2, v0}, Lcom/dropbox/sync/android/q;->a(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)Lcom/dropbox/sync/android/p;

    move-result-object v0

    .line 283
    iget-object v2, p0, Lcom/dropbox/sync/android/V;->l:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    :cond_0
    monitor-exit p0

    return-object v0

    .line 284
    :catch_0
    move-exception v0

    .line 288
    :try_start_2
    iget-object v1, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Client initialization failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 157
    monitor-enter p0

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->q:Lcom/dropbox/sync/android/J;

    if-eqz v0, :cond_0

    .line 159
    invoke-static {}, Lcom/dropbox/sync/android/I;->a()Lcom/dropbox/sync/android/I;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/V;->q:Lcom/dropbox/sync/android/J;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/I;->b(Lcom/dropbox/sync/android/J;)V

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/V;->q:Lcom/dropbox/sync/android/J;

    .line 162
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 163
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/V;->a(Z)V

    .line 166
    iget-object v1, p0, Lcom/dropbox/sync/android/V;->g:Lcom/dropbox/sync/android/NativeApp;

    invoke-virtual {v1, v0}, Lcom/dropbox/sync/android/NativeApp;->a(Z)V

    .line 167
    return-void

    .line 162
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/dropbox/sync/android/Z;)V
    .locals 1

    .prologue
    .line 388
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 389
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->m:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_0
    monitor-exit p0

    return-void

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/dropbox/sync/android/p;)V
    .locals 2

    .prologue
    .line 368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->l:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/p;->b()Lcom/dropbox/sync/android/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/sync/android/q;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-nez v0, :cond_0

    .line 373
    invoke-virtual {p1}, Lcom/dropbox/sync/android/p;->a()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/t;->a(Ljava/io/File;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :cond_0
    monitor-exit p0

    return-void

    .line 368
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Z)V
    .locals 2

    .prologue
    .line 307
    monitor-enter p0

    .line 308
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/sync/android/V;->l:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 309
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/p;

    .line 314
    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/p;->a(Z)V

    goto :goto_0

    .line 309
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 316
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->e:Ljava/lang/String;

    return-object v0
.end method

.method final c()Lcom/dropbox/sync/android/be;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->f:Lcom/dropbox/sync/android/be;

    return-object v0
.end method

.method final d()Lcom/dropbox/sync/android/G;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    return-object v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 219
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final f()Lcom/dropbox/sync/android/a;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->c:Lcom/dropbox/sync/android/a;

    return-object v0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->g:Lcom/dropbox/sync/android/NativeApp;

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/dropbox/sync/android/V;->a()V

    .line 178
    :cond_0
    return-void
.end method

.method final g()Lcom/dropbox/sync/android/NativeApp;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->g:Lcom/dropbox/sync/android/NativeApp;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/V;->b(Z)V

    .line 329
    return-void
.end method

.method final i()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/V;->b(Z)V

    .line 338
    return-void
.end method

.method final j()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 420
    .line 422
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->g:Lcom/dropbox/sync/android/NativeApp;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeApp;->d()Lcom/dropbox/sync/android/DbxAccountInfo;
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 430
    :goto_0
    monitor-enter p0

    .line 431
    if-eqz v0, :cond_2

    .line 432
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/dropbox/sync/android/V;->o:J

    .line 433
    iget-object v4, p0, Lcom/dropbox/sync/android/V;->n:Lcom/dropbox/sync/android/DbxAccountInfo;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/dropbox/sync/android/V;->n:Lcom/dropbox/sync/android/DbxAccountInfo;

    invoke-virtual {v4, v0}, Lcom/dropbox/sync/android/DbxAccountInfo;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 435
    :cond_0
    iput-object v0, p0, Lcom/dropbox/sync/android/V;->n:Lcom/dropbox/sync/android/DbxAccountInfo;

    .line 437
    iget-object v0, p0, Lcom/dropbox/sync/android/V;->m:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v0, v3

    .line 442
    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    if-eqz v1, :cond_1

    .line 447
    iget-object v4, p0, Lcom/dropbox/sync/android/V;->c:Lcom/dropbox/sync/android/a;

    invoke-virtual {v4, p0}, Lcom/dropbox/sync/android/a;->b(Lcom/dropbox/sync/android/V;)V

    .line 450
    invoke-direct {p0, v1}, Lcom/dropbox/sync/android/V;->a(Ljava/util/Iterator;)V

    .line 456
    :cond_1
    monitor-enter p0

    .line 457
    :try_start_2
    iput-boolean v0, p0, Lcom/dropbox/sync/android/V;->p:Z

    .line 458
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 460
    if-nez v0, :cond_4

    :goto_2
    return v2

    .line 423
    :catch_0
    move-exception v0

    .line 424
    iget-object v4, p0, Lcom/dropbox/sync/android/V;->i:Lcom/dropbox/sync/android/G;

    sget-object v5, Lcom/dropbox/sync/android/V;->b:Ljava/lang/String;

    const-string v6, "Failed to update account info."

    invoke-virtual {v4, v5, v6, v0}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 440
    :cond_2
    :try_start_3
    iget-boolean v0, p0, Lcom/dropbox/sync/android/V;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/dropbox/sync/android/V;->n:Lcom/dropbox/sync/android/DbxAccountInfo;

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_1

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 458
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_4
    move v2, v3

    .line 460
    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_1
.end method
