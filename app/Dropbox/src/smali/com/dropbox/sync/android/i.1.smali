.class Lcom/dropbox/sync/android/i;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/Object;

.field private static d:Lcom/dropbox/sync/android/i;


# instance fields
.field a:I

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private f:Landroid/content/Context;

.field private g:Lcom/dropbox/sync/android/aY;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/dropbox/sync/android/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/i;->b:Ljava/lang/String;

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/i;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/sync/android/i;->a:I

    .line 50
    const/4 v0, 0x1

    new-instance v1, Lcom/dropbox/sync/android/k;

    invoke-direct {v1}, Lcom/dropbox/sync/android/k;-><init>()V

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/i;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 51
    return-void
.end method

.method public static a()Lcom/dropbox/sync/android/i;
    .locals 2

    .prologue
    .line 41
    sget-object v1, Lcom/dropbox/sync/android/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/i;->d:Lcom/dropbox/sync/android/i;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/dropbox/sync/android/i;

    invoke-direct {v0}, Lcom/dropbox/sync/android/i;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/i;->d:Lcom/dropbox/sync/android/i;

    .line 45
    :cond_0
    sget-object v0, Lcom/dropbox/sync/android/i;->d:Lcom/dropbox/sync/android/i;

    monitor-exit v1

    return-object v0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/i;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/sync/android/i;->d()V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 260
    invoke-direct {p0}, Lcom/dropbox/sync/android/i;->d()V

    .line 261
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->e:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 262
    return-void
.end method

.method static synthetic b(Lcom/dropbox/sync/android/i;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->e:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 75
    sget-object v1, Lcom/dropbox/sync/android/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/i;->d:Lcom/dropbox/sync/android/i;

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/dropbox/sync/android/i;->d:Lcom/dropbox/sync/android/i;

    iget-object v0, v0, Lcom/dropbox/sync/android/i;->e:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/sync/android/i;->d:Lcom/dropbox/sync/android/i;

    .line 80
    :cond_0
    monitor-exit v1

    .line 81
    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dropbox/sync/android/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/sync/android/i;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/sync/android/i;->e()V

    return-void
.end method

.method private declared-synchronized d()V
    .locals 2

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/dropbox/sync/android/i;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/sync/android/i;->a:I

    .line 266
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->g:Lcom/dropbox/sync/android/aY;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->g:Lcom/dropbox/sync/android/aY;

    sget-object v1, Lcom/dropbox/sync/android/ba;->a:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aY;->a(Lcom/dropbox/sync/android/ba;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :cond_0
    monitor-exit p0

    return-void

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/dropbox/sync/android/i;->a:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 273
    iget v0, p0, Lcom/dropbox/sync/android/i;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/sync/android/i;->a:I

    .line 274
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->g:Lcom/dropbox/sync/android/aY;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/sync/android/i;->a:I

    if-nez v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->g:Lcom/dropbox/sync/android/aY;

    sget-object v1, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aY;->a(Lcom/dropbox/sync/android/ba;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :cond_0
    monitor-exit p0

    return-void

    .line 272
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/i;->f:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 61
    iput-object p1, p0, Lcom/dropbox/sync/android/i;->f:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/dropbox/sync/android/aY;

    iget-object v1, p0, Lcom/dropbox/sync/android/i;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/aY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/i;->g:Lcom/dropbox/sync/android/aY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_0
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Lcom/dropbox/sync/android/V;)V
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 97
    new-instance v0, Lcom/dropbox/sync/android/n;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/sync/android/n;-><init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/V;)V

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/i;->a(Ljava/lang/Runnable;)V

    .line 98
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Lcom/dropbox/sync/android/V;)V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/dropbox/sync/android/o;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/sync/android/o;-><init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/V;)V

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/i;->a(Ljava/lang/Runnable;)V

    .line 148
    return-void
.end method

.method final c(Lcom/dropbox/sync/android/V;)V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/dropbox/sync/android/m;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/sync/android/m;-><init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/V;)V

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/i;->a(Ljava/lang/Runnable;)V

    .line 177
    return-void
.end method
