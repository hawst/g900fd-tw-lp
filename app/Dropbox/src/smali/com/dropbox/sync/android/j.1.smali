.class abstract Lcom/dropbox/sync/android/j;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected final a:Lcom/dropbox/sync/android/G;

.field final synthetic b:Lcom/dropbox/sync/android/i;

.field private final c:I

.field private final d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/G;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 210
    iput-object p1, p0, Lcom/dropbox/sync/android/j;->b:Lcom/dropbox/sync/android/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p2, p0, Lcom/dropbox/sync/android/j;->a:Lcom/dropbox/sync/android/G;

    .line 212
    iput v0, p0, Lcom/dropbox/sync/android/j;->c:I

    .line 213
    iput v0, p0, Lcom/dropbox/sync/android/j;->d:I

    .line 214
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/sync/android/j;->e:I

    .line 215
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/G;II)V
    .locals 1

    .prologue
    .line 217
    iput-object p1, p0, Lcom/dropbox/sync/android/j;->b:Lcom/dropbox/sync/android/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-object p2, p0, Lcom/dropbox/sync/android/j;->a:Lcom/dropbox/sync/android/G;

    .line 219
    iput p3, p0, Lcom/dropbox/sync/android/j;->c:I

    .line 220
    iput p4, p0, Lcom/dropbox/sync/android/j;->d:I

    .line 221
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/sync/android/j;->e:I

    .line 222
    return-void
.end method

.method private declared-synchronized b()I
    .locals 3

    .prologue
    .line 253
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/dropbox/sync/android/j;->c:I

    iget v1, p0, Lcom/dropbox/sync/android/j;->e:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/dropbox/sync/android/j;->d:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/dropbox/sync/android/j;->e:I

    .line 255
    iget v0, p0, Lcom/dropbox/sync/android/j;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public run()V
    .locals 5

    .prologue
    .line 227
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/sync/android/j;->a()Z

    move-result v0

    .line 228
    if-nez v0, :cond_0

    iget v0, p0, Lcom/dropbox/sync/android/j;->d:I

    if-lez v0, :cond_0

    .line 229
    invoke-direct {p0}, Lcom/dropbox/sync/android/j;->b()I

    move-result v0

    .line 231
    iget-object v1, p0, Lcom/dropbox/sync/android/j;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Background task will retry in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/dropbox/sync/android/j;->b:Lcom/dropbox/sync/android/i;

    invoke-static {v1}, Lcom/dropbox/sync/android/i;->a(Lcom/dropbox/sync/android/i;)V

    .line 234
    iget-object v1, p0, Lcom/dropbox/sync/android/j;->b:Lcom/dropbox/sync/android/i;

    invoke-static {v1}, Lcom/dropbox/sync/android/i;->b(Lcom/dropbox/sync/android/i;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, p0, v2, v3, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :goto_0
    iget-object v0, p0, Lcom/dropbox/sync/android/j;->b:Lcom/dropbox/sync/android/i;

    invoke-static {v0}, Lcom/dropbox/sync/android/i;->c(Lcom/dropbox/sync/android/i;)V

    .line 248
    return-void

    .line 236
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/j;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Background task complete."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/sync/android/j;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Thread;Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 241
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dropbox/sync/android/j;->b:Lcom/dropbox/sync/android/i;

    invoke-static {v1}, Lcom/dropbox/sync/android/i;->c(Lcom/dropbox/sync/android/i;)V

    throw v0

    .line 242
    :catch_1
    move-exception v0

    .line 243
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/sync/android/j;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Thread;Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 244
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method
