.class final Lcom/dropbox/sync/android/T;
.super Lcom/dropbox/sync/android/y;
.source "panda.py"


# instance fields
.field final synthetic b:Lcom/dropbox/sync/android/Q;

.field private c:Ljavax/net/ssl/HttpsURLConnection;


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/Q;Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 1

    .prologue
    .line 259
    iput-object p1, p0, Lcom/dropbox/sync/android/T;->b:Lcom/dropbox/sync/android/Q;

    .line 260
    invoke-static {p2}, Lcom/dropbox/sync/android/O;->a(Ljavax/net/ssl/HttpsURLConnection;)Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/y;-><init>(Ljava/io/OutputStream;)V

    .line 261
    iget-object v0, p0, Lcom/dropbox/sync/android/T;->a:Ljava/io/OutputStream;

    invoke-static {p1, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/io/OutputStream;)V

    .line 262
    invoke-virtual {p2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 263
    iput-object p2, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    .line 264
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    if-nez v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/sync/android/w;
    .locals 4

    .prologue
    .line 285
    iget-object v1, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    .line 286
    if-nez v1, :cond_0

    .line 287
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t finish().  Uploader already closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    .line 293
    :try_start_0
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 297
    :goto_0
    iget-object v2, p0, Lcom/dropbox/sync/android/T;->b:Lcom/dropbox/sync/android/Q;

    invoke-static {v2, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/io/InputStream;)V

    .line 298
    new-instance v2, Lcom/dropbox/sync/android/w;

    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v3

    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v2, v3, v0, v1}, Lcom/dropbox/sync/android/w;-><init>(ILjava/io/InputStream;Ljava/util/Map;)V

    return-object v2

    .line 294
    :catch_0
    move-exception v0

    .line 295
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    if-nez v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/T;->c:Ljavax/net/ssl/HttpsURLConnection;

    goto :goto_0
.end method
