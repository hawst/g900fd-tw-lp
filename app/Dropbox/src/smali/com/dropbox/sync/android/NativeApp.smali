.class Lcom/dropbox/sync/android/NativeApp;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/dropbox/sync/android/NativeLib;

.field private final d:Lcom/dropbox/sync/android/NativeEnv;

.field private final e:Lcom/dropbox/sync/android/G;

.field private final f:Lcom/dropbox/sync/android/bh;

.field private final g:J

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/dropbox/sync/android/NativeApp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/NativeApp;->a:Z

    .line 10
    const-class v0, Lcom/dropbox/sync/android/NativeApp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeApp;->b:Ljava/lang/String;

    .line 203
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 204
    invoke-static {}, Lcom/dropbox/sync/android/NativeApp;->nativeClassInit()V

    .line 205
    return-void

    .line 9
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/dropbox/sync/android/NativeLib;Lcom/dropbox/sync/android/NativeEnv;Ljava/lang/String;Lcom/dropbox/sync/android/be;Lcom/dropbox/sync/android/bh;)V
    .locals 6

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-virtual {p2}, Lcom/dropbox/sync/android/NativeEnv;->d()Lcom/dropbox/sync/android/NativeEnv$Config;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/sync/android/NativeEnv$Config;->appSecret:Ljava/lang/String;

    invoke-static {v0, p4}, Lcom/dropbox/sync/android/V;->a(Ljava/lang/String;Lcom/dropbox/sync/android/be;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 57
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeApp;->c:Lcom/dropbox/sync/android/NativeLib;

    .line 58
    iput-object p2, p0, Lcom/dropbox/sync/android/NativeApp;->d:Lcom/dropbox/sync/android/NativeEnv;

    .line 59
    new-instance v0, Lcom/dropbox/sync/android/G;

    invoke-direct {v0, p2}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeApp;->e:Lcom/dropbox/sync/android/G;

    .line 60
    iput-object p5, p0, Lcom/dropbox/sync/android/NativeApp;->f:Lcom/dropbox/sync/android/bh;

    .line 62
    instance-of v0, p4, Lcom/dropbox/sync/android/bf;

    if-eqz v0, :cond_0

    .line 63
    check-cast p4, Lcom/dropbox/sync/android/bf;

    .line 64
    invoke-virtual {p2}, Lcom/dropbox/sync/android/NativeEnv;->c()J

    move-result-wide v1

    iget-object v3, p4, Lcom/dropbox/sync/android/bf;->a:Ljava/lang/String;

    iget-object v4, p4, Lcom/dropbox/sync/android/bf;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeApp;->nativeInitOAuth1(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    .line 71
    :goto_0
    sget-boolean v0, Lcom/dropbox/sync/android/NativeApp;->a:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid native app handle."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 65
    :cond_0
    instance-of v0, p4, Lcom/dropbox/sync/android/bg;

    if-eqz v0, :cond_1

    .line 66
    check-cast p4, Lcom/dropbox/sync/android/bg;

    .line 67
    invoke-virtual {p2}, Lcom/dropbox/sync/android/NativeEnv;->c()J

    move-result-wide v0

    iget-object v2, p4, Lcom/dropbox/sync/android/bg;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, p3}, Lcom/dropbox/sync/android/NativeApp;->nativeInitOAuth2(JLjava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    goto :goto_0

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unexpected token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0

    .line 73
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeApp;->h:Z

    .line 74
    return-void
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeDeinit(JZ)V
.end method

.method private native nativeFree(J)V
.end method

.method private native nativeGetAccountInfo(JLcom/dropbox/sync/android/NativeApp$AccountInfoBuilder;)Lcom/dropbox/sync/android/DbxAccountInfo;
.end method

.method private native nativeInitOAuth1(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method private native nativeInitOAuth2(JLjava/lang/String;Ljava/lang/String;)J
.end method

.method private native nativeUnlinkAuth(J)V
.end method

.method private onUnlink()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 182
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeApp;->f:Lcom/dropbox/sync/android/bh;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeApp;->f:Lcom/dropbox/sync/android/bh;

    invoke-interface {v0}, Lcom/dropbox/sync/android/bh;->a()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/NativeApp;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :catch_1
    move-exception v0

    .line 188
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/NativeApp;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    return-wide v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 87
    monitor-enter p0

    .line 88
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeApp;->h:Z

    if-nez v0, :cond_0

    .line 89
    monitor-exit p0

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeApp;->h:Z

    .line 92
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    invoke-direct {p0, v0, v1, p1}, Lcom/dropbox/sync/android/NativeApp;->nativeDeinit(JZ)V

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Lcom/dropbox/sync/android/NativeEnv;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeApp;->d:Lcom/dropbox/sync/android/NativeEnv;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeApp;->nativeUnlinkAuth(J)V

    .line 163
    return-void
.end method

.method public final d()Lcom/dropbox/sync/android/DbxAccountInfo;
    .locals 3

    .prologue
    .line 175
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    new-instance v2, Lcom/dropbox/sync/android/NativeApp$AccountInfoBuilder;

    invoke-direct {v2}, Lcom/dropbox/sync/android/NativeApp$AccountInfoBuilder;-><init>()V

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/sync/android/NativeApp;->nativeGetAccountInfo(JLcom/dropbox/sync/android/NativeApp$AccountInfoBuilder;)Lcom/dropbox/sync/android/DbxAccountInfo;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeApp;->h:Z

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeApp;->e:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeApp;->b:Ljava/lang/String;

    const-string v2, "NativeApp finalized without being deinitialized."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeApp;->g:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeApp;->nativeFree(J)V

    goto :goto_0
.end method
