.class Lcom/dropbox/sync/android/NativeClient$FileStatusBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/NativeClient;


# direct methods
.method private a(I)Lcom/dropbox/sync/android/aI;
    .locals 5

    .prologue
    .line 1014
    packed-switch p1, :pswitch_data_0

    .line 1022
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unhandled native pending op state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1016
    :pswitch_0
    sget-object v0, Lcom/dropbox/sync/android/aI;->a:Lcom/dropbox/sync/android/aI;

    .line 1020
    :goto_0
    return-object v0

    .line 1018
    :pswitch_1
    sget-object v0, Lcom/dropbox/sync/android/aI;->c:Lcom/dropbox/sync/android/aI;

    goto :goto_0

    .line 1020
    :pswitch_2
    sget-object v0, Lcom/dropbox/sync/android/aI;->b:Lcom/dropbox/sync/android/aI;

    goto :goto_0

    .line 1014
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public createStatus(ZZIILjava/lang/String;JJ)Lcom/dropbox/sync/android/DbxFileStatus;
    .locals 9
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 996
    :try_start_0
    new-instance v0, Lcom/dropbox/sync/android/DbxFileStatus;

    invoke-direct {p0, p3}, Lcom/dropbox/sync/android/NativeClient$FileStatusBuilder;->a(I)Lcom/dropbox/sync/android/aI;

    move-result-object v3

    if-nez p4, :cond_0

    const/4 v4, 0x0

    :goto_0
    move v1, p1

    move v2, p2

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-direct/range {v0 .. v8}, Lcom/dropbox/sync/android/DbxFileStatus;-><init>(ZZLcom/dropbox/sync/android/aI;Lcom/dropbox/sync/android/DbxException;JJ)V

    return-object v0

    :cond_0
    const-string v1, "download status"

    invoke-static {v1, p4, p5}, Lcom/dropbox/sync/android/DbxException;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    goto :goto_0

    .line 1000
    :catch_0
    move-exception v0

    .line 1001
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$FileStatusBuilder;->a:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 1002
    throw v0

    .line 1003
    :catch_1
    move-exception v0

    .line 1004
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$FileStatusBuilder;->a:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 1005
    throw v0
.end method
