.class Lcom/dropbox/sync/android/NativeHttp;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/sync/android/G;

.field private final c:Lcom/dropbox/sync/android/NativeEnv;

.field private final d:Lcom/dropbox/sync/android/u;

.field private final e:[B

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dropbox/sync/android/NativeHttp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    .line 544
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 545
    invoke-static {}, Lcom/dropbox/sync/android/NativeHttp;->nativeClassInit()V

    .line 546
    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeHttp;->f:Z

    .line 62
    iput-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    .line 63
    iput-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->c:Lcom/dropbox/sync/android/NativeEnv;

    .line 64
    iput-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    .line 65
    iput-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    .line 66
    return-void
.end method

.method constructor <init>(Lcom/dropbox/sync/android/NativeEnv;)V
    .locals 1
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeHttp;->f:Z

    .line 49
    new-instance v0, Lcom/dropbox/sync/android/G;

    invoke-direct {v0, p1}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    .line 50
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeHttp;->c:Lcom/dropbox/sync/android/NativeEnv;

    .line 51
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->c:Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeEnv;->e()Lcom/dropbox/sync/android/u;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    .line 53
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    .line 54
    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 364
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    const-string v3, "Sending HTTP GET"

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-direct {p0, p2, p3}, Lcom/dropbox/sync/android/NativeHttp;->a([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 369
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 370
    iget-object v3, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/u;->a()Lcom/dropbox/sync/android/x;

    move-result-object v3

    .line 373
    :try_start_0
    invoke-virtual {v3, p1, v0, p4}, Lcom/dropbox/sync/android/x;->a(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/w;

    move-result-object v0

    .line 374
    iget-object v1, v0, Lcom/dropbox/sync/android/w;->b:Ljava/io/InputStream;

    .line 375
    iget-object v4, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v5}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BLcom/dropbox/sync/android/C;)V

    .line 376
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 381
    iget-object v5, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v6, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HTTP GET status "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lcom/dropbox/sync/android/w;->a:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    new-instance v5, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;

    iget v0, v0, Lcom/dropbox/sync/android/w;->a:I

    invoke-direct {v5, v0, v4}, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;-><init>(I[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 387
    if-eqz v1, :cond_0

    .line 388
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 390
    :cond_0
    invoke-virtual {v3}, Lcom/dropbox/sync/android/x;->a()V

    return-object v5

    .line 386
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 387
    if-eqz v1, :cond_1

    .line 388
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 390
    :cond_1
    invoke-virtual {v3}, Lcom/dropbox/sync/android/x;->a()V

    throw v0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 399
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    const-string v3, "Sending HTTP GET"

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-direct {p0, p2, p3}, Lcom/dropbox/sync/android/NativeHttp;->a([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 404
    iget-object v2, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    invoke-virtual {v2}, Lcom/dropbox/sync/android/u;->a()Lcom/dropbox/sync/android/x;

    move-result-object v4

    .line 408
    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {v4, p1, v0, v2}, Lcom/dropbox/sync/android/x;->a(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/w;

    move-result-object v5

    .line 409
    iget-object v3, v5, Lcom/dropbox/sync/android/w;->b:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    const/16 v0, 0xc8

    :try_start_1
    iget v2, v5, Lcom/dropbox/sync/android/w;->a:I

    if-ne v0, v2, :cond_2

    .line 415
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    new-instance v6, Lcom/dropbox/sync/android/bo;

    invoke-direct {v6, p0, p5, p6}, Lcom/dropbox/sync/android/bo;-><init>(Lcom/dropbox/sync/android/NativeHttp;J)V

    invoke-static {v3, v0, v2, v6}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/File;[BLcom/dropbox/sync/android/C;)V

    .line 427
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HTTP GET status "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v5, Lcom/dropbox/sync/android/w;->a:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v2, v6}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 441
    :goto_0
    new-instance v2, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;

    iget v5, v5, Lcom/dropbox/sync/android/w;->a:I

    invoke-direct {v2, v5, v0}, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;-><init>(I[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 444
    if-eqz v1, :cond_0

    .line 445
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 447
    :cond_0
    if-eqz v3, :cond_1

    .line 448
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 450
    :cond_1
    invoke-virtual {v4}, Lcom/dropbox/sync/android/x;->a()V

    return-object v2

    .line 430
    :cond_2
    :try_start_2
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 432
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    const/4 v1, 0x0

    invoke-static {v3, v2, v0, v1}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BLcom/dropbox/sync/android/C;)V

    .line 433
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 438
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v6, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HTTP GET status "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/dropbox/sync/android/w;->a:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v0, v1

    move-object v1, v2

    goto :goto_0

    .line 444
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_1
    if-eqz v1, :cond_3

    .line 445
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 447
    :cond_3
    if-eqz v2, :cond_4

    .line 448
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 450
    :cond_4
    invoke-virtual {v4}, Lcom/dropbox/sync/android/x;->a()V

    throw v0

    .line 444
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JJJ)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 12

    .prologue
    .line 460
    iget-object v2, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v3, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    const-string v4, "Sending HTTP PUT"

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-direct {p0, p2, p3}, Lcom/dropbox/sync/android/NativeHttp;->a([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 466
    new-instance v3, Lcom/dropbox/sync/android/v;

    const-string v4, "Content-Type"

    const-string v5, "application/octet-stream"

    invoke-direct {v3, v4, v5}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    new-instance v3, Lcom/dropbox/sync/android/v;

    const-string v4, "Content-Length"

    invoke-static/range {p7 .. p8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    iget-object v3, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/u;->a()Lcom/dropbox/sync/android/x;

    move-result-object v10

    .line 472
    const/4 v3, -0x1

    :try_start_0
    invoke-virtual {v10, p1, v2, v3}, Lcom/dropbox/sync/android/x;->c(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v11

    .line 478
    :try_start_1
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p4

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v3, v11, Lcom/dropbox/sync/android/y;->a:Ljava/io/OutputStream;

    iget-object v4, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    new-instance v9, Lcom/dropbox/sync/android/bp;

    move-wide/from16 v0, p9

    invoke-direct {v9, p0, v0, v1}, Lcom/dropbox/sync/android/bp;-><init>(Lcom/dropbox/sync/android/NativeHttp;J)V

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-static/range {v2 .. v9}, Lcom/dropbox/sync/android/z;->a(Ljava/io/File;Ljava/io/OutputStream;[BJJLcom/dropbox/sync/android/C;)V

    .line 486
    invoke-virtual {v11}, Lcom/dropbox/sync/android/y;->b()Lcom/dropbox/sync/android/w;

    move-result-object v2

    .line 487
    iget-object v3, v2, Lcom/dropbox/sync/android/w;->b:Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489
    :try_start_2
    invoke-virtual {v11}, Lcom/dropbox/sync/android/y;->a()V

    .line 493
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 495
    :try_start_3
    iget-object v5, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BLcom/dropbox/sync/android/C;)V

    .line 496
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    .line 501
    iget-object v6, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v7, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HTTP PUT status "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v2, Lcom/dropbox/sync/android/w;->a:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    new-instance v6, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;

    iget v2, v2, Lcom/dropbox/sync/android/w;->a:I

    invoke-direct {v6, v2, v5}, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;-><init>(I[B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 506
    :try_start_4
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 507
    if-eqz v3, :cond_0

    .line 508
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 512
    :cond_0
    invoke-virtual {v10}, Lcom/dropbox/sync/android/x;->a()V

    return-object v6

    .line 489
    :catchall_0
    move-exception v2

    :try_start_5
    invoke-virtual {v11}, Lcom/dropbox/sync/android/y;->a()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 512
    :catchall_1
    move-exception v2

    invoke-virtual {v10}, Lcom/dropbox/sync/android/x;->a()V

    throw v2

    .line 506
    :catchall_2
    move-exception v2

    :try_start_6
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 507
    if-eqz v3, :cond_1

    .line 508
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[BI)Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;
    .locals 7

    .prologue
    .line 150
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    const-string v2, "Sending HTTP POST"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-direct {p0, p2, p3}, Lcom/dropbox/sync/android/NativeHttp;->a([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 156
    new-instance v1, Lcom/dropbox/sync/android/v;

    const-string v2, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded; charset=utf-8"

    invoke-direct {v1, v2, v3}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v1, Lcom/dropbox/sync/android/v;

    const-string v2, "Content-Length"

    array-length v3, p4

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    invoke-virtual {v1}, Lcom/dropbox/sync/android/u;->a()Lcom/dropbox/sync/android/x;

    move-result-object v1

    .line 160
    invoke-virtual {v1, p1, v0, p5}, Lcom/dropbox/sync/android/x;->b(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/y;

    move-result-object v2

    .line 161
    const/4 v1, 0x0

    .line 164
    :try_start_0
    iget-object v0, v2, Lcom/dropbox/sync/android/y;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p4}, Ljava/io/OutputStream;->write([B)V

    .line 165
    invoke-virtual {v2}, Lcom/dropbox/sync/android/y;->b()Lcom/dropbox/sync/android/w;

    move-result-object v0

    .line 166
    iget-object v1, v0, Lcom/dropbox/sync/android/w;->b:Ljava/io/InputStream;

    .line 171
    iget-object v3, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v4, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HTTP POST status "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/dropbox/sync/android/w;->a:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    new-instance v3, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;

    iget v0, v0, Lcom/dropbox/sync/android/w;->a:I

    invoke-direct {v3, v0, v1, v2}, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;-><init>(ILjava/io/InputStream;Lcom/dropbox/sync/android/y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    return-object v3

    .line 177
    :catchall_0
    move-exception v0

    .line 178
    invoke-virtual {v2}, Lcom/dropbox/sync/android/y;->a()V

    .line 179
    if-eqz v1, :cond_0

    .line 180
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v0
.end method

.method private a()Lcom/dropbox/sync/android/v;
    .locals 3

    .prologue
    .line 517
    new-instance v0, Lcom/dropbox/sync/android/v;

    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/dropbox/sync/android/NativeHttp;->c:Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v2}, Lcom/dropbox/sync/android/NativeEnv;->d()Lcom/dropbox/sync/android/NativeEnv$Config;

    move-result-object v2

    iget-object v2, v2, Lcom/dropbox/sync/android/NativeEnv$Config;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/sync/android/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 302
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 304
    invoke-direct {p0}, Lcom/dropbox/sync/android/NativeHttp;->a()Lcom/dropbox/sync/android/v;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 306
    new-instance v2, Lcom/dropbox/sync/android/v;

    aget-object v3, p1, v0

    aget-object v4, p2, v0

    invoke-direct {v2, v3, v4}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_0
    return-object v1
.end method

.method static synthetic a(JJ)V
    .locals 0

    .prologue
    .line 22
    invoke-static {p0, p1, p2, p3}, Lcom/dropbox/sync/android/NativeHttp;->nativeUpdateFileProgress(JJ)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 231
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/dropbox/sync/android/NativeHttp;->b(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/dropbox/sync/android/bq;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->c:Lcom/dropbox/sync/android/NativeEnv;

    iget-object v2, v0, Lcom/dropbox/sync/android/bq;->a:Lcom/dropbox/sync/android/ao;

    iget v3, v0, Lcom/dropbox/sync/android/bq;->b:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/NativeEnv;->a(Lcom/dropbox/sync/android/ao;ILjava/lang/String;)Z

    .line 238
    :goto_1
    return-void

    .line 232
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ": "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    const-string v3, "Failed to set error code: "

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 531
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/dropbox/sync/android/NativeHttp;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    monitor-exit p0

    return-void

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[BI)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 317
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    const-string v3, "Sending HTTP POST"

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    invoke-direct {p0, p2, p3}, Lcom/dropbox/sync/android/NativeHttp;->a([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 323
    new-instance v2, Lcom/dropbox/sync/android/v;

    const-string v3, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded; charset=utf-8"

    invoke-direct {v2, v3, v4}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    new-instance v2, Lcom/dropbox/sync/android/v;

    const-string v3, "Content-Length"

    array-length v4, p4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/dropbox/sync/android/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    iget-object v2, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    invoke-virtual {v2}, Lcom/dropbox/sync/android/u;->a()Lcom/dropbox/sync/android/x;

    move-result-object v4

    .line 331
    :try_start_0
    invoke-virtual {v4, p1, v0, p5}, Lcom/dropbox/sync/android/x;->b(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 332
    :try_start_1
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 333
    :try_start_2
    iget-object v0, v3, Lcom/dropbox/sync/android/y;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p4}, Ljava/io/OutputStream;->write([B)V

    .line 334
    invoke-virtual {v3}, Lcom/dropbox/sync/android/y;->b()Lcom/dropbox/sync/android/w;

    move-result-object v0

    .line 335
    iget-object v1, v0, Lcom/dropbox/sync/android/w;->b:Ljava/io/InputStream;

    .line 336
    iget-object v5, p0, Lcom/dropbox/sync/android/NativeHttp;->e:[B

    const/4 v6, 0x0

    invoke-static {v1, v2, v5, v6}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BLcom/dropbox/sync/android/C;)V

    .line 337
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    .line 342
    iget-object v6, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v7, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HTTP POST status "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Lcom/dropbox/sync/android/w;->a:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    new-instance v6, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;

    iget v0, v0, Lcom/dropbox/sync/android/w;->a:I

    invoke-direct {v6, v0, v5}, Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;-><init>(I[B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 346
    if-eqz v2, :cond_0

    .line 347
    invoke-virtual {v3}, Lcom/dropbox/sync/android/y;->a()V

    .line 349
    :cond_0
    if-eqz v2, :cond_1

    .line 350
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 352
    :cond_1
    if-eqz v1, :cond_2

    .line 353
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 355
    :cond_2
    invoke-virtual {v4}, Lcom/dropbox/sync/android/x;->a()V

    return-object v6

    .line 346
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_0
    if-eqz v2, :cond_3

    .line 347
    invoke-virtual {v3}, Lcom/dropbox/sync/android/y;->a()V

    .line 349
    :cond_3
    if-eqz v2, :cond_4

    .line 350
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 352
    :cond_4
    if-eqz v1, :cond_5

    .line 353
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 355
    :cond_5
    invoke-virtual {v4}, Lcom/dropbox/sync/android/x;->a()V

    throw v0

    .line 346
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_0

    :catchall_2
    move-exception v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/dropbox/sync/android/bq;
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 246
    invoke-direct {p0}, Lcom/dropbox/sync/android/NativeHttp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " failed after shutdown: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    new-instance v0, Lcom/dropbox/sync/android/bq;

    sget-object v1, Lcom/dropbox/sync/android/ao;->c:Lcom/dropbox/sync/android/ao;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/sync/android/bq;-><init>(Lcom/dropbox/sync/android/ao;I)V

    .line 298
    :goto_0
    return-object v0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/dropbox/sync/android/G;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    instance-of v0, p2, Ljava/lang/RuntimeException;

    if-nez v0, :cond_1

    instance-of v0, p2, Ljava/lang/Error;

    if-eqz v0, :cond_2

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    invoke-static {p2, v0, v1}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 262
    :cond_2
    instance-of v0, p2, Ljava/io/IOException;

    if-nez v0, :cond_3

    .line 263
    new-instance v0, Lcom/dropbox/sync/android/bq;

    sget-object v1, Lcom/dropbox/sync/android/ao;->a:Lcom/dropbox/sync/android/ao;

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/bq;-><init>(Lcom/dropbox/sync/android/ao;I)V

    goto :goto_0

    .line 265
    :cond_3
    check-cast p2, Ljava/io/IOException;

    .line 269
    sget-object v0, Lcom/dropbox/sync/android/ao;->t:Lcom/dropbox/sync/android/ao;

    .line 271
    instance-of v1, p2, Lcom/dropbox/sync/android/A;

    if-nez v1, :cond_4

    instance-of v1, p2, Lcom/dropbox/sync/android/B;

    if-eqz v1, :cond_a

    .line 274
    :cond_4
    sget-object v0, Lcom/dropbox/sync/android/ao;->k:Lcom/dropbox/sync/android/ao;

    .line 276
    check-cast p2, Lcom/dropbox/sync/android/E;

    invoke-virtual {p2}, Lcom/dropbox/sync/android/E;->a()Ljava/io/IOException;

    move-result-object p2

    move-object v1, v0

    move-object v0, p2

    .line 278
    :goto_1
    instance-of v4, v0, Lcom/dropbox/sync/android/D;

    if-nez v4, :cond_5

    instance-of v4, v0, Lcom/dropbox/sync/android/F;

    if-eqz v4, :cond_9

    .line 281
    :cond_5
    sget-object v1, Lcom/dropbox/sync/android/ao;->t:Lcom/dropbox/sync/android/ao;

    .line 283
    check-cast v0, Lcom/dropbox/sync/android/E;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/E;->a()Ljava/io/IOException;

    move-result-object v0

    move-object v4, v0

    move v0, v3

    .line 287
    :goto_2
    instance-of v5, v4, Ljava/net/SocketTimeoutException;

    if-eqz v5, :cond_6

    .line 288
    sget-object v1, Lcom/dropbox/sync/android/ao;->u:Lcom/dropbox/sync/android/ao;

    .line 298
    :goto_3
    new-instance v0, Lcom/dropbox/sync/android/bq;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/sync/android/bq;-><init>(Lcom/dropbox/sync/android/ao;I)V

    goto/16 :goto_0

    .line 290
    :cond_6
    instance-of v5, v4, Ljava/net/ConnectException;

    if-eqz v5, :cond_7

    .line 291
    sget-object v1, Lcom/dropbox/sync/android/ao;->v:Lcom/dropbox/sync/android/ao;

    goto :goto_3

    .line 293
    :cond_7
    instance-of v3, v4, Ljavax/net/ssl/SSLException;

    if-eqz v3, :cond_8

    .line 294
    sget-object v1, Lcom/dropbox/sync/android/ao;->w:Lcom/dropbox/sync/android/ao;

    move v3, v2

    .line 295
    goto :goto_3

    :cond_8
    move v3, v0

    goto :goto_3

    :cond_9
    move-object v4, v0

    move v0, v2

    goto :goto_2

    :cond_a
    move-object v1, v0

    move-object v0, p2

    goto :goto_1
.end method

.method private declared-synchronized b()Z
    .locals 1

    .prologue
    .line 535
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeHttp;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private httpGetToFile(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 211
    :try_start_0
    invoke-direct/range {p0 .. p6}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 214
    :goto_0
    return-object v0

    .line 212
    :catch_0
    move-exception v0

    .line 213
    const-string v1, "GET-to-file"

    invoke-direct {p0, v1, v0}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 214
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private httpPutFile(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JJJ)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 222
    :try_start_0
    invoke-direct/range {p0 .. p10}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JJJ)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 225
    :goto_0
    return-object v0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    const-string v1, "PUT-file"

    invoke-direct {p0, v1, v0}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 225
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private httpRequest(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[BI)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 134
    if-eqz p4, :cond_0

    .line 135
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/dropbox/sync/android/NativeHttp;->b(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[BI)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    .line 137
    :cond_0
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I)Lcom/dropbox/sync/android/NativeHttp$NativeHttpResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 140
    if-nez p4, :cond_1

    const-string v0, "GET"

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 141
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_1
    const-string v0, "POST"

    goto :goto_1
.end method

.method private httpShutDown()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/NativeHttp;->a(Z)V

    .line 123
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp;->d:Lcom/dropbox/sync/android/u;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/u;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeHttp;->b:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeHttp;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private httpStreamingRequest(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[BI)Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;
    .locals 4
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 192
    if-eqz p4, :cond_0

    .line 193
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[BI)Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    .line 197
    :cond_0
    const-string v0, "GET"

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "doGetStreaming not yet implemented"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v2}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 198
    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 201
    if-nez p4, :cond_1

    const-string v0, "GET"

    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/dropbox/sync/android/NativeHttp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 202
    goto :goto_0

    .line 201
    :cond_1
    const-string v0, "POST"

    goto :goto_1
.end method

.method private static native nativeClassInit()V
.end method

.method private static native nativeUpdateFileProgress(JJ)V
.end method
