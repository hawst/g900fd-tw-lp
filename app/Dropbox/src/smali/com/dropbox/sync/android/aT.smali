.class public final Lcom/dropbox/sync/android/aT;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/dropbox/sync/android/aT;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/dropbox/sync/android/aT;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/dropbox/sync/android/aT;

    invoke-direct {v0}, Lcom/dropbox/sync/android/aT;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/aT;->a:Lcom/dropbox/sync/android/aT;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "/"

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/aT;-><init>(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method constructor <init>(J)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->a()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v0

    .line 100
    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->c(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/sync/android/aT;->b:Ljava/lang/String;

    .line 101
    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->d(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    .line 102
    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->e(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/sync/android/aT;->e:Ljava/lang/String;

    .line 105
    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->a(J)V

    .line 106
    iput-wide p1, p0, Lcom/dropbox/sync/android/aT;->c:J

    .line 107
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->a()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v0

    .line 54
    :try_start_0
    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->a()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/dropbox/sync/android/NativeLib;->a(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/dropbox/sync/android/aT;->c:J

    .line 58
    iget-wide v1, p0, Lcom/dropbox/sync/android/aT;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/NativeLib;->c(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/sync/android/aT;->b:Ljava/lang/String;

    .line 59
    iget-wide v1, p0, Lcom/dropbox/sync/android/aT;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/NativeLib;->d(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    .line 60
    iget-wide v1, p0, Lcom/dropbox/sync/android/aT;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/NativeLib;->e(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/aT;->e:Ljava/lang/String;
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    new-instance v1, Lcom/dropbox/sync/android/aU;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid Dropbox path \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/dropbox/sync/android/aU;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method private b(Lcom/dropbox/sync/android/aT;)Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/aT;)I
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/dropbox/sync/android/aT;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/sync/android/aT;->b:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final b()J
    .locals 2

    .prologue
    .line 275
    iget-wide v0, p0, Lcom/dropbox/sync/android/aT;->c:J

    return-wide v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/dropbox/sync/android/aT;

    invoke-virtual {p0, p1}, Lcom/dropbox/sync/android/aT;->a(Lcom/dropbox/sync/android/aT;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/dropbox/sync/android/aT;

    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/aT;->b(Lcom/dropbox/sync/android/aT;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final finalize()V
    .locals 4

    .prologue
    .line 115
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/dropbox/sync/android/aT;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 116
    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->a()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v0

    iget-wide v1, p0, Lcom/dropbox/sync/android/aT;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/NativeLib;->b(J)V

    .line 118
    :cond_0
    return-void
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/dropbox/sync/android/aT;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/dropbox/sync/android/aT;->b:Ljava/lang/String;

    return-object v0
.end method
