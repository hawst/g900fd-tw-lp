.class public final Lcom/dropbox/sync/android/bk;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static c:I

.field private static d:I


# instance fields
.field public final a:Lcom/dropbox/sync/android/aT;

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    sput v0, Lcom/dropbox/sync/android/bk;->c:I

    .line 131
    const/4 v0, 0x2

    sput v0, Lcom/dropbox/sync/android/bk;->d:I

    return-void
.end method

.method constructor <init>(Lcom/dropbox/sync/android/aT;I)V
    .locals 3

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "path shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    iput-object p1, p0, Lcom/dropbox/sync/android/bk;->a:Lcom/dropbox/sync/android/aT;

    .line 153
    sget v0, Lcom/dropbox/sync/android/bk;->c:I

    if-lt p2, v0, :cond_1

    sget v0, Lcom/dropbox/sync/android/bk;->d:I

    if-le p2, v0, :cond_2

    .line 154
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nativeMode must be in the range ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/dropbox/sync/android/bk;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/dropbox/sync/android/bk;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_2
    iput p2, p0, Lcom/dropbox/sync/android/bk;->b:I

    .line 158
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 162
    if-ne p0, p1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    instance-of v2, p1, Lcom/dropbox/sync/android/bk;

    if-nez v2, :cond_2

    move v0, v1

    .line 166
    goto :goto_0

    .line 168
    :cond_2
    check-cast p1, Lcom/dropbox/sync/android/bk;

    .line 169
    iget-object v2, p0, Lcom/dropbox/sync/android/bk;->a:Lcom/dropbox/sync/android/aT;

    iget-object v3, p1, Lcom/dropbox/sync/android/bk;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v2, v3}, Lcom/dropbox/sync/android/aT;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/dropbox/sync/android/bk;->b:I

    iget v3, p1, Lcom/dropbox/sync/android/bk;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 175
    .line 176
    iget-object v0, p0, Lcom/dropbox/sync/android/bk;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aT;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 177
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/dropbox/sync/android/bk;->b:I

    add-int/2addr v0, v1

    .line 178
    return v0
.end method
