.class public Lcom/dropbox/sync/android/bz;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Lcom/dropbox/sync/android/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/dropbox/sync/android/bz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/bz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/dropbox/sync/android/bz;->b:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/dropbox/sync/android/V;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/sync/android/bz;->c:Lcom/dropbox/sync/android/aa;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aa;->e()Lcom/dropbox/sync/android/V;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/ai;
    .locals 3

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 95
    :try_start_0
    invoke-static {p1}, Lcom/dropbox/sync/android/ai;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/ai;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 96
    :catch_0
    move-exception v1

    .line 100
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V

    .line 101
    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/y/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/ac;)V
    .locals 5

    .prologue
    .line 54
    invoke-static {}, Lcom/dropbox/sync/android/aa;->d()V

    .line 56
    iget-object v0, p0, Lcom/dropbox/sync/android/bz;->b:Landroid/content/Context;

    iget-object v1, p1, Ldbxyzptlk/db231222/y/l;->a:Ljava/lang/String;

    iget-object v2, p1, Ldbxyzptlk/db231222/y/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/dropbox/sync/android/bx;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/ag;

    move-result-object v1

    invoke-static {p2, p3, p4, p5}, Lcom/dropbox/sync/android/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/s;

    move-result-object v2

    invoke-static {}, Lcom/dropbox/sync/android/bx;->a()Lcom/dropbox/sync/android/e;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dropbox/sync/android/aa;->a(Landroid/content/Context;Lcom/dropbox/sync/android/ag;Lcom/dropbox/sync/android/s;Lcom/dropbox/sync/android/e;Z)Lcom/dropbox/sync/android/aa;

    move-result-object v0

    .line 62
    invoke-virtual {v0, p6}, Lcom/dropbox/sync/android/aa;->a(Lcom/dropbox/sync/android/ac;)V

    .line 63
    iput-object v0, p0, Lcom/dropbox/sync/android/bz;->c:Lcom/dropbox/sync/android/aa;

    .line 64
    return-void
.end method

.method public final a(Ljava/lang/String;Ldbxyzptlk/db231222/y/k;)V
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/dropbox/sync/android/bz;->a(Ljava/lang/String;)Lcom/dropbox/sync/android/V;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/dropbox/sync/android/bz;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "syncapi already linked to account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :goto_0
    return-void

    .line 75
    :cond_0
    sget-object v0, Lcom/dropbox/sync/android/bz;->a:Ljava/lang/String;

    const-string v1, "linking syncapi account"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/dropbox/sync/android/bf;

    iget-object v1, p2, Ldbxyzptlk/db231222/y/k;->a:Ljava/lang/String;

    iget-object v2, p2, Ldbxyzptlk/db231222/y/k;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/bf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/dropbox/sync/android/bz;->c:Lcom/dropbox/sync/android/aa;

    invoke-virtual {v1}, Lcom/dropbox/sync/android/aa;->f()Lcom/dropbox/sync/android/a;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/dropbox/sync/android/a;->a(Ljava/lang/String;Lcom/dropbox/sync/android/be;)V

    goto :goto_0
.end method
