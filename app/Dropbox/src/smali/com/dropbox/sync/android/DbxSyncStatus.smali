.class public final Lcom/dropbox/sync/android/DbxSyncStatus;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Lcom/dropbox/sync/android/bd;

.field public final c:Lcom/dropbox/sync/android/bd;

.field public final d:Lcom/dropbox/sync/android/bd;


# direct methods
.method constructor <init>(ZLcom/dropbox/sync/android/bd;Lcom/dropbox/sync/android/bd;Lcom/dropbox/sync/android/bd;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-boolean p1, p0, Lcom/dropbox/sync/android/DbxSyncStatus;->a:Z

    .line 107
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "metadata must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    iput-object p2, p0, Lcom/dropbox/sync/android/DbxSyncStatus;->b:Lcom/dropbox/sync/android/bd;

    .line 109
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "download must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_1
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxSyncStatus;->c:Lcom/dropbox/sync/android/bd;

    .line 111
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "upload must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_2
    iput-object p4, p0, Lcom/dropbox/sync/android/DbxSyncStatus;->d:Lcom/dropbox/sync/android/bd;

    .line 113
    return-void
.end method
