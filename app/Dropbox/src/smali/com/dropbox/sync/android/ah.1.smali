.class public final enum Lcom/dropbox/sync/android/ah;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/sync/android/ah;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/sync/android/ah;

.field public static final enum b:Lcom/dropbox/sync/android/ah;

.field public static final enum c:Lcom/dropbox/sync/android/ah;

.field public static final enum d:Lcom/dropbox/sync/android/ah;

.field public static final enum e:Lcom/dropbox/sync/android/ah;

.field public static final enum f:Lcom/dropbox/sync/android/ah;

.field private static final synthetic g:[Lcom/dropbox/sync/android/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/dropbox/sync/android/ah;

    const-string v1, "DbxContactTypeEmpty"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/sync/android/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ah;->a:Lcom/dropbox/sync/android/ah;

    .line 17
    new-instance v0, Lcom/dropbox/sync/android/ah;

    const-string v1, "DbxContactTypeDropboxAccount"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/sync/android/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ah;->b:Lcom/dropbox/sync/android/ah;

    .line 18
    new-instance v0, Lcom/dropbox/sync/android/ah;

    const-string v1, "DbxContactTypeEmailAddress"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/sync/android/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ah;->c:Lcom/dropbox/sync/android/ah;

    .line 19
    new-instance v0, Lcom/dropbox/sync/android/ah;

    const-string v1, "DbxContactTypePhoneNumber"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/sync/android/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ah;->d:Lcom/dropbox/sync/android/ah;

    .line 20
    new-instance v0, Lcom/dropbox/sync/android/ah;

    const-string v1, "DbxContactTypeFacebookid"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/sync/android/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ah;->e:Lcom/dropbox/sync/android/ah;

    .line 21
    new-instance v0, Lcom/dropbox/sync/android/ah;

    const-string v1, "DbxContactTypeMultiValue"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/ah;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ah;->f:Lcom/dropbox/sync/android/ah;

    .line 15
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dropbox/sync/android/ah;

    sget-object v1, Lcom/dropbox/sync/android/ah;->a:Lcom/dropbox/sync/android/ah;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/sync/android/ah;->b:Lcom/dropbox/sync/android/ah;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/sync/android/ah;->c:Lcom/dropbox/sync/android/ah;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/sync/android/ah;->d:Lcom/dropbox/sync/android/ah;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/sync/android/ah;->e:Lcom/dropbox/sync/android/ah;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/sync/android/ah;->f:Lcom/dropbox/sync/android/ah;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/sync/android/ah;->g:[Lcom/dropbox/sync/android/ah;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/sync/android/ah;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/dropbox/sync/android/ah;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ah;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/sync/android/ah;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/dropbox/sync/android/ah;->g:[Lcom/dropbox/sync/android/ah;

    invoke-virtual {v0}, [Lcom/dropbox/sync/android/ah;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/ah;

    return-object v0
.end method
