.class Lcom/dropbox/sync/android/G;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static c:Lcom/dropbox/sync/android/NativeEnv;

.field private static d:Lcom/dropbox/sync/android/J;

.field private static e:Lcom/dropbox/sync/android/G;


# instance fields
.field private final f:Lcom/dropbox/sync/android/NativeEnv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const-class v0, Lcom/dropbox/sync/android/G;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/G;->a:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/G;->b:Ljava/lang/Object;

    .line 30
    new-instance v0, Lcom/dropbox/sync/android/G;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    sput-object v0, Lcom/dropbox/sync/android/G;->e:Lcom/dropbox/sync/android/G;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/NativeEnv;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/dropbox/sync/android/G;->f:Lcom/dropbox/sync/android/NativeEnv;

    .line 44
    return-void
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 111
    sget-object v1, Lcom/dropbox/sync/android/G;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/G;->c:Lcom/dropbox/sync/android/NativeEnv;

    if-nez v0, :cond_0

    .line 113
    monitor-exit v1

    .line 126
    :goto_0
    return-void

    .line 115
    :cond_0
    sget-object v0, Lcom/dropbox/sync/android/G;->d:Lcom/dropbox/sync/android/J;

    if-eqz v0, :cond_1

    .line 116
    invoke-static {}, Lcom/dropbox/sync/android/I;->a()Lcom/dropbox/sync/android/I;

    move-result-object v0

    sget-object v2, Lcom/dropbox/sync/android/G;->d:Lcom/dropbox/sync/android/J;

    invoke-virtual {v0, v2}, Lcom/dropbox/sync/android/I;->b(Lcom/dropbox/sync/android/J;)V

    .line 117
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/sync/android/G;->d:Lcom/dropbox/sync/android/J;

    .line 119
    :cond_1
    sget-object v0, Lcom/dropbox/sync/android/G;->c:Lcom/dropbox/sync/android/NativeEnv;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/dropbox/sync/android/NativeEnv;->a(Z)V

    .line 120
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/sync/android/G;->c:Lcom/dropbox/sync/android/NativeEnv;

    .line 121
    new-instance v0, Lcom/dropbox/sync/android/G;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    sput-object v0, Lcom/dropbox/sync/android/G;->e:Lcom/dropbox/sync/android/G;

    .line 125
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/dropbox/sync/android/G;->f:Lcom/dropbox/sync/android/NativeEnv;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/dropbox/sync/android/G;->f:Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v0, p2, p3, p4}, Lcom/dropbox/sync/android/NativeEnv;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    return-void
.end method

.method private a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, Lcom/dropbox/sync/android/G;->f:Lcom/dropbox/sync/android/NativeEnv;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/dropbox/sync/android/G;->f:Lcom/dropbox/sync/android/NativeEnv;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, p3, v1}, Lcom/dropbox/sync/android/NativeEnv;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_0
    return-void
.end method

.method static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/sync/android/r;Ljava/io/File;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 62
    sget-object v1, Lcom/dropbox/sync/android/G;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    sget-object v2, Lcom/dropbox/sync/android/G;->c:Lcom/dropbox/sync/android/NativeEnv;

    if-eqz v2, :cond_0

    .line 64
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_0
    return v0

    .line 69
    :cond_0
    :try_start_1
    new-instance v2, Lcom/dropbox/sync/android/NativeEnv;

    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->a()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v3

    invoke-direct {v2, v3, p1, p2}, Lcom/dropbox/sync/android/NativeEnv;-><init>(Lcom/dropbox/sync/android/NativeLib;Lcom/dropbox/sync/android/r;Ljava/io/File;)V

    .line 70
    invoke-virtual {v2}, Lcom/dropbox/sync/android/NativeEnv;->a()V

    .line 72
    new-instance v3, Lcom/dropbox/sync/android/H;

    invoke-direct {v3, v2}, Lcom/dropbox/sync/android/H;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    sput-object v3, Lcom/dropbox/sync/android/G;->d:Lcom/dropbox/sync/android/J;

    .line 84
    invoke-static {}, Lcom/dropbox/sync/android/I;->a()Lcom/dropbox/sync/android/I;

    move-result-object v3

    .line 85
    sget-object v4, Lcom/dropbox/sync/android/G;->d:Lcom/dropbox/sync/android/J;

    invoke-virtual {v3, v4}, Lcom/dropbox/sync/android/I;->a(Lcom/dropbox/sync/android/J;)V

    .line 86
    sget-object v4, Lcom/dropbox/sync/android/G;->d:Lcom/dropbox/sync/android/J;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/I;->b()Z

    move-result v3

    invoke-interface {v4, v3}, Lcom/dropbox/sync/android/J;->a(Z)V
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    :try_start_2
    sput-object v2, Lcom/dropbox/sync/android/G;->c:Lcom/dropbox/sync/android/NativeEnv;

    .line 96
    new-instance v3, Lcom/dropbox/sync/android/G;

    invoke-direct {v3, v2}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    sput-object v3, Lcom/dropbox/sync/android/G;->e:Lcom/dropbox/sync/android/G;

    .line 97
    monitor-exit v1

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    :try_start_3
    sget-object v2, Lcom/dropbox/sync/android/G;->e:Lcom/dropbox/sync/android/G;

    sget-object v3, Lcom/dropbox/sync/android/G;->a:Ljava/lang/String;

    const-string v4, "Failed to set up global NativeEnv."

    invoke-virtual {v2, v3, v4, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 89
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method static b()Lcom/dropbox/sync/android/G;
    .locals 2

    .prologue
    .line 135
    sget-object v1, Lcom/dropbox/sync/android/G;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 136
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/G;->e:Lcom/dropbox/sync/android/G;

    monitor-exit v1

    return-object v0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dropbox/sync/android/G;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p2}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 229
    throw p2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 189
    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 190
    const/4 v1, 0x6

    const/4 v2, 0x3

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/G;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 191
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 184
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/dropbox/sync/android/G;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 199
    invoke-static {p1, p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    const/4 v1, 0x4

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/G;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 201
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 194
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/dropbox/sync/android/G;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 196
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 209
    invoke-static {p1, p2, p3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    const/4 v1, 0x5

    const/4 v2, 0x2

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/G;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 211
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 204
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v0, 0x5

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/dropbox/sync/android/G;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 218
    return-void
.end method
