.class public final Lcom/dropbox/sync/android/DbxNotificationSyncStatus;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Lcom/dropbox/sync/android/aS;

.field public final c:Lcom/dropbox/sync/android/aS;


# direct methods
.method constructor <init>(ZLcom/dropbox/sync/android/aS;Lcom/dropbox/sync/android/aS;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean p1, p0, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->a:Z

    .line 94
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "retrieve must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iput-object p2, p0, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->b:Lcom/dropbox/sync/android/aS;

    .line 96
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ops must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_1
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->c:Lcom/dropbox/sync/android/aS;

    .line 98
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->b:Lcom/dropbox/sync/android/aS;

    iget-boolean v0, v0, Lcom/dropbox/sync/android/aS;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->c:Lcom/dropbox/sync/android/aS;

    iget-boolean v0, v0, Lcom/dropbox/sync/android/aS;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
