.class public final Lcom/dropbox/sync/android/bx;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/ag;
    .locals 1

    .prologue
    .line 23
    invoke-static {p0, p1}, Lcom/dropbox/sync/android/bx;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/ag;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lcom/dropbox/sync/android/e;
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/dropbox/sync/android/bx;->b()Lcom/dropbox/sync/android/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/s;
    .locals 1

    .prologue
    .line 23
    invoke-static {p0, p1, p2, p3}, Lcom/dropbox/sync/android/bx;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/s;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/dropbox/sync/android/DbxException;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxException;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/dropbox/sync/android/V;)V
    .locals 0

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/dropbox/sync/android/V;->i()V

    .line 142
    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/ag;
    .locals 6

    .prologue
    .line 109
    new-instance v0, Lcom/dropbox/sync/android/ag;

    const-wide/32 v3, 0x20000000

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/ag;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    return-object v0
.end method

.method private static b()Lcom/dropbox/sync/android/e;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/dropbox/sync/android/by;

    invoke-direct {v0}, Lcom/dropbox/sync/android/by;-><init>()V

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/s;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/dropbox/sync/android/s;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/dropbox/sync/android/s;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
