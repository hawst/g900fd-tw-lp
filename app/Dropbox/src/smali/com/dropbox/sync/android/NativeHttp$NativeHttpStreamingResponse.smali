.class Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:Lcom/dropbox/sync/android/y;

.field public final responseCode:I
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field


# direct methods
.method constructor <init>(ILjava/io/InputStream;Lcom/dropbox/sync/android/y;)V
    .locals 0
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput p1, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->responseCode:I

    .line 96
    iput-object p2, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->a:Ljava/io/InputStream;

    .line 97
    iput-object p3, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->b:Lcom/dropbox/sync/android/y;

    .line 98
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->b:Lcom/dropbox/sync/android/y;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/y;->a()V

    .line 103
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 106
    :cond_0
    return-void
.end method

.method public read([B)I
    .locals 1
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 111
    const/4 v0, -0x1

    .line 114
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeHttp$NativeHttpStreamingResponse;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    goto :goto_0
.end method
