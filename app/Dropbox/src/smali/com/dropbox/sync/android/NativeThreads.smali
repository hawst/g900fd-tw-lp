.class Lcom/dropbox/sync/android/NativeThreads;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/sync/android/G;

.field private final c:I

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeThreads;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;JJLcom/dropbox/sync/android/G;)V
    .locals 1

    .prologue
    .line 30
    invoke-static {p6, p2, p3, p4, p5}, Lcom/dropbox/sync/android/NativeThreads;->a(Lcom/dropbox/sync/android/G;JJ)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0, p6}, Lcom/dropbox/sync/android/NativeThreads;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/dropbox/sync/android/G;)V

    .line 31
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/dropbox/sync/android/G;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;",
            "Lcom/dropbox/sync/android/G;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    .line 42
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected at least 1 native thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object p3, p0, Lcom/dropbox/sync/android/NativeThreads;->b:Lcom/dropbox/sync/android/G;

    .line 44
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->b:Lcom/dropbox/sync/android/G;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "logger shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/dropbox/sync/android/NativeThreads;->c:I

    .line 46
    iput-object p2, p0, Lcom/dropbox/sync/android/NativeThreads;->d:Ljava/util/List;

    .line 47
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeThreads;->e:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "name shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/dropbox/sync/android/NativeThreads;)Lcom/dropbox/sync/android/G;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->b:Lcom/dropbox/sync/android/G;

    return-object v0
.end method

.method private static a(Lcom/dropbox/sync/android/G;JJ)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/sync/android/G;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {p1, p2}, Lcom/dropbox/sync/android/NativeThreads;->nativeGetThreadCount(J)I

    move-result v6

    .line 126
    if-gtz v6, :cond_0

    .line 127
    sget-object v0, Lcom/dropbox/sync/android/NativeThreads;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid native thread count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 130
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 131
    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_1

    .line 133
    new-instance v0, Lcom/dropbox/sync/android/bw;

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/bw;-><init>(JJI)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 143
    :cond_1
    return-object v7
.end method

.method static synthetic a(JJI)V
    .locals 0

    .prologue
    .line 15
    invoke-static {p0, p1, p2, p3, p4}, Lcom/dropbox/sync/android/NativeThreads;->nativeRunThread(JJI)V

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/dropbox/sync/android/NativeThreads;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static native nativeGetThreadCount(J)I
.end method

.method private static native nativeRunThread(JJI)V
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 75
    :cond_0
    monitor-exit p0

    return-void

    .line 61
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/dropbox/sync/android/NativeThreads;->c:I

    if-ge v1, v0, :cond_0

    .line 65
    new-instance v2, Ljava/lang/Thread;

    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/dropbox/sync/android/NativeThreads;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/dropbox/sync/android/bv;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/bv;-><init>(Lcom/dropbox/sync/android/NativeThreads;)V

    invoke-virtual {v2, v0}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 72
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 73
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 87
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 97
    monitor-enter p0

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    .line 99
    if-nez v0, :cond_0

    .line 100
    monitor-exit p0

    .line 118
    :goto_0
    return-void

    .line 102
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 104
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 106
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 108
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 109
    :catch_0
    move-exception v0

    .line 110
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 115
    :cond_1
    monitor-enter p0

    .line 116
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/dropbox/sync/android/NativeThreads;->f:Ljava/util/List;

    .line 117
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 102
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
