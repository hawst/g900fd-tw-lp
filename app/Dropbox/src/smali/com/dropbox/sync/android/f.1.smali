.class final Lcom/dropbox/sync/android/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Landroid/os/Looper;

.field private static b:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/f;->a:Landroid/os/Looper;

    .line 14
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/dropbox/sync/android/f;->a:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/dropbox/sync/android/f;->b:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/dropbox/sync/android/f;->b:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 101
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    if-eqz p2, :cond_0

    invoke-static {}, Lcom/dropbox/sync/android/f;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    throw v0

    .line 85
    :cond_0
    sget-object v1, Lcom/dropbox/sync/android/f;->b:Landroid/os/Handler;

    new-instance v2, Lcom/dropbox/sync/android/g;

    invoke-direct {v2, v0}, Lcom/dropbox/sync/android/g;-><init>(Ljava/lang/RuntimeException;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 91
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    sget-object v1, Lcom/dropbox/sync/android/f;->a:Landroid/os/Looper;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method
