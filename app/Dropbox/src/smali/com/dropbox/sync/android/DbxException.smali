.class public Lcom/dropbox/sync/android/DbxException;
.super Ljava/io/IOException;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field public static final serialVersionUID:J


# instance fields
.field private final b:Lcom/dropbox/sync/android/ao;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/dropbox/sync/android/DbxException;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/DbxException;->a:Ljava/lang/String;

    .line 405
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 406
    invoke-static {}, Lcom/dropbox/sync/android/DbxException;->nativeClassInit()V

    .line 407
    return-void
.end method

.method private constructor <init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 30
    if-eqz p4, :cond_0

    .line 31
    invoke-virtual {p0, p4}, Lcom/dropbox/sync/android/DbxException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/dropbox/sync/android/DbxException;->b:Lcom/dropbox/sync/android/ao;

    .line 34
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxException;->c:Ljava/lang/String;

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Lcom/dropbox/sync/android/aq;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/sync/android/DbxException;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method static a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 68
    invoke-static {p1}, Lcom/dropbox/sync/android/ao;->a(I)Lcom/dropbox/sync/android/ao;

    move-result-object v2

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/dropbox/sync/android/ao;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 71
    invoke-static {v2, v3, v5}, Lcom/dropbox/sync/android/DbxException;->a(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Throwable;

    move-result-object v0

    .line 72
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v4, Lcom/dropbox/sync/android/DbxException;->a:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    instance-of v1, v0, Lcom/dropbox/sync/android/DbxException;

    if-eqz v1, :cond_1

    .line 74
    check-cast v0, Lcom/dropbox/sync/android/DbxException;

    .line 76
    :goto_1
    return-object v0

    .line 69
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_1
    new-instance v1, Lcom/dropbox/sync/android/aE;

    invoke-direct {v1, v2, v3, v5, v0}, Lcom/dropbox/sync/android/aE;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Throwable;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 93
    sget-object v0, Lcom/dropbox/sync/android/aq;->a:[I

    invoke-virtual {p0}, Lcom/dropbox/sync/android/ao;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v0, v4

    .line 170
    :goto_0
    return-object v0

    .line 100
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :pswitch_1
    new-instance v0, Ljava/lang/ClassCastException;

    invoke-direct {v0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :pswitch_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :pswitch_3
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Out of memory for SDK."

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :pswitch_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    :pswitch_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :pswitch_6
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :pswitch_7
    new-instance v0, Lcom/dropbox/sync/android/av;

    sget-object v1, Lcom/dropbox/sync/android/ao;->m:Lcom/dropbox/sync/android/ao;

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/av;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto :goto_0

    .line 122
    :pswitch_8
    new-instance v0, Lcom/dropbox/sync/android/aA;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/aA;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 124
    :pswitch_9
    new-instance v0, Lcom/dropbox/sync/android/au;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/au;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 126
    :pswitch_a
    new-instance v0, Lcom/dropbox/sync/android/ar;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/ar;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 128
    :pswitch_b
    new-instance v0, Lcom/dropbox/sync/android/aB;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/aB;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 130
    :pswitch_c
    new-instance v0, Lcom/dropbox/sync/android/as;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/as;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 132
    :pswitch_d
    new-instance v0, Lcom/dropbox/sync/android/az;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/az;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 134
    :pswitch_e
    new-instance v0, Lcom/dropbox/sync/android/at;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/at;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto :goto_0

    .line 136
    :pswitch_f
    new-instance v0, Lcom/dropbox/sync/android/ay;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/ay;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto :goto_0

    .line 138
    :pswitch_10
    new-instance v0, Lcom/dropbox/sync/android/ax;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/ax;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto :goto_0

    .line 140
    :pswitch_11
    new-instance v0, Lcom/dropbox/sync/android/aG;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/aG;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto/16 :goto_0

    .line 142
    :pswitch_12
    new-instance v0, Lcom/dropbox/sync/android/aF;

    invoke-direct {v0, p1, p2, v4}, Lcom/dropbox/sync/android/aF;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 144
    :pswitch_13
    new-instance v0, Lcom/dropbox/sync/android/aH;

    invoke-direct {v0, p1, p2, v4, v4}, Lcom/dropbox/sync/android/aH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto/16 :goto_0

    .line 146
    :pswitch_14
    new-instance v0, Lcom/dropbox/sync/android/aC;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/aC;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto/16 :goto_0

    .line 148
    :pswitch_15
    new-instance v0, Lcom/dropbox/sync/android/aw;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/aw;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto/16 :goto_0

    .line 152
    :pswitch_16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null pointer in path construction."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 164
    :pswitch_17
    new-instance v0, Lcom/dropbox/sync/android/av;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/av;-><init>(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Lcom/dropbox/sync/android/aq;)V

    goto/16 :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
    .end packed-switch
.end method

.method private static native nativeClassInit()V
.end method

.method private static throwFrom(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 53
    invoke-static {p1}, Lcom/dropbox/sync/android/ao;->a(I)Lcom/dropbox/sync/android/ao;

    move-result-object v1

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/dropbox/sync/android/ao;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, p3}, Lcom/dropbox/sync/android/DbxException;->a(Lcom/dropbox/sync/android/ao;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Throwable;

    move-result-object v0

    .line 56
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/DbxException;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    throw v0

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxException;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxException;->b:Lcom/dropbox/sync/android/ao;

    invoke-virtual {v1}, Lcom/dropbox/sync/android/ao;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
