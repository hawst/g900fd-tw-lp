.class public Lcom/dropbox/sync/android/DbxDatastore;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field private final a:Lcom/dropbox/sync/android/ak;

.field private final b:Ljava/lang/String;

.field private c:Z


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 294
    if-ne p0, p1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    instance-of v2, p1, Lcom/dropbox/sync/android/DbxDatastore;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 296
    :cond_2
    check-cast p1, Lcom/dropbox/sync/android/DbxDatastore;

    .line 298
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxDatastore;->a:Lcom/dropbox/sync/android/ak;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxDatastore;->a:Lcom/dropbox/sync/android/ak;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/sync/android/DbxDatastore;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxDatastore;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxDatastore;->c:Z

    if-eqz v0, :cond_0

    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DbxDatastore was finalized without being closed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 304
    .line 306
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxDatastore;->a:Lcom/dropbox/sync/android/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 307
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxDatastore;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<DbxDatastore "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxDatastore;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
