.class final Lcom/dropbox/sync/android/m;
.super Lcom/dropbox/sync/android/j;
.source "panda.py"


# instance fields
.field final synthetic c:Lcom/dropbox/sync/android/i;

.field private final d:Lcom/dropbox/sync/android/V;


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/V;)V
    .locals 4

    .prologue
    .line 186
    iput-object p1, p0, Lcom/dropbox/sync/android/m;->c:Lcom/dropbox/sync/android/i;

    .line 187
    invoke-virtual {p2}, Lcom/dropbox/sync/android/V;->d()Lcom/dropbox/sync/android/G;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x3c

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/dropbox/sync/android/j;-><init>(Lcom/dropbox/sync/android/i;Lcom/dropbox/sync/android/G;II)V

    .line 188
    iput-object p2, p0, Lcom/dropbox/sync/android/m;->d:Lcom/dropbox/sync/android/V;

    .line 189
    iget-object v0, p0, Lcom/dropbox/sync/android/m;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scheduling task to update account info for uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' token=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/sync/android/V;->c()Lcom/dropbox/sync/android/be;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Lcom/dropbox/sync/android/m;->a:Lcom/dropbox/sync/android/G;

    invoke-static {}, Lcom/dropbox/sync/android/i;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updating account info for uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/sync/android/m;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' token=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/sync/android/m;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/V;->c()Lcom/dropbox/sync/android/be;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/dropbox/sync/android/m;->d:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->j()Z

    move-result v0

    return v0
.end method
