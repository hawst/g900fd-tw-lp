.class public final Lcom/dropbox/sync/android/DbxFileStatus;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public final c:Lcom/dropbox/sync/android/aI;

.field public final d:Lcom/dropbox/sync/android/DbxException;

.field public final e:J

.field public final f:J


# direct methods
.method constructor <init>(ZZLcom/dropbox/sync/android/aI;Lcom/dropbox/sync/android/DbxException;JJ)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-boolean p1, p0, Lcom/dropbox/sync/android/DbxFileStatus;->a:Z

    .line 84
    iput-boolean p2, p0, Lcom/dropbox/sync/android/DbxFileStatus;->b:Z

    .line 85
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "upload must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxFileStatus;->c:Lcom/dropbox/sync/android/aI;

    .line 87
    iput-object p4, p0, Lcom/dropbox/sync/android/DbxFileStatus;->d:Lcom/dropbox/sync/android/DbxException;

    .line 88
    iput-wide p5, p0, Lcom/dropbox/sync/android/DbxFileStatus;->e:J

    .line 89
    iput-wide p7, p0, Lcom/dropbox/sync/android/DbxFileStatus;->f:J

    .line 90
    return-void
.end method
