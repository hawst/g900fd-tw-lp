.class final Lcom/dropbox/sync/android/Q;
.super Lcom/dropbox/sync/android/x;
.source "panda.py"


# instance fields
.field final synthetic b:Lcom/dropbox/sync/android/O;

.field private c:Z

.field private d:Z

.field private e:Ljavax/net/ssl/HttpsURLConnection;

.field private f:Lcom/dropbox/sync/android/T;

.field private g:Ljava/io/InputStream;

.field private h:Ljava/io/OutputStream;

.field private i:Ljava/net/Socket;


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/O;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 88
    iput-object p1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/x;-><init>(Lcom/dropbox/sync/android/u;)V

    .line 79
    iput-boolean v1, p0, Lcom/dropbox/sync/android/Q;->c:Z

    .line 80
    iput-boolean v1, p0, Lcom/dropbox/sync/android/Q;->d:Z

    .line 82
    iput-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    .line 83
    iput-object v0, p0, Lcom/dropbox/sync/android/Q;->f:Lcom/dropbox/sync/android/T;

    .line 84
    iput-object v0, p0, Lcom/dropbox/sync/android/Q;->g:Ljava/io/InputStream;

    .line 85
    iput-object v0, p0, Lcom/dropbox/sync/android/Q;->h:Ljava/io/OutputStream;

    .line 86
    iput-object v0, p0, Lcom/dropbox/sync/android/Q;->i:Ljava/net/Socket;

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/dropbox/sync/android/Q;Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/Q;->a(Ljava/io/InputStream;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/sync/android/Q;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/Q;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/Q;->a(Ljava/net/Socket;)V

    return-void
.end method

.method private a(Lcom/dropbox/sync/android/T;)V
    .locals 2

    .prologue
    .line 107
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v0}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/O;)V

    .line 109
    iput-object p1, p0, Lcom/dropbox/sync/android/Q;->f:Lcom/dropbox/sync/android/T;

    .line 110
    monitor-exit v1

    .line 111
    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v0}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/O;)V

    .line 116
    iput-object p1, p0, Lcom/dropbox/sync/android/Q;->g:Ljava/io/InputStream;

    .line 117
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 121
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v0}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/O;)V

    .line 123
    iput-object p1, p0, Lcom/dropbox/sync/android/Q;->h:Ljava/io/OutputStream;

    .line 124
    monitor-exit v1

    .line 125
    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/net/Socket;)V
    .locals 2

    .prologue
    .line 128
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v0}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/O;)V

    .line 130
    iput-object p1, p0, Lcom/dropbox/sync/android/Q;->i:Ljava/net/Socket;

    .line 131
    monitor-exit v1

    .line 132
    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 2

    .prologue
    .line 100
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v0}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/O;)V

    .line 102
    iput-object p1, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    .line 103
    monitor-exit v1

    .line 104
    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f(Ljava/lang/String;Ljava/lang/Iterable;I)Ljavax/net/ssl/HttpsURLConnection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/dropbox/sync/android/v;",
            ">;I)",
            "Ljavax/net/ssl/HttpsURLConnection;"
        }
    .end annotation

    .prologue
    const/16 v4, 0x7530

    const/4 v3, 0x0

    .line 366
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 367
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v1}, Lcom/dropbox/sync/android/O;->c(Lcom/dropbox/sync/android/O;)Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 370
    new-instance v1, Lcom/dropbox/sync/android/S;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/dropbox/sync/android/S;-><init>(Lcom/dropbox/sync/android/Q;Ljavax/net/ssl/SSLSocketFactory;)V

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 372
    invoke-virtual {v0, v4}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 373
    invoke-static {p3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 374
    invoke-virtual {v0, v3}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 375
    invoke-virtual {v0, v3}, Ljavax/net/ssl/HttpsURLConnection;->setAllowUserInteraction(Z)V

    .line 377
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/sync/android/v;

    .line 378
    iget-object v3, v1, Lcom/dropbox/sync/android/v;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/dropbox/sync/android/v;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Ljavax/net/ssl/HttpsURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/dropbox/sync/android/v;",
            ">;I)",
            "Lcom/dropbox/sync/android/w;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 138
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    if-eqz v0, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t use SingleRequest for more than one request."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 141
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/Q;->f(Ljava/lang/String;Ljava/lang/Iterable;I)Ljavax/net/ssl/HttpsURLConnection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/Q;->a(Ljavax/net/ssl/HttpsURLConnection;)V

    .line 143
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 148
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 152
    :goto_0
    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/Q;->a(Ljava/io/InputStream;)V

    .line 153
    new-instance v1, Lcom/dropbox/sync/android/w;

    iget-object v2, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v2

    iget-object v3, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v3}, Ljavax/net/ssl/HttpsURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/dropbox/sync/android/w;-><init>(ILjava/io/InputStream;Ljava/util/Map;)V

    return-object v1

    .line 149
    :catch_0
    move-exception v0

    .line 150
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 250
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v0, p0}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/O;Lcom/dropbox/sync/android/x;)V

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/Q;->d:Z

    .line 253
    monitor-exit v1

    .line 254
    return-void

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final synthetic b(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/y;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/sync/android/Q;->d(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/T;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 189
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 190
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    if-nez v0, :cond_0

    .line 191
    monitor-exit v1

    .line 246
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->f:Lcom/dropbox/sync/android/T;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->f:Lcom/dropbox/sync/android/T;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/T;->c()V

    .line 202
    :cond_2
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->g:Ljava/io/InputStream;

    if-eqz v0, :cond_3

    .line 203
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->g:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;)V

    .line 205
    :cond_3
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->h:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    .line 207
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->h:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :cond_4
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->i:Ljava/net/Socket;

    if-eqz v0, :cond_5

    .line 220
    invoke-static {}, Lcom/dropbox/sync/android/f;->a()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_6

    .line 222
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->i:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 245
    :cond_5
    :goto_2
    :try_start_4
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    :try_start_5
    iget-object v2, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v2}, Lcom/dropbox/sync/android/O;->b(Lcom/dropbox/sync/android/O;)Lcom/dropbox/sync/android/G;

    move-result-object v2

    invoke-static {}, Lcom/dropbox/sync/android/O;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to close OutputStream: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 223
    :catch_1
    move-exception v0

    .line 224
    iget-object v2, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    invoke-static {v2}, Lcom/dropbox/sync/android/O;->b(Lcom/dropbox/sync/android/O;)Lcom/dropbox/sync/android/G;

    move-result-object v2

    invoke-static {}, Lcom/dropbox/sync/android/O;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to close Socket: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 229
    :cond_6
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->i:Ljava/net/Socket;

    .line 230
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/dropbox/sync/android/R;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/sync/android/R;-><init>(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 240
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method public final synthetic c(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/y;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/sync/android/Q;->e(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/T;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/T;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/dropbox/sync/android/v;",
            ">;I)",
            "Lcom/dropbox/sync/android/T;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    if-eqz v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t use SingleRequest for more than one request."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 163
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    .line 164
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/Q;->f(Ljava/lang/String;Ljava/lang/Iterable;I)Ljavax/net/ssl/HttpsURLConnection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/Q;->a(Ljavax/net/ssl/HttpsURLConnection;)V

    .line 165
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 167
    new-instance v0, Lcom/dropbox/sync/android/T;

    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/sync/android/T;-><init>(Lcom/dropbox/sync/android/Q;Ljavax/net/ssl/HttpsURLConnection;)V

    .line 168
    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/T;)V

    .line 169
    return-object v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/Iterable;I)Lcom/dropbox/sync/android/T;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/dropbox/sync/android/v;",
            ">;I)",
            "Lcom/dropbox/sync/android/T;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 176
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t use SingleRequest for more than one request."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 179
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/dropbox/sync/android/Q;->c:Z

    .line 180
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/Q;->f(Ljava/lang/String;Ljava/lang/Iterable;I)Ljavax/net/ssl/HttpsURLConnection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/Q;->a(Ljavax/net/ssl/HttpsURLConnection;)V

    .line 181
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    iget-object v0, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 183
    new-instance v0, Lcom/dropbox/sync/android/T;

    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->e:Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/sync/android/T;-><init>(Lcom/dropbox/sync/android/Q;Ljavax/net/ssl/HttpsURLConnection;)V

    .line 184
    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/T;)V

    .line 185
    return-object v0
.end method

.method protected final finalize()V
    .locals 3

    .prologue
    .line 92
    iget-object v1, p0, Lcom/dropbox/sync/android/Q;->b:Lcom/dropbox/sync/android/O;

    monitor-enter v1

    .line 93
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/Q;->d:Z

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "SingleRequest not finished before finalization."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    return-void
.end method
