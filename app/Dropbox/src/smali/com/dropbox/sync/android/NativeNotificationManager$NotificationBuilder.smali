.class Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/A/a;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field final synthetic c:Lcom/dropbox/sync/android/NativeNotificationManager;


# direct methods
.method private constructor <init>(Lcom/dropbox/sync/android/NativeNotificationManager;)V
    .locals 1

    .prologue
    .line 326
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->c:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->a:Ljava/util/List;

    .line 328
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/NativeNotificationManager;Lcom/dropbox/sync/android/br;)V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;-><init>(Lcom/dropbox/sync/android/NativeNotificationManager;)V

    return-void
.end method


# virtual methods
.method public addNotification(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;)V
    .locals 4
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 333
    :try_start_0
    invoke-static {p1, p2}, Ldbxyzptlk/db231222/A/a;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;)Ldbxyzptlk/db231222/A/a;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_0

    .line 335
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 338
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->c:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/NativeNotificationManager;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeNotificationManager;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to parse notification"

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 340
    :catch_1
    move-exception v0

    .line 341
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->c:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/NativeNotificationManager;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeNotificationManager;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 342
    throw v0

    .line 343
    :catch_2
    move-exception v0

    .line 344
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->c:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/NativeNotificationManager;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeNotificationManager;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 345
    throw v0
.end method

.method public createHeader(JILjava/lang/String;JI)Lcom/dropbox/sync/android/DbxNotificationHeader;
    .locals 7
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 351
    new-instance v0, Lcom/dropbox/sync/android/DbxNotificationHeader;

    new-instance v5, Ljava/util/Date;

    const-wide/16 v1, 0x3e8

    mul-long/2addr v1, p5

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/DbxNotificationHeader;-><init>(JILjava/lang/String;Ljava/util/Date;I)V

    return-object v0
.end method

.method public setHaveOldest()V
    .locals 1
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->b:Z

    .line 357
    return-void
.end method
