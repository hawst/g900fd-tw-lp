.class Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/sync/android/DbxFileInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/dropbox/sync/android/NativeClient;


# direct methods
.method private a(JZJJJZLjava/lang/String;)Lcom/dropbox/sync/android/DbxFileInfo;
    .locals 8

    .prologue
    .line 944
    if-eqz p3, :cond_0

    .line 945
    :goto_0
    new-instance v0, Lcom/dropbox/sync/android/DbxFileInfo;

    new-instance v1, Lcom/dropbox/sync/android/aT;

    invoke-direct {v1, p1, p2}, Lcom/dropbox/sync/android/aT;-><init>(J)V

    new-instance v5, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p6

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    move v2, p3

    move-wide v3, p4

    move/from16 v6, p10

    move-object/from16 v7, p11

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/sync/android/DbxFileInfo;-><init>(Lcom/dropbox/sync/android/aT;ZJLjava/util/Date;ZLjava/lang/String;)V

    return-object v0

    :cond_0
    move-wide/from16 p6, p8

    .line 944
    goto :goto_0
.end method


# virtual methods
.method public addMetadata(JZJJJZLjava/lang/String;)V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 916
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->a:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p11}, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->createMetadata(JZJJJZLjava/lang/String;)Lcom/dropbox/sync/android/DbxFileInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 925
    return-void

    .line 918
    :catch_0
    move-exception v0

    .line 919
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->b:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 920
    throw v0

    .line 921
    :catch_1
    move-exception v0

    .line 922
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->b:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 923
    throw v0
.end method

.method public createMetadata(JZJJJZLjava/lang/String;)Lcom/dropbox/sync/android/DbxFileInfo;
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 931
    :try_start_0
    invoke-direct/range {p0 .. p11}, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->a(JZJJJZLjava/lang/String;)Lcom/dropbox/sync/android/DbxFileInfo;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 933
    :catch_0
    move-exception v0

    .line 934
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->b:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 935
    throw v0

    .line 936
    :catch_1
    move-exception v0

    .line 937
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeClient$MetadataBuilder;->b:Lcom/dropbox/sync/android/NativeClient;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeClient;->a(Lcom/dropbox/sync/android/NativeClient;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeClient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 938
    throw v0
.end method
