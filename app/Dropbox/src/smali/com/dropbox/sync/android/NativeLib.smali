.class Lcom/dropbox/sync/android/NativeLib;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/dropbox/sync/android/NativeLib;


# instance fields
.field private final c:Lcom/dropbox/sync/android/NativeHttp;

.field private final d:Lcom/dropbox/sync/android/DbxException;

.field private e:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/dropbox/sync/android/NativeLib;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeLib;->a:Ljava/lang/String;

    .line 11
    new-instance v0, Lcom/dropbox/sync/android/NativeLib;

    invoke-direct {v0}, Lcom/dropbox/sync/android/NativeLib;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/NativeLib;->b:Lcom/dropbox/sync/android/NativeLib;

    .line 148
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 149
    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->nativeClassInit()V

    .line 150
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v2, p0, Lcom/dropbox/sync/android/NativeLib;->e:Ljava/io/File;

    .line 33
    new-instance v0, Lcom/dropbox/sync/android/NativeHttp;

    invoke-direct {v0}, Lcom/dropbox/sync/android/NativeHttp;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeLib;->c:Lcom/dropbox/sync/android/NativeHttp;

    .line 34
    new-instance v0, Lcom/dropbox/sync/android/aA;

    const-string v1, "dummy"

    invoke-direct {v0, v1, v2, v2}, Lcom/dropbox/sync/android/aA;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeLib;->d:Lcom/dropbox/sync/android/DbxException;

    .line 35
    return-void
.end method

.method public static a()Lcom/dropbox/sync/android/NativeLib;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/dropbox/sync/android/NativeLib;->b:Lcom/dropbox/sync/android/NativeLib;

    return-object v0
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeCreatePath(Ljava/lang/String;)J
.end method

.method private native nativeGetCanonicalPath(J)Ljava/lang/String;
.end method

.method private native nativeGetHashedPath(J)Ljava/lang/String;
.end method

.method private native nativeGetOriginalPath(J)Ljava/lang/String;
.end method

.method private native nativeIncrementPathRef(J)J
.end method

.method private native nativeReleasePathRef(J)V
.end method

.method private native nativeSetup(Ljava/lang/String;)V
.end method


# virtual methods
.method public final a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/NativeLib;->nativeCreatePath(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->nativeIncrementPathRef(J)J

    .line 86
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/NativeLib;->nativeSetup(Ljava/lang/String;)V

    .line 58
    monitor-enter p0

    .line 59
    :try_start_0
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeLib;->e:Ljava/io/File;

    .line 60
    monitor-exit p0

    .line 61
    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(J)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->nativeReleasePathRef(J)V

    .line 97
    return-void
.end method

.method public final c(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->nativeGetOriginalPath(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->nativeGetCanonicalPath(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/dropbox/sync/android/NativeLib;->nativeGetHashedPath(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
