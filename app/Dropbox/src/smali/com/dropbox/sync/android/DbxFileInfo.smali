.class public final Lcom/dropbox/sync/android/DbxFileInfo;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/dropbox/sync/android/DbxFileInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/dropbox/sync/android/aT;

.field public final b:Z

.field public final c:J

.field public final d:Ljava/util/Date;

.field public final e:Z

.field public final f:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/aT;ZJLjava/util/Date;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    .line 68
    iput-boolean p2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    .line 69
    iput-wide p3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    .line 70
    iput-object p5, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    .line 71
    iput-boolean p6, p0, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    .line 72
    iput-object p7, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    .line 73
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/DbxFileInfo;)I
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 132
    iget-object v3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    iget-object v4, p1, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v3, v4}, Lcom/dropbox/sync/android/aT;->a(Lcom/dropbox/sync/android/aT;)I

    move-result v3

    .line 133
    if-eqz v3, :cond_1

    move v1, v3

    .line 161
    :cond_0
    :goto_0
    return v1

    .line 136
    :cond_1
    iget-object v3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    iget-object v4, p1, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v3

    .line 137
    if-eqz v3, :cond_2

    move v1, v3

    .line 138
    goto :goto_0

    .line 140
    :cond_2
    iget-wide v3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    iget-wide v5, p1, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    sub-long/2addr v3, v5

    .line 141
    cmp-long v5, v7, v3

    if-eqz v5, :cond_4

    .line 142
    cmp-long v2, v3, v7

    if-gez v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 144
    :cond_4
    iget-boolean v3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    iget-boolean v4, p1, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    if-eq v3, v4, :cond_5

    .line 145
    iget-boolean v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    if-nez v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 147
    :cond_5
    iget-boolean v3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    iget-boolean v4, p1, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    if-eq v3, v4, :cond_6

    .line 148
    iget-boolean v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    if-nez v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 150
    :cond_6
    iget-object v3, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 151
    iget-object v1, p1, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    if-nez v1, :cond_7

    move v0, v2

    :cond_7
    move v1, v0

    goto :goto_0

    .line 152
    :cond_8
    iget-object v0, p1, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    .line 156
    if-nez v1, :cond_0

    move v1, v2

    .line 161
    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lcom/dropbox/sync/android/DbxFileInfo;

    invoke-virtual {p0, p1}, Lcom/dropbox/sync/android/DbxFileInfo;->a(Lcom/dropbox/sync/android/DbxFileInfo;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 98
    if-ne p0, p1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 99
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 102
    :cond_3
    check-cast p1, Lcom/dropbox/sync/android/DbxFileInfo;

    .line 104
    iget-boolean v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    iget-boolean v3, p1, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 105
    :cond_4
    iget-boolean v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    iget-boolean v3, p1, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 107
    :cond_5
    iget-wide v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    iget-wide v4, p1, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    .line 109
    :cond_6
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    if-nez v2, :cond_7

    .line 110
    iget-object v2, p1, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    .line 111
    :cond_7
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 112
    goto :goto_0

    .line 115
    :cond_8
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    if-nez v2, :cond_9

    .line 116
    iget-object v2, p1, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    .line 117
    :cond_9
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v2, v3}, Lcom/dropbox/sync/android/aT;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 118
    goto :goto_0

    .line 121
    :cond_a
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 122
    iget-object v2, p1, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 123
    :cond_b
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 124
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 84
    .line 86
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 87
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    iget-wide v4, p0, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 89
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 91
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x2694

    :goto_3
    add-int/2addr v0, v2

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 93
    return v0

    .line 86
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aT;->hashCode()I

    move-result v0

    goto :goto_1

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    goto :goto_2

    .line 91
    :cond_3
    const/16 v0, 0x265d

    goto :goto_3

    .line 92
    :cond_4
    iget-object v1, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "folder"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/dropbox/sync/android/DbxFileInfo;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "bytes; lastModified:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxFileInfo;->d:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxFileInfo;->e:Z

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "thumb available; icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxFileInfo;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "file"

    goto :goto_0

    :cond_1
    const-string v0, "no "

    goto :goto_1
.end method
