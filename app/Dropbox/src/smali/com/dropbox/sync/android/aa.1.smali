.class public Lcom/dropbox/sync/android/aa;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static c:Lcom/dropbox/sync/android/aa;


# instance fields
.field private final d:Lcom/dropbox/sync/android/G;

.field private final e:Lcom/dropbox/sync/android/a;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/dropbox/sync/android/ac;",
            "Lcom/dropbox/sync/android/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/dropbox/sync/android/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/aa;->a:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/aa;->b:Ljava/lang/Object;

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    return-void
.end method

.method private constructor <init>(Lcom/dropbox/sync/android/a;)V
    .locals 5

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/aa;->f:Ljava/util/Map;

    .line 197
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/aa;->d:Lcom/dropbox/sync/android/G;

    .line 198
    iput-object p1, p0, Lcom/dropbox/sync/android/aa;->e:Lcom/dropbox/sync/android/a;

    .line 200
    iget-object v0, p0, Lcom/dropbox/sync/android/aa;->e:Lcom/dropbox/sync/android/a;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/a;->c()Lcom/dropbox/sync/android/r;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/dropbox/sync/android/aa;->d:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/aa;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dropbox initialized for application: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    iget-object v4, v4, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/dropbox/sync/android/r;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method static a()Lcom/dropbox/sync/android/aa;
    .locals 3

    .prologue
    .line 215
    sget-object v1, Lcom/dropbox/sync/android/aa;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 216
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    if-nez v0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Dropbox isn\'t initialized."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 219
    :cond_0
    :try_start_1
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/dropbox/sync/android/ag;Lcom/dropbox/sync/android/s;Lcom/dropbox/sync/android/e;Z)Lcom/dropbox/sync/android/aa;
    .locals 7

    .prologue
    .line 176
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "applicationContext shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "config shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_1
    invoke-static {p0, p1}, Lcom/dropbox/sync/android/aa;->a(Landroid/content/Context;Lcom/dropbox/sync/android/ag;)Lcom/dropbox/sync/android/ad;

    move-result-object v1

    .line 180
    new-instance v0, Lcom/dropbox/sync/android/r;

    invoke-static {v1}, Lcom/dropbox/sync/android/ad;->a(Lcom/dropbox/sync/android/ad;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lcom/dropbox/sync/android/ad;->b(Lcom/dropbox/sync/android/ad;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Lcom/dropbox/sync/android/ad;->c(Lcom/dropbox/sync/android/ad;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/r;-><init>(Lcom/dropbox/sync/android/ag;Lcom/dropbox/sync/android/s;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 183
    sget-object v1, Lcom/dropbox/sync/android/aa;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 184
    :try_start_0
    sget-object v2, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    if-nez v2, :cond_3

    .line 185
    new-instance v2, Lcom/dropbox/sync/android/aa;

    new-instance v3, Lcom/dropbox/sync/android/a;

    invoke-direct {v3, p0, v0, p3}, Lcom/dropbox/sync/android/a;-><init>(Landroid/content/Context;Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/e;)V

    invoke-direct {v2, v3}, Lcom/dropbox/sync/android/aa;-><init>(Lcom/dropbox/sync/android/a;)V

    sput-object v2, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    .line 192
    :cond_2
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    monitor-exit v1

    return-object v0

    .line 187
    :cond_3
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    iget-object v0, v0, Lcom/dropbox/sync/android/aa;->e:Lcom/dropbox/sync/android/a;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/a;->b()Lcom/dropbox/sync/android/ag;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/dropbox/sync/android/ag;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 188
    new-instance v0, Lcom/dropbox/sync/android/ae;

    const-string v2, "Dropbox.ensureInitialized called with different configuration."

    invoke-direct {v0, v2}, Lcom/dropbox/sync/android/ae;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Landroid/content/Context;Lcom/dropbox/sync/android/ag;)Lcom/dropbox/sync/android/ad;
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 479
    if-nez p0, :cond_0

    .line 480
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The provided context is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 485
    if-eq v2, p0, :cond_1

    .line 486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The provided context isn\'t an application context."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 493
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 494
    if-eqz v3, :cond_4

    move v2, v0

    :goto_0
    invoke-static {v2}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 495
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x1005

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 502
    if-eqz v2, :cond_5

    :goto_1
    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 506
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 507
    iget-boolean v0, p1, Lcom/dropbox/sync/android/ag;->d:Z

    if-eqz v0, :cond_2

    .line 508
    const-class v0, Lcom/dropbox/sync/android/DbxAuthActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 509
    const-class v0, Lcom/dropbox/client2/android/AuthActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 511
    :cond_2
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_6

    .line 512
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    array-length v5, v4

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_6

    aget-object v6, v4, v0

    .line 513
    if-eqz v6, :cond_3

    iget-object v7, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 514
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 512
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v2, v1

    .line 494
    goto :goto_0

    .line 498
    :catch_0
    move-exception v0

    .line 499
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/aa;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unable to get package info for app package."

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5
    move v0, v1

    .line 502
    goto :goto_1

    .line 518
    :cond_6
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 519
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required Sync API Activity isn\'t included in application manifest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :cond_7
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 527
    const-class v0, Lcom/dropbox/sync/android/DbxSyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 528
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    if-eqz v0, :cond_9

    .line 529
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    array-length v5, v4

    move v0, v1

    :goto_3
    if-ge v0, v5, :cond_9

    aget-object v6, v4, v0

    .line 530
    if-eqz v6, :cond_8

    iget-object v7, v6, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    if-eqz v7, :cond_8

    .line 531
    iget-object v6, v6, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 529
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 535
    :cond_9
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 536
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required Sync API Service isn\'t included in application manifest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_a
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 544
    const-string v0, "android.permission.INTERNET"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 545
    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 546
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 547
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v5, v4

    move v0, v1

    :goto_4
    if-ge v0, v5, :cond_c

    aget-object v6, v4, v0

    .line 548
    if-eqz v6, :cond_b

    .line 549
    invoke-interface {v3, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 547
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 553
    :cond_c
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 554
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required Sync API permission isn\'t requested in application manifest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 559
    :cond_d
    iget-boolean v0, p1, Lcom/dropbox/sync/android/ag;->d:Z

    if-eqz v0, :cond_e

    .line 563
    iget-object v0, p1, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/dropbox/client2/android/AuthActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    .line 568
    :cond_e
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 572
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 573
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "DropboxSync/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "2.0.2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    const-string v3, " (Android; "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/dropbox/sync/android/f;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 582
    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 583
    const-string v2, "Dropbox-Sync-Sdk-Android/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "2.0.2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    new-instance v2, Lcom/dropbox/sync/android/ad;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/dropbox/sync/android/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v0, v3}, Lcom/dropbox/sync/android/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method static b()Z
    .locals 2

    .prologue
    .line 228
    sget-object v1, Lcom/dropbox/sync/android/aa;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static d()V
    .locals 2

    .prologue
    .line 256
    sget-object v1, Lcom/dropbox/sync/android/aa;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 257
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aa;->c()V

    .line 260
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/sync/android/aa;->c:Lcom/dropbox/sync/android/aa;

    .line 261
    monitor-exit v1

    .line 262
    return-void

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/dropbox/sync/android/ac;)V
    .locals 2

    .prologue
    .line 436
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 437
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aa;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    new-instance v0, Lcom/dropbox/sync/android/ab;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/sync/android/ab;-><init>(Lcom/dropbox/sync/android/aa;Lcom/dropbox/sync/android/ac;)V

    .line 445
    iget-object v1, p0, Lcom/dropbox/sync/android/aa;->e:Lcom/dropbox/sync/android/a;

    invoke-virtual {v1, v0}, Lcom/dropbox/sync/android/a;->a(Lcom/dropbox/sync/android/d;)V

    .line 446
    iget-object v1, p0, Lcom/dropbox/sync/android/aa;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    :cond_0
    monitor-exit p0

    return-void

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final c()V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/dropbox/sync/android/aa;->d:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/aa;->a:Ljava/lang/String;

    const-string v2, "Dropbox shutting down."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/dropbox/sync/android/aa;->e:Lcom/dropbox/sync/android/a;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/a;->a()V

    .line 242
    return-void
.end method

.method public final e()Lcom/dropbox/sync/android/V;
    .locals 5

    .prologue
    .line 409
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/sync/android/aa;->f()Lcom/dropbox/sync/android/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/a;->g()Lcom/dropbox/sync/android/V;
    :try_end_0
    .catch Lcom/dropbox/sync/android/af; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 410
    :catch_0
    move-exception v0

    .line 411
    iget-object v1, p0, Lcom/dropbox/sync/android/aa;->d:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/aa;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "More than one account linked."

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method final f()Lcom/dropbox/sync/android/a;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/dropbox/sync/android/aa;->e:Lcom/dropbox/sync/android/a;

    return-object v0
.end method
