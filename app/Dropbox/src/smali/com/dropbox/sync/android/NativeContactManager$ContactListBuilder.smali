.class Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# static fields
.field private static a:[Lcom/dropbox/sync/android/ah;


# instance fields
.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/sync/android/DbxContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/dropbox/sync/android/ah;->values()[Lcom/dropbox/sync/android/ah;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->a:[Lcom/dropbox/sync/android/ah;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->b:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/bm;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 8
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 57
    if-ltz p5, :cond_0

    sget-object v0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->a:[Lcom/dropbox/sync/android/ah;

    array-length v0, v0

    if-ge p5, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 58
    sget-object v0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->a:[Lcom/dropbox/sync/android/ah;

    aget-object v5, v0, p5

    .line 59
    iget-object v7, p0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->b:Ljava/util/ArrayList;

    new-instance v0, Lcom/dropbox/sync/android/DbxContact;

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/DbxContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/ah;Z)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createArray()[Lcom/dropbox/sync/android/DbxContact;
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/dropbox/sync/android/DbxContact;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/DbxContact;

    return-object v0
.end method
