.class Lcom/dropbox/sync/android/NativeSharedFolderManager;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 262
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 263
    invoke-static {}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeClassInit()V

    .line 264
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/V;)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->g()Lcom/dropbox/sync/android/NativeApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeApp;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    .line 16
    return-void
.end method

.method private native nativeCancelInviteToFolder(JLjava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeGetSharedFolderInfo(JLjava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private native nativeInviteToFolder(JLjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private native nativeKickUserFromFolder(JLjava/lang/String;Ljava/lang/String;ZLcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private native nativeLeaveSharedFolder(JLjava/lang/String;Z)V
.end method

.method private native nativeResendInviteToFolder(JLjava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private native nativeShareFolder(JJ[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private native nativeTransferFolderToUser(JLjava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method

.method private native nativeUnshareSharedFolder(JLjava/lang/String;Z)V
.end method

.method private native nativeUpdateSharedFolderSettings(JLjava/lang/String;ZZLcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/aT;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/sync/android/aT;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/dropbox/sync/android/DbxSharedFolderInfo;"
        }
    .end annotation

    .prologue
    .line 36
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    invoke-virtual {p1}, Lcom/dropbox/sync/android/aT;->b()J

    move-result-wide v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    new-instance v10, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v10, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v10}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeShareFolder(JJ[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 4

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    new-instance v2, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeGetSharedFolderInfo(JLjava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 6

    .prologue
    .line 106
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    new-instance v5, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v5, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeResendInviteToFolder(JLjava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 7

    .prologue
    .line 141
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    new-instance v6, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v6, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeKickUserFromFolder(JLjava/lang/String;Ljava/lang/String;ZLcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/dropbox/sync/android/DbxSharedFolderInfo;"
        }
    .end annotation

    .prologue
    .line 56
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    new-instance v7, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v7, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v3, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeInviteToFolder(JLjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 7

    .prologue
    .line 89
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    new-instance v6, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v6, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeUpdateSharedFolderSettings(JLjava/lang/String;ZZLcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 157
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeLeaveSharedFolder(JLjava/lang/String;Z)V

    .line 158
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 6

    .prologue
    .line 123
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    new-instance v5, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v5, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeCancelInviteToFolder(JLjava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeUnshareSharedFolder(JLjava/lang/String;Z)V

    .line 173
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 6

    .prologue
    .line 188
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a:J

    new-instance v5, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;

    const/4 v0, 0x0

    invoke-direct {v5, p0, v0}, Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;-><init>(Lcom/dropbox/sync/android/NativeSharedFolderManager;Lcom/dropbox/sync/android/bu;)V

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->nativeTransferFolderToUser(JLjava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/NativeSharedFolderManager$SharedFolderInfoBuilder;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method
