.class Lcom/dropbox/sync/android/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;

.field private static final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lcom/dropbox/sync/android/G;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/dropbox/sync/android/r;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/io/File;

.field private final h:Lcom/dropbox/sync/android/NativeLib;

.field private final i:Lcom/dropbox/sync/android/e;

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/dropbox/sync/android/V;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/sync/android/V;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/dropbox/sync/android/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    const-class v0, Lcom/dropbox/sync/android/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/a;->a:Z

    .line 33
    const-class v0, Lcom/dropbox/sync/android/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    .line 551
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "logs"

    aput-object v3, v0, v2

    const-string v2, "Temp"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/a;->m:Ljava/util/List;

    return-void

    :cond_0
    move v0, v2

    .line 32
    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/e;)V
    .locals 3

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    .line 124
    iput-object p1, p0, Lcom/dropbox/sync/android/a;->d:Landroid/content/Context;

    .line 125
    iput-object p2, p0, Lcom/dropbox/sync/android/a;->e:Lcom/dropbox/sync/android/r;

    .line 126
    iget-object v0, p2, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    iget-object v0, v0, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->f:Ljava/lang/String;

    .line 127
    const-string v0, "DropboxSyncCache"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 128
    iget-object v1, p2, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    iget-object v1, v1, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/a;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->g:Ljava/io/File;

    .line 129
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/dropbox/sync/android/a;->g:Ljava/io/File;

    const-string v2, "Temp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/a;->a(Ljava/io/File;)Lcom/dropbox/sync/android/NativeLib;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->h:Lcom/dropbox/sync/android/NativeLib;

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    .line 131
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    .line 132
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->l:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 133
    iput-object p3, p0, Lcom/dropbox/sync/android/a;->i:Lcom/dropbox/sync/android/e;

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/sync/android/a;->d()Ljava/io/File;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/dropbox/sync/android/G;->a(Landroid/content/Context;Lcom/dropbox/sync/android/r;Ljava/io/File;)Z

    .line 137
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    .line 140
    invoke-static {}, Lcom/dropbox/sync/android/i;->a()Lcom/dropbox/sync/android/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/i;->a(Landroid/content/Context;)V

    .line 143
    new-instance v0, Lcom/dropbox/sync/android/b;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/b;-><init>(Lcom/dropbox/sync/android/a;)V

    invoke-static {v0}, Lcom/dropbox/client2/android/AuthActivity;->a(Lcom/dropbox/client2/android/c;)V

    .line 150
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->h()V

    .line 151
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->i()V

    .line 152
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->j()V

    .line 153
    return-void
.end method

.method private a(Ljava/io/File;)Lcom/dropbox/sync/android/NativeLib;
    .locals 5

    .prologue
    .line 156
    invoke-static {p1}, Lcom/dropbox/sync/android/t;->a(Ljava/io/File;)Z

    .line 157
    invoke-static {p1}, Lcom/dropbox/sync/android/t;->b(Ljava/io/File;)V

    .line 158
    invoke-static {}, Lcom/dropbox/sync/android/NativeLib;->a()Lcom/dropbox/sync/android/NativeLib;

    move-result-object v0

    .line 160
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeLib;->a(Ljava/io/File;)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    return-object v0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to set temp dir."

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 5

    .prologue
    .line 534
    invoke-static {p1}, Lcom/dropbox/sync/android/t;->b(Ljava/io/File;)V

    .line 535
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 536
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 535
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 541
    :cond_0
    invoke-static {v3}, Lcom/dropbox/sync/android/t;->a(Ljava/io/File;)Z

    goto :goto_1

    .line 543
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 544
    invoke-static {v0}, Lcom/dropbox/sync/android/t;->b(Ljava/io/File;)V

    .line 545
    new-instance v1, Ljava/io/File;

    const-string v2, "logs"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/dropbox/sync/android/t;->b(Ljava/io/File;)V

    .line 546
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Prepared cache dir \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    return-object v0
.end method

.method private static a(Ljava/util/Iterator;Lcom/dropbox/sync/android/V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lcom/dropbox/sync/android/d;",
            ">;",
            "Lcom/dropbox/sync/android/V;",
            ")V"
        }
    .end annotation

    .prologue
    .line 683
    .line 685
    new-instance v0, Lcom/dropbox/sync/android/c;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/sync/android/c;-><init>(Ljava/util/Iterator;Lcom/dropbox/sync/android/V;)V

    invoke-static {v0}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/Runnable;)V

    .line 693
    return-void
.end method

.method private h()V
    .locals 7

    .prologue
    .line 615
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 616
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 617
    if-eqz v0, :cond_0

    .line 618
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 620
    :cond_0
    sget-object v0, Lcom/dropbox/sync/android/a;->m:Ljava/util/List;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->removeAll(Ljava/util/Collection;)Z

    .line 622
    monitor-enter p0

    .line 623
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 624
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 625
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->i:Lcom/dropbox/sync/android/e;

    iget-object v2, p0, Lcom/dropbox/sync/android/a;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/sync/android/a;->e:Lcom/dropbox/sync/android/r;

    invoke-interface {v0, v2, p0, v3}, Lcom/dropbox/sync/android/e;->a(Ljava/lang/String;Lcom/dropbox/sync/android/a;Lcom/dropbox/sync/android/r;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/V;

    .line 626
    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 627
    iget-object v3, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 628
    iget-object v3, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v4, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Ignoring duplicate account in persistent linked accounts set for uid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->a()V

    goto :goto_0

    .line 649
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 632
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 633
    iget-object v3, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v4, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Single account mode ignoring extra account in persistent linked accounts set for uid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->a()V

    goto :goto_0

    .line 638
    :cond_2
    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    .line 639
    iget-object v4, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    invoke-virtual {v1, v3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x2d

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 643
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 644
    invoke-virtual {v1, v0, v3}, Ljava/util/TreeSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->clear()V

    goto/16 :goto_0

    .line 646
    :cond_3
    iget-object v3, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 649
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 652
    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 653
    iget-object v2, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v3, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Removing unclaimed file/directory in cache: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/dropbox/sync/android/U;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/dropbox/sync/android/a;->g:Ljava/io/File;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/dropbox/sync/android/t;->a(Ljava/io/File;)Z

    goto :goto_1

    .line 656
    :cond_5
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 660
    monitor-enter p0

    .line 661
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 662
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 663
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->i:Lcom/dropbox/sync/android/e;

    iget-object v2, p0, Lcom/dropbox/sync/android/a;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/dropbox/sync/android/e;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 665
    return-void

    .line 663
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 669
    monitor-enter p0

    .line 670
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 671
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/V;

    .line 673
    invoke-static {}, Lcom/dropbox/sync/android/i;->a()Lcom/dropbox/sync/android/i;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/dropbox/sync/android/i;->a(Lcom/dropbox/sync/android/V;)V

    goto :goto_0

    .line 671
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 675
    :cond_0
    return-void
.end method

.method private k()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/dropbox/sync/android/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 679
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->l:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 229
    if-nez p2, :cond_0

    .line 234
    :goto_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/dropbox/sync/android/a;->g:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0

    .line 232
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 171
    invoke-static {}, Lcom/dropbox/sync/android/i;->b()V

    .line 174
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 175
    monitor-enter p0

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 177
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 178
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/V;

    .line 182
    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->a()V

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 186
    :cond_0
    invoke-static {}, Lcom/dropbox/sync/android/G;->a()V

    .line 187
    return-void
.end method

.method final a(Lcom/dropbox/sync/android/V;)V
    .locals 1

    .prologue
    .line 482
    monitor-enter p0

    .line 483
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 484
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->i()V

    .line 485
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->a()V

    .line 489
    return-void

    .line 485
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final a(Lcom/dropbox/sync/android/V;Z)V
    .locals 2

    .prologue
    .line 447
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 450
    monitor-enter p0

    .line 454
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->k()Ljava/util/Iterator;

    move-result-object v0

    .line 457
    if-eqz p2, :cond_0

    .line 458
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->k:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 460
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->i()V

    .line 461
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    invoke-static {v0, p1}, Lcom/dropbox/sync/android/a;->a(Ljava/util/Iterator;Lcom/dropbox/sync/android/V;)V

    .line 467
    if-eqz p2, :cond_2

    .line 468
    invoke-static {}, Lcom/dropbox/sync/android/i;->a()Lcom/dropbox/sync/android/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/i;->a(Lcom/dropbox/sync/android/V;)V

    .line 472
    :goto_1
    return-void

    .line 447
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 461
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 470
    :cond_2
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->a()V

    goto :goto_1
.end method

.method public final a(Lcom/dropbox/sync/android/d;)V
    .locals 1

    .prologue
    .line 514
    if-eqz p1, :cond_0

    .line 515
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->l:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 517
    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;Lcom/dropbox/sync/android/be;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 343
    sget-boolean v0, Lcom/dropbox/sync/android/a;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/sync/android/a;->e:Lcom/dropbox/sync/android/r;

    invoke-static {v0, p2}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/r;Lcom/dropbox/sync/android/be;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 348
    :cond_0
    :try_start_0
    new-instance v0, Lcom/dropbox/sync/android/V;

    iget-object v2, p0, Lcom/dropbox/sync/android/a;->e:Lcom/dropbox/sync/android/r;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/V;-><init>(Lcom/dropbox/sync/android/a;Lcom/dropbox/sync/android/r;Ljava/lang/String;Lcom/dropbox/sync/android/be;Lcom/dropbox/sync/android/DbxAccountInfo;Z)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    monitor-enter p0

    .line 358
    :try_start_1
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 359
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v9, v1, :cond_3

    :goto_0
    invoke-static {v9}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 360
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/sync/android/V;

    .line 361
    iget-object v2, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v3, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Extra link completed for ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " when an account is already linked for ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ".  Unlinking new account."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/sync/android/G;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v8

    .line 374
    :goto_1
    if-eqz v2, :cond_4

    .line 375
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->i()V

    .line 377
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->k()Ljava/util/Iterator;

    move-result-object v1

    .line 379
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382
    if-nez v2, :cond_1

    .line 384
    invoke-virtual {v0}, Lcom/dropbox/sync/android/V;->h()V

    .line 386
    :cond_1
    if-eqz v1, :cond_2

    .line 387
    invoke-static {v1, v0}, Lcom/dropbox/sync/android/a;->a(Ljava/util/Iterator;Lcom/dropbox/sync/android/V;)V

    .line 389
    :cond_2
    return-void

    .line 349
    :catch_0
    move-exception v0

    .line 350
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/a;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to set up newly linked user account."

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_3
    move v9, v8

    .line 359
    goto :goto_0

    .line 379
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_4
    move-object v1, v7

    goto :goto_2

    :cond_5
    move v2, v9

    goto :goto_1
.end method

.method public final b()Lcom/dropbox/sync/android/ag;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->e:Lcom/dropbox/sync/android/r;

    iget-object v0, v0, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    return-object v0
.end method

.method final b(Lcom/dropbox/sync/android/V;)V
    .locals 0

    .prologue
    .line 498
    invoke-direct {p0}, Lcom/dropbox/sync/android/a;->i()V

    .line 499
    return-void
.end method

.method final c()Lcom/dropbox/sync/android/r;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->e:Lcom/dropbox/sync/android/r;

    return-object v0
.end method

.method final d()Ljava/io/File;
    .locals 3

    .prologue
    .line 244
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/dropbox/sync/android/a;->g:Ljava/io/File;

    const-string v2, "logs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method final e()Landroid/content/Context;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->d:Landroid/content/Context;

    return-object v0
.end method

.method final f()Lcom/dropbox/sync/android/NativeLib;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->h:Lcom/dropbox/sync/android/NativeLib;

    return-object v0
.end method

.method public final declared-synchronized g()Lcom/dropbox/sync/android/V;
    .locals 2

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    const/4 v0, 0x0

    .line 422
    :goto_0
    monitor-exit p0

    return-object v0

    .line 421
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iget-object v1, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 422
    iget-object v0, p0, Lcom/dropbox/sync/android/a;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/V;

    goto :goto_0

    .line 424
    :cond_1
    new-instance v0, Lcom/dropbox/sync/android/af;

    const-string v1, "There are multiple linked Dropbox accounts.  Use CoreAccountManager to select one."

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/af;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
