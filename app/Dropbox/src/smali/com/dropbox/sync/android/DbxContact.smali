.class public final Lcom/dropbox/sync/android/DbxContact;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/dropbox/sync/android/ah;

.field private final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 70
    sget-object v5, Lcom/dropbox/sync/android/ah;->c:Lcom/dropbox/sync/android/ah;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/DbxContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/ah;Z)V

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/ah;Z)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/dropbox/sync/android/DbxContact;->a:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/dropbox/sync/android/DbxContact;->b:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxContact;->c:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lcom/dropbox/sync/android/DbxContact;->d:Ljava/lang/String;

    .line 64
    iput-object p5, p0, Lcom/dropbox/sync/android/DbxContact;->e:Lcom/dropbox/sync/android/ah;

    .line 66
    iput-boolean p6, p0, Lcom/dropbox/sync/android/DbxContact;->f:Z

    .line 67
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxContact;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/dropbox/sync/android/ah;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxContact;->e:Lcom/dropbox/sync/android/ah;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxContact;->f:Z

    return v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxContact;->b:Ljava/lang/String;

    return-object v0
.end method

.method final getPrimaryValue()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxContact;->d:Ljava/lang/String;

    return-object v0
.end method
