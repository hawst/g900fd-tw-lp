.class Lcom/dropbox/sync/android/h;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/dropbox/sync/android/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/h;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/String;)Ljava/lang/AssertionError;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unimplemented feature: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 41
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v2

    sget-object v3, Lcom/dropbox/sync/android/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v1}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 42
    throw v1
.end method

.method static a(Ljava/lang/Thread;Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Uncaught exception on Sync API thread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-virtual {p2, p3, v0, p1}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 74
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 75
    return-void
.end method

.method static a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 88
    const-string v0, "Uncaught exception in API callback."

    .line 89
    invoke-virtual {p1, p2, v0, p0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 93
    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 94
    return-void
.end method

.method static a(Z)V
    .locals 4

    .prologue
    .line 15
    if-nez p0, :cond_0

    .line 16
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Assertion failed."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 17
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/h;->a:Ljava/lang/String;

    const-string v3, "Assertion failed."

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    throw v0

    .line 20
    :cond_0
    return-void
.end method
