.class public Lcom/dropbox/sync/android/DbxNotificationHeader;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:J

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/Date;

.field private final e:I


# direct methods
.method public constructor <init>(JILjava/lang/String;Ljava/util/Date;I)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30
    :cond_1
    iput-wide p1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    .line 31
    iput p3, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    .line 32
    iput-object p4, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    .line 34
    iput p6, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->e:I

    .line 35
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    return-wide v0
.end method

.method public final a(I)Lcom/dropbox/sync/android/DbxNotificationHeader;
    .locals 7

    .prologue
    .line 66
    new-instance v0, Lcom/dropbox/sync/android/DbxNotificationHeader;

    iget-wide v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    iget v3, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    iget-object v4, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/DbxNotificationHeader;-><init>(JILjava/lang/String;Ljava/util/Date;I)V

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/util/Date;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    if-ne p0, p1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_2
    instance-of v2, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;

    if-nez v2, :cond_3

    move v0, v1

    .line 92
    goto :goto_0

    .line 94
    :cond_3
    check-cast p1, Lcom/dropbox/sync/android/DbxNotificationHeader;

    .line 95
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    if-nez v2, :cond_4

    .line 96
    iget-object v2, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    if-eqz v2, :cond_5

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_4
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_5
    iget-wide v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    iget-wide v4, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_6
    iget v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->e:I

    iget v3, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_7
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 109
    iget-object v2, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_8
    iget-object v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 113
    goto :goto_0

    .line 115
    :cond_9
    iget v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    iget v3, p1, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 116
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 71
    .line 73
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 75
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    iget-wide v4, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 76
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->e:I

    add-int/2addr v0, v2

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 79
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    add-int/2addr v0, v1

    .line 80
    return v0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    goto :goto_0

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; tok:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; feedtime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->d:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; status:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/sync/android/DbxNotificationHeader;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
