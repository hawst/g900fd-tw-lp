.class public final Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->a:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->c:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->b:Ljava/lang/String;

    .line 41
    iput-boolean p4, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->d:Z

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->d:Z

    return v0
.end method
