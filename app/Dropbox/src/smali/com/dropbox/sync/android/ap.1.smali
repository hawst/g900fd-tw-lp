.class final enum Lcom/dropbox/sync/android/ap;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/sync/android/ap;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/sync/android/ap;

.field public static final enum b:Lcom/dropbox/sync/android/ap;

.field public static final enum c:Lcom/dropbox/sync/android/ap;

.field private static final synthetic d:[Lcom/dropbox/sync/android/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/dropbox/sync/android/ap;

    const-string v1, "UNCHECKED"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    .line 71
    new-instance v0, Lcom/dropbox/sync/android/ap;

    const-string v1, "CHECKED"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/sync/android/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    .line 75
    new-instance v0, Lcom/dropbox/sync/android/ap;

    const-string v1, "INVALID_PATH"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/sync/android/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/sync/android/ap;

    sget-object v1, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/sync/android/ap;->d:[Lcom/dropbox/sync/android/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/sync/android/ap;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/dropbox/sync/android/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ap;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/sync/android/ap;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/dropbox/sync/android/ap;->d:[Lcom/dropbox/sync/android/ap;

    invoke-virtual {v0}, [Lcom/dropbox/sync/android/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/ap;

    return-object v0
.end method
