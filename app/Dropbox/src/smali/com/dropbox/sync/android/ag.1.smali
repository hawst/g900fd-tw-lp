.class final Lcom/dropbox/sync/android/ag;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field final c:J

.field final d:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'appKey\' shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    const-string v0, "appKey"

    invoke-static {p1, v0}, Lcom/dropbox/sync/android/be;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    if-eqz p2, :cond_1

    .line 87
    const-string v0, "appSecret"

    invoke-static {p2, v0}, Lcom/dropbox/sync/android/be;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_2

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'userCacheSizeLimit\' must be non-negative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_2
    iput-object p1, p0, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    .line 94
    iput-object p2, p0, Lcom/dropbox/sync/android/ag;->b:Ljava/lang/String;

    .line 95
    iput-wide p3, p0, Lcom/dropbox/sync/android/ag;->c:J

    .line 96
    iput-boolean p5, p0, Lcom/dropbox/sync/android/ag;->d:Z

    .line 97
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 106
    if-ne p0, p1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    instance-of v2, p1, Lcom/dropbox/sync/android/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_2
    check-cast p1, Lcom/dropbox/sync/android/ag;

    .line 113
    iget-object v2, p0, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/sync/android/ag;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/sync/android/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/dropbox/sync/android/U;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/dropbox/sync/android/ag;->c:J

    iget-wide v4, p1, Lcom/dropbox/sync/android/ag;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/dropbox/sync/android/ag;->d:Z

    iget-boolean v3, p1, Lcom/dropbox/sync/android/ag;->d:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 121
    .line 122
    iget-object v0, p0, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 123
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/dropbox/sync/android/ag;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/sync/android/U;->c(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/dropbox/sync/android/ag;->c:J

    iget-wide v3, p0, Lcom/dropbox/sync/android/ag;->c:J

    const/16 v5, 0x20

    shr-long/2addr v3, v5

    and-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 125
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/sync/android/ag;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 126
    return v0

    .line 125
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{appKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userCacheSizeLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/dropbox/sync/android/ag;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
