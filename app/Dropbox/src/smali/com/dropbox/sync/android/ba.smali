.class public final enum Lcom/dropbox/sync/android/ba;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/sync/android/ba;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/sync/android/ba;

.field public static final enum b:Lcom/dropbox/sync/android/ba;

.field public static final enum c:Lcom/dropbox/sync/android/ba;

.field private static final synthetic f:[Lcom/dropbox/sync/android/ba;


# instance fields
.field private final d:Z

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Lcom/dropbox/sync/android/ba;

    const-string v1, "FOREGROUND"

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/dropbox/sync/android/ba;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/dropbox/sync/android/ba;->a:Lcom/dropbox/sync/android/ba;

    .line 73
    new-instance v0, Lcom/dropbox/sync/android/ba;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/dropbox/sync/android/ba;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/dropbox/sync/android/ba;->b:Lcom/dropbox/sync/android/ba;

    .line 74
    new-instance v0, Lcom/dropbox/sync/android/ba;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4, v2, v2}, Lcom/dropbox/sync/android/ba;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/sync/android/ba;

    sget-object v1, Lcom/dropbox/sync/android/ba;->a:Lcom/dropbox/sync/android/ba;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/sync/android/ba;->b:Lcom/dropbox/sync/android/ba;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/sync/android/ba;->f:[Lcom/dropbox/sync/android/ba;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    iput-boolean p3, p0, Lcom/dropbox/sync/android/ba;->d:Z

    .line 81
    iput-boolean p4, p0, Lcom/dropbox/sync/android/ba;->e:Z

    .line 82
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/sync/android/ba;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/dropbox/sync/android/ba;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ba;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/sync/android/ba;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/dropbox/sync/android/ba;->f:[Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0}, [Lcom/dropbox/sync/android/ba;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/ba;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/dropbox/sync/android/ba;->d:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/dropbox/sync/android/ba;->e:Z

    return v0
.end method
