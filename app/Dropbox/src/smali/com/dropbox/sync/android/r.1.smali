.class final Lcom/dropbox/sync/android/r;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Lcom/dropbox/sync/android/ag;

.field public final b:Lcom/dropbox/sync/android/s;

.field public final c:Ljava/util/Locale;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/ag;Lcom/dropbox/sync/android/s;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "publicConfig shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iput-object p1, p0, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    .line 67
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "hosts shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    iput-object p2, p0, Lcom/dropbox/sync/android/r;->b:Lcom/dropbox/sync/android/s;

    .line 69
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/r;->c:Ljava/util/Locale;

    .line 70
    iget-object v0, p0, Lcom/dropbox/sync/android/r;->c:Ljava/util/Locale;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "locale shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_2
    iput-object p3, p0, Lcom/dropbox/sync/android/r;->d:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/dropbox/sync/android/r;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "userAgent shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_3
    iput-object p4, p0, Lcom/dropbox/sync/android/r;->e:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/dropbox/sync/android/r;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "appVersion shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_4
    iput-object p5, p0, Lcom/dropbox/sync/android/r;->f:Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lcom/dropbox/sync/android/r;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "deviceId shouldn\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_5
    iput-boolean p6, p0, Lcom/dropbox/sync/android/r;->g:Z

    .line 78
    return-void
.end method
