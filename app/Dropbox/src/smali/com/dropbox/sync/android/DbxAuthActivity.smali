.class public Lcom/dropbox/sync/android/DbxAuthActivity;
.super Landroid/app/Activity;
.source "panda.py"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/DbxAuthActivity;->a:Z

    return-void
.end method

.method private static a(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 65
    if-nez p0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Result intent can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    const-string v0, "ACCESS_TOKEN"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_1

    const-string v1, "oauth2:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 71
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid result intent passed in. Missing \"oauth2:\" tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 74
    :cond_2
    const-string v0, "ACCESS_SECRET"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    if-nez v0, :cond_3

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid result intent passed in. Missing OAuth 2 access token."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_3
    const-string v1, "UID"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 80
    if-nez v1, :cond_4

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid result intent passed in. Missing uid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_4
    invoke-static {}, Lcom/dropbox/sync/android/aa;->b()Z

    move-result v2

    if-nez v2, :cond_5

    .line 89
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    .line 92
    :cond_5
    new-instance v2, Lcom/dropbox/sync/android/bg;

    invoke-direct {v2, v0}, Lcom/dropbox/sync/android/bg;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-static {}, Lcom/dropbox/sync/android/aa;->a()Lcom/dropbox/sync/android/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aa;->f()Lcom/dropbox/sync/android/a;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/a;->a(Ljava/lang/String;Lcom/dropbox/sync/android/be;)V

    .line 94
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 40
    const-string v1, "EXTRA_REAL_INTENET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 41
    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/DbxAuthActivity;->startActivity(Landroid/content/Intent;)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/DbxAuthActivity;->a:Z

    .line 45
    :cond_0
    const v0, 0x1030010

    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/DbxAuthActivity;->setTheme(I)V

    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 52
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxAuthActivity;->a:Z

    if-nez v0, :cond_0

    .line 53
    sget-object v0, Lcom/dropbox/client2/android/AuthActivity;->a:Landroid/content/Intent;

    .line 54
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/dropbox/sync/android/DbxAuthActivity;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/sync/android/DbxAuthActivity;->setResult(ILandroid/content/Intent;)V

    .line 59
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxAuthActivity;->finish()V

    .line 61
    :cond_0
    iput-boolean v2, p0, Lcom/dropbox/sync/android/DbxAuthActivity;->a:Z

    .line 62
    return-void

    .line 57
    :cond_1
    invoke-virtual {p0, v2}, Lcom/dropbox/sync/android/DbxAuthActivity;->setResult(I)V

    goto :goto_0
.end method
