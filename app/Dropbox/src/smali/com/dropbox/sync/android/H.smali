.class final Lcom/dropbox/sync/android/H;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/sync/android/J;


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/NativeEnv;


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/NativeEnv;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/dropbox/sync/android/H;->a:Lcom/dropbox/sync/android/NativeEnv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 76
    :try_start_0
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/G;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting network status on global NativeEnv: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_0

    const-string v0, "online"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/dropbox/sync/android/H;->a:Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeEnv;->b(Z)V

    .line 82
    :goto_1
    return-void

    .line 76
    :cond_0
    const-string v0, "offline"
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/sync/android/G;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to set online status on global NativeEnv."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
