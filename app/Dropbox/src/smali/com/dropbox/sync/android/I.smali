.class Lcom/dropbox/sync/android/I;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static c:Lcom/dropbox/sync/android/I;


# instance fields
.field private final d:Lcom/dropbox/sync/android/G;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/sync/android/J;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dropbox/sync/android/I;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/I;->a:Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/I;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/I;->e:Ljava/util/Set;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/I;->f:Z

    .line 63
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/I;->d:Lcom/dropbox/sync/android/G;

    .line 64
    return-void
.end method

.method public static a()Lcom/dropbox/sync/android/I;
    .locals 2

    .prologue
    .line 54
    sget-object v1, Lcom/dropbox/sync/android/I;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    sget-object v0, Lcom/dropbox/sync/android/I;->c:Lcom/dropbox/sync/android/I;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/dropbox/sync/android/I;

    invoke-direct {v0}, Lcom/dropbox/sync/android/I;-><init>()V

    sput-object v0, Lcom/dropbox/sync/android/I;->c:Lcom/dropbox/sync/android/I;

    .line 58
    :cond_0
    sget-object v0, Lcom/dropbox/sync/android/I;->c:Lcom/dropbox/sync/android/I;

    monitor-exit v1

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 81
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 82
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 84
    :goto_0
    iget-object v2, p0, Lcom/dropbox/sync/android/I;->d:Lcom/dropbox/sync/android/G;

    sget-object v3, Lcom/dropbox/sync/android/I;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating network status: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v1, :cond_2

    const-string v0, "online"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const/4 v0, 0x0

    .line 87
    monitor-enter p0

    .line 88
    :try_start_0
    iget-boolean v2, p0, Lcom/dropbox/sync/android/I;->f:Z

    if-eq v1, v2, :cond_0

    .line 89
    iput-boolean v1, p0, Lcom/dropbox/sync/android/I;->f:Z

    .line 90
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/dropbox/sync/android/I;->e:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 92
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    if-eqz v0, :cond_3

    .line 95
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/J;

    .line 96
    invoke-interface {v0, v1}, Lcom/dropbox/sync/android/J;->a(Z)V

    goto :goto_2

    .line 83
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 84
    :cond_2
    const-string v0, "offline"

    goto :goto_1

    .line 92
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 99
    :cond_3
    return-void
.end method

.method public final declared-synchronized a(Lcom/dropbox/sync/android/J;)V
    .locals 1

    .prologue
    .line 113
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/I;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_0
    monitor-exit p0

    return-void

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/dropbox/sync/android/J;)V
    .locals 1

    .prologue
    .line 124
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/I;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/I;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
