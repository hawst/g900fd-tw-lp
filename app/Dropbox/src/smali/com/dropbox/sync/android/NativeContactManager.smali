.class Lcom/dropbox/sync/android/NativeContactManager;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 44
    invoke-static {}, Lcom/dropbox/sync/android/NativeContactManager;->nativeClassInit()V

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/V;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->g()Lcom/dropbox/sync/android/NativeApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeApp;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeContactManager;->a:J

    .line 17
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeContactManager;->a:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeContactManager;->nativeInit(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeContactManager;->b:J

    .line 18
    return-void
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeFetchContacts(J[Lcom/dropbox/sync/android/DbxContact;)V
.end method

.method private native nativeInit(J)J
.end method

.method private native nativeReleaseContactManagerHandle(J)V
.end method

.method private native nativeSearchContacts(JLjava/lang/String;Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;)[Lcom/dropbox/sync/android/DbxContact;
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeContactManager;->b:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeContactManager;->nativeReleaseContactManagerHandle(J)V

    .line 22
    return-void
.end method

.method public final a([Lcom/dropbox/sync/android/DbxContact;)V
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeContactManager;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/dropbox/sync/android/NativeContactManager;->nativeFetchContacts(J[Lcom/dropbox/sync/android/DbxContact;)V

    .line 27
    return-void
.end method

.method public final a(Ljava/lang/String;)[Lcom/dropbox/sync/android/DbxContact;
    .locals 4

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeContactManager;->b:J

    new-instance v2, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;-><init>(Lcom/dropbox/sync/android/bm;)V

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/dropbox/sync/android/NativeContactManager;->nativeSearchContacts(JLjava/lang/String;Lcom/dropbox/sync/android/NativeContactManager$ContactListBuilder;)[Lcom/dropbox/sync/android/DbxContact;

    move-result-object v0

    return-object v0
.end method
