.class public Lcom/dropbox/sync/android/NativeEnv$Config;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final apiHost:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final appKey:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final appSecret:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final contentHost:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final locale:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final logAppVersion:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final logDeviceId:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final logDir:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final logSystemModel:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final logSystemVersion:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final notifyHost:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final systemName:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field

.field public final webHost:Ljava/lang/String;
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/r;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "android"

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->systemName:Ljava/lang/String;

    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "coreConfig"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->b:Lcom/dropbox/sync/android/s;

    iget-object v0, v0, Lcom/dropbox/sync/android/s;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->apiHost:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->apiHost:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "apiHost"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->b:Lcom/dropbox/sync/android/s;

    iget-object v0, v0, Lcom/dropbox/sync/android/s;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->contentHost:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->contentHost:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "contentHost"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_2
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->b:Lcom/dropbox/sync/android/s;

    iget-object v0, v0, Lcom/dropbox/sync/android/s;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->webHost:Ljava/lang/String;

    .line 50
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->webHost:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "webHost"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_3
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->b:Lcom/dropbox/sync/android/s;

    iget-object v0, v0, Lcom/dropbox/sync/android/s;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->notifyHost:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->notifyHost:Ljava/lang/String;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "notifyHost"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_4
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    iget-object v0, v0, Lcom/dropbox/sync/android/ag;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->appKey:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->appKey:Ljava/lang/String;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "appKey"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_5
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->a:Lcom/dropbox/sync/android/ag;

    iget-object v0, v0, Lcom/dropbox/sync/android/ag;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->appSecret:Ljava/lang/String;

    .line 56
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 57
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Locale.getDefault()"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->locale:Ljava/lang/String;

    .line 59
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->a:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->a:Ljava/lang/String;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "userAgent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_7
    invoke-static {}, Lcom/dropbox/sync/android/f;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logSystemModel:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logSystemModel:Ljava/lang/String;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "logSystemModel"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_8
    invoke-static {}, Lcom/dropbox/sync/android/f;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logSystemVersion:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logSystemVersion:Ljava/lang/String;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "logSystemVersion"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_9
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logAppVersion:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logAppVersion:Ljava/lang/String;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "logAppVersion"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_a
    iget-object v0, p1, Lcom/dropbox/sync/android/r;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logDeviceId:Ljava/lang/String;

    .line 68
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logDeviceId:Ljava/lang/String;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "logDeviceId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_b
    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logDir:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv$Config;->logDir:Ljava/lang/String;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "logDeviceId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_c
    return-void
.end method
