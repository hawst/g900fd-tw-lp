.class Lcom/dropbox/sync/android/O;
.super Lcom/dropbox/sync/android/u;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/net/Proxy;

.field private final c:Lcom/dropbox/sync/android/G;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/sync/android/x;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-class v0, Lcom/dropbox/sync/android/O;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/O;->a:Ljava/lang/String;

    .line 420
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 421
    const-string v0, "http.keepAlive"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 423
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/O;-><init>(Ljava/net/Proxy;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/net/Proxy;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/dropbox/sync/android/u;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/O;->d:Ljava/util/Set;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/O;->e:Z

    .line 48
    iput-object p1, p0, Lcom/dropbox/sync/android/O;->b:Ljava/net/Proxy;

    .line 49
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/O;->c:Lcom/dropbox/sync/android/G;

    .line 50
    return-void
.end method

.method static synthetic a(Ljavax/net/ssl/HttpsURLConnection;)Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 25
    invoke-static {p0}, Lcom/dropbox/sync/android/O;->b(Ljavax/net/ssl/HttpsURLConnection;)Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/O;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dropbox/sync/android/O;->d()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/sync/android/O;Lcom/dropbox/sync/android/x;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/O;->a(Lcom/dropbox/sync/android/x;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/dropbox/sync/android/x;)V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/O;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/sync/android/O;)Lcom/dropbox/sync/android/G;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/dropbox/sync/android/O;->c:Lcom/dropbox/sync/android/G;

    return-object v0
.end method

.method private static b(Ljavax/net/ssl/HttpsURLConnection;)Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    .line 387
    invoke-virtual {p0}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/dropbox/sync/android/O;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/sync/android/O;)Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/dropbox/sync/android/O;->b:Ljava/net/Proxy;

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 2

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/O;->e:Z

    if-eqz v0, :cond_0

    .line 392
    new-instance v0, Lcom/dropbox/sync/android/P;

    const-string v1, "Request cancelled due to shutdown."

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/P;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 394
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 397
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/dropbox/sync/android/O;->e:Z

    .line 398
    iget-object v0, p0, Lcom/dropbox/sync/android/O;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/x;

    .line 399
    check-cast v0, Lcom/dropbox/sync/android/Q;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/Q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 401
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/O;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/dropbox/sync/android/x;
    .locals 2

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/dropbox/sync/android/Q;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/Q;-><init>(Lcom/dropbox/sync/android/O;)V

    .line 65
    iget-object v1, p0, Lcom/dropbox/sync/android/O;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/dropbox/sync/android/O;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit p0

    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
