.class public final enum Lcom/dropbox/sync/android/aI;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/sync/android/aI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/sync/android/aI;

.field public static final enum b:Lcom/dropbox/sync/android/aI;

.field public static final enum c:Lcom/dropbox/sync/android/aI;

.field private static final synthetic d:[Lcom/dropbox/sync/android/aI;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/dropbox/sync/android/aI;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/aI;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/aI;->a:Lcom/dropbox/sync/android/aI;

    .line 26
    new-instance v0, Lcom/dropbox/sync/android/aI;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/sync/android/aI;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/aI;->b:Lcom/dropbox/sync/android/aI;

    .line 31
    new-instance v0, Lcom/dropbox/sync/android/aI;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/sync/android/aI;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/sync/android/aI;->c:Lcom/dropbox/sync/android/aI;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/sync/android/aI;

    sget-object v1, Lcom/dropbox/sync/android/aI;->a:Lcom/dropbox/sync/android/aI;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/sync/android/aI;->b:Lcom/dropbox/sync/android/aI;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/sync/android/aI;->c:Lcom/dropbox/sync/android/aI;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/sync/android/aI;->d:[Lcom/dropbox/sync/android/aI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/sync/android/aI;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/dropbox/sync/android/aI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/aI;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/sync/android/aI;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dropbox/sync/android/aI;->d:[Lcom/dropbox/sync/android/aI;

    invoke-virtual {v0}, [Lcom/dropbox/sync/android/aI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/aI;

    return-object v0
.end method
