.class final Lcom/dropbox/sync/android/S;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/Q;

.field private final b:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method constructor <init>(Lcom/dropbox/sync/android/Q;Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/dropbox/sync/android/S;->a:Lcom/dropbox/sync/android/Q;

    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    .line 306
    iput-object p2, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    .line 307
    return-void
.end method


# virtual methods
.method public final createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    .line 323
    iget-object v1, p0, Lcom/dropbox/sync/android/S;->a:Lcom/dropbox/sync/android/Q;

    invoke-static {v1, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V

    .line 324
    return-object v0
.end method

.method public final createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    .line 342
    iget-object v1, p0, Lcom/dropbox/sync/android/S;->a:Lcom/dropbox/sync/android/Q;

    invoke-static {v1, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V

    .line 343
    return-object v0
.end method

.method public final createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    .line 331
    iget-object v1, p0, Lcom/dropbox/sync/android/S;->a:Lcom/dropbox/sync/android/Q;

    invoke-static {v1, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V

    .line 332
    return-object v0
.end method

.method public final createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    .line 351
    iget-object v1, p0, Lcom/dropbox/sync/android/S;->a:Lcom/dropbox/sync/android/Q;

    invoke-static {v1, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V

    .line 352
    return-object v0
.end method

.method public final createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    .line 359
    iget-object v1, p0, Lcom/dropbox/sync/android/S;->a:Lcom/dropbox/sync/android/Q;

    invoke-static {v1, v0}, Lcom/dropbox/sync/android/Q;->a(Lcom/dropbox/sync/android/Q;Ljava/net/Socket;)V

    .line 360
    return-object v0
.end method

.method public final getDefaultCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getDefaultCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/dropbox/sync/android/S;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
