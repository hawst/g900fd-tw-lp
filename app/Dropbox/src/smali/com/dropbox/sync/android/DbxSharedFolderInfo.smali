.class public final Lcom/dropbox/sync/android/DbxSharedFolderInfo;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

.field private final d:Z

.field private final e:Z

.field private final f:Lcom/dropbox/sync/android/DbxFileInfo;

.field private final g:Ljava/lang/String;

.field private final h:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;ZZLcom/dropbox/sync/android/DbxFileInfo;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;",
            ">;",
            "Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;",
            "ZZ",
            "Lcom/dropbox/sync/android/DbxFileInfo;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->a:Ljava/util/List;

    .line 64
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->b:Ljava/util/List;

    .line 65
    iput-object p3, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->c:Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    .line 66
    iput-boolean p4, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->d:Z

    .line 67
    iput-boolean p5, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->e:Z

    .line 68
    iput-object p6, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->f:Lcom/dropbox/sync/android/DbxFileInfo;

    .line 69
    iput-object p7, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->g:Ljava/lang/String;

    .line 70
    iput-boolean p8, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->h:Z

    .line 71
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->a:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->c:Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->e:Z

    return v0
.end method

.method public final f()Lcom/dropbox/sync/android/DbxFileInfo;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->f:Lcom/dropbox/sync/android/DbxFileInfo;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->h:Z

    return v0
.end method
