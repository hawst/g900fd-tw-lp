.class Lcom/dropbox/sync/android/z;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dropbox/sync/android/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/z;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/io/OutputStream;[BJJLcom/dropbox/sync/android/C;)V
    .locals 8

    .prologue
    .line 188
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    move-object v7, p7

    .line 193
    :try_start_1
    invoke-static/range {v0 .. v7}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BJJLcom/dropbox/sync/android/C;)V
    :try_end_1
    .catch Lcom/dropbox/sync/android/D; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 203
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    new-instance v1, Lcom/dropbox/sync/android/A;

    invoke-direct {v1, v0}, Lcom/dropbox/sync/android/A;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 199
    :catch_1
    move-exception v0

    .line 200
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/z;->a:Ljava/lang/String;

    const-string v3, "Failed to close FileInputStream: "

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 194
    :catch_2
    move-exception v1

    .line 195
    :try_start_3
    new-instance v2, Lcom/dropbox/sync/android/A;

    invoke-direct {v2, v1}, Lcom/dropbox/sync/android/A;-><init>(Lcom/dropbox/sync/android/D;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 197
    :catchall_0
    move-exception v1

    .line 198
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 201
    :goto_1
    throw v1

    .line 199
    :catch_3
    move-exception v0

    .line 200
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v2

    sget-object v3, Lcom/dropbox/sync/android/z;->a:Ljava/lang/String;

    const-string v4, "Failed to close FileInputStream: "

    invoke-virtual {v2, v3, v4, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 266
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_0
    return-void

    .line 268
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;J)V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    .line 58
    cmp-long v0, p1, v3

    if-gtz v0, :cond_3

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    const-wide/16 v0, 0x1

    .line 74
    :cond_2
    sub-long/2addr p1, v0

    .line 61
    :cond_3
    cmp-long v0, p1, v3

    if-lez v0, :cond_0

    .line 62
    invoke-virtual {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 66
    cmp-long v2, v0, v3

    if-ltz v2, :cond_0

    .line 68
    cmp-long v2, v0, v3

    if-nez v2, :cond_2

    .line 70
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/File;[BJJLcom/dropbox/sync/android/C;)V
    .locals 8

    .prologue
    .line 242
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, p0

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    move-object v7, p7

    .line 247
    :try_start_1
    invoke-static/range {v0 .. v7}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BJJLcom/dropbox/sync/android/C;)V
    :try_end_1
    .catch Lcom/dropbox/sync/android/F; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 257
    :goto_0
    return-void

    .line 243
    :catch_0
    move-exception v0

    .line 244
    new-instance v1, Lcom/dropbox/sync/android/B;

    invoke-direct {v1, v0}, Lcom/dropbox/sync/android/B;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 253
    :catch_1
    move-exception v0

    .line 254
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/z;->a:Ljava/lang/String;

    const-string v3, "Failed to close FileInputStream: "

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 248
    :catch_2
    move-exception v0

    .line 249
    :try_start_3
    new-instance v2, Lcom/dropbox/sync/android/B;

    invoke-direct {v2, v0}, Lcom/dropbox/sync/android/B;-><init>(Lcom/dropbox/sync/android/F;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 251
    :catchall_0
    move-exception v0

    .line 252
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 255
    :goto_1
    throw v0

    .line 253
    :catch_3
    move-exception v1

    .line 254
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v2

    sget-object v3, Lcom/dropbox/sync/android/z;->a:Ljava/lang/String;

    const-string v4, "Failed to close FileInputStream: "

    invoke-virtual {v2, v3, v4, v1}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/File;[BLcom/dropbox/sync/android/C;)V
    .locals 8

    .prologue
    .line 220
    const-wide/16 v3, 0x0

    const-wide v5, 0x7fffffffffffffffL

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/File;[BJJLcom/dropbox/sync/android/C;)V

    .line 221
    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;[BJJLcom/dropbox/sync/android/C;)V
    .locals 14

    .prologue
    .line 98
    :try_start_0
    move-wide/from16 v0, p3

    invoke-static {p0, v0, v1}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    const-wide/16 v4, 0x0

    .line 105
    const-wide/16 v8, 0x0

    .line 106
    const-wide/16 v2, 0x0

    .line 110
    :cond_0
    :goto_0
    const/4 v6, 0x0

    :try_start_1
    move-object/from16 v0, p2

    array-length v7, v0

    int-to-long v10, v7

    sub-long v12, p5, v4

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    long-to-int v7, v10

    move-object/from16 v0, p2

    invoke-virtual {p0, v0, v6, v7}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .line 114
    if-gtz v6, :cond_2

    .line 137
    if-eqz p7, :cond_1

    .line 138
    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 139
    move-object/from16 v0, p7

    invoke-interface {v0, v4, v5}, Lcom/dropbox/sync/android/C;->a(J)V

    .line 145
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 149
    return-void

    .line 99
    :catch_0
    move-exception v2

    .line 100
    new-instance v3, Lcom/dropbox/sync/android/D;

    invoke-direct {v3, v2}, Lcom/dropbox/sync/android/D;-><init>(Ljava/io/IOException;)V

    throw v3

    .line 111
    :catch_1
    move-exception v2

    .line 112
    new-instance v3, Lcom/dropbox/sync/android/D;

    invoke-direct {v3, v2}, Lcom/dropbox/sync/android/D;-><init>(Ljava/io/IOException;)V

    throw v3

    .line 118
    :cond_2
    const/4 v7, 0x0

    :try_start_3
    move-object/from16 v0, p2

    invoke-virtual {p1, v0, v7, v6}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 122
    int-to-long v6, v6

    add-long/2addr v4, v6

    .line 125
    if-eqz p7, :cond_0

    .line 126
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 127
    sub-long v10, v6, v8

    const-wide/16 v12, 0xa

    cmp-long v10, v10, v12

    if-ltz v10, :cond_3

    .line 130
    move-object/from16 v0, p7

    invoke-interface {v0, v4, v5}, Lcom/dropbox/sync/android/C;->a(J)V

    move-wide v2, v4

    :goto_1
    move-wide v8, v6

    .line 132
    goto :goto_0

    .line 119
    :catch_2
    move-exception v2

    .line 120
    new-instance v3, Lcom/dropbox/sync/android/F;

    invoke-direct {v3, v2}, Lcom/dropbox/sync/android/F;-><init>(Ljava/io/IOException;)V

    throw v3

    .line 146
    :catch_3
    move-exception v2

    .line 147
    new-instance v3, Lcom/dropbox/sync/android/F;

    invoke-direct {v3, v2}, Lcom/dropbox/sync/android/F;-><init>(Ljava/io/IOException;)V

    throw v3

    :cond_3
    move-wide v6, v8

    goto :goto_1
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;[BLcom/dropbox/sync/android/C;)V
    .locals 8

    .prologue
    .line 44
    const-wide/16 v3, 0x0

    const-wide v5, 0x7fffffffffffffffL

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/dropbox/sync/android/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BJJLcom/dropbox/sync/android/C;)V

    .line 45
    return-void
.end method
