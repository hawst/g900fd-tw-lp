.class Lcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/NativeNotificationManager;


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/NativeNotificationManager;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;->a:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366
    return-void
.end method


# virtual methods
.method public createStatus(ZZILjava/lang/String;ZILjava/lang/String;)Lcom/dropbox/sync/android/DbxNotificationSyncStatus;
    .locals 4
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 373
    :try_start_0
    new-instance v2, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;

    new-instance v3, Lcom/dropbox/sync/android/aS;

    if-nez p3, :cond_0

    move-object v1, v0

    :goto_0
    invoke-direct {v3, p2, v1}, Lcom/dropbox/sync/android/aS;-><init>(ZLcom/dropbox/sync/android/DbxException;)V

    new-instance v1, Lcom/dropbox/sync/android/aS;

    if-nez p6, :cond_1

    :goto_1
    invoke-direct {v1, p5, v0}, Lcom/dropbox/sync/android/aS;-><init>(ZLcom/dropbox/sync/android/DbxException;)V

    invoke-direct {v2, p1, v3, v1}, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;-><init>(ZLcom/dropbox/sync/android/aS;Lcom/dropbox/sync/android/aS;)V

    return-object v2

    :cond_0
    const-string v1, "notifications retrieve sync status"

    invoke-static {v1, p3, p4}, Lcom/dropbox/sync/android/DbxException;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v0, "op status"

    invoke-static {v0, p6, p7}, Lcom/dropbox/sync/android/DbxException;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/dropbox/sync/android/DbxException;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_1

    .line 379
    :catch_0
    move-exception v0

    .line 380
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;->a:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/NativeNotificationManager;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeNotificationManager;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 381
    throw v0

    .line 382
    :catch_1
    move-exception v0

    .line 383
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;->a:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-static {v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/NativeNotificationManager;)Lcom/dropbox/sync/android/G;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/sync/android/NativeNotificationManager;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 384
    throw v0
.end method
