.class Lcom/dropbox/sync/android/K;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dropbox/sync/android/K;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/K;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/security/SecureRandom;
    .locals 3

    .prologue
    .line 35
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-gt v0, v1, :cond_0

    .line 36
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v0

    sget-object v1, Lcom/dropbox/sync/android/K;->a:Ljava/lang/String;

    const-string v2, "Using LinuxPRNGSecureRandom to work around OpenSSL seeding issue."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/dropbox/sync/android/L;

    invoke-direct {v0}, Lcom/dropbox/sync/android/L;-><init>()V

    .line 40
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    goto :goto_0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dropbox/sync/android/K;->a:Ljava/lang/String;

    return-object v0
.end method
