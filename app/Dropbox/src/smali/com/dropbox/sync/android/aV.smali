.class public final Lcom/dropbox/sync/android/aV;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/dropbox/sync/android/V;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-direct {v0, p1}, Lcom/dropbox/sync/android/NativeSharedFolderManager;-><init>(Lcom/dropbox/sync/android/V;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/aV;->b:Z

    .line 25
    new-instance v0, Lcom/dropbox/sync/android/aW;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/aW;-><init>(Lcom/dropbox/sync/android/aV;)V

    invoke-virtual {p1, v0}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/Z;)V

    .line 33
    return-void
.end method

.method public static a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aV;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/dropbox/sync/android/aV;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/aV;-><init>(Lcom/dropbox/sync/android/V;)V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/aV;Z)Z
    .locals 0

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/dropbox/sync/android/aV;->b:Z

    return p1
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/aT;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/sync/android/aT;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/dropbox/sync/android/DbxSharedFolderInfo;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Lcom/dropbox/sync/android/aT;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/dropbox/sync/android/DbxSharedFolderInfo;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->a(Ljava/lang/String;Z)V

    .line 149
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->b(Ljava/lang/String;Z)V

    .line 161
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/dropbox/sync/android/aV;->a:Lcom/dropbox/sync/android/NativeSharedFolderManager;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/sync/android/NativeSharedFolderManager;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    return-object v0
.end method
