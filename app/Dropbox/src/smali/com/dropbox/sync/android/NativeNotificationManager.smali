.class Lcom/dropbox/sync/android/NativeNotificationManager;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/dropbox/sync/android/G;

.field private final d:Lcom/dropbox/sync/android/NativeNotificationManager$Config;

.field private final e:Lcom/dropbox/sync/android/NativeApp;

.field private final f:J

.field private final g:Lcom/dropbox/sync/android/NativeThreads;

.field private h:Lcom/dropbox/sync/android/bt;

.field private i:Z

.field private j:Lcom/dropbox/sync/android/bs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/NativeNotificationManager;->a:Z

    .line 18
    const-class v0, Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    .line 318
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeClassInit()V

    .line 320
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->h:Lcom/dropbox/sync/android/bt;

    .line 64
    iput-boolean v7, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z

    .line 65
    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;

    .line 79
    new-instance v0, Lcom/dropbox/sync/android/G;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/NativeApp;->b()Lcom/dropbox/sync/android/NativeEnv;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    .line 80
    new-instance v0, Lcom/dropbox/sync/android/NativeNotificationManager$Config;

    invoke-direct {v0, p2}, Lcom/dropbox/sync/android/NativeNotificationManager$Config;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->d:Lcom/dropbox/sync/android/NativeNotificationManager$Config;

    .line 81
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->e:Lcom/dropbox/sync/android/NativeApp;

    .line 82
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->d:Lcom/dropbox/sync/android/NativeNotificationManager$Config;

    invoke-direct {p0, p1, v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/NativeApp;Lcom/dropbox/sync/android/NativeNotificationManager$Config;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    .line 83
    new-instance v0, Lcom/dropbox/sync/android/NativeThreads;

    const-string v1, "NativeNotificationManager"

    invoke-direct {p0}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeGetRunFuncs()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    iget-object v6, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/sync/android/NativeThreads;-><init>(Ljava/lang/String;JJLcom/dropbox/sync/android/G;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    return-void

    .line 93
    :catchall_0
    move-exception v0

    .line 94
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v1, v2, v7}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeDeinit(JZ)V

    .line 93
    throw v0
.end method

.method private a(Lcom/dropbox/sync/android/NativeApp;Lcom/dropbox/sync/android/NativeNotificationManager$Config;)J
    .locals 4

    .prologue
    .line 102
    new-instance v0, Ljava/io/File;

    iget-object v1, p2, Lcom/dropbox/sync/android/NativeNotificationManager$Config;->cacheRoot:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    invoke-static {v0}, Lcom/dropbox/sync/android/t;->b(Ljava/io/File;)V

    .line 104
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Prepared cache dir \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/dropbox/sync/android/NativeNotificationManager$Config;->cacheRoot:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1}, Lcom/dropbox/sync/android/NativeApp;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p2}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeInit(JLcom/dropbox/sync/android/NativeNotificationManager$Config;)J

    move-result-wide v0

    .line 108
    sget-boolean v2, Lcom/dropbox/sync/android/NativeNotificationManager;->a:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid native client handle."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 112
    :cond_0
    return-wide v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/NativeNotificationManager;)Lcom/dropbox/sync/android/G;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    return-object v0
.end method

.method private native nativeAckNotifications(J[J)V
.end method

.method private native nativeAwaitFirstSync(J)V
.end method

.method private native nativeBlockingUpdate(J)V
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeDeinit(JZ)V
.end method

.method private native nativeFree(J)V
.end method

.method private native nativeGetRunFuncs()J
.end method

.method private native nativeGetSyncStatus(JLcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;)Lcom/dropbox/sync/android/DbxNotificationSyncStatus;
.end method

.method private native nativeInit(JLcom/dropbox/sync/android/NativeNotificationManager$Config;)J
.end method

.method private native nativeListNotifications(JLcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;)I
.end method

.method private native nativeSetOrClearNotificationCallback(JZ)V
.end method

.method private native nativeSetOrClearSyncStatusCallback(JZ)V
.end method

.method private native nativeTakeFreshNotifications(JLcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;)I
.end method

.method private notificationChangeCallback()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 288
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 289
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;

    .line 290
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    :try_start_2
    invoke-interface {v0}, Lcom/dropbox/sync/android/bs;->a()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 290
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1

    .line 294
    :catch_0
    move-exception v0

    .line 295
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 296
    :catch_1
    move-exception v0

    .line 297
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncStatusCallback()V
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccessInternal;
    .end annotation

    .prologue
    .line 270
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 271
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->h:Lcom/dropbox/sync/android/bt;

    .line 272
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273
    if-eqz v0, :cond_0

    .line 274
    :try_start_2
    invoke-interface {v0}, Lcom/dropbox/sync/android/bt;->a()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 272
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1

    .line 276
    :catch_0
    move-exception v0

    .line 277
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0

    .line 278
    :catch_1
    move-exception v0

    .line 279
    iget-object v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeAwaitFirstSync(J)V

    .line 174
    return-void
.end method

.method public final declared-synchronized a(Lcom/dropbox/sync/android/bs;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 202
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 221
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 206
    :cond_1
    if-nez p1, :cond_2

    .line 207
    :try_start_1
    iget-object v2, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;

    if-eqz v2, :cond_5

    .line 209
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;

    move v2, v0

    .line 218
    :goto_1
    if-eqz v2, :cond_0

    .line 219
    iget-wide v2, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    if-eqz p1, :cond_3

    :goto_2
    invoke-direct {p0, v2, v3, v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeSetOrClearNotificationCallback(JZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 212
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;

    if-nez v2, :cond_4

    move v2, v0

    .line 215
    :goto_3
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 219
    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move v2, v1

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/dropbox/sync/android/bt;)V
    .locals 3

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z

    if-eqz v0, :cond_0

    .line 187
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->h:Lcom/dropbox/sync/android/bt;

    .line 188
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v2, v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeSetOrClearSyncStatusCallback(JZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :cond_0
    monitor-exit p0

    return-void

    .line 188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 126
    monitor-enter p0

    .line 127
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z

    if-nez v0, :cond_0

    .line 128
    monitor-exit p0

    .line 138
    :goto_0
    return-void

    .line 130
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->j:Lcom/dropbox/sync/android/bs;

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->h:Lcom/dropbox/sync/android/bt;

    .line 135
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->b()V

    .line 137
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v0, v1, p1}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeDeinit(JZ)V

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a([J)V
    .locals 2

    .prologue
    .line 248
    if-nez p1, :cond_0

    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "nids must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_0
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v0, v1, p1}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeAckNotifications(J[J)V

    .line 252
    return-void
.end method

.method public final b()Lcom/dropbox/sync/android/DbxNotificationSyncStatus;
    .locals 3

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    new-instance v2, Lcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;

    invoke-direct {v2, p0}, Lcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;-><init>(Lcom/dropbox/sync/android/NativeNotificationManager;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeGetSyncStatus(JLcom/dropbox/sync/android/NativeNotificationManager$SyncStatusBuilder;)Lcom/dropbox/sync/android/DbxNotificationSyncStatus;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/dropbox/sync/android/aO;
    .locals 3

    .prologue
    .line 236
    new-instance v0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;-><init>(Lcom/dropbox/sync/android/NativeNotificationManager;Lcom/dropbox/sync/android/br;)V

    .line 237
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v1, v2, v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeListNotifications(JLcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;)I

    .line 238
    new-instance v1, Lcom/dropbox/sync/android/aO;

    iget-object v2, v0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->a:Ljava/util/List;

    iget-boolean v0, v0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->b:Z

    invoke-direct {v1, v2, v0}, Lcom/dropbox/sync/android/aO;-><init>(Ljava/util/List;Z)V

    return-object v1
.end method

.method public final d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/A/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    new-instance v0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;-><init>(Lcom/dropbox/sync/android/NativeNotificationManager;Lcom/dropbox/sync/android/br;)V

    .line 243
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v1, v2, v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeTakeFreshNotifications(JLcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;)I

    .line 244
    iget-object v0, v0, Lcom/dropbox/sync/android/NativeNotificationManager$NotificationBuilder;->a:Ljava/util/List;

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 262
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeBlockingUpdate(J)V

    .line 263
    return-void
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->i:Z

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->c:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeNotificationManager;->b:Ljava/lang/String;

    const-string v2, "NativeNotificationManager finalized without being deinitialized."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->c()V

    .line 149
    :cond_1
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeNotificationManager;->f:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->nativeFree(J)V

    goto :goto_0
.end method
