.class abstract Lcom/dropbox/sync/android/p;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field protected final a:Lcom/dropbox/sync/android/V;

.field private final b:Ljava/io/File;


# direct methods
.method protected constructor <init>(Lcom/dropbox/sync/android/V;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/sync/android/h;->a(Z)V

    .line 55
    iput-object p1, p0, Lcom/dropbox/sync/android/p;->a:Lcom/dropbox/sync/android/V;

    .line 57
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cache dir must exist before constructing a CoreClient"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_1
    iput-object p2, p0, Lcom/dropbox/sync/android/p;->b:Ljava/io/File;

    .line 61
    return-void
.end method


# virtual methods
.method protected final a()Ljava/io/File;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/sync/android/p;->b:Ljava/io/File;

    return-object v0
.end method

.method abstract a(Z)V
.end method

.method abstract b()Lcom/dropbox/sync/android/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/sync/android/q",
            "<+",
            "Lcom/dropbox/sync/android/p;",
            ">;"
        }
    .end annotation
.end method
