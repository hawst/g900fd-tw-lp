.class final Lcom/dropbox/sync/android/s;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Lcom/dropbox/sync/android/s;

.field private static final f:Lcom/dropbox/sync/android/s;

.field private static final g:Lcom/dropbox/sync/android/s;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 100
    new-instance v0, Lcom/dropbox/sync/android/s;

    const-string v1, "api.dropbox.com"

    const-string v2, "api-content.dropbox.com"

    const-string v3, "www.dropbox.com"

    const-string v4, "api-notify.dropbox.com"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/sync/android/s;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/sync/android/s;->f:Lcom/dropbox/sync/android/s;

    .line 101
    new-instance v0, Lcom/dropbox/sync/android/s;

    const-string v1, "api-dbdev.dev.corp.dropbox.com"

    const-string v2, "api-content-dbdev.dev.corp.dropbox.com"

    const-string v3, "meta-dbdev.dev.corp.dropbox.com"

    const-string v4, "api-dbdev.dev.corp.dropbox.com"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/sync/android/s;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/sync/android/s;->g:Lcom/dropbox/sync/android/s;

    .line 107
    sget-object v0, Lcom/dropbox/sync/android/s;->f:Lcom/dropbox/sync/android/s;

    sput-object v0, Lcom/dropbox/sync/android/s;->a:Lcom/dropbox/sync/android/s;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'api\' shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'content\' shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'web\' shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_2
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'notify\' shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_3
    iput-object p1, p0, Lcom/dropbox/sync/android/s;->b:Ljava/lang/String;

    .line 147
    iput-object p2, p0, Lcom/dropbox/sync/android/s;->c:Ljava/lang/String;

    .line 148
    iput-object p3, p0, Lcom/dropbox/sync/android/s;->d:Ljava/lang/String;

    .line 149
    iput-object p4, p0, Lcom/dropbox/sync/android/s;->e:Ljava/lang/String;

    .line 150
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 159
    if-ne p0, p1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    instance-of v2, p1, Lcom/dropbox/sync/android/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 163
    goto :goto_0

    .line 165
    :cond_2
    check-cast p1, Lcom/dropbox/sync/android/s;

    .line 166
    iget-object v2, p0, Lcom/dropbox/sync/android/s;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/s;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/sync/android/s;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/sync/android/s;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/sync/android/s;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 173
    .line 174
    iget-object v0, p0, Lcom/dropbox/sync/android/s;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 175
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/dropbox/sync/android/s;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/dropbox/sync/android/s;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{api="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/s;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/sync/android/U;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", content="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/s;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/sync/android/U;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", web="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/s;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/sync/android/U;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
