.class final enum Lcom/dropbox/sync/android/ao;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/sync/android/ao;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/dropbox/sync/android/ao;

.field public static final enum B:Lcom/dropbox/sync/android/ao;

.field public static final enum C:Lcom/dropbox/sync/android/ao;

.field public static final enum D:Lcom/dropbox/sync/android/ao;

.field public static final enum E:Lcom/dropbox/sync/android/ao;

.field public static final enum F:Lcom/dropbox/sync/android/ao;

.field public static final enum G:Lcom/dropbox/sync/android/ao;

.field public static final enum H:Lcom/dropbox/sync/android/ao;

.field public static final enum I:Lcom/dropbox/sync/android/ao;

.field public static final enum J:Lcom/dropbox/sync/android/ao;

.field public static final enum K:Lcom/dropbox/sync/android/ao;

.field public static final enum L:Lcom/dropbox/sync/android/ao;

.field public static final enum M:Lcom/dropbox/sync/android/ao;

.field private static final N:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dropbox/sync/android/ao;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic R:[Lcom/dropbox/sync/android/ao;

.field public static final enum a:Lcom/dropbox/sync/android/ao;

.field public static final enum b:Lcom/dropbox/sync/android/ao;

.field public static final enum c:Lcom/dropbox/sync/android/ao;

.field public static final enum d:Lcom/dropbox/sync/android/ao;

.field public static final enum e:Lcom/dropbox/sync/android/ao;

.field public static final enum f:Lcom/dropbox/sync/android/ao;

.field public static final enum g:Lcom/dropbox/sync/android/ao;

.field public static final enum h:Lcom/dropbox/sync/android/ao;

.field public static final enum i:Lcom/dropbox/sync/android/ao;

.field public static final enum j:Lcom/dropbox/sync/android/ao;

.field public static final enum k:Lcom/dropbox/sync/android/ao;

.field public static final enum l:Lcom/dropbox/sync/android/ao;

.field public static final enum m:Lcom/dropbox/sync/android/ao;

.field public static final enum n:Lcom/dropbox/sync/android/ao;

.field public static final enum o:Lcom/dropbox/sync/android/ao;

.field public static final enum p:Lcom/dropbox/sync/android/ao;

.field public static final enum q:Lcom/dropbox/sync/android/ao;

.field public static final enum r:Lcom/dropbox/sync/android/ao;

.field public static final enum s:Lcom/dropbox/sync/android/ao;

.field public static final enum t:Lcom/dropbox/sync/android/ao;

.field public static final enum u:Lcom/dropbox/sync/android/ao;

.field public static final enum v:Lcom/dropbox/sync/android/ao;

.field public static final enum w:Lcom/dropbox/sync/android/ao;

.field public static final enum x:Lcom/dropbox/sync/android/ao;

.field public static final enum y:Lcom/dropbox/sync/android/ao;

.field public static final enum z:Lcom/dropbox/sync/android/ao;


# instance fields
.field private final O:I

.field private final P:Lcom/dropbox/sync/android/ap;

.field private final Q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/dropbox/sync/android/ao;

    const-string v1, "INTERNAL"

    const/16 v3, -0x3e8

    sget-object v4, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v5, "Internal error"

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/sync/android/ao;->a:Lcom/dropbox/sync/android/ao;

    .line 14
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "CACHE"

    const/16 v6, -0x3e9

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Cache error"

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->b:Lcom/dropbox/sync/android/ao;

    .line 15
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SHUTDOWN"

    const/16 v6, -0x3ea

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Already shut down"

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->c:Lcom/dropbox/sync/android/ao;

    .line 16
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "CLOSED"

    const/16 v6, -0x3eb

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Already closed"

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->d:Lcom/dropbox/sync/android/ao;

    .line 17
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "DELETED"

    const/16 v6, -0x3ec

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Deleted"

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->e:Lcom/dropbox/sync/android/ao;

    .line 18
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "BAD_TYPE"

    const/4 v5, 0x5

    const/16 v6, -0x3ef

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Value is of the wrong type"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->f:Lcom/dropbox/sync/android/ao;

    .line 19
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SIZE_LIMIT"

    const/4 v5, 0x6

    const/16 v6, -0x3f0

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Size limit exceeded"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->g:Lcom/dropbox/sync/android/ao;

    .line 20
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "BAD_INDEX"

    const/4 v5, 0x7

    const/16 v6, -0x3f1

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Index is out of bounds"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->h:Lcom/dropbox/sync/android/ao;

    .line 21
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "ILLEGAL_ARGUMENT"

    const/16 v5, 0x8

    const/16 v6, -0x3f2

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Illegal argument"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->i:Lcom/dropbox/sync/android/ao;

    .line 23
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "MEMORY"

    const/16 v5, 0x9

    const/16 v6, -0x76c

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Out of memory"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->j:Lcom/dropbox/sync/android/ao;

    .line 24
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SYSTEM"

    const/16 v5, 0xa

    const/16 v6, -0x76d

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "System error"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->k:Lcom/dropbox/sync/android/ao;

    .line 26
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "NOT_CACHED"

    const/16 v5, 0xb

    const/16 v6, -0x7d0

    sget-object v7, Lcom/dropbox/sync/android/ap;->a:Lcom/dropbox/sync/android/ap;

    const-string v8, "Not cache"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->l:Lcom/dropbox/sync/android/ao;

    .line 28
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "INVALID_OPERATION"

    const/16 v5, 0xc

    const/16 v6, -0x2710

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Invalid operation attempted"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->m:Lcom/dropbox/sync/android/ao;

    .line 29
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "NOT_FOUND"

    const/16 v5, 0xd

    const/16 v6, -0x2711

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Path doesn\'t exist"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->n:Lcom/dropbox/sync/android/ao;

    .line 30
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "EXISTS"

    const/16 v5, 0xe

    const/16 v6, -0x2712

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "File already exists"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->o:Lcom/dropbox/sync/android/ao;

    .line 31
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "ALREADY_OPEN"

    const/16 v5, 0xf

    const/16 v6, -0x2713

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "File is already open"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->p:Lcom/dropbox/sync/android/ao;

    .line 32
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "PARENT"

    const/16 v5, 0x10

    const/16 v6, -0x2714

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Parent path doesn\'t exist or isn\'t a directory"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->q:Lcom/dropbox/sync/android/ao;

    .line 33
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "DISKSPACE"

    const/16 v5, 0x11

    const/16 v6, -0x2716

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Out of disk space"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->r:Lcom/dropbox/sync/android/ao;

    .line 34
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "DISALLOWED"

    const/16 v5, 0x12

    const/16 v6, -0x2717

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "App is not allowed access"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->s:Lcom/dropbox/sync/android/ao;

    .line 36
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "NETWORK"

    const/16 v5, 0x13

    const/16 v6, -0x2af8

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Network error"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->t:Lcom/dropbox/sync/android/ao;

    .line 37
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "TIMEOUT"

    const/16 v5, 0x14

    const/16 v6, -0x2af9

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Network timeout"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->u:Lcom/dropbox/sync/android/ao;

    .line 38
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "CONNECTION"

    const/16 v5, 0x15

    const/16 v6, -0x2afa

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "No network connection"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->v:Lcom/dropbox/sync/android/ao;

    .line 39
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SSL"

    const/16 v5, 0x16

    const/16 v6, -0x2afb

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "SSL error"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->w:Lcom/dropbox/sync/android/ao;

    .line 40
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SERVER"

    const/16 v5, 0x17

    const/16 v6, -0x2afc

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Server error"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->x:Lcom/dropbox/sync/android/ao;

    .line 41
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "AUTH"

    const/16 v5, 0x18

    const/16 v6, -0x2afd

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Not authenticated"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->y:Lcom/dropbox/sync/android/ao;

    .line 42
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "QUOTA"

    const/16 v5, 0x19

    const/16 v6, -0x2afe

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "Quota exceeded"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->z:Lcom/dropbox/sync/android/ao;

    .line 44
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "NO_THUMB"

    const/16 v5, 0x1a

    const/16 v6, -0x2ee0

    sget-object v7, Lcom/dropbox/sync/android/ap;->b:Lcom/dropbox/sync/android/ap;

    const-string v8, "No thumbnail"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->A:Lcom/dropbox/sync/android/ao;

    .line 47
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "UTF8_ERROR"

    const/16 v5, 0x1b

    const/16 v6, -0x1389

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Illegal UTF-8 encoding"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->B:Lcom/dropbox/sync/android/ao;

    .line 48
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "RESERVED_NAME"

    const/16 v5, 0x1c

    const/16 v6, -0x138a

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Filename reserved"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->C:Lcom/dropbox/sync/android/ao;

    .line 49
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "ASTRAL_PLANE"

    const/16 v5, 0x1d

    const/16 v6, -0x138b

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Illegal character (outside the BMP)"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->D:Lcom/dropbox/sync/android/ao;

    .line 50
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SURROGATE"

    const/16 v5, 0x1e

    const/16 v6, -0x138c

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Illegal character (surrogate pair codepoint)"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->E:Lcom/dropbox/sync/android/ao;

    .line 51
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "NONCHARACTER"

    const/16 v5, 0x1f

    const/16 v6, -0x138d

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Illegal character (byte order mark)"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->F:Lcom/dropbox/sync/android/ao;

    .line 52
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "ENDS_WITH_SPACE"

    const/16 v5, 0x20

    const/16 v6, -0x138e

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Path component tends with space"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->G:Lcom/dropbox/sync/android/ao;

    .line 53
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "DOT"

    const/16 v5, 0x21

    const/16 v6, -0x138f

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "\'.\' isn\'t a legal path component"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->H:Lcom/dropbox/sync/android/ao;

    .line 54
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "DOTDOT"

    const/16 v5, 0x22

    const/16 v6, -0x1390

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "\'..\' isn\'t a legal path component"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->I:Lcom/dropbox/sync/android/ao;

    .line 55
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "BACKSLASH"

    const/16 v5, 0x23

    const/16 v6, -0x1391

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Backslash in path"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->J:Lcom/dropbox/sync/android/ao;

    .line 56
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "SLASHSLASH"

    const/16 v5, 0x24

    const/16 v6, -0x1392

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Empty component or double slash"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->K:Lcom/dropbox/sync/android/ao;

    .line 57
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "COMPONENT_LENGTH"

    const/16 v5, 0x25

    const/16 v6, -0x1393

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Path component longer than 255 characters"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->L:Lcom/dropbox/sync/android/ao;

    .line 58
    new-instance v3, Lcom/dropbox/sync/android/ao;

    const-string v4, "PATH_NULL"

    const/16 v5, 0x26

    const/16 v6, -0x1395

    sget-object v7, Lcom/dropbox/sync/android/ap;->c:Lcom/dropbox/sync/android/ap;

    const-string v8, "Path value is null"

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/sync/android/ao;-><init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V

    sput-object v3, Lcom/dropbox/sync/android/ao;->M:Lcom/dropbox/sync/android/ao;

    .line 11
    const/16 v0, 0x27

    new-array v0, v0, [Lcom/dropbox/sync/android/ao;

    sget-object v1, Lcom/dropbox/sync/android/ao;->a:Lcom/dropbox/sync/android/ao;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/sync/android/ao;->b:Lcom/dropbox/sync/android/ao;

    aput-object v1, v0, v9

    sget-object v1, Lcom/dropbox/sync/android/ao;->c:Lcom/dropbox/sync/android/ao;

    aput-object v1, v0, v10

    sget-object v1, Lcom/dropbox/sync/android/ao;->d:Lcom/dropbox/sync/android/ao;

    aput-object v1, v0, v11

    sget-object v1, Lcom/dropbox/sync/android/ao;->e:Lcom/dropbox/sync/android/ao;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/sync/android/ao;->f:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/sync/android/ao;->g:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/sync/android/ao;->h:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/sync/android/ao;->i:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/sync/android/ao;->j:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/sync/android/ao;->k:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dropbox/sync/android/ao;->l:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/sync/android/ao;->m:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dropbox/sync/android/ao;->n:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dropbox/sync/android/ao;->o:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dropbox/sync/android/ao;->p:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dropbox/sync/android/ao;->q:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/dropbox/sync/android/ao;->r:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dropbox/sync/android/ao;->s:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/dropbox/sync/android/ao;->t:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/dropbox/sync/android/ao;->u:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/dropbox/sync/android/ao;->v:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/dropbox/sync/android/ao;->w:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/dropbox/sync/android/ao;->x:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/dropbox/sync/android/ao;->y:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/dropbox/sync/android/ao;->z:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/dropbox/sync/android/ao;->A:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/dropbox/sync/android/ao;->B:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/dropbox/sync/android/ao;->C:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/dropbox/sync/android/ao;->D:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/dropbox/sync/android/ao;->E:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/dropbox/sync/android/ao;->F:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/dropbox/sync/android/ao;->G:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/dropbox/sync/android/ao;->H:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/dropbox/sync/android/ao;->I:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/dropbox/sync/android/ao;->J:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/dropbox/sync/android/ao;->K:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/dropbox/sync/android/ao;->L:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/dropbox/sync/android/ao;->M:Lcom/dropbox/sync/android/ao;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/sync/android/ao;->R:[Lcom/dropbox/sync/android/ao;

    .line 78
    invoke-static {}, Lcom/dropbox/sync/android/ao;->c()Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/ao;->N:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/dropbox/sync/android/ap;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/dropbox/sync/android/ap;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    iput p3, p0, Lcom/dropbox/sync/android/ao;->O:I

    .line 86
    iput-object p4, p0, Lcom/dropbox/sync/android/ao;->P:Lcom/dropbox/sync/android/ap;

    .line 87
    iput-object p5, p0, Lcom/dropbox/sync/android/ao;->Q:Ljava/lang/String;

    .line 88
    return-void
.end method

.method static a(I)Lcom/dropbox/sync/android/ao;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/dropbox/sync/android/ao;->N:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ao;

    .line 128
    if-eqz v0, :cond_0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/dropbox/sync/android/ao;->a:Lcom/dropbox/sync/android/ao;

    goto :goto_0
.end method

.method private static c()Landroid/util/SparseArray;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dropbox/sync/android/ao;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v1, Landroid/util/SparseArray;

    invoke-static {}, Lcom/dropbox/sync/android/ao;->values()[Lcom/dropbox/sync/android/ao;

    move-result-object v0

    array-length v0, v0

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 136
    invoke-static {}, Lcom/dropbox/sync/android/ao;->values()[Lcom/dropbox/sync/android/ao;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 137
    invoke-virtual {v4}, Lcom/dropbox/sync/android/ao;->a()I

    move-result v5

    invoke-virtual {v1, v5, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/sync/android/ao;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/dropbox/sync/android/ao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ao;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/sync/android/ao;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/dropbox/sync/android/ao;->R:[Lcom/dropbox/sync/android/ao;

    invoke-virtual {v0}, [Lcom/dropbox/sync/android/ao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/sync/android/ao;

    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/dropbox/sync/android/ao;->O:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/dropbox/sync/android/ao;->Q:Ljava/lang/String;

    return-object v0
.end method
