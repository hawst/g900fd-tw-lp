.class public Lcom/dropbox/sync/android/aJ;
.super Lcom/dropbox/sync/android/p;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Lcom/dropbox/sync/android/aP;


# instance fields
.field private final d:Lcom/dropbox/sync/android/G;

.field private final e:Lcom/dropbox/sync/android/NativeNotificationManager;

.field private final f:Lcom/dropbox/sync/android/aY;

.field private final g:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/dropbox/sync/android/aR;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private final i:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/dropbox/sync/android/aQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    const-class v0, Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/aJ;->b:Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/dropbox/sync/android/aP;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/aP;-><init>(Lcom/dropbox/sync/android/aK;)V

    sput-object v0, Lcom/dropbox/sync/android/aJ;->c:Lcom/dropbox/sync/android/aP;

    return-void
.end method

.method private constructor <init>(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    invoke-direct {p0, p1, p3}, Lcom/dropbox/sync/android/p;-><init>(Lcom/dropbox/sync/android/V;Ljava/io/File;)V

    .line 76
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/aJ;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 78
    iput-boolean v4, p0, Lcom/dropbox/sync/android/aJ;->h:Z

    .line 79
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 105
    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->d()Lcom/dropbox/sync/android/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/aJ;->d:Lcom/dropbox/sync/android/G;

    .line 106
    new-instance v0, Lcom/dropbox/sync/android/aY;

    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->f()Lcom/dropbox/sync/android/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/sync/android/a;->e()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/sync/android/aY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/aJ;->f:Lcom/dropbox/sync/android/aY;

    .line 107
    new-instance v0, Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {p0}, Lcom/dropbox/sync/android/aJ;->a()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;-><init>(Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    .line 108
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->d:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/aJ;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Created DbxNotificationManager for uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    new-instance v1, Lcom/dropbox/sync/android/aK;

    invoke-direct {v1, p0}, Lcom/dropbox/sync/android/aK;-><init>(Lcom/dropbox/sync/android/aJ;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/bt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/aJ;->h:Z

    .line 124
    return-void

    .line 119
    :catchall_0
    move-exception v0

    .line 120
    iget-object v1, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v1, v4}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Z)V

    .line 119
    throw v0
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;Lcom/dropbox/sync/android/aK;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/aJ;-><init>(Lcom/dropbox/sync/android/V;Lcom/dropbox/sync/android/NativeApp;Ljava/io/File;)V

    return-void
.end method

.method public static a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aJ;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/dropbox/sync/android/aJ;->c:Lcom/dropbox/sync/android/aP;

    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/q;)Lcom/dropbox/sync/android/p;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/aJ;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/aJ;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/sync/android/aJ;->h()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/sync/android/aJ;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/sync/android/aJ;->g()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 404
    new-instance v1, Lcom/dropbox/sync/android/aM;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/sync/android/aM;-><init>(Lcom/dropbox/sync/android/aJ;Ljava/util/Iterator;)V

    .line 413
    invoke-static {v1}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/Runnable;)V

    .line 414
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 418
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->b()Lcom/dropbox/sync/android/DbxNotificationSyncStatus;

    move-result-object v1

    .line 419
    sget-object v0, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    .line 420
    invoke-virtual {v1}, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 421
    sget-object v0, Lcom/dropbox/sync/android/ba;->a:Lcom/dropbox/sync/android/ba;

    .line 425
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/dropbox/sync/android/aJ;->f:Lcom/dropbox/sync/android/aY;

    invoke-virtual {v1, v0}, Lcom/dropbox/sync/android/aY;->a(Lcom/dropbox/sync/android/ba;)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    :goto_1
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 432
    new-instance v1, Lcom/dropbox/sync/android/aN;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/sync/android/aN;-><init>(Lcom/dropbox/sync/android/aJ;Ljava/util/Iterator;)V

    invoke-static {v1}, Lcom/dropbox/sync/android/f;->a(Ljava/lang/Runnable;)V

    .line 440
    return-void

    .line 422
    :cond_1
    :try_start_1
    iget-boolean v1, v1, Lcom/dropbox/sync/android/DbxNotificationSyncStatus;->a:Z

    if-eqz v1, :cond_0

    .line 423
    sget-object v0, Lcom/dropbox/sync/android/ba;->b:Lcom/dropbox/sync/android/ba;
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    iget-object v1, p0, Lcom/dropbox/sync/android/aJ;->d:Lcom/dropbox/sync/android/G;

    sget-object v2, Lcom/dropbox/sync/android/aJ;->b:Ljava/lang/String;

    const-string v3, "Failed to get sync status in callback."

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/sync/android/G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/dropbox/sync/android/aQ;)V
    .locals 2

    .prologue
    .line 362
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 363
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/dropbox/sync/android/aJ;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 376
    :goto_0
    monitor-exit p0

    return-void

    .line 367
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    new-instance v1, Lcom/dropbox/sync/android/aL;

    invoke-direct {v1, p0}, Lcom/dropbox/sync/android/aL;-><init>(Lcom/dropbox/sync/android/aJ;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/bs;)V

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final a(Z)V
    .locals 4

    .prologue
    .line 170
    monitor-enter p0

    .line 171
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/aJ;->h:Z

    if-nez v0, :cond_0

    .line 172
    monitor-exit p0

    .line 191
    :goto_0
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/aJ;->h:Z

    .line 179
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->d:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/aJ;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing DbxNotificationManager for uid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/sync/android/aJ;->a:Lcom/dropbox/sync/android/V;

    invoke-virtual {v3}, Lcom/dropbox/sync/android/V;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Z)V

    .line 181
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->a:Lcom/dropbox/sync/android/V;

    invoke-virtual {v0, p0}, Lcom/dropbox/sync/android/V;->a(Lcom/dropbox/sync/android/p;)V

    .line 182
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 185
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->f:Lcom/dropbox/sync/android/aY;

    sget-object v1, Lcom/dropbox/sync/android/ba;->c:Lcom/dropbox/sync/android/ba;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aY;->a(Lcom/dropbox/sync/android/ba;)V

    .line 187
    monitor-enter p0

    .line 188
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 189
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 190
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 182
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a([J)V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0, p1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a([J)V

    .line 331
    return-void
.end method

.method final b()Lcom/dropbox/sync/android/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/sync/android/q",
            "<+",
            "Lcom/dropbox/sync/android/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    sget-object v0, Lcom/dropbox/sync/android/aJ;->c:Lcom/dropbox/sync/android/aP;

    return-object v0
.end method

.method public final declared-synchronized b(Lcom/dropbox/sync/android/aQ;)V
    .locals 2

    .prologue
    .line 389
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 390
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/dropbox/sync/android/aJ;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    .line 397
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 394
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/NativeNotificationManager;->a(Lcom/dropbox/sync/android/bs;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/sync/android/aJ;->a(Z)V

    .line 145
    return-void
.end method

.method public final d()Lcom/dropbox/sync/android/aO;
    .locals 3

    .prologue
    .line 299
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->a()V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :goto_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->c()Lcom/dropbox/sync/android/aO;

    move-result-object v0

    return-object v0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->d:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/aJ;->b:Ljava/lang/String;

    const-string v2, "First sync failed.  Listing notifications with cached contents."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/A/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->a()V
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    :goto_0
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->d()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->d:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/aJ;->b:Ljava/lang/String;

    const-string v2, "First sync failed."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/dropbox/sync/android/aJ;->e:Lcom/dropbox/sync/android/NativeNotificationManager;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeNotificationManager;->e()V

    .line 343
    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/dropbox/sync/android/aJ;->h:Z

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lcom/dropbox/sync/android/aJ;->c()V

    .line 202
    :cond_0
    return-void
.end method
