.class Lcom/dropbox/sync/android/NativeEnv;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
.end annotation


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/dropbox/sync/android/NativeLib;

.field private final d:Lcom/dropbox/sync/android/G;

.field private final e:Lcom/dropbox/sync/android/NativeEnv$Config;

.field private final f:J

.field private final g:Lcom/dropbox/sync/android/NativeThreads;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/sync/android/NativeEnv;->a:Z

    .line 14
    const-class v0, Lcom/dropbox/sync/android/NativeEnv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/sync/android/NativeEnv;->b:Ljava/lang/String;

    .line 301
    const-string v0, "DropboxSync"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 302
    invoke-static {}, Lcom/dropbox/sync/android/NativeEnv;->nativeClassInit()V

    .line 303
    return-void

    .line 12
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/dropbox/sync/android/NativeLib;Lcom/dropbox/sync/android/r;Ljava/io/File;)V
    .locals 2
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/dropbox/sync/android/NativeEnv;->c:Lcom/dropbox/sync/android/NativeLib;

    .line 97
    new-instance v0, Lcom/dropbox/sync/android/G;

    invoke-direct {v0, p0}, Lcom/dropbox/sync/android/G;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->d:Lcom/dropbox/sync/android/G;

    .line 98
    new-instance v0, Lcom/dropbox/sync/android/NativeEnv$Config;

    invoke-direct {v0, p2, p3}, Lcom/dropbox/sync/android/NativeEnv$Config;-><init>(Lcom/dropbox/sync/android/r;Ljava/io/File;)V

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->e:Lcom/dropbox/sync/android/NativeEnv$Config;

    .line 99
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->e:Lcom/dropbox/sync/android/NativeEnv$Config;

    invoke-direct {p0, v0}, Lcom/dropbox/sync/android/NativeEnv;->a(Lcom/dropbox/sync/android/NativeEnv$Config;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    .line 100
    invoke-direct {p0}, Lcom/dropbox/sync/android/NativeEnv;->f()Lcom/dropbox/sync/android/NativeThreads;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->g:Lcom/dropbox/sync/android/NativeThreads;

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeEnv;->h:Z

    .line 102
    return-void
.end method

.method private a(Lcom/dropbox/sync/android/NativeEnv$Config;)J
    .locals 4

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/NativeEnv;->nativeInit(Lcom/dropbox/sync/android/NativeEnv$Config;)J

    move-result-wide v0

    .line 107
    sget-boolean v2, Lcom/dropbox/sync/android/NativeEnv;->a:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid native app handle."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 109
    :cond_0
    return-wide v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/NativeEnv;)J
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    return-wide v0
.end method

.method static synthetic a(Lcom/dropbox/sync/android/NativeEnv;JI)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/sync/android/NativeEnv;->nativeLogUploadThread(JI)V

    return-void
.end method

.method private f()Lcom/dropbox/sync/android/NativeThreads;
    .locals 4

    .prologue
    .line 262
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 263
    new-instance v1, Lcom/dropbox/sync/android/bn;

    invoke-direct {v1, p0}, Lcom/dropbox/sync/android/bn;-><init>(Lcom/dropbox/sync/android/NativeEnv;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    new-instance v1, Lcom/dropbox/sync/android/NativeThreads;

    const-string v2, "NativeEnv:logUpload"

    iget-object v3, p0, Lcom/dropbox/sync/android/NativeEnv;->d:Lcom/dropbox/sync/android/G;

    invoke-direct {v1, v2, v0, v3}, Lcom/dropbox/sync/android/NativeThreads;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/dropbox/sync/android/G;)V

    return-object v1
.end method

.method private static isMainThread()Z
    .locals 3
    .annotation build Lcom/dropbox/sync/android/annotations/JniAccess;
    .end annotation

    .prologue
    .line 279
    :try_start_0
    invoke-static {}, Lcom/dropbox/sync/android/f;->a()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 285
    :goto_0
    return v0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/NativeEnv;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    .line 285
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 282
    :catch_1
    move-exception v0

    .line 283
    invoke-static {}, Lcom/dropbox/sync/android/G;->b()Lcom/dropbox/sync/android/G;

    move-result-object v1

    sget-object v2, Lcom/dropbox/sync/android/NativeEnv;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/dropbox/sync/android/h;->a(Ljava/lang/Throwable;Lcom/dropbox/sync/android/G;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeDeinit(JZ)V
.end method

.method private native nativeFree(J)V
.end method

.method private native nativeInit(Lcom/dropbox/sync/android/NativeEnv$Config;)J
.end method

.method private native nativeLog(JILjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeLogUploadThread(JI)V
.end method

.method private native nativeSetError(JIILjava/lang/String;)Z
.end method

.method private native nativeSetOnline(JZ)V
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->a()V

    .line 152
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/dropbox/sync/android/NativeEnv;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    move-object v0, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeEnv;->nativeLog(JILjava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 123
    monitor-enter p0

    .line 124
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeEnv;->h:Z

    if-nez v0, :cond_0

    .line 125
    monitor-exit p0

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/sync/android/NativeEnv;->h:Z

    .line 128
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->b()V

    .line 130
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    invoke-direct {p0, v0, v1, p1}, Lcom/dropbox/sync/android/NativeEnv;->nativeDeinit(JZ)V

    .line 131
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->g:Lcom/dropbox/sync/android/NativeThreads;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/NativeThreads;->c()V

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/dropbox/sync/android/ao;ILjava/lang/String;)Z
    .locals 6

    .prologue
    .line 226
    iget-wide v1, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    invoke-virtual {p1}, Lcom/dropbox/sync/android/ao;->a()I

    move-result v3

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/sync/android/NativeEnv;->nativeSetError(JIILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    invoke-direct {p0, v0, v1, p1}, Lcom/dropbox/sync/android/NativeEnv;->nativeSetOnline(JZ)V

    .line 259
    return-void
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeEnv;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    return-wide v0
.end method

.method public final d()Lcom/dropbox/sync/android/NativeEnv$Config;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->e:Lcom/dropbox/sync/android/NativeEnv$Config;

    return-object v0
.end method

.method public final e()Lcom/dropbox/sync/android/u;
    .locals 1

    .prologue
    .line 198
    new-instance v0, Lcom/dropbox/sync/android/O;

    invoke-direct {v0}, Lcom/dropbox/sync/android/O;-><init>()V

    return-object v0
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/dropbox/sync/android/NativeEnv;->h:Z

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/dropbox/sync/android/NativeEnv;->d:Lcom/dropbox/sync/android/G;

    sget-object v1, Lcom/dropbox/sync/android/NativeEnv;->b:Ljava/lang/String;

    const-string v2, "NativeEnv finalized without being deinitialized."

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-wide v0, p0, Lcom/dropbox/sync/android/NativeEnv;->f:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/sync/android/NativeEnv;->nativeFree(J)V

    goto :goto_0
.end method
