.class final Lcom/dropbox/sync/android/bc;
.super Landroid/content/BroadcastReceiver;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/sync/android/DbxSyncService;


# direct methods
.method private constructor <init>(Lcom/dropbox/sync/android/DbxSyncService;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/dropbox/sync/android/bc;->a:Lcom/dropbox/sync/android/DbxSyncService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/sync/android/DbxSyncService;Lcom/dropbox/sync/android/aX;)V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/dropbox/sync/android/bc;-><init>(Lcom/dropbox/sync/android/DbxSyncService;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 286
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 287
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 295
    iget-object v1, p0, Lcom/dropbox/sync/android/bc;->a:Lcom/dropbox/sync/android/DbxSyncService;

    invoke-virtual {v1, p0, v0}, Lcom/dropbox/sync/android/DbxSyncService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 296
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/dropbox/sync/android/bc;->a:Lcom/dropbox/sync/android/DbxSyncService;

    invoke-virtual {v0, p0}, Lcom/dropbox/sync/android/DbxSyncService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 300
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 279
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 280
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    :cond_0
    invoke-static {}, Lcom/dropbox/sync/android/I;->a()Lcom/dropbox/sync/android/I;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/sync/android/bc;->a:Lcom/dropbox/sync/android/DbxSyncService;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/I;->a(Landroid/content/Context;)V

    .line 283
    :cond_1
    return-void
.end method
