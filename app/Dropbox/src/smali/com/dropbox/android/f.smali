.class final Lcom/dropbox/android/f;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/r/e;

.field final synthetic b:Lcom/dropbox/android/gcm/GcmSubscriber;

.field final synthetic c:Ldbxyzptlk/db231222/n/k;

.field final synthetic d:Lcom/dropbox/android/DropboxApplication;


# direct methods
.method constructor <init>(Lcom/dropbox/android/DropboxApplication;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;Ldbxyzptlk/db231222/n/k;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/dropbox/android/f;->d:Lcom/dropbox/android/DropboxApplication;

    iput-object p2, p0, Lcom/dropbox/android/f;->a:Ldbxyzptlk/db231222/r/e;

    iput-object p3, p0, Lcom/dropbox/android/f;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    iput-object p4, p0, Lcom/dropbox/android/f;->c:Ldbxyzptlk/db231222/n/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/f;->a:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_3

    .line 156
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 157
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->b()V

    .line 159
    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->c()Ldbxyzptlk/db231222/k/b;

    move-result-object v0

    const-wide/32 v3, 0xf731400

    invoke-virtual {v0, v3, v4}, Ldbxyzptlk/db231222/k/b;->a(J)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V

    .line 186
    :cond_0
    :goto_1
    return-void

    .line 161
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/f;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/gcm/GcmSubscriber;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 162
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->g()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/f;->d:Lcom/dropbox/android/DropboxApplication;

    invoke-static {v0, v2}, Lcom/dropbox/android/filemanager/au;->a(Ldbxyzptlk/db231222/n/K;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    invoke-static {}, Lcom/dropbox/android/service/ReportReceiver;->a()V

    .line 166
    :cond_2
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_3

    .line 168
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/i;->b()V

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/f;->c:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    new-instance v0, Lcom/dropbox/android/util/analytics/u;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/u;-><init>()V

    .line 175
    iget-object v1, p0, Lcom/dropbox/android/f;->d:Lcom/dropbox/android/DropboxApplication;

    invoke-virtual {v1}, Lcom/dropbox/android/DropboxApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/filemanager/ad;->a()[Lcom/dropbox/android/filemanager/ad;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/u;->a(Landroid/content/ContentResolver;[Lcom/dropbox/android/filemanager/ad;)Lcom/dropbox/android/util/analytics/v;

    move-result-object v0

    .line 177
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bQ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "photos-count"

    iget v3, v0, Lcom/dropbox/android/util/analytics/v;->a:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "video-count"

    iget v3, v0, Lcom/dropbox/android/util/analytics/v;->b:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "screenshot-count"

    iget v3, v0, Lcom/dropbox/android/util/analytics/v;->c:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "total"

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/v;->a()I

    move-result v0

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/f;->c:Ldbxyzptlk/db231222/n/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->a(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
