.class public Lcom/dropbox/android/DropboxApplication;
.super Lcom/github/droidfu/DroidFuApplication;
.source "panda.py"


# static fields
.field protected static final a:Ljava/lang/Object;

.field protected static b:Z

.field private static c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/DropboxApplication;->a:Ljava/lang/Object;

    .line 45
    sput-boolean v1, Lcom/dropbox/android/DropboxApplication;->b:Z

    .line 47
    sput-boolean v1, Lcom/dropbox/android/DropboxApplication;->c:Z

    .line 51
    const-string v0, "org.joda.time.DateTimeZone.Provider"

    const-class v1, Lcom/dropbox/android/util/aq;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/github/droidfu/DroidFuApplication;-><init>()V

    return-void
.end method

.method private a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;Ldbxyzptlk/db231222/n/k;)V
    .locals 4

    .prologue
    .line 150
    new-instance v0, Lcom/dropbox/android/f;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/dropbox/android/f;-><init>(Lcom/dropbox/android/DropboxApplication;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;Ldbxyzptlk/db231222/n/k;)V

    const-wide/16 v1, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2, v3}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 188
    return-void
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 206
    const-string v0, "main"

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    sget-object v1, Lcom/dropbox/android/DropboxApplication;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :goto_0
    :try_start_0
    sget-boolean v0, Lcom/dropbox/android/DropboxApplication;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 210
    :try_start_1
    sget-object v0, Lcom/dropbox/android/DropboxApplication;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    goto :goto_0

    .line 214
    :cond_0
    :try_start_2
    monitor-exit v1

    .line 216
    :cond_1
    return-void

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 191
    sget-object v1, Lcom/dropbox/android/DropboxApplication;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 192
    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lcom/dropbox/android/DropboxApplication;->b:Z

    .line 193
    sget-object v0, Lcom/dropbox/android/DropboxApplication;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 194
    monitor-exit v1

    .line 195
    return-void

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreate()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 57
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v0

    .line 58
    const-class v1, Lcom/dropbox/android/DropboxApplication;

    monitor-enter v1

    .line 63
    :try_start_0
    sget-boolean v2, Lcom/dropbox/android/DropboxApplication;->c:Z

    if-eqz v2, :cond_0

    .line 64
    invoke-super {p0}, Lcom/github/droidfu/DroidFuApplication;->onCreate()V

    .line 65
    monitor-exit v1

    .line 139
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/dropbox/android/DropboxApplication;->c:Z

    .line 69
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    invoke-static {}, Lcom/dropbox/android/g;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    new-instance v1, Lcom/dropbox/android/DropboxModule;

    invoke-direct {v1}, Lcom/dropbox/android/DropboxModule;-><init>()V

    invoke-static {v1}, Lcom/dropbox/android/g;->b(Ljava/lang/Object;)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/DropboxApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/a;->a(Landroid/content/Context;)V

    .line 80
    invoke-static {p0}, Lcom/dropbox/android/util/h;->a(Landroid/content/Context;)V

    .line 85
    invoke-static {p0}, Ldbxyzptlk/db231222/i/d;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    .line 86
    invoke-static {p0}, Lcom/dropbox/android/service/G;->a(Landroid/content/Context;)Lcom/dropbox/android/service/G;

    move-result-object v2

    .line 87
    invoke-static {p0}, Lcom/dropbox/android/util/bl;->a(Landroid/content/Context;)V

    .line 89
    invoke-static {p0}, Ldbxyzptlk/db231222/n/k;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/n/k;

    move-result-object v3

    .line 91
    invoke-virtual {v1, v3}, Ldbxyzptlk/db231222/i/d;->a(Ldbxyzptlk/db231222/n/k;)V

    .line 93
    invoke-static {p0, v3}, Ldbxyzptlk/db231222/n/K;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;)Ldbxyzptlk/db231222/n/K;

    move-result-object v4

    .line 94
    new-instance v5, Ldbxyzptlk/db231222/r/i;

    new-instance v6, Lcom/dropbox/android/provider/m;

    invoke-direct {v6, p0}, Lcom/dropbox/android/provider/m;-><init>(Landroid/content/Context;)V

    invoke-direct {v5, p0, v3, v4, v6}, Ldbxyzptlk/db231222/r/i;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/n/K;Lcom/dropbox/android/provider/m;)V

    .line 96
    new-instance v6, Lcom/dropbox/sync/android/bz;

    invoke-direct {v6, p0}, Lcom/dropbox/sync/android/bz;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v5, v6}, Ldbxyzptlk/db231222/r/e;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/h;Lcom/dropbox/sync/android/bz;)Ldbxyzptlk/db231222/r/e;

    move-result-object v5

    .line 97
    invoke-virtual {v1, v5}, Ldbxyzptlk/db231222/i/d;->a(Ldbxyzptlk/db231222/r/e;)V

    .line 98
    invoke-static {p0, v5, v2}, Lcom/dropbox/android/gcm/GcmSubscriber;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/service/G;)Lcom/dropbox/android/gcm/GcmSubscriber;

    move-result-object v2

    .line 99
    invoke-static {p0, v3, v5, v2}, Lcom/dropbox/android/filemanager/a;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;)Lcom/dropbox/android/filemanager/a;

    move-result-object v6

    .line 100
    invoke-static {p0}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;)V

    .line 103
    invoke-static {p0}, Ldbxyzptlk/db231222/k/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v7

    invoke-static {v7}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/io/File;)V

    .line 105
    invoke-virtual {v1}, Ldbxyzptlk/db231222/i/d;->c()V

    .line 111
    invoke-static {p0, v5}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;)V

    .line 113
    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/dropbox/android/payments/DbxSubscriptions;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/k;)V

    .line 115
    invoke-super {p0}, Lcom/github/droidfu/DroidFuApplication;->onCreate()V

    .line 117
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v7

    .line 118
    invoke-virtual {v7, p0, v4}, Lcom/dropbox/android/filemanager/au;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/K;)V

    .line 120
    invoke-virtual {v6}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v4

    invoke-static {v3, v7, v4}, Lcom/dropbox/android/util/analytics/r;->a(Ldbxyzptlk/db231222/n/k;Lcom/dropbox/android/filemanager/au;Ldbxyzptlk/db231222/z/F;)Lcom/dropbox/android/util/analytics/r;

    move-result-object v4

    .line 125
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "mobile-remote-installer"

    invoke-virtual {v4, v6}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/e;->e()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 126
    :cond_2
    const-string v6, "mobile-remote-installer"

    const-string v7, "NOT_IN_EXPERIMENT"

    invoke-virtual {v4, v6, v7}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_3
    new-instance v6, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v6, v8}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    .line 131
    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v7

    invoke-virtual {v1}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v1

    invoke-virtual {v4, v7, v1, v6}, Lcom/dropbox/android/util/analytics/r;->a(Ldbxyzptlk/db231222/r/k;Ldbxyzptlk/db231222/i/c;Ljava/util/concurrent/Executor;)V

    .line 133
    invoke-direct {p0, v6, v5, v2, v3}, Lcom/dropbox/android/DropboxApplication;->a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;Ldbxyzptlk/db231222/n/k;)V

    .line 134
    invoke-virtual {v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/android/DropboxApplication;->a()V

    .line 138
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bD()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_0

    .line 69
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
