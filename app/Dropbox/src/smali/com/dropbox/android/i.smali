.class public final Lcom/dropbox/android/i;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final ab_back_albums:I = 0x7f020000

.field public static final ab_unshare:I = 0x7f020001

.field public static final abs__ab_bottom_solid_dark_holo:I = 0x7f020002

.field public static final abs__ab_bottom_solid_inverse_holo:I = 0x7f020003

.field public static final abs__ab_bottom_solid_light_holo:I = 0x7f020004

.field public static final abs__ab_bottom_transparent_dark_holo:I = 0x7f020005

.field public static final abs__ab_bottom_transparent_light_holo:I = 0x7f020006

.field public static final abs__ab_share_pack_holo_dark:I = 0x7f020007

.field public static final abs__ab_share_pack_holo_light:I = 0x7f020008

.field public static final abs__ab_solid_dark_holo:I = 0x7f020009

.field public static final abs__ab_solid_light_holo:I = 0x7f02000a

.field public static final abs__ab_solid_shadow_holo:I = 0x7f02000b

.field public static final abs__ab_stacked_solid_dark_holo:I = 0x7f02000c

.field public static final abs__ab_stacked_solid_light_holo:I = 0x7f02000d

.field public static final abs__ab_stacked_transparent_dark_holo:I = 0x7f02000e

.field public static final abs__ab_stacked_transparent_light_holo:I = 0x7f02000f

.field public static final abs__ab_transparent_dark_holo:I = 0x7f020010

.field public static final abs__ab_transparent_light_holo:I = 0x7f020011

.field public static final abs__activated_background_holo_dark:I = 0x7f020012

.field public static final abs__activated_background_holo_light:I = 0x7f020013

.field public static final abs__btn_cab_done_default_holo_dark:I = 0x7f020014

.field public static final abs__btn_cab_done_default_holo_light:I = 0x7f020015

.field public static final abs__btn_cab_done_focused_holo_dark:I = 0x7f020016

.field public static final abs__btn_cab_done_focused_holo_light:I = 0x7f020017

.field public static final abs__btn_cab_done_holo_dark:I = 0x7f020018

.field public static final abs__btn_cab_done_holo_light:I = 0x7f020019

.field public static final abs__btn_cab_done_pressed_holo_dark:I = 0x7f02001a

.field public static final abs__btn_cab_done_pressed_holo_light:I = 0x7f02001b

.field public static final abs__cab_background_bottom_holo_dark:I = 0x7f02001c

.field public static final abs__cab_background_bottom_holo_light:I = 0x7f02001d

.field public static final abs__cab_background_top_holo_dark:I = 0x7f02001e

.field public static final abs__cab_background_top_holo_light:I = 0x7f02001f

.field public static final abs__ic_ab_back_holo_dark:I = 0x7f020020

.field public static final abs__ic_ab_back_holo_light:I = 0x7f020021

.field public static final abs__ic_cab_done_holo_dark:I = 0x7f020022

.field public static final abs__ic_cab_done_holo_light:I = 0x7f020023

.field public static final abs__ic_clear:I = 0x7f020024

.field public static final abs__ic_clear_disabled:I = 0x7f020025

.field public static final abs__ic_clear_holo_light:I = 0x7f020026

.field public static final abs__ic_clear_normal:I = 0x7f020027

.field public static final abs__ic_clear_search_api_disabled_holo_light:I = 0x7f020028

.field public static final abs__ic_clear_search_api_holo_light:I = 0x7f020029

.field public static final abs__ic_commit_search_api_holo_dark:I = 0x7f02002a

.field public static final abs__ic_commit_search_api_holo_light:I = 0x7f02002b

.field public static final abs__ic_go:I = 0x7f02002c

.field public static final abs__ic_go_search_api_holo_light:I = 0x7f02002d

.field public static final abs__ic_menu_moreoverflow_holo_dark:I = 0x7f02002e

.field public static final abs__ic_menu_moreoverflow_holo_light:I = 0x7f02002f

.field public static final abs__ic_menu_moreoverflow_normal_holo_dark:I = 0x7f020030

.field public static final abs__ic_menu_moreoverflow_normal_holo_light:I = 0x7f020031

.field public static final abs__ic_menu_share_holo_dark:I = 0x7f020032

.field public static final abs__ic_menu_share_holo_light:I = 0x7f020033

.field public static final abs__ic_search:I = 0x7f020034

.field public static final abs__ic_search_api_holo_light:I = 0x7f020035

.field public static final abs__ic_voice_search:I = 0x7f020036

.field public static final abs__ic_voice_search_api_holo_light:I = 0x7f020037

.field public static final abs__item_background_holo_dark:I = 0x7f020038

.field public static final abs__item_background_holo_light:I = 0x7f020039

.field public static final abs__list_activated_holo:I = 0x7f02003a

.field public static final abs__list_divider_holo_dark:I = 0x7f02003b

.field public static final abs__list_divider_holo_light:I = 0x7f02003c

.field public static final abs__list_focused_holo:I = 0x7f02003d

.field public static final abs__list_longpressed_holo:I = 0x7f02003e

.field public static final abs__list_pressed_holo_dark:I = 0x7f02003f

.field public static final abs__list_pressed_holo_light:I = 0x7f020040

.field public static final abs__list_selector_background_transition_holo_dark:I = 0x7f020041

.field public static final abs__list_selector_background_transition_holo_light:I = 0x7f020042

.field public static final abs__list_selector_disabled_holo_dark:I = 0x7f020043

.field public static final abs__list_selector_disabled_holo_light:I = 0x7f020044

.field public static final abs__list_selector_holo_dark:I = 0x7f020045

.field public static final abs__list_selector_holo_light:I = 0x7f020046

.field public static final abs__menu_dropdown_panel_holo_dark:I = 0x7f020047

.field public static final abs__menu_dropdown_panel_holo_light:I = 0x7f020048

.field public static final abs__progress_bg_holo_dark:I = 0x7f020049

.field public static final abs__progress_bg_holo_light:I = 0x7f02004a

.field public static final abs__progress_horizontal_holo_dark:I = 0x7f02004b

.field public static final abs__progress_horizontal_holo_light:I = 0x7f02004c

.field public static final abs__progress_medium_holo:I = 0x7f02004d

.field public static final abs__progress_primary_holo_dark:I = 0x7f02004e

.field public static final abs__progress_primary_holo_light:I = 0x7f02004f

.field public static final abs__progress_secondary_holo_dark:I = 0x7f020050

.field public static final abs__progress_secondary_holo_light:I = 0x7f020051

.field public static final abs__search_dropdown_dark:I = 0x7f020052

.field public static final abs__search_dropdown_light:I = 0x7f020053

.field public static final abs__spinner_48_inner_holo:I = 0x7f020054

.field public static final abs__spinner_48_outer_holo:I = 0x7f020055

.field public static final abs__spinner_ab_default_holo_dark:I = 0x7f020056

.field public static final abs__spinner_ab_default_holo_light:I = 0x7f020057

.field public static final abs__spinner_ab_disabled_holo_dark:I = 0x7f020058

.field public static final abs__spinner_ab_disabled_holo_light:I = 0x7f020059

.field public static final abs__spinner_ab_focused_holo_dark:I = 0x7f02005a

.field public static final abs__spinner_ab_focused_holo_light:I = 0x7f02005b

.field public static final abs__spinner_ab_holo_dark:I = 0x7f02005c

.field public static final abs__spinner_ab_holo_light:I = 0x7f02005d

.field public static final abs__spinner_ab_pressed_holo_dark:I = 0x7f02005e

.field public static final abs__spinner_ab_pressed_holo_light:I = 0x7f02005f

.field public static final abs__tab_indicator_ab_holo:I = 0x7f020060

.field public static final abs__tab_selected_focused_holo:I = 0x7f020061

.field public static final abs__tab_selected_holo:I = 0x7f020062

.field public static final abs__tab_selected_pressed_holo:I = 0x7f020063

.field public static final abs__tab_unselected_pressed_holo:I = 0x7f020064

.field public static final abs__textfield_search_default_holo_dark:I = 0x7f020065

.field public static final abs__textfield_search_default_holo_light:I = 0x7f020066

.field public static final abs__textfield_search_right_default_holo_dark:I = 0x7f020067

.field public static final abs__textfield_search_right_default_holo_light:I = 0x7f020068

.field public static final abs__textfield_search_right_selected_holo_dark:I = 0x7f020069

.field public static final abs__textfield_search_right_selected_holo_light:I = 0x7f02006a

.field public static final abs__textfield_search_selected_holo_dark:I = 0x7f02006b

.field public static final abs__textfield_search_selected_holo_light:I = 0x7f02006c

.field public static final abs__textfield_searchview_holo_dark:I = 0x7f02006d

.field public static final abs__textfield_searchview_holo_light:I = 0x7f02006e

.field public static final abs__textfield_searchview_right_holo_dark:I = 0x7f02006f

.field public static final abs__textfield_searchview_right_holo_light:I = 0x7f020070

.field public static final abs__toast_frame:I = 0x7f020071

.field public static final actionbar_alert:I = 0x7f020072

.field public static final actionbar_bg:I = 0x7f020073

.field public static final actionbar_bg_flipped:I = 0x7f020074

.field public static final actionmode_bg:I = 0x7f020075

.field public static final actionmode_bg_flipped:I = 0x7f020076

.field public static final add_folder:I = 0x7f020077

.field public static final add_folder_light:I = 0x7f020078

.field public static final album_list_divider_holo_light:I = 0x7f020079

.field public static final all_done:I = 0x7f02007a

.field public static final battery:I = 0x7f02007b

.field public static final bg_separator:I = 0x7f02007c

.field public static final bg_separator_inset:I = 0x7f02007d

.field public static final black:I = 0x7f02007e

.field public static final blue_gradient_background:I = 0x7f02007f

.field public static final browser:I = 0x7f020080

.field public static final btn_camera_upload:I = 0x7f020081

.field public static final btn_check:I = 0x7f020082

.field public static final btn_check_blue:I = 0x7f020083

.field public static final btn_check_label_background:I = 0x7f020084

.field public static final btn_check_light:I = 0x7f020085

.field public static final btn_check_light_off:I = 0x7f020086

.field public static final btn_check_light_off_pressed:I = 0x7f020087

.field public static final btn_check_light_off_selected:I = 0x7f020088

.field public static final btn_check_light_on:I = 0x7f020089

.field public static final btn_check_light_on_pressed:I = 0x7f02008a

.field public static final btn_check_light_on_selected:I = 0x7f02008b

.field public static final btn_check_off:I = 0x7f02008c

.field public static final btn_check_off_pressed:I = 0x7f02008d

.field public static final btn_check_off_selected:I = 0x7f02008e

.field public static final btn_check_on:I = 0x7f02008f

.field public static final btn_check_on_pressed:I = 0x7f020090

.field public static final btn_check_on_selected:I = 0x7f020091

.field public static final btn_radio:I = 0x7f020092

.field public static final btn_radio_blue:I = 0x7f020093

.field public static final btn_radio_blue_off:I = 0x7f020094

.field public static final btn_radio_blue_off_pressed:I = 0x7f020095

.field public static final btn_radio_blue_off_selected:I = 0x7f020096

.field public static final btn_radio_blue_on:I = 0x7f020097

.field public static final btn_radio_blue_on_pressed:I = 0x7f020098

.field public static final btn_radio_blue_on_selected:I = 0x7f020099

.field public static final btn_radio_label_background:I = 0x7f02009a

.field public static final btn_radio_off:I = 0x7f02009b

.field public static final btn_radio_off_pressed:I = 0x7f02009c

.field public static final btn_radio_off_selected:I = 0x7f02009d

.field public static final btn_radio_on:I = 0x7f02009e

.field public static final btn_radio_on_pressed:I = 0x7f02009f

.field public static final btn_radio_on_selected:I = 0x7f0200a0

.field public static final button_red:I = 0x7f0200a1

.field public static final button_red_focused:I = 0x7f0200a2

.field public static final button_red_pressed:I = 0x7f0200a3

.field public static final button_red_states:I = 0x7f0200a4

.field public static final button_update:I = 0x7f0200a5

.field public static final button_update_icon:I = 0x7f0200a6

.field public static final camera_upload_done_2x:I = 0x7f0200a7

.field public static final camera_upload_item_bg_color:I = 0x7f0200a8

.field public static final camera_upload_item_bottom_line:I = 0x7f0200a9

.field public static final camera_uploads_icon:I = 0x7f0200aa

.field public static final camera_uploads_icon_focused:I = 0x7f0200ab

.field public static final camera_uploads_icon_pressed:I = 0x7f0200ac

.field public static final ccicons:I = 0x7f0200ad

.field public static final check_selected:I = 0x7f0200ae

.field public static final check_unselected:I = 0x7f0200af

.field public static final chip_background:I = 0x7f0200b0

.field public static final chip_background_invalid:I = 0x7f0200b1

.field public static final chip_background_invalid_pressed:I = 0x7f0200b2

.field public static final chip_background_pressed:I = 0x7f0200b3

.field public static final chip_background_warn:I = 0x7f0200b4

.field public static final chip_background_warn_pressed:I = 0x7f0200b5

.field public static final chip_checkmark:I = 0x7f0200b6

.field public static final chip_delete:I = 0x7f0200b7

.field public static final common_arrow_blue_left:I = 0x7f0200b8

.field public static final common_arrow_blue_right:I = 0x7f0200b9

.field public static final common_arw_blue_left_disabled:I = 0x7f0200ba

.field public static final common_arw_blue_left_focused:I = 0x7f0200bb

.field public static final common_arw_blue_left_normal:I = 0x7f0200bc

.field public static final common_arw_blue_left_pressed:I = 0x7f0200bd

.field public static final common_arw_blue_right_disabled:I = 0x7f0200be

.field public static final common_arw_blue_right_focused:I = 0x7f0200bf

.field public static final common_arw_blue_right_normal:I = 0x7f0200c0

.field public static final common_arw_blue_right_pressed:I = 0x7f0200c1

.field public static final common_bar_dark_gray_bottom:I = 0x7f0200c2

.field public static final common_bar_dark_gray_top:I = 0x7f0200c3

.field public static final common_bar_light_gray_bottom:I = 0x7f0200c4

.field public static final common_bar_light_gray_top:I = 0x7f0200c5

.field public static final common_btn_blue_disabled:I = 0x7f0200c6

.field public static final common_btn_blue_focused:I = 0x7f0200c7

.field public static final common_btn_blue_normal:I = 0x7f0200c8

.field public static final common_btn_blue_pressed:I = 0x7f0200c9

.field public static final common_btn_dark_gray_disabled:I = 0x7f0200ca

.field public static final common_btn_dark_gray_focused:I = 0x7f0200cb

.field public static final common_btn_dark_gray_normal:I = 0x7f0200cc

.field public static final common_btn_dark_gray_pressed:I = 0x7f0200cd

.field public static final common_btn_green_disabled:I = 0x7f0200ce

.field public static final common_btn_green_focused:I = 0x7f0200cf

.field public static final common_btn_green_normal:I = 0x7f0200d0

.field public static final common_btn_green_pressed:I = 0x7f0200d1

.field public static final common_btn_light_gray_disabled:I = 0x7f0200d2

.field public static final common_btn_light_gray_focused:I = 0x7f0200d3

.field public static final common_btn_light_gray_normal:I = 0x7f0200d4

.field public static final common_btn_light_gray_pressed:I = 0x7f0200d5

.field public static final common_btn_red_disabled:I = 0x7f0200d6

.field public static final common_btn_red_focused:I = 0x7f0200d7

.field public static final common_btn_red_normal:I = 0x7f0200d8

.field public static final common_btn_red_pressed:I = 0x7f0200d9

.field public static final common_btn_white_disabled:I = 0x7f0200da

.field public static final common_btn_white_focused:I = 0x7f0200db

.field public static final common_btn_white_normal:I = 0x7f0200dc

.field public static final common_btn_white_pressed:I = 0x7f0200dd

.field public static final common_button_blue:I = 0x7f0200de

.field public static final common_button_dark_gray:I = 0x7f0200df

.field public static final common_button_green:I = 0x7f0200e0

.field public static final common_button_light_gray:I = 0x7f0200e1

.field public static final common_button_red:I = 0x7f0200e2

.field public static final common_button_white:I = 0x7f0200e3

.field public static final contact_email:I = 0x7f0200e4

.field public static final contact_facebook:I = 0x7f0200e5

.field public static final copy_link:I = 0x7f0200e6

.field public static final cu_promo:I = 0x7f0200e7

.field public static final db_browser_list_item:I = 0x7f0200e8

.field public static final db_logo_blue:I = 0x7f0200e9

.field public static final divider_horizontal_dark:I = 0x7f0200ea

.field public static final dropbox:I = 0x7f0200eb

.field public static final dropbox_big:I = 0x7f0200ec

.field public static final emptystate_albums:I = 0x7f0200ed

.field public static final emptystate_cu:I = 0x7f0200ee

.field public static final emptystate_favorites:I = 0x7f0200ef

.field public static final emptystate_files:I = 0x7f0200f0

.field public static final emptystate_notifications:I = 0x7f0200f1

.field public static final emptystate_photos:I = 0x7f0200f2

.field public static final expand_lightweight_item_selector:I = 0x7f0200f3

.field public static final expander_down_chevron:I = 0x7f0200f4

.field public static final expander_up_chevron:I = 0x7f0200f5

.field public static final export:I = 0x7f0200f6

.field public static final export_focused:I = 0x7f0200f7

.field public static final export_pressed:I = 0x7f0200f8

.field public static final fade_gradient:I = 0x7f0200f9

.field public static final fastscroll_thumb:I = 0x7f0200fa

.field public static final fastscroll_thumb_default_holo_blue:I = 0x7f0200fb

.field public static final fastscroll_thumb_default_holo_white:I = 0x7f0200fc

.field public static final fastscroll_thumb_holo:I = 0x7f0200fd

.field public static final fastscroll_thumb_pressed_holo_blue:I = 0x7f0200fe

.field public static final fastscroll_thumb_pressed_holo_white:I = 0x7f0200ff

.field public static final filelist_highlight:I = 0x7f020100

.field public static final folder:I = 0x7f020101

.field public static final folder_42:I = 0x7f020102

.field public static final folder_4x:I = 0x7f020103

.field public static final folder_app:I = 0x7f020104

.field public static final folder_app_4x:I = 0x7f020105

.field public static final folder_camera:I = 0x7f020106

.field public static final folder_camera_4x:I = 0x7f020107

.field public static final folder_photos:I = 0x7f020108

.field public static final folder_photos_4x:I = 0x7f020109

.field public static final folder_public:I = 0x7f02010a

.field public static final folder_public_4x:I = 0x7f02010b

.field public static final folder_share:I = 0x7f02010c

.field public static final folder_star:I = 0x7f02010d

.field public static final folder_star_4x:I = 0x7f02010e

.field public static final folder_up:I = 0x7f02010f

.field public static final folder_user:I = 0x7f020110

.field public static final folder_user_4x:I = 0x7f020111

.field public static final folder_user_big:I = 0x7f020112

.field public static final folderbox:I = 0x7f020113

.field public static final gallery_glow_on_press:I = 0x7f020114

.field public static final gallery_item_checked_dark:I = 0x7f020115

.field public static final gallery_item_checked_light:I = 0x7f020116

.field public static final gallery_item_checked_share:I = 0x7f020117

.field public static final gallery_item_unchecked_dark:I = 0x7f020118

.field public static final gallery_item_unchecked_light:I = 0x7f020119

.field public static final gallery_item_unchecked_share:I = 0x7f02011a

.field public static final gallery_stateful_checkbox_local:I = 0x7f02011b

.field public static final gallery_stateful_checkbox_remote:I = 0x7f02011c

.field public static final gallery_stateful_checkbox_share:I = 0x7f02011d

.field public static final googleplay:I = 0x7f02011e

.field public static final gradient_gray_noisy_topstroke:I = 0x7f02011f

.field public static final holo_pressed_state_background:I = 0x7f020120

.field public static final hugbox_2x:I = 0x7f020121

.field public static final ic_ab_action_light:I = 0x7f020122

.field public static final ic_ab_add_light:I = 0x7f020123

.field public static final ic_ab_add_to_album_dark:I = 0x7f020124

.field public static final ic_ab_add_to_album_dark_disabled:I = 0x7f020125

.field public static final ic_ab_add_to_album_light:I = 0x7f020126

.field public static final ic_ab_add_to_album_light_disabled:I = 0x7f020127

.field public static final ic_ab_add_to_album_light_focused:I = 0x7f020128

.field public static final ic_ab_add_to_album_light_pressed:I = 0x7f020129

.field public static final ic_ab_add_to_album_light_stateful:I = 0x7f02012a

.field public static final ic_ab_back_albums_light:I = 0x7f02012b

.field public static final ic_ab_back_holo_dark:I = 0x7f02012c

.field public static final ic_ab_cancel_light:I = 0x7f02012d

.field public static final ic_ab_cancel_light_disabled:I = 0x7f02012e

.field public static final ic_ab_cancel_light_stateful:I = 0x7f02012f

.field public static final ic_ab_check_light:I = 0x7f020130

.field public static final ic_ab_check_light_disabled:I = 0x7f020131

.field public static final ic_ab_check_light_stateful:I = 0x7f020132

.field public static final ic_ab_delete_dark:I = 0x7f020133

.field public static final ic_ab_delete_dark_disabled:I = 0x7f020134

.field public static final ic_ab_delete_light:I = 0x7f020135

.field public static final ic_ab_delete_light_disabled:I = 0x7f020136

.field public static final ic_ab_delete_light_stateful:I = 0x7f020137

.field public static final ic_ab_more_light:I = 0x7f020138

.field public static final ic_ab_photos_light:I = 0x7f020139

.field public static final ic_ab_remove_dark:I = 0x7f02013a

.field public static final ic_ab_remove_dark_disabled:I = 0x7f02013b

.field public static final ic_ab_remove_light:I = 0x7f02013c

.field public static final ic_ab_remove_light_disabled:I = 0x7f02013d

.field public static final ic_ab_remove_light_stateful:I = 0x7f02013e

.field public static final ic_ab_search_light:I = 0x7f02013f

.field public static final ic_ab_select_light:I = 0x7f020140

.field public static final ic_ab_share_dark:I = 0x7f020141

.field public static final ic_ab_share_dark_disabled:I = 0x7f020142

.field public static final ic_ab_share_light:I = 0x7f020143

.field public static final ic_ab_share_light_disabled:I = 0x7f020144

.field public static final ic_ab_share_light_stateful:I = 0x7f020145

.field public static final ic_ab_unshare_light:I = 0x7f020146

.field public static final ic_accept_dark:I = 0x7f020147

.field public static final ic_add_to_album:I = 0x7f020148

.field public static final ic_cancel:I = 0x7f020149

.field public static final ic_clear:I = 0x7f02014a

.field public static final ic_contact_picture:I = 0x7f02014b

.field public static final ic_delete:I = 0x7f02014c

.field public static final ic_dialog_menu_generic:I = 0x7f02014d

.field public static final ic_export:I = 0x7f02014e

.field public static final ic_favorite:I = 0x7f02014f

.field public static final ic_gallery_pause:I = 0x7f020150

.field public static final ic_gallery_play:I = 0x7f020151

.field public static final ic_menu_compose:I = 0x7f020152

.field public static final ic_menu_new_folder:I = 0x7f020153

.field public static final ic_menu_refresh:I = 0x7f020154

.field public static final ic_menu_sort:I = 0x7f020155

.field public static final ic_more:I = 0x7f020156

.field public static final ic_move:I = 0x7f020157

.field public static final ic_open:I = 0x7f020158

.field public static final ic_overflow:I = 0x7f020159

.field public static final ic_remove:I = 0x7f02015a

.field public static final ic_rename:I = 0x7f02015b

.field public static final ic_share:I = 0x7f02015c

.field public static final icon:I = 0x7f02015d

.field public static final image_transition:I = 0x7f02015e

.field public static final info:I = 0x7f02015f

.field public static final info_button:I = 0x7f020160

.field public static final info_pressed:I = 0x7f020161

.field public static final info_selected:I = 0x7f020162

.field public static final inline_tab_albums:I = 0x7f020163

.field public static final inline_tab_albums_normal:I = 0x7f020164

.field public static final inline_tab_albums_tablet:I = 0x7f020165

.field public static final inline_tab_photos:I = 0x7f020166

.field public static final inline_tab_photos_normal:I = 0x7f020167

.field public static final inline_tab_photos_tablet:I = 0x7f020168

.field public static final inline_tab_switcher_button:I = 0x7f020169

.field public static final internet:I = 0x7f02016a

.field public static final invite_to_folder:I = 0x7f02016b

.field public static final invite_to_folder_button_icon:I = 0x7f02016c

.field public static final invite_to_folder_disabled:I = 0x7f02016d

.field public static final keynote:I = 0x7f02016e

.field public static final keynote_4x:I = 0x7f02016f

.field public static final line:I = 0x7f020170

.field public static final list_divider_holo_light:I = 0x7f020171

.field public static final list_item_font_primary:I = 0x7f020172

.field public static final list_item_font_secondary:I = 0x7f020173

.field public static final list_shared:I = 0x7f020174

.field public static final local_browser_list_item:I = 0x7f020175

.field public static final more:I = 0x7f020176

.field public static final more_focused:I = 0x7f020177

.field public static final more_pressed:I = 0x7f020178

.field public static final move:I = 0x7f020179

.field public static final move_focused:I = 0x7f02017a

.field public static final move_pressed:I = 0x7f02017b

.field public static final navigation_cancel:I = 0x7f02017c

.field public static final navigation_collapse:I = 0x7f02017d

.field public static final navigation_expand:I = 0x7f02017e

.field public static final nessie:I = 0x7f02017f

.field public static final no_connection:I = 0x7f020180

.field public static final no_contacts:I = 0x7f020181

.field public static final noise:I = 0x7f020182

.field public static final nook_spinner_bg:I = 0x7f020183

.field public static final notif_feed_item_bg_read:I = 0x7f020184

.field public static final notif_feed_item_bg_unread:I = 0x7f020185

.field public static final notif_feed_item_bg_urgent:I = 0x7f020186

.field public static final notif_shared_folder_badge_minus:I = 0x7f020187

.field public static final notif_shared_folder_badge_plus:I = 0x7f020188

.field public static final notification:I = 0x7f020189

.field public static final notifications_badge:I = 0x7f02018a

.field public static final notifications_button_blue:I = 0x7f02018b

.field public static final notifications_button_blue_normal:I = 0x7f02018c

.field public static final notifications_button_blue_pressed:I = 0x7f02018d

.field public static final notifications_button_disabled:I = 0x7f02018e

.field public static final notifications_button_gray:I = 0x7f02018f

.field public static final notifications_button_gray_disabled:I = 0x7f020190

.field public static final notifications_button_gray_normal:I = 0x7f020191

.field public static final notifications_button_gray_pressed:I = 0x7f020192

.field public static final notifications_inset_button_blue:I = 0x7f020193

.field public static final notifications_inset_button_gray:I = 0x7f020194

.field public static final numbers:I = 0x7f020195

.field public static final numbers_4x:I = 0x7f020196

.field public static final open:I = 0x7f020197

.field public static final open_focused:I = 0x7f020198

.field public static final open_pressed:I = 0x7f020199

.field public static final package_icon:I = 0x7f02019a

.field public static final package_icon_4x:I = 0x7f02019b

.field public static final page_white:I = 0x7f02019c

.field public static final page_white_4x:I = 0x7f02019d

.field public static final page_white_acrobat:I = 0x7f02019e

.field public static final page_white_acrobat_4x:I = 0x7f02019f

.field public static final page_white_actionscript:I = 0x7f0201a0

.field public static final page_white_actionscript_4x:I = 0x7f0201a1

.field public static final page_white_c:I = 0x7f0201a2

.field public static final page_white_c_4x:I = 0x7f0201a3

.field public static final page_white_code:I = 0x7f0201a4

.field public static final page_white_code_4x:I = 0x7f0201a5

.field public static final page_white_compressed:I = 0x7f0201a6

.field public static final page_white_compressed_4x:I = 0x7f0201a7

.field public static final page_white_cplusplus:I = 0x7f0201a8

.field public static final page_white_cplusplus_4x:I = 0x7f0201a9

.field public static final page_white_csharp:I = 0x7f0201aa

.field public static final page_white_csharp_4x:I = 0x7f0201ab

.field public static final page_white_cup:I = 0x7f0201ac

.field public static final page_white_cup_4x:I = 0x7f0201ad

.field public static final page_white_dvd:I = 0x7f0201ae

.field public static final page_white_dvd_4x:I = 0x7f0201af

.field public static final page_white_excel:I = 0x7f0201b0

.field public static final page_white_excel_4x:I = 0x7f0201b1

.field public static final page_white_film:I = 0x7f0201b2

.field public static final page_white_film_4x:I = 0x7f0201b3

.field public static final page_white_flash:I = 0x7f0201b4

.field public static final page_white_flash_4x:I = 0x7f0201b5

.field public static final page_white_gear:I = 0x7f0201b6

.field public static final page_white_gear_4x:I = 0x7f0201b7

.field public static final page_white_h:I = 0x7f0201b8

.field public static final page_white_h_4x:I = 0x7f0201b9

.field public static final page_white_java:I = 0x7f0201ba

.field public static final page_white_js:I = 0x7f0201bb

.field public static final page_white_js_4x:I = 0x7f0201bc

.field public static final page_white_paint:I = 0x7f0201bd

.field public static final page_white_paint_4x:I = 0x7f0201be

.field public static final page_white_php:I = 0x7f0201bf

.field public static final page_white_php_4x:I = 0x7f0201c0

.field public static final page_white_picture:I = 0x7f0201c1

.field public static final page_white_picture_4x:I = 0x7f0201c2

.field public static final page_white_powerpoint:I = 0x7f0201c3

.field public static final page_white_powerpoint_4x:I = 0x7f0201c4

.field public static final page_white_py:I = 0x7f0201c5

.field public static final page_white_py_4x:I = 0x7f0201c6

.field public static final page_white_ruby:I = 0x7f0201c7

.field public static final page_white_ruby_4x:I = 0x7f0201c8

.field public static final page_white_sound:I = 0x7f0201c9

.field public static final page_white_sound_4x:I = 0x7f0201ca

.field public static final page_white_text:I = 0x7f0201cb

.field public static final page_white_text_4x:I = 0x7f0201cc

.field public static final page_white_tux:I = 0x7f0201cd

.field public static final page_white_tux_4x:I = 0x7f0201ce

.field public static final page_white_vector:I = 0x7f0201cf

.field public static final page_white_vector_4x:I = 0x7f0201d0

.field public static final page_white_visualstudio:I = 0x7f0201d1

.field public static final page_white_visualstudio_4x:I = 0x7f0201d2

.field public static final page_white_word:I = 0x7f0201d3

.field public static final page_white_word_4x:I = 0x7f0201d4

.field public static final pages:I = 0x7f0201d5

.field public static final pages_4x:I = 0x7f0201d6

.field public static final passcode_logo:I = 0x7f0201d7

.field public static final pencil:I = 0x7f0201d8

.field public static final pencil_focused:I = 0x7f0201d9

.field public static final pencil_pressed:I = 0x7f0201da

.field public static final phone:I = 0x7f0201db

.field public static final photo:I = 0x7f0201dc

.field public static final photo_check:I = 0x7f0201dd

.field public static final photo_close:I = 0x7f0201de

.field public static final photo_collections:I = 0x7f0201df

.field public static final photo_delete:I = 0x7f0201e0

.field public static final photo_frame:I = 0x7f0201e1

.field public static final photo_link:I = 0x7f0201e2

.field public static final photo_popup_beak:I = 0x7f0201e3

.field public static final photo_popup_image:I = 0x7f0201e4

.field public static final photo_popup_x:I = 0x7f0201e5

.field public static final photo_share:I = 0x7f0201e6

.field public static final photo_share_ab_btn_blue_focused:I = 0x7f0201e7

.field public static final photo_share_ab_btn_blue_normal:I = 0x7f0201e8

.field public static final photo_share_ab_btn_blue_pressed:I = 0x7f0201e9

.field public static final photo_share_ab_btn_dark_focused:I = 0x7f0201ea

.field public static final photo_share_ab_btn_dark_normal:I = 0x7f0201eb

.field public static final photo_share_ab_btn_dark_pressed:I = 0x7f0201ec

.field public static final photo_share_button_blue:I = 0x7f0201ed

.field public static final photo_share_button_dark:I = 0x7f0201ee

.field public static final progress_bg_holo_light:I = 0x7f0201ef

.field public static final progress_horizontal_holo_light:I = 0x7f0201f0

.field public static final progress_indeterminate_horizontal_holo_light:I = 0x7f0201f1

.field public static final progress_indeterminate_horizontal_holo_light_overlay:I = 0x7f0201f2

.field public static final progress_primary_holo_light:I = 0x7f0201f3

.field public static final progress_secondary_holo_light:I = 0x7f0201f4

.field public static final progressbar_indeterminate_holo1:I = 0x7f0201f5

.field public static final progressbar_indeterminate_holo1_overlay:I = 0x7f0201f6

.field public static final progressbar_indeterminate_holo2:I = 0x7f0201f7

.field public static final progressbar_indeterminate_holo2_overlay:I = 0x7f0201f8

.field public static final progressbar_indeterminate_holo3:I = 0x7f0201f9

.field public static final progressbar_indeterminate_holo3_overlay:I = 0x7f0201fa

.field public static final progressbar_indeterminate_holo4:I = 0x7f0201fb

.field public static final progressbar_indeterminate_holo4_overlay:I = 0x7f0201fc

.field public static final progressbar_indeterminate_holo5:I = 0x7f0201fd

.field public static final progressbar_indeterminate_holo5_overlay:I = 0x7f0201fe

.field public static final progressbar_indeterminate_holo6:I = 0x7f0201ff

.field public static final progressbar_indeterminate_holo6_overlay:I = 0x7f020200

.field public static final progressbar_indeterminate_holo7:I = 0x7f020201

.field public static final progressbar_indeterminate_holo7_overlay:I = 0x7f020202

.field public static final progressbar_indeterminate_holo8:I = 0x7f020203

.field public static final progressbar_indeterminate_holo8_overlay:I = 0x7f020204

.field public static final qa_cancel:I = 0x7f020205

.field public static final qa_cancel_focused:I = 0x7f020206

.field public static final qa_cancel_pressed:I = 0x7f020207

.field public static final qa_clear:I = 0x7f020208

.field public static final qa_clear_focused:I = 0x7f020209

.field public static final qa_clear_pressed:I = 0x7f02020a

.field public static final qr_bubble_outline_gray:I = 0x7f02020b

.field public static final qr_bubble_solid_blue:I = 0x7f02020c

.field public static final qr_bubble_solid_gray:I = 0x7f02020d

.field public static final qr_error_phone_center:I = 0x7f02020e

.field public static final qr_error_tablet_center:I = 0x7f02020f

.field public static final qr_install_center:I = 0x7f020210

.field public static final qr_near_computer_center:I = 0x7f020211

.field public static final qr_start_center:I = 0x7f020212

.field public static final quickaction_arrow:I = 0x7f020213

.field public static final quickaction_arrow_active:I = 0x7f020214

.field public static final quickaction_arrow_disabled:I = 0x7f020215

.field public static final quickaction_arrow_inactive:I = 0x7f020216

.field public static final quickaction_arrow_pressed:I = 0x7f020217

.field public static final quickactionsoverlay:I = 0x7f020218

.field public static final quickactionsoverlay_opaque:I = 0x7f020219

.field public static final quickactionsoverlay_up:I = 0x7f02021a

.field public static final quickactionsoverlay_up_opaque:I = 0x7f02021b

.field public static final quota_full:I = 0x7f02021c

.field public static final remote_install_laptop:I = 0x7f02021d

.field public static final remove:I = 0x7f02021e

.field public static final remove_focused:I = 0x7f02021f

.field public static final remove_pressed:I = 0x7f020220

.field public static final removed_folder:I = 0x7f020221

.field public static final right_chevron:I = 0x7f020222

.field public static final sdcard:I = 0x7f020223

.field public static final send_link:I = 0x7f020224

.field public static final send_to_contact:I = 0x7f020225

.field public static final share:I = 0x7f020226

.field public static final share_focused:I = 0x7f020227

.field public static final share_pressed:I = 0x7f020228

.field public static final shared_folder_bottom_background:I = 0x7f020229

.field public static final shared_folder_icon:I = 0x7f02022a

.field public static final shared_folder_member:I = 0x7f02022b

.field public static final shared_folder_settings:I = 0x7f02022c

.field public static final shared_folder_top_background:I = 0x7f02022d

.field public static final small_star:I = 0x7f02022e

.field public static final small_star_grey:I = 0x7f02022f

.field public static final space:I = 0x7f020230

.field public static final ss_action_item_background_focused_holo_dark:I = 0x7f020231

.field public static final ss_action_item_background_focused_holo_light:I = 0x7f020232

.field public static final ss_action_item_background_pressed_holo_dark:I = 0x7f020233

.field public static final ss_action_item_background_pressed_holo_light:I = 0x7f020234

.field public static final ss_action_item_background_selected_holo_dark:I = 0x7f020235

.field public static final ss_action_item_background_selected_holo_light:I = 0x7f020236

.field public static final ss_bottom_button_bg_dark:I = 0x7f020237

.field public static final ss_bottom_button_bg_light:I = 0x7f020238

.field public static final ss_bottom_button_divider_dark:I = 0x7f020239

.field public static final ss_bottom_button_divider_light:I = 0x7f02023a

.field public static final ss_btn_blue_normal:I = 0x7f02023b

.field public static final ss_btn_blue_pressed:I = 0x7f02023c

.field public static final ss_btn_check_off_focused_holo_dark:I = 0x7f02023d

.field public static final ss_btn_check_off_focused_holo_light:I = 0x7f02023e

.field public static final ss_btn_check_off_holo_dark:I = 0x7f02023f

.field public static final ss_btn_check_off_holo_light:I = 0x7f020240

.field public static final ss_btn_check_off_pressed_holo_dark:I = 0x7f020241

.field public static final ss_btn_check_off_pressed_holo_light:I = 0x7f020242

.field public static final ss_btn_check_on_focused_holo_dark:I = 0x7f020243

.field public static final ss_btn_check_on_focused_holo_light:I = 0x7f020244

.field public static final ss_btn_check_on_holo_dark:I = 0x7f020245

.field public static final ss_btn_check_on_holo_light:I = 0x7f020246

.field public static final ss_btn_check_on_pressed_holo_dark:I = 0x7f020247

.field public static final ss_btn_check_on_pressed_holo_light:I = 0x7f020248

.field public static final ss_button_blue:I = 0x7f020249

.field public static final ss_button_check_dark:I = 0x7f02024a

.field public static final ss_button_check_light:I = 0x7f02024b

.field public static final ss_dark_header_bg:I = 0x7f02024c

.field public static final ss_hugbox_2x:I = 0x7f02024d

.field public static final ss_ic_gallery_pause:I = 0x7f02024e

.field public static final ss_ic_gallery_play:I = 0x7f02024f

.field public static final ss_next_arrow_dark:I = 0x7f020250

.field public static final ss_next_arrow_dark_normal:I = 0x7f020251

.field public static final ss_next_arrow_dark_pressed:I = 0x7f020252

.field public static final ss_next_arrow_light:I = 0x7f020253

.field public static final ss_next_button_dark:I = 0x7f020254

.field public static final ss_next_button_light:I = 0x7f020255

.field public static final ss_oobe_bg_dark:I = 0x7f020256

.field public static final ss_oobe_bg_dark_unscaled:I = 0x7f020257

.field public static final ss_oobe_bg_light:I = 0x7f020258

.field public static final ss_oobe_bg_light_unscaled:I = 0x7f020259

.field public static final ss_oobe_bg_no_waterdrop_light:I = 0x7f02025a

.field public static final ss_oobe_bg_no_waterdrop_light_unscaled:I = 0x7f02025b

.field public static final ss_oobe_tablet_logo:I = 0x7f02025c

.field public static final ss_prev_arrow_dark:I = 0x7f02025d

.field public static final ss_prev_arrow_dark_normal:I = 0x7f02025e

.field public static final ss_prev_arrow_dark_pressed:I = 0x7f02025f

.field public static final ss_prev_arrow_light:I = 0x7f020260

.field public static final ss_scrubber_control_normal_holo:I = 0x7f020261

.field public static final star_empty:I = 0x7f020262

.field public static final star_empty_focused:I = 0x7f020263

.field public static final star_empty_pressed:I = 0x7f020264

.field public static final star_full:I = 0x7f020265

.field public static final star_full_focused:I = 0x7f020266

.field public static final star_full_pressed:I = 0x7f020267

.field public static final stockphoto1:I = 0x7f020268

.field public static final stockphoto2:I = 0x7f020269

.field public static final stockphoto3:I = 0x7f02026a

.field public static final stockphoto4:I = 0x7f02026b

.field public static final stockphoto5:I = 0x7f02026c

.field public static final stockphoto6:I = 0x7f02026d

.field public static final stockphoto7:I = 0x7f02026e

.field public static final submit_feedback:I = 0x7f02026f

.field public static final switch_bg_disabled_dropbox_blue:I = 0x7f020270

.field public static final switch_bg_dropbox_blue:I = 0x7f020271

.field public static final switch_bg_focused_dropbox_blue:I = 0x7f020272

.field public static final switch_inner_dropbox_blue:I = 0x7f020273

.field public static final switch_thumb_activated_dropbox_blue:I = 0x7f020274

.field public static final switch_thumb_disabled_dropbox_blue:I = 0x7f020275

.field public static final switch_thumb_dropbox_blue:I = 0x7f020276

.field public static final switch_thumb_pressed_dropbox_blue:I = 0x7f020277

.field public static final switch_track_dropbox_blue:I = 0x7f020278

.field public static final sym_keyboard_delete:I = 0x7f020279

.field public static final sym_keyboard_num0_no_plus:I = 0x7f02027a

.field public static final sym_keyboard_num1:I = 0x7f02027b

.field public static final sym_keyboard_num2:I = 0x7f02027c

.field public static final sym_keyboard_num3:I = 0x7f02027d

.field public static final sym_keyboard_num4:I = 0x7f02027e

.field public static final sym_keyboard_num5:I = 0x7f02027f

.field public static final sym_keyboard_num6:I = 0x7f020280

.field public static final sym_keyboard_num7:I = 0x7f020281

.field public static final sym_keyboard_num8:I = 0x7f020282

.field public static final sym_keyboard_num9:I = 0x7f020283

.field public static final tab_dropbox:I = 0x7f020284

.field public static final tab_dropbox_inactive:I = 0x7f020285

.field public static final tab_focused_holo:I = 0x7f020286

.field public static final tab_indicator:I = 0x7f020287

.field public static final tab_notifications:I = 0x7f020288

.field public static final tab_photos:I = 0x7f020289

.field public static final tab_pressed_holo_dark:I = 0x7f02028a

.field public static final tab_selected_focused_holo:I = 0x7f02028b

.field public static final tab_selected_holo:I = 0x7f02028c

.field public static final tab_selected_pressed_holo:I = 0x7f02028d

.field public static final tab_star:I = 0x7f02028e

.field public static final text_bubble:I = 0x7f02028f

.field public static final text_chevron_dark:I = 0x7f020290

.field public static final textb:I = 0x7f020291

.field public static final thumb_outline:I = 0x7f020292

.field public static final thumbnail_loading:I = 0x7f020293

.field public static final tour_docs_icon:I = 0x7f020294

.field public static final tour_photos_icon:I = 0x7f020295

.field public static final tour_splash:I = 0x7f020296

.field public static final tour_wrapup_icon:I = 0x7f020297

.field public static final trash:I = 0x7f020298

.field public static final trash_focused:I = 0x7f020299

.field public static final trash_pressed:I = 0x7f02029a

.field public static final update_button:I = 0x7f02029b

.field public static final update_button_disabled:I = 0x7f02029c

.field public static final update_button_pressed:I = 0x7f02029d

.field public static final update_button_selected:I = 0x7f02029e

.field public static final update_icon:I = 0x7f02029f

.field public static final update_icon_disabled:I = 0x7f0202a0

.field public static final video_icon:I = 0x7f0202a1

.field public static final vpi__tab_indicator:I = 0x7f0202a2

.field public static final vpi__tab_selected_focused_holo:I = 0x7f0202a3

.field public static final vpi__tab_selected_holo:I = 0x7f0202a4

.field public static final vpi__tab_selected_pressed_holo:I = 0x7f0202a5

.field public static final vpi__tab_unselected_focused_holo:I = 0x7f0202a6

.field public static final vpi__tab_unselected_holo:I = 0x7f0202a7

.field public static final vpi__tab_unselected_pressed_holo:I = 0x7f0202a8

.field public static final warning_icon:I = 0x7f0202a9

.field public static final warning_icon_white:I = 0x7f0202aa

.field public static final white_left_arrow:I = 0x7f0202ab

.field public static final white_with_border_bg:I = 0x7f0202ac

.field public static final wifi:I = 0x7f0202ad

.field public static final wood_pattern:I = 0x7f0202ae


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
