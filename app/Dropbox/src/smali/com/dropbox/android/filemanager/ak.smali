.class public final Lcom/dropbox/android/filemanager/ak;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/provider/MetadataManager;

.field private final u:Ldbxyzptlk/db231222/j/i;

.field private final v:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/provider/MetadataManager;Ldbxyzptlk/db231222/j/i;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 25
    iput-object p2, p0, Lcom/dropbox/android/filemanager/ak;->l:Lcom/dropbox/android/provider/MetadataManager;

    .line 26
    iput-object p3, p0, Lcom/dropbox/android/filemanager/ak;->u:Ldbxyzptlk/db231222/j/i;

    .line 27
    iput-object p4, p0, Lcom/dropbox/android/filemanager/ak;->v:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/ak;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 32
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ak;->l:Lcom/dropbox/android/provider/MetadataManager;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/ak;->v:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/provider/MetadataManager;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 36
    new-instance v0, Ldbxyzptlk/db231222/j/j;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/ak;->u:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v3, v2}, Ldbxyzptlk/db231222/j/j;-><init>(Ldbxyzptlk/db231222/j/i;Landroid/database/Cursor;)V
    :try_end_0
    .catch Lcom/dropbox/android/provider/J; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/filemanager/ak;->a(Landroid/database/Cursor;)V

    .line 47
    new-instance v2, Lcom/dropbox/android/util/A;

    invoke-direct {v2, v0, v1}, Lcom/dropbox/android/util/A;-><init>(Landroid/database/Cursor;Landroid/os/Bundle;)V

    return-object v2

    .line 37
    :catch_0
    move-exception v0

    .line 39
    const-string v0, "EXTRA_NETWORK_ERROR"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 43
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    invoke-static {v2}, Lcom/dropbox/android/provider/V;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_0
.end method
