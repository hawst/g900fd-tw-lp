.class public final Lcom/dropbox/android/filemanager/aq;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/ag;


# static fields
.field private static a:Lcom/dropbox/android/filemanager/aq;


# instance fields
.field private b:Landroid/database/Cursor;

.field private c:Lcom/dropbox/android/filemanager/at;

.field private d:I

.field private final e:Landroid/os/Handler;

.field private f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/aq;->e:Landroid/os/Handler;

    .line 34
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    .line 47
    return-void
.end method

.method public static declared-synchronized a()Lcom/dropbox/android/filemanager/aq;
    .locals 2

    .prologue
    .line 41
    const-class v1, Lcom/dropbox/android/filemanager/aq;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/filemanager/aq;->a:Lcom/dropbox/android/filemanager/aq;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/dropbox/android/filemanager/aq;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/aq;-><init>()V

    sput-object v0, Lcom/dropbox/android/filemanager/aq;->a:Lcom/dropbox/android/filemanager/aq;

    .line 44
    :cond_0
    sget-object v0, Lcom/dropbox/android/filemanager/aq;->a:Lcom/dropbox/android/filemanager/aq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/aq;Lcom/dropbox/android/filemanager/at;)Lcom/dropbox/android/filemanager/at;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/aq;->b(Lcom/dropbox/android/filemanager/at;)Lcom/dropbox/android/filemanager/at;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/filemanager/at;)V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/dropbox/android/filemanager/aq;->a()Lcom/dropbox/android/filemanager/aq;

    move-result-object v0

    .line 100
    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/aq;->b(Lcom/dropbox/android/filemanager/at;)Lcom/dropbox/android/filemanager/at;

    .line 101
    new-instance v1, Lcom/dropbox/android/filemanager/ar;

    invoke-direct {v1, v0}, Lcom/dropbox/android/filemanager/ar;-><init>(Lcom/dropbox/android/filemanager/aq;)V

    .line 112
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 113
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/aq;)[Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/aq;->d()[Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b(Lcom/dropbox/android/filemanager/at;)Lcom/dropbox/android/filemanager/at;
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->c:Lcom/dropbox/android/filemanager/at;

    .line 119
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aq;->c:Lcom/dropbox/android/filemanager/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-object v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()[Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x7

    .line 76
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 77
    if-lt v1, v5, :cond_2

    .line 78
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 79
    :goto_0
    if-ge v1, v5, :cond_0

    .line 80
    iget-object v2, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 83
    const/4 v1, 0x7

    new-array v1, v1, [Landroid/graphics/Bitmap;

    move v2, v0

    .line 84
    :goto_1
    if-ge v2, v5, :cond_1

    .line 85
    iget-object v4, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    aput-object v0, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 89
    :goto_2
    monitor-exit p0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 4

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    rsub-int/lit8 v1, v0, 0x7

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 141
    const/4 v0, 0x0

    .line 142
    :cond_0
    :goto_0
    if-ge v0, v1, :cond_1

    iget v3, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    if-ge v3, v2, :cond_1

    .line 143
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/aq;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_1
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 153
    monitor-enter p0

    const/4 v0, 0x0

    .line 154
    :try_start_0
    iget v1, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    invoke-virtual {p0, v1}, Lcom/dropbox/android/filemanager/aq;->a(I)Lcom/dropbox/android/filemanager/l;

    move-result-object v3

    .line 156
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    iget v2, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 157
    if-eqz v3, :cond_1

    iget-object v1, v3, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 158
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    iget-object v1, v3, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    iget-object v2, v3, Lcom/dropbox/android/filemanager/l;->b:Ljava/lang/String;

    iget-boolean v3, v3, Lcom/dropbox/android/filemanager/l;->c:Z

    iget v4, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    const/4 v5, 0x1

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;

    move v0, v7

    .line 169
    :cond_0
    :goto_0
    iget v1, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dropbox/android/filemanager/aq;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    monitor-exit p0

    return v0

    .line 162
    :cond_1
    if-eqz v3, :cond_0

    :try_start_1
    iget-object v1, v3, Lcom/dropbox/android/filemanager/l;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    iget-object v1, v3, Lcom/dropbox/android/filemanager/l;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/dropbox/android/filemanager/X;->b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v7

    .line 166
    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->e:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/filemanager/as;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/as;-><init>(Lcom/dropbox/android/filemanager/aq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 200
    return-void
.end method

.method private declared-synchronized h()Z
    .locals 2

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    const-string v1, "_cursor_type_tag"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 228
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 229
    const-string v1, "_tag_photo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "_tag_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    :cond_0
    const/4 v0, 0x1

    .line 233
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)Lcom/dropbox/android/filemanager/l;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 203
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 223
    :goto_0
    monitor-exit p0

    return-object v0

    .line 207
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 209
    iget-object v2, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 210
    iget-object v2, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/aq;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    const-string v2, "thumb_path"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    const-string v3, "content_uri"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 217
    const-string v0, "_tag_video"

    iget-object v3, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    const-string v5, "_cursor_type_tag"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 219
    new-instance v0, Lcom/dropbox/android/filemanager/l;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/l;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 222
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(ILcom/dropbox/android/filemanager/ai;)V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 176
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 177
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lt v0, v2, :cond_1

    .line 179
    iget-object v0, p2, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 181
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    iget-object v1, p2, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 183
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/aq;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    .line 129
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    .line 130
    if-eqz v0, :cond_0

    .line 131
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/aq;->d:I

    .line 134
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/aq;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 54
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/dropbox/android/filemanager/aq;->c:Lcom/dropbox/android/filemanager/at;

    .line 55
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 58
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/aq;->b:Landroid/database/Cursor;

    .line 59
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 61
    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 59
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aq;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()[Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/aq;->d()[Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
