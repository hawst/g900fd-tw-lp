.class public final enum Lcom/dropbox/android/filemanager/H;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/filemanager/H;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/filemanager/H;

.field public static final enum b:Lcom/dropbox/android/filemanager/H;

.field public static final enum c:Lcom/dropbox/android/filemanager/H;

.field private static final synthetic d:[Lcom/dropbox/android/filemanager/H;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/dropbox/android/filemanager/H;

    const-string v1, "SYNCING"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/filemanager/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/H;->a:Lcom/dropbox/android/filemanager/H;

    new-instance v0, Lcom/dropbox/android/filemanager/H;

    const-string v1, "SYNCED"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/filemanager/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/H;->b:Lcom/dropbox/android/filemanager/H;

    new-instance v0, Lcom/dropbox/android/filemanager/H;

    const-string v1, "NETWORK_ERROR_ON_LAST_SYNC"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/filemanager/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/H;->c:Lcom/dropbox/android/filemanager/H;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/filemanager/H;

    sget-object v1, Lcom/dropbox/android/filemanager/H;->a:Lcom/dropbox/android/filemanager/H;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/filemanager/H;->b:Lcom/dropbox/android/filemanager/H;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/filemanager/H;->c:Lcom/dropbox/android/filemanager/H;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/filemanager/H;->d:[Lcom/dropbox/android/filemanager/H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/filemanager/H;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dropbox/android/filemanager/H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/H;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/filemanager/H;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/dropbox/android/filemanager/H;->d:[Lcom/dropbox/android/filemanager/H;

    invoke-virtual {v0}, [Lcom/dropbox/android/filemanager/H;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/filemanager/H;

    return-object v0
.end method
