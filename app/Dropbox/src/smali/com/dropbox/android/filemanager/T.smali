.class public final Lcom/dropbox/android/filemanager/T;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Ldbxyzptlk/db231222/j/i;

.field private final u:Lcom/dropbox/android/taskqueue/U;

.field private final v:Lcom/dropbox/android/provider/MetadataManager;

.field private final w:Lcom/dropbox/android/util/DropboxPath;

.field private final x:Ldbxyzptlk/db231222/n/r;

.field private final y:Lcom/dropbox/android/provider/E;

.field private final z:Lcom/dropbox/android/filemanager/U;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/j/i;Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;Lcom/dropbox/android/filemanager/U;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-static {p6}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 57
    iput-object p2, p0, Lcom/dropbox/android/filemanager/T;->l:Ldbxyzptlk/db231222/j/i;

    .line 58
    iput-object p3, p0, Lcom/dropbox/android/filemanager/T;->u:Lcom/dropbox/android/taskqueue/U;

    .line 59
    iput-object p4, p0, Lcom/dropbox/android/filemanager/T;->v:Lcom/dropbox/android/provider/MetadataManager;

    .line 60
    iput-object p5, p0, Lcom/dropbox/android/filemanager/T;->w:Lcom/dropbox/android/util/DropboxPath;

    .line 61
    iput-object p6, p0, Lcom/dropbox/android/filemanager/T;->x:Ldbxyzptlk/db231222/n/r;

    .line 62
    iput-object p7, p0, Lcom/dropbox/android/filemanager/T;->y:Lcom/dropbox/android/provider/E;

    .line 63
    iput-object p8, p0, Lcom/dropbox/android/filemanager/T;->z:Lcom/dropbox/android/filemanager/U;

    .line 64
    return-void
.end method

.method private a(Landroid/database/Cursor;Z)Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 98
    new-instance v0, Lcom/dropbox/android/provider/W;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/T;->u:Lcom/dropbox/android/taskqueue/U;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/T;->w:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/util/DropboxPath;)Landroid/database/Cursor;

    move-result-object v1

    const-string v2, "_upload_in_progress"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 101
    new-instance v1, Lcom/dropbox/android/provider/S;

    new-instance v2, Lcom/dropbox/android/provider/X;

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/T;->k()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dropbox/android/provider/X;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/provider/S;-><init>(Landroid/database/Cursor;Ljava/util/Comparator;)V

    .line 103
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 104
    new-instance v1, Lcom/dropbox/android/provider/v;

    new-instance v2, Lcom/dropbox/android/provider/n;

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/T;->k()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/filemanager/T;->x:Ldbxyzptlk/db231222/n/r;

    invoke-direct {v2, v3, p2, v4}, Lcom/dropbox/android/provider/n;-><init>(Landroid/content/Context;ZLdbxyzptlk/db231222/n/r;)V

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/dropbox/android/provider/v;-><init>([Landroid/database/Cursor;Ljava/util/Comparator;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/T;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 68
    .line 70
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/T;->v:Lcom/dropbox/android/provider/MetadataManager;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/T;->w:Lcom/dropbox/android/util/DropboxPath;

    iget-object v5, p0, Lcom/dropbox/android/filemanager/T;->x:Ldbxyzptlk/db231222/n/r;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/T;->y:Lcom/dropbox/android/provider/E;

    invoke-virtual {v0, v2, v5, v6}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    :try_end_0
    .catch Lcom/dropbox/android/provider/z; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/android/provider/J; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 73
    :try_start_1
    iget-object v0, v2, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/T;->y:Lcom/dropbox/android/provider/E;

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "EXTRA_NO_FILTER_MATCHES"

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 76
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/j/j;

    iget-object v5, p0, Lcom/dropbox/android/filemanager/T;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v6, v2, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-direct {v0, v5, v6}, Ldbxyzptlk/db231222/j/j;-><init>(Ldbxyzptlk/db231222/j/i;Landroid/database/Cursor;)V
    :try_end_1
    .catch Lcom/dropbox/android/provider/z; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/dropbox/android/provider/J; {:try_start_1 .. :try_end_1} :catch_2

    move-object v1, v2

    .line 83
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/filemanager/T;->z:Lcom/dropbox/android/filemanager/U;

    if-eqz v2, :cond_1

    .line 84
    iget-object v2, p0, Lcom/dropbox/android/filemanager/T;->z:Lcom/dropbox/android/filemanager/U;

    invoke-interface {v2, v4}, Lcom/dropbox/android/filemanager/U;->a(Landroid/os/Bundle;)V

    .line 89
    :cond_1
    if-eqz v1, :cond_2

    iget-boolean v1, v1, Lcom/dropbox/android/provider/H;->b:Z

    .line 91
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/filemanager/T;->a(Landroid/database/Cursor;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 93
    invoke-virtual {p0, v0}, Lcom/dropbox/android/filemanager/T;->a(Landroid/database/Cursor;)V

    .line 94
    new-instance v1, Lcom/dropbox/android/util/A;

    invoke-direct {v1, v0, v4}, Lcom/dropbox/android/util/A;-><init>(Landroid/database/Cursor;Landroid/os/Bundle;)V

    return-object v1

    .line 77
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 78
    :goto_2
    const-string v2, "EXTRA_DIR_DOES_NOT_EXIST"

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 81
    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 80
    :goto_3
    const-string v2, "EXTRA_NETWORK_ERROR"

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_0

    :cond_2
    move v1, v3

    .line 89
    goto :goto_1

    .line 79
    :catch_2
    move-exception v0

    move-object v0, v2

    goto :goto_3

    .line 77
    :catch_3
    move-exception v0

    move-object v0, v2

    goto :goto_2
.end method
