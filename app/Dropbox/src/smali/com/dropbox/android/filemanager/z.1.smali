.class public Lcom/dropbox/android/filemanager/z;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/r/d;

.field private final c:Lcom/dropbox/android/provider/j;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/taskqueue/DownloadTask;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/dropbox/android/filemanager/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/z;->d:Ljava/util/ArrayList;

    .line 37
    iput-object p1, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    .line 38
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/z;->c:Lcom/dropbox/android/provider/j;

    .line 39
    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/ContentValues;ZLcom/dropbox/android/filemanager/am;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 221
    .line 223
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 225
    :goto_0
    new-instance v2, Ljava/io/File;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 228
    if-nez p3, :cond_0

    if-eqz v0, :cond_1

    .line 231
    :cond_0
    invoke-virtual {p4, p1}, Lcom/dropbox/android/filemanager/am;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v1

    .line 248
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 249
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v1

    .line 251
    new-instance v2, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-direct {v2, v3}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v1

    .line 252
    if-nez v1, :cond_2

    .line 254
    sget-object v1, Lcom/dropbox/android/filemanager/z;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File changed remotely, so re-downloading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    new-instance v1, Lcom/dropbox/android/taskqueue/DownloadTask;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v1, v2, p1}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 256
    iget-object v2, p0, Lcom/dropbox/android/filemanager/z;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/A;->a(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 262
    :cond_2
    return-object p2

    :cond_3
    move v0, v1

    .line 223
    goto :goto_0

    .line 235
    :cond_4
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/z;->e:Z

    if-eqz v0, :cond_1

    .line 237
    if-nez p2, :cond_5

    .line 238
    new-instance p2, Landroid/content/ContentValues;

    invoke-direct {p2}, Landroid/content/ContentValues;-><init>()V

    .line 240
    :cond_5
    const-string v0, "local_bytes"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 241
    const-string v0, "local_modified"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 242
    const-string v0, "local_revision"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p4, p1}, Lcom/dropbox/android/filemanager/am;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v1

    goto :goto_1
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/v/j;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/am;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 164
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 166
    const/4 v0, 0x0

    .line 170
    if-eqz p2, :cond_5

    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v4, p2, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    move v4, v2

    .line 171
    :goto_0
    if-eqz p2, :cond_6

    iget-object v1, p2, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p2, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    iget-object v5, p3, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    .line 173
    :goto_1
    if-nez v4, :cond_0

    if-eqz v1, :cond_7

    :cond_0
    move v1, v3

    move v4, v2

    .line 180
    :goto_2
    if-nez v4, :cond_1

    if-eqz v1, :cond_2

    .line 181
    :cond_1
    invoke-static {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->b(Ldbxyzptlk/db231222/v/j;)Landroid/content/ContentValues;

    move-result-object v0

    .line 184
    :cond_2
    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 185
    invoke-direct {p0, p3, v0, v4, p4}, Lcom/dropbox/android/filemanager/z;->a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/ContentValues;ZLcom/dropbox/android/filemanager/am;)Landroid/content/ContentValues;

    move-result-object v0

    .line 205
    :cond_3
    :goto_3
    if-eqz v0, :cond_4

    .line 206
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 207
    const-string v3, "dropbox"

    const-string v4, "canon_path = ?"

    invoke-virtual {p1, v3, v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 209
    if-eq v0, v2, :cond_4

    .line 210
    sget-object v0, Lcom/dropbox/android/filemanager/z;->a:Ljava/lang/String;

    const-string v1, "Error updating entry"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_4
    return-void

    :cond_5
    move v4, v3

    .line 170
    goto :goto_0

    :cond_6
    move v1, v3

    .line 171
    goto :goto_1

    .line 176
    :cond_7
    if-eqz p2, :cond_9

    invoke-virtual {p3, p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ldbxyzptlk/db231222/v/j;)Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    move v4, v3

    .line 177
    goto :goto_2

    .line 187
    :cond_8
    invoke-virtual {p4, p3}, Lcom/dropbox/android/filemanager/am;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 188
    iget-object v1, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v4

    .line 190
    new-instance v5, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v6

    invoke-direct {v5, v6}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v4, v5}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v4

    .line 191
    if-nez v4, :cond_3

    .line 195
    sget-object v4, Lcom/dropbox/android/filemanager/z;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No local copy of the file, and should download: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    new-instance v4, Lcom/dropbox/android/taskqueue/DownloadTask;

    iget-object v5, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v4, v5, p3}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 197
    iget-object v5, p0, Lcom/dropbox/android/filemanager/z;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-static {v1, p3}, Lcom/dropbox/android/filemanager/A;->a(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_3

    :cond_9
    move v1, v3

    move v4, v3

    goto/16 :goto_2
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/v/j;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 143
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v2

    invoke-static {v2}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 144
    invoke-static {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->b(Ldbxyzptlk/db231222/v/j;)Landroid/content/ContentValues;

    move-result-object v2

    .line 146
    const-wide/16 v3, -0x1

    :try_start_0
    const-string v5, "dropbox"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    cmp-long v2, v3, v5

    if-eqz v2, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 146
    goto :goto_0

    .line 147
    :catch_0
    move-exception v3

    .line 148
    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v3, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    .line 149
    const-string v4, "dropbox"

    const-string v5, "canon_path = ?"

    new-array v6, v0, [Ljava/lang/String;

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {p0, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 152
    if-eq v2, v0, :cond_0

    .line 153
    sget-object v0, Lcom/dropbox/android/filemanager/z;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Wrong number of database entries for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 154
    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    move-result-object v1

    .line 132
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    .line 133
    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 136
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/z;->b()V

    .line 125
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/am;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;",
            "Lcom/dropbox/android/filemanager/am;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/z;->e:Z

    .line 54
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 56
    if-eqz p2, :cond_1

    .line 57
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;

    .line 58
    iget-boolean v3, v0, Ldbxyzptlk/db231222/v/j;->m:Z

    if-nez v3, :cond_0

    .line 60
    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v3, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    .line 61
    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 68
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 70
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 71
    sget-object v1, Lcom/dropbox/android/filemanager/am;->a:Lcom/dropbox/android/filemanager/am;

    if-ne p3, v1, :cond_2

    .line 73
    const/4 v1, 0x0

    invoke-direct {p0, v3, v1, v0, p3}, Lcom/dropbox/android/filemanager/z;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/v/j;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/am;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_1
    sget-object v1, Lcom/dropbox/android/filemanager/z;->a:Ljava/lang/String;

    const-string v2, "Exception in insert()"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 121
    :goto_2
    return-void

    .line 75
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    .line 76
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 78
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/v/j;

    .line 81
    invoke-direct {p0, v3, v1, v0, p3}, Lcom/dropbox/android/filemanager/z;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/v/j;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/am;)V

    .line 84
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 119
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 87
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/z;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v1

    invoke-static {v3, v0, v1, v5}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_1

    .line 98
    :cond_4
    const/4 v1, 0x0

    .line 99
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;

    .line 101
    invoke-static {v3, v0}, Lcom/dropbox/android/filemanager/z;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/v/j;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 102
    const/4 v0, 0x1

    :goto_4
    move v1, v0

    .line 104
    goto :goto_3

    .line 106
    :cond_5
    if-eqz v1, :cond_6

    .line 110
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "add/update failed!"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    .line 114
    :cond_6
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 119
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_4
.end method
