.class public final Lcom/dropbox/android/filemanager/X;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Lcom/dropbox/android/filemanager/X;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/filemanager/ah;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/X;->a:Lcom/dropbox/android/filemanager/X;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/X;->c:Ljava/util/LinkedList;

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    .line 50
    new-instance v0, Lcom/dropbox/android/filemanager/aj;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/aj;-><init>(Lcom/dropbox/android/filemanager/X;)V

    .line 51
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/aj;->setPriority(I)V

    .line 52
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aj;->start()V

    .line 53
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;ILandroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 13

    .prologue
    const/4 v2, 0x0

    const-wide/high16 v11, 0x3ff8000000000000L    # 1.5

    const/4 v10, 0x0

    .line 896
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 897
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 898
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 904
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b005f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 906
    int-to-double v6, v1

    int-to-double v8, v5

    mul-double/2addr v8, v11

    cmpl-double v0, v6, v8

    if-gtz v0, :cond_0

    int-to-double v6, v4

    int-to-double v8, v5

    mul-double/2addr v8, v11

    cmpl-double v0, v6, v8

    if-lez v0, :cond_1

    .line 911
    :cond_0
    if-le v1, v4, :cond_2

    .line 912
    sub-int v0, v1, v4

    div-int/lit8 v1, v0, 0x2

    move v3, v4

    .line 919
    :goto_0
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v6, v3

    int-to-float v7, v4

    invoke-direct {v0, v10, v10, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 920
    new-instance v6, Landroid/graphics/RectF;

    int-to-float v7, v5

    int-to-float v5, v5

    invoke-direct {v6, v10, v10, v7, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 921
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 922
    sget-object v7, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v0, v6, v7}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 924
    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 925
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    move-object p0, v0

    .line 929
    :cond_1
    return-object p0

    .line 915
    :cond_2
    sub-int v0, v4, v1

    div-int/lit8 v0, v0, 0x2

    move v4, v1

    move v3, v1

    move v1, v2

    move v2, v0

    .line 916
    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;ZLjava/lang/String;ILandroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 822
    invoke-static {p2}, Lcom/dropbox/android/filemanager/X;->c(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 823
    const/4 v3, 0x0

    .line 824
    if-eqz v0, :cond_0

    .line 825
    invoke-static {v0, p3, p4}, Lcom/dropbox/android/filemanager/X;->a(Landroid/graphics/Bitmap;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 826
    if-eq v3, v0, :cond_0

    move-object v0, p0

    move v1, p1

    move-object v2, p4

    move v4, p5

    move v5, p3

    move-object v6, p2

    .line 827
    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/X;->b(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    .line 830
    :cond_0
    return-object v3
.end method

.method protected static a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 240
    if-nez p1, :cond_3

    .line 244
    :try_start_0
    const-string v0, "android.media.MediaFile"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 245
    const-string v0, "getFileType"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 246
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_2

    .line 248
    const-string v4, "android.media.MediaFile$MediaFileType"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 249
    const-string v5, "fileType"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 250
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 251
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 252
    const-string v4, "isVideoFileType"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 253
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v0

    :goto_0
    move v1, v0

    .line 266
    :goto_1
    if-eqz v1, :cond_0

    .line 267
    :try_start_1
    invoke-static {p0, p3}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 284
    :goto_2
    return-object v0

    .line 270
    :cond_0
    const-class v0, Landroid/media/ThumbnailUtils;

    const-string v1, "createImageThumbnail"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 272
    const/4 v1, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 273
    if-eqz v0, :cond_1

    .line 274
    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 281
    :catch_0
    move-exception v0

    :cond_1
    :goto_3
    move-object v0, v2

    .line 284
    goto :goto_2

    .line 280
    :catch_1
    move-exception v0

    goto :goto_3

    .line 279
    :catch_2
    move-exception v0

    goto :goto_3

    .line 278
    :catch_3
    move-exception v0

    goto :goto_3

    .line 277
    :catch_4
    move-exception v0

    goto :goto_3

    .line 261
    :catch_5
    move-exception v0

    goto :goto_1

    .line 260
    :catch_6
    move-exception v0

    goto :goto_1

    .line 259
    :catch_7
    move-exception v0

    goto :goto_1

    .line 258
    :catch_8
    move-exception v0

    goto :goto_1

    .line 257
    :catch_9
    move-exception v0

    goto :goto_1

    .line 256
    :catch_a
    move-exception v0

    goto :goto_1

    .line 255
    :catch_b
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, p2

    goto :goto_1
.end method

.method protected static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/dropbox/android/filemanager/ad;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    invoke-static {p1}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/ad;

    move-result-object v1

    .line 325
    if-eqz v1, :cond_0

    .line 326
    invoke-static {v1, p0, p1}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v2

    .line 327
    if-lez v2, :cond_0

    .line 328
    new-instance v0, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 331
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/dropbox/android/filemanager/ad;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    .line 314
    invoke-static {}, Lcom/dropbox/android/filemanager/ad;->a()[Lcom/dropbox/android/filemanager/ad;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 315
    invoke-static {v3, p0, p1}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    .line 316
    if-lez v4, :cond_0

    .line 317
    new-instance v0, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 320
    :goto_1
    return-object v0

    .line 314
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 320
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a()Lcom/dropbox/android/filemanager/X;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/dropbox/android/filemanager/X;->a:Lcom/dropbox/android/filemanager/X;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 67
    :cond_0
    sget-object v0, Lcom/dropbox/android/filemanager/X;->a:Lcom/dropbox/android/filemanager/X;

    return-object v0
.end method

.method protected static a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/ad;
    .locals 7

    .prologue
    .line 292
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 293
    invoke-static {}, Lcom/dropbox/android/filemanager/ad;->a()[Lcom/dropbox/android/filemanager/ad;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 294
    invoke-static {v0}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 298
    :goto_1
    return-object v0

    .line 293
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 298
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/X;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->c:Ljava/util/LinkedList;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/dropbox/android/filemanager/X;->a:Lcom/dropbox/android/filemanager/X;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcom/dropbox/android/filemanager/X;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/X;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dropbox/android/filemanager/X;->a:Lcom/dropbox/android/filemanager/X;

    .line 61
    return-void

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method static synthetic a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-static/range {p0 .. p6}, Lcom/dropbox/android/filemanager/X;->b(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/ah;)V
    .locals 2

    .prologue
    .line 335
    iget-object v1, p0, Lcom/dropbox/android/filemanager/X;->c:Ljava/util/LinkedList;

    monitor-enter v1

    .line 336
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 338
    monitor-exit v1

    .line 339
    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 71
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/X;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    return-object v0
.end method

.method protected static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 302
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 304
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 308
    :cond_0
    return-object p0
.end method

.method private static b(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V
    .locals 4

    .prologue
    .line 844
    const/4 v0, 0x0

    .line 846
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v1

    .line 847
    if-eqz v1, :cond_0

    .line 848
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 851
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 855
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->e()Ljava/io/File;

    move-result-object v1

    .line 856
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 857
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 861
    :cond_2
    if-nez v0, :cond_4

    .line 892
    :cond_3
    :goto_0
    return-void

    .line 865
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/.thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 868
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 869
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {p3, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v2

    .line 870
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 871
    if-eqz v2, :cond_3

    .line 872
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 873
    if-eqz p1, :cond_5

    .line 874
    const-string v2, "_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const-string v0, "width"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 876
    const-string v0, "height"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 877
    const-string v0, "kind"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 878
    const-string v0, "video_id"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 887
    :goto_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 889
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 880
    :cond_5
    const-string v2, "_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    const-string v0, "width"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 882
    const-string v0, "height"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 883
    const-string v0, "kind"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 884
    const-string v0, "image_id"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 814
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 815
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 816
    invoke-static {}, Lcom/dropbox/android/util/bi;->a()[B

    move-result-object v1

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 817
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;
    .locals 7

    .prologue
    .line 130
    new-instance v0, Lcom/dropbox/android/filemanager/aa;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/filemanager/aa;-><init>(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/ag;)V

    .line 131
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    .line 132
    return-object v0
.end method

.method protected final a(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;
    .locals 7

    .prologue
    .line 144
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v3

    .line 145
    if-eqz v3, :cond_0

    .line 146
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v2, v1, p3}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 147
    if-eqz v4, :cond_0

    .line 148
    new-instance v2, Lcom/dropbox/android/filemanager/ai;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v6, v1}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;I)J

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    move-object v0, v2

    .line 159
    :goto_0
    return-object v0

    .line 155
    :cond_0
    if-eqz v3, :cond_1

    .line 156
    new-instance v0, Lcom/dropbox/android/filemanager/af;

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/ad;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/af;-><init>(Lcom/dropbox/android/filemanager/ad;IIILcom/dropbox/android/filemanager/ag;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    .line 159
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;
    .locals 1

    .prologue
    .line 100
    invoke-static {p1}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/X;->c(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Landroid/net/Uri;I)V
    .locals 6

    .prologue
    .line 163
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v2

    .line 164
    if-eqz v2, :cond_1

    .line 165
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v3, v1, p2}, Lcom/dropbox/android/filemanager/ad;->b(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    if-eqz v2, :cond_0

    .line 173
    new-instance v0, Lcom/dropbox/android/filemanager/af;

    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/ad;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/af;-><init>(Lcom/dropbox/android/filemanager/ad;IIILcom/dropbox/android/filemanager/ag;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 108
    invoke-static {p1}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;I)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected final b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lcom/dropbox/android/filemanager/Z;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/Z;-><init>(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)V

    .line 234
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    .line 235
    return-object v0
.end method

.method public final b(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;
    .locals 1

    .prologue
    .line 116
    invoke-static {p1}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/dropbox/android/filemanager/X;->b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/X;->d(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;

    move-result-object v0

    goto :goto_0
.end method

.method protected final b(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 209
    invoke-static {p1}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    .line 212
    if-eqz v4, :cond_0

    .line 213
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v6, v1, p2}, Lcom/dropbox/android/filemanager/ad;->b(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 219
    :cond_0
    if-eqz v4, :cond_1

    .line 220
    new-instance v0, Lcom/dropbox/android/filemanager/af;

    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/ad;

    iget-object v2, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/af;-><init>(Lcom/dropbox/android/filemanager/ad;IIILcom/dropbox/android/filemanager/ag;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    goto :goto_0

    .line 222
    :cond_1
    new-instance v0, Lcom/dropbox/android/filemanager/ac;

    invoke-direct {v0, v2, v3, p2, v5}, Lcom/dropbox/android/filemanager/ac;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    goto :goto_0
.end method

.method protected final c(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;
    .locals 7

    .prologue
    .line 186
    invoke-static {p1}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 187
    iget-object v0, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    .line 189
    if-eqz v3, :cond_0

    .line 190
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v4, v1, p3}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 191
    if-eqz v4, :cond_0

    .line 192
    new-instance v2, Lcom/dropbox/android/filemanager/ai;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/X;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v6, v1}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;I)J

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    move-object v0, v2

    .line 205
    :goto_0
    return-object v0

    .line 199
    :cond_0
    if-eqz v3, :cond_1

    .line 200
    new-instance v0, Lcom/dropbox/android/filemanager/af;

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/ad;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/af;-><init>(Lcom/dropbox/android/filemanager/ad;IIILcom/dropbox/android/filemanager/ag;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    .line 205
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 202
    :cond_1
    new-instance v0, Lcom/dropbox/android/filemanager/ac;

    invoke-direct {v0, v2, p2, p3, p4}, Lcom/dropbox/android/filemanager/ac;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    goto :goto_1
.end method

.method protected final d(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;
    .locals 1

    .prologue
    .line 227
    new-instance v0, Lcom/dropbox/android/filemanager/ab;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/ab;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)V

    .line 228
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/X;->a(Lcom/dropbox/android/filemanager/ah;)V

    .line 229
    return-object v0
.end method
