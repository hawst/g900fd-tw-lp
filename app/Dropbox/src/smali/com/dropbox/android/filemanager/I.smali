.class public Lcom/dropbox/android/filemanager/I;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/dropbox/android/provider/j;

.field private final e:Ldbxyzptlk/db231222/k/c;

.field private final f:Ljava/lang/Object;

.field private final g:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/dropbox/android/taskqueue/U;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/dropbox/android/util/DropboxPath;

.field private i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/DownloadTask;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/dropbox/android/taskqueue/D;

.field private k:Ljava/lang/Thread;

.field private final l:Ldbxyzptlk/db231222/j/i;

.field private final m:Lcom/dropbox/android/filemanager/R;

.field private final n:Ldbxyzptlk/db231222/r/d;

.field private final o:Ldbxyzptlk/db231222/k/h;

.field private final p:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/k/h;)V
    .locals 3

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->f:Ljava/lang/Object;

    .line 108
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->g:Ljava/util/concurrent/atomic/AtomicReference;

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->h:Lcom/dropbox/android/util/DropboxPath;

    .line 118
    new-instance v0, Ldbxyzptlk/db231222/j/i;

    invoke-direct {v0}, Ldbxyzptlk/db231222/j/i;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    .line 140
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 126
    iput-object p1, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    .line 127
    iput-object p2, p0, Lcom/dropbox/android/filemanager/I;->n:Ldbxyzptlk/db231222/r/d;

    .line 128
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->c:Landroid/os/Handler;

    .line 129
    iput-object p3, p0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    .line 130
    iput-object p4, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    .line 131
    new-instance v0, Ldbxyzptlk/db231222/k/c;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    new-instance v2, Lcom/dropbox/android/filemanager/J;

    invoke-direct {v2, p0}, Lcom/dropbox/android/filemanager/J;-><init>(Lcom/dropbox/android/filemanager/I;)V

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/k/c;-><init>(Ldbxyzptlk/db231222/k/h;Ldbxyzptlk/db231222/k/e;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->e:Ldbxyzptlk/db231222/k/c;

    .line 137
    new-instance v0, Lcom/dropbox/android/filemanager/R;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->n:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/filemanager/R;-><init>(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/notifications/d;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->m:Lcom/dropbox/android/filemanager/R;

    .line 138
    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 959
    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-static {v1, v2, p1}, Lcom/dropbox/android/util/ab;->b(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 991
    :cond_0
    :goto_0
    return-object v0

    .line 963
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "r"

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 964
    if-eqz v1, :cond_0

    .line 965
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 968
    iget-object v2, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 969
    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/k/h;->f()Ljava/io/File;

    move-result-object v3

    .line 970
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3

    .line 974
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 975
    :try_start_2
    invoke-static {v1, v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 976
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 980
    :try_start_3
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    move-object v0, v1

    goto :goto_0

    .line 977
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 980
    :goto_1
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 984
    :catch_1
    move-exception v1

    goto :goto_0

    .line 980
    :catchall_0
    move-exception v1

    move-object v2, v0

    :goto_2
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3

    .line 986
    :catch_2
    move-exception v1

    goto :goto_0

    .line 990
    :catch_3
    move-exception v1

    goto :goto_0

    .line 980
    :catchall_1
    move-exception v1

    goto :goto_2

    .line 977
    :catch_4
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/util/DropboxPath;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/dropbox/android/filemanager/I;->h:Lcom/dropbox/android/util/DropboxPath;

    return-object p1
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;Z)Ldbxyzptlk/db231222/g/K;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 504
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 505
    invoke-direct {p0, v1, p2}, Lcom/dropbox/android/filemanager/I;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Ljava/util/HashMap;

    move-result-object v3

    .line 511
    if-eqz p3, :cond_0

    .line 512
    :try_start_0
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    move-object v6, v0

    .line 545
    :goto_0
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    .line 546
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v2, v0}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v7

    .line 547
    new-instance v4, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v4, v6}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    .line 548
    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 550
    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/I;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 553
    sget-object v0, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    const-string v1, "moveHelper returning with SUCCESS_WITH_WARNING"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    invoke-virtual {v0, v2, v4}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)V

    .line 555
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->c:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    .line 576
    :goto_1
    return-object v0

    .line 514
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ldbxyzptlk/db231222/z/M;->b(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/j;
    :try_end_1
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    .line 516
    :catch_0
    move-exception v0

    .line 517
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_1

    .line 518
    :catch_1
    move-exception v0

    .line 519
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x199

    if-ne v1, v2, :cond_1

    .line 520
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->p:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_1

    .line 521
    :cond_1
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x1fb

    if-ne v1, v2, :cond_2

    .line 522
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_1

    .line 524
    :cond_2
    iget-object v1, v0, Ldbxyzptlk/db231222/w/i;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v1, v1, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 525
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "userError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Ldbxyzptlk/db231222/w/i;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v0, v0, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :goto_2
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_1

    .line 528
    :cond_3
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 533
    :catch_2
    move-exception v0

    .line 535
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 536
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_1

    .line 558
    :cond_4
    invoke-direct {p0, v1, v4}, Lcom/dropbox/android/filemanager/I;->b(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)V

    .line 562
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 563
    invoke-direct {p0, v1, p1, v6}, Lcom/dropbox/android/filemanager/I;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/filemanager/LocalEntry;Ldbxyzptlk/db231222/v/j;)V

    .line 565
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_5

    .line 566
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;)V

    .line 571
    :goto_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 573
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 575
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    invoke-virtual {v0, v2, v4}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)V

    .line 576
    new-instance v0, Ldbxyzptlk/db231222/g/K;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/K;-><init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V

    goto/16 :goto_1

    .line 568
    :cond_5
    :try_start_3
    invoke-static {v7}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 573
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 589
    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 590
    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->i()Ljava/lang/String;

    move-result-object v1

    .line 591
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "canon_path"

    aput-object v3, v2, v8

    const-string v3, "revision"

    aput-object v3, v2, v9

    .line 592
    const-string v3, "canon_parent_path= ? and canon_path LIKE ? ESCAPE \'\\\'"

    .line 594
    new-array v4, v4, [Ljava/lang/String;

    aput-object v1, v4, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/dropbox/android/provider/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    .line 597
    const-string v1, "dropbox"

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 600
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 602
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 603
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 605
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 607
    return-object v1
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/filemanager/LocalEntry;Ldbxyzptlk/db231222/v/j;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 691
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 693
    invoke-static {p3}, Lcom/dropbox/android/filemanager/LocalEntry;->b(Ldbxyzptlk/db231222/v/j;)Landroid/content/ContentValues;

    move-result-object v1

    .line 695
    const-string v2, "_data"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 697
    const-string v2, "dropbox"

    const-string v3, "canon_path = ?"

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 699
    if-eq v1, v6, :cond_0

    .line 700
    sget-object v2, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "move Error moving entry, not one: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 705
    iget-boolean v1, p2, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_1

    .line 714
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/dropbox/android/provider/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 715
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 717
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v2, p3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v2

    .line 718
    const-string v3, "update dropbox set canon_path= ? || substr(canon_path,?), path= ? || substr(path,?), canon_parent_path= ? || substr(canon_parent_path,?), parent_path= ? || substr(parent_path,?), _data= ?  where canon_parent_path LIKE ? ESCAPE \'\\\'"

    .line 725
    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x2

    iget-object v6, p3, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object v2, v4, v5

    const/4 v2, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x6

    iget-object v5, p3, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    const/16 v0, 0x8

    const/4 v2, 0x0

    aput-object v2, v4, v0

    const/16 v0, 0x9

    aput-object v1, v4, v0

    .line 733
    :try_start_0
    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 738
    :cond_1
    :goto_0
    return-void

    .line 734
    :catch_0
    move-exception v0

    .line 735
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    const-string v2, "moveLocalMetadata"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 748
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    invoke-static {p2}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/util/DropboxPath;)V

    .line 753
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->a()V

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->j()Ljava/io/File;

    move-result-object v0

    .line 246
    invoke-static {v0, p1}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;Ljava/util/Set;)V

    .line 248
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->d()Z

    .line 251
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/D;->a(Ljava/util/Set;)V

    .line 254
    :cond_1
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)Z
    .locals 8

    .prologue
    .line 630
    const-string v1, "dropbox"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "canon_path"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "revision"

    aput-object v3, v2, v0

    const-string v3, "canon_path = ? or canon_path = ? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p4}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 635
    const/4 v0, 0x0

    .line 637
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 638
    add-int/lit8 v0, v0, 0x1

    .line 640
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 641
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 642
    const/4 v0, 0x1

    .line 652
    :goto_0
    return v0

    .line 645
    :cond_1
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 646
    const/4 v0, 0x1

    goto :goto_0

    .line 651
    :cond_2
    if-nez p5, :cond_3

    const/4 v1, 0x1

    .line 652
    :goto_1
    if-eq v0, v1, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    .line 651
    :cond_3
    const/4 v1, 0x2

    goto :goto_1

    .line 652
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 4

    .prologue
    .line 661
    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 662
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 664
    const/4 v1, 0x2

    :try_start_0
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/dropbox/android/provider/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/%"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 665
    const-string v0, "dropbox"

    const-string v2, "canon_path = ? or canon_path LIKE ? ESCAPE \'\\\'"

    invoke-virtual {p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 669
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 675
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {p2, v0}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    .line 677
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    invoke-direct {p0, p2, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;)V

    .line 680
    :cond_0
    return-void

    .line 671
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/I;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->t()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/k/h;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/filemanager/I;)Lcom/dropbox/android/provider/j;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/k/c;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->e:Ldbxyzptlk/db231222/k/c;

    return-object v0
.end method

.method private e(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    .line 1121
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    move-result-object v0

    invoke-static {p1}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->c(Ljava/lang/String;)Z

    .line 1122
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/filemanager/I;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->p()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/j/i;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/filemanager/I;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 172
    new-instance v0, Lcom/dropbox/android/filemanager/L;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/L;-><init>(Lcom/dropbox/android/filemanager/I;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->k:Ljava/lang/Thread;

    .line 183
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->k:Ljava/lang/Thread;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->k:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 185
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 193
    new-instance v0, Lcom/dropbox/android/filemanager/M;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/M;-><init>(Lcom/dropbox/android/filemanager/I;)V

    .line 201
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 202
    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 203
    iget-object v2, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 204
    return-void
.end method

.method private r()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 267
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/MetadataManager;->a()Lcom/dropbox/android/provider/H;

    move-result-object v1

    .line 268
    iget-object v1, v1, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    .line 270
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    invoke-static {v1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    .line 272
    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 285
    const/4 v0, 0x0

    move v1, v0

    .line 289
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->k:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    if-eqz v1, :cond_0

    .line 297
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->e:Ldbxyzptlk/db231222/k/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/c;->a()V

    .line 301
    return-void

    .line 291
    :catch_0
    move-exception v0

    .line 292
    const/4 v0, 0x1

    move v1, v0

    .line 293
    goto :goto_0

    .line 296
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 297
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_1
    throw v0
.end method

.method private t()V
    .locals 7

    .prologue
    .line 355
    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 357
    monitor-exit v1

    .line 372
    :goto_0
    return-void

    .line 360
    :cond_0
    new-instance v0, Lcom/dropbox/android/filemanager/O;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/O;-><init>(Lcom/dropbox/android/filemanager/I;)V

    .line 366
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 367
    new-instance v3, Lcom/dropbox/android/filemanager/Q;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/dropbox/android/filemanager/Q;-><init>(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/J;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->m:Lcom/dropbox/android/filemanager/R;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->g:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v4, Lcom/dropbox/android/taskqueue/U;

    iget-object v5, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v6

    invoke-direct {v4, v5, v6, v0, v2}, Lcom/dropbox/android/taskqueue/U;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/taskqueue/h;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 371
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private u()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->n:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/taskqueue/w;
    .locals 4

    .prologue
    .line 760
    :try_start_0
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 785
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/provider/MetadataManager;->b(Ldbxyzptlk/db231222/v/j;)V

    .line 787
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    :goto_0
    return-object v0

    .line 761
    :catch_0
    move-exception v0

    .line 762
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 763
    :catch_1
    move-exception v0

    .line 764
    iget-object v1, v0, Ldbxyzptlk/db231222/w/i;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v1, v1, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 765
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error creating folder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Ldbxyzptlk/db231222/w/i;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v3, v3, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    :goto_1
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x193

    if-ne v1, v2, :cond_1

    .line 771
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->o:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 768
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 772
    :cond_1
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x199

    if-ne v1, v2, :cond_2

    .line 773
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->p:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 774
    :cond_2
    iget v0, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x1fb

    if-ne v0, v1, :cond_3

    .line 775
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 777
    :cond_3
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 779
    :catch_2
    move-exception v0

    .line 781
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 782
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/g/K;
    .locals 6

    .prologue
    .line 461
    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 462
    new-instance v1, Ldbxyzptlk/db231222/j/e;

    invoke-direct {v1}, Ldbxyzptlk/db231222/j/e;-><init>()V

    .line 463
    new-instance v2, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-direct {v2, v0}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    .line 466
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 470
    :try_start_0
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    invoke-virtual {p2, v0, v3}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 472
    sget-object v3, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Moving "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const/4 v3, 0x1

    invoke-direct {p0, p1, v0, v3}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;Z)Ldbxyzptlk/db231222/g/K;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 476
    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 478
    iget-boolean v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_0

    .line 479
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aY()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 485
    :goto_0
    return-object v0

    .line 481
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aZ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "mime-type"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 476
    :catchall_0
    move-exception v0

    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 478
    iget-boolean v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_1

    .line 479
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aY()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 481
    :goto_1
    throw v0

    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aZ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "mime-type"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_1
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)Ldbxyzptlk/db231222/g/K;
    .locals 6

    .prologue
    .line 425
    new-instance v1, Ldbxyzptlk/db231222/j/f;

    invoke-direct {v1}, Ldbxyzptlk/db231222/j/f;-><init>()V

    .line 426
    new-instance v2, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-direct {v2, v0}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    .line 429
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 432
    :try_start_0
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 433
    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    invoke-virtual {v0, p2, v3}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 434
    sget-object v3, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Renaming "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;Z)Ldbxyzptlk/db231222/g/K;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 439
    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 441
    iget-boolean v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_0

    .line 442
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aW()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 447
    :goto_0
    return-object v0

    .line 444
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aX()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "mime-type"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 439
    :catchall_0
    move-exception v0

    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 441
    iget-boolean v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_1

    .line 442
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aW()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 444
    :goto_1
    throw v0

    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aX()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "mime-type"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_1
.end method

.method public final a(Ljava/io/File;Ljava/io/File;Z)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 810
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 812
    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 813
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to create export destination path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 816
    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_1

    if-nez p3, :cond_1

    move-object p2, v0

    .line 832
    :goto_0
    return-object p2

    .line 820
    :cond_1
    invoke-static {v1}, Lcom/dropbox/android/util/t;->a(Landroid/os/StatFs;)J

    move-result-wide v2

    invoke-static {v1}, Lcom/dropbox/android/util/t;->c(Landroid/os/StatFs;)J

    move-result-wide v4

    mul-long v1, v2, v4

    .line 821
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    const/4 v1, 0x1

    .line 822
    :goto_1
    if-eqz v1, :cond_3

    .line 823
    invoke-static {p1, p2}, Ldbxyzptlk/db231222/X/b;->a(Ljava/io/File;Ljava/io/File;)V

    .line 831
    :goto_2
    new-instance v1, Lcom/dropbox/android/util/aB;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-direct {v1, v2, p2, v0}, Lcom/dropbox/android/util/aB;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 821
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 826
    :cond_3
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    const-string v2, "Not enough free space to copy, trying a move"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_2
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->p()V

    .line 149
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->q()V

    .line 153
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Start UploadQueue Timer"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/dropbox/android/filemanager/K;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/K;-><init>(Lcom/dropbox/android/filemanager/I;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 163
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1115
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/taskqueue/U;->a(J)V

    .line 1117
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->i()V

    .line 1118
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V
    .locals 3

    .prologue
    .line 843
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    move-result-object v0

    .line 844
    if-eqz v0, :cond_0

    .line 845
    new-instance v1, Lcom/dropbox/android/taskqueue/ExportTask;

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/dropbox/android/taskqueue/ExportTask;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 847
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Z)V
    .locals 2

    .prologue
    .line 791
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    .line 792
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 793
    invoke-virtual {p1, p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Z)V

    .line 796
    :cond_0
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 798
    new-instance v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 799
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 800
    invoke-static {p0, p1}, Lcom/dropbox/android/filemanager/A;->a(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 801
    const-string v0, "favorite"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 806
    :goto_0
    return-void

    .line 804
    :cond_1
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/I;->e(Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;Z)V
    .locals 1

    .prologue
    .line 910
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;ZZ)V

    .line 911
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;ZZ)V
    .locals 1

    .prologue
    .line 915
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3, p4}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Collection;ZZ)V

    .line 916
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Collection;ZZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 919
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 920
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 927
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 928
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    .line 929
    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/content/Context;->checkUriPermission(Landroid/net/Uri;III)I

    move-result v1

    .line 930
    if-nez v1, :cond_2

    .line 931
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 932
    if-eqz v1, :cond_2

    move-object v4, v1

    .line 937
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 939
    if-eqz p4, :cond_0

    .line 940
    new-instance v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 945
    :goto_2
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 943
    :cond_0
    new-instance v0, Lcom/dropbox/android/taskqueue/UploadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 947
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/List;)V

    .line 948
    return-void

    :cond_2
    move-object v4, v0

    goto :goto_1
.end method

.method public final a(Ldbxyzptlk/db231222/k/f;)V
    .locals 17

    .prologue
    .line 1000
    const-string v4, "canon_path = ?"

    .line 1001
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/k/f;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 1005
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1007
    const/4 v14, 0x0

    .line 1008
    const-wide/16 v12, 0x0

    .line 1009
    const-string v11, ""

    .line 1010
    const-string v10, ""

    .line 1012
    const/4 v9, 0x0

    .line 1014
    :try_start_0
    const-string v2, "dropbox"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_data"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "local_modified"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string v7, "local_hash"

    aput-object v7, v3, v6

    const/4 v6, 0x3

    const-string v7, "local_revision"

    aput-object v7, v3, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 1022
    if-eqz v6, :cond_6

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1023
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1024
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 1025
    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1026
    const/4 v1, 0x3

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    move-object v15, v1

    move-object/from16 v16, v2

    move-wide v1, v7

    move-object v7, v15

    move-object/from16 v8, v16

    .line 1029
    :goto_0
    if-eqz v6, :cond_0

    .line 1030
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1034
    :cond_0
    if-nez v3, :cond_4

    .line 1038
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/k/f;->c()Ljava/io/File;

    move-result-object v3

    .line 1039
    const-wide/16 v1, 0x1

    .line 1040
    if-nez v3, :cond_3

    .line 1042
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t update, since couldn\'t get a new file for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/k/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    :cond_1
    :goto_1
    return-void

    .line 1029
    :catchall_0
    move-exception v1

    move-object v2, v9

    :goto_2
    if-eqz v2, :cond_2

    .line 1030
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    .line 1046
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1049
    :cond_4
    new-instance v6, Lcom/dropbox/android/filemanager/C;

    invoke-direct {v6, v8, v1, v2, v7}, Lcom/dropbox/android/filemanager/C;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-static {v3, v6}, Lcom/dropbox/android/filemanager/A;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/C;)Lcom/dropbox/android/filemanager/B;

    move-result-object v6

    .line 1051
    iget-boolean v7, v6, Lcom/dropbox/android/filemanager/B;->a:Z

    if-eqz v7, :cond_5

    .line 1052
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File changed, uploading: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    new-instance v1, Lcom/dropbox/android/taskqueue/UploadTask;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-direct/range {p0 .. p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/k/f;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/k/f;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/k/f;->d()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1061
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v2

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->b()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/dropbox/android/taskqueue/U;->b(Ljava/util/List;)V

    .line 1062
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_1

    .line 1063
    :cond_5
    iget-wide v7, v6, Lcom/dropbox/android/filemanager/B;->c:J

    cmp-long v1, v1, v7

    if-eqz v1, :cond_1

    .line 1066
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File unchanged: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    iget-wide v1, v6, Lcom/dropbox/android/filemanager/B;->c:J

    const-wide/16 v7, 0x0

    cmp-long v1, v1, v7

    if-lez v1, :cond_1

    .line 1069
    sget-object v1, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    const-string v2, "File didn\'t really change, updating local modified time"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1071
    const-string v2, "local_modified"

    iget-wide v6, v6, Lcom/dropbox/android/filemanager/B;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1073
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1075
    const-string v3, "dropbox"

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1076
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1077
    sget-object v2, Lcom/dropbox/android/filemanager/I;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " rows modified in uploadNewVersion update operation, unexpected!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1029
    :catchall_1
    move-exception v1

    move-object v2, v6

    goto/16 :goto_2

    :cond_6
    move-object v7, v10

    move-object v8, v11

    move-wide v1, v12

    move-object v3, v14

    goto/16 :goto_0
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .locals 5

    .prologue
    .line 401
    new-instance v1, Ldbxyzptlk/db231222/j/b;

    invoke-direct {v1}, Ldbxyzptlk/db231222/j/b;-><init>()V

    .line 402
    new-instance v2, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-direct {v2, v0}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    .line 403
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 405
    :try_start_0
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/z/M;->b(Lcom/dropbox/android/util/DropboxPath;)V

    .line 406
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/I;->e(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 407
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-static {v0, v3, v4, p1}, Lcom/dropbox/android/filemanager/A;->a(Lcom/dropbox/android/provider/j;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/util/DropboxPath;)V

    .line 410
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    .line 411
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/provider/MetadataManager;->b(Lcom/dropbox/android/util/DropboxPath;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 415
    const/4 v0, 0x1

    return v0

    .line 413
    :catchall_0
    move-exception v0

    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    throw v0
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1088
    const-string v3, "canon_path = ?"

    .line 1089
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1091
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1095
    :try_start_0
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1098
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1099
    invoke-static {v1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1103
    if-eqz v1, :cond_0

    .line 1104
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1107
    :cond_0
    :goto_0
    return-object v0

    .line 1103
    :cond_1
    if-eqz v1, :cond_2

    .line 1104
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v8

    .line 1107
    goto :goto_0

    .line 1103
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v8, :cond_3

    .line 1104
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1103
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_1
.end method

.method public final b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/db231222/j/g;
    .locals 3

    .prologue
    .line 1129
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v0

    new-instance v1, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/j/i;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->l:Ldbxyzptlk/db231222/j/i;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->s()V

    .line 211
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->r()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Ljava/util/Set;)V

    .line 213
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->p()V

    .line 214
    return-void
.end method

.method public final c(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/db231222/j/g;

    move-result-object v0

    .line 1139
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/U;

    .line 223
    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->b()V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->a()V

    .line 231
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->s()V

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->m:Lcom/dropbox/android/filemanager/R;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/R;->a()V

    .line 235
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Ljava/util/Set;)V

    .line 236
    return-void
.end method

.method public final d(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .locals 1

    .prologue
    .line 1144
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/db231222/j/g;

    move-result-object v0

    .line 1145
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/g;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()J
    .locals 5

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->r()Ljava/util/Set;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/k/h;->a(Ljava/util/Set;)J

    move-result-wide v1

    .line 260
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/dropbox/android/taskqueue/D;->b(Ljava/util/Set;)J

    move-result-wide v3

    .line 261
    add-long v0, v1, v3

    return-wide v0
.end method

.method public final f()Lcom/dropbox/android/taskqueue/D;
    .locals 5

    .prologue
    .line 305
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Lcom/dropbox/android/taskqueue/D;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->d:Lcom/dropbox/android/provider/j;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/k/h;->i()Ljava/io/File;

    move-result-object v2

    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->u()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/filemanager/I;->o:Ldbxyzptlk/db231222/k/h;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;-><init>(Lcom/dropbox/android/provider/j;Ljava/io/File;Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->j:Lcom/dropbox/android/taskqueue/D;

    return-object v0
.end method

.method public final g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/DownloadTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 316
    new-instance v1, Lcom/dropbox/android/filemanager/N;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/N;-><init>(Lcom/dropbox/android/filemanager/I;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    new-instance v1, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3, v0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;-><init>(IILjava/util/List;)V

    iput-object v1, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->i:Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    return-object v0
.end method

.method public final h()Lcom/dropbox/android/taskqueue/U;
    .locals 1

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/I;->t()V

    .line 377
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/U;

    return-object v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/U;

    .line 387
    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->d()V

    .line 390
    :cond_0
    return-void
.end method

.method public final j()Ldbxyzptlk/db231222/k/c;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->e:Ldbxyzptlk/db231222/k/c;

    return-object v0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 1125
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    const-class v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/lang/Class;)V

    .line 1126
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1133
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 1184
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 1185
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->h:Lcom/dropbox/android/util/DropboxPath;

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1196
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->m:Lcom/dropbox/android/filemanager/R;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/R;->b()V

    .line 1197
    return-void
.end method
