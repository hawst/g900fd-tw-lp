.class final Lcom/dropbox/android/filemanager/Z;
.super Lcom/dropbox/android/filemanager/ah;
.source "panda.py"


# instance fields
.field private final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)V
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x0

    invoke-direct {p0, p4, p2, p3, v0}, Lcom/dropbox/android/filemanager/ah;-><init>(Lcom/dropbox/android/filemanager/ag;IILcom/dropbox/android/filemanager/Y;)V

    .line 719
    iput-object p1, p0, Lcom/dropbox/android/filemanager/Z;->a:Landroid/net/Uri;

    .line 720
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/ai;
    .locals 5

    .prologue
    .line 724
    iget-object v0, p0, Lcom/dropbox/android/filemanager/Z;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/ad;

    move-result-object v2

    .line 726
    if-eqz v2, :cond_0

    .line 727
    iget-object v0, p0, Lcom/dropbox/android/filemanager/Z;->a:Landroid/net/Uri;

    invoke-static {v2, p1, v0}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    .line 728
    if-lez v3, :cond_0

    .line 729
    iget v0, p0, Lcom/dropbox/android/filemanager/Z;->d:I

    invoke-static {v2, p1, v3, v0}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 730
    if-nez v0, :cond_1

    .line 731
    iget v0, p0, Lcom/dropbox/android/filemanager/Z;->d:I

    invoke-virtual {v2, p1, v3, v0}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 733
    :goto_0
    if-eqz v1, :cond_0

    .line 734
    new-instance v0, Lcom/dropbox/android/filemanager/ai;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v4

    invoke-static {v2, p1, v3}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;I)J

    move-result-wide v2

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    .line 742
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
