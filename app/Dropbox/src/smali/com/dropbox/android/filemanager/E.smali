.class public final Lcom/dropbox/android/filemanager/E;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/provider/MetadataManager;

.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile c:Lcom/dropbox/android/filemanager/H;

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/G;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dropbox/android/provider/MetadataManager;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/E;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 27
    sget-object v0, Lcom/dropbox/android/filemanager/H;->b:Lcom/dropbox/android/filemanager/H;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/E;->c:Lcom/dropbox/android/filemanager/H;

    .line 28
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/E;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 31
    iput-object p1, p0, Lcom/dropbox/android/filemanager/E;->a:Lcom/dropbox/android/provider/MetadataManager;

    .line 32
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/E;)Lcom/dropbox/android/provider/MetadataManager;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->a:Lcom/dropbox/android/provider/MetadataManager;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/E;Lcom/dropbox/android/filemanager/H;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/E;->a(Lcom/dropbox/android/filemanager/H;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/H;)V
    .locals 2

    .prologue
    .line 86
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 87
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->c:Lcom/dropbox/android/filemanager/H;

    if-eq p1, v0, :cond_0

    .line 88
    iput-object p1, p0, Lcom/dropbox/android/filemanager/E;->c:Lcom/dropbox/android/filemanager/H;

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/G;

    .line 90
    invoke-interface {v0, p1}, Lcom/dropbox/android/filemanager/G;->a(Lcom/dropbox/android/filemanager/H;)V

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/E;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/filemanager/H;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->c:Lcom/dropbox/android/filemanager/H;

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/filemanager/G;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/filemanager/F;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/filemanager/F;-><init>(Lcom/dropbox/android/filemanager/E;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 71
    :cond_0
    return-void
.end method

.method public final b(Lcom/dropbox/android/filemanager/G;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/filemanager/E;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method
