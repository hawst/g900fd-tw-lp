.class final Lcom/dropbox/android/filemanager/Q;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/aj;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/I;


# direct methods
.method private constructor <init>(Lcom/dropbox/android/filemanager/I;)V
    .locals 0

    .prologue
    .line 1148
    iput-object p1, p0, Lcom/dropbox/android/filemanager/Q;->a:Lcom/dropbox/android/filemanager/I;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/J;)V
    .locals 0

    .prologue
    .line 1148
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/Q;-><init>(Lcom/dropbox/android/filemanager/I;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/U;)V
    .locals 0

    .prologue
    .line 1176
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/u;)V
    .locals 5

    .prologue
    .line 1156
    new-instance v2, Ldbxyzptlk/db231222/j/n;

    invoke-direct {v2}, Ldbxyzptlk/db231222/j/n;-><init>()V

    .line 1157
    new-instance v0, Lcom/dropbox/android/filemanager/P;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/Q;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/I;->g(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/j/i;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/P;-><init>(Ldbxyzptlk/db231222/j/i;Ldbxyzptlk/db231222/j/m;Lcom/dropbox/android/filemanager/J;)V

    .line 1158
    invoke-virtual {p2, v0}, Lcom/dropbox/android/taskqueue/u;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 1160
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/u;->b()Ljava/util/List;

    move-result-object v0

    .line 1161
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1163
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 1164
    iget-object v4, p0, Lcom/dropbox/android/filemanager/Q;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v4}, Lcom/dropbox/android/filemanager/I;->g(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/j/i;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 1165
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1167
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1168
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 1169
    iget-object v4, p0, Lcom/dropbox/android/filemanager/Q;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v4}, Lcom/dropbox/android/filemanager/I;->g(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/j/i;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    goto :goto_1

    .line 1171
    :cond_0
    throw v1

    .line 1173
    :cond_1
    return-void
.end method

.method public final declared-synchronized b(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 1179
    monitor-enter p0

    monitor-exit p0

    return-void
.end method
