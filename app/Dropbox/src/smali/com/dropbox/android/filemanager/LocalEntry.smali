.class public Lcom/dropbox/android/filemanager/LocalEntry;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Z

.field public final o:J

.field private p:J

.field private final q:Lcom/dropbox/android/util/DropboxPath;

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/dropbox/android/filemanager/V;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/V;-><init>()V

    sput-object v0, Lcom/dropbox/android/filemanager/LocalEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JJZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    .line 121
    iput-wide p1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    .line 122
    iput-object p3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    .line 123
    iput-object p4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    .line 124
    iput-boolean p5, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    .line 125
    if-nez p6, :cond_0

    .line 126
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    .line 130
    :goto_0
    iput-object p7, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    .line 131
    iput-object p8, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    .line 132
    iput-boolean p9, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    .line 133
    iput-object p10, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    .line 134
    iput-object p11, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    .line 135
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    .line 136
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    .line 137
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    .line 138
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    .line 139
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    .line 140
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    .line 141
    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    .line 142
    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    .line 143
    move/from16 v0, p22

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    .line 144
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    .line 145
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    .line 146
    return-void

    .line 128
    :cond_0
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v2, p6, p5}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    goto :goto_0
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    .line 95
    const-class v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    .line 112
    return-void

    :cond_0
    move v0, v2

    .line 94
    goto :goto_0

    :cond_1
    move v0, v2

    .line 98
    goto :goto_1

    :cond_2
    move v0, v2

    .line 104
    goto :goto_2

    :cond_3
    move v0, v2

    .line 105
    goto :goto_3

    :cond_4
    move v1, v2

    .line 109
    goto :goto_4
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dropbox/android/filemanager/V;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/LocalEntry;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static b(Ldbxyzptlk/db231222/v/j;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 364
    if-nez p0, :cond_0

    .line 365
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t create content values from a null entry"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t create content values from an entry w/ a null path"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 371
    const-string v0, "path"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v0, "bytes"

    iget-wide v2, p0, Ldbxyzptlk/db231222/v/j;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 373
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 374
    const-string v0, "revision"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 377
    const-string v0, "hash"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 380
    const-string v0, "icon"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_4
    const-string v0, "is_dir"

    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 383
    iget-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->d:Z

    if-nez v0, :cond_5

    .line 384
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(Ljava/util/Date;)J

    move-result-wide v2

    .line 385
    const-string v0, "modified_millis"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 387
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 388
    const-string v0, "mime_type"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_6
    const-string v0, "thumb_exists"

    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 391
    const-string v0, "parent_path"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v0, "_display_name"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    .line 395
    const-string v2, "canon_path"

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, ""

    .line 398
    :goto_0
    const-string v2, "canon_parent_path"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v0, "_natsort_name"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/provider/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string v0, "is_dirty"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 405
    const-string v0, "is_shareable"

    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 406
    const-string v0, "shared_folder_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v0, "parent_shared_folder_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    return-object v1

    .line 397
    :cond_7
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public final a(J)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 426
    iput-wide p1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    .line 427
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 438
    iput-object p1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    .line 439
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 462
    iput-boolean p1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    .line 463
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/v/j;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 305
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v2, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "paths don\'t match"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_0
    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    if-eqz v1, :cond_2

    .line 359
    :cond_1
    :goto_0
    return v0

    .line 317
    :cond_2
    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_5

    .line 318
    iget-boolean v1, p1, Ldbxyzptlk/db231222/v/j;->d:Z

    if-eqz v1, :cond_1

    .line 324
    iget-object v1, p1, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p1, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 349
    :cond_3
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 350
    iget-object v1, p1, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 356
    :cond_4
    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    iget-boolean v2, p1, Ldbxyzptlk/db231222/v/j;->l:Z

    if-ne v1, v2, :cond_1

    .line 359
    const/4 v0, 0x0

    goto :goto_0

    .line 328
    :cond_5
    iget-boolean v1, p1, Ldbxyzptlk/db231222/v/j;->d:Z

    if-nez v1, :cond_1

    .line 331
    iget-wide v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    iget-wide v3, p1, Ldbxyzptlk/db231222/v/j;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    .line 334
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 335
    iget-object v1, p1, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 341
    :cond_6
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 342
    iget-object v1, p1, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    if-eqz v1, :cond_3

    goto :goto_0

    .line 338
    :cond_7
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 345
    :cond_8
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 353
    :cond_9
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final b(J)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 450
    iput-wide p1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    .line 451
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 475
    iput-object p1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    .line 476
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 442
    iget-wide v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 218
    if-ne p0, p1, :cond_1

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 220
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 221
    goto :goto_0

    .line 222
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 223
    goto :goto_0

    .line 225
    :cond_3
    check-cast p1, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 226
    iget-wide v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    iget-wide v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 227
    goto :goto_0

    .line 228
    :cond_4
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 229
    goto :goto_0

    .line 230
    :cond_5
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 231
    goto :goto_0

    .line 232
    :cond_6
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 233
    goto :goto_0

    .line 234
    :cond_7
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 235
    goto :goto_0

    .line 236
    :cond_8
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 237
    goto :goto_0

    .line 238
    :cond_9
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 239
    goto :goto_0

    .line 240
    :cond_a
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 241
    goto :goto_0

    .line 242
    :cond_b
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 243
    goto :goto_0

    .line 244
    :cond_c
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 245
    goto :goto_0

    .line 246
    :cond_d
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 247
    goto/16 :goto_0

    .line 248
    :cond_e
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 249
    goto/16 :goto_0

    .line 250
    :cond_f
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 251
    goto/16 :goto_0

    .line 252
    :cond_10
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 253
    goto/16 :goto_0

    .line 254
    :cond_11
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    if-nez v2, :cond_12

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    if-eqz v2, :cond_12

    move v0, v1

    .line 255
    goto/16 :goto_0

    .line 256
    :cond_12
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 257
    goto/16 :goto_0

    .line 258
    :cond_13
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-nez v2, :cond_14

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-eqz v2, :cond_14

    move v0, v1

    .line 259
    goto/16 :goto_0

    .line 260
    :cond_14
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 261
    goto/16 :goto_0

    .line 262
    :cond_15
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-nez v2, :cond_16

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-eqz v2, :cond_16

    move v0, v1

    .line 263
    goto/16 :goto_0

    .line 264
    :cond_16
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    .line 265
    goto/16 :goto_0

    .line 266
    :cond_17
    iget-wide v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    iget-wide v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    move v0, v1

    .line 267
    goto/16 :goto_0

    .line 268
    :cond_18
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    if-nez v2, :cond_19

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    if-eqz v2, :cond_19

    move v0, v1

    .line 269
    goto/16 :goto_0

    .line 270
    :cond_19
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 271
    goto/16 :goto_0

    .line 272
    :cond_1a
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    if-nez v2, :cond_1b

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v2, :cond_1b

    move v0, v1

    .line 273
    goto/16 :goto_0

    .line 274
    :cond_1b
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v2, v3}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    move v0, v1

    .line 275
    goto/16 :goto_0

    .line 276
    :cond_1c
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-nez v2, :cond_1d

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-eqz v2, :cond_1d

    move v0, v1

    .line 277
    goto/16 :goto_0

    .line 278
    :cond_1d
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    move v0, v1

    .line 279
    goto/16 :goto_0

    .line 280
    :cond_1e
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 281
    goto/16 :goto_0

    .line 282
    :cond_1f
    iget-wide v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    iget-wide v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_20

    move v0, v1

    .line 283
    goto/16 :goto_0

    .line 284
    :cond_20
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 285
    goto/16 :goto_0

    .line 286
    :cond_21
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    move v0, v1

    .line 287
    goto/16 :goto_0

    .line 288
    :cond_22
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 289
    goto/16 :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 454
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 184
    .line 186
    iget-wide v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    iget-wide v6, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/lit8 v0, v0, 0x1f

    .line 187
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 188
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 189
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 191
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 192
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 193
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v4

    .line 194
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    .line 195
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 196
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v4

    .line 198
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v4

    .line 200
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v4

    .line 202
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    iget-wide v6, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 204
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v4

    .line 206
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v4

    .line 207
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v4

    .line 208
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_e

    move v0, v2

    :goto_e
    add-int/2addr v0, v4

    .line 209
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    iget-wide v6, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 210
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-eqz v4, :cond_f

    :goto_f
    add-int/2addr v0, v2

    .line 211
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    .line 212
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    if-nez v2, :cond_11

    :goto_11
    add-int/2addr v0, v1

    .line 213
    return v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 192
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_5
    move v0, v3

    .line 193
    goto/16 :goto_5

    :cond_6
    move v0, v3

    .line 194
    goto/16 :goto_6

    :cond_7
    move v0, v3

    .line 195
    goto/16 :goto_7

    .line 196
    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 198
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 200
    :cond_a
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 204
    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 206
    :cond_c
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 207
    :cond_d
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_e
    move v0, v3

    .line 208
    goto/16 :goto_e

    :cond_f
    move v2, v3

    .line 210
    goto :goto_f

    .line 211
    :cond_10
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_10

    .line 212
    :cond_11
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    iget-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 163
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 167
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 172
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 173
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 174
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    iget-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->u:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 176
    iget-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 177
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 180
    return-void

    :cond_0
    move v0, v2

    .line 162
    goto :goto_0

    :cond_1
    move v0, v2

    .line 166
    goto :goto_1

    :cond_2
    move v0, v2

    .line 172
    goto :goto_2

    :cond_3
    move v0, v2

    .line 173
    goto :goto_3

    :cond_4
    move v1, v2

    .line 177
    goto :goto_4
.end method
