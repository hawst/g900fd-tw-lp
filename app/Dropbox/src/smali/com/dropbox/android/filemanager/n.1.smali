.class public final Lcom/dropbox/android/filemanager/n;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/P;


# static fields
.field private static final i:Lcom/dropbox/android/taskqueue/I;


# instance fields
.field private a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/filemanager/y;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/dropbox/android/util/q;

.field private final c:Landroid/os/Handler;

.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/dropbox/android/filemanager/v;

.field private f:Z

.field private g:I

.field private final h:Ldbxyzptlk/db231222/v/n;

.field private final j:I

.field private k:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/concurrent/ThreadPoolExecutor;

.field private m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Lcom/dropbox/android/util/n;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field private final o:Lcom/dropbox/android/taskqueue/D;

.field private final p:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/dropbox/android/taskqueue/I;->c:Lcom/dropbox/android/taskqueue/I;

    sput-object v0, Lcom/dropbox/android/filemanager/n;->i:Lcom/dropbox/android/taskqueue/I;

    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/v;Lcom/dropbox/android/filemanager/n;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    .line 62
    iput-boolean v8, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/n;->g:I

    .line 92
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->k:Ljava/util/concurrent/BlockingQueue;

    .line 93
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/n;->k:Ljava/util/concurrent/BlockingQueue;

    new-instance v7, Lcom/dropbox/android/filemanager/x;

    invoke-direct {v7, v9}, Lcom/dropbox/android/filemanager/x;-><init>(Lcom/dropbox/android/filemanager/o;)V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 99
    iput v8, p0, Lcom/dropbox/android/filemanager/n;->n:I

    .line 244
    new-instance v0, Lcom/dropbox/android/filemanager/o;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/o;-><init>(Lcom/dropbox/android/filemanager/n;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->p:Ljava/lang/Runnable;

    .line 113
    iget v0, p3, Lcom/dropbox/android/filemanager/n;->j:I

    iput v0, p0, Lcom/dropbox/android/filemanager/n;->j:I

    .line 114
    iput-object p2, p0, Lcom/dropbox/android/filemanager/n;->e:Lcom/dropbox/android/filemanager/v;

    .line 115
    new-instance v0, Lcom/dropbox/android/util/q;

    iget v1, p0, Lcom/dropbox/android/filemanager/n;->j:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/q;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    .line 116
    iget-object v0, p3, Lcom/dropbox/android/filemanager/n;->h:Ldbxyzptlk/db231222/v/n;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->h:Ldbxyzptlk/db231222/v/n;

    .line 117
    iget-object v0, p3, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    .line 118
    iput-object v9, p3, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    .line 119
    iget-object v0, p3, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    .line 120
    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/v;Ldbxyzptlk/db231222/v/n;ILcom/dropbox/android/taskqueue/D;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    .line 62
    iput-boolean v8, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/n;->g:I

    .line 92
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->k:Ljava/util/concurrent/BlockingQueue;

    .line 93
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/n;->k:Ljava/util/concurrent/BlockingQueue;

    new-instance v7, Lcom/dropbox/android/filemanager/x;

    const/4 v2, 0x0

    invoke-direct {v7, v2}, Lcom/dropbox/android/filemanager/x;-><init>(Lcom/dropbox/android/filemanager/o;)V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 99
    iput v8, p0, Lcom/dropbox/android/filemanager/n;->n:I

    .line 244
    new-instance v0, Lcom/dropbox/android/filemanager/o;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/o;-><init>(Lcom/dropbox/android/filemanager/n;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->p:Ljava/lang/Runnable;

    .line 105
    iput p4, p0, Lcom/dropbox/android/filemanager/n;->j:I

    .line 106
    new-instance v0, Lcom/dropbox/android/util/q;

    iget v1, p0, Lcom/dropbox/android/filemanager/n;->j:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/q;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    .line 107
    iput-object p2, p0, Lcom/dropbox/android/filemanager/n;->e:Lcom/dropbox/android/filemanager/v;

    .line 108
    iput-object p3, p0, Lcom/dropbox/android/filemanager/n;->h:Ldbxyzptlk/db231222/v/n;

    .line 109
    iput-object p5, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    .line 110
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Ljava/util/List;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 351
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 352
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 370
    :goto_0
    return-void

    .line 357
    :cond_0
    new-instance v1, Lcom/dropbox/android/util/n;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/n;-><init>(Landroid/graphics/Bitmap;)V

    .line 359
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 360
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, v3, v1}, Lcom/dropbox/android/util/q;->a(ILcom/dropbox/android/util/n;)V

    .line 362
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/y;

    .line 364
    invoke-interface {v0, v3, v1, p3}, Lcom/dropbox/android/filemanager/y;->a(ILcom/dropbox/android/util/n;Z)V

    goto :goto_2

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_1

    .line 369
    :cond_3
    invoke-virtual {v1}, Lcom/dropbox/android/util/n;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/n;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->e()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/n;Landroid/graphics/Bitmap;Ljava/util/List;Z)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/n;->a(Landroid/graphics/Bitmap;Ljava/util/List;Z)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/filemanager/w;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/n;->a(Lcom/dropbox/android/filemanager/w;I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/filemanager/w;Landroid/util/Pair;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/n;->a(Lcom/dropbox/android/filemanager/w;Landroid/util/Pair;I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/n;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/n;->b(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/w;I)V
    .locals 5

    .prologue
    .line 308
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    sget-object v1, Lcom/dropbox/android/filemanager/n;->i:Lcom/dropbox/android/taskqueue/I;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/w;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/n;->h:Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Landroid/util/Pair;

    move-result-object v0

    .line 311
    iget-object v1, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    new-instance v2, Lcom/dropbox/android/filemanager/q;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/dropbox/android/filemanager/q;-><init>(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/filemanager/w;Landroid/util/Pair;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 317
    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/w;Landroid/util/Pair;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/filemanager/w;",
            "Landroid/util/Pair",
            "<",
            "Lcom/dropbox/android/taskqueue/O;",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 320
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/taskqueue/O;

    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/O;->b:Z

    if-nez v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    iget-object v1, p1, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V

    .line 322
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    if-nez v0, :cond_3

    .line 326
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 327
    new-instance v1, Lcom/dropbox/android/util/n;

    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/n;-><init>(Landroid/graphics/Bitmap;)V

    .line 328
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p3, v1}, Lcom/dropbox/android/util/q;->a(ILcom/dropbox/android/util/n;)V

    .line 329
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/y;

    .line 331
    const/4 v3, 0x0

    invoke-interface {v0, p3, v1, v3}, Lcom/dropbox/android/filemanager/y;->a(ILcom/dropbox/android/util/n;Z)V

    goto :goto_0

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->remove(I)V

    .line 335
    :cond_2
    invoke-virtual {v1}, Lcom/dropbox/android/util/n;->b()V

    .line 336
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    .line 342
    :cond_3
    :goto_1
    return-void

    .line 338
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p3}, Lcom/dropbox/android/util/q;->b(I)V

    .line 339
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    goto :goto_1
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->h:Ldbxyzptlk/db231222/v/n;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {v0, p1, p0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V

    .line 404
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    .line 405
    return-void
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;I)Z
    .locals 3

    .prologue
    .line 172
    const/4 v1, 0x0

    .line 173
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    .line 174
    iget v0, p0, Lcom/dropbox/android/filemanager/n;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/n;->n:I

    .line 175
    if-nez p1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->e:Lcom/dropbox/android/filemanager/v;

    invoke-interface {v0, p2}, Lcom/dropbox/android/filemanager/v;->a(I)Lcom/dropbox/android/filemanager/w;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 178
    iget-object p1, v0, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 181
    :cond_0
    if-eqz p1, :cond_2

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/n;

    .line 183
    if-eqz v0, :cond_2

    .line 185
    iget-object v1, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v1, p2, v0}, Lcom/dropbox/android/util/q;->a(ILcom/dropbox/android/util/n;)V

    .line 186
    const/4 v0, 0x1

    .line 191
    :goto_0
    iget v1, p0, Lcom/dropbox/android/filemanager/n;->n:I

    iget-object v2, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 192
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->c()V

    .line 195
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/n;)Lcom/dropbox/android/taskqueue/D;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    return-object v0
.end method

.method static synthetic b()Lcom/dropbox/android/taskqueue/I;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/dropbox/android/filemanager/n;->i:Lcom/dropbox/android/taskqueue/I;

    return-object v0
.end method

.method private b(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 7

    .prologue
    .line 373
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->h:Ldbxyzptlk/db231222/v/n;

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 375
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {v0, p1, p0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V

    .line 377
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    if-nez v0, :cond_0

    .line 378
    iget-object v6, p0, Lcom/dropbox/android/filemanager/n;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v0, Lcom/dropbox/android/filemanager/r;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/r;-><init>(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 396
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    .line 397
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/filemanager/n;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/n;

    .line 158
    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    goto :goto_0

    .line 160
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    .line 162
    :cond_1
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/n;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 264
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 272
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Lcom/dropbox/android/util/q;->b()I

    move-result v1

    .line 273
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/q;->b(I)V

    .line 276
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->e:Lcom/dropbox/android/filemanager/v;

    invoke-interface {v0, v1}, Lcom/dropbox/android/filemanager/v;->a(I)Lcom/dropbox/android/filemanager/w;

    move-result-object v2

    .line 277
    if-eqz v2, :cond_3

    .line 278
    iget-object v0, v2, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/filemanager/n;->a(Lcom/dropbox/android/util/DropboxPath;I)Z

    move-result v0

    .line 279
    if-eqz v0, :cond_1

    .line 280
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    iget-object v3, v2, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 285
    if-nez v0, :cond_2

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 287
    iget-object v3, p0, Lcom/dropbox/android/filemanager/n;->d:Ljava/util/HashMap;

    iget-object v4, v2, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->o:Lcom/dropbox/android/taskqueue/D;

    iget-object v3, v2, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v3, v4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/ref/WeakReference;)V

    .line 293
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v3, Lcom/dropbox/android/filemanager/p;

    invoke-direct {v3, p0, v2, v1}, Lcom/dropbox/android/filemanager/p;-><init>(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/filemanager/w;I)V

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 301
    :cond_3
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/filemanager/y;)Lcom/dropbox/android/util/n;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 206
    iget-object v1, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v1, p1}, Lcom/dropbox/android/util/q;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/q;->c(I)Lcom/dropbox/android/util/n;

    move-result-object v0

    .line 214
    :goto_0
    return-object v0

    .line 209
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/filemanager/n;->a(Lcom/dropbox/android/util/DropboxPath;I)Z

    move-result v1

    .line 210
    if-eqz v1, :cond_1

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/q;->c(I)Lcom/dropbox/android/util/n;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/filemanager/n;->b(ILcom/dropbox/android/filemanager/y;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/n;->f:Z

    .line 137
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->c()V

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Lcom/dropbox/android/util/q;->c()Landroid/util/SparseArray;

    move-result-object v1

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    .line 140
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 141
    iget-object v2, p0, Lcom/dropbox/android/filemanager/n;->m:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/n;->e:Lcom/dropbox/android/filemanager/v;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/dropbox/android/filemanager/v;->a(I)Lcom/dropbox/android/filemanager/w;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/w;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Lcom/dropbox/android/util/q;->a()V

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 146
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 199
    iput p2, p0, Lcom/dropbox/android/filemanager/n;->g:I

    .line 200
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/util/q;->a(II)V

    .line 201
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    .line 202
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/filemanager/u;

    invoke-direct {v1, p0, p1, p2}, Lcom/dropbox/android/filemanager/u;-><init>(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 425
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->c:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/filemanager/t;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/t;-><init>(Lcom/dropbox/android/filemanager/n;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 415
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 252
    if-eqz p1, :cond_1

    .line 253
    iget v0, p0, Lcom/dropbox/android/filemanager/n;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 254
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    iget v1, p0, Lcom/dropbox/android/filemanager/n;->g:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/q;->a(I)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->b:Lcom/dropbox/android/util/q;

    iget v1, p0, Lcom/dropbox/android/filemanager/n;->j:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/q;->a(I)V

    .line 258
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/n;->d()V

    goto :goto_0
.end method

.method protected final b(ILcom/dropbox/android/filemanager/y;)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 221
    if-eqz v0, :cond_0

    .line 223
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    :goto_0
    return-void

    .line 225
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 226
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v1, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(ILcom/dropbox/android/filemanager/y;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 233
    if-eqz v0, :cond_0

    .line 234
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 235
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 239
    :cond_0
    return-void
.end method

.method protected final finalize()V
    .locals 1

    .prologue
    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/n;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 90
    return-void

    .line 88
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
