.class final Lcom/dropbox/android/filemanager/aE;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/z/aA;

.field private final b:Ljava/io/File;

.field private final c:Ljava/io/File;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/z/aA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object v2, p0, Lcom/dropbox/android/filemanager/aE;->d:Ljava/lang/String;

    .line 135
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    .line 137
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Ldbxyzptlk/db231222/z/aA;->c:Z

    if-eqz v0, :cond_1

    .line 138
    :cond_0
    iput-object v2, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    .line 139
    iput-object v2, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    .line 152
    :goto_0
    return-void

    .line 143
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/k/h;->a()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    .line 144
    iget-object v1, p1, Ldbxyzptlk/db231222/z/aA;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/k/h;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    .line 146
    iget-object v1, p1, Ldbxyzptlk/db231222/z/aA;->g:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 147
    iput-object v2, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    goto :goto_0

    .line 149
    :cond_2
    iget-object v1, p1, Ldbxyzptlk/db231222/z/aA;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/k/h;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    goto :goto_0
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/z/aA;Ljava/io/File;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/aE;->d:Ljava/lang/String;

    .line 129
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    .line 130
    iput-object p2, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    .line 131
    iput-object p3, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    .line 132
    return-void
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/z/aA;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    return-object v0
.end method

.method public final b()Ljava/io/File;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    if-nez v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v0

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    iget-boolean v1, v1, Ldbxyzptlk/db231222/z/aA;->c:Z

    if-eqz v1, :cond_2

    .line 177
    const/4 v0, 0x1

    goto :goto_0

    .line 180
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->d:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    iget-object v1, v1, Ldbxyzptlk/db231222/z/aA;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    const-string v3, "UTF-8"

    invoke-static {v1, v3}, Ldbxyzptlk/db231222/X/b;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/filemanager/aE;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 191
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 199
    :cond_3
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :try_start_2
    iget-object v2, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Ldbxyzptlk/db231222/z/a;->a(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/g;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 206
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 210
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/g;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/aE;->a:Ldbxyzptlk/db231222/z/aA;

    iget-object v1, v1, Ldbxyzptlk/db231222/z/aA;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v1

    move-object v1, v2

    .line 206
    :goto_1
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 203
    :catch_1
    move-exception v1

    move-object v1, v2

    .line 206
    :goto_2
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 203
    :catch_2
    move-exception v2

    goto :goto_2

    .line 201
    :catch_3
    move-exception v2

    goto :goto_1

    .line 187
    :catch_4
    move-exception v1

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->b:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aE;->c:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 223
    :cond_1
    return-void
.end method
