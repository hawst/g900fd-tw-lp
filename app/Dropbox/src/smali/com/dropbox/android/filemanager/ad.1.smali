.class public Lcom/dropbox/android/filemanager/ad;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:[Lcom/dropbox/android/filemanager/ad;


# instance fields
.field private final b:Landroid/net/Uri;

.field private final c:Landroid/net/Uri;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/ad;->a:[Lcom/dropbox/android/filemanager/ad;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Z)V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350
    iput-object p1, p0, Lcom/dropbox/android/filemanager/ad;->b:Landroid/net/Uri;

    .line 351
    iput-object p2, p0, Lcom/dropbox/android/filemanager/ad;->c:Landroid/net/Uri;

    .line 352
    iput-boolean p3, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    .line 353
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 510
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 511
    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-ltz v2, :cond_0

    .line 512
    long-to-int v0, v0

    .line 531
    :goto_0
    return v0

    .line 516
    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    .line 519
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 520
    if-eqz v1, :cond_2

    .line 522
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 523
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 524
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 527
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 531
    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    .line 527
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 489
    const-string v3, "_data = ?"

    .line 490
    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v1

    .line 491
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 493
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/ad;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 494
    if-eqz v1, :cond_1

    .line 496
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 497
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 498
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 501
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 505
    :goto_0
    return v0

    .line 501
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 505
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 501
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;I)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 408
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    if-nez v0, :cond_1

    move-wide v0, v6

    .line 424
    :cond_0
    :goto_0
    return-wide v0

    .line 411
    :cond_1
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "duration"

    aput-object v0, v2, v1

    .line 412
    const-string v3, "_id = ?"

    .line 413
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 414
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/ad;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 417
    if-eqz v2, :cond_2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 418
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 423
    if-eqz v2, :cond_0

    .line 424
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 423
    :cond_2
    if-eqz v2, :cond_3

    .line 424
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    move-wide v0, v6

    goto :goto_0

    .line 423
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    .line 424
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;I)J
    .locals 2

    .prologue
    .line 341
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ad;->b(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/ad;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ad;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public static a()[Lcom/dropbox/android/filemanager/ad;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 356
    sget-object v0, Lcom/dropbox/android/filemanager/ad;->a:[Lcom/dropbox/android/filemanager/ad;

    if-nez v0, :cond_0

    .line 357
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dropbox/android/filemanager/ad;

    new-instance v1, Lcom/dropbox/android/filemanager/ae;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, v2, v3, v5}, Lcom/dropbox/android/filemanager/ae;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/dropbox/android/filemanager/ad;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Landroid/provider/MediaStore$Images$Thumbnails;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, v2, v3, v5}, Lcom/dropbox/android/filemanager/ad;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v1, v0, v6

    const/4 v1, 0x2

    new-instance v2, Lcom/dropbox/android/filemanager/ad;

    const-string v3, "phoneStorage"

    invoke-static {v3}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "phoneStorage"

    invoke-static {v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4, v5}, Lcom/dropbox/android/filemanager/ad;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/dropbox/android/filemanager/ae;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v6}, Lcom/dropbox/android/filemanager/ae;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/dropbox/android/filemanager/ad;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Landroid/provider/MediaStore$Video$Thumbnails;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v6}, Lcom/dropbox/android/filemanager/ad;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/dropbox/android/filemanager/ad;

    const-string v3, "phoneStorage"

    invoke-static {v3}, Landroid/provider/MediaStore$Video$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "phoneStorage"

    invoke-static {v4}, Landroid/provider/MediaStore$Video$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4, v6}, Lcom/dropbox/android/filemanager/ad;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/filemanager/ad;->a:[Lcom/dropbox/android/filemanager/ad;

    .line 370
    :cond_0
    sget-object v0, Lcom/dropbox/android/filemanager/ad;->a:[Lcom/dropbox/android/filemanager/ad;

    return-object v0
.end method

.method private b(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 400
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ad;->c(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    .line 401
    if-eqz v2, :cond_0

    .line 402
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ad;->c:Landroid/net/Uri;

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    move v3, p3

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;ZLjava/lang/String;ILandroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 404
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 535
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 536
    const-string v3, "_id = ?"

    .line 537
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 538
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/ad;->b:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 540
    if-eqz v1, :cond_0

    .line 542
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 544
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 548
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 551
    :cond_0
    :goto_0
    return-object v5

    .line 548
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ad;->c(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/content/Context;II)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v5, 0x3

    const/4 v1, 0x1

    .line 431
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v7

    .line 440
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    if-eqz v0, :cond_3

    .line 441
    if-ne p3, v5, :cond_2

    .line 442
    const-string v3, "video_id = ? AND (kind = ? OR kind = ?)"

    .line 444
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 446
    const-string v5, "kind DESC"

    .line 468
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/ad;->c:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 471
    if-eqz v1, :cond_1

    .line 473
    :try_start_0
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 474
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 475
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 476
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 481
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    .line 485
    :cond_1
    :goto_1
    return-object v6

    .line 448
    :cond_2
    const-string v3, "video_id = ? AND kind = ?"

    .line 450
    new-array v4, v8, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    move-object v5, v6

    .line 451
    goto :goto_0

    .line 454
    :cond_3
    if-ne p3, v5, :cond_4

    .line 455
    const-string v3, "image_id = ? AND (kind = ? OR kind = ?)"

    .line 457
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 459
    const-string v5, "kind DESC"

    goto :goto_0

    .line 461
    :cond_4
    const-string v3, "image_id = ? AND kind = ?"

    .line 463
    new-array v4, v8, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    move-object v5, v6

    .line 464
    goto :goto_0

    .line 481
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 386
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/ad;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 387
    if-eqz v6, :cond_1

    .line 388
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    invoke-static {v6, v0, v1, p3}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 389
    if-eqz v3, :cond_0

    .line 391
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ad;->c:Landroid/net/Uri;

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    .line 396
    :cond_0
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ad;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ad;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/ad;->d:Z

    return v0
.end method
