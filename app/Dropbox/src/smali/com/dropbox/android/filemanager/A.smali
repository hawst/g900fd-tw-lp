.class public Lcom/dropbox/android/filemanager/A;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dropbox/android/filemanager/A;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/A;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/filemanager/C;)Lcom/dropbox/android/filemanager/B;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    .line 147
    invoke-static {p0}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/dropbox/android/filemanager/C;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 150
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v6, p1, Lcom/dropbox/android/filemanager/C;->c:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    iget-wide v6, p1, Lcom/dropbox/android/filemanager/C;->c:J

    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    .line 151
    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 152
    iget-object v0, p1, Lcom/dropbox/android/filemanager/C;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/dropbox/android/filemanager/C;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    new-instance v0, Lcom/dropbox/android/filemanager/B;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/C;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/B;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    .line 158
    :goto_0
    return-object v0

    .line 155
    :cond_0
    new-instance v0, Lcom/dropbox/android/filemanager/B;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/C;->d:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/B;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    goto :goto_0

    .line 158
    :cond_1
    new-instance v0, Lcom/dropbox/android/filemanager/B;

    const/4 v1, 0x0

    iget-object v4, p1, Lcom/dropbox/android/filemanager/C;->d:Ljava/lang/String;

    move-wide v2, v8

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/B;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 105
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 108
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 111
    :cond_0
    const-string v1, "canon_path = ?"

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 113
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    goto :goto_0
.end method

.method public static a(Ldbxyzptlk/db231222/k/h;Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/k/h;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/k/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 63
    const-string v1, "dropbox"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "path"

    aput-object v0, v2, v3

    const-string v3, "local_revision NOT NULL AND local_revision != \'\' AND is_dir = 0"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 75
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 76
    const/4 v2, -0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 77
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v2, p0}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 121
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    const-string v7, "is_dir DESC, _display_name COLLATE NOCASE"

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 124
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 125
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 126
    const/4 v2, -0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 130
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    invoke-static {v0}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    .line 132
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 135
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 136
    return-object v1
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->i()Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string v1, "canon_parent_path = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v0

    .line 92
    if-eqz p2, :cond_0

    .line 94
    invoke-static {p0, p1}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    .line 95
    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    return-object v0
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 188
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 194
    :try_start_0
    invoke-virtual {p3, p2}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 201
    :cond_0
    invoke-virtual {p1, p3}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/util/DropboxPath;)V

    .line 204
    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 205
    const-string v1, "canon_path = ? or canon_path LIKE ? ESCAPE \'\\\'"

    .line 206
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/dropbox/android/provider/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/%"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 207
    const-string v0, "dropbox"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 208
    if-eq v0, v5, :cond_1

    .line 209
    sget-object v1, Lcom/dropbox/android/filemanager/A;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error deleting entry in directory sync, not one: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :cond_1
    :goto_0
    return-void

    .line 211
    :catch_0
    move-exception v0

    .line 213
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V

    .line 214
    sget-object v1, Lcom/dropbox/android/filemanager/A;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception in deleteLocalEntryAndMedia() with path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static a(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 5

    .prologue
    .line 234
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    .line 237
    sget-object v1, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 238
    sget-object v1, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bn;->k()Ldbxyzptlk/db231222/v/n;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 240
    :cond_0
    return-void
.end method

.method public static a(Lcom/dropbox/android/provider/j;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 171
    :try_start_0
    invoke-virtual {p3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v1, p1, p2, v0}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/util/DropboxPath;)V

    .line 172
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 176
    return-void

    .line 174
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Lcom/dropbox/android/provider/j;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/provider/j;",
            "Lcom/dropbox/android/taskqueue/D;",
            "Ldbxyzptlk/db231222/k/h;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 222
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 224
    :try_start_0
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 225
    invoke-static {v1, p1, p2, v0}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/util/DropboxPath;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 227
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 231
    return-void
.end method
