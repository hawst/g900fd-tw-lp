.class final Lcom/dropbox/android/filemanager/aa;
.super Lcom/dropbox/android/filemanager/ah;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/ag;)V
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x0

    invoke-direct {p0, p6, p4, p5, v0}, Lcom/dropbox/android/filemanager/ah;-><init>(Lcom/dropbox/android/filemanager/ag;IILcom/dropbox/android/filemanager/Y;)V

    .line 755
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aa;->a:Ljava/lang/String;

    .line 756
    iput-object p2, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    .line 757
    iput-boolean p3, p0, Lcom/dropbox/android/filemanager/aa;->f:Z

    .line 758
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/ai;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    .line 763
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->c(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 764
    if-eqz v0, :cond_2

    .line 765
    iget v1, p0, Lcom/dropbox/android/filemanager/aa;->d:I

    invoke-static {v0, v1, p1}, Lcom/dropbox/android/filemanager/X;->a(Landroid/graphics/Bitmap;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 766
    if-eq v3, v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 768
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 769
    invoke-static {v2}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/ad;

    move-result-object v4

    .line 770
    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/ad;->c()Landroid/net/Uri;

    move-result-object v0

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/aa;->f:Z

    invoke-static {v4, p1, v2}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/filemanager/aa;->d:I

    iget-object v6, p0, Lcom/dropbox/android/filemanager/aa;->a:Ljava/lang/String;

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    .line 779
    :cond_0
    :goto_0
    new-instance v0, Lcom/dropbox/android/filemanager/ai;

    invoke-direct {v0, v3, v7, v8, v9}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    .line 808
    :goto_1
    return-object v0

    .line 772
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 773
    invoke-static {p1, v0}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 774
    if-eqz v2, :cond_0

    .line 775
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/ad;->c()Landroid/net/Uri;

    move-result-object v0

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/aa;->f:Z

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/filemanager/aa;->d:I

    iget-object v6, p0, Lcom/dropbox/android/filemanager/aa;->a:Ljava/lang/String;

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    goto :goto_0

    .line 782
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 783
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 784
    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/ad;

    move-result-object v1

    .line 785
    if-eqz v1, :cond_5

    .line 786
    invoke-static {v1, p1, v0}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    .line 787
    if-lez v0, :cond_5

    .line 788
    iget v2, p0, Lcom/dropbox/android/filemanager/aa;->d:I

    invoke-virtual {v1, p1, v0, v2}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 789
    if-eqz v1, :cond_5

    .line 790
    new-instance v0, Lcom/dropbox/android/filemanager/ai;

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    goto :goto_1

    .line 795
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aa;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 796
    invoke-static {p1, v0}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 798
    if-eqz v1, :cond_4

    .line 799
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/dropbox/android/filemanager/aa;->d:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 803
    :goto_2
    if-eqz v1, :cond_5

    .line 804
    new-instance v0, Lcom/dropbox/android/filemanager/ai;

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    goto :goto_1

    .line 801
    :cond_4
    iget v1, p0, Lcom/dropbox/android/filemanager/aa;->d:I

    invoke-static {v0, v7, v7, v1}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 808
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method
