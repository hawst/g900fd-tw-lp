.class final Lcom/dropbox/android/filemanager/P;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/v;


# instance fields
.field private final a:Ldbxyzptlk/db231222/j/i;

.field private final b:Ldbxyzptlk/db231222/j/m;


# direct methods
.method private constructor <init>(Ldbxyzptlk/db231222/j/i;Ldbxyzptlk/db231222/j/m;)V
    .locals 0

    .prologue
    .line 858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 859
    iput-object p1, p0, Lcom/dropbox/android/filemanager/P;->a:Ldbxyzptlk/db231222/j/i;

    .line 860
    iput-object p2, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    .line 861
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/j/i;Ldbxyzptlk/db231222/j/m;Lcom/dropbox/android/filemanager/J;)V
    .locals 0

    .prologue
    .line 853
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/P;-><init>(Ldbxyzptlk/db231222/j/i;Ldbxyzptlk/db231222/j/m;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/u;JJ)V
    .locals 4

    .prologue
    .line 870
    iget-object v0, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/m;->a(Lcom/dropbox/android/taskqueue/w;)V

    .line 871
    iget-object v0, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    const/high16 v1, 0x42c80000    # 100.0f

    long-to-float v2, p2

    long-to-float v3, p4

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/m;->a(F)V

    .line 872
    iget-object v0, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v0, p4, p5}, Ldbxyzptlk/db231222/j/m;->a(J)V

    .line 873
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 865
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 4

    .prologue
    .line 886
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 887
    iget-object v0, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/m;->a(F)V

    .line 888
    iget-object v0, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/j/m;->a(Lcom/dropbox/android/taskqueue/w;)V

    .line 896
    :cond_0
    return-void

    .line 890
    :cond_1
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->b()Ljava/util/List;

    move-result-object v0

    .line 891
    invoke-virtual {p1, p0}, Lcom/dropbox/android/taskqueue/u;->b(Lcom/dropbox/android/taskqueue/v;)V

    .line 892
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 893
    iget-object v2, p0, Lcom/dropbox/android/filemanager/P;->a:Ldbxyzptlk/db231222/j/i;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v2, v0, v3}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    goto :goto_0
.end method

.method public final c(Lcom/dropbox/android/taskqueue/u;)V
    .locals 4

    .prologue
    .line 877
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->b()Ljava/util/List;

    move-result-object v0

    .line 878
    invoke-virtual {p1, p0}, Lcom/dropbox/android/taskqueue/u;->b(Lcom/dropbox/android/taskqueue/v;)V

    .line 879
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 880
    iget-object v2, p0, Lcom/dropbox/android/filemanager/P;->a:Ldbxyzptlk/db231222/j/i;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v2, v0, v3}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    goto :goto_0

    .line 882
    :cond_0
    return-void
.end method

.method public final d(Lcom/dropbox/android/taskqueue/u;)V
    .locals 4

    .prologue
    .line 900
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->b()Ljava/util/List;

    move-result-object v0

    .line 901
    invoke-virtual {p1, p0}, Lcom/dropbox/android/taskqueue/u;->b(Lcom/dropbox/android/taskqueue/v;)V

    .line 902
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 903
    iget-object v2, p0, Lcom/dropbox/android/filemanager/P;->a:Ldbxyzptlk/db231222/j/i;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/P;->b:Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v2, v0, v3}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    goto :goto_0

    .line 905
    :cond_0
    return-void
.end method
