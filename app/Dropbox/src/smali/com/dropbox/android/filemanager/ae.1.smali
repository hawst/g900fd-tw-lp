.class final Lcom/dropbox/android/filemanager/ae;
.super Lcom/dropbox/android/filemanager/ad;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Z)V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ad;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    .line 560
    return-void
.end method

.method private b(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 572
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 573
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 574
    invoke-static {}, Lcom/dropbox/android/util/bi;->a()[B

    move-result-object v1

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 575
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/ae;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 576
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    int-to-long v2, p2

    invoke-static {v1, v2, v3, p3, v0}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 579
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    int-to-long v2, p2

    invoke-static {v1, v2, v3, p3, v0}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 564
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ae;->b(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 565
    if-nez v0, :cond_0

    .line 566
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 568
    :cond_0
    return-object v0
.end method
