.class final Lcom/dropbox/android/filemanager/R;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/aj;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/I;

.field private final b:Lcom/dropbox/android/notifications/d;

.field private c:I

.field private d:I

.field private e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/notifications/d;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1213
    iput-object p1, p0, Lcom/dropbox/android/filemanager/R;->a:Lcom/dropbox/android/filemanager/I;

    .line 1214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1206
    iput v0, p0, Lcom/dropbox/android/filemanager/R;->c:I

    .line 1209
    iput v0, p0, Lcom/dropbox/android/filemanager/R;->d:I

    .line 1211
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/R;->e:Z

    .line 1292
    new-instance v0, Lcom/dropbox/android/filemanager/S;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/S;-><init>(Lcom/dropbox/android/filemanager/R;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/R;->f:Ljava/lang/Runnable;

    .line 1215
    iput-object p2, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    .line 1216
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 1217
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/notifications/d;
    .locals 1

    .prologue
    .line 1199
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    return-object v0
.end method

.method private declared-synchronized b(Lcom/dropbox/android/taskqueue/U;)V
    .locals 2

    .prologue
    .line 1250
    monitor-enter p0

    :try_start_0
    const-class v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    invoke-virtual {p1, v0}, Lcom/dropbox/android/taskqueue/U;->b(Ljava/lang/Class;)I

    move-result v0

    .line 1251
    iget v1, p0, Lcom/dropbox/android/filemanager/R;->c:I

    if-eq v0, v1, :cond_0

    .line 1253
    iput v0, p0, Lcom/dropbox/android/filemanager/R;->c:I

    .line 1254
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/R;->e:Z

    if-eqz v0, :cond_1

    .line 1255
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/R;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1261
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1256
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/dropbox/android/filemanager/R;->c:I

    if-nez v0, :cond_0

    .line 1258
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/filemanager/R;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1264
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/dropbox/android/filemanager/R;->c:I

    if-gtz v2, :cond_1

    .line 1265
    :goto_0
    if-eqz v0, :cond_2

    .line 1267
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    sget-object v1, Lcom/dropbox/android/util/aM;->f:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 1270
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    sget-object v1, Lcom/dropbox/android/util/aM;->b:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 1271
    iget v0, p0, Lcom/dropbox/android/filemanager/R;->d:I

    if-lez v0, :cond_0

    .line 1274
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 1275
    const-string v1, "ARG_NUM_UPLOADS"

    iget v2, p0, Lcom/dropbox/android/filemanager/R;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1276
    const/4 v1, 0x0

    iput v1, p0, Lcom/dropbox/android/filemanager/R;->d:I

    .line 1277
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/R;->e()V

    .line 1278
    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    sget-object v2, Lcom/dropbox/android/util/aM;->g:Lcom/dropbox/android/util/aM;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 1279
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/R;->d()V

    .line 1282
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/R;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1290
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 1264
    goto :goto_0

    .line 1285
    :cond_2
    :try_start_1
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 1286
    const-string v1, "ARG_NUM_UPLOADS"

    iget v2, p0, Lcom/dropbox/android/filemanager/R;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1287
    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    sget-object v2, Lcom/dropbox/android/util/aM;->g:Lcom/dropbox/android/util/aM;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 1288
    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    sget-object v2, Lcom/dropbox/android/util/aM;->f:Lcom/dropbox/android/util/aM;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 4

    .prologue
    .line 1300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->h(Lcom/dropbox/android/filemanager/I;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1301
    monitor-exit p0

    return-void

    .line 1300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 1304
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->h(Lcom/dropbox/android/filemanager/I;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1305
    monitor-exit p0

    return-void

    .line 1304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1220
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/dropbox/android/filemanager/R;->c:I

    .line 1221
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/filemanager/R;->d:I

    .line 1222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/R;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1223
    monitor-exit p0

    return-void

    .line 1220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/taskqueue/U;)V
    .locals 2

    .prologue
    .line 1230
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/R;->b(Lcom/dropbox/android/taskqueue/U;)V

    .line 1231
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/dropbox/android/util/s;->a(Lcom/dropbox/android/notifications/d;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/taskqueue/U;)V

    .line 1233
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 1226
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1310
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/dropbox/android/filemanager/R;->c:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/R;->e:Z

    if-nez v0, :cond_0

    .line 1311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/R;->e:Z

    .line 1312
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/R;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1314
    :cond_0
    monitor-exit p0

    return-void

    .line 1310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/u;)V
    .locals 2

    .prologue
    .line 1237
    monitor-enter p0

    :try_start_0
    instance-of v0, p2, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    if-eqz v0, :cond_1

    .line 1238
    iget v0, p0, Lcom/dropbox/android/filemanager/R;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/R;->d:I

    .line 1239
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->a:Lcom/dropbox/android/filemanager/I;

    check-cast p2, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->t()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/util/DropboxPath;

    .line 1240
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/R;->b(Lcom/dropbox/android/taskqueue/U;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1247
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1241
    :cond_1
    :try_start_1
    instance-of v0, p2, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    if-eqz v0, :cond_0

    .line 1242
    iget-object v0, p0, Lcom/dropbox/android/filemanager/R;->a:Lcom/dropbox/android/filemanager/I;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/I;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 1243
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->u()V

    .line 1244
    iget-object v1, p0, Lcom/dropbox/android/filemanager/R;->b:Lcom/dropbox/android/notifications/d;

    invoke-static {v1, v0, p1}, Lcom/dropbox/android/util/s;->a(Lcom/dropbox/android/notifications/d;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/taskqueue/U;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
