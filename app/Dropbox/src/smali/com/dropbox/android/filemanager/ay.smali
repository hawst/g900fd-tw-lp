.class final Lcom/dropbox/android/filemanager/ay;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/g/ac;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/au;


# direct methods
.method constructor <init>(Lcom/dropbox/android/filemanager/au;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 520
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No update available"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    invoke-static {}, Ldbxyzptlk/db231222/k/h;->a()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->g()V

    .line 522
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    new-instance v1, Lcom/dropbox/android/filemanager/aE;

    invoke-direct {v1, v2}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;)V

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aE;)Lcom/dropbox/android/filemanager/aE;

    .line 523
    new-instance v0, Lcom/dropbox/android/notifications/d;

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/notifications/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    sget-object v1, Lcom/dropbox/android/util/aM;->h:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 524
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->e(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->e(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->dismissAllowingStateLoss()V

    .line 527
    :cond_0
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/z/aA;)V
    .locals 2

    .prologue
    .line 460
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/filemanager/az;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/filemanager/az;-><init>(Lcom/dropbox/android/filemanager/ay;Ldbxyzptlk/db231222/z/aA;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 516
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 531
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Check failure"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    return-void
.end method
