.class final Lcom/dropbox/android/filemanager/aD;
.super Landroid/database/ContentObserver;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/au;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/au;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 103
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aD;->a:Lcom/dropbox/android/filemanager/au;

    .line 104
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/filemanager/aD;->b:J

    .line 106
    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 4

    .prologue
    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dropbox/android/filemanager/aD;->b:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    .line 111
    :goto_0
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    .line 112
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->a(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aD;->a:Lcom/dropbox/android/filemanager/au;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Landroid/content/Context;)V

    .line 115
    :cond_0
    return-void

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
