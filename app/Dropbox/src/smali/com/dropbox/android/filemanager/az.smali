.class final Lcom/dropbox/android/filemanager/az;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/z/aA;

.field final synthetic b:Lcom/dropbox/android/filemanager/ay;


# direct methods
.method constructor <init>(Lcom/dropbox/android/filemanager/ay;Ldbxyzptlk/db231222/z/aA;)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iput-object p2, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 463
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    iget-boolean v1, v1, Ldbxyzptlk/db231222/z/aA;->b:Z

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->b(Lcom/dropbox/android/filemanager/au;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    const-string v0, "update_available"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->e(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "force"

    iget-object v2, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    iget-boolean v2, v2, Ldbxyzptlk/db231222/z/aA;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "use_play_store"

    iget-object v2, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    iget-boolean v2, v2, Ldbxyzptlk/db231222/z/aA;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "version"

    iget-object v2, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/aA;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 474
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    iget-boolean v0, v0, Ldbxyzptlk/db231222/z/aA;->c:Z

    if-eqz v0, :cond_2

    .line 475
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    new-instance v1, Lcom/dropbox/android/filemanager/aE;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    invoke-direct {v1, v2}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;)V

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aE;)Lcom/dropbox/android/filemanager/aE;

    .line 476
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v0, v3}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Landroid/app/Activity;)V

    goto :goto_0

    .line 483
    :cond_2
    new-instance v0, Lcom/dropbox/android/filemanager/aE;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/az;->a:Ldbxyzptlk/db231222/z/aA;

    invoke-direct {v0, v1}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;)V

    .line 484
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aE;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 485
    iget-object v1, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1, v0}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aE;)Lcom/dropbox/android/filemanager/aE;

    .line 486
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v0, v3}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Landroid/app/Activity;)V

    goto :goto_0

    .line 491
    :cond_3
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aE;->e()V

    .line 496
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    .line 497
    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/dropbox/android/filemanager/az;->b:Lcom/dropbox/android/filemanager/ay;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ay;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->d(Lcom/dropbox/android/filemanager/au;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/filemanager/aA;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/aA;-><init>(Lcom/dropbox/android/filemanager/az;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
