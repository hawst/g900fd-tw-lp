.class public final enum Lcom/dropbox/android/filemanager/aC;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/filemanager/aC;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/filemanager/aC;

.field public static final enum b:Lcom/dropbox/android/filemanager/aC;

.field public static final enum c:Lcom/dropbox/android/filemanager/aC;

.field private static final synthetic d:[Lcom/dropbox/android/filemanager/aC;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 437
    new-instance v0, Lcom/dropbox/android/filemanager/aC;

    const-string v1, "IF_NEEDED"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/filemanager/aC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/aC;->a:Lcom/dropbox/android/filemanager/aC;

    .line 444
    new-instance v0, Lcom/dropbox/android/filemanager/aC;

    const-string v1, "IMMEDIATELY"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/filemanager/aC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/aC;->b:Lcom/dropbox/android/filemanager/aC;

    .line 452
    new-instance v0, Lcom/dropbox/android/filemanager/aC;

    const-string v1, "IMMEDIATELY_AND_NAG_IF_UPDATE_AVAIL"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/filemanager/aC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/aC;->c:Lcom/dropbox/android/filemanager/aC;

    .line 433
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/filemanager/aC;

    sget-object v1, Lcom/dropbox/android/filemanager/aC;->a:Lcom/dropbox/android/filemanager/aC;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/filemanager/aC;->b:Lcom/dropbox/android/filemanager/aC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/filemanager/aC;->c:Lcom/dropbox/android/filemanager/aC;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/filemanager/aC;->d:[Lcom/dropbox/android/filemanager/aC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 433
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/filemanager/aC;
    .locals 1

    .prologue
    .line 433
    const-class v0, Lcom/dropbox/android/filemanager/aC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/aC;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/filemanager/aC;
    .locals 1

    .prologue
    .line 433
    sget-object v0, Lcom/dropbox/android/filemanager/aC;->d:[Lcom/dropbox/android/filemanager/aC;

    invoke-virtual {v0}, [Lcom/dropbox/android/filemanager/aC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/filemanager/aC;

    return-object v0
.end method
