.class public final Lcom/dropbox/android/filemanager/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/ag;


# static fields
.field private static final h:I


# instance fields
.field protected a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/filemanager/m;",
            ">;>;"
        }
    .end annotation
.end field

.field protected final b:Lcom/dropbox/android/util/q;

.field protected final c:Landroid/os/Handler;

.field protected final d:Lcom/dropbox/android/filemanager/k;

.field protected e:Z

.field protected f:Z

.field protected final g:I

.field private i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/util/n;",
            ">;"
        }
    .end annotation
.end field

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/dropbox/android/util/bn;->i()I

    move-result v0

    sput v0, Lcom/dropbox/android/filemanager/i;->h:I

    .line 27
    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/k;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Landroid/util/SparseArray;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Landroid/os/Handler;

    .line 56
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->e:Z

    .line 58
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    .line 61
    iput v1, p0, Lcom/dropbox/android/filemanager/i;->j:I

    .line 66
    new-instance v0, Lcom/dropbox/android/util/q;

    sget v1, Lcom/dropbox/android/filemanager/i;->h:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/q;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    .line 67
    iput-object p2, p0, Lcom/dropbox/android/filemanager/i;->d:Lcom/dropbox/android/filemanager/k;

    .line 68
    iput p3, p0, Lcom/dropbox/android/filemanager/i;->g:I

    .line 69
    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/k;Lcom/dropbox/android/filemanager/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Landroid/util/SparseArray;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Landroid/os/Handler;

    .line 56
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->e:Z

    .line 58
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    .line 61
    iput v1, p0, Lcom/dropbox/android/filemanager/i;->j:I

    .line 72
    iput-object p2, p0, Lcom/dropbox/android/filemanager/i;->d:Lcom/dropbox/android/filemanager/k;

    .line 73
    new-instance v0, Lcom/dropbox/android/util/q;

    sget v1, Lcom/dropbox/android/filemanager/i;->h:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/q;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    .line 74
    iget v0, p3, Lcom/dropbox/android/filemanager/i;->g:I

    iput v0, p0, Lcom/dropbox/android/filemanager/i;->g:I

    .line 75
    iget-object v0, p3, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p3, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    .line 77
    return-void
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x0

    .line 107
    iget v1, p0, Lcom/dropbox/android/filemanager/i;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dropbox/android/filemanager/i;->j:I

    .line 108
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 109
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/n;

    invoke-virtual {v1, p2, v0}, Lcom/dropbox/android/util/q;->a(ILcom/dropbox/android/util/n;)V

    .line 112
    const/4 v0, 0x1

    .line 114
    :cond_0
    iget v1, p0, Lcom/dropbox/android/filemanager/i;->j:I

    iget-object v2, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 115
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 118
    :cond_1
    return v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/n;

    .line 131
    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    goto :goto_0

    .line 133
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    .line 135
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/filemanager/m;)Lcom/dropbox/android/util/n;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/q;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/q;->c(I)Lcom/dropbox/android/util/n;

    move-result-object v0

    .line 154
    :goto_0
    return-object v0

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Lcom/dropbox/android/filemanager/k;

    invoke-interface {v0, p1}, Lcom/dropbox/android/filemanager/k;->a(I)Lcom/dropbox/android/filemanager/l;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/filemanager/i;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 150
    :goto_1
    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/q;->c(I)Lcom/dropbox/android/util/n;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 153
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/filemanager/i;->b(ILcom/dropbox/android/filemanager/m;)V

    .line 154
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Lcom/dropbox/android/util/q;->c()Landroid/util/SparseArray;

    move-result-object v1

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    .line 98
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/dropbox/android/filemanager/i;->i:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/i;->d:Lcom/dropbox/android/filemanager/k;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/dropbox/android/filemanager/k;->a(I)Lcom/dropbox/android/filemanager/l;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Lcom/dropbox/android/util/q;->a()V

    .line 103
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/util/q;->a(II)V

    .line 139
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/i;->b()V

    .line 140
    return-void
.end method

.method public final a(ILcom/dropbox/android/filemanager/ai;)V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/filemanager/j;

    invoke-direct {v1, p0, p2, p1}, Lcom/dropbox/android/filemanager/j;-><init>(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/ai;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 234
    return-void
.end method

.method protected final b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 182
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/i;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Lcom/dropbox/android/util/q;->b()I

    move-result v4

    .line 184
    const/4 v0, -0x1

    if-eq v4, v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/q;

    invoke-virtual {v0, v4}, Lcom/dropbox/android/util/q;->b(I)V

    .line 186
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Lcom/dropbox/android/filemanager/k;

    invoke-interface {v0, v4}, Lcom/dropbox/android/filemanager/k;->a(I)Lcom/dropbox/android/filemanager/l;

    move-result-object v3

    .line 187
    if-eqz v3, :cond_2

    iget-object v0, v3, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 188
    iget-object v0, v3, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v4}, Lcom/dropbox/android/filemanager/i;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 189
    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/i;->b()V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->e:Z

    .line 194
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    iget-object v1, v3, Lcom/dropbox/android/filemanager/l;->a:Ljava/lang/String;

    iget-object v2, v3, Lcom/dropbox/android/filemanager/l;->b:Ljava/lang/String;

    iget-boolean v3, v3, Lcom/dropbox/android/filemanager/l;->c:Z

    iget v5, p0, Lcom/dropbox/android/filemanager/i;->g:I

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;

    goto :goto_0

    .line 197
    :cond_2
    if-eqz v3, :cond_3

    iget-object v0, v3, Lcom/dropbox/android/filemanager/l;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 199
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->e:Z

    .line 200
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    iget-object v1, v3, Lcom/dropbox/android/filemanager/l;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/filemanager/i;->g:I

    invoke-virtual {v0, v1, v4, v2, p0}, Lcom/dropbox/android/filemanager/X;->b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;

    goto :goto_0

    .line 203
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/i;->b()V

    goto :goto_0
.end method

.method protected final b(ILcom/dropbox/android/filemanager/m;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 161
    if-eqz v0, :cond_0

    .line 163
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    :goto_0
    return-void

    .line 165
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 166
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(ILcom/dropbox/android/filemanager/m;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 173
    if-eqz v0, :cond_0

    .line 174
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 175
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 179
    :cond_0
    return-void
.end method
