.class final Lcom/dropbox/android/filemanager/aB;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/g/l;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/au;


# direct methods
.method constructor <init>(Lcom/dropbox/android/filemanager/au;)V
    .locals 0

    .prologue
    .line 536
    iput-object p1, p0, Lcom/dropbox/android/filemanager/aB;->a:Lcom/dropbox/android/filemanager/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/z/aA;Ldbxyzptlk/db231222/g/n;)V
    .locals 3

    .prologue
    .line 540
    new-instance v0, Lcom/dropbox/android/filemanager/aE;

    iget-object v1, p2, Ldbxyzptlk/db231222/g/n;->a:Ljava/io/File;

    iget-object v2, p2, Ldbxyzptlk/db231222/g/n;->b:Ljava/io/File;

    invoke-direct {v0, p1, v1, v2}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;Ljava/io/File;Ljava/io/File;)V

    .line 543
    iget-object v1, p0, Lcom/dropbox/android/filemanager/aB;->a:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1, v0}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aE;)Lcom/dropbox/android/filemanager/aE;

    .line 544
    iget-object v0, p0, Lcom/dropbox/android/filemanager/aB;->a:Lcom/dropbox/android/filemanager/au;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Landroid/app/Activity;)V

    .line 545
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 549
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Download failed, message=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    return-void
.end method
