.class public Lcom/dropbox/android/filemanager/au;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/dropbox/android/filemanager/au;


# instance fields
.field private c:J

.field private d:Lcom/dropbox/android/filemanager/aE;

.field private e:Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

.field private f:Lcom/dropbox/android/filemanager/aD;

.field private g:Z

.field private final h:Landroid/os/Handler;

.field private final i:Ldbxyzptlk/db231222/g/ac;

.field private final j:Ldbxyzptlk/db231222/g/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/dropbox/android/filemanager/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/au;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/filemanager/au;->c:J

    .line 87
    new-instance v0, Lcom/dropbox/android/filemanager/aE;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/au;->g:Z

    .line 455
    new-instance v0, Lcom/dropbox/android/filemanager/ay;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/ay;-><init>(Lcom/dropbox/android/filemanager/au;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/au;->i:Ldbxyzptlk/db231222/g/ac;

    .line 535
    new-instance v0, Lcom/dropbox/android/filemanager/aB;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/aB;-><init>(Lcom/dropbox/android/filemanager/au;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/au;->j:Ldbxyzptlk/db231222/g/l;

    .line 227
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 228
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/au;->h:Landroid/os/Handler;

    .line 229
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aD;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/filemanager/au;->f:Lcom/dropbox/android/filemanager/aD;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aD;)Lcom/dropbox/android/filemanager/aD;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/dropbox/android/filemanager/au;->f:Lcom/dropbox/android/filemanager/aD;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aE;)Lcom/dropbox/android/filemanager/aE;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    return-object p1
.end method

.method public static a()Lcom/dropbox/android/filemanager/au;
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/dropbox/android/filemanager/au;->b:Lcom/dropbox/android/filemanager/au;

    if-nez v0, :cond_0

    .line 233
    new-instance v0, Lcom/dropbox/android/filemanager/au;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/au;-><init>()V

    sput-object v0, Lcom/dropbox/android/filemanager/au;->b:Lcom/dropbox/android/filemanager/au;

    .line 235
    :cond_0
    sget-object v0, Lcom/dropbox/android/filemanager/au;->b:Lcom/dropbox/android/filemanager/au;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/au;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/au;->b(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic a(Landroid/content/ContentResolver;)Z
    .locals 1

    .prologue
    .line 74
    invoke-static {p0}, Lcom/dropbox/android/filemanager/au;->b(Landroid/content/ContentResolver;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/au;Z)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/au;->a(Z)Z

    move-result v0

    return v0
.end method

.method public static a(Ldbxyzptlk/db231222/n/K;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 615
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/K;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/dropbox/android/filemanager/au;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 273
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->e()V

    .line 276
    new-instance v1, Lcom/dropbox/android/filemanager/aE;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;)V

    iput-object v1, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    .line 280
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->a()Ldbxyzptlk/db231222/z/aA;

    move-result-object v1

    iget-boolean v1, v1, Ldbxyzptlk/db231222/z/aA;->b:Z

    invoke-direct {p0, v1}, Lcom/dropbox/android/filemanager/au;->b(Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->d()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aE;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    return-object v0
.end method

.method private b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/dropbox/android/filemanager/au;->h:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/filemanager/aw;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/filemanager/aw;-><init>(Lcom/dropbox/android/filemanager/au;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 328
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/au;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/au;->c(Landroid/app/Activity;)V

    return-void
.end method

.method private static b(Landroid/content/ContentResolver;)Z
    .locals 4

    .prologue
    .line 578
    invoke-static {p0}, Lcom/dropbox/android/filemanager/au;->c(Landroid/content/ContentResolver;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 606
    invoke-static {p0}, Lcom/dropbox/android/util/g;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->c(Landroid/content/ContentResolver;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/au;Z)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/au;->b(Z)Z

    move-result v0

    return v0
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 284
    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/au;->g:Z

    if-eqz v1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    if-nez p1, :cond_0

    .line 292
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v1

    .line 293
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v2

    iget-object v2, v2, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    .line 294
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 295
    invoke-static {}, Ldbxyzptlk/db231222/n/K;->a()Ldbxyzptlk/db231222/n/K;

    move-result-object v5

    invoke-virtual {v5}, Ldbxyzptlk/db231222/n/K;->f()J

    move-result-wide v5

    .line 298
    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/k;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_2

    sub-long v1, v3, v5

    const-wide/32 v3, 0x5265c00

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static c(Landroid/content/ContentResolver;)J
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const-wide/16 v1, -0x1

    .line 584
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    const-string v0, "install_non_market_apps"

    invoke-static {p0, v0, v1, v2}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    .line 587
    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "install_non_market_apps"

    invoke-static {p0, v0, v1, v2}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/dropbox/android/filemanager/au;)Ldbxyzptlk/db231222/g/l;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/filemanager/au;->j:Ldbxyzptlk/db231222/g/l;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/dropbox/android/filemanager/au;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c(Landroid/app/Activity;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 332
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 333
    if-nez p1, :cond_3

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    .line 334
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->a()Ldbxyzptlk/db231222/z/aA;

    move-result-object v1

    .line 335
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v2

    .line 337
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v3

    iget-object v3, v3, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    .line 338
    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/n/k;->b(Ljava/lang/String;)V

    .line 339
    invoke-static {}, Ldbxyzptlk/db231222/n/K;->a()Ldbxyzptlk/db231222/n/K;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ldbxyzptlk/db231222/n/K;->b(J)V

    .line 340
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dropbox/android/filemanager/au;->g:Z

    .line 342
    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 343
    const-string v3, "EXTRA_FORCE_UPDATE"

    iget-boolean v4, v1, Ldbxyzptlk/db231222/z/aA;->b:Z

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 345
    const-string v3, "EXTRA_USE_PLAY_STORE"

    iget-boolean v4, v1, Ldbxyzptlk/db231222/z/aA;->c:Z

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 347
    const-string v3, "EXTRA_RELEASE_NOTES"

    iget-object v4, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/aE;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    new-instance v3, Lcom/dropbox/android/notifications/d;

    invoke-direct {v3, v0, v5}, Lcom/dropbox/android/notifications/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    sget-object v4, Lcom/dropbox/android/util/aM;->h:Lcom/dropbox/android/util/aM;

    invoke-virtual {v3, v4, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 352
    iget-object v2, p0, Lcom/dropbox/android/filemanager/au;->e:Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    if-eqz v2, :cond_0

    .line 353
    sget-object v2, Lcom/dropbox/android/filemanager/au;->a:Ljava/lang/String;

    const-string v3, "Dismissing existing update dialog."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v2, p0, Lcom/dropbox/android/filemanager/au;->e:Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->dismissAllowingStateLoss()V

    .line 355
    iput-object v5, p0, Lcom/dropbox/android/filemanager/au;->e:Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    .line 358
    :cond_0
    invoke-static {}, Lcom/dropbox/android/activity/base/d;->a()I

    move-result v2

    if-lez v2, :cond_2

    .line 359
    sget-object v2, Lcom/dropbox/android/filemanager/au;->a:Ljava/lang/String;

    const-string v3, "Showing update dialog."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/dropbox/android/activity/InstallUpdateActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 361
    const-string v3, "EXTRA_FORCE_UPDATE"

    iget-boolean v4, v1, Ldbxyzptlk/db231222/z/aA;->b:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 362
    const-string v3, "EXTRA_USE_PLAY_STORE"

    iget-boolean v1, v1, Ldbxyzptlk/db231222/z/aA;->c:Z

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 363
    const-string v1, "EXTRA_RELEASE_NOTES"

    iget-object v3, p0, Lcom/dropbox/android/filemanager/au;->d:Lcom/dropbox/android/filemanager/aE;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/aE;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    if-nez p1, :cond_1

    .line 366
    const/high16 v1, 0x10000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 368
    :cond_1
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 370
    :cond_2
    return-void

    :cond_3
    move-object v0, p1

    .line 333
    goto/16 :goto_0
.end method

.method private static d()Landroid/net/Uri;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 594
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    const-string v0, "install_non_market_apps"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 597
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "install_non_market_apps"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/dropbox/android/filemanager/au;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/filemanager/au;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/filemanager/au;->e:Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/filemanager/av;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/filemanager/av;-><init>(Lcom/dropbox/android/filemanager/au;Landroid/app/Activity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    monitor-exit p0

    return-void

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 378
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/filemanager/ax;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/filemanager/ax;-><init>(Lcom/dropbox/android/filemanager/au;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 431
    return-void
.end method

.method public final a(Landroid/content/Context;Ldbxyzptlk/db231222/n/K;)V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/dropbox/android/filemanager/au;->e:Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    .line 374
    return-void
.end method

.method public final declared-synchronized a(Lcom/dropbox/android/filemanager/aC;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/analytics/r;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 560
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 561
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 562
    sget-object v2, Lcom/dropbox/android/filemanager/aC;->a:Lcom/dropbox/android/filemanager/aC;

    if-eq p1, v2, :cond_2

    move v2, v0

    .line 564
    :goto_0
    if-nez v2, :cond_0

    iget-wide v5, p0, Lcom/dropbox/android/filemanager/au;->c:J

    sub-long v5, v3, v5

    const-wide/32 v7, 0x5265c00

    cmp-long v2, v5, v7

    if-lez v2, :cond_1

    .line 567
    :cond_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ldbxyzptlk/db231222/n/K;->a(J)V

    .line 568
    iput-wide v3, p0, Lcom/dropbox/android/filemanager/au;->c:J

    .line 569
    sget-object v2, Lcom/dropbox/android/filemanager/aC;->c:Lcom/dropbox/android/filemanager/aC;

    if-ne p1, v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/au;->g:Z

    .line 570
    new-instance v0, Ldbxyzptlk/db231222/g/ab;

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/filemanager/au;->i:Ldbxyzptlk/db231222/g/ac;

    invoke-direct {v0, v1, p2, v2, p3}, Ldbxyzptlk/db231222/g/ab;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/g/ac;Lcom/dropbox/android/util/analytics/r;)V

    .line 572
    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/ab;->f()V

    .line 573
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/ab;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 575
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    .line 562
    goto :goto_0

    :cond_3
    move v0, v1

    .line 569
    goto :goto_1

    .line 560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ldbxyzptlk/db231222/n/K;)V
    .locals 1

    .prologue
    .line 249
    const-string v0, "user_disabled"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->e(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 250
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/n/K;->a(Z)V

    .line 251
    invoke-static {}, Lcom/dropbox/android/util/by;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/n/K;->a(I)V

    .line 252
    invoke-static {}, Lcom/dropbox/android/service/ReportReceiver;->b()V

    .line 253
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/r/d;)V
    .locals 2

    .prologue
    .line 239
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 240
    const-string v0, "user_enabled"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->e(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 241
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/K;->a(Z)V

    .line 242
    invoke-static {}, Lcom/dropbox/android/service/ReportReceiver;->a()V

    .line 245
    sget-object v0, Lcom/dropbox/android/filemanager/aC;->c:Lcom/dropbox/android/filemanager/aC;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/aC;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/analytics/r;)V

    .line 246
    return-void
.end method
