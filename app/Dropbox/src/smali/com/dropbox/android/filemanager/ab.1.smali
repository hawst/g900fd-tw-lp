.class final Lcom/dropbox/android/filemanager/ab;
.super Lcom/dropbox/android/filemanager/ac;
.source "panda.py"


# direct methods
.method public constructor <init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)V
    .locals 0

    .prologue
    .line 681
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/ac;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)V

    .line 682
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/ai;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 687
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ab;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 688
    invoke-static {p1, v3}, Lcom/dropbox/android/filemanager/X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    .line 689
    if-eqz v4, :cond_0

    .line 690
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/dropbox/android/filemanager/ab;->d:I

    invoke-static {v0, p1, v1, v2}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 691
    if-nez v0, :cond_2

    .line 692
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/dropbox/android/filemanager/ab;->d:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/dropbox/android/filemanager/ad;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    .line 694
    :goto_0
    if-eqz v2, :cond_0

    .line 695
    new-instance v1, Lcom/dropbox/android/filemanager/ai;

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v3

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/ad;

    iget v4, p0, Lcom/dropbox/android/filemanager/ab;->c:I

    invoke-static {v0, p1, v4}, Lcom/dropbox/android/filemanager/ad;->a(Lcom/dropbox/android/filemanager/ad;Landroid/content/Context;I)J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    move-object v0, v1

    .line 708
    :goto_1
    return-object v0

    .line 704
    :cond_0
    iget v0, p0, Lcom/dropbox/android/filemanager/ab;->d:I

    invoke-static {v3, v5, v5, v0}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 705
    if-eqz v1, :cond_1

    .line 706
    new-instance v0, Lcom/dropbox/android/filemanager/ai;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/dropbox/android/filemanager/ai;-><init>(Landroid/graphics/Bitmap;ZJ)V

    goto :goto_1

    .line 708
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object v2, v0

    goto :goto_0
.end method
