.class final Lcom/dropbox/android/filemanager/ax;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/dropbox/android/filemanager/au;


# direct methods
.method constructor <init>(Lcom/dropbox/android/filemanager/au;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    iput-object p2, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    const/4 v3, 0x0

    .line 381
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 382
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aD;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aD;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 384
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1, v3}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aD;)Lcom/dropbox/android/filemanager/aD;

    .line 388
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/au;->b(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 389
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->b(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aE;->e()V

    .line 391
    new-instance v0, Lcom/dropbox/android/notifications/d;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/notifications/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    sget-object v1, Lcom/dropbox/android/util/aM;->h:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 392
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    new-instance v1, Lcom/dropbox/android/filemanager/aE;

    invoke-direct {v1, v3}, Lcom/dropbox/android/filemanager/aE;-><init>(Ldbxyzptlk/db231222/z/aA;)V

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aE;)Lcom/dropbox/android/filemanager/aE;

    .line 429
    :cond_1
    :goto_0
    return-void

    .line 396
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/au;->b(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->a()Ldbxyzptlk/db231222/z/aA;

    move-result-object v1

    iget-boolean v1, v1, Ldbxyzptlk/db231222/z/aA;->c:Z

    if-eqz v1, :cond_5

    .line 397
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/i/c;->b:Ljava/lang/String;

    .line 398
    if-eqz v0, :cond_3

    const-string v1, "com."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 399
    :cond_3
    const-string v0, "com.dropbox.android"

    .line 401
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "market://details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 404
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/Activities;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 410
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 415
    :cond_5
    invoke-static {v0}, Lcom/dropbox/android/filemanager/au;->a(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 416
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    new-instance v1, Lcom/dropbox/android/filemanager/aD;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/filemanager/aD;-><init>(Lcom/dropbox/android/filemanager/au;Landroid/os/Handler;)V

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;Lcom/dropbox/android/filemanager/aD;)Lcom/dropbox/android/filemanager/aD;

    .line 417
    iget-object v0, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/filemanager/au;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v3}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aD;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 423
    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->b:Lcom/dropbox/android/filemanager/au;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/au;->b(Lcom/dropbox/android/filemanager/au;)Lcom/dropbox/android/filemanager/aE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/aE;->b()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 426
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 427
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ax;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
