.class abstract Lcom/dropbox/android/filemanager/ah;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field protected final b:Lcom/dropbox/android/filemanager/ag;

.field protected final c:I

.field protected final d:I


# direct methods
.method private constructor <init>(Lcom/dropbox/android/filemanager/ag;II)V
    .locals 0

    .prologue
    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    iput-object p1, p0, Lcom/dropbox/android/filemanager/ah;->b:Lcom/dropbox/android/filemanager/ag;

    .line 618
    iput p2, p0, Lcom/dropbox/android/filemanager/ah;->c:I

    .line 619
    iput p3, p0, Lcom/dropbox/android/filemanager/ah;->d:I

    .line 620
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/filemanager/ag;IILcom/dropbox/android/filemanager/Y;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/ah;-><init>(Lcom/dropbox/android/filemanager/ag;II)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/ah;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/ah;->b(Landroid/content/Context;)V

    return-void
.end method

.method private final b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 625
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/ah;->a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v0

    .line 627
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ah;->b:Lcom/dropbox/android/filemanager/ag;

    if-nez v1, :cond_1

    .line 629
    if-eqz v0, :cond_0

    .line 630
    iget-object v0, v0, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/filemanager/ah;->b:Lcom/dropbox/android/filemanager/ag;

    iget v2, p0, Lcom/dropbox/android/filemanager/ah;->c:I

    invoke-interface {v1, v2, v0}, Lcom/dropbox/android/filemanager/ag;->a(ILcom/dropbox/android/filemanager/ai;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/ai;
.end method
