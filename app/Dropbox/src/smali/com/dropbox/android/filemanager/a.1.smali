.class public Lcom/dropbox/android/filemanager/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile b:Lcom/dropbox/android/filemanager/a;


# instance fields
.field private volatile c:Ldbxyzptlk/db231222/z/F;

.field private final d:Lcom/dropbox/android/gcm/GcmSubscriber;

.field private final e:Landroid/content/Context;

.field private final f:Ldbxyzptlk/db231222/n/k;

.field private final g:Ldbxyzptlk/db231222/r/e;

.field private final h:Lcom/dropbox/sync/android/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/dropbox/android/filemanager/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/a;->b:Lcom/dropbox/android/filemanager/a;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;)V
    .locals 2

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Lcom/dropbox/android/filemanager/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/b;-><init>(Lcom/dropbox/android/filemanager/a;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/a;->h:Lcom/dropbox/sync/android/ac;

    .line 135
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    .line 136
    iput-object p2, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    .line 137
    iput-object p3, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    .line 138
    iput-object p4, p0, Lcom/dropbox/android/filemanager/a;->d:Lcom/dropbox/android/gcm/GcmSubscriber;

    .line 139
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->b()V

    .line 140
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->h:Lcom/dropbox/sync/android/ac;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/r/e;->a(Lcom/dropbox/sync/android/ac;)V

    .line 141
    return-void
.end method

.method public static a()Lcom/dropbox/android/filemanager/a;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Lcom/dropbox/android/filemanager/a;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 158
    :cond_0
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Lcom/dropbox/android/filemanager/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;)Lcom/dropbox/android/filemanager/a;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Lcom/dropbox/android/filemanager/a;

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lcom/dropbox/android/filemanager/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/a;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/gcm/GcmSubscriber;)V

    sput-object v0, Lcom/dropbox/android/filemanager/a;->b:Lcom/dropbox/android/filemanager/a;

    .line 151
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Lcom/dropbox/android/filemanager/a;

    return-object v0

    .line 149
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;
    .locals 5

    .prologue
    .line 396
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 397
    sget-object v0, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v1, "handling logged in user"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->g()V

    .line 403
    new-instance v0, Ldbxyzptlk/db231222/z/M;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/H;->c()Ldbxyzptlk/db231222/y/k;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/F;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/filemanager/a;->a(Ldbxyzptlk/db231222/y/k;Ldbxyzptlk/db231222/z/D;)Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->c()Ldbxyzptlk/db231222/v/r;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/z/M;-><init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V

    .line 405
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/M;->d()Ldbxyzptlk/db231222/z/R;

    move-result-object v0

    .line 407
    monitor-enter p0

    .line 409
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->k()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "uid"

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 410
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aB()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 411
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aQ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "uid"

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "device_id"

    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v3

    iget-object v3, v3, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 414
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/H;->c()Ldbxyzptlk/db231222/y/k;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/z/R;Ldbxyzptlk/db231222/y/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 416
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ct()V

    .line 418
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/payments/DbxSubscriptions;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/k;)V

    .line 420
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/content/Context;)V

    .line 422
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    invoke-static {v1, v0, v2}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;Landroid/content/Context;)V

    .line 425
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 426
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->G()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 427
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v2

    .line 428
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->H()Z

    move-result v1

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/n/P;->m(Z)V

    .line 431
    :cond_0
    monitor-exit p0

    return-object v0

    .line 432
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/a;)Ldbxyzptlk/db231222/r/e;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/r/d;)Ldbxyzptlk/db231222/z/D;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Ldbxyzptlk/db231222/z/D;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/z/D;-><init>(Ldbxyzptlk/db231222/r/d;)V

    .line 195
    return-object v0
.end method

.method private static a(Ldbxyzptlk/db231222/y/k;Ldbxyzptlk/db231222/z/D;)Ldbxyzptlk/db231222/z/D;
    .locals 3

    .prologue
    .line 210
    new-instance v0, Lcom/dropbox/android/filemanager/d;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/z/D;->f()Ldbxyzptlk/db231222/y/l;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/dropbox/android/filemanager/d;-><init>(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/y/k;Ldbxyzptlk/db231222/z/D;)V

    return-object v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 610
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->D()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 613
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.android.filemanager.ApiManager.ACTION_UNLINKED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 614
    invoke-static {p0}, Landroid/support/v4/content/s;->a(Landroid/content/Context;)Landroid/support/v4/content/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/s;->b(Landroid/content/Intent;)V

    .line 615
    return-void
.end method

.method public static c()Ldbxyzptlk/db231222/v/r;
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/dropbox/android/filemanager/e;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/e;-><init>()V

    return-object v0
.end method

.method private declared-synchronized c(Ldbxyzptlk/db231222/r/d;Z)V
    .locals 3

    .prologue
    .line 471
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 473
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 476
    sget-object v0, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v1, "User has already been deauthenticated!"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 559
    :goto_0
    monitor-exit p0

    return-void

    .line 480
    :cond_0
    :try_start_1
    sget-object v0, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v1, "Deauthenticating dropbox."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aC()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 484
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    .line 485
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/service/i;->a(Lcom/dropbox/android/provider/j;)V

    .line 486
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/albums/PhotosModel;->a()V

    .line 487
    invoke-static {}, Lcom/dropbox/android/service/ReportReceiver;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    :try_start_2
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->d:Lcom/dropbox/android/gcm/GcmSubscriber;

    invoke-virtual {v1}, Lcom/dropbox/android/gcm/GcmSubscriber;->b()V
    :try_end_2
    .catch Lcom/dropbox/android/gcm/a; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 498
    :goto_1
    :try_start_3
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->a()V

    .line 499
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->d()V

    .line 500
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->B()Lcom/dropbox/android/util/aE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/aE;->d()V

    .line 503
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/K;->b()V

    .line 505
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/MetadataManager;->c()V

    .line 506
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/I;->d()V

    .line 511
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/k;->d(Z)V

    .line 513
    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->a()V

    .line 515
    invoke-static {}, Ldbxyzptlk/db231222/i/a;->a()V

    .line 519
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/d;->c()V

    .line 521
    if-eqz p2, :cond_2

    .line 527
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->d()V

    .line 542
    :goto_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    .line 543
    if-eqz v0, :cond_1

    .line 551
    invoke-static {v0}, Lcom/dropbox/sync/android/bx;->a(Lcom/dropbox/sync/android/V;)V

    .line 554
    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ct()V

    .line 556
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/content/Context;)V

    .line 558
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/a;->a(Landroid/content/Context;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 471
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 534
    :cond_2
    const/4 v0, 0x1

    :try_start_4
    sput-boolean v0, Lcom/dropbox/android/authenticator/a;->a:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 536
    :try_start_5
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->g:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 538
    const/4 v0, 0x0

    :try_start_6
    sput-boolean v0, Lcom/dropbox/android/authenticator/a;->a:Z

    goto :goto_2

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/dropbox/android/authenticator/a;->a:Z

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 491
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/dropbox/android/filemanager/h;
    .locals 2

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->e:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Ldbxyzptlk/db231222/z/F;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/dropbox/android/filemanager/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/filemanager/h;Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 276
    sget-object v0, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v1, "Retrieving access token for SSO user"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    iget-object v1, p1, Lcom/dropbox/android/filemanager/h;->b:Ldbxyzptlk/db231222/y/m;

    invoke-virtual {v0, v1, p2}, Ldbxyzptlk/db231222/z/F;->a(Ldbxyzptlk/db231222/y/m;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;

    move-result-object v0

    .line 279
    sget-object v1, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v2, "Successfully authenticated via SSO"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->a(J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 281
    iget-object v1, p1, Lcom/dropbox/android/filemanager/h;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/z/K;

    move-result-object v0

    .line 289
    sget-object v1, Ldbxyzptlk/db231222/z/K;->c:Ldbxyzptlk/db231222/z/K;

    if-eq v0, v1, :cond_0

    sget-object v1, Ldbxyzptlk/db231222/z/K;->b:Ldbxyzptlk/db231222/z/K;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 291
    :cond_0
    new-instance v1, Lcom/dropbox/android/filemanager/g;

    invoke-direct {v1, v0}, Lcom/dropbox/android/filemanager/g;-><init>(Ldbxyzptlk/db231222/z/K;)V

    throw v1

    .line 293
    :cond_1
    sget-object v1, Ldbxyzptlk/db231222/z/K;->b:Ldbxyzptlk/db231222/z/K;

    if-ne v0, v1, :cond_2

    .line 294
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aT()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 297
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 300
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->b()Ldbxyzptlk/db231222/n/s;

    move-result-object v0

    .line 301
    sget-object v1, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v2, "Partially authenticated - need twofactor"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    new-instance v2, Ldbxyzptlk/db231222/n/u;

    invoke-direct {v2, v0, p1}, Ldbxyzptlk/db231222/n/u;-><init>(Ldbxyzptlk/db231222/n/s;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/n/u;)V

    .line 304
    new-instance v0, Lcom/dropbox/android/filemanager/f;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/f;-><init>()V

    throw v0

    .line 306
    :cond_3
    sget-object v1, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v2, "Successfully authenticated"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->b(J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 308
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;

    move-result-object v0

    .line 369
    sget-object v1, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v2, "Successfully created new user"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->e(J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 371
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/r/d;Z)V
    .locals 1

    .prologue
    .line 437
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 438
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/M;->e()V

    .line 439
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/a;->c(Ldbxyzptlk/db231222/r/d;Z)V

    .line 440
    return-void
.end method

.method public final b(Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    .locals 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->m()Ldbxyzptlk/db231222/n/u;

    move-result-object v0

    .line 315
    if-nez v0, :cond_0

    .line 316
    new-instance v0, Ldbxyzptlk/db231222/w/a;

    const-string v1, "Tried to log in without twofactor checkpoint token"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ldbxyzptlk/db231222/z/F;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;

    move-result-object v1

    .line 320
    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/a;->f(J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 322
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/u;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 199
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/dropbox/android/filemanager/a;->a(Ldbxyzptlk/db231222/r/d;)Ldbxyzptlk/db231222/z/D;

    move-result-object v0

    .line 200
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->c()Ldbxyzptlk/db231222/v/r;

    move-result-object v1

    .line 201
    new-instance v2, Ldbxyzptlk/db231222/z/F;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/db231222/z/F;-><init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V

    iput-object v2, p0, Lcom/dropbox/android/filemanager/a;->c:Ldbxyzptlk/db231222/z/F;

    .line 202
    return-void
.end method

.method public final declared-synchronized b(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 461
    monitor-enter p0

    .line 462
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/filemanager/a;->c(Ldbxyzptlk/db231222/r/d;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    monitor-exit p0

    return-void

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ldbxyzptlk/db231222/r/d;Z)V
    .locals 1

    .prologue
    .line 443
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 445
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/M;->e()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/a;->c(Ldbxyzptlk/db231222/r/d;Z)V

    .line 450
    return-void

    .line 446
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/z/K;

    move-result-object v0

    .line 360
    sget-object v1, Ldbxyzptlk/db231222/z/K;->c:Ldbxyzptlk/db231222/z/K;

    if-ne v0, v1, :cond_0

    .line 361
    new-instance v1, Lcom/dropbox/android/filemanager/g;

    invoke-direct {v1, v0}, Lcom/dropbox/android/filemanager/g;-><init>(Ldbxyzptlk/db231222/z/K;)V

    throw v1

    .line 363
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/z/F;->d(Ljava/lang/String;)V

    .line 364
    return-void
.end method

.method public final d(Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/z/F;->e(Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;

    move-result-object v0

    .line 376
    sget-object v1, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v2, "Successfully authenticated"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->d(J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 378
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->e()Ljava/lang/String;

    move-result-object v1

    .line 380
    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 381
    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 7

    .prologue
    .line 334
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->m()Ldbxyzptlk/db231222/n/u;

    move-result-object v6

    .line 336
    if-nez v6, :cond_0

    .line 337
    new-instance v0, Ldbxyzptlk/db231222/w/a;

    const-string v1, "Tried to resend twofactor code without having checkpoint token"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v6}, Ldbxyzptlk/db231222/n/u;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/F;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 343
    if-eqz v4, :cond_1

    .line 344
    new-instance v0, Ldbxyzptlk/db231222/n/u;

    invoke-virtual {v6}, Ldbxyzptlk/db231222/n/u;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Ldbxyzptlk/db231222/n/u;->c()J

    move-result-wide v2

    invoke-virtual {v6}, Ldbxyzptlk/db231222/n/u;->e()Ldbxyzptlk/db231222/n/t;

    move-result-object v5

    invoke-virtual {v6}, Ldbxyzptlk/db231222/n/u;->f()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/n/u;-><init>(Ljava/lang/String;JLjava/lang/String;Ldbxyzptlk/db231222/n/t;Ljava/lang/String;)V

    .line 350
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/n/u;)V

    .line 353
    :cond_1
    return-object v4
.end method

.method public final e(Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/z/F;->f(Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;

    move-result-object v0

    .line 386
    sget-object v1, Lcom/dropbox/android/filemanager/a;->a:Ljava/lang/String;

    const-string v2, "Successfully authenticated"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->c(J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 388
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/H;->e()Ljava/lang/String;

    move-result-object v1

    .line 390
    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 391
    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/H;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 566
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/k;->m()Ldbxyzptlk/db231222/n/u;

    move-result-object v1

    .line 567
    if-eqz v1, :cond_1

    .line 568
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/s;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 570
    iget-object v2, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/n/u;)V

    .line 572
    :cond_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/s;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 574
    :cond_1
    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->m()Ldbxyzptlk/db231222/n/u;

    move-result-object v0

    .line 585
    if-nez v0, :cond_0

    .line 586
    const/4 v0, 0x0

    .line 588
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/s;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->m()Ldbxyzptlk/db231222/n/u;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->f:Ldbxyzptlk/db231222/n/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/n/u;)V

    .line 598
    :cond_0
    return-void
.end method

.method public final h()Ldbxyzptlk/db231222/z/F;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->c:Ldbxyzptlk/db231222/z/F;

    return-object v0
.end method
