.class public Lcom/dropbox/android/provider/SDKProvider;
.super Landroid/content/ContentProvider;
.source "panda.py"


# static fields
.field private static final b:Landroid/content/UriMatcher;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 203
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/dropbox/android/provider/SDKProvider;->b:Landroid/content/UriMatcher;

    .line 205
    invoke-static {}, Lcom/dropbox/android/provider/O;->values()[Lcom/dropbox/android/provider/O;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 206
    sget-object v4, Lcom/dropbox/android/provider/SDKProvider;->b:Landroid/content/UriMatcher;

    const-string v5, "com.dropbox.android.provider.SDK"

    invoke-virtual {v3}, Lcom/dropbox/android/provider/O;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/dropbox/android/provider/O;->ordinal()I

    move-result v3

    invoke-virtual {v4, v5, v6, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/dropbox/android/provider/SDKProvider;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 33
    return-void
.end method

.method private static a(Landroid/net/Uri;)Lcom/dropbox/android/provider/O;
    .locals 6

    .prologue
    .line 212
    sget-object v0, Lcom/dropbox/android/provider/SDKProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 214
    invoke-static {}, Lcom/dropbox/android/provider/O;->values()[Lcom/dropbox/android/provider/O;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 215
    invoke-virtual {v0}, Lcom/dropbox/android/provider/O;->ordinal()I

    move-result v5

    if-ne v2, v5, :cond_0

    .line 220
    :goto_1
    return-object v0

    .line 214
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 220
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 302
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/O;->a:Lcom/dropbox/android/provider/O;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/O;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 303
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 296
    if-eqz p0, :cond_0

    .line 297
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 306
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/O;->c:Lcom/dropbox/android/provider/O;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/O;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 307
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 225
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 226
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/O;

    move-result-object v0

    .line 232
    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {v0}, Lcom/dropbox/android/provider/O;->c()Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 273
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 274
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/O;

    move-result-object v0

    .line 275
    if-nez v0, :cond_0

    .line 276
    const/4 v0, 0x0

    .line 280
    :goto_0
    return-object v0

    .line 279
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/provider/O;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/provider/SDKProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/provider/SDKProvider;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 280
    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/provider/O;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 248
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 250
    invoke-virtual {p0}, Lcom/dropbox/android/provider/SDKProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 256
    iget-object v0, p0, Lcom/dropbox/android/provider/SDKProvider;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 258
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bC()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "calling.pkgs"

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 261
    :cond_0
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/O;

    move-result-object v0

    .line 262
    if-nez v0, :cond_1

    .line 263
    const/4 v0, 0x0

    .line 268
    :goto_0
    return-object v0

    .line 266
    :cond_1
    invoke-virtual {v0}, Lcom/dropbox/android/provider/O;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/dropbox/android/provider/SDKProvider;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 267
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 268
    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/provider/O;->a(Ldbxyzptlk/db231222/r/e;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 285
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 286
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/O;

    move-result-object v0

    .line 287
    if-nez v0, :cond_0

    .line 288
    const/4 v0, 0x0

    .line 292
    :goto_0
    return v0

    .line 291
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/provider/O;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/provider/SDKProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/provider/SDKProvider;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 292
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/dropbox/android/provider/O;->a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
