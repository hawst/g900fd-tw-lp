.class final Lcom/dropbox/android/provider/G;
.super Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue",
        "<",
        "Lcom/dropbox/android/provider/MetadataManager$MetadataTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1276
    const/4 v0, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;-><init>(IILjava/util/List;)V

    .line 1265
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/G;->a:Ljava/lang/Object;

    .line 1267
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/G;->b:Ljava/util/HashSet;

    .line 1270
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/G;->c:Ljava/util/HashMap;

    .line 1277
    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/u;)Z
    .locals 6

    .prologue
    .line 1317
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v0

    .line 1318
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 1319
    iget-object v3, p0, Lcom/dropbox/android/provider/G;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 1320
    :try_start_0
    iget-object v4, p0, Lcom/dropbox/android/provider/G;->c:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1321
    iget-object v4, p0, Lcom/dropbox/android/provider/G;->c:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1322
    sub-long v0, v1, v4

    const-wide/16 v4, 0x2710

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 1324
    const/4 v0, 0x0

    monitor-exit v3

    .line 1327
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    monitor-exit v3

    goto :goto_0

    .line 1328
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)Z
    .locals 3

    .prologue
    .line 1311
    iget-object v1, p0, Lcom/dropbox/android/provider/G;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1312
    :try_start_0
    invoke-virtual {p1}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/provider/G;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)V
    .locals 1

    .prologue
    .line 1281
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/G;->d(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1282
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 1284
    :cond_0
    return-void
.end method

.method protected final a(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 5

    .prologue
    .line 1300
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_0

    .line 1301
    invoke-virtual {p1}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->a()Ljava/lang/String;

    move-result-object v0

    .line 1302
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 1303
    iget-object v3, p0, Lcom/dropbox/android/provider/G;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 1304
    :try_start_0
    iget-object v4, p0, Lcom/dropbox/android/provider/G;->b:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1305
    iget-object v4, p0, Lcom/dropbox/android/provider/G;->c:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1306
    monitor-exit v3

    .line 1308
    :cond_0
    return-void

    .line 1306
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final bridge synthetic a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 1263
    check-cast p1, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/provider/G;->a(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;Lcom/dropbox/android/taskqueue/w;)V

    return-void
.end method

.method protected final synthetic a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)Z
    .locals 1

    .prologue
    .line 1263
    check-cast p1, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/G;->c(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)V
    .locals 1

    .prologue
    .line 1288
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/G;->d(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1289
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 1291
    :cond_0
    return-void
.end method

.method public final bridge synthetic b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V
    .locals 0

    .prologue
    .line 1263
    check-cast p1, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/G;->b(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)V

    return-void
.end method

.method public final synthetic c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V
    .locals 0

    .prologue
    .line 1263
    check-cast p1, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/G;->a(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)V

    return-void
.end method

.method protected final c(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)Z
    .locals 1

    .prologue
    .line 1295
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/G;->a(Lcom/dropbox/android/taskqueue/u;)Z

    move-result v0

    return v0
.end method
