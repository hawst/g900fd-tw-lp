.class public final Lcom/dropbox/android/provider/W;
.super Lcom/dropbox/android/util/bf;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/bf;-><init>(Landroid/database/Cursor;)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/provider/W;->b:[Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/dropbox/android/provider/W;->a:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/provider/W;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getColumnCount()I
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/dropbox/android/util/bf;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 88
    const-string v0, "_cursor_type_tag"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/dropbox/android/provider/W;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 97
    const-string v0, "_cursor_type_tag"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/dropbox/android/provider/W;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 100
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/W;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/provider/W;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/provider/W;->b:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/provider/W;->getColumnCount()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 69
    invoke-super {p0}, Lcom/dropbox/android/util/bf;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-super {p0}, Lcom/dropbox/android/util/bf;->getColumnCount()I

    move-result v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    invoke-virtual {p0}, Lcom/dropbox/android/provider/W;->getColumnCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const-string v2, "_cursor_type_tag"

    aput-object v2, v0, v1

    .line 71
    iput-object v0, p0, Lcom/dropbox/android/provider/W;->b:[Ljava/lang/String;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/provider/W;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/W;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/provider/W;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getType(I)I
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/W;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->getType(I)I

    move-result v0

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method
