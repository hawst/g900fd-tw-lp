.class public final Lcom/dropbox/android/provider/n;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Z

.field private final c:Ldbxyzptlk/db231222/n/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLdbxyzptlk/db231222/n/r;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/dropbox/android/provider/n;->a:Landroid/content/Context;

    .line 28
    iput-boolean p2, p0, Lcom/dropbox/android/provider/n;->b:Z

    .line 29
    iput-object p3, p0, Lcom/dropbox/android/provider/n;->c:Ldbxyzptlk/db231222/n/r;

    .line 30
    return-void
.end method

.method private a(ZLandroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    if-eqz p1, :cond_1

    .line 123
    const-string v0, "local_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/dropbox/android/provider/n;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 125
    iget-boolean v1, p0, Lcom/dropbox/android/provider/n;->b:Z

    if-eqz v1, :cond_0

    .line 126
    invoke-static {v0}, Lcom/dropbox/android/provider/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    :cond_0
    :goto_0
    return-object v0

    .line 130
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/provider/n;->b:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xe

    .line 133
    :goto_1
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 130
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private static a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 108
    const-string v0, "_cursor_type_tag"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 109
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/Z;->c:Lcom/dropbox/android/provider/Z;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/Z;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 113
    const-string v0, "is_dir"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 114
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 118
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Landroid/database/Cursor;)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 58
    invoke-static {p1}, Lcom/dropbox/android/provider/n;->a(Landroid/database/Cursor;)Z

    move-result v4

    .line 59
    invoke-static {p2}, Lcom/dropbox/android/provider/n;->a(Landroid/database/Cursor;)Z

    move-result v5

    .line 60
    if-nez v4, :cond_2

    invoke-static {p1}, Lcom/dropbox/android/provider/n;->b(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v1

    .line 61
    :goto_0
    if-nez v5, :cond_0

    invoke-static {p2}, Lcom/dropbox/android/provider/n;->b(Landroid/database/Cursor;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v1

    .line 62
    :cond_0
    if-eqz v3, :cond_4

    if-nez v2, :cond_4

    .line 63
    iget-object v2, p0, Lcom/dropbox/android/provider/n;->c:Ldbxyzptlk/db231222/n/r;

    sget-object v3, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    if-ne v2, v3, :cond_3

    .line 102
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v3, v2

    .line 60
    goto :goto_0

    :cond_3
    move v0, v1

    .line 66
    goto :goto_1

    .line 68
    :cond_4
    if-eqz v2, :cond_5

    if-nez v3, :cond_5

    .line 69
    iget-object v2, p0, Lcom/dropbox/android/provider/n;->c:Ldbxyzptlk/db231222/n/r;

    sget-object v3, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 70
    goto :goto_1

    .line 74
    :cond_5
    iget-object v2, p0, Lcom/dropbox/android/provider/n;->c:Ldbxyzptlk/db231222/n/r;

    sget-object v6, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    if-eq v2, v6, :cond_a

    .line 76
    if-nez v3, :cond_a

    .line 79
    if-nez v4, :cond_6

    if-eqz v5, :cond_8

    .line 81
    :cond_6
    if-eqz v4, :cond_7

    if-eqz v5, :cond_1

    .line 83
    :cond_7
    if-eqz v5, :cond_a

    if-nez v4, :cond_a

    move v0, v1

    .line 84
    goto :goto_1

    .line 89
    :cond_8
    iget-object v2, p0, Lcom/dropbox/android/provider/n;->c:Ldbxyzptlk/db231222/n/r;

    sget-object v3, Ldbxyzptlk/db231222/n/r;->b:Ldbxyzptlk/db231222/n/r;

    if-ne v2, v3, :cond_a

    .line 90
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/n;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    .line 91
    invoke-direct {p0, p2}, Lcom/dropbox/android/provider/n;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 92
    cmp-long v8, v2, v6

    if-gez v8, :cond_9

    move v0, v1

    .line 93
    goto :goto_1

    .line 94
    :cond_9
    cmp-long v1, v2, v6

    if-gtz v1, :cond_1

    .line 102
    :cond_a
    invoke-direct {p0, v4, p1}, Lcom/dropbox/android/provider/n;->a(ZLandroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v5, p2}, Lcom/dropbox/android/provider/n;->a(ZLandroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16
    check-cast p1, Landroid/database/Cursor;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/provider/n;->a(Landroid/database/Cursor;Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method
