.class public final Lcom/dropbox/android/provider/r;
.super Lcom/dropbox/android/util/bf;
.source "panda.py"


# instance fields
.field protected final a:Landroid/database/Cursor;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Lcom/dropbox/android/util/aY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/util/aY",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Lcom/dropbox/android/util/aY;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/dropbox/android/util/aY",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/bf;-><init>(Landroid/database/Cursor;)V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/provider/r;->d:Lcom/dropbox/android/util/aY;

    .line 28
    iput-object p1, p0, Lcom/dropbox/android/provider/r;->a:Landroid/database/Cursor;

    .line 29
    iput-object p2, p0, Lcom/dropbox/android/provider/r;->d:Lcom/dropbox/android/util/aY;

    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->a()V

    .line 31
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 35
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->a:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 38
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/provider/r;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 39
    add-int/lit8 v0, v0, 0x1

    .line 40
    iget-object v2, p0, Lcom/dropbox/android/provider/r;->d:Lcom/dropbox/android/util/aY;

    iget-object v3, p0, Lcom/dropbox/android/provider/r;->a:Landroid/database/Cursor;

    invoke-interface {v2, v3}, Lcom/dropbox/android/util/aY;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    iget-object v2, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->a:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 46
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->isBeforeFirst()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    iget-object v1, p0, Lcom/dropbox/android/provider/r;->a:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    iget v2, p0, Lcom/dropbox/android/provider/r;->c:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 149
    :cond_0
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getPosition()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/dropbox/android/provider/r;->c:I

    return v0
.end method

.method public final isAfterLast()Z
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lcom/dropbox/android/provider/r;->c:I

    iget-object v1, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isBeforeFirst()Z
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/dropbox/android/provider/r;->c:I

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFirst()Z
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/dropbox/android/provider/r;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLast()Z
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lcom/dropbox/android/provider/r;->c:I

    iget-object v1, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToFirst()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 80
    iget-object v1, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 81
    const/4 v1, -0x1

    iput v1, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 87
    :goto_0
    return v0

    .line 85
    :cond_0
    iput v0, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->b()V

    .line 87
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final moveToLast()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 94
    const/4 v0, 0x0

    .line 99
    :goto_0
    return v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 98
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->b()V

    .line 99
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final moveToNext()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    iget v1, p0, Lcom/dropbox/android/provider/r;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 110
    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->b()V

    .line 115
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final moveToPosition(I)Z
    .locals 1

    .prologue
    .line 120
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 121
    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    return v0

    .line 124
    :cond_1
    iput p1, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 125
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->b()V

    .line 126
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final moveToPrevious()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->isBeforeFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    iget v1, p0, Lcom/dropbox/android/provider/r;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/dropbox/android/provider/r;->c:I

    .line 137
    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->isBeforeFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->b()V

    .line 142
    const/4 v0, 0x1

    goto :goto_0
.end method
