.class public final Lcom/dropbox/android/provider/e;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Lcom/dropbox/android/provider/c;

.field public static final b:Lcom/dropbox/android/provider/c;

.field public static final c:Lcom/dropbox/android/provider/c;

.field public static final d:Lcom/dropbox/android/provider/c;

.field public static final e:Lcom/dropbox/android/provider/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 8
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "album_item"

    const-string v2, "_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    const-string v4, "PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/e;->a:Lcom/dropbox/android/provider/c;

    .line 13
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "album_item"

    const-string v2, "col_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    .line 18
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "album_item"

    const-string v2, "item_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    .line 20
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "album_item"

    const-string v2, "sort_key"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    .line 21
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "album_item"

    const-string v2, "canon_path"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/e;->e:Lcom/dropbox/android/provider/c;

    return-void
.end method

.method public static a()[Lcom/dropbox/android/provider/c;
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/provider/c;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/provider/e;->a:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/provider/e;->e:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    return-object v0
.end method
