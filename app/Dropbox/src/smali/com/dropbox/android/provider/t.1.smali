.class public final Lcom/dropbox/android/provider/t;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/text/DateFormat;


# instance fields
.field private final b:[Lcom/dropbox/android/provider/u;

.field private final c:J

.field private d:J

.field private e:Ljava/util/Calendar;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 45
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/t;->a:Ljava/text/DateFormat;

    .line 48
    sget-object v0, Lcom/dropbox/android/provider/t;->a:Ljava/text/DateFormat;

    new-instance v1, Ljava/util/SimpleTimeZone;

    const/4 v2, 0x0

    const-string v3, "UTC"

    invoke-direct {v1, v2, v3}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p0}, Lcom/dropbox/android/provider/t;->b()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/t;->e:Ljava/util/Calendar;

    .line 62
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/dropbox/android/provider/t;->f:J

    .line 75
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/t;->a(Landroid/content/res/Resources;)[Lcom/dropbox/android/provider/u;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/t;->b:[Lcom/dropbox/android/provider/u;

    .line 77
    invoke-direct {p0}, Lcom/dropbox/android/provider/t;->c()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/provider/t;->c:J

    .line 78
    iget-wide v0, p0, Lcom/dropbox/android/provider/t;->c:J

    iput-wide v0, p0, Lcom/dropbox/android/provider/t;->d:J

    .line 79
    return-void
.end method

.method private static a(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 2

    .prologue
    .line 212
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 213
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 214
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 215
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 216
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 217
    return-object p0
.end method

.method private static a(Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/provider/u;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 192
    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/u;

    iget-wide v0, v0, Lcom/dropbox/android/provider/u;->a:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 198
    :cond_0
    new-instance v0, Lcom/dropbox/android/provider/u;

    invoke-direct {v0, p1, p2, p3}, Lcom/dropbox/android/provider/u;-><init>(JLjava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Landroid/content/res/Resources;)[Lcom/dropbox/android/provider/u;
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x2

    const/4 v6, -0x1

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 156
    invoke-direct {p0}, Lcom/dropbox/android/provider/t;->c()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 159
    invoke-direct {p0}, Lcom/dropbox/android/provider/t;->d()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const v5, 0x7f0d01a1

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/dropbox/android/provider/t;->a(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 162
    invoke-direct {p0}, Lcom/dropbox/android/provider/t;->d()Ljava/util/Calendar;

    move-result-object v3

    .line 163
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v6}, Ljava/util/Calendar;->add(II)V

    .line 164
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 165
    cmp-long v5, v3, v1

    if-ltz v5, :cond_0

    .line 166
    const v5, 0x7f0d01a2

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/dropbox/android/provider/t;->a(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 170
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/t;->d()Ljava/util/Calendar;

    move-result-object v3

    .line 171
    invoke-virtual {v3, v8, v7}, Ljava/util/Calendar;->set(II)V

    .line 172
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 173
    cmp-long v5, v3, v1

    if-ltz v5, :cond_1

    .line 174
    const v5, 0x7f0d01a3

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/dropbox/android/provider/t;->a(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 178
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/provider/t;->d()Ljava/util/Calendar;

    move-result-object v3

    .line 179
    invoke-virtual {v3, v8, v7}, Ljava/util/Calendar;->set(II)V

    .line 180
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v6}, Ljava/util/Calendar;->add(II)V

    .line 181
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 182
    cmp-long v5, v3, v1

    if-ltz v5, :cond_2

    .line 183
    const v5, 0x7f0d01a4

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/dropbox/android/provider/t;->a(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 186
    :cond_2
    const v3, 0x7f0d01a5

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/provider/t;->a(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 188
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/dropbox/android/provider/u;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/provider/u;

    return-object v0
.end method

.method private static b(Ljava/util/Calendar;)V
    .locals 2

    .prologue
    .line 221
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 222
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 223
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 224
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 225
    return-void
.end method

.method private c()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/dropbox/android/provider/t;->b()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/t;->a(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/provider/t;->b()Ljava/util/Calendar;

    move-result-object v0

    .line 207
    invoke-static {v0}, Lcom/dropbox/android/provider/t;->b(Ljava/util/Calendar;)V

    .line 208
    return-object v0
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 95
    const/4 v0, 0x0

    .line 97
    iget-wide v3, p0, Lcom/dropbox/android/provider/t;->c:J

    cmp-long v3, p1, v3

    if-ltz v3, :cond_2

    move v3, v2

    .line 98
    :goto_0
    if-nez v3, :cond_3

    .line 99
    iget-wide v3, p0, Lcom/dropbox/android/provider/t;->d:J

    cmp-long v3, p1, v3

    if-gez v3, :cond_0

    move v1, v2

    .line 100
    :cond_0
    if-eqz v1, :cond_1

    .line 101
    sget-object v1, Lcom/dropbox/android/provider/t;->a:Ljava/text/DateFormat;

    monitor-enter v1

    .line 102
    :try_start_0
    sget-object v0, Lcom/dropbox/android/provider/t;->a:Ljava/text/DateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 103
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    iget-object v1, p0, Lcom/dropbox/android/provider/t;->e:Ljava/util/Calendar;

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 105
    iget-object v1, p0, Lcom/dropbox/android/provider/t;->e:Ljava/util/Calendar;

    invoke-static {v1}, Lcom/dropbox/android/provider/t;->a(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/dropbox/android/provider/t;->d:J

    .line 118
    :cond_1
    :goto_1
    iput-wide p1, p0, Lcom/dropbox/android/provider/t;->f:J

    .line 120
    return-object v0

    :cond_2
    move v3, v1

    .line 97
    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 108
    :cond_3
    const-wide v2, 0x7fffffffffffffffL

    .line 109
    iget-object v4, p0, Lcom/dropbox/android/provider/t;->b:[Lcom/dropbox/android/provider/u;

    array-length v5, v4

    :goto_2
    if-ge v1, v5, :cond_1

    aget-object v6, v4, v1

    .line 110
    cmp-long v7, p1, v2

    if-gez v7, :cond_4

    iget-wide v7, v6, Lcom/dropbox/android/provider/u;->a:J

    cmp-long v7, p1, v7

    if-ltz v7, :cond_4

    iget-wide v7, p0, Lcom/dropbox/android/provider/t;->f:J

    cmp-long v2, v7, v2

    if-ltz v2, :cond_4

    .line 111
    iget-object v0, v6, Lcom/dropbox/android/provider/u;->b:Ljava/lang/String;

    goto :goto_1

    .line 114
    :cond_4
    iget-wide v2, v6, Lcom/dropbox/android/provider/u;->a:J

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method protected final a()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Calendar;
    .locals 8

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/dropbox/android/provider/t;->a()Ljava/util/Calendar;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 141
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    int-to-long v3, v3

    .line 142
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 143
    new-instance v5, Ljava/util/SimpleTimeZone;

    const/4 v6, 0x0

    const-string v7, "UTC"

    invoke-direct {v5, v6, v7}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 144
    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 145
    return-object v0
.end method
