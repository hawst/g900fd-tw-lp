.class public abstract Lcom/dropbox/android/provider/E;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/provider/E;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/E;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/provider/E;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/dropbox/android/provider/E;->b([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/provider/E;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/provider/E;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/MetadataManager;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b([Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/dropbox/android/provider/E;->b:[Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/provider/E;->b:[Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/MetadataManager;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/dropbox/android/provider/E;->a:Ljava/lang/String;

    .line 102
    return-void
.end method

.method protected final a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/dropbox/android/provider/E;->b:[Ljava/lang/String;

    .line 106
    return-void
.end method
