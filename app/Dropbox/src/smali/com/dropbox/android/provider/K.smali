.class final Lcom/dropbox/android/provider/K;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/provider/x;)V
    .locals 0

    .prologue
    .line 1160
    invoke-direct {p0}, Lcom/dropbox/android/provider/K;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1170
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 1171
    iget-wide v3, p0, Lcom/dropbox/android/provider/K;->c:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x493e0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 1173
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/android/provider/K;->a:Ljava/util/ArrayList;

    .line 1175
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/provider/K;->b:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1176
    iget-object v0, p0, Lcom/dropbox/android/provider/K;->a:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1178
    :cond_1
    monitor-exit p0

    return-object v0

    .line 1170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1183
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/dropbox/android/provider/K;->b:Ljava/lang/String;

    .line 1184
    iput-object p2, p0, Lcom/dropbox/android/provider/K;->a:Ljava/util/ArrayList;

    .line 1185
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/provider/K;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1186
    monitor-exit p0

    return-void

    .line 1183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/dropbox/android/util/DropboxPath;)Z
    .locals 3

    .prologue
    .line 1199
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/provider/K;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1200
    iget-object v0, p0, Lcom/dropbox/android/provider/K;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1201
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1202
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;

    invoke-direct {v2, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-virtual {v2, p1}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1203
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204
    const/4 v0, 0x1

    .line 1208
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
