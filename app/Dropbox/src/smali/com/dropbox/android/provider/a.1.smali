.class public abstract Lcom/dropbox/android/provider/a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "panda.py"


# static fields
.field private static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/dropbox/android/provider/a;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    iput-object p2, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/dropbox/android/provider/a;->c:Landroid/content/Context;

    .line 42
    sget-object v1, Lcom/dropbox/android/provider/a;->a:Ljava/util/HashSet;

    monitor-enter v1

    .line 43
    :try_start_0
    sget-object v0, Lcom/dropbox/android/provider/a;->a:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already have a SQLiteOpenHelper for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dropbox/android/util/C;->b(ZLjava/lang/String;)V

    .line 44
    sget-object v0, Lcom/dropbox/android/provider/a;->a:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 45
    monitor-exit v1

    .line 47
    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 136
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/dropbox/android/provider/a;->close()V

    .line 57
    iget-object v0, p0, Lcom/dropbox/android/provider/a;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 58
    return-void
.end method

.method public close()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 65
    sget-object v1, Lcom/dropbox/android/provider/a;->a:Ljava/util/HashSet;

    monitor-enter v1

    .line 66
    :try_start_0
    sget-object v0, Lcom/dropbox/android/provider/a;->a:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    iget-object v2, p0, Lcom/dropbox/android/provider/a;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 67
    monitor-exit v1

    .line 69
    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onConfigure(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onConfigure(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 76
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {p1}, Lcom/dropbox/android/provider/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 81
    return-void

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call. This was added in API 16."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 87
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/dropbox/android/util/by;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-static {p1}, Lcom/dropbox/android/provider/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 91
    :cond_0
    return-void
.end method
