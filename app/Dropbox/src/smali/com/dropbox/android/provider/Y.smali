.class public final Lcom/dropbox/android/provider/Y;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:[Lcom/dropbox/android/provider/Z;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/dropbox/android/provider/Z;->values()[Lcom/dropbox/android/provider/Z;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/Y;->a:[Lcom/dropbox/android/provider/Z;

    return-void
.end method

.method public static a(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_up_folder"

    aput-object v1, v0, v4

    .line 53
    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    .line 54
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 55
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 57
    new-instance v0, Lcom/dropbox/android/provider/W;

    const-string v1, "_up_folder"

    invoke-direct {v0, v2, v1}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 58
    new-instance v1, Landroid/database/MergeCursor;

    new-array v2, v5, [Landroid/database/Cursor;

    aput-object v0, v2, v3

    aput-object p0, v2, v4

    invoke-direct {v1, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 60
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_separator_text"

    aput-object v1, v0, v4

    const-string v1, "_cursor_type_tag"

    aput-object v1, v0, v5

    .line 44
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p0, v1, v4

    const-string v2, "_separator"

    aput-object v2, v1, v5

    .line 45
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 46
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 47
    return-object v2
.end method

.method static synthetic a()[Lcom/dropbox/android/provider/Z;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dropbox/android/provider/Y;->a:[Lcom/dropbox/android/provider/Z;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 69
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_cursor_type_tag"

    aput-object v1, v0, v4

    .line 70
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 71
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    aput-object p0, v0, v4

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 72
    return-object v1
.end method
