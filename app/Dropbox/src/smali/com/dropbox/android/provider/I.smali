.class public final Lcom/dropbox/android/provider/I;
.super Lcom/dropbox/android/provider/E;
.source "panda.py"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127
    invoke-direct {p0}, Lcom/dropbox/android/provider/E;-><init>()V

    .line 129
    const-string v0, "*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-static {p1}, Lcom/dropbox/android/provider/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 132
    const-string v0, "mime_type LIKE ? ESCAPE \'\\\' OR is_dir = 1"

    .line 137
    :goto_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/provider/I;->a(Ljava/lang/String;)V

    .line 138
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v3

    invoke-virtual {p0, v0}, Lcom/dropbox/android/provider/I;->a([Ljava/lang/String;)V

    .line 139
    return-void

    .line 135
    :cond_0
    const-string v0, "mime_type = ? OR is_dir = 1"

    goto :goto_0
.end method
