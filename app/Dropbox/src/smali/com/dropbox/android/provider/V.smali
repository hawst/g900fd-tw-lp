.class public final Lcom/dropbox/android/provider/V;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "dropbox._id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "bytes"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_dir"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_favorite"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "revision"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "local_revision"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "encoding"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "hash"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "thumb_exists"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "_natsort_name"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "modified_millis"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "is_dirty"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "local_hash"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "is_shareable"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "shared_folder_id"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "parent_shared_folder_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 27

    .prologue
    .line 71
    new-instance v2, Lcom/dropbox/android/filemanager/LocalEntry;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/16 v5, 0xc

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    const/4 v8, 0x4

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x5

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x8

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0xd

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x1

    :goto_1
    const/16 v12, 0x9

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0xe

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0xa

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    if-eqz v17, :cond_2

    const/16 v17, 0x1

    :goto_2
    const/16 v18, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-eqz v18, :cond_3

    const/16 v18, 0x1

    :goto_3
    const/16 v19, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const-wide/16 v20, -0x1

    const/16 v22, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    const/16 v24, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    if-eqz v24, :cond_4

    const/16 v24, 0x1

    :goto_4
    const/16 v25, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v2 .. v26}, Lcom/dropbox/android/filemanager/LocalEntry;-><init>(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JJZLjava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_0
    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_2
    const/16 v17, 0x0

    goto :goto_2

    :cond_3
    const/16 v18, 0x0

    goto :goto_3

    :cond_4
    const/16 v24, 0x0

    goto :goto_4
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 102
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/String;

    .line 107
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 108
    aget-object v2, p0, v0

    invoke-static {v2}, Lcom/dropbox/android/provider/V;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_0
    return-object v1
.end method
