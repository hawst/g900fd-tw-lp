.class public final Lcom/dropbox/android/provider/h;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Lcom/dropbox/android/provider/c;

.field public static final b:Lcom/dropbox/android/provider/c;

.field public static final c:Lcom/dropbox/android/provider/c;

.field public static final d:Lcom/dropbox/android/provider/c;

.field public static final e:Lcom/dropbox/android/provider/c;

.field public static final f:Lcom/dropbox/android/provider/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 15
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "photos"

    const-string v2, "_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    const-string v4, "PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/h;->a:Lcom/dropbox/android/provider/c;

    .line 21
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "photos"

    const-string v2, "item_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    const-string v4, "NOT NULL UNIQUE"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/h;->b:Lcom/dropbox/android/provider/c;

    .line 23
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "photos"

    const-string v2, "sort_key"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/h;->c:Lcom/dropbox/android/provider/c;

    .line 25
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "photos"

    const-string v2, "canon_path"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/h;->d:Lcom/dropbox/android/provider/c;

    .line 34
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "photos"

    const-string v2, "time_taken"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/h;->e:Lcom/dropbox/android/provider/c;

    .line 40
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "photos"

    const-string v2, "shared_folder_status"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/h;->f:Lcom/dropbox/android/provider/c;

    return-void
.end method

.method public static a()[Lcom/dropbox/android/provider/c;
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dropbox/android/provider/c;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/provider/h;->a:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/dropbox/android/provider/h;->b:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/provider/h;->c:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/dropbox/android/provider/h;->d:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/provider/h;->e:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/provider/h;->f:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    return-object v0
.end method
