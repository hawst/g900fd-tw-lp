.class public final Lcom/dropbox/android/provider/S;
.super Landroid/database/AbstractCursor;
.source "panda.py"


# instance fields
.field private a:Landroid/database/DataSetObserver;

.field private final b:Landroid/database/Cursor;

.field private final c:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/dropbox/android/provider/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 12
    new-instance v0, Lcom/dropbox/android/provider/T;

    invoke-direct {v0, p0}, Lcom/dropbox/android/provider/T;-><init>(Lcom/dropbox/android/provider/S;)V

    iput-object v0, p0, Lcom/dropbox/android/provider/S;->a:Landroid/database/DataSetObserver;

    .line 28
    iput-object p1, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    .line 32
    new-instance v1, Lcom/dropbox/android/provider/U;

    invoke-direct {v1, p0, p2}, Lcom/dropbox/android/provider/U;-><init>(Lcom/dropbox/android/provider/S;Ljava/util/Comparator;)V

    .line 40
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/dropbox/android/provider/S;->c:[Ljava/lang/Integer;

    .line 41
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/provider/S;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 42
    iget-object v2, p0, Lcom/dropbox/android/provider/S;->c:[Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->c:[Ljava/lang/Integer;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 46
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/provider/S;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 49
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/provider/S;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lcom/dropbox/android/provider/S;->mPos:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/provider/S;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/provider/S;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lcom/dropbox/android/provider/S;->mPos:I

    return p1
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_0
    return-void
.end method

.method public final deactivate()V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "deactivate not supported by Sort, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getBlob(I)[B
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getFloat(I)F
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getLong(I)J
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShort(I)S
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType(I)I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public final isNull(I)Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public final onMove(II)Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 63
    :goto_0
    return v0

    .line 60
    :cond_0
    const/4 v0, -0x1

    if-le p2, v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p2, v0, :cond_2

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    goto :goto_0

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/provider/S;->c:[Ljava/lang/Integer;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 138
    :cond_0
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 152
    :cond_0
    return-void
.end method

.method public final requery()Z
    .locals 2

    .prologue
    .line 163
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requery not supported by MergeSortedCursor, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 145
    :cond_0
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/provider/S;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 159
    :cond_0
    return-void
.end method
