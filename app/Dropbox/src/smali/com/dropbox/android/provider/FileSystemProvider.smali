.class public Lcom/dropbox/android/provider/FileSystemProvider;
.super Landroid/content/ContentProvider;
.source "panda.py"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;

.field private static final c:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "content://com.dropbox.android.LocalFile"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    .line 27
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "internal"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->b:Landroid/net/Uri;

    .line 30
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sdcard"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 288
    return-void
.end method

.method private a(Landroid/content/res/Resources;Ljava/util/List;Landroid/net/Uri;Lcom/dropbox/android/provider/q;ZZ)Landroid/database/Cursor;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/File;",
            ">;>;",
            "Landroid/net/Uri;",
            "Lcom/dropbox/android/provider/q;",
            "ZZ)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v3, Landroid/database/MatrixCursor;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "filename"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "is_dir"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "modified"

    aput-object v2, v0, v1

    invoke-direct {v3, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 264
    const/4 v0, 0x1

    .line 265
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 266
    const/4 v1, 0x7

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x1

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v6, 0x2

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v6, 0x3

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v6, 0x4

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v6, 0x5

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x6

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 275
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 276
    goto :goto_0

    .line 279
    :cond_0
    sget-object v0, Lcom/dropbox/android/provider/q;->d:Lcom/dropbox/android/provider/q;

    if-eq p4, v0, :cond_1

    sget-object v0, Lcom/dropbox/android/provider/q;->e:Lcom/dropbox/android/provider/q;

    if-eq p4, v0, :cond_1

    sget-object v0, Lcom/dropbox/android/provider/q;->a:Lcom/dropbox/android/provider/q;

    if-eq p4, v0, :cond_2

    if-eqz p5, :cond_2

    if-eqz p6, :cond_2

    .line 281
    :cond_1
    invoke-static {p3}, Lcom/dropbox/android/provider/FileSystemProvider;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 282
    invoke-static {v3, v0}, Lcom/dropbox/android/provider/Y;->a(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 284
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method

.method public static a()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 51
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v0

    .line 52
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->b()Z

    move-result v1

    .line 53
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 54
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    .line 60
    :goto_0
    return-object v0

    .line 55
    :cond_0
    if-eqz v0, :cond_1

    .line 56
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->b:Landroid/net/Uri;

    goto :goto_0

    .line 57
    :cond_1
    if-eqz v1, :cond_2

    .line 58
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->c:Landroid/net/Uri;

    goto :goto_0

    .line 60
    :cond_2
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    const v0, 0x7f0d00e8

    const v1, 0x7f0d00e7

    .line 72
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad authority: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->d(Landroid/net/Uri;)Lcom/dropbox/android/provider/q;

    move-result-object v2

    .line 77
    sget-object v3, Lcom/dropbox/android/provider/q;->a:Lcom/dropbox/android/provider/q;

    if-ne v2, v3, :cond_1

    .line 78
    const v0, 0x7f0d00e6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    .line 79
    :cond_1
    sget-object v3, Lcom/dropbox/android/provider/q;->b:Lcom/dropbox/android/provider/q;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/dropbox/android/provider/q;->c:Lcom/dropbox/android/provider/q;

    if-ne v2, v3, :cond_6

    .line 81
    :cond_2
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 82
    sget-object v3, Lcom/dropbox/android/provider/q;->b:Lcom/dropbox/android/provider/q;

    if-ne v2, v3, :cond_3

    .line 88
    :goto_1
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 82
    goto :goto_1

    .line 85
    :cond_4
    sget-object v3, Lcom/dropbox/android/provider/q;->b:Lcom/dropbox/android/provider/q;

    if-ne v2, v3, :cond_5

    :goto_2
    move v0, v1

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_2

    .line 90
    :cond_6
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Lcom/dropbox/android/provider/q;ZZLjava/io/File;Ljava/io/File;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/dropbox/android/provider/q;",
            "ZZ",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 216
    sget-object v1, Lcom/dropbox/android/provider/o;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/provider/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 239
    :cond_0
    :goto_0
    return-object v0

    .line 218
    :pswitch_0
    if-eqz p2, :cond_1

    .line 219
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/dropbox/android/provider/FileSystemProvider;->b:Landroid/net/Uri;

    invoke-direct {v1, v2, p4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_1
    if-eqz p3, :cond_0

    .line 222
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/dropbox/android/provider/FileSystemProvider;->c:Landroid/net/Uri;

    invoke-direct {v1, v2, p5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 227
    :pswitch_1
    if-eqz p2, :cond_0

    .line 228
    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Ljava/util/List;Ljava/io/File;Landroid/net/Uri;)V

    goto :goto_0

    .line 233
    :pswitch_2
    if-eqz p3, :cond_0

    .line 234
    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Ljava/util/List;Ljava/io/File;Landroid/net/Uri;)V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/util/List;Ljava/io/File;Landroid/net/Uri;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/File;",
            ">;>;",
            "Ljava/io/File;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_1

    .line 256
    :cond_0
    return-void

    .line 247
    :cond_1
    invoke-static {p1}, Ldbxyzptlk/db231222/k/a;->b(Ljava/io/File;)[Ljava/io/File;

    move-result-object v1

    .line 248
    new-instance v0, Lcom/dropbox/android/provider/p;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/dropbox/android/provider/p;-><init>(Lcom/dropbox/android/provider/o;)V

    invoke-static {v1, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 249
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 250
    invoke-virtual {v3}, Ljava/io/File;->isHidden()Z

    move-result v4

    if-nez v4, :cond_2

    .line 251
    new-instance v4, Landroid/util/Pair;

    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    .line 101
    sget-object v0, Lcom/dropbox/android/provider/o;->a:[I

    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->d(Landroid/net/Uri;)Lcom/dropbox/android/provider/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid file system uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :pswitch_0
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 113
    :goto_0
    return v0

    .line 109
    :cond_0
    :pswitch_1
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_0

    .line 113
    :cond_1
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Landroid/net/Uri;)Ljava/io/File;
    .locals 4

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 126
    sget-object v1, Lcom/dropbox/android/provider/o;->a:[I

    invoke-static {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->d(Landroid/net/Uri;)Lcom/dropbox/android/provider/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/provider/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 140
    :goto_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 141
    const/16 v2, 0x2f

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 142
    if-lez v2, :cond_0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 143
    :goto_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v2

    .line 128
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get file system path for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :pswitch_1
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 135
    :pswitch_2
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->e()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_0
    const-string v1, ""

    goto :goto_1

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static c(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 148
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 149
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/net/Uri;)Lcom/dropbox/android/provider/q;
    .locals 3

    .prologue
    .line 153
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    sget-object v0, Lcom/dropbox/android/provider/q;->a:Lcom/dropbox/android/provider/q;

    .line 162
    :goto_0
    return-object v0

    .line 155
    :cond_0
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->b:Landroid/net/Uri;

    invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    sget-object v0, Lcom/dropbox/android/provider/q;->b:Lcom/dropbox/android/provider/q;

    goto :goto_0

    .line 157
    :cond_1
    sget-object v0, Lcom/dropbox/android/provider/FileSystemProvider;->c:Landroid/net/Uri;

    invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    sget-object v0, Lcom/dropbox/android/provider/q;->c:Lcom/dropbox/android/provider/q;

    goto :goto_0

    .line 159
    :cond_2
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/FileSystemProvider;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    sget-object v0, Lcom/dropbox/android/provider/q;->d:Lcom/dropbox/android/provider/q;

    goto :goto_0

    .line 161
    :cond_3
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/FileSystemProvider;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 162
    sget-object v0, Lcom/dropbox/android/provider/q;->e:Lcom/dropbox/android/provider/q;

    goto :goto_0

    .line 165
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid file system uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11

    .prologue
    .line 193
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v2

    .line 194
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->b()Z

    move-result v3

    .line 195
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v4

    .line 196
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->e()Ljava/io/File;

    move-result-object v5

    .line 197
    invoke-static {p1}, Lcom/dropbox/android/provider/FileSystemProvider;->d(Landroid/net/Uri;)Lcom/dropbox/android/provider/q;

    move-result-object v1

    move-object v0, p1

    .line 199
    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;Lcom/dropbox/android/provider/q;ZZLjava/io/File;Ljava/io/File;)Ljava/util/List;

    move-result-object v6

    .line 202
    invoke-virtual {p0}, Lcom/dropbox/android/provider/FileSystemProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object v4, p0

    move-object v7, p1

    move-object v8, v1

    move v9, v2

    move v10, v3

    invoke-direct/range {v4 .. v10}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/content/res/Resources;Ljava/util/List;Landroid/net/Uri;Lcom/dropbox/android/provider/q;ZZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method
