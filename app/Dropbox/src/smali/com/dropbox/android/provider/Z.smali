.class public final enum Lcom/dropbox/android/provider/Z;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/provider/Z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/provider/Z;

.field public static final enum b:Lcom/dropbox/android/provider/Z;

.field public static final enum c:Lcom/dropbox/android/provider/Z;

.field public static final enum d:Lcom/dropbox/android/provider/Z;

.field public static final enum e:Lcom/dropbox/android/provider/Z;

.field public static final enum f:Lcom/dropbox/android/provider/Z;

.field public static final enum g:Lcom/dropbox/android/provider/Z;

.field public static final enum h:Lcom/dropbox/android/provider/Z;

.field public static final enum i:Lcom/dropbox/android/provider/Z;

.field public static final enum j:Lcom/dropbox/android/provider/Z;

.field public static final enum k:Lcom/dropbox/android/provider/Z;

.field public static final enum l:Lcom/dropbox/android/provider/Z;

.field public static final enum m:Lcom/dropbox/android/provider/Z;

.field private static final synthetic o:[Lcom/dropbox/android/provider/Z;


# instance fields
.field private final n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 77
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "DROPBOX_ENTRY"

    const-string v2, "DropboxEntry"

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    .line 78
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "UP_FOLDER"

    const-string v2, "_up_folder"

    invoke-direct {v0, v1, v5, v2}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->b:Lcom/dropbox/android/provider/Z;

    .line 79
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "IN_PROGRESS_UPLOAD"

    const-string v2, "_upload_in_progress"

    invoke-direct {v0, v1, v6, v2}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->c:Lcom/dropbox/android/provider/Z;

    .line 80
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "CAMERA_UPLOAD_STATUS"

    const-string v2, "_camera_upload_status_item"

    invoke-direct {v0, v1, v7, v2}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->d:Lcom/dropbox/android/provider/Z;

    .line 81
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "SEPARATOR"

    const-string v2, "_separator"

    invoke-direct {v0, v1, v8, v2}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->e:Lcom/dropbox/android/provider/Z;

    .line 82
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "TURN_ON"

    const/4 v2, 0x5

    const-string v3, "turn_camera_upload_on"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->f:Lcom/dropbox/android/provider/Z;

    .line 83
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "TMP_ALBUM_ITEM"

    const/4 v2, 0x6

    const-string v3, "_temp_album_item"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->g:Lcom/dropbox/android/provider/Z;

    .line 84
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "ALBUM"

    const/4 v2, 0x7

    const-string v3, "_album"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->h:Lcom/dropbox/android/provider/Z;

    .line 85
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "CREATE_NEW_ALBUM_UI"

    const/16 v2, 0x8

    const-string v3, "_create_new_album_ui"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->i:Lcom/dropbox/android/provider/Z;

    .line 86
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "EXPAND_LIGHTWEIGHT_SHARES"

    const/16 v2, 0x9

    const-string v3, "_expand_lightweight_shared"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->j:Lcom/dropbox/android/provider/Z;

    .line 87
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "NEXT_PAGE_ITEM"

    const/16 v2, 0xa

    const-string v3, "_next_page_item"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->k:Lcom/dropbox/android/provider/Z;

    .line 88
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "PREV_PAGE_ITEM"

    const/16 v2, 0xb

    const-string v3, "_prev_page_item"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->l:Lcom/dropbox/android/provider/Z;

    .line 89
    new-instance v0, Lcom/dropbox/android/provider/Z;

    const-string v1, "REMOTE_INSTALL_ON"

    const/16 v2, 0xc

    const-string v3, "_remote_install"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/Z;->m:Lcom/dropbox/android/provider/Z;

    .line 76
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/dropbox/android/provider/Z;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/provider/Z;->b:Lcom/dropbox/android/provider/Z;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/provider/Z;->c:Lcom/dropbox/android/provider/Z;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/provider/Z;->d:Lcom/dropbox/android/provider/Z;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/android/provider/Z;->e:Lcom/dropbox/android/provider/Z;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/provider/Z;->f:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/provider/Z;->g:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/provider/Z;->h:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/provider/Z;->i:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/android/provider/Z;->j:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/android/provider/Z;->k:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dropbox/android/provider/Z;->l:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/android/provider/Z;->m:Lcom/dropbox/android/provider/Z;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/Z;->o:[Lcom/dropbox/android/provider/Z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput-object p3, p0, Lcom/dropbox/android/provider/Z;->n:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public static a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;
    .locals 5

    .prologue
    .line 103
    const-string v0, "_cursor_type_tag"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 104
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 110
    :cond_0
    return-object p1

    .line 107
    :cond_1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-static {}, Lcom/dropbox/android/provider/Y;->a()[Lcom/dropbox/android/provider/Z;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object p1, v2, v0

    .line 109
    iget-object v4, p1, Lcom/dropbox/android/provider/Z;->n:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unrecognized type of tagged cursor entry"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/provider/Z;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/dropbox/android/provider/Z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/Z;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/provider/Z;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/dropbox/android/provider/Z;->o:[Lcom/dropbox/android/provider/Z;

    invoke-virtual {v0}, [Lcom/dropbox/android/provider/Z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/provider/Z;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/dropbox/android/provider/Z;->n:Ljava/lang/String;

    return-object v0
.end method
