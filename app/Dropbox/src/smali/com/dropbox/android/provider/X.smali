.class public final Lcom/dropbox/android/provider/X;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/dropbox/android/provider/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/dropbox/android/provider/X;->a:Landroid/content/Context;

    .line 18
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/dropbox/android/provider/b;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 21
    iget-object v0, p1, Lcom/dropbox/android/provider/b;->a:Landroid/database/Cursor;

    iget v1, p1, Lcom/dropbox/android/provider/b;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected to be able to move to position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/dropbox/android/provider/b;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iget-object v0, p1, Lcom/dropbox/android/provider/b;->a:Landroid/database/Cursor;

    iget-object v1, p1, Lcom/dropbox/android/provider/b;->a:Landroid/database/Cursor;

    const-string v2, "local_uri"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 25
    invoke-static {p0, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/b;Lcom/dropbox/android/provider/b;)I
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/provider/X;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/dropbox/android/provider/X;->a(Landroid/content/Context;Lcom/dropbox/android/provider/b;)Ljava/lang/String;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/dropbox/android/provider/X;->a:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/dropbox/android/provider/X;->a(Landroid/content/Context;Lcom/dropbox/android/provider/b;)Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/dropbox/android/provider/b;

    check-cast p2, Lcom/dropbox/android/provider/b;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/provider/X;->a(Lcom/dropbox/android/provider/b;Lcom/dropbox/android/provider/b;)I

    move-result v0

    return v0
.end method
