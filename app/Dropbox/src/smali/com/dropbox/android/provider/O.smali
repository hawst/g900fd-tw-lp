.class public abstract enum Lcom/dropbox/android/provider/O;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/provider/O;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/provider/O;

.field public static final enum b:Lcom/dropbox/android/provider/O;

.field public static final enum c:Lcom/dropbox/android/provider/O;

.field private static final synthetic i:[Lcom/dropbox/android/provider/O;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 34
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "ACCOUNT_INFO"

    const/4 v2, 0x0

    const-string v3, "account_info"

    const-string v4, "com.dropbox.android.provider.ACCOUNT_INFO_READ"

    const/4 v5, 0x0

    const-string v6, "vnd.android.cursor.item/vnd.dropbox.account_info"

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/O;->a:Lcom/dropbox/android/provider/O;

    .line 73
    new-instance v0, Lcom/dropbox/android/provider/Q;

    const-string v1, "IS_USER_LOGGED_IN"

    const/4 v2, 0x1

    const-string v3, "is_user_logged_in/#"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "vnd.android.cursor.item/vnd.dropbox.is_user_logged_in"

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/provider/Q;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/O;->b:Lcom/dropbox/android/provider/O;

    .line 122
    new-instance v0, Lcom/dropbox/android/provider/R;

    const-string v1, "CAMERA_UPLOAD_SETTINGS"

    const/4 v2, 0x2

    const-string v3, "1/camera_upload_settings"

    const-string v4, "com.dropbox.android.provider.CAMERA_UPLOAD_SETTINGS_READ"

    const-string v5, "com.dropbox.android.provider.CAMERA_UPLOAD_SETTINGS_WRITE"

    const-string v6, "vnd.android.cursor.item/vnd.dropbox.camera_upload_settings"

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/provider/R;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/O;->c:Lcom/dropbox/android/provider/O;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/provider/O;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/provider/O;->a:Lcom/dropbox/android/provider/O;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/dropbox/android/provider/O;->b:Lcom/dropbox/android/provider/O;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/provider/O;->c:Lcom/dropbox/android/provider/O;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/O;->i:[Lcom/dropbox/android/provider/O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 169
    iput-object p3, p0, Lcom/dropbox/android/provider/O;->d:Ljava/lang/String;

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.dropbox.android.provider.SDK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/O;->e:Landroid/net/Uri;

    .line 171
    iput-object p6, p0, Lcom/dropbox/android/provider/O;->f:Ljava/lang/String;

    .line 172
    iput-object p4, p0, Lcom/dropbox/android/provider/O;->g:Ljava/lang/String;

    .line 173
    iput-object p5, p0, Lcom/dropbox/android/provider/O;->h:Ljava/lang/String;

    .line 174
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/N;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct/range {p0 .. p6}, Lcom/dropbox/android/provider/O;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/provider/O;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/dropbox/android/provider/O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/O;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/provider/O;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/dropbox/android/provider/O;->i:[Lcom/dropbox/android/provider/O;

    invoke-virtual {v0}, [Lcom/dropbox/android/provider/O;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/provider/O;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method public abstract a(Ldbxyzptlk/db231222/r/e;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/dropbox/android/provider/O;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/dropbox/android/provider/O;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/dropbox/android/provider/O;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/dropbox/android/provider/O;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/dropbox/android/provider/O;->h:Ljava/lang/String;

    return-object v0
.end method
