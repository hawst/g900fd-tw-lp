.class public Lcom/dropbox/android/provider/ZipperedMediaProvider;
.super Landroid/content/ContentProvider;
.source "panda.py"


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-string v0, "com.dropbox.android.ZipperedMediaProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/ZipperedMediaProvider;->a:Landroid/net/Uri;

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/ZipperedMediaProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/ZipperedMediaProvider;->b:Landroid/net/Uri;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/dropbox/android/provider/ZipperedMediaProvider;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/camera_roll"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/ZipperedMediaProvider;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/net/Uri;ZI)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "ZI)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x3

    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 73
    if-eqz p2, :cond_2

    .line 74
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "video_id"

    aput-object v0, v2, v6

    const-string v0, "kind"

    aput-object v0, v2, v7

    const-string v0, "_data"

    aput-object v0, v2, v8

    .line 79
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/provider/ZipperedMediaProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 82
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 83
    if-eqz v0, :cond_4

    .line 88
    :cond_0
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eq v2, p3, :cond_1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    :cond_1
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 76
    :cond_2
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "image_id"

    aput-object v0, v2, v6

    const-string v0, "kind"

    aput-object v0, v2, v7

    const-string v0, "_data"

    aput-object v0, v2, v8

    goto :goto_0

    .line 93
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_4
    return-object v1
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12

    .prologue
    .line 115
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 118
    const/4 v0, 0x0

    .line 119
    if-eqz p2, :cond_0

    .line 120
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p2, v1

    .line 121
    if-eqz v3, :cond_2

    const-string v4, "mini_thumb_path"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 122
    const/4 v0, 0x1

    .line 128
    :cond_0
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v6, v0

    .line 130
    :goto_1
    invoke-static {}, Lcom/dropbox/android/filemanager/ad;->a()[Lcom/dropbox/android/filemanager/ad;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_2
    if-ge v7, v10, :cond_4

    aget-object v11, v9, v7

    .line 131
    invoke-virtual {p0}, Lcom/dropbox/android/provider/ZipperedMediaProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v11}, Lcom/dropbox/android/filemanager/ad;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "date_added DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_1

    .line 133
    new-instance v1, Lcom/dropbox/android/provider/aa;

    invoke-virtual {v11}, Lcom/dropbox/android/filemanager/ad;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v11}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v3

    invoke-virtual {p0, v2, v3, v6}, Lcom/dropbox/android/provider/ZipperedMediaProvider;->a(Landroid/net/Uri;ZI)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, p0, v11, v0, v2}, Lcom/dropbox/android/provider/aa;-><init>(Lcom/dropbox/android/provider/ZipperedMediaProvider;Lcom/dropbox/android/filemanager/ad;Landroid/database/Cursor;Ljava/util/Map;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    .line 120
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    :cond_3
    const/4 v0, 0x3

    move v6, v0

    goto :goto_1

    .line 138
    :cond_4
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "content_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thumb_path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "vid_duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_cursor_type_tag"

    aput-object v2, v0, v1

    .line 139
    new-instance v4, Landroid/database/MatrixCursor;

    invoke-direct {v4, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 140
    new-instance v6, Landroid/database/MatrixCursor;

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 143
    const/4 v0, 0x0

    .line 144
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/aa;

    .line 145
    iget-object v3, v0, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v1, v3

    .line 146
    iget-object v0, v0, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    goto :goto_3

    .line 167
    :cond_5
    const-wide/16 v2, 0x0

    :goto_4
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v10, v0

    const/4 v2, 0x6

    iget-object v0, v1, Lcom/dropbox/android/provider/aa;->a:Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "_tag_video"

    :goto_5
    aput-object v0, v10, v2

    .line 175
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    .line 177
    invoke-static {v9}, Lcom/dropbox/android/util/av;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 178
    invoke-virtual {v4, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_6
    :goto_6
    move v1, v5

    .line 149
    :cond_7
    add-int/lit8 v5, v1, -0x1

    if-lez v1, :cond_e

    .line 150
    const/4 v1, 0x0

    .line 151
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/aa;

    .line 152
    iget-object v3, v0, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_a

    .line 153
    if-nez v1, :cond_9

    :cond_8
    :goto_8
    move-object v1, v0

    .line 163
    goto :goto_7

    .line 156
    :cond_9
    iget-object v3, v0, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    iget-object v7, v0, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    const-string v9, "date_added"

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 157
    iget-object v7, v1, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    iget-object v9, v1, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    const-string v10, "date_added"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 158
    invoke-virtual {v3, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-gtz v3, :cond_8

    :cond_a
    move-object v0, v1

    goto :goto_8

    .line 165
    :cond_b
    iget-object v7, v1, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    .line 166
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 167
    const/4 v0, 0x7

    new-array v10, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v2, "_id"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v0

    const/4 v0, 0x1

    const-string v2, "_data"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    const/4 v0, 0x2

    const-string v2, "date_modified"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    const/4 v0, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lcom/dropbox/android/provider/aa;->a:Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/ad;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    const/4 v0, 0x4

    iget-object v2, v1, Lcom/dropbox/android/provider/aa;->c:Ljava/util/Map;

    const-string v3, "_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v10, v0

    const/4 v0, 0x5

    iget-object v2, v1, Lcom/dropbox/android/provider/aa;->a:Lcom/dropbox/android/filemanager/ad;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/ad;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "duration"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    goto/16 :goto_4

    :cond_c
    const-string v0, "_tag_photo"

    goto/16 :goto_5

    .line 179
    :cond_d
    invoke-static {v9}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 182
    invoke-virtual {v6, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 187
    :cond_e
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/aa;

    .line 188
    iget-object v0, v0, Lcom/dropbox/android/provider/aa;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_9

    .line 192
    :cond_f
    sget-object v0, Lcom/dropbox/android/provider/ZipperedMediaProvider;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 194
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->close()V

    move-object v0, v4

    .line 212
    :goto_a
    return-object v0

    .line 197
    :cond_10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    if-lez v1, :cond_11

    .line 199
    const-string v1, "_sep_camera_roll"

    invoke-static {v1}, Lcom/dropbox/android/provider/Y;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    :cond_11
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    if-lez v1, :cond_12

    .line 203
    const-string v1, "_sep_other_media"

    invoke-static {v1}, Lcom/dropbox/android/provider/Y;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_12
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/Cursor;

    .line 208
    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    move-object v0, v1

    goto :goto_a
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method
