.class public Lcom/dropbox/android/provider/MetadataManager;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final h:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:[Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/r/d;

.field private final c:Lcom/dropbox/android/provider/j;

.field private final d:Lcom/dropbox/android/filemanager/a;

.field private final e:Landroid/content/ContentResolver;

.field private final f:Lcom/dropbox/android/provider/K;

.field private final g:Lcom/dropbox/android/provider/G;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const-class v0, Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/MetadataManager;->a:Ljava/lang/String;

    .line 391
    new-instance v0, Lcom/dropbox/android/provider/x;

    invoke-direct {v0}, Lcom/dropbox/android/provider/x;-><init>()V

    sput-object v0, Lcom/dropbox/android/provider/MetadataManager;->h:Ljava/util/Comparator;

    .line 415
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "is_favorite"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "is_dirty"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_revision"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_hash"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "encoding"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/MetadataManager;->i:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/a;Landroid/content/ContentResolver;)V
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    new-instance v0, Lcom/dropbox/android/provider/K;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/provider/K;-><init>(Lcom/dropbox/android/provider/x;)V

    iput-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->f:Lcom/dropbox/android/provider/K;

    .line 180
    new-instance v0, Lcom/dropbox/android/provider/G;

    invoke-direct {v0}, Lcom/dropbox/android/provider/G;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->g:Lcom/dropbox/android/provider/G;

    .line 186
    iput-object p1, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    .line 187
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    .line 188
    iput-object p2, p0, Lcom/dropbox/android/provider/MetadataManager;->d:Lcom/dropbox/android/filemanager/a;

    .line 189
    iput-object p3, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    .line 190
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/provider/C;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 942
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 943
    const-string v1, "dropbox"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "hash"

    aput-object v0, v2, v9

    const-string v0, "is_dir"

    aput-object v0, v2, v8

    const-string v3, "canon_path = ?"

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 951
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 952
    const-string v0, "hash"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 953
    const-string v0, "is_dir"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v2, v8

    .line 954
    :goto_0
    new-instance v0, Lcom/dropbox/android/provider/C;

    const/4 v4, 0x1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v8

    :goto_1
    invoke-direct {v0, v4, v2, v1}, Lcom/dropbox/android/provider/C;-><init>(ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 957
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 960
    :goto_2
    return-object v0

    :cond_0
    move v2, v9

    .line 953
    goto :goto_0

    :cond_1
    move v1, v9

    .line 954
    goto :goto_1

    .line 957
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 960
    new-instance v0, Lcom/dropbox/android/provider/C;

    invoke-direct {v0, v9, v9, v9}, Lcom/dropbox/android/provider/C;-><init>(ZZZ)V

    goto :goto_2

    .line 957
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 991
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 993
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    const/16 v2, 0x2710

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    return-object v0

    .line 994
    :catch_0
    move-exception v0

    .line 995
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0

    .line 996
    :catch_1
    move-exception v0

    .line 997
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 998
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0

    .line 999
    :catch_2
    move-exception v0

    .line 1000
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->d:Lcom/dropbox/android/filemanager/a;

    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->b(Ldbxyzptlk/db231222/r/d;)V

    .line 1001
    new-instance v0, Lcom/dropbox/android/provider/z;

    invoke-direct {v0}, Lcom/dropbox/android/provider/z;-><init>()V

    throw v0

    .line 1002
    :catch_3
    move-exception v0

    .line 1003
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x130

    if-ne v1, v2, :cond_0

    .line 1004
    new-instance v0, Lcom/dropbox/android/provider/A;

    invoke-direct {v0, v6}, Lcom/dropbox/android/provider/A;-><init>(Lcom/dropbox/android/provider/x;)V

    throw v0

    .line 1005
    :cond_0
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_1

    .line 1006
    new-instance v0, Lcom/dropbox/android/provider/z;

    invoke-direct {v0}, Lcom/dropbox/android/provider/z;-><init>()V

    throw v0

    .line 1008
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 1009
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0

    .line 1011
    :catch_4
    move-exception v0

    .line 1013
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 1014
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(ZLdbxyzptlk/db231222/n/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1116
    sget-object v0, Ldbxyzptlk/db231222/n/r;->b:Ldbxyzptlk/db231222/n/r;

    if-ne p1, v0, :cond_1

    .line 1117
    if-eqz p0, :cond_0

    .line 1118
    const-string v0, "is_dir ASC, modified_millis DESC, _natsort_name COLLATE NOCASE"

    .line 1127
    :goto_0
    return-object v0

    .line 1120
    :cond_0
    const-string v0, "is_dir ASC, modified_millis DESC, _display_name COLLATE NOCASE"

    goto :goto_0

    .line 1124
    :cond_1
    if-eqz p0, :cond_2

    .line 1125
    const-string v0, "is_dir DESC, _natsort_name COLLATE NOCASE"

    goto :goto_0

    .line 1127
    :cond_2
    const-string v0, "is_dir DESC, _display_name COLLATE NOCASE"

    goto :goto_0
.end method

.method private static a(Landroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 927
    const-string v0, "path"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 929
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    const-string v1, "path"

    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 930
    const-string v1, "canon_path"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    :cond_0
    const-string v0, "parent_path"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 935
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    const-string v1, "parent_path"

    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->i()Ljava/lang/String;

    move-result-object v0

    .line 936
    const-string v1, "canon_parent_path"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    :cond_1
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/dropbox/android/e;->b:Landroid/net/Uri;

    invoke-interface {p1, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1064
    return-void
.end method

.method private a(Landroid/database/Cursor;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    invoke-static {p2}, Lcom/dropbox/android/provider/MetadataManager;->f(Lcom/dropbox/android/util/DropboxPath;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1058
    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 971
    const-string v0, "_natsort_name IS NULL"

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/MetadataManager;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 972
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 973
    const-string v1, "dropbox"

    new-array v2, v8, [Ljava/lang/String;

    const-string v4, "COUNT(*)"

    aput-object v4, v2, v9

    move-object v4, p2

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 982
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    .line 984
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    :cond_0
    move v0, v9

    .line 982
    goto :goto_0

    .line 984
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1137
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 1138
    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Z

    if-eqz v0, :cond_0

    .line 1139
    const/4 v0, 0x1

    .line 1142
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->b([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146
    if-eqz p0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/dropbox/android/d;->b:Landroid/net/Uri;

    invoke-interface {p1, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1070
    return-void
.end method

.method private static b([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1151
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private d(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    .locals 9

    .prologue
    .line 344
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 345
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 349
    :try_start_0
    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/provider/MetadataManager;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/provider/C;

    move-result-object v1

    .line 350
    invoke-virtual {v1}, Lcom/dropbox/android/provider/C;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 351
    const-string v3, "canon_parent_path = ?"

    .line 352
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 356
    if-eqz p3, :cond_0

    .line 357
    invoke-static {p3, v3}, Lcom/dropbox/android/provider/E;->a(Lcom/dropbox/android/provider/E;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 358
    invoke-static {p3, v4}, Lcom/dropbox/android/provider/E;->a(Lcom/dropbox/android/provider/E;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 361
    :cond_0
    invoke-direct {p0, v3, v4}, Lcom/dropbox/android/provider/MetadataManager;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v8

    .line 364
    invoke-static {v8, p2}, Lcom/dropbox/android/provider/MetadataManager;->a(ZLdbxyzptlk/db231222/n/r;)Ljava/lang/String;

    move-result-object v7

    .line 367
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 375
    invoke-direct {p0, v1, p1}, Lcom/dropbox/android/provider/MetadataManager;->a(Landroid/database/Cursor;Lcom/dropbox/android/util/DropboxPath;)V

    .line 377
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 378
    new-instance v2, Lcom/dropbox/android/provider/H;

    invoke-direct {v2, v1, v8}, Lcom/dropbox/android/provider/H;-><init>(Landroid/database/Cursor;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v2

    .line 380
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 381
    new-instance v2, Lcom/dropbox/android/provider/B;

    invoke-direct {v2, v1}, Lcom/dropbox/android/provider/B;-><init>(Lcom/dropbox/android/provider/C;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 675
    invoke-virtual {p0}, Lcom/dropbox/android/provider/MetadataManager;->a()Lcom/dropbox/android/provider/H;

    move-result-object v0

    iget-object v1, v0, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    .line 677
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 678
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 679
    invoke-static {v1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 683
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/dropbox/android/e;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1106
    return-void
.end method

.method private static f(Lcom/dropbox/android/util/DropboxPath;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1133
    invoke-virtual {p0}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/dropbox/android/d;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1110
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/DropboxPath;Landroid/content/ContentValues;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 863
    invoke-static {p2}, Lcom/dropbox/android/provider/MetadataManager;->a(Landroid/content/ContentValues;)V

    .line 865
    const-string v0, "canon_path = ?"

    .line 866
    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 870
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    if-nez v0, :cond_7

    .line 872
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 876
    :try_start_0
    const-string v1, "dropbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_data"

    aput-object v5, v2, v3

    const-string v3, "canon_path = ?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 881
    if-eqz v1, :cond_6

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_6

    move v0, v9

    .line 885
    :goto_0
    if-eqz v1, :cond_0

    .line 886
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 893
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 894
    if-eqz v0, :cond_5

    .line 895
    const-string v0, "path"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 896
    const-string v0, "path"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 898
    :cond_1
    const-string v0, "dropbox"

    const-string v2, "canon_path = ?"

    invoke-virtual {v1, v0, p2, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 900
    if-eq v8, v9, :cond_2

    .line 901
    sget-object v0, Lcom/dropbox/android/provider/MetadataManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows modified in update operation!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    :cond_2
    :goto_2
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V

    .line 923
    :cond_3
    return v8

    .line 885
    :catchall_0
    move-exception v0

    move-object v1, v10

    :goto_3
    if-eqz v1, :cond_4

    .line 886
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 904
    :cond_5
    const-string v0, "path"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 916
    const-string v0, "dropbox"

    invoke-virtual {v1, v0, v10, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 917
    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    move v8, v9

    .line 918
    goto :goto_2

    .line 885
    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_6
    move v0, v8

    goto :goto_0

    :cond_7
    move v0, v8

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21

    .prologue
    .line 449
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 452
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 454
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/provider/MetadataManager;->f:Lcom/dropbox/android/provider/K;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/provider/K;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 455
    if-nez v1, :cond_8

    .line 457
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    const/16 v3, 0x1f4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0, v3, v4}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;IZ)Ljava/util/List;

    move-result-object v1

    .line 458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/provider/MetadataManager;->f:Lcom/dropbox/android/provider/K;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/dropbox/android/provider/K;->a(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    move-object v11, v1

    .line 470
    :goto_1
    invoke-static {v11}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 473
    new-instance v18, Ljava/util/ArrayList;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    move-object/from16 v0, v18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 475
    const-string v1, "canon_path = ?"

    .line 476
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v5, v1

    .line 477
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 478
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 480
    :try_start_1
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ldbxyzptlk/db231222/v/j;

    move-object v10, v0

    .line 481
    const/16 v17, 0x0

    .line 482
    const/16 v16, 0x0

    .line 483
    const/4 v15, 0x0

    .line 484
    const/4 v14, 0x0

    .line 485
    const/4 v13, 0x0

    .line 486
    const/4 v12, 0x0

    .line 491
    const/4 v2, 0x0

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v3, v10}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    .line 492
    const-string v2, "dropbox"

    sget-object v3, Lcom/dropbox/android/provider/MetadataManager;->i:[Ljava/lang/String;

    const-string v4, "canon_path = ?"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v20

    .line 502
    :try_start_2
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 503
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 504
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 505
    const/4 v2, 0x2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 506
    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 507
    const/4 v2, 0x4

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 508
    const/4 v2, 0x5

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    move v9, v8

    move v8, v7

    move-object v7, v6

    move-object v6, v4

    move-object v4, v3

    move-object v3, v2

    .line 511
    :goto_3
    :try_start_3
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 514
    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    array-length v2, v2

    new-array v12, v2, [Ljava/lang/Object;

    .line 515
    const/4 v2, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v2

    .line 516
    const/4 v2, 0x1

    invoke-virtual {v10}, Ldbxyzptlk/db231222/v/j;->a()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v2

    .line 517
    const/4 v2, 0x2

    iget-object v13, v10, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    aput-object v13, v12, v2

    .line 518
    const/4 v2, 0x3

    iget-wide v13, v10, Ldbxyzptlk/db231222/v/j;->a:J

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v12, v2

    .line 519
    const/4 v2, 0x4

    iget-object v13, v10, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    aput-object v13, v12, v2

    .line 520
    const/4 v2, 0x5

    iget-object v13, v10, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    aput-object v13, v12, v2

    .line 521
    const/4 v13, 0x6

    iget-boolean v2, v10, Ldbxyzptlk/db231222/v/j;->d:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v12, v13

    .line 522
    const/4 v2, 0x7

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v12, v2

    .line 523
    const/16 v2, 0x8

    iget-object v9, v10, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    aput-object v9, v12, v2

    .line 524
    const/16 v2, 0x9

    aput-object v7, v12, v2

    .line 525
    const/16 v2, 0xa

    aput-object v6, v12, v2

    .line 526
    const/16 v2, 0xb

    aput-object v3, v12, v2

    .line 527
    const/16 v2, 0xc

    iget-object v3, v10, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    aput-object v3, v12, v2

    .line 528
    const/16 v3, 0xd

    iget-boolean v2, v10, Ldbxyzptlk/db231222/v/j;->l:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v12, v3

    .line 529
    const/16 v2, 0xe

    invoke-virtual {v10}, Ldbxyzptlk/db231222/v/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/provider/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v12, v2

    .line 531
    const/16 v6, 0xf

    iget-object v2, v10, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v10, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    :goto_6
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v12, v6

    .line 533
    const/16 v2, 0x10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v12, v2

    .line 534
    const/16 v2, 0x11

    aput-object v4, v12, v2

    .line 535
    const/16 v3, 0x12

    iget-boolean v2, v10, Ldbxyzptlk/db231222/v/j;->o:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v12, v3

    .line 536
    const/16 v2, 0x13

    iget-object v3, v10, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    aput-object v3, v12, v2

    .line 537
    const/16 v2, 0x14

    iget-object v3, v10, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    aput-object v3, v12, v2

    .line 538
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 542
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 452
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 459
    :catch_0
    move-exception v1

    .line 460
    new-instance v1, Lcom/dropbox/android/provider/J;

    invoke-direct {v1}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v1

    .line 461
    :catch_1
    move-exception v1

    .line 462
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/provider/MetadataManager;->d:Lcom/dropbox/android/filemanager/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/filemanager/a;->b(Ldbxyzptlk/db231222/r/d;)V

    .line 463
    new-instance v1, Lcom/dropbox/android/provider/J;

    invoke-direct {v1}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v1

    .line 464
    :catch_2
    move-exception v1

    .line 466
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 467
    new-instance v1, Lcom/dropbox/android/provider/J;

    invoke-direct {v1}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v1

    .line 511
    :catchall_1
    move-exception v2

    :try_start_4
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    throw v2

    .line 521
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 528
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 531
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_6

    .line 535
    :cond_4
    const/4 v2, 0x0

    goto :goto_7

    .line 540
    :cond_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 542
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 545
    sget-object v1, Lcom/dropbox/android/provider/MetadataManager;->h:Ljava/util/Comparator;

    move-object/from16 v0, v18

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 547
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/provider/V;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v1, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 550
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    .line 551
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_8

    .line 554
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/dropbox/android/provider/MetadataManager;->a(Landroid/database/Cursor;)V

    .line 555
    return-object v2

    :cond_7
    move-object v3, v12

    move-object v4, v13

    move-object v6, v14

    move-object v7, v15

    move/from16 v8, v16

    move/from16 v9, v17

    goto/16 :goto_3

    :cond_8
    move-object v11, v1

    goto/16 :goto_1
.end method

.method public final a()Lcom/dropbox/android/provider/H;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 591
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 592
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 593
    const-string v3, "is_favorite = 1"

    .line 596
    invoke-direct {p0, v3, v4}, Lcom/dropbox/android/provider/MetadataManager;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v8

    .line 597
    sget-object v1, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    invoke-static {v8, v1}, Lcom/dropbox/android/provider/MetadataManager;->a(ZLdbxyzptlk/db231222/n/r;)Ljava/lang/String;

    move-result-object v7

    .line 600
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 608
    invoke-direct {p0, v0}, Lcom/dropbox/android/provider/MetadataManager;->b(Landroid/database/Cursor;)V

    .line 610
    new-instance v1, Lcom/dropbox/android/provider/H;

    invoke-direct {v1, v0, v8}, Lcom/dropbox/android/provider/H;-><init>(Landroid/database/Cursor;Z)V

    return-object v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    .locals 4

    .prologue
    .line 232
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 233
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 234
    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/android/provider/MetadataManager;->b(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;

    move-result-object v0

    .line 236
    if-nez v0, :cond_2

    .line 239
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->d(Lcom/dropbox/android/util/DropboxPath;)V

    .line 241
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/provider/MetadataManager;->d(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    :try_end_0
    .catch Lcom/dropbox/android/provider/B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 258
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 266
    :goto_0
    return-object v0

    .line 242
    :catch_0
    move-exception v0

    .line 250
    iget-object v1, v0, Lcom/dropbox/android/provider/B;->a:Lcom/dropbox/android/provider/C;

    iget-boolean v1, v1, Lcom/dropbox/android/provider/C;->b:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/dropbox/android/provider/B;->a:Lcom/dropbox/android/provider/C;

    iget-boolean v1, v1, Lcom/dropbox/android/provider/C;->a:Z

    if-nez v1, :cond_1

    .line 253
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aV()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    iget-object v0, v0, Lcom/dropbox/android/provider/B;->a:Lcom/dropbox/android/provider/C;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 256
    :cond_1
    new-instance v0, Lcom/dropbox/android/provider/z;

    invoke-direct {v0}, Lcom/dropbox/android/provider/z;-><init>()V

    throw v0

    .line 263
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager;->g:Lcom/dropbox/android/provider/G;

    new-instance v2, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;-><init>(Lcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/android/util/DropboxPath;Z)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/provider/G;->a(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)V

    goto :goto_0
.end method

.method public final a(J)Lcom/dropbox/android/util/DropboxPath;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1024
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 1025
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1027
    const-string v3, "shared_folder_id = ?"

    .line 1028
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 1035
    const-string v7, "modified_millis DESC"

    .line 1036
    const-string v8, "1"

    .line 1038
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1047
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048
    invoke-static {v1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 1051
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1053
    :goto_0
    return-object v5

    .line 1051
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->f:Lcom/dropbox/android/provider/K;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/K;->a(Lcom/dropbox/android/util/DropboxPath;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1081
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->e()V

    .line 1083
    :cond_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V

    .line 1084
    invoke-virtual {p0, p2}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V

    .line 1085
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/v/j;)V
    .locals 4

    .prologue
    .line 753
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 754
    iget-boolean v0, p1, Ldbxyzptlk/db231222/v/j;->d:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 756
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 757
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Z)Ljava/util/List;

    move-result-object v0

    .line 758
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 759
    const/4 v2, 0x0

    iput-object v2, p1, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    .line 760
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 762
    new-instance v2, Lcom/dropbox/android/filemanager/z;

    iget-object v3, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v2, v3}, Lcom/dropbox/android/filemanager/z;-><init>(Ldbxyzptlk/db231222/r/d;)V

    .line 763
    sget-object v3, Lcom/dropbox/android/filemanager/am;->b:Lcom/dropbox/android/filemanager/am;

    invoke-virtual {v2, v0, v1, v3}, Lcom/dropbox/android/filemanager/z;->a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/am;)V

    .line 764
    return-void
.end method

.method public final a(Z)V
    .locals 8

    .prologue
    .line 627
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 629
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->d()Ljava/util/List;

    move-result-object v1

    .line 630
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    :goto_0
    return-void

    .line 634
    :cond_0
    new-instance v2, Lcom/dropbox/android/filemanager/z;

    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v2, v0}, Lcom/dropbox/android/filemanager/z;-><init>(Ldbxyzptlk/db231222/r/d;)V

    .line 636
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 637
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 638
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 655
    :catch_0
    move-exception v0

    .line 656
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->d:Lcom/dropbox/android/filemanager/a;

    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->b(Ldbxyzptlk/db231222/r/d;)V

    .line 657
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0

    .line 640
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v4

    .line 642
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-eq v0, v5, :cond_2

    .line 646
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Expected "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " results, but got "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 651
    :cond_2
    if-eqz p1, :cond_3

    invoke-static {}, Lcom/dropbox/android/filemanager/al;->c()Lcom/dropbox/android/filemanager/am;

    move-result-object v0

    .line 652
    :goto_2
    invoke-virtual {v2, v1, v4, v0}, Lcom/dropbox/android/filemanager/z;->a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/am;)V

    .line 654
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->f()V
    :try_end_1
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1 .. :try_end_1} :catch_2

    .line 668
    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/z;->a()V

    goto/16 :goto_0

    .line 651
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/dropbox/android/filemanager/al;->b()Lcom/dropbox/android/filemanager/am;
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_2

    .line 658
    :catch_1
    move-exception v0

    .line 659
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0

    .line 660
    :catch_2
    move-exception v0

    .line 662
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 663
    new-instance v0, Lcom/dropbox/android/provider/J;

    invoke-direct {v0}, Lcom/dropbox/android/provider/J;-><init>()V

    throw v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 308
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 310
    const-string v1, "dropbox"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "canon_path"

    aput-object v3, v2, v7

    const-string v3, "canon_path = ?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 318
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 320
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Z)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 697
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v2

    invoke-static {v2}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 698
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 699
    const-string v3, "is_favorite"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 701
    iget-object v3, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v3}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 702
    const-string v4, "dropbox"

    const-string v5, "is_dir = 0 AND canon_path = ?"

    new-array v6, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 705
    if-eq v2, v1, :cond_0

    .line 706
    sget-object v3, Lcom/dropbox/android/provider/MetadataManager;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updated "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rows when setting favorite, instead of just 1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_0
    if-ge v2, v1, :cond_1

    .line 713
    :goto_0
    return v0

    .line 711
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->f()V

    .line 712
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V

    move v0, v1

    .line 713
    goto :goto_0
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    .locals 1

    .prologue
    .line 296
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 298
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/provider/MetadataManager;->d(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    :try_end_0
    .catch Lcom/dropbox/android/provider/B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 300
    :goto_0
    return-object v0

    .line 299
    :catch_0
    move-exception v0

    .line 300
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 736
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->g:Lcom/dropbox/android/provider/G;

    new-instance v1, Lcom/dropbox/android/provider/y;

    invoke-direct {v1, p0}, Lcom/dropbox/android/provider/y;-><init>(Lcom/dropbox/android/provider/MetadataManager;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/G;->a(Lcom/dropbox/android/util/aY;)V

    .line 742
    return-void
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->f:Lcom/dropbox/android/provider/K;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/K;->a(Lcom/dropbox/android/util/DropboxPath;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->e()V

    .line 570
    :cond_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V

    .line 571
    return-void
.end method

.method public final b(Ldbxyzptlk/db231222/v/j;)V
    .locals 4

    .prologue
    .line 847
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 848
    invoke-static {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->b(Ldbxyzptlk/db231222/v/j;)Landroid/content/ContentValues;

    move-result-object v0

    .line 849
    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 850
    const-string v2, "dropbox"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 852
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V

    .line 853
    return-void
.end method

.method public final c(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    .locals 1

    .prologue
    .line 335
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/provider/MetadataManager;->d(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;
    :try_end_0
    .catch Lcom/dropbox/android/provider/B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 337
    :goto_0
    return-object v0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 771
    sget-object v0, Lcom/dropbox/android/provider/MetadataManager;->a:Ljava/lang/String;

    const-string v1, "Clearing out metadata."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->g:Lcom/dropbox/android/provider/G;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/G;->a()V

    .line 773
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 774
    const-string v1, "dropbox"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 775
    return-void
.end method

.method public final c(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3

    .prologue
    .line 724
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 725
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->g:Lcom/dropbox/android/provider/G;

    new-instance v1, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;

    const/4 v2, 0x1

    invoke-direct {v1, p0, p1, v2}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;-><init>(Lcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/android/util/DropboxPath;Z)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/G;->a(Lcom/dropbox/android/provider/MetadataManager$MetadataTask;)V

    .line 726
    return-void
.end method

.method public final d(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 787
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 788
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 789
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->c:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 791
    const/4 v2, 0x0

    invoke-static {v0, p1, v2}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Z)Ljava/util/List;

    move-result-object v2

    .line 792
    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/A;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 794
    if-eqz v0, :cond_3

    .line 795
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 799
    invoke-static {v2}, Lcom/dropbox/android/provider/MetadataManager;->a(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 800
    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    .line 804
    :goto_0
    new-instance v3, Lcom/dropbox/android/filemanager/z;

    iget-object v4, p0, Lcom/dropbox/android/provider/MetadataManager;->b:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v3, v4}, Lcom/dropbox/android/filemanager/z;-><init>(Ldbxyzptlk/db231222/r/d;)V

    .line 807
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    .line 809
    iget-boolean v4, v0, Ldbxyzptlk/db231222/v/j;->d:Z

    if-eqz v4, :cond_0

    .line 810
    iget-object v4, v0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    invoke-static {v4}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 813
    :cond_0
    new-instance v4, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v4, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    .line 814
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 815
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " vs "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 817
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    const-string v7, "Result path mismatch"

    invoke-direct {v6, v7}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4, v6}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 821
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 822
    iget-object v5, v0, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 823
    iget-object v5, v0, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 825
    const/4 v5, 0x0

    iput-object v5, v0, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    .line 827
    :cond_2
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 828
    invoke-static {}, Lcom/dropbox/android/filemanager/al;->a()Lcom/dropbox/android/filemanager/am;

    move-result-object v0

    invoke-virtual {v3, v2, v4, v0}, Lcom/dropbox/android/filemanager/z;->a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/am;)V

    .line 830
    invoke-virtual {p0, p1}, Lcom/dropbox/android/provider/MetadataManager;->e(Lcom/dropbox/android/util/DropboxPath;)V
    :try_end_0
    .catch Lcom/dropbox/android/provider/A; {:try_start_0 .. :try_end_0} :catch_0

    .line 837
    :goto_1
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/z;->a()V

    .line 838
    return-void

    .line 831
    :catch_0
    move-exception v0

    .line 833
    sget-object v0, Lcom/dropbox/android/filemanager/am;->a:Lcom/dropbox/android/filemanager/am;

    invoke-virtual {v3, v2, v1, v0}, Lcom/dropbox/android/filemanager/z;->a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/am;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method final e(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1094
    invoke-static {p1}, Lcom/dropbox/android/provider/MetadataManager;->f(Lcom/dropbox/android/util/DropboxPath;)Landroid/net/Uri;

    move-result-object v0

    .line 1095
    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1099
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->f()V

    .line 1100
    invoke-direct {p0}, Lcom/dropbox/android/provider/MetadataManager;->e()V

    .line 1101
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/dropbox/android/provider/PhotosProvider;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1102
    return-void
.end method
