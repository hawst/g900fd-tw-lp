.class Lcom/dropbox/android/provider/MetadataManager$MetadataTask;
.super Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/provider/MetadataManager;

.field private final b:Lcom/dropbox/android/util/DropboxPath;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/android/util/DropboxPath;Z)V
    .locals 0

    .prologue
    .line 1218
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;-><init>()V

    .line 1219
    iput-object p1, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->a:Lcom/dropbox/android/provider/MetadataManager;

    .line 1220
    iput-object p2, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->b:Lcom/dropbox/android/util/DropboxPath;

    .line 1221
    iput-boolean p3, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->c:Z

    .line 1222
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "metadata-update:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->b:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Ljava/util/List;
    .locals 1

    .prologue
    .line 1212
    invoke-virtual {p0}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->j_()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 2

    .prologue
    .line 1230
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 1233
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->a:Lcom/dropbox/android/provider/MetadataManager;

    iget-object v1, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->b:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/MetadataManager;->d(Lcom/dropbox/android/util/DropboxPath;)V
    :try_end_0
    .catch Lcom/dropbox/android/provider/z; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/dropbox/android/provider/J; {:try_start_0 .. :try_end_0} :catch_0

    .line 1240
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1236
    :catch_0
    move-exception v0

    .line 1237
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_1

    .line 1234
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1225
    iget-boolean v0, p0, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->c:Z

    return v0
.end method

.method public final j_()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1255
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1250
    invoke-virtual {p0}, Lcom/dropbox/android/provider/MetadataManager$MetadataTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
