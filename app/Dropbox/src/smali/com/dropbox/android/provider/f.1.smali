.class public final Lcom/dropbox/android/provider/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Lcom/dropbox/android/provider/c;

.field public static final b:Lcom/dropbox/android/provider/c;

.field public static final c:Lcom/dropbox/android/provider/c;

.field public static final d:Lcom/dropbox/android/provider/c;

.field public static final e:Lcom/dropbox/android/provider/c;

.field public static final f:Lcom/dropbox/android/provider/c;

.field public static final g:Lcom/dropbox/android/provider/c;

.field public static final h:Lcom/dropbox/android/provider/c;

.field public static final i:Lcom/dropbox/android/provider/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 11
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    const-string v4, "PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->a:Lcom/dropbox/android/provider/c;

    .line 16
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "col_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    const-string v4, "NOT NULL UNIQUE"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    .line 17
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "name"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->c:Lcom/dropbox/android/provider/c;

    .line 18
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "count"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->d:Lcom/dropbox/android/provider/c;

    .line 20
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "cover_image_canon_path"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->e:Lcom/dropbox/android/provider/c;

    .line 22
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "share_link"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->f:Lcom/dropbox/android/provider/c;

    .line 28
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "creation_time"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->g:Lcom/dropbox/android/provider/c;

    .line 33
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "update_time"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->h:Lcom/dropbox/android/provider/c;

    .line 35
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "albums"

    const-string v2, "is_lightweight"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    return-void
.end method

.method public static a()[Lcom/dropbox/android/provider/c;
    .locals 3

    .prologue
    .line 39
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/dropbox/android/provider/c;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/provider/f;->a:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/provider/f;->c:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/dropbox/android/provider/f;->d:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/provider/f;->e:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/provider/f;->f:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/provider/f;->g:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/provider/f;->h:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    aput-object v2, v0, v1

    return-object v0
.end method
