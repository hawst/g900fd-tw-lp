.class final Lcom/dropbox/android/provider/x;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<[",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;[Ljava/lang/Object;)I
    .locals 8

    .prologue
    const/16 v5, 0xf

    const/16 v7, 0xe

    const/4 v4, 0x6

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 395
    aget-object v0, p1, v4

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 396
    aget-object v0, p2, v4

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 397
    if-eq v3, v0, :cond_1

    .line 398
    if-nez v3, :cond_0

    move v0, v1

    .line 411
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 398
    goto :goto_0

    .line 402
    :cond_1
    aget-object v0, p1, v5

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 403
    aget-object v0, p2, v5

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 404
    cmp-long v0, v3, v5

    if-eqz v0, :cond_3

    .line 405
    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 409
    :cond_3
    aget-object v0, p1, v7

    check-cast v0, Ljava/lang/String;

    .line 410
    aget-object v1, p2, v7

    check-cast v1, Ljava/lang/String;

    .line 411
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 391
    check-cast p1, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/provider/x;->a([Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
