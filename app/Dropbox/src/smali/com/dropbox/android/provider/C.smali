.class final Lcom/dropbox/android/provider/C;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/analytics/m;


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>(ZZZ)V
    .locals 0

    .prologue
    .line 1343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1344
    iput-boolean p1, p0, Lcom/dropbox/android/provider/C;->a:Z

    .line 1345
    iput-boolean p2, p0, Lcom/dropbox/android/provider/C;->b:Z

    .line 1346
    iput-boolean p3, p0, Lcom/dropbox/android/provider/C;->c:Z

    .line 1347
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/analytics/l;)V
    .locals 3

    .prologue
    .line 1372
    const-string v0, "isDir"

    iget-boolean v1, p0, Lcom/dropbox/android/provider/C;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "exists"

    iget-boolean v2, p0, Lcom/dropbox/android/provider/C;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "hasHash"

    iget-boolean v2, p0, Lcom/dropbox/android/provider/C;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 1373
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1367
    iget-boolean v0, p0, Lcom/dropbox/android/provider/C;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/provider/C;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/provider/C;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
