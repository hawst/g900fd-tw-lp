.class public final Lcom/dropbox/android/provider/v;
.super Landroid/database/AbstractCursor;
.source "panda.py"


# instance fields
.field private final a:Landroid/database/DataSetObserver;

.field private final b:I

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/database/Cursor;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:[Landroid/database/Cursor;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;Ljava/util/Comparator;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/database/Cursor;",
            "Ljava/util/Comparator",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 39
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 23
    new-instance v0, Lcom/dropbox/android/provider/w;

    invoke-direct {v0, p0}, Lcom/dropbox/android/provider/w;-><init>(Lcom/dropbox/android/provider/v;)V

    iput-object v0, p0, Lcom/dropbox/android/provider/v;->a:Landroid/database/DataSetObserver;

    .line 40
    iput-object p1, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    .line 44
    new-instance v7, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    array-length v1, p1

    move v0, v4

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 46
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 47
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v2, v3

    move v1, v4

    .line 53
    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 55
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v5, v3

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 56
    if-eqz v5, :cond_2

    invoke-interface {p2, v5, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    if-lez v8, :cond_b

    :cond_2
    :goto_3
    move-object v5, v0

    .line 59
    goto :goto_2

    .line 60
    :cond_3
    if-nez v5, :cond_4

    .line 61
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Canidate should never be null.  PC LOAD LETTER!?!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_4
    if-eqz p3, :cond_a

    .line 66
    invoke-interface {v5, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 67
    if-eqz v2, :cond_9

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 68
    const/4 v0, 0x1

    move-object v2, v6

    .line 72
    :goto_4
    if-nez v0, :cond_c

    .line 73
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    new-instance v6, Landroid/util/Pair;

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v6, v5, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    add-int/lit8 v0, v1, 0x1

    .line 76
    :goto_5
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    .line 77
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 78
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_5
    move v1, v0

    .line 80
    goto :goto_1

    .line 82
    :cond_6
    iput v1, p0, Lcom/dropbox/android/provider/v;->b:I

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v1, v0

    :goto_6
    if-ge v4, v1, :cond_8

    aget-object v2, v0, v4

    .line 85
    if-nez v2, :cond_7

    .line 84
    :goto_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 87
    :cond_7
    iget-object v3, p0, Lcom/dropbox/android/provider/v;->a:Landroid/database/DataSetObserver;

    invoke-interface {v2, v3}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_7

    .line 89
    :cond_8
    return-void

    :cond_9
    move v0, v4

    move-object v2, v6

    goto :goto_4

    :cond_a
    move v0, v4

    goto :goto_4

    :cond_b
    move-object v0, v5

    goto :goto_3

    :cond_c
    move v0, v1

    goto :goto_5
.end method

.method static synthetic a(Lcom/dropbox/android/provider/v;I)I
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/provider/v;I)I
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    return p1
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 186
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 187
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-nez v2, :cond_0

    .line 186
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 192
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 193
    return-void
.end method

.method public final deactivate()V
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "deactivate not supported by MergeSortedCursor, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getBlob(I)[B
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 163
    iget v0, p0, Lcom/dropbox/android/provider/v;->mPos:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/provider/v;->mPos:I

    iget v2, p0, Lcom/dropbox/android/provider/v;->b:I

    if-ge v0, v2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    .line 168
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 169
    if-eqz v4, :cond_1

    .line 170
    invoke-interface {v4}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 174
    :cond_2
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/dropbox/android/provider/v;->b:I

    return v0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getFloat(I)F
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getLong(I)J
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShort(I)S
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType(I)I
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public final isNull(I)Z
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/v;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public final onMove(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 100
    const/4 v1, -0x1

    if-ge p2, v1, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    iget v1, p0, Lcom/dropbox/android/provider/v;->b:I

    if-gt p2, v1, :cond_0

    .line 105
    if-ltz p2, :cond_2

    iget v0, p0, Lcom/dropbox/android/provider/v;->b:I

    if-ge p2, v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 110
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/database/Cursor;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 112
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 198
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 199
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 200
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 198
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_1
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 217
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 218
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 219
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 217
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222
    :cond_1
    return-void
.end method

.method public final requery()Z
    .locals 2

    .prologue
    .line 236
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requery not supported by MergeSortedCursor, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 207
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 208
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 209
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 207
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 212
    :cond_1
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 227
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 228
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, p0, Lcom/dropbox/android/provider/v;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 227
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_1
    return-void
.end method
