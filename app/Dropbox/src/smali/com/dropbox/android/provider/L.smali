.class public final Lcom/dropbox/android/provider/L;
.super Lcom/dropbox/android/util/bf;
.source "panda.py"


# instance fields
.field private final a:Landroid/database/ContentObservable;

.field private b:Landroid/database/ContentObservable;

.field private c:Landroid/database/ContentObserver;

.field private final d:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/bf;-><init>(Landroid/database/Cursor;)V

    .line 14
    new-instance v0, Landroid/database/ContentObservable;

    invoke-direct {v0}, Landroid/database/ContentObservable;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/L;->a:Landroid/database/ContentObservable;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/provider/L;->b:Landroid/database/ContentObservable;

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/L;->d:Ljava/lang/Object;

    .line 18
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 55
    iget-object v1, p0, Lcom/dropbox/android/provider/L;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->a:Landroid/database/ContentObservable;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 57
    monitor-exit v1

    .line 58
    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/database/ContentObservable;)V
    .locals 3

    .prologue
    .line 61
    iget-object v1, p0, Lcom/dropbox/android/provider/L;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->b:Landroid/database/ContentObservable;

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "We don\'t support resetting notifcation sourcees yet"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 65
    :cond_0
    :try_start_1
    new-instance v0, Lcom/dropbox/android/provider/M;

    invoke-direct {v0, p0}, Lcom/dropbox/android/provider/M;-><init>(Lcom/dropbox/android/provider/L;)V

    iput-object v0, p0, Lcom/dropbox/android/provider/L;->c:Landroid/database/ContentObserver;

    .line 66
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->c:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0}, Landroid/database/ContentObservable;->registerObserver(Landroid/database/ContentObserver;)V

    .line 67
    iput-object p1, p0, Lcom/dropbox/android/provider/L;->b:Landroid/database/ContentObservable;

    .line 68
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    return-void
.end method

.method protected final a(Z)V
    .locals 2

    .prologue
    .line 48
    iget-object v1, p0, Lcom/dropbox/android/provider/L;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->a:Landroid/database/ContentObservable;

    invoke-static {v0, p1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 50
    monitor-exit v1

    .line 51
    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->b:Landroid/database/ContentObservable;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->b:Landroid/database/ContentObservable;

    iget-object v1, p0, Lcom/dropbox/android/provider/L;->c:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/provider/L;->b:Landroid/database/ContentObservable;

    .line 40
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/util/bf;->close()V

    .line 41
    return-void
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->a:Landroid/database/ContentObservable;

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->registerObserver(Landroid/database/ContentObserver;)V

    .line 23
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 24
    return-void
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 29
    iget-object v0, p0, Lcom/dropbox/android/provider/L;->a:Landroid/database/ContentObservable;

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 30
    return-void
.end method
