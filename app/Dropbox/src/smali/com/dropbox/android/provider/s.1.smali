.class public Lcom/dropbox/android/provider/s;
.super Landroid/database/AbstractCursor;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:Landroid/database/Cursor;

.field private d:Landroid/database/Cursor;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/database/Cursor;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-class v0, Lcom/dropbox/android/provider/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/s;->a:Ljava/lang/String;

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_separator_text"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/s;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 228
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 37
    iput-boolean v7, p0, Lcom/dropbox/android/provider/s;->f:Z

    .line 229
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/dropbox/android/provider/s;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 231
    new-instance v1, Lcom/dropbox/android/provider/W;

    const-string v2, "DropboxEntry"

    invoke-direct {v1, p2, v2}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    .line 232
    new-instance v1, Lcom/dropbox/android/provider/W;

    const-string v2, "_separator"

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/provider/s;->d:Landroid/database/Cursor;

    .line 233
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/List;

    .line 235
    sget-object v1, Lcom/dropbox/android/provider/h;->e:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 237
    new-instance v2, Lcom/dropbox/android/provider/t;

    invoke-direct {v2, p1}, Lcom/dropbox/android/provider/t;-><init>(Landroid/content/res/Resources;)V

    .line 238
    invoke-interface {p2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 239
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 241
    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/provider/t;->a(J)Ljava/lang/String;

    move-result-object v3

    .line 242
    if-eqz v3, :cond_0

    .line 244
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 245
    iget-object v3, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/List;

    iget-object v4, p0, Lcom/dropbox/android/provider/s;->d:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/dropbox/android/provider/s;->d:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    :cond_0
    iget-object v3, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/List;

    iget-object v4, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 249
    :cond_1
    return-void
.end method

.method private a()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 341
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/List;

    iget v1, p0, Lcom/dropbox/android/provider/s;->mPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 342
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/database/Cursor;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 343
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method private b()Lcom/dropbox/android/provider/W;
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/List;

    iget v1, p0, Lcom/dropbox/android/provider/s;->mPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    .line 358
    instance-of v1, v0, Lcom/dropbox/android/provider/W;

    if-eqz v1, :cond_0

    .line 359
    check-cast v0, Lcom/dropbox/android/provider/W;

    .line 361
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 439
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/provider/s;->f:Z

    .line 440
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 441
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 442
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 384
    return-void
.end method

.method public deactivate()V
    .locals 2

    .prologue
    .line 429
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBlob(I)[B
    .locals 1

    .prologue
    .line 348
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->b()Lcom/dropbox/android/provider/W;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {v0}, Lcom/dropbox/android/provider/W;->getColumnCount()I

    move-result v0

    .line 333
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    goto :goto_0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->b()Lcom/dropbox/android/provider/W;

    move-result-object v0

    .line 263
    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/W;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 269
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->b()Lcom/dropbox/android/provider/W;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/W;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 285
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->b()Lcom/dropbox/android/provider/W;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_0

    .line 299
    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/W;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 301
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->b()Lcom/dropbox/android/provider/W;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {v0}, Lcom/dropbox/android/provider/W;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 317
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 414
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 485
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFloat(I)F
    .locals 1

    .prologue
    .line 409
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 2

    .prologue
    .line 393
    if-nez p1, :cond_0

    .line 394
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Asked for _id as int instead of long."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 401
    if-nez p1, :cond_0

    .line 402
    iget v0, p0, Lcom/dropbox/android/provider/s;->mPos:I

    int-to-long v0, v0

    .line 404
    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 1

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 366
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->b()Lcom/dropbox/android/provider/W;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 373
    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/W;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/W;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 378
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/provider/s;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 2

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/dropbox/android/provider/s;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/provider/s;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "HeaderWrappedCursor is not closed, but one of its wrapped cursors is"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/provider/s;->f:Z

    return v0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 455
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 465
    return-void
.end method

.method public requery()Z
    .locals 2

    .prologue
    .line 434
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 490
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 475
    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 460
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 470
    return-void
.end method
