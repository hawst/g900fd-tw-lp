.class public Lcom/dropbox/android/widget/DropboxItemListView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# static fields
.field public static final a:Ldbxyzptlk/db231222/v/n;

.field private static final c:Ljava/lang/String;


# instance fields
.field protected b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field private d:Lcom/dropbox/android/taskqueue/D;

.field private e:Lcom/dropbox/android/widget/ao;

.field private f:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/DropboxItemListView;->c:Ljava/lang/String;

    .line 36
    sget-object v0, Ldbxyzptlk/db231222/v/n;->c:Ldbxyzptlk/db231222/v/n;

    sput-object v0, Lcom/dropbox/android/widget/DropboxItemListView;->a:Ldbxyzptlk/db231222/v/n;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/content/Context;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ao;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 121
    sget-object v0, Lcom/dropbox/android/widget/DropboxItemListView;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to get item at position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but length is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ao;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 129
    :goto_0
    return-object v0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/ao;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 126
    instance-of v2, v0, Landroid/database/Cursor;

    if-eqz v2, :cond_1

    .line 127
    check-cast v0, Landroid/database/Cursor;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 129
    goto :goto_0
.end method

.method public final a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/ao;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 82
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002c

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 84
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DropboxItemListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    .line 85
    return-void
.end method

.method public final a(ZLandroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/p;Lcom/dropbox/android/widget/quickactions/f;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V
    .locals 6

    .prologue
    .line 96
    invoke-virtual {p6}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v5

    .line 97
    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->d:Lcom/dropbox/android/taskqueue/D;

    .line 100
    if-eqz p1, :cond_0

    .line 101
    new-instance v0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-direct {v0, p2}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;-><init>(Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0, p3}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/p;)V

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0, p4}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/f;)V

    .line 107
    :cond_0
    new-instance v0, Lcom/dropbox/android/widget/ao;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    move-object v2, p2

    move-object v3, p5

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/ao;-><init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/I;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    .line 108
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 109
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c()V

    .line 75
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    return v0
.end method

.method public final d()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public setItemClickListener(Lcom/dropbox/android/widget/am;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/widget/ak;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/ak;-><init>(Lcom/dropbox/android/widget/DropboxItemListView;Lcom/dropbox/android/widget/am;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/widget/al;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/al;-><init>(Lcom/dropbox/android/widget/DropboxItemListView;Lcom/dropbox/android/widget/am;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 158
    return-void
.end method

.method public setListViewScrollState(II)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 175
    if-ne v0, p1, :cond_0

    .line 176
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0, p2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 177
    return-void

    .line 175
    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 135
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 171
    return-void
.end method
