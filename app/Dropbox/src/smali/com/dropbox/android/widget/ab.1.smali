.class public final Lcom/dropbox/android/widget/ab;
.super Landroid/app/AlertDialog;
.source "panda.py"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ProgressBar;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 49
    iput-object p2, p0, Lcom/dropbox/android/widget/ab;->h:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 108
    iget-object v2, p0, Lcom/dropbox/android/widget/ab;->c:Landroid/widget/ProgressBar;

    if-gtz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 109
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ab;->f:Z

    if-eqz v0, :cond_2

    .line 110
    if-lez p1, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/ab;->g:Z

    if-nez v0, :cond_0

    .line 113
    iput-boolean v1, p0, Lcom/dropbox/android/widget/ab;->g:Z

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/widget/ab;->c:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/dropbox/android/widget/ab;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ab;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 120
    :goto_1
    return-void

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_2
    iput p1, p0, Lcom/dropbox/android/widget/ab;->e:I

    goto :goto_1
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ab;->g:Z

    .line 124
    iput p1, p0, Lcom/dropbox/android/widget/ab;->d:I

    .line 125
    return-void
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 130
    :try_start_0
    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 54
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/ab;->requestWindowFeature(I)Z

    .line 55
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ab;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/ab;->setCancelable(Z)V

    .line 61
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/ab;->setCanceledOnTouchOutside(Z)V

    .line 63
    const v1, 0x7f03002b

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 64
    const v0, 0x7f070098

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/ab;->c:Landroid/widget/ProgressBar;

    .line 65
    const v0, 0x7f070096

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/ab;->a:Landroid/widget/ImageView;

    .line 66
    const v0, 0x7f070097

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/ab;->b:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f070099

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 69
    new-instance v2, Lcom/dropbox/android/widget/ac;

    invoke-direct {v2, p0}, Lcom/dropbox/android/widget/ac;-><init>(Lcom/dropbox/android/widget/ab;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/ab;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/ab;->setView(Landroid/view/View;)V

    .line 78
    iget v0, p0, Lcom/dropbox/android/widget/ab;->e:I

    if-lez v0, :cond_0

    .line 79
    iget v0, p0, Lcom/dropbox/android/widget/ab;->e:I

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ab;->a(I)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ab;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/ab;->h:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/an;->a(Ljava/lang/String;)I

    move-result v0

    .line 85
    if-eqz v0, :cond_1

    .line 86
    iget-object v1, p0, Lcom/dropbox/android/widget/ab;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/ab;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/dropbox/android/widget/ab;->h:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 93
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/AlertDialog;->onStart()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ab;->f:Z

    .line 99
    return-void
.end method

.method protected final onStop()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Landroid/app/AlertDialog;->onStop()V

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ab;->f:Z

    .line 105
    return-void
.end method
