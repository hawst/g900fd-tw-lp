.class final Lcom/dropbox/android/widget/qr/g;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/widget/qr/d;

.field private final b:Lcom/dropbox/android/widget/qr/j;

.field private final c:Ldbxyzptlk/db231222/M/a;

.field private d:Z


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/qr/d;Lcom/dropbox/android/widget/qr/j;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/g;->d:Z

    .line 48
    new-instance v0, Ldbxyzptlk/db231222/M/a;

    invoke-direct {v0}, Ldbxyzptlk/db231222/M/a;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/g;->c:Ldbxyzptlk/db231222/M/a;

    .line 49
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/g;->a:Lcom/dropbox/android/widget/qr/d;

    .line 50
    iput-object p2, p0, Lcom/dropbox/android/widget/qr/g;->b:Lcom/dropbox/android/widget/qr/j;

    .line 51
    return-void
.end method

.method private a([BII)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/g;->a:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/widget/qr/d;->a([BII)Ldbxyzptlk/db231222/I/i;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_1

    .line 85
    new-instance v2, Ldbxyzptlk/db231222/I/c;

    new-instance v3, Ldbxyzptlk/db231222/J/i;

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/J/i;-><init>(Ldbxyzptlk/db231222/I/g;)V

    invoke-direct {v2, v3}, Ldbxyzptlk/db231222/I/c;-><init>(Ldbxyzptlk/db231222/I/b;)V

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/g;->c:Ldbxyzptlk/db231222/M/a;

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/M/a;->a(Ldbxyzptlk/db231222/I/c;)Ldbxyzptlk/db231222/I/k;
    :try_end_0
    .catch Ldbxyzptlk/db231222/I/j; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 91
    iget-object v2, p0, Lcom/dropbox/android/widget/qr/g;->c:Ldbxyzptlk/db231222/M/a;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/M/a;->a()V

    .line 96
    :goto_0
    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/g;->b:Lcom/dropbox/android/widget/qr/j;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 105
    :goto_1
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 91
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/g;->c:Ldbxyzptlk/db231222/M/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/M/a;->a()V

    move-object v0, v1

    .line 92
    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/g;->c:Ldbxyzptlk/db231222/M/a;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/M/a;->a()V

    throw v0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/g;->b:Lcom/dropbox/android/widget/qr/j;

    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/g;->d:Z

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 58
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown DecodeHandler message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/g;->d:Z

    .line 61
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/android/widget/qr/g;->a([BII)V

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
