.class public final enum Lcom/dropbox/android/widget/aF;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/widget/aF;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/aF;

.field public static final enum b:Lcom/dropbox/android/widget/aF;

.field private static final synthetic c:[Lcom/dropbox/android/widget/aF;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 211
    new-instance v0, Lcom/dropbox/android/widget/aF;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/aF;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/aF;->a:Lcom/dropbox/android/widget/aF;

    .line 212
    new-instance v0, Lcom/dropbox/android/widget/aF;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/aF;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/aF;->b:Lcom/dropbox/android/widget/aF;

    .line 210
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/widget/aF;

    sget-object v1, Lcom/dropbox/android/widget/aF;->a:Lcom/dropbox/android/widget/aF;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/widget/aF;->b:Lcom/dropbox/android/widget/aF;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/widget/aF;->c:[Lcom/dropbox/android/widget/aF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/aF;
    .locals 1

    .prologue
    .line 210
    const-class v0, Lcom/dropbox/android/widget/aF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/aF;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/aF;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/dropbox/android/widget/aF;->c:[Lcom/dropbox/android/widget/aF;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/aF;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/aF;

    return-object v0
.end method
