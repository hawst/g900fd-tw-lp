.class final Lcom/dropbox/android/widget/bw;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Lcom/dropbox/android/widget/bv;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/bv;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/dropbox/android/widget/bw;->b:Lcom/dropbox/android/widget/bv;

    iput-object p2, p0, Lcom/dropbox/android/widget/bw;->a:Landroid/widget/ListView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 86
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/widget/bw;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bw;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    if-gt v1, v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/bw;->b:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 90
    iget-object v2, p0, Lcom/dropbox/android/widget/bw;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    .line 91
    iget-object v2, p0, Lcom/dropbox/android/widget/bw;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070176

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 93
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->g()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    :cond_0
    return-void
.end method
