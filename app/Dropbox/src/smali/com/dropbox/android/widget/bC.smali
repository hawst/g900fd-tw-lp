.class public abstract Lcom/dropbox/android/widget/bC;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field protected final c:Landroid/content/Context;

.field protected d:Landroid/database/Cursor;

.field protected e:Lcom/dropbox/android/widget/bF;

.field protected final f:Lcom/dropbox/android/widget/aG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/dropbox/android/widget/aG;

    invoke-direct {v0}, Lcom/dropbox/android/widget/aG;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bC;->f:Lcom/dropbox/android/widget/aG;

    .line 49
    iput-object p1, p0, Lcom/dropbox/android/widget/bC;->c:Landroid/content/Context;

    .line 50
    return-void
.end method

.method private j()Landroid/view/View;
    .locals 4

    .prologue
    .line 234
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->f()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 235
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->e()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 237
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    return-object v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 417
    const/4 v0, -0x1

    return v0
.end method

.method public a(ILandroid/view/View;Lcom/dropbox/android/widget/SweetListView;IZZZ)Landroid/view/View;
    .locals 9

    .prologue
    .line 267
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 269
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/bC;->b(Landroid/database/Cursor;)I

    move-result v4

    .line 270
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/bC;->e(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 273
    if-eqz p2, :cond_2

    move-object v1, p2

    .line 284
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/bC;->c(Landroid/database/Cursor;)Z

    move-result v2

    move-object v0, v1

    .line 287
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v2, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 288
    if-eqz v2, :cond_0

    move-object v0, v1

    .line 289
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->d()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    move-object v0, v1

    .line 291
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v2, :cond_4

    if-nez p7, :cond_4

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 292
    if-eqz v2, :cond_1

    if-nez p7, :cond_1

    move-object v0, v1

    .line 293
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 297
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->h()Lcom/dropbox/android/widget/bF;

    move-result-object v3

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v5, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v3, v1, v0, v5}, Lcom/dropbox/android/widget/bF;->a(Landroid/view/View;Landroid/view/View;Landroid/database/Cursor;)V

    .line 299
    iget-object v3, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v3, v0, v4}, Lcom/dropbox/android/widget/bC;->a(Landroid/database/Cursor;Landroid/view/View;I)V

    .line 300
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 364
    :goto_3
    return-object v1

    .line 276
    :cond_2
    new-instance v1, Lcom/dropbox/android/widget/ViewButton;

    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->c:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/dropbox/android/widget/ViewButton;-><init>(Landroid/content/Context;)V

    .line 277
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/ViewButton;->setOrientation(I)V

    .line 278
    invoke-direct {p0}, Lcom/dropbox/android/widget/bC;->j()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/ViewButton;->addView(Landroid/view/View;)V

    .line 279
    invoke-virtual {p0, v1, v4}, Lcom/dropbox/android/widget/bC;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 280
    invoke-direct {p0}, Lcom/dropbox/android/widget/bC;->j()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/ViewButton;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 287
    :cond_3
    const/16 v0, 0x8

    goto/16 :goto_1

    .line 291
    :cond_4
    const/16 v0, 0x8

    goto :goto_2

    .line 304
    :cond_5
    if-eqz p2, :cond_7

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, p4, :cond_7

    move-object v0, p2

    .line 333
    :cond_6
    const/4 v3, 0x0

    .line 334
    check-cast v0, Landroid/view/ViewGroup;

    .line 336
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v5

    if-eqz p5, :cond_8

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v1

    :goto_4
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v6

    if-eqz p6, :cond_9

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v2

    :goto_5
    invoke-virtual {v0, v5, v1, v6, v2}, Landroid/view/ViewGroup;->setPadding(IIII)V

    move v2, v3

    .line 342
    :goto_6
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_a

    if-ge v2, p4, :cond_a

    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/bC;->e(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 343
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 344
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/ViewGroup;

    .line 345
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 346
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 347
    iget-object v6, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v6, v5, v4}, Lcom/dropbox/android/widget/bC;->a(Landroid/database/Cursor;Landroid/view/View;I)V

    .line 348
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->h()Lcom/dropbox/android/widget/bF;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v6, v1, v5, v7}, Lcom/dropbox/android/widget/bF;->a(Landroid/view/View;Landroid/view/View;Landroid/database/Cursor;)V

    .line 349
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 350
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 351
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 352
    goto :goto_6

    .line 313
    :cond_7
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 314
    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 315
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 317
    const/4 v1, 0x0

    :goto_7
    if-ge v1, p4, :cond_6

    .line 318
    new-instance v2, Lcom/dropbox/android/widget/ViewButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/bC;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/dropbox/android/widget/ViewButton;-><init>(Landroid/content/Context;)V

    .line 319
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/dropbox/android/widget/ViewButton;->setOrientation(I)V

    .line 320
    invoke-virtual {p0, v2, v4}, Lcom/dropbox/android/widget/bC;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 321
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v3, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 324
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v5

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v6

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v7

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bC;->i()I

    move-result v8

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 327
    invoke-virtual {v2, v3}, Lcom/dropbox/android/widget/ViewButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 328
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 317
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 336
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 358
    :cond_a
    :goto_8
    if-ge v2, p4, :cond_b

    .line 359
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/ViewGroup;

    .line 360
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 361
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    add-int/lit8 v2, v2, 0x1

    .line 363
    goto :goto_8

    :cond_b
    move-object v1, v0

    .line 364
    goto/16 :goto_3
.end method

.method public abstract a(Landroid/view/ViewGroup;I)Landroid/view/View;
.end method

.method public final a(ILandroid/view/View;)V
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 379
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 380
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/bC;->b(Landroid/database/Cursor;)I

    move-result v2

    invoke-virtual {p0, v1, p2, v2}, Lcom/dropbox/android/widget/bC;->a(Landroid/database/Cursor;Landroid/view/View;I)V

    .line 381
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 382
    return-void
.end method

.method public abstract a(Landroid/database/Cursor;Landroid/view/View;I)V
.end method

.method final a(Lcom/dropbox/android/widget/bF;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/dropbox/android/widget/bC;->e:Lcom/dropbox/android/widget/bF;

    .line 82
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->f:Lcom/dropbox/android/widget/aG;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/aG;->c(Ljava/lang/String;)V

    .line 402
    return-void
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method public abstract b(Landroid/database/Cursor;)I
.end method

.method public abstract b()Lcom/dropbox/android/widget/bG;
.end method

.method public abstract b(I)Z
.end method

.method public abstract c()I
.end method

.method public c(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, -0x1

    return v0
.end method

.method public d(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    .line 101
    iput-object p1, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    .line 102
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->e:Lcom/dropbox/android/widget/bF;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/dropbox/android/widget/bC;->e:Lcom/dropbox/android/widget/bF;

    invoke-interface {v1, p1}, Lcom/dropbox/android/widget/bF;->a(Landroid/database/Cursor;)V

    .line 105
    :cond_0
    return-object v0
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bC;->b(Landroid/database/Cursor;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/bC;->d(I)Z

    move-result v0

    return v0
.end method

.method public final f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->c:Landroid/content/Context;

    return-object v0
.end method

.method public final g()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->d:Landroid/database/Cursor;

    return-object v0
.end method

.method protected final h()Lcom/dropbox/android/widget/bF;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/dropbox/android/widget/bC;->e:Lcom/dropbox/android/widget/bF;

    return-object v0
.end method

.method public final i()I
    .locals 3

    .prologue
    .line 397
    const/4 v0, 0x2

    const/high16 v1, 0x3fc00000    # 1.5f

    iget-object v2, p0, Lcom/dropbox/android/widget/bC;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
