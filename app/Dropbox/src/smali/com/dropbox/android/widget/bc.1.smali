.class final Lcom/dropbox/android/widget/bc;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/dropbox/android/filemanager/ai;

.field final synthetic c:Lcom/dropbox/android/widget/bb;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/bb;ILcom/dropbox/android/filemanager/ai;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iput p2, p0, Lcom/dropbox/android/widget/bc;->a:I

    iput-object p3, p0, Lcom/dropbox/android/widget/bc;->b:Lcom/dropbox/android/filemanager/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 154
    iget v0, p0, Lcom/dropbox/android/widget/bc;->a:I

    iget-object v1, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v1, v1, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v1}, Lcom/dropbox/android/widget/ba;->a(Lcom/dropbox/android/widget/ba;)I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v0, v0, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v0}, Lcom/dropbox/android/widget/ba;->b(Lcom/dropbox/android/widget/ba;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->b:Lcom/dropbox/android/filemanager/ai;

    if-eqz v0, :cond_1

    .line 155
    new-instance v8, Lcom/dropbox/android/util/n;

    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->b:Lcom/dropbox/android/filemanager/ai;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-direct {v8, v0}, Lcom/dropbox/android/util/n;-><init>(Landroid/graphics/Bitmap;)V

    .line 156
    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v0, v0, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v0, v8}, Lcom/dropbox/android/widget/ba;->a(Lcom/dropbox/android/widget/ba;Lcom/dropbox/android/util/n;)Lcom/dropbox/android/util/n;

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v0, v0, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v0}, Lcom/dropbox/android/widget/ba;->c(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v0, v0, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v0}, Lcom/dropbox/android/widget/ba;->d(Lcom/dropbox/android/widget/ba;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v2, v2, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v2}, Lcom/dropbox/android/widget/ba;->d(Lcom/dropbox/android/widget/ba;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v8}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v3, v3, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v3}, Lcom/dropbox/android/widget/ba;->e(Lcom/dropbox/android/widget/ba;)Z

    move-result v3

    iget-object v4, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v4, v4, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v4}, Lcom/dropbox/android/widget/ba;->f(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v5, v5, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v5}, Lcom/dropbox/android/widget/ba;->g(Lcom/dropbox/android/widget/ba;)Landroid/widget/FrameLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v6, v6, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v6}, Lcom/dropbox/android/widget/ba;->h(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v7, v7, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v7}, Lcom/dropbox/android/widget/ba;->i(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v0, v0, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v0}, Lcom/dropbox/android/widget/ba;->j(Lcom/dropbox/android/widget/ba;)Lcom/dropbox/android/util/o;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bc;->c:Lcom/dropbox/android/widget/bb;

    iget-object v1, v1, Lcom/dropbox/android/widget/bb;->a:Lcom/dropbox/android/widget/ba;

    invoke-static {v1}, Lcom/dropbox/android/widget/ba;->b(Lcom/dropbox/android/widget/ba;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Lcom/dropbox/android/util/o;->a(Ljava/lang/String;Lcom/dropbox/android/util/n;)V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->b:Lcom/dropbox/android/filemanager/ai;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/widget/bc;->b:Lcom/dropbox/android/filemanager/ai;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method
