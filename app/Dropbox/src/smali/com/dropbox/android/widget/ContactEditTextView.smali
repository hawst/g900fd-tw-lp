.class public Lcom/dropbox/android/widget/ContactEditTextView;
.super Lcom/android/ex/chips/RecipientEditTextView;
.source "panda.py"


# instance fields
.field private c:Lcom/dropbox/android/service/A;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x2

    const/4 v4, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/RecipientEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-boolean v4, p0, Lcom/dropbox/android/widget/ContactEditTextView;->d:Z

    .line 41
    new-instance v0, Landroid/text/util/Rfc822Tokenizer;

    invoke-direct {v0}, Landroid/text/util/Rfc822Tokenizer;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 42
    new-instance v0, Lcom/dropbox/android/widget/D;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/D;-><init>(Lcom/dropbox/android/widget/C;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    .line 43
    invoke-virtual {p0, v6}, Lcom/dropbox/android/widget/ContactEditTextView;->setThreshold(I)V

    .line 44
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ContactEditTextView;->getInputType()I

    move-result v0

    or-int/lit8 v0, v0, 0x20

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setInputType(I)V

    .line 45
    new-instance v0, Lcom/dropbox/android/service/A;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/service/A;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/ContactEditTextView;->c:Lcom/dropbox/android/service/A;

    .line 46
    new-instance v0, Lcom/dropbox/android/widget/E;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ContactEditTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/ContactEditTextView;->c:Lcom/dropbox/android/service/A;

    invoke-static {}, Lcom/dropbox/android/widget/ContactEditTextView;->u()[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/widget/E;-><init>(Landroid/content/Context;Lcom/dropbox/android/service/A;[Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    iput-boolean v6, p0, Lcom/dropbox/android/widget/ContactEditTextView;->d:Z

    .line 58
    const-string v0, "bar"

    invoke-static {v0, v4}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/dropbox/android/widget/ContactEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/widget/ContactEditTextView;->measure(II)V

    .line 61
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ContactEditTextView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setMinHeight(I)V

    .line 62
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iput-boolean v4, p0, Lcom/dropbox/android/widget/ContactEditTextView;->d:Z

    .line 64
    return-void
.end method

.method public static u()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 86
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "photo_thumb_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "display_name_source"

    aput-object v2, v0, v1

    .line 96
    return-object v0
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/android/ex/chips/RecipientEditTextView;->onDetachedFromWindow()V

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/widget/ContactEditTextView;->c:Lcom/dropbox/android/service/A;

    invoke-virtual {v0}, Lcom/dropbox/android/service/A;->close()V

    .line 70
    return-void
.end method
