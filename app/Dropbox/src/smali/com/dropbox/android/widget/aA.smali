.class final Lcom/dropbox/android/widget/aA;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/util/Pair;

.field final synthetic b:Lcom/dropbox/android/util/DropboxPath;

.field final synthetic c:Lcom/dropbox/android/widget/ay;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/ay;Landroid/util/Pair;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 0

    .prologue
    .line 1833
    iput-object p1, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    iput-object p2, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iput-object p3, p0, Lcom/dropbox/android/widget/aA;->b:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/taskqueue/O;

    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/O;->a:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    localEntry:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 1837
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 1839
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;Z)Z

    .line 1843
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->b:Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 1844
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->p(Lcom/dropbox/android/widget/ay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845
    invoke-static {}, Lcom/dropbox/android/widget/GalleryView;->k()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCacheChange setting bmp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1846
    iget-object v1, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v1, v0}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;Landroid/graphics/Bitmap;)V

    .line 1851
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    iget-object v0, v0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->l(Lcom/dropbox/android/widget/GalleryView;)V

    .line 1852
    return-void

    .line 1848
    :cond_0
    invoke-static {}, Lcom/dropbox/android/widget/GalleryView;->k()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCacheChange recycling bmp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/aA;->c:Lcom/dropbox/android/widget/ay;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849
    iget-object v0, p0, Lcom/dropbox/android/widget/aA;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method
