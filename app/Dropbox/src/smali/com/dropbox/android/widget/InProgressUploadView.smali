.class public Lcom/dropbox/android/widget/InProgressUploadView;
.super Lcom/dropbox/android/widget/QuickActionView;
.source "panda.py"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CheckBox;

.field private d:Lcom/dropbox/android/widget/ba;

.field private e:Landroid/widget/ProgressBar;

.field private f:Ldbxyzptlk/db231222/j/g;

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/dropbox/android/widget/aS;

.field private final i:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/util/o;Lcom/dropbox/android/filemanager/I;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/QuickActionView;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->g:Landroid/os/Handler;

    .line 39
    new-instance v0, Lcom/dropbox/android/widget/aS;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/aS;-><init>(Lcom/dropbox/android/widget/InProgressUploadView;Lcom/dropbox/android/widget/aQ;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/aS;

    .line 44
    const v0, 0x7f030050

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/InProgressUploadView;->addView(Landroid/view/View;)V

    .line 47
    const v0, 0x7f0700d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->a:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0700d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->c:Landroid/widget/CheckBox;

    .line 49
    const v0, 0x7f0700d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0700d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    .line 52
    const v0, 0x7f0700d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 53
    const v0, 0x7f0700da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 54
    const v0, 0x7f0700d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 55
    const v0, 0x7f07005e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 56
    const v0, 0x7f0700db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 58
    new-instance v0, Lcom/dropbox/android/widget/ba;

    iget-object v3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->g:Landroid/os/Handler;

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lcom/dropbox/android/widget/ba;-><init>(Lcom/dropbox/android/util/o;Landroid/content/Context;Landroid/os/Handler;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/FrameLayout;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->d:Lcom/dropbox/android/widget/ba;

    .line 67
    iput-object p3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->i:Lcom/dropbox/android/filemanager/I;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/InProgressUploadView;)Ldbxyzptlk/db231222/j/g;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/InProgressUploadView;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 139
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/InProgressUploadView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/InProgressUploadView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->g:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Z)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    iget-object v3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/aS;

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    .line 78
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->b()V

    .line 80
    const-string v0, "id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 81
    new-instance v0, Ldbxyzptlk/db231222/j/l;

    const-string v5, "id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ldbxyzptlk/db231222/j/l;-><init>(J)V

    .line 82
    iget-object v5, p0, Lcom/dropbox/android/widget/InProgressUploadView;->i:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v5

    invoke-virtual {v5, v0}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    iget-object v5, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/aS;

    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/j/g;->a(Ldbxyzptlk/db231222/j/h;)V

    .line 87
    :cond_1
    const-string v0, "local_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-nez v0, :cond_4

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    instance-of v0, v0, Ldbxyzptlk/db231222/j/n;

    if-eqz v0, :cond_4

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    check-cast v0, Ldbxyzptlk/db231222/j/n;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/n;->e()F

    move-result v0

    const/4 v6, 0x0

    cmpg-float v0, v0, v6

    if-gez v0, :cond_4

    .line 97
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    const v6, 0x7f0d0077

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    .line 106
    :goto_0
    if-eqz v0, :cond_2

    .line 107
    invoke-virtual {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    iget-object v6, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    iget-object v7, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    invoke-static {v0, v1, v2, v6, v7}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Ldbxyzptlk/db231222/j/g;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;)V

    .line 111
    :cond_2
    const-string v0, "local_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    const-string v1, "mime_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 115
    iget-object v2, p0, Lcom/dropbox/android/widget/InProgressUploadView;->d:Lcom/dropbox/android/widget/ba;

    invoke-virtual {v2, v0, v5, v1}, Lcom/dropbox/android/widget/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->c:Landroid/widget/CheckBox;

    new-instance v1, Lcom/dropbox/android/widget/aR;

    iget-object v2, p0, Lcom/dropbox/android/widget/InProgressUploadView;->i:Lcom/dropbox/android/filemanager/I;

    invoke-direct {v1, v3, v4, v2}, Lcom/dropbox/android/widget/aR;-><init>(JLcom/dropbox/android/filemanager/I;)V

    invoke-virtual {p0, p2, v0, p3, v1}, Lcom/dropbox/android/widget/InProgressUploadView;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/widget/CheckBox;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/bd;)V

    .line 120
    if-eqz p4, :cond_3

    .line 121
    invoke-virtual {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->a()V

    .line 123
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0}, Lcom/dropbox/android/widget/QuickActionView;->onDetachedFromWindow()V

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->d:Lcom/dropbox/android/widget/ba;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->d:Lcom/dropbox/android/widget/ba;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ba;->a()V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/aS;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/db231222/j/g;

    .line 135
    :cond_1
    return-void
.end method
