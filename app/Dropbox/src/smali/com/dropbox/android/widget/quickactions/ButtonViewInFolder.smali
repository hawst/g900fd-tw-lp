.class public Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private d:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 15
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 19
    const v0, 0x7f03008a

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.BROWSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 31
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 33
    const-string v1, "EXTRA_FILE_SCROLL_TO"

    iget-object v2, p0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 34
    const-string v1, "EXTRA_DONT_CLEAR_FLAGS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 35
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 36
    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 37
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f0d01eb

    return v0
.end method
