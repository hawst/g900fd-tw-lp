.class public Lcom/dropbox/android/widget/CameraUploadItemView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field protected a:Z

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ProgressBar;

.field private d:Landroid/widget/FrameLayout;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Ldbxyzptlk/db231222/j/g;

.field private final k:Ldbxyzptlk/db231222/j/i;

.field private l:I

.field private final m:Lcom/dropbox/android/widget/A;

.field private n:Landroid/os/Handler;

.field private o:Lcom/dropbox/android/widget/y;

.field private final p:Landroid/os/Handler;

.field private final q:Lcom/dropbox/android/widget/bY;

.field private final r:Lcom/dropbox/android/widget/bZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/widget/bY;Ldbxyzptlk/db231222/j/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:I

    .line 51
    new-instance v0, Lcom/dropbox/android/widget/A;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/A;-><init>(Lcom/dropbox/android/widget/CameraUploadItemView;Lcom/dropbox/android/widget/v;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->m:Lcom/dropbox/android/widget/A;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->n:Landroid/os/Handler;

    .line 53
    new-instance v0, Lcom/dropbox/android/widget/y;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/y;-><init>(Lcom/dropbox/android/widget/CameraUploadItemView;Lcom/dropbox/android/widget/v;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->o:Lcom/dropbox/android/widget/y;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->p:Landroid/os/Handler;

    .line 58
    new-instance v0, Lcom/dropbox/android/widget/v;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/v;-><init>(Lcom/dropbox/android/widget/CameraUploadItemView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->r:Lcom/dropbox/android/widget/bZ;

    .line 75
    const v0, 0x7f03004e

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 76
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CameraUploadItemView;->addView(Landroid/view/View;)V

    .line 79
    new-instance v0, Lcom/dropbox/android/widget/w;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/w;-><init>(Lcom/dropbox/android/widget/CameraUploadItemView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 88
    const v0, 0x7f0700d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    .line 89
    const v0, 0x7f0700d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    .line 90
    const v0, 0x7f0700da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    .line 91
    const v0, 0x7f07005e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    .line 92
    const v0, 0x7f0700d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->h:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0700dd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0700d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0700d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    .line 97
    iput-object p2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->q:Lcom/dropbox/android/widget/bY;

    .line 98
    iput-object p3, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:Ldbxyzptlk/db231222/j/i;

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/CameraUploadItemView;)Lcom/dropbox/android/widget/bY;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->q:Lcom/dropbox/android/widget/bY;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->m:Lcom/dropbox/android/widget/A;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 108
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    const v1, 0x7f020293

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 114
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 140
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3eb33333    # 0.35f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 142
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 143
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 144
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 145
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->setEnabled(Z)V

    .line 148
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/CameraUploadItemView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->b()V

    return-void
.end method

.method private b(Lcom/dropbox/android/widget/u;)V
    .locals 11

    .prologue
    const/16 v10, 0xff

    const/4 v0, 0x0

    const/16 v4, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 174
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v1

    .line 175
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->c()Ljava/lang/String;

    move-result-object v3

    .line 176
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    .line 177
    if-eqz v3, :cond_4

    .line 179
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 183
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->a()Ljava/lang/String;

    move-result-object v2

    .line 184
    iget v4, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:I

    const/4 v5, 0x3

    invoke-virtual {v1, v2, v4, v5, v0}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v2

    .line 186
    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    .line 187
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    move v2, v8

    .line 198
    :goto_0
    iget v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:I

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->o:Lcom/dropbox/android/widget/y;

    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_1

    .line 201
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 202
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v3, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    iget-object v4, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 217
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->h:Landroid/widget/TextView;

    const v1, 0x7f0d0180

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 219
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->f()Lcom/dropbox/android/taskqueue/ag;

    move-result-object v0

    .line 222
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->g()Z

    move-result v1

    if-nez v1, :cond_2

    .line 223
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->e()I

    move-result v1

    .line 224
    if-lez v1, :cond_2

    .line 225
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0015

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 228
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    :cond_2
    sget-object v1, Lcom/dropbox/android/widget/x;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/ag;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 277
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected camera upload tracker state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_3
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    move v2, v9

    goto :goto_0

    .line 207
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    const v1, 0x7f0200e7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 238
    :pswitch_0
    const v0, 0x7f0d0077

    move v1, v0

    move v0, v9

    .line 280
    :goto_2
    if-eqz v8, :cond_5

    .line 281
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 282
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 285
    :cond_5
    if-eqz v0, :cond_9

    .line 287
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080092

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 288
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    const/16 v1, 0xb3

    invoke-static {v1, v10, v10, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 292
    :goto_3
    return-void

    .line 241
    :pswitch_1
    const v0, 0x7f0d018d

    move v1, v0

    move v0, v9

    .line 242
    goto :goto_2

    .line 244
    :pswitch_2
    const v0, 0x7f0d018c

    move v1, v0

    move v0, v9

    .line 245
    goto :goto_2

    .line 247
    :pswitch_3
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/i;->a(Landroid/content/Context;)Lcom/dropbox/android/util/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/m;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 248
    const v0, 0x7f0d0191

    move v1, v0

    move v0, v9

    goto :goto_2

    .line 250
    :cond_6
    const v0, 0x7f0d0190

    move v1, v0

    move v0, v9

    .line 252
    goto :goto_2

    .line 254
    :pswitch_4
    const v0, 0x7f0d018b

    move v1, v0

    move v0, v8

    .line 256
    goto :goto_2

    .line 258
    :pswitch_5
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->h()Ldbxyzptlk/db231222/j/l;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    .line 260
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_7

    .line 261
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->m:Lcom/dropbox/android/widget/A;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->a(Ldbxyzptlk/db231222/j/h;)V

    .line 263
    :cond_7
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-static {v0, v1, v8, v2, v3}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Ldbxyzptlk/db231222/j/g;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;)V

    move v8, v9

    move v0, v9

    move v1, v9

    .line 266
    goto :goto_2

    .line 268
    :pswitch_6
    const v0, 0x7f0d0074

    move v1, v0

    move v0, v9

    .line 269
    goto/16 :goto_2

    .line 271
    :pswitch_7
    invoke-interface {p1}, Lcom/dropbox/android/widget/u;->g()Z

    move-result v0

    if-nez v0, :cond_8

    .line 272
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "we should be initial scan, as non-initial state is supressed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_8
    const v0, 0x7f0d018f

    move v1, v0

    move v0, v9

    .line 275
    goto/16 :goto_2

    .line 290
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080091

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 151
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3eb33333    # 0.35f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 153
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 154
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 155
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 156
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 157
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/CameraUploadItemView;->setEnabled(Z)V

    .line 159
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/CameraUploadItemView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->c()V

    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/widget/CameraUploadItemView;)Ldbxyzptlk/db231222/j/g;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->p:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/CameraUploadItemView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:I

    return v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic l(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic m(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->n:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/widget/u;)V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    .line 164
    invoke-direct {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->a()V

    .line 167
    iget v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:I

    .line 169
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/CameraUploadItemView;->b(Lcom/dropbox/android/widget/u;)V

    .line 170
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 119
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->q:Lcom/dropbox/android/widget/bY;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->q:Lcom/dropbox/android/widget/bY;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->r:Lcom/dropbox/android/widget/bZ;

    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/bY;->a(Lcom/dropbox/android/widget/bZ;)V

    .line 122
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->q:Lcom/dropbox/android/widget/bY;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->q:Lcom/dropbox/android/widget/bY;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->r:Lcom/dropbox/android/widget/bZ;

    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/bY;->b(Lcom/dropbox/android/widget/bZ;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->m:Lcom/dropbox/android/widget/A;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/db231222/j/g;

    .line 134
    :cond_1
    return-void
.end method
