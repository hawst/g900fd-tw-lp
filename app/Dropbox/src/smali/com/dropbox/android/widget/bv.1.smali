.class public final Lcom/dropbox/android/widget/bv;
.super Landroid/widget/ArrayAdapter;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/dropbox/android/service/E;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private d:Ljava/util/concurrent/ExecutorService;

.field private volatile e:Z

.field private f:Lcom/dropbox/android/activity/ReferralActivity;

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/dropbox/android/service/D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/service/D",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/ReferralActivity;Ljava/util/ArrayList;Landroid/widget/ListView;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/ReferralActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/widget/ListView;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 46
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/bv;->d:Ljava/util/concurrent/ExecutorService;

    .line 54
    iput-boolean v1, p0, Lcom/dropbox/android/widget/bv;->e:Z

    .line 76
    iput-object p1, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    .line 77
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bv;->g:Ljava/util/Set;

    .line 78
    invoke-virtual {p1}, Lcom/dropbox/android/activity/ReferralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/bv;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/dropbox/android/activity/ReferralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/bv;->b:I

    .line 80
    invoke-virtual {p1}, Lcom/dropbox/android/activity/ReferralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/bv;->c:I

    .line 81
    new-instance v0, Lcom/dropbox/android/util/bb;

    iget v1, p0, Lcom/dropbox/android/widget/bv;->a:I

    iget v2, p0, Lcom/dropbox/android/widget/bv;->b:I

    iget v3, p0, Lcom/dropbox/android/widget/bv;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bb;-><init>(III)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bv;->i:Lcom/dropbox/android/service/D;

    .line 83
    new-instance v0, Lcom/dropbox/android/widget/bw;

    invoke-direct {v0, p0, p3}, Lcom/dropbox/android/widget/bw;-><init>(Lcom/dropbox/android/widget/bv;Landroid/widget/ListView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bv;->h:Landroid/os/Handler;

    .line 97
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/bv;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->g:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/bv;)Lcom/dropbox/android/activity/ReferralActivity;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/bv;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bv;->e:Z

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/bv;)Lcom/dropbox/android/service/D;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->i:Lcom/dropbox/android/service/D;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/bv;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->h:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/widget/bv;->g:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bv;->clear()V

    .line 118
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 119
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 120
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/bv;->add(Ljava/lang/Object;)V

    .line 121
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 126
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/dropbox/android/widget/bv;->insert(Ljava/lang/Object;I)V

    .line 128
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x5

    if-le v0, v2, :cond_2

    .line 129
    const/4 v0, 0x6

    invoke-virtual {p0, v3, v0}, Lcom/dropbox/android/widget/bv;->insert(Ljava/lang/Object;I)V

    .line 132
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bv;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 133
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 134
    iget-object v3, p0, Lcom/dropbox/android/widget/bv;->g:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 137
    :cond_4
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bv;->notifyDataSetChanged()V

    .line 138
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/dropbox/android/widget/bv;->e:Z

    .line 144
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bv;->e:Z

    if-nez v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bv;->notifyDataSetChanged()V

    .line 149
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 158
    if-eqz p1, :cond_0

    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    .line 159
    :cond_0
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const v5, 0x7f0f0026

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 173
    if-nez p2, :cond_0

    .line 174
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bv;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ReferralActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03008d

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 183
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bv;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_4

    .line 185
    const v0, 0x7f070172

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 186
    const v1, 0x7f070173

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 189
    if-nez p1, :cond_3

    .line 190
    iget-object v2, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    const v3, 0x7f0d02b1

    invoke-virtual {v2, v3}, Lcom/dropbox/android/activity/ReferralActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 191
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bv;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 193
    iget-object v2, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/ReferralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v5, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 194
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :goto_1
    return-object p2

    .line 176
    :cond_1
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bv;->getItemViewType(I)I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ReferralActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03008e

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 179
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 196
    :cond_3
    iget-object v2, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    const v3, 0x7f0d02b2

    invoke-virtual {v2, v3}, Lcom/dropbox/android/activity/ReferralActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 197
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bv;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x5

    add-int/lit8 v0, v0, -0x2

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 199
    iget-object v2, p0, Lcom/dropbox/android/widget/bv;->f:Lcom/dropbox/android/activity/ReferralActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/ReferralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v5, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 202
    :cond_4
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bv;->getItemViewType(I)I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 203
    const v0, 0x7f070177

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 204
    const v1, 0x7f070178

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 205
    const v2, 0x7f070175

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 206
    const v3, 0x7f070176

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 207
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bv;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dropbox/android/service/E;

    .line 209
    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 211
    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :goto_2
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->g:Ljava/util/Set;

    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 217
    new-instance v0, Lcom/dropbox/android/widget/bx;

    invoke-direct {v0, p0, v4}, Lcom/dropbox/android/widget/bx;-><init>(Lcom/dropbox/android/widget/bv;Lcom/dropbox/android/service/E;)V

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->g()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 232
    invoke-virtual {v4}, Lcom/dropbox/android/service/E;->g()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 213
    :cond_5
    const-string v0, ""

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 235
    :cond_6
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 236
    iget v1, p0, Lcom/dropbox/android/widget/bv;->a:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 237
    iget v1, p0, Lcom/dropbox/android/widget/bv;->a:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 238
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bv;->e:Z

    if-eqz v0, :cond_7

    .line 241
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 243
    :cond_7
    iget-object v0, p0, Lcom/dropbox/android/widget/bv;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/dropbox/android/widget/by;

    invoke-direct {v1, p0, v4, p1}, Lcom/dropbox/android/widget/by;-><init>(Lcom/dropbox/android/widget/bv;Lcom/dropbox/android/service/E;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 264
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x2

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 167
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bv;->getItemViewType(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
