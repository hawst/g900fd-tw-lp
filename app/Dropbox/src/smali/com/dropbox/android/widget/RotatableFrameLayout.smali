.class public Lcom/dropbox/android/widget/RotatableFrameLayout;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# static fields
.field private static f:Ljava/lang/reflect/Method;


# instance fields
.field protected a:Z

.field private b:F

.field private c:F

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private e:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    .line 23
    iput-boolean v1, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->a:Z

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setWillNotDraw(Z)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    .line 23
    iput-boolean v1, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->a:Z

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setWillNotDraw(Z)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    .line 23
    iput-boolean v1, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->a:Z

    .line 39
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setWillNotDraw(Z)V

    .line 41
    return-void
.end method

.method private a(IIII)F
    .locals 3

    .prologue
    .line 86
    int-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v0, v1

    int-to-float v1, p2

    int-to-float v2, p4

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 88
    return v0
.end method

.method private a(II)[I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 76
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 77
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 78
    invoke-direct {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->b()F

    move-result v2

    div-int/lit8 v3, p1, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, p2, 0x2

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 79
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 80
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    .line 81
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    .line 82
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v1, 0x1

    aput v0, v2, v1

    return-object v2
.end method

.method private b()F
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->c:F

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->b:F

    goto :goto_0
.end method

.method private static b(II)I
    .locals 5

    .prologue
    .line 216
    :try_start_0
    sget-object v0, Lcom/dropbox/android/widget/RotatableFrameLayout;->f:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_0

    .line 218
    :try_start_1
    const-class v0, Landroid/view/Gravity;

    const-string v1, "getAbsoluteGravity"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/RotatableFrameLayout;->f:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 224
    :cond_0
    :try_start_2
    sget-object v0, Lcom/dropbox/android/widget/RotatableFrameLayout;->f:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3

    .line 225
    :catch_1
    move-exception v0

    .line 226
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 227
    :catch_2
    move-exception v0

    .line 228
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 229
    :catch_3
    move-exception v0

    .line 230
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->b:F

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 308
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 309
    iget v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->b:F

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 310
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 311
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 312
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 15

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getChildCount()I

    move-result v3

    .line 238
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingLeft()I

    move-result v4

    .line 239
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingRight()I

    move-result v1

    sub-int v5, v0, v1

    .line 241
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingTop()I

    move-result v6

    .line 242
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingBottom()I

    move-result v1

    sub-int v7, v0, v1

    .line 245
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v0, v1, v2, v8}, Lcom/dropbox/android/widget/RotatableFrameLayout;->onSizeChanged(IIII)V

    .line 247
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 248
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 249
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    .line 250
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 252
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 253
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 255
    int-to-float v1, v9

    const/high16 v11, 0x3f800000    # 1.0f

    iget v12, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->e:F

    sub-float/2addr v11, v12

    mul-float/2addr v1, v11

    float-to-int v1, v1

    div-int/lit8 v11, v1, 0x2

    .line 256
    int-to-float v1, v10

    const/high16 v12, 0x3f800000    # 1.0f

    iget v13, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->e:F

    sub-float/2addr v12, v13

    mul-float/2addr v1, v12

    float-to-int v1, v1

    div-int/lit8 v12, v1, 0x2

    .line 261
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 262
    const/4 v13, -0x1

    if-ne v1, v13, :cond_0

    .line 264
    const/16 v1, 0x33

    .line 268
    :cond_0
    const/4 v13, 0x0

    invoke-static {v1, v13}, Lcom/dropbox/android/widget/RotatableFrameLayout;->b(II)I

    move-result v13

    .line 269
    and-int/lit8 v14, v1, 0x70

    .line 271
    and-int/lit8 v1, v13, 0x7

    packed-switch v1, :pswitch_data_0

    .line 283
    :pswitch_0
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v4

    add-int/2addr v1, v11

    .line 286
    :goto_1
    sparse-switch v14, :sswitch_data_0

    .line 298
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v6

    add-int/2addr v0, v12

    .line 301
    :goto_2
    add-int/2addr v9, v1

    add-int/2addr v10, v0

    invoke-virtual {v8, v1, v0, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 247
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 273
    :pswitch_1
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v4

    add-int/2addr v1, v11

    .line 274
    goto :goto_1

    .line 276
    :pswitch_2
    sub-int v1, v5, v4

    sub-int/2addr v1, v9

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    iget v11, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v11

    iget v11, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v11

    .line 278
    goto :goto_1

    .line 280
    :pswitch_3
    sub-int v1, v5, v9

    iget v13, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v13

    sub-int/2addr v1, v11

    .line 281
    goto :goto_1

    .line 288
    :sswitch_0
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v6

    add-int/2addr v0, v12

    .line 289
    goto :goto_2

    .line 291
    :sswitch_1
    sub-int v11, v7, v6

    sub-int/2addr v11, v10

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v11, v6

    iget v12, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v11, v12

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v11, v0

    .line 293
    goto :goto_2

    .line 295
    :sswitch_2
    sub-int v11, v7, v10

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v11, v0

    sub-int/2addr v0, v12

    .line 296
    goto :goto_2

    .line 304
    :cond_2
    return-void

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 286
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getChildCount()I

    move-result v11

    .line 95
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_4

    :cond_0
    const/4 v0, 0x1

    move v6, v0

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 100
    const/4 v9, 0x0

    .line 101
    const/4 v8, 0x0

    .line 102
    const/4 v7, 0x0

    .line 106
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 107
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 108
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 109
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 111
    invoke-direct {p0, v1, v4}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a(II)[I

    move-result-object v2

    .line 112
    const/4 v5, 0x0

    aget v5, v2, v5

    const/4 v10, 0x1

    aget v2, v2, v10

    invoke-direct {p0, v1, v4, v5, v2}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a(IIII)F

    move-result v5

    .line 113
    int-to-float v1, v1

    mul-float/2addr v1, v5

    float-to-int v1, v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 114
    int-to-float v0, v4

    mul-float/2addr v0, v5

    float-to-int v0, v0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 116
    const/4 v0, 0x0

    move v10, v0

    :goto_1
    if-ge v10, v11, :cond_5

    .line 117
    invoke-virtual {p0, v10}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getMeasureAllChildren()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-eq v0, v3, :cond_9

    .line 120
    :cond_1
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/RotatableFrameLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 121
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 123
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v3, v5

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v3, v5

    invoke-static {v9, v3}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 125
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v3, v5

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v3, v5

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 127
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v3

    invoke-static {v7, v3}, Lcom/dropbox/android/widget/RotatableFrameLayout;->combineMeasuredStates(II)I

    move-result v3

    .line 128
    if-eqz v6, :cond_3

    .line 129
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_2

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/4 v7, -0x1

    if-ne v0, v7, :cond_3

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move v0, v3

    move v1, v5

    move v3, v9

    .line 116
    :goto_2
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    move v7, v0

    move v8, v1

    move v9, v3

    goto :goto_1

    .line 95
    :cond_4
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    .line 138
    :cond_5
    invoke-direct {p0, v9, v8}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a(II)[I

    move-result-object v0

    .line 139
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-direct {p0, v9, v8, v1, v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a(IIII)F

    move-result v0

    .line 140
    int-to-float v1, v9

    div-float/2addr v1, v0

    float-to-int v1, v1

    .line 141
    int-to-float v2, v8

    div-float v0, v2, v0

    float-to-int v0, v0

    .line 144
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 148
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 149
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 151
    invoke-static {v1, p1, v7}, Lcom/dropbox/android/widget/RotatableFrameLayout;->resolveSizeAndState(III)I

    move-result v1

    .line 152
    shl-int/lit8 v2, v7, 0x10

    invoke-static {v0, p2, v2}, Lcom/dropbox/android/widget/RotatableFrameLayout;->resolveSizeAndState(III)I

    move-result v0

    .line 153
    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setMeasuredDimension(II)V

    .line 155
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a(II)[I

    move-result-object v2

    .line 157
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getMeasuredHeight()I

    move-result v4

    const/4 v5, 0x0

    aget v5, v2, v5

    const/4 v6, 0x1

    aget v2, v2, v6

    invoke-direct {p0, v3, v4, v5, v2}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a(IIII)F

    move-result v2

    iput v2, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->e:F

    .line 162
    iget v2, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->e:F

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v4, v1

    .line 163
    iget v1, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->e:F

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v5, v0

    .line 171
    iget-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 172
    const/4 v0, 0x1

    if-le v6, v0, :cond_8

    .line 173
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v6, :cond_8

    .line 174
    iget-object v0, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 176
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 180
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    const/4 v7, -0x1

    if-ne v2, v7, :cond_6

    .line 181
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingLeft()I

    move-result v2

    sub-int v2, v4, v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingRight()I

    move-result v7

    sub-int/2addr v2, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v2, v7

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 192
    :goto_4
    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_7

    .line 193
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingTop()I

    move-result v7

    sub-int v7, v5, v7

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v7, v8

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v1, v7, v1

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 204
    :goto_5
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    .line 173
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 186
    :cond_6
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingRight()I

    move-result v7

    add-int/2addr v2, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {p1, v2, v7}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getChildMeasureSpec(III)I

    move-result v2

    goto :goto_4

    .line 198
    :cond_7
    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getPaddingBottom()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v7, v8

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {p2, v7, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->getChildMeasureSpec(III)I

    move-result v1

    goto :goto_5

    .line 207
    :cond_8
    return-void

    :cond_9
    move v0, v7

    move v1, v8

    move v3, v9

    goto/16 :goto_2
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/dropbox/android/widget/RotatableFrameLayout;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t support foreground drawables"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRotationBackport(F)V
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->b:F

    .line 58
    return-void
.end method

.method public setRotationMaxBackport(F)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/dropbox/android/widget/RotatableFrameLayout;->c:F

    .line 54
    return-void
.end method
