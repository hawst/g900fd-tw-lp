.class final Lcom/dropbox/android/widget/bA;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/dropbox/android/widget/bB;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/bz;

.field private final b:Landroid/content/pm/ResolveInfo$DisplayNameComparator;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/bz;)V
    .locals 2

    .prologue
    .line 83
    iput-object p1, p0, Lcom/dropbox/android/widget/bA;->a:Lcom/dropbox/android/widget/bz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    iget-object v1, p0, Lcom/dropbox/android/widget/bA;->a:Lcom/dropbox/android/widget/bz;

    iget-object v1, v1, Lcom/dropbox/android/widget/bz;->a:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bA;->b:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/widget/bB;Lcom/dropbox/android/widget/bB;)I
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/bA;->b:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    iget-object v1, p1, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    iget-object v2, p2, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    move-result v0

    return v0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 83
    check-cast p1, Lcom/dropbox/android/widget/bB;

    check-cast p2, Lcom/dropbox/android/widget/bB;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/widget/bA;->a(Lcom/dropbox/android/widget/bB;Lcom/dropbox/android/widget/bB;)I

    move-result v0

    return v0
.end method
