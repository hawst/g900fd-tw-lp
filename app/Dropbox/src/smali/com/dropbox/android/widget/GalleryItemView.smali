.class public Lcom/dropbox/android/widget/GalleryItemView;
.super Landroid/widget/FrameLayout;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/m;


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/FrameLayout;

.field private final e:Landroid/widget/TextView;

.field private f:I

.field private g:Lcom/dropbox/android/util/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    .line 32
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/GalleryItemView;->setDuplicateParentStateEnabled(Z)V

    .line 33
    invoke-virtual {p2, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 34
    const v0, 0x7f0300cb

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 35
    invoke-virtual {v1, v2}, Landroid/view/View;->setDuplicateParentStateEnabled(Z)V

    .line 36
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/GalleryItemView;->addView(Landroid/view/View;)V

    .line 37
    const v0, 0x7f0701bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    .line 38
    const v0, 0x7f0701c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->b:Landroid/widget/ImageView;

    .line 39
    const v0, 0x7f07005d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->d:Landroid/widget/FrameLayout;

    .line 40
    const v0, 0x7f07005f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->e:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f0701bf

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->c:Landroid/view/View;

    .line 42
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 112
    iput-object v1, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    .line 114
    :cond_1
    return-void
.end method

.method private a(Lcom/dropbox/android/util/n;)V
    .locals 3

    .prologue
    .line 85
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->a()V

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryItemView;->g:Lcom/dropbox/android/util/n;

    invoke-virtual {v1}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/util/n;)V
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 119
    invoke-direct {p0, p2}, Lcom/dropbox/android/widget/GalleryItemView;->a(Lcom/dropbox/android/util/n;)V

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    .line 122
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;ZLcom/dropbox/android/filemanager/i;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, -0x1

    .line 49
    iget v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    if-eq v0, v1, :cond_0

    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    invoke-virtual {p3, v0, p0}, Lcom/dropbox/android/filemanager/i;->c(ILcom/dropbox/android/filemanager/m;)V

    .line 53
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryItemView;->a()V

    .line 55
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    .line 57
    iget v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    invoke-virtual {p3, v0, p0}, Lcom/dropbox/android/filemanager/i;->a(ILcom/dropbox/android/filemanager/m;)Lcom/dropbox/android/util/n;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_1

    .line 59
    iput v1, p0, Lcom/dropbox/android/widget/GalleryItemView;->f:I

    .line 60
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryItemView;->a(Lcom/dropbox/android/util/n;)V

    .line 61
    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 67
    :goto_0
    const-string v0, "_cursor_type_tag"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_tag_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    const-string v0, "vid_duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 69
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 70
    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryItemView;->e:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->d:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 79
    :goto_2
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->c:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setSelected(Z)V

    .line 81
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->e:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 76
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryItemView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 103
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryItemView;->a()V

    .line 104
    return-void
.end method
