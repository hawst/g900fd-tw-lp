.class public final enum Lcom/dropbox/android/widget/bM;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/widget/bM;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/bM;

.field public static final enum b:Lcom/dropbox/android/widget/bM;

.field public static final enum c:Lcom/dropbox/android/widget/bM;

.field public static final enum d:Lcom/dropbox/android/widget/bM;

.field private static final synthetic e:[Lcom/dropbox/android/widget/bM;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80
    new-instance v0, Lcom/dropbox/android/widget/bM;

    const-string v1, "ENTRY"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/bM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bM;->a:Lcom/dropbox/android/widget/bM;

    .line 84
    new-instance v0, Lcom/dropbox/android/widget/bM;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/bM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bM;->b:Lcom/dropbox/android/widget/bM;

    .line 88
    new-instance v0, Lcom/dropbox/android/widget/bM;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/widget/bM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bM;->c:Lcom/dropbox/android/widget/bM;

    .line 92
    new-instance v0, Lcom/dropbox/android/widget/bM;

    const-string v1, "POSITION"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/widget/bM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bM;->d:Lcom/dropbox/android/widget/bM;

    .line 75
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/widget/bM;

    sget-object v1, Lcom/dropbox/android/widget/bM;->a:Lcom/dropbox/android/widget/bM;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/widget/bM;->b:Lcom/dropbox/android/widget/bM;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/bM;->c:Lcom/dropbox/android/widget/bM;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/widget/bM;->d:Lcom/dropbox/android/widget/bM;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/widget/bM;->e:[Lcom/dropbox/android/widget/bM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/bM;
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/dropbox/android/widget/bM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bM;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/bM;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/dropbox/android/widget/bM;->e:[Lcom/dropbox/android/widget/bM;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/bM;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/bM;

    return-object v0
.end method
