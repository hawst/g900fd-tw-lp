.class final Lcom/dropbox/android/widget/bS;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/TextureVideoView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/TextureVideoView;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0, v2}, Lcom/dropbox/android/widget/TextureVideoView;->a(Lcom/dropbox/android/widget/TextureVideoView;Z)Z

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/TextureVideoView;->a(Lcom/dropbox/android/widget/TextureVideoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/TextureVideoView;->b(Lcom/dropbox/android/widget/TextureVideoView;)V

    .line 165
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/TextureVideoView;->a(Lcom/dropbox/android/widget/TextureVideoView;I)I

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/TextureVideoView;->b(Lcom/dropbox/android/widget/TextureVideoView;I)I

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/TextureVideoView;->d(Lcom/dropbox/android/widget/TextureVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/TextureVideoView;->c(Lcom/dropbox/android/widget/TextureVideoView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/TextureVideoView;->e(Lcom/dropbox/android/widget/TextureVideoView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/TextureVideoView;->d(Lcom/dropbox/android/widget/TextureVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 164
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/TextureVideoView;->requestLayout()V

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bS;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/TextureVideoView;->d(Lcom/dropbox/android/widget/TextureVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_1
.end method
