.class public Lcom/dropbox/android/widget/GalleryView;
.super Landroid/widget/RelativeLayout;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/al/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Ldbxyzptlk/db231222/al/b",
        "<",
        "Lcom/dropbox/android/widget/ay;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:I

.field private B:I

.field private C:Lcom/dropbox/android/widget/bR;

.field private D:Lcom/dropbox/android/widget/bR;

.field private E:Lcom/dropbox/android/widget/aD;

.field private F:I

.field private G:I

.field private H:Z

.field private final I:Ldbxyzptlk/db231222/al/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/al/a",
            "<",
            "Lcom/dropbox/android/widget/ay;",
            ">;"
        }
    .end annotation
.end field

.field private final J:Ldbxyzptlk/db231222/al/c;

.field private final K:Ldbxyzptlk/db231222/al/c;

.field private L:Z

.field private M:F

.field private N:F

.field private O:F

.field private P:F

.field private Q:F

.field private R:F

.field private S:F

.field private T:F

.field private U:Z

.field private V:J

.field private W:I

.field private Z:I

.field private aa:F

.field private ab:Landroid/database/Cursor;

.field private ac:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z

.field private d:Lcom/dropbox/android/widget/ax;

.field private final e:Ljava/lang/Runnable;

.field private f:Lcom/dropbox/android/util/analytics/ChainInfo;

.field private final g:Ljava/lang/Runnable;

.field private h:Lcom/dropbox/android/taskqueue/D;

.field private i:I

.field private j:I

.field private k:I

.field private final l:Landroid/os/Handler;

.field private m:Z

.field private n:J

.field private o:J

.field private p:F

.field private final q:Lcom/dropbox/android/widget/aE;

.field private final r:Ljava/text/DateFormat;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private v:Lcom/dropbox/android/activity/bq;

.field private w:Lcom/dropbox/android/albums/Album;

.field private x:Landroid/graphics/drawable/Drawable;

.field private y:Landroid/widget/ProgressBar;

.field private z:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/GalleryView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/widget/GalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 309
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/widget/GalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 313
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x3

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 332
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 98
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    .line 99
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    .line 117
    iput-object v4, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ax;

    .line 119
    new-instance v0, Lcom/dropbox/android/widget/au;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/au;-><init>(Lcom/dropbox/android/widget/GalleryView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    .line 137
    new-instance v0, Lcom/dropbox/android/widget/av;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/av;-><init>(Lcom/dropbox/android/widget/GalleryView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Ljava/lang/Runnable;

    .line 163
    iput v3, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    .line 164
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 170
    iput v3, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    .line 172
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->l:Landroid/os/Handler;

    .line 177
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    .line 241
    new-instance v0, Lcom/dropbox/android/widget/aE;

    invoke-direct {v0, v4}, Lcom/dropbox/android/widget/aE;-><init>(Lcom/dropbox/android/widget/au;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->q:Lcom/dropbox/android/widget/aE;

    .line 264
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:I

    .line 265
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->B:I

    .line 268
    new-instance v0, Lcom/dropbox/android/widget/aD;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aD;-><init>(Lcom/dropbox/android/widget/GalleryView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    .line 269
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    .line 270
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    .line 271
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->H:Z

    .line 273
    new-instance v0, Ldbxyzptlk/db231222/al/a;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/al/a;-><init>(Ldbxyzptlk/db231222/al/b;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->I:Ldbxyzptlk/db231222/al/a;

    .line 274
    new-instance v0, Ldbxyzptlk/db231222/al/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/al/c;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    .line 276
    new-instance v0, Ldbxyzptlk/db231222/al/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/al/c;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->K:Ldbxyzptlk/db231222/al/c;

    .line 278
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->L:Z

    .line 294
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->U:Z

    .line 1186
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    .line 334
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->setBackgroundColor(I)V

    .line 336
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/au;->b(Landroid/content/res/Resources;)Ljava/util/Locale;

    move-result-object v0

    .line 338
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    invoke-static {}, Lcom/dropbox/android/widget/GalleryView;->a()Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->r:Ljava/text/DateFormat;

    .line 345
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->r:Ljava/text/DateFormat;

    new-instance v1, Ljava/util/SimpleTimeZone;

    const-string v2, "UTC"

    invoke-direct {v1, v3, v2}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 346
    return-void

    .line 341
    :cond_0
    invoke-static {v2, v2, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->r:Ljava/text/DateFormat;

    goto :goto_0
.end method

.method private a(Lcom/dropbox/android/widget/ay;F)F
    .locals 3

    .prologue
    const/high16 v2, 0x40800000    # 4.0f

    .line 1478
    invoke-virtual {p1}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v0

    .line 1480
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1481
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->I:Ldbxyzptlk/db231222/al/a;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/al/a;->a(FF)V

    .line 1484
    :cond_0
    invoke-static {p2, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1485
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1487
    return v0
.end method

.method private a(Lcom/dropbox/android/widget/ay;Z)F
    .locals 3

    .prologue
    .line 1456
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 1457
    :goto_0
    if-eqz p2, :cond_0

    .line 1458
    const v1, 0x3d4ccccd    # 0.05f

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 1459
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->e(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    sub-float/2addr v2, v0

    cmpg-float v2, v2, v1

    if-gez v2, :cond_0

    .line 1460
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->e(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    sub-float/2addr v0, v1

    .line 1463
    :cond_0
    return v0

    .line 1456
    :cond_1
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->e(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;I)I
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->A:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;Lcom/dropbox/android/util/analytics/ChainInfo;)Lcom/dropbox/android/util/analytics/ChainInfo;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/ax;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ax;

    return-object v0
.end method

.method private a(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->r:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1245
    return-object v0
.end method

.method public static a()Ljava/text/DateFormat;
    .locals 5

    .prologue
    .line 323
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "M/d/yyyy h:mmaa"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 324
    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->getDateFormatSymbols()Ljava/text/DateFormatSymbols;

    move-result-object v1

    .line 325
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "am"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "pm"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/text/DateFormatSymbols;->setAmPmStrings([Ljava/lang/String;)V

    .line 326
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setDateFormatSymbols(Ljava/text/DateFormatSymbols;)V

    .line 328
    return-object v0
.end method

.method private a(F)V
    .locals 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1309
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(FF)Landroid/util/Pair;

    move-result-object v2

    .line 1310
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move-object v0, p0

    move v3, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/GalleryView;->b(FFFFF)V

    .line 1311
    return-void
.end method

.method private a(FF)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1299
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v3

    .line 1300
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1301
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(F)V

    .line 1306
    :goto_0
    return-void

    .line 1303
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->b(FF)Landroid/util/Pair;

    move-result-object v2

    .line 1304
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v0

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    sub-float v4, v0, p1

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    sub-float v5, v0, p2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/GalleryView;->b(FFFFF)V

    goto :goto_0
.end method

.method private a(FFFFF)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1332
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 1334
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->g(Lcom/dropbox/android/widget/ay;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1352
    :goto_0
    return-void

    .line 1338
    :cond_0
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    .line 1341
    const/high16 v2, 0x40800000    # 4.0f

    invoke-static {p3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1342
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1344
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->n(Lcom/dropbox/android/widget/ay;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    sub-float/2addr v4, p1

    sub-float v5, v2, v1

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    add-float/2addr v3, p4

    .line 1345
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v4

    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->o(Lcom/dropbox/android/widget/ay;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    sub-float/2addr v5, p2

    sub-float v1, v2, v1

    mul-float/2addr v1, v5

    add-float/2addr v1, v4

    add-float/2addr v1, p5

    .line 1347
    invoke-virtual {p0, v3, v1, v2}, Lcom/dropbox/android/widget/GalleryView;->setCurrentImagePosScale(FFF)V

    .line 1350
    invoke-direct {p0, v0, v2}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;F)V

    .line 1351
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    goto :goto_0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 1107
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1111
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-eq v0, p1, :cond_0

    .line 1117
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-lt p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    .line 1123
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aU()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "image.index"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1125
    :cond_0
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    .line 1126
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 1129
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;F)V

    .line 1131
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->l(Lcom/dropbox/android/widget/ay;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 1133
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->t()V

    .line 1134
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->s()V

    .line 1136
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;F)V

    .line 1138
    :cond_1
    return-void

    .line 1117
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 84
    invoke-static {p0}, Lcom/dropbox/android/widget/GalleryView;->d(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1250
    .line 1253
    sget-object v0, Lcom/dropbox/android/widget/aw;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->v:Lcom/dropbox/android/activity/bq;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1274
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1255
    :pswitch_0
    const/4 v0, 0x0

    .line 1277
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d01e2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v5}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1280
    if-eqz v0, :cond_2

    .line 1281
    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1282
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1287
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1289
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Lcom/dropbox/android/activity/bq;

    sget-object v1, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v1, :cond_0

    .line 1290
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->w()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1292
    :cond_0
    return-void

    .line 1259
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->w:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1263
    :pswitch_2
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->e()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1264
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "LocalEntry should have a defined timeTaken"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1266
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->e()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1270
    :pswitch_3
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    goto :goto_0

    .line 1284
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->s:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1253
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1183
    sub-int v1, p2, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    return p1
.end method

.method private a(Lcom/dropbox/android/widget/ay;)Z
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/high16 v7, 0x40400000    # 3.0f

    const/high16 v6, -0x3fc00000    # -3.0f

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1384
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1385
    iget-wide v4, p0, Lcom/dropbox/android/widget/GalleryView;->o:J

    sub-long v4, v0, v4

    long-to-float v4, v4

    .line 1386
    iput-wide v0, p0, Lcom/dropbox/android/widget/GalleryView;->o:J

    .line 1388
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-nez v0, :cond_0

    move v1, v2

    .line 1452
    :goto_0
    return v1

    .line 1395
    :cond_0
    const/4 v0, 0x0

    .line 1396
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-eq v1, v8, :cond_a

    .line 1397
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-nez v1, :cond_4

    .line 1399
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v0, v2, :cond_3

    .line 1400
    invoke-direct {p0, p1, v3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Z)F

    move-result v0

    neg-float v0, v0

    .line 1413
    :cond_1
    :goto_1
    const/high16 v1, 0x40c00000    # 6.0f

    div-float v1, v0, v1

    .line 1423
    const v4, 0x3dcccccd    # 0.1f

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1424
    const v4, -0x42333333    # -0.1f

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1425
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->p:F

    .line 1429
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-ne v4, v2, :cond_6

    cmpg-float v4, v0, v7

    if-gez v4, :cond_6

    .line 1430
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ab()Lcom/dropbox/android/util/analytics/l;

    move-result-object v4

    const-string v5, "index"

    iget v6, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v6, v6, -0x1

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1431
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 1432
    iget-object v4, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v4, v5}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v4

    invoke-static {v4}, Lcom/dropbox/android/widget/ay;->l(Lcom/dropbox/android/widget/ay;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 1443
    :cond_2
    :goto_2
    if-eqz v2, :cond_9

    .line 1444
    invoke-direct {p0, v3}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 1445
    iput v8, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 1449
    :goto_3
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    move v1, v2

    .line 1451
    :goto_4
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    add-float/2addr v0, v2

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    invoke-static {p1, v0, v2, v3}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    goto :goto_0

    .line 1403
    :cond_3
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    invoke-direct {p0, p1, v3}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;Z)F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_1

    .line 1405
    :cond_4
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-ne v1, v2, :cond_5

    .line 1406
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    invoke-direct {p0, p1, v2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Z)F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_1

    .line 1407
    :cond_5
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    .line 1408
    invoke-direct {p0, p1, v2}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;Z)F

    move-result v0

    neg-float v0, v0

    goto/16 :goto_1

    .line 1434
    :cond_6
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    cmpl-float v4, v0, v6

    if-lez v4, :cond_7

    .line 1435
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ab()Lcom/dropbox/android/util/analytics/l;

    move-result-object v4

    const-string v5, "index"

    iget v6, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1436
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {p0, v4}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 1437
    iget-object v4, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v4, v5}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v4

    invoke-static {v4}, Lcom/dropbox/android/widget/ay;->l(Lcom/dropbox/android/widget/ay;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_2

    .line 1439
    :cond_7
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-nez v4, :cond_8

    cmpg-float v4, v0, v7

    if-gez v4, :cond_8

    cmpl-float v4, v0, v6

    if-gtz v4, :cond_2

    :cond_8
    move v2, v3

    goto :goto_2

    :cond_9
    move v0, v1

    .line 1447
    goto :goto_3

    :cond_a
    move v1, v3

    goto :goto_4
.end method

.method private b(Lcom/dropbox/android/widget/ay;Z)F
    .locals 3

    .prologue
    .line 1467
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 1468
    :goto_0
    if-eqz p2, :cond_0

    .line 1469
    const v1, 0x3d4ccccd    # 0.05f

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 1470
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->f(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    sub-float v2, v0, v2

    cmpg-float v2, v2, v1

    if-gez v2, :cond_0

    .line 1471
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->f(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    add-float/2addr v0, v1

    .line 1474
    :cond_0
    return v0

    .line 1467
    :cond_1
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->f(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/GalleryView;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/GalleryView;I)I
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->B:I

    return p1
.end method

.method private b(FF)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1315
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 1318
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    sub-float/2addr v1, p1

    .line 1319
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    sub-float/2addr v2, p2

    .line 1322
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    div-float/2addr v1, v3

    .line 1323
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    div-float/2addr v2, v3

    .line 1325
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->n(Lcom/dropbox/android/widget/ay;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    sub-float v1, v3, v1

    .line 1326
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->o(Lcom/dropbox/android/widget/ay;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    sub-float/2addr v0, v2

    .line 1328
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private b(FFFFF)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1355
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->M:F

    .line 1356
    iput p2, p0, Lcom/dropbox/android/widget/GalleryView;->N:F

    .line 1357
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->O:F

    .line 1358
    iput p3, p0, Lcom/dropbox/android/widget/GalleryView;->P:F

    .line 1359
    iput p4, p0, Lcom/dropbox/android/widget/GalleryView;->Q:F

    .line 1360
    iput p5, p0, Lcom/dropbox/android/widget/GalleryView;->R:F

    .line 1361
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/widget/GalleryView;->V:J

    .line 1362
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->S:F

    .line 1363
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:F

    .line 1364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:Z

    .line 1365
    return-void
.end method

.method static synthetic b(Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 84
    invoke-static {p0}, Lcom/dropbox/android/widget/GalleryView;->c(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method private b(Lcom/dropbox/android/widget/ay;F)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1500
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;F)F

    move-result v2

    .line 1502
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1503
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->v()V

    .line 1506
    :cond_0
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    .line 1509
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    .line 1510
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    .line 1511
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 1513
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->e(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_3

    .line 1514
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    div-float/2addr v0, v5

    .line 1522
    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->i(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6

    .line 1524
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->j(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_5

    .line 1525
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->i(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    div-float/2addr v1, v5

    .line 1533
    :cond_2
    :goto_1
    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    .line 1534
    return-void

    .line 1515
    :cond_3
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->f(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 1516
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    div-float/2addr v3, v5

    sub-float/2addr v0, v3

    goto :goto_0

    .line 1520
    :cond_4
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    goto :goto_0

    .line 1526
    :cond_5
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->k(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 1527
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v1, v1

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->i(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    div-float/2addr v3, v5

    sub-float/2addr v1, v3

    goto :goto_1

    .line 1531
    :cond_6
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v1, v1

    div-float/2addr v1, v5

    goto :goto_1
.end method

.method private b(I)Z
    .locals 2

    .prologue
    .line 1172
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/util/analytics/ChainInfo;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    return-object v0
.end method

.method private c(I)V
    .locals 0

    .prologue
    .line 1295
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    .line 1296
    return-void
.end method

.method private static c(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 1064
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1065
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/aD;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    return-object v0
.end method

.method private static d(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 1068
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1069
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/widget/GalleryView;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/GalleryView;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/D;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->h:Lcom/dropbox/android/taskqueue/D;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/GalleryView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->x:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/GalleryView;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:I

    return v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/GalleryView;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->B:I

    return v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/GalleryView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/dropbox/android/widget/GalleryView;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/dropbox/android/widget/GalleryView;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->s()V

    return-void
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Lcom/dropbox/android/widget/ay;
    .locals 2

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    const/4 v0, 0x0

    .line 533
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    const v2, 0x7f020183

    .line 939
    .line 941
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02024f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->x:Landroid/graphics/drawable/Drawable;

    .line 943
    const v0, 0x7f0700be

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->y:Landroid/widget/ProgressBar;

    .line 944
    const v0, 0x7f0700bf

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->z:Landroid/widget/ProgressBar;

    .line 946
    invoke-static {}, Lcom/dropbox/android/util/bn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->y:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setBackgroundResource(I)V

    .line 952
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->z:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setBackgroundResource(I)V

    .line 955
    :cond_0
    invoke-static {}, Lcom/dropbox/android/widget/bR;->a()Landroid/text/TextPaint;

    move-result-object v0

    .line 956
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 958
    new-instance v1, Lcom/dropbox/android/widget/bR;

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/widget/bR;-><init>(Landroid/text/TextPaint;Landroid/text/Layout$Alignment;)V

    iput-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:Lcom/dropbox/android/widget/bR;

    .line 959
    new-instance v1, Lcom/dropbox/android/widget/bR;

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/widget/bR;-><init>(Landroid/text/TextPaint;Landroid/text/Layout$Alignment;)V

    iput-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->D:Lcom/dropbox/android/widget/bR;

    .line 961
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->postInvalidate()V

    .line 962
    return-void
.end method

.method private o()V
    .locals 8

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 1043
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dropbox/android/widget/GalleryView;->V:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    mul-double/2addr v0, v4

    const-wide v2, 0x406f400000000000L    # 250.0

    div-double/2addr v0, v2

    .line 1044
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 1047
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v6, v0

    .line 1049
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->Q:F

    mul-float/2addr v0, v6

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->S:F

    sub-float v4, v0, v1

    .line 1050
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->R:F

    mul-float/2addr v0, v6

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:F

    sub-float v5, v0, v1

    .line 1051
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->S:F

    add-float/2addr v0, v4

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->S:F

    .line 1052
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:F

    add-float/2addr v0, v5

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:F

    .line 1054
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->M:F

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->N:F

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->O:F

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->P:F

    iget v7, p0, Lcom/dropbox/android/widget/GalleryView;->O:F

    sub-float/2addr v3, v7

    mul-float/2addr v3, v6

    add-float/2addr v3, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/GalleryView;->a(FFFFF)V

    .line 1056
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 1057
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:Z

    .line 1061
    :goto_0
    return-void

    .line 1059
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    goto :goto_0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->y:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->d(Landroid/widget/ProgressBar;)V

    .line 1073
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->z:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->d(Landroid/widget/ProgressBar;)V

    .line 1074
    return-void
.end method

.method private q()V
    .locals 6

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1142
    sget-object v2, Lcom/dropbox/android/widget/GalleryView;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recycling bmp as unloadDrawables: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1143
    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->e()V

    goto :goto_0

    .line 1145
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1146
    return-void
.end method

.method private r()[I
    .locals 4

    .prologue
    .line 1154
    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x5

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x6

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x7

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    mul-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    aput v2, v0, v1

    return-object v0
.end method

.method private s()V
    .locals 8

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->h:Lcom/dropbox/android/taskqueue/D;

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;)V

    .line 1193
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v1

    .line 1195
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->r()[I

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget v4, v2, v0

    .line 1197
    if-ltz v4, :cond_0

    if-lt v4, v1, :cond_1

    .line 1195
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1201
    :cond_1
    iget-object v5, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v5, v4}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v5

    .line 1203
    iget v6, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-direct {p0, v4, v6}, Lcom/dropbox/android/widget/GalleryView;->a(II)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-boolean v6, p0, Lcom/dropbox/android/widget/GalleryView;->H:Z

    if-eqz v6, :cond_4

    .line 1204
    invoke-direct {p0, v4}, Lcom/dropbox/android/widget/GalleryView;->b(I)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v5}, Lcom/dropbox/android/widget/ay;->m(Lcom/dropbox/android/widget/ay;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1219
    :cond_2
    :goto_2
    return-void

    .line 1208
    :cond_3
    iget-object v6, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1209
    iget-object v6, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1210
    invoke-virtual {v5}, Lcom/dropbox/android/widget/ay;->d()Z

    move-result v5

    .line 1211
    if-eqz v5, :cond_0

    invoke-direct {p0, v4}, Lcom/dropbox/android/widget/GalleryView;->b(I)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_2

    .line 1216
    :cond_4
    invoke-virtual {v5}, Lcom/dropbox/android/widget/ay;->c()V

    goto :goto_1
.end method

.method private t()V
    .locals 6

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1223
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1225
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-direct {p0, v2, v3}, Lcom/dropbox/android/widget/GalleryView;->a(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1226
    sget-object v2, Lcom/dropbox/android/widget/GalleryView;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recycling bmp as removeImagesNoLongerLive: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->e()V

    .line 1228
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1231
    :cond_1
    return-void
.end method

.method private u()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1368
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1369
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1373
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_0

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    .line 1375
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_1

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    .line 1378
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->aa:F

    .line 1379
    return-void

    .line 1373
    :cond_0
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 1375
    :cond_1
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1491
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 1493
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->g(Lcom/dropbox/android/widget/ay;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1494
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->g()Z

    move-result v0

    .line 1495
    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->L:Z

    .line 1497
    :cond_0
    return-void
.end method

.method private w()Landroid/view/View;
    .locals 2

    .prologue
    .line 1541
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Lcom/dropbox/android/activity/bq;

    sget-object v1, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v1, :cond_0

    .line 1542
    const v0, 0x7f0700b9

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/view/View;

    .line 1545
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/al/c;)Lcom/dropbox/android/widget/ay;
    .locals 3

    .prologue
    .line 627
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    if-eqz v0, :cond_1

    .line 628
    :cond_0
    const/4 v0, 0x0

    .line 650
    :goto_0
    return-object v0

    .line 631
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 632
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-lez v0, :cond_3

    .line 633
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 634
    invoke-virtual {p1}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 635
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    goto :goto_0

    .line 639
    :cond_2
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 640
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 641
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 642
    invoke-virtual {p1}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 643
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    goto :goto_0

    .line 649
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/activity/bq;Ldbxyzptlk/db231222/v/n;I)V
    .locals 6

    .prologue
    .line 439
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->ab:Landroid/database/Cursor;

    .line 443
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    if-nez v0, :cond_1

    .line 444
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->u()V

    .line 448
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 449
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 450
    iget-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 451
    new-instance v3, Landroid/util/Pair;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v5

    iget-object v5, v5, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 456
    new-instance v0, Lcom/dropbox/android/widget/aD;

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->ab:Landroid/database/Cursor;

    invoke-direct {v0, p0, v1, p2, p3}, Lcom/dropbox/android/widget/aD;-><init>(Lcom/dropbox/android/widget/GalleryView;Landroid/database/Cursor;Lcom/dropbox/android/activity/bq;Ldbxyzptlk/db231222/v/n;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    .line 459
    const/4 v0, 0x0

    add-int/lit8 v1, p4, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v0

    add-int/lit8 v3, p4, 0x2

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 460
    invoke-direct {p0, v1, p4}, Lcom/dropbox/android/widget/GalleryView;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 461
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v3

    .line 462
    new-instance v0, Landroid/util/Pair;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v5

    iget-object v5, v5, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 464
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 465
    iget-object v4, p0, Lcom/dropbox/android/widget/GalleryView;->ac:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 469
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ay;->d()Z

    .line 470
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ay;

    invoke-virtual {v3, v0}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;)V

    .line 459
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 475
    :cond_4
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ay;

    .line 476
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->e()V

    goto :goto_2

    .line 478
    :cond_5
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 481
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aa()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "size"

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "index"

    int-to-long v2, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 482
    invoke-direct {p0, p4}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 483
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 484
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/ay;Ldbxyzptlk/db231222/al/c;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x41200000    # 10.0f

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 660
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/widget/GalleryView;->n:J

    .line 661
    if-eqz p2, :cond_1

    .line 662
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/al/c;->a(Ldbxyzptlk/db231222/al/c;)V

    .line 663
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 665
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->K:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/al/c;->a(Ldbxyzptlk/db231222/al/c;)V

    .line 666
    iput v3, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    .line 668
    iput v7, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 670
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 673
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 738
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 740
    :cond_1
    return-void

    .line 677
    :cond_2
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    if-eqz v0, :cond_3

    .line 678
    iput-boolean v4, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    goto :goto_0

    .line 681
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->K:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v1

    sub-float/2addr v0, v1

    .line 682
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->K:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v2

    sub-float/2addr v1, v2

    .line 683
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    .line 684
    const/high16 v1, 0x43100000    # 144.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 686
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 687
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 689
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    if-eqz v0, :cond_6

    .line 690
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(FF)V

    .line 691
    iput-boolean v4, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    .line 708
    :goto_1
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-ne v0, v7, :cond_1

    .line 714
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 715
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v1, v3, :cond_9

    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Z)F

    move-result v1

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->aa:F

    mul-float/2addr v1, v2

    cmpl-float v1, v1, v6

    if-lez v1, :cond_9

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-lez v1, :cond_9

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->q:Lcom/dropbox/android/widget/aE;

    sget-object v2, Lcom/dropbox/android/widget/aF;->b:Lcom/dropbox/android/widget/aF;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/aE;->b(Lcom/dropbox/android/widget/aF;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 720
    iput v3, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 721
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    .line 735
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->q:Lcom/dropbox/android/widget/aE;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aE;->a()V

    goto :goto_0

    .line 694
    :cond_6
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    .line 695
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(II)Z

    move-result v0

    .line 697
    if-eqz v0, :cond_7

    .line 698
    invoke-static {}, Lcom/dropbox/android/util/analytics/ChainInfo;->a()Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    .line 699
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ae()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/G;->c()Lcom/dropbox/android/service/J;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 704
    :cond_7
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Ljava/lang/Runnable;

    :goto_3
    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/widget/GalleryView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    goto :goto_3

    .line 722
    :cond_9
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v1, v5, :cond_a

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v1, v1

    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;Z)F

    move-result v0

    sub-float v0, v1, v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->aa:F

    mul-float/2addr v0, v1

    cmpl-float v0, v0, v6

    if-lez v0, :cond_a

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_a

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->q:Lcom/dropbox/android/widget/aE;

    sget-object v1, Lcom/dropbox/android/widget/aF;->a:Lcom/dropbox/android/widget/aF;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aE;->b(Lcom/dropbox/android/widget/aF;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 727
    iput v5, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 728
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    goto :goto_2

    .line 730
    :cond_a
    iput v4, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 731
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    if-ne v0, v5, :cond_5

    .line 732
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ad()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->g()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;D)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_2
.end method

.method public final a(Lcom/dropbox/android/widget/ay;Ldbxyzptlk/db231222/al/d;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 748
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v4

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v6

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v7

    const/4 v9, 0x0

    move-object v0, p2

    move v8, v5

    invoke-virtual/range {v0 .. v9}, Ldbxyzptlk/db231222/al/d;->a(FFZFZFFZF)V

    .line 751
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/c;)V
    .locals 0

    .prologue
    .line 84
    check-cast p1, Lcom/dropbox/android/widget/ay;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Ldbxyzptlk/db231222/al/c;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/d;)V
    .locals 0

    .prologue
    .line 84
    check-cast p1, Lcom/dropbox/android/widget/ay;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Ldbxyzptlk/db231222/al/d;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/ay;Ldbxyzptlk/db231222/al/d;Ldbxyzptlk/db231222/al/c;)Z
    .locals 9

    .prologue
    .line 759
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    if-eqz v0, :cond_1

    .line 760
    :cond_0
    const/4 v0, 0x1

    .line 898
    :goto_0
    return v0

    .line 762
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 763
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    .line 764
    invoke-virtual {p3}, Ldbxyzptlk/db231222/al/c;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->e(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    const/high16 v1, -0x3f600000    # -5.0f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_2

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->f(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    add-int/lit8 v1, v1, 0x5

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 766
    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    .line 767
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 768
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ac()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "scale"

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    float-to-double v5, v2

    invoke-virtual {v0, v1, v5, v6}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;D)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 775
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->J:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0, p3}, Ldbxyzptlk/db231222/al/c;->a(Ldbxyzptlk/db231222/al/c;)V

    .line 777
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 778
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ax;

    if-eqz v0, :cond_4

    .line 779
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ax;

    invoke-interface {v0}, Lcom/dropbox/android/widget/ax;->b()V

    .line 782
    :cond_4
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->g(Lcom/dropbox/android/widget/ay;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 783
    invoke-virtual {p2}, Ldbxyzptlk/db231222/al/d;->c()F

    move-result v0

    .line 784
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;F)V

    .line 788
    :cond_5
    invoke-virtual {p2}, Ldbxyzptlk/db231222/al/d;->a()F

    move-result v1

    .line 789
    invoke-virtual {p2}, Ldbxyzptlk/db231222/al/d;->b()F

    move-result v2

    .line 791
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    sub-float v5, v1, v0

    .line 792
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    sub-float v6, v2, v0

    .line 794
    iget-wide v7, p0, Lcom/dropbox/android/widget/GalleryView;->n:J

    sub-long/2addr v7, v3

    long-to-float v0, v7

    .line 795
    const/high16 v7, 0x42c80000    # 100.0f

    cmpl-float v7, v0, v7

    if-lez v7, :cond_6

    .line 796
    const/high16 v0, 0x42c80000    # 100.0f

    .line 799
    :cond_6
    const/4 v7, 0x0

    cmpl-float v7, v0, v7

    if-eqz v7, :cond_8

    .line 800
    div-float v0, v5, v0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->p:F

    .line 801
    iput-wide v3, p0, Lcom/dropbox/android/widget/GalleryView;->n:J

    .line 803
    const/4 v0, 0x0

    .line 804
    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->p:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_d

    .line 805
    sget-object v0, Lcom/dropbox/android/widget/aF;->b:Lcom/dropbox/android/widget/aF;

    .line 809
    :cond_7
    :goto_2
    iget-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->q:Lcom/dropbox/android/widget/aE;

    invoke-virtual {v3, v0}, Lcom/dropbox/android/widget/aE;->a(Lcom/dropbox/android/widget/aF;)V

    .line 812
    :cond_8
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_9

    .line 815
    :cond_9
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_e

    .line 816
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    add-float/2addr v0, v1

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_13

    .line 818
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    int-to-float v0, v0

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    sub-float/2addr v0, v1

    .line 820
    :goto_3
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    sub-float v1, v0, v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-lez v1, :cond_a

    .line 822
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->h(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 828
    :cond_a
    :goto_4
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 831
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_b

    .line 834
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->aa:F

    mul-float/2addr v1, v3

    const/high16 v3, 0x41a00000    # 20.0f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_b

    .line 835
    const/4 v1, 0x0

    cmpl-float v1, v5, v1

    if-lez v1, :cond_f

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-lez v1, :cond_f

    .line 837
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 838
    const/4 v1, 0x1

    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 839
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    .line 840
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    .line 882
    :cond_b
    :goto_5
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->i(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_11

    .line 884
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->j(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    add-float/2addr v1, v6

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-lez v1, :cond_10

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->k(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    add-float/2addr v1, v6

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_10

    .line 886
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->i(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 894
    :goto_6
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    .line 897
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 898
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 770
    :cond_c
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 771
    invoke-virtual {p3}, Ldbxyzptlk/db231222/al/c;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 772
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    goto/16 :goto_1

    .line 806
    :cond_d
    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->p:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_7

    .line 807
    sget-object v0, Lcom/dropbox/android/widget/aF;->a:Lcom/dropbox/android/widget/aF;

    goto/16 :goto_2

    .line 826
    :cond_e
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    goto/16 :goto_4

    .line 841
    :cond_f
    const/4 v1, 0x0

    cmpg-float v1, v5, v1

    if-gez v1, :cond_b

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_b

    .line 842
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 843
    const/4 v1, 0x2

    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 844
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    .line 845
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    goto :goto_5

    .line 887
    :cond_10
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->j(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    add-float/2addr v1, v6

    const/4 v3, 0x0

    cmpg-float v1, v1, v3

    if-gez v1, :cond_12

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->k(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    add-float/2addr v1, v6

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_12

    .line 889
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    int-to-float v1, v1

    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->i(Lcom/dropbox/android/widget/ay;)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    goto :goto_6

    .line 892
    :cond_11
    invoke-static {p1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    goto :goto_6

    :cond_12
    move v1, v2

    goto :goto_6

    :cond_13
    move v0, v1

    goto/16 :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/d;Ldbxyzptlk/db231222/al/c;)Z
    .locals 1

    .prologue
    .line 84
    check-cast p1, Lcom/dropbox/android/widget/ay;

    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Ldbxyzptlk/db231222/al/d;Ldbxyzptlk/db231222/al/c;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 514
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    return v0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/al/c;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/GalleryView;->a(Ldbxyzptlk/db231222/al/c;)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 522
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:I

    return v0
.end method

.method public final d()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 538
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    const/4 v0, 0x0

    .line 542
    :goto_0
    return-object v0

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ab:Landroid/database/Cursor;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 542
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->ab:Landroid/database/Cursor;

    goto :goto_0
.end method

.method public final e()Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 547
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 548
    if-nez v0, :cond_0

    .line 549
    const/4 v0, 0x0

    .line 551
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Lcom/dropbox/android/albums/AlbumItemEntry;
    .locals 1

    .prologue
    .line 556
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 557
    if-nez v0, :cond_0

    .line 558
    const/4 v0, 0x0

    .line 560
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->b()Lcom/dropbox/android/albums/AlbumItemEntry;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 566
    if-nez v0, :cond_0

    .line 567
    const/high16 v0, 0x3f800000    # 1.0f

    .line 569
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v0

    goto :goto_0
.end method

.method public final h()[F
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 574
    new-array v0, v2, [F

    .line 575
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v1

    .line 576
    if-nez v1, :cond_0

    .line 577
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    .line 581
    :goto_0
    return-object v0

    .line 579
    :cond_0
    const/4 v2, 0x0

    invoke-static {v1}, Lcom/dropbox/android/widget/ay;->c(Lcom/dropbox/android/widget/ay;)F

    move-result v3

    aput v3, v0, v2

    .line 580
    const/4 v2, 0x1

    invoke-static {v1}, Lcom/dropbox/android/widget/ay;->d(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    aput v1, v0, v2

    goto :goto_0

    .line 577
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public final i()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 590
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-lez v0, :cond_2

    .line 591
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 592
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 596
    :cond_0
    :goto_0
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-nez v0, :cond_1

    .line 597
    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 599
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 601
    :cond_2
    return-void

    .line 593
    :cond_3
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 594
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 604
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 605
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 606
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 610
    :cond_0
    :goto_0
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-nez v0, :cond_1

    .line 611
    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->c(I)V

    .line 613
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 615
    :cond_2
    return-void

    .line 607
    :cond_3
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 608
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 916
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->u()V

    .line 917
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 919
    iget-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->L:Z

    if-nez v1, :cond_1

    .line 920
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;F)V

    .line 925
    :cond_0
    :goto_0
    return-void

    .line 922
    :cond_1
    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;F)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 966
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 968
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 971
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v1

    .line 973
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->v()V

    .line 976
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    .line 978
    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;)Z

    move-result v3

    .line 979
    if-eqz v3, :cond_2

    iget-boolean v4, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    if-eqz v4, :cond_2

    .line 980
    iput-boolean v2, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    .line 983
    :cond_2
    if-ne v0, v6, :cond_7

    .line 984
    if-eqz v3, :cond_6

    .line 985
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 986
    if-eq v0, v1, :cond_f

    .line 988
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v4

    invoke-static {v0, v1, v3, v4}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    :goto_1
    move-object v1, v0

    .line 1015
    :cond_3
    :goto_2
    invoke-virtual {v1, p1}, Lcom/dropbox/android/widget/ay;->a(Landroid/graphics/Canvas;)V

    .line 1017
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_9

    move v5, v6

    .line 1018
    :goto_3
    if-eqz v5, :cond_a

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:Lcom/dropbox/android/widget/bR;

    move-object v4, v0

    .line 1019
    :goto_4
    if-eqz v5, :cond_b

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->D:Lcom/dropbox/android/widget/bR;

    move-object v3, v0

    .line 1021
    :goto_5
    if-eqz v5, :cond_c

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->y:Landroid/widget/ProgressBar;

    move-object v2, v0

    .line 1022
    :goto_6
    if-eqz v5, :cond_d

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->z:Landroid/widget/ProgressBar;

    .line 1024
    :goto_7
    invoke-virtual {v1, v4, v2, p1}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/bR;Landroid/widget/ProgressBar;Landroid/graphics/Canvas;)V

    .line 1026
    const/4 v1, -0x1

    .line 1027
    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v2, v7, :cond_e

    .line 1028
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v1, v1, 0x1

    .line 1033
    :cond_4
    :goto_8
    if-ltz v1, :cond_5

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 1034
    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v2, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v1

    invoke-virtual {v1, v3, v0, p1}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/bR;Landroid/widget/ProgressBar;Landroid/graphics/Canvas;)V

    .line 1037
    :cond_5
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:Z

    if-eqz v0, :cond_0

    .line 1038
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->o()V

    goto :goto_0

    .line 991
    :cond_6
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    if-lez v0, :cond_3

    .line 992
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 993
    invoke-direct {p0, v1, v6}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;Z)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 994
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v5

    invoke-static {v0, v3, v4, v5}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    .line 995
    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/ay;->a(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 998
    :cond_7
    if-ne v0, v7, :cond_3

    .line 999
    if-eqz v3, :cond_8

    .line 1000
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 1001
    if-eq v0, v1, :cond_3

    .line 1003
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-static {v0}, Lcom/dropbox/android/widget/ay;->b(Lcom/dropbox/android/widget/ay;)F

    move-result v4

    invoke-static {v0, v1, v3, v4}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    move-object v1, v0

    goto/16 :goto_2

    .line 1006
    :cond_8
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    iget-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/aD;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 1007
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Lcom/dropbox/android/widget/aD;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 1008
    invoke-direct {p0, v1, v6}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ay;Z)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    .line 1010
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v5

    invoke-static {v0, v3, v4, v5}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    .line 1011
    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/ay;->a(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    :cond_9
    move v5, v2

    .line 1017
    goto/16 :goto_3

    .line 1018
    :cond_a
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->D:Lcom/dropbox/android/widget/bR;

    move-object v4, v0

    goto/16 :goto_4

    .line 1019
    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:Lcom/dropbox/android/widget/bR;

    move-object v3, v0

    goto/16 :goto_5

    .line 1021
    :cond_c
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->z:Landroid/widget/ProgressBar;

    move-object v2, v0

    goto/16 :goto_6

    .line 1022
    :cond_d
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->y:Landroid/widget/ProgressBar;

    goto/16 :goto_7

    .line 1029
    :cond_e
    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v2, v6, :cond_4

    .line 1030
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:I

    add-int/lit8 v1, v1, -0x1

    goto/16 :goto_8

    :cond_f
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 932
    const v0, 0x7f0700bc

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->s:Landroid/widget/TextView;

    .line 933
    const v0, 0x7f0700bd

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->t:Landroid/widget/TextView;

    .line 935
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->n()V

    .line 936
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 1086
    iput p2, p0, Lcom/dropbox/android/widget/GalleryView;->Z:I

    .line 1087
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->W:I

    .line 1088
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 907
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:Z

    if-eqz v0, :cond_0

    .line 909
    const/4 v0, 0x1

    .line 911
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->I:Ldbxyzptlk/db231222/al/a;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/al/a;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 1078
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    .line 1079
    if-nez p1, :cond_0

    .line 1080
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->p()V

    .line 1082
    :cond_0
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    .prologue
    .line 1092
    if-nez p1, :cond_0

    .line 1093
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->H:Z

    .line 1094
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->u()V

    .line 1095
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->s()V

    .line 1100
    :goto_0
    return-void

    .line 1097
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->H:Z

    .line 1098
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()V

    goto :goto_0
.end method

.method public setAlbum(Lcom/dropbox/android/albums/Album;)V
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Lcom/dropbox/android/activity/bq;

    sget-object v1, Lcom/dropbox/android/activity/bq;->c:Lcom/dropbox/android/activity/bq;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Lcom/dropbox/android/activity/bq;

    sget-object v1, Lcom/dropbox/android/activity/bq;->d:Lcom/dropbox/android/activity/bq;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 492
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->w:Lcom/dropbox/android/albums/Album;

    .line 493
    return-void

    .line 491
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentImagePosScale(FFF)V
    .locals 2

    .prologue
    .line 496
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 497
    if-eqz v0, :cond_0

    .line 498
    invoke-direct {p0, v0, p3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ay;F)F

    move-result v1

    .line 499
    invoke-static {v0, p1, p2, v1}, Lcom/dropbox/android/widget/ay;->a(Lcom/dropbox/android/widget/ay;FFF)Z

    .line 500
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->v()V

    .line 501
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 503
    :cond_0
    return-void
.end method

.method public setThumbnailStore(Lcom/dropbox/android/taskqueue/D;)V
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->h:Lcom/dropbox/android/taskqueue/D;

    .line 353
    return-void
.end method

.method public setTouchListener(Lcom/dropbox/android/widget/ax;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ax;

    .line 507
    return-void
.end method

.method public setType(Lcom/dropbox/android/activity/bq;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->v:Lcom/dropbox/android/activity/bq;

    .line 488
    return-void
.end method
