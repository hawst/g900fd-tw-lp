.class public final Lcom/dropbox/android/widget/ay;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/P;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/GalleryView;

.field private final b:Ldbxyzptlk/db231222/v/n;

.field private final c:Lcom/dropbox/android/filemanager/LocalEntry;

.field private final d:Lcom/dropbox/android/albums/AlbumItemEntry;

.field private e:I

.field private f:I

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:Landroid/graphics/drawable/BitmapDrawable;

.field private q:Z

.field private r:Ljava/lang/CharSequence;

.field private s:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/GalleryView;Lcom/dropbox/android/widget/aC;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1567
    iput-object p1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1561
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    .line 1562
    iput-boolean v1, p0, Lcom/dropbox/android/widget/ay;->q:Z

    .line 1565
    iput-boolean v1, p0, Lcom/dropbox/android/widget/ay;->s:Z

    .line 1568
    iget-object v0, p2, Lcom/dropbox/android/widget/aC;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object v0, p0, Lcom/dropbox/android/widget/ay;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 1569
    iget-object v0, p2, Lcom/dropbox/android/widget/aC;->b:Lcom/dropbox/android/albums/AlbumItemEntry;

    iput-object v0, p0, Lcom/dropbox/android/widget/ay;->d:Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 1570
    iput-object p3, p0, Lcom/dropbox/android/widget/ay;->b:Ldbxyzptlk/db231222/v/n;

    .line 1571
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1582
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    .line 1583
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/GalleryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    .line 1584
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/dropbox/android/widget/ay;->e:I

    .line 1585
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/dropbox/android/widget/ay;->f:I

    .line 1587
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    .line 1588
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1589
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1592
    :cond_0
    iget v0, p0, Lcom/dropbox/android/widget/ay;->k:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1594
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 1595
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->f(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 1596
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v2

    .line 1597
    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(FFF)Z

    .line 1601
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/ay;->a(Ljava/lang/CharSequence;)V

    .line 1602
    return-void

    .line 1599
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/ay;->i:F

    iget v1, p0, Lcom/dropbox/android/widget/ay;->j:F

    iget v2, p0, Lcom/dropbox/android/widget/ay;->k:F

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/android/widget/ay;->a(FFF)Z

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1724
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1725
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1726
    iget v2, p0, Lcom/dropbox/android/widget/ay;->i:F

    float-to-int v2, v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    .line 1727
    iget v3, p0, Lcom/dropbox/android/widget/ay;->j:F

    float-to-int v3, v3

    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    .line 1728
    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1729
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1730
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ay;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1550
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/ay;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ay;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1550
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/ay;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1605
    iput-object p1, p0, Lcom/dropbox/android/widget/ay;->r:Ljava/lang/CharSequence;

    .line 1606
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 1607
    return-void
.end method

.method private a(FFF)Z
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 1681
    iget v0, p0, Lcom/dropbox/android/widget/ay;->e:I

    int-to-float v0, v0

    mul-float/2addr v0, p3

    iput v0, p0, Lcom/dropbox/android/widget/ay;->g:F

    .line 1682
    iget v0, p0, Lcom/dropbox/android/widget/ay;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, p3

    iput v0, p0, Lcom/dropbox/android/widget/ay;->h:F

    .line 1683
    iget v0, p0, Lcom/dropbox/android/widget/ay;->g:F

    mul-float/2addr v0, v2

    .line 1684
    iget v1, p0, Lcom/dropbox/android/widget/ay;->h:F

    mul-float/2addr v1, v2

    .line 1685
    sub-float v2, p1, v0

    iput v2, p0, Lcom/dropbox/android/widget/ay;->l:F

    .line 1686
    sub-float v2, p2, v1

    iput v2, p0, Lcom/dropbox/android/widget/ay;->n:F

    .line 1687
    add-float/2addr v0, p1

    iput v0, p0, Lcom/dropbox/android/widget/ay;->m:F

    .line 1688
    add-float v0, p2, v1

    iput v0, p0, Lcom/dropbox/android/widget/ay;->o:F

    .line 1690
    iput p1, p0, Lcom/dropbox/android/widget/ay;->i:F

    .line 1691
    iput p2, p0, Lcom/dropbox/android/widget/ay;->j:F

    .line 1692
    iput p3, p0, Lcom/dropbox/android/widget/ay;->k:F

    .line 1694
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ay;FFF)Z
    .locals 1

    .prologue
    .line 1550
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/widget/ay;->a(FFF)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ay;Z)Z
    .locals 0

    .prologue
    .line 1550
    iput-boolean p1, p0, Lcom/dropbox/android/widget/ay;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->k:F

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/ay;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1550
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/ay;->b(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1811
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->k(Lcom/dropbox/android/widget/GalleryView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/widget/az;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/az;-><init>(Lcom/dropbox/android/widget/ay;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1819
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->i:F

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->j:F

    return v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->l:F

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->m:F

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/ay;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->g:F

    return v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->h:F

    return v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->n:F

    return v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/ay;)F
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->o:F

    return v0
.end method

.method static synthetic l(Lcom/dropbox/android/widget/ay;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method static synthetic m(Lcom/dropbox/android/widget/ay;)Z
    .locals 1

    .prologue
    .line 1550
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ay;->q:Z

    return v0
.end method

.method static synthetic n(Lcom/dropbox/android/widget/ay;)I
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->e:I

    return v0
.end method

.method static synthetic o(Lcom/dropbox/android/widget/ay;)I
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/dropbox/android/widget/ay;->f:I

    return v0
.end method

.method static synthetic p(Lcom/dropbox/android/widget/ay;)Z
    .locals 1

    .prologue
    .line 1550
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ay;->s:Z

    return v0
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 1574
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1798
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 1799
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    iget v1, p0, Lcom/dropbox/android/widget/ay;->l:F

    float-to-int v1, v1

    iget v2, p0, Lcom/dropbox/android/widget/ay;->n:F

    float-to-int v2, v2

    iget v3, p0, Lcom/dropbox/android/widget/ay;->m:F

    float-to-int v3, v3

    iget v4, p0, Lcom/dropbox/android/widget/ay;->o:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 1801
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1808
    :cond_0
    :goto_0
    return-void

    .line 1802
    :catch_0
    move-exception v0

    .line 1804
    invoke-static {}, Lcom/dropbox/android/widget/GalleryView;->k()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Bad drawable state in gallery"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V
    .locals 2

    .prologue
    .line 1858
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->b:Ldbxyzptlk/db231222/v/n;

    if-eq p2, v0, :cond_0

    .line 1871
    :goto_0
    return-void

    .line 1863
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->k(Lcom/dropbox/android/widget/GalleryView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/widget/aB;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/aB;-><init>(Lcom/dropbox/android/widget/ay;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 5

    .prologue
    .line 1824
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->b:Ldbxyzptlk/db231222/v/n;

    if-eq p3, v0, :cond_0

    .line 1854
    :goto_0
    return-void

    .line 1831
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->g(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/widget/ay;->b:Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Landroid/util/Pair;

    move-result-object v0

    .line 1833
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->k(Lcom/dropbox/android/widget/GalleryView;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/dropbox/android/widget/aA;

    invoke-direct {v2, p0, v0, p1}, Lcom/dropbox/android/widget/aA;-><init>(Lcom/dropbox/android/widget/ay;Landroid/util/Pair;Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/widget/ay;)V
    .locals 1

    .prologue
    .line 1699
    iget v0, p1, Lcom/dropbox/android/widget/ay;->g:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->g:F

    .line 1700
    iget v0, p1, Lcom/dropbox/android/widget/ay;->h:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->h:F

    .line 1701
    iget v0, p1, Lcom/dropbox/android/widget/ay;->l:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->l:F

    .line 1702
    iget v0, p1, Lcom/dropbox/android/widget/ay;->n:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->n:F

    .line 1703
    iget v0, p1, Lcom/dropbox/android/widget/ay;->m:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->m:F

    .line 1704
    iget v0, p1, Lcom/dropbox/android/widget/ay;->o:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->o:F

    .line 1705
    iget v0, p1, Lcom/dropbox/android/widget/ay;->i:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->i:F

    .line 1706
    iget v0, p1, Lcom/dropbox/android/widget/ay;->j:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->j:F

    .line 1707
    iget v0, p1, Lcom/dropbox/android/widget/ay;->k:F

    iput v0, p0, Lcom/dropbox/android/widget/ay;->k:F

    .line 1708
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/bR;Landroid/widget/ProgressBar;Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1733
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 1734
    invoke-static {p2}, Lcom/dropbox/android/widget/GalleryView;->a(Landroid/widget/ProgressBar;)V

    .line 1738
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->h(Lcom/dropbox/android/widget/GalleryView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/dropbox/android/widget/ay;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;)V

    .line 1780
    :cond_0
    :goto_0
    return-void

    .line 1743
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->r:Ljava/lang/CharSequence;

    .line 1745
    if-eqz v0, :cond_3

    .line 1746
    invoke-static {p2}, Lcom/dropbox/android/widget/GalleryView;->a(Landroid/widget/ProgressBar;)V

    .line 1747
    invoke-virtual {p1, v0}, Lcom/dropbox/android/widget/bR;->a(Ljava/lang/CharSequence;)V

    .line 1749
    invoke-virtual {p1}, Lcom/dropbox/android/widget/bR;->b()I

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 1751
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v0

    invoke-virtual {p1, v3, v3, v0, v3}, Lcom/dropbox/android/widget/bR;->setBounds(IIII)V

    .line 1754
    :cond_2
    invoke-virtual {p1}, Lcom/dropbox/android/widget/bR;->c()I

    move-result v0

    .line 1755
    iget v1, p0, Lcom/dropbox/android/widget/ay;->i:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v2}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 1756
    iget-object v2, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v2}, Lcom/dropbox/android/widget/GalleryView;->f(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v2

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 1758
    iget-object v2, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v2}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p1, v1, v0, v2, v0}, Lcom/dropbox/android/widget/bR;->setBounds(IIII)V

    .line 1759
    invoke-virtual {p1, p3}, Lcom/dropbox/android/widget/bR;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1761
    :cond_3
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ay;->q:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_6

    .line 1762
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->i(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 1763
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1764
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-static {v1, v2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/GalleryView;I)I

    .line 1765
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {v1, v0}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/GalleryView;I)I

    .line 1768
    :cond_5
    iget v0, p0, Lcom/dropbox/android/widget/ay;->i:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->i(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 1769
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->f(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v0

    iget-object v2, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v2}, Lcom/dropbox/android/widget/GalleryView;->j(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v2, v0, 0x2

    .line 1771
    invoke-static {p2}, Lcom/dropbox/android/widget/GalleryView;->b(Landroid/widget/ProgressBar;)V

    .line 1772
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1773
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1774
    invoke-virtual {p2, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1776
    :cond_6
    invoke-static {p2}, Lcom/dropbox/android/widget/GalleryView;->a(Landroid/widget/ProgressBar;)V

    goto/16 :goto_0
.end method

.method public final a(FF)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1786
    iget-object v2, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_2

    .line 1788
    iget v2, p0, Lcom/dropbox/android/widget/ay;->l:F

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/dropbox/android/widget/ay;->m:F

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lcom/dropbox/android/widget/ay;->n:F

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/dropbox/android/widget/ay;->o:F

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_1

    .line 1793
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1788
    goto :goto_0

    .line 1791
    :cond_2
    iget v2, p0, Lcom/dropbox/android/widget/ay;->i:F

    iget-object v3, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v3}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 1792
    iget v3, p0, Lcom/dropbox/android/widget/ay;->i:F

    iget-object v4, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v4}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    .line 1793
    cmpl-float v2, p1, v2

    if-ltz v2, :cond_3

    cmpg-float v2, p1, v3

    if-lez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final a(II)Z
    .locals 5

    .prologue
    .line 1711
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->h(Lcom/dropbox/android/widget/GalleryView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1713
    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->h(Lcom/dropbox/android/widget/GalleryView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1714
    iget v2, p0, Lcom/dropbox/android/widget/ay;->i:F

    float-to-int v2, v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    .line 1715
    iget v3, p0, Lcom/dropbox/android/widget/ay;->j:F

    float-to-int v3, v3

    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    .line 1716
    if-ge v2, p1, :cond_0

    add-int/2addr v0, v2

    if-ge p1, v0, :cond_0

    if-ge v3, p2, :cond_0

    add-int v0, v3, v1

    if-ge p2, v0, :cond_0

    .line 1717
    const/4 v0, 0x1

    .line 1720
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/android/albums/AlbumItemEntry;
    .locals 1

    .prologue
    .line 1578
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->d:Lcom/dropbox/android/albums/AlbumItemEntry;

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->g(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/widget/ay;->b:Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 1611
    return-void
.end method

.method public final d()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1618
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ay;->s:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 1619
    iput-boolean v5, p0, Lcom/dropbox/android/widget/ay;->s:Z

    .line 1620
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->g(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/ref/WeakReference;)V

    .line 1621
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->g(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/widget/ay;->b:Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Landroid/util/Pair;

    move-result-object v1

    .line 1623
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/taskqueue/O;

    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/O;->b:Z

    if-eqz v0, :cond_1

    .line 1624
    iput-boolean v5, p0, Lcom/dropbox/android/widget/ay;->q:Z

    .line 1632
    :cond_0
    :goto_0
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/taskqueue/O;

    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/O;->b:Z

    return v0

    .line 1626
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ay;->q:Z

    .line 1627
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/taskqueue/O;

    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/O;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1628
    invoke-static {}, Lcom/dropbox/android/widget/GalleryView;->k()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setting bmp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1629
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/ay;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1641
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ay;->s:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 1642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ay;->s:Z

    .line 1643
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->g(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V

    .line 1645
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 1646
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1647
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1648
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/ay;->p:Landroid/graphics/drawable/BitmapDrawable;

    .line 1650
    :cond_0
    return-void
.end method

.method public final f()F
    .locals 3

    .prologue
    .line 1653
    iget v0, p0, Lcom/dropbox/android/widget/ay;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/ay;->f:I

    if-nez v0, :cond_1

    .line 1654
    :cond_0
    const/4 v0, 0x0

    .line 1658
    :goto_0
    return v0

    .line 1657
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->e(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/dropbox/android/widget/ay;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/dropbox/android/widget/ay;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->f(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/dropbox/android/widget/ay;->f:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1658
    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 1666
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ay;->f()F

    move-result v0

    .line 1667
    iget v1, p0, Lcom/dropbox/android/widget/ay;->k:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    iget v1, p0, Lcom/dropbox/android/widget/ay;->k:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x358637bd    # 1.0E-6f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
