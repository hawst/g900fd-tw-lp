.class final Lcom/dropbox/android/widget/bH;
.super Landroid/widget/BaseAdapter;
.source "panda.py"

# interfaces
.implements Landroid/widget/ListAdapter;
.implements Lcom/dropbox/android/widget/bF;


# instance fields
.field protected final a:Landroid/database/DataSetObservable;

.field protected final b:Landroid/database/DataSetObserver;

.field private final c:Lcom/dropbox/android/widget/bC;

.field private final d:Landroid/content/Context;

.field private e:Landroid/database/Cursor;

.field private f:[I

.field private g:[I

.field private h:Landroid/widget/AdapterView$OnItemClickListener;

.field private i:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private j:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/bC;)V
    .locals 1

    .prologue
    .line 1280
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1260
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->a:Landroid/database/DataSetObservable;

    .line 1261
    new-instance v0, Lcom/dropbox/android/widget/bI;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bI;-><init>(Lcom/dropbox/android/widget/bH;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->b:Landroid/database/DataSetObserver;

    .line 1272
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->f:[I

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    .line 1273
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->f:[I

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    .line 1278
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/bH;->j:I

    .line 1281
    iput-object p1, p0, Lcom/dropbox/android/widget/bH;->c:Lcom/dropbox/android/widget/bC;

    .line 1282
    invoke-virtual {p1}, Lcom/dropbox/android/widget/bC;->f()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->d:Landroid/content/Context;

    .line 1283
    invoke-virtual {p1, p0}, Lcom/dropbox/android/widget/bC;->a(Lcom/dropbox/android/widget/bF;)V

    .line 1284
    invoke-virtual {p1}, Lcom/dropbox/android/widget/bC;->g()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/bH;->a(Landroid/database/Cursor;)V

    .line 1285
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/bH;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->h:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/bH;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->i:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private d(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1509
    if-nez p1, :cond_0

    move v2, v0

    .line 1510
    :goto_0
    if-nez v2, :cond_2

    .line 1511
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 1512
    iget-object v3, p0, Lcom/dropbox/android/widget/bH;->f:[I

    add-int/lit8 v4, p1, -0x1

    aget v3, v3, v4

    .line 1513
    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v4, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1514
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-virtual {v3, v4}, Lcom/dropbox/android/widget/bC;->e(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1515
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1517
    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 1509
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1514
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1331
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 1332
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->f:[I

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    .line 1333
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->f:[I

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    .line 1372
    :goto_0
    return-void

    .line 1335
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1336
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1339
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v1

    .line 1340
    :goto_1
    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1341
    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->c:Lcom/dropbox/android/widget/bC;

    iget-object v5, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-virtual {v4, v5}, Lcom/dropbox/android/widget/bC;->e(Landroid/database/Cursor;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1344
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1345
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->c:Lcom/dropbox/android/widget/bC;

    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->c:Lcom/dropbox/android/widget/bC;

    iget-object v5, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-virtual {v4, v5}, Lcom/dropbox/android/widget/bC;->b(Landroid/database/Cursor;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/dropbox/android/widget/bC;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1346
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1356
    :goto_2
    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 1349
    :cond_1
    iget v4, p0, Lcom/dropbox/android/widget/bH;->j:I

    rem-int v4, v0, v4

    if-nez v4, :cond_2

    .line 1351
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1353
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1360
    :cond_3
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->g:[Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    invoke-static {v0}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Integer;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    .line 1362
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->g:[Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    invoke-static {v0}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Integer;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    .line 1366
    invoke-static {}, Lcom/dropbox/android/widget/SweetListView;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "determineFauxCount reset mFauxToInnerCursorPos, new length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private e(I)Z
    .locals 3

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1522
    const/4 v0, 0x0

    .line 1529
    :goto_0
    return v0

    .line 1524
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 1525
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    add-int/lit8 v2, p1, 0x1

    aget v0, v0, v2

    .line 1526
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1527
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/bC;->c(Landroid/database/Cursor;)Z

    move-result v0

    .line 1528
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0
.end method

.method private f(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1533
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_0

    move v2, v0

    .line 1534
    :goto_0
    if-nez v2, :cond_2

    .line 1535
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 1536
    iget-object v3, p0, Lcom/dropbox/android/widget/bH;->f:[I

    add-int/lit8 v4, p1, 0x1

    aget v3, v3, v4

    .line 1537
    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v4, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1538
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-virtual {v3, v4}, Lcom/dropbox/android/widget/bC;->e(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1539
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1541
    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 1533
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1538
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/widget/bC;
    .locals 1

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->c:Lcom/dropbox/android/widget/bC;

    return-object v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 1555
    int-to-double v0, p1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bC;->c()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1557
    iget v1, p0, Lcom/dropbox/android/widget/bH;->j:I

    if-eq v1, v0, :cond_0

    .line 1558
    iput v0, p0, Lcom/dropbox/android/widget/bH;->j:I

    .line 1559
    invoke-direct {p0}, Lcom/dropbox/android/widget/bH;->e()V

    .line 1560
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 1562
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-ne p1, v0, :cond_0

    .line 1320
    :goto_0
    return-void

    .line 1307
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 1308
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->b:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1311
    :cond_1
    iput-object p1, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    .line 1312
    invoke-direct {p0}, Lcom/dropbox/android/widget/bH;->e()V

    .line 1314
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 1315
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->b:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1316
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    goto :goto_0

    .line 1318
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/View;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 1579
    .line 1580
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/dropbox/android/widget/bC;->a(Landroid/database/Cursor;)Z

    move-result v2

    .line 1581
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 1582
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 1583
    new-instance v0, Lcom/dropbox/android/widget/bJ;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/bJ;-><init>(Lcom/dropbox/android/widget/bH;ZLandroid/view/View;IJ)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1591
    new-instance v0, Lcom/dropbox/android/widget/bK;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/bK;-><init>(Lcom/dropbox/android/widget/bH;ZLandroid/view/View;IJ)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1601
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 1565
    iput-object p1, p0, Lcom/dropbox/android/widget/bH;->h:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1566
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0

    .prologue
    .line 1569
    iput-object p1, p0, Lcom/dropbox/android/widget/bH;->i:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 1570
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/bO;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1392
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1393
    invoke-virtual {p1}, Lcom/dropbox/android/widget/bO;->a()V

    .line 1411
    :goto_0
    return-void

    .line 1396
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    invoke-static {v0, p2}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 1403
    if-ltz v0, :cond_2

    move v4, v0

    .line 1404
    :goto_1
    if-lt v4, v3, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 1405
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    array-length v0, v0

    if-ge v4, v0, :cond_4

    :goto_3
    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 1407
    if-ltz v4, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->g:[I

    aget v0, v0, v4

    :goto_4
    add-int/lit8 v1, v4, 0x1

    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->g:[I

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->g:[I

    add-int/lit8 v2, v4, 0x1

    aget v3, v1, v2

    :cond_1
    invoke-virtual {p1, v0, v3}, Lcom/dropbox/android/widget/bO;->a(II)V

    goto :goto_0

    .line 1403
    :cond_2
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x2

    move v4, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 1404
    goto :goto_2

    :cond_4
    move v1, v2

    .line 1405
    goto :goto_3

    :cond_5
    move v0, v3

    .line 1407
    goto :goto_4
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 1505
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bH;->getItemViewType(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bC;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1476
    const/4 v0, 0x1

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1605
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public final b()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    return-object v0
.end method

.method public final c(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1611
    move v1, v0

    .line 1612
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1613
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->f:[I

    aget v2, v2, v0

    if-le v2, p1, :cond_1

    .line 1619
    :cond_0
    return v1

    .line 1612
    :cond_1
    add-int/lit8 v1, v0, 0x1

    move v3, v1

    move v1, v0

    move v0, v3

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1327
    iget v0, p0, Lcom/dropbox/android/widget/bH;->j:I

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1415
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 1425
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->f:[I

    aget v1, v1, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1426
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    .line 1428
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 3

    .prologue
    .line 1438
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 1439
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->f:[I

    aget v1, v1, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1440
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1442
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1492
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 1493
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->f:[I

    aget v0, v0, p1

    .line 1494
    iget-object v2, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1496
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v2

    .line 1497
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    invoke-virtual {v2, v0}, Lcom/dropbox/android/widget/bC;->b(Landroid/database/Cursor;)I

    move-result v0

    .line 1498
    if-ne v0, v1, :cond_0

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bC;->a()I

    move-result v0

    .line 1500
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 1546
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bH;->d(I)Z

    move-result v5

    .line 1547
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bH;->f(I)Z

    move-result v6

    .line 1548
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->f:[I

    aget v1, v1, p1

    move-object v3, p3

    check-cast v3, Lcom/dropbox/android/widget/SweetListView;

    iget v4, p0, Lcom/dropbox/android/widget/bH;->j:I

    if-eqz v6, :cond_0

    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bH;->e(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v7, 0x1

    :goto_0
    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/dropbox/android/widget/bC;->a(ILandroid/view/View;Lcom/dropbox/android/widget/SweetListView;IZZZ)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1487
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bH;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bC;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1448
    const/4 v0, 0x1

    return v0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1455
    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->e:Landroid/database/Cursor;

    if-nez v1, :cond_1

    .line 1459
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/widget/bH;->f:[I

    array-length v1, v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1481
    const/4 v0, 0x1

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 1465
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/dropbox/android/widget/bH;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 1470
    return-void
.end method
