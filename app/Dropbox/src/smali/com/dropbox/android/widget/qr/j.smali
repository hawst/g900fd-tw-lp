.class final Lcom/dropbox/android/widget/qr/j;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/widget/qr/QrReaderView;

.field private final b:Lcom/dropbox/android/widget/qr/d;

.field private c:Lcom/dropbox/android/widget/qr/h;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/qr/QrReaderView;Lcom/dropbox/android/widget/qr/d;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/j;->a:Lcom/dropbox/android/widget/qr/QrReaderView;

    .line 40
    iput-object p2, p0, Lcom/dropbox/android/widget/qr/j;->b:Lcom/dropbox/android/widget/qr/d;

    .line 41
    return-void
.end method

.method private declared-synchronized c()V
    .locals 3

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->b:Lcom/dropbox/android/widget/qr/d;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/qr/h;->a()Lcom/dropbox/android/widget/qr/g;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/qr/d;->a(Landroid/os/Handler;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_0
    monitor-exit p0

    return-void

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 82
    :goto_0
    monitor-exit p0

    return-void

    .line 78
    :cond_0
    :try_start_1
    new-instance v0, Lcom/dropbox/android/widget/qr/h;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/j;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-direct {v0, v1, p0}, Lcom/dropbox/android/widget/qr/h;-><init>(Lcom/dropbox/android/widget/qr/d;Lcom/dropbox/android/widget/qr/j;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/h;->start()V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/d;->c()V

    .line 81
    invoke-direct {p0}, Lcom/dropbox/android/widget/qr/j;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 106
    :goto_0
    monitor-exit p0

    return-void

    .line 94
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/d;->d()V

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/h;->a()Lcom/dropbox/android/widget/qr/g;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/qr/h;->join(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 105
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/dropbox/android/widget/qr/j;->c:Lcom/dropbox/android/widget/qr/h;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 45
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown QrReaderHandler message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ldbxyzptlk/db231222/I/k;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/I/k;

    .line 49
    invoke-virtual {v0}, Ldbxyzptlk/db231222/I/k;->a()Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/j;->a:Lcom/dropbox/android/widget/qr/QrReaderView;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/dropbox/android/widget/qr/j;->c()V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 61
    :pswitch_1
    invoke-direct {p0}, Lcom/dropbox/android/widget/qr/j;->c()V

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
