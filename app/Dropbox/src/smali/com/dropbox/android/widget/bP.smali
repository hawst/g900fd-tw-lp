.class final Lcom/dropbox/android/widget/bP;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/widget/bO;

.field private final b:Landroid/database/DataSetObserver;

.field private c:Lcom/dropbox/android/widget/bG;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroid/graphics/Rect;

.field private o:Landroid/graphics/RectF;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907
    new-instance v0, Lcom/dropbox/android/widget/bO;

    invoke-direct {v0}, Lcom/dropbox/android/widget/bO;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->a:Lcom/dropbox/android/widget/bO;

    .line 908
    new-instance v0, Lcom/dropbox/android/widget/bQ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bQ;-><init>(Lcom/dropbox/android/widget/bP;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->b:Landroid/database/DataSetObserver;

    .line 918
    iput-object v2, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    .line 920
    iput-boolean v1, p0, Lcom/dropbox/android/widget/bP;->d:Z

    .line 921
    iput-boolean v1, p0, Lcom/dropbox/android/widget/bP;->e:Z

    .line 923
    iput v3, p0, Lcom/dropbox/android/widget/bP;->f:I

    .line 924
    iput-object v2, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    .line 926
    iput-object v2, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    .line 927
    iput v3, p0, Lcom/dropbox/android/widget/bP;->i:I

    .line 929
    iput v1, p0, Lcom/dropbox/android/widget/bP;->j:I

    .line 930
    iput v1, p0, Lcom/dropbox/android/widget/bP;->k:I

    .line 931
    iput v1, p0, Lcom/dropbox/android/widget/bP;->l:I

    .line 932
    const/16 v0, 0xff

    iput v0, p0, Lcom/dropbox/android/widget/bP;->m:I

    .line 935
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->n:Landroid/graphics/Rect;

    .line 936
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->o:Landroid/graphics/RectF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/widget/bD;)V
    .locals 0

    .prologue
    .line 905
    invoke-direct {p0}, Lcom/dropbox/android/widget/bP;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/bP;)Landroid/database/DataSetObserver;
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->b:Landroid/database/DataSetObserver;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/bP;Lcom/dropbox/android/widget/bG;)Lcom/dropbox/android/widget/bG;
    .locals 0

    .prologue
    .line 905
    iput-object p1, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    return-object p1
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 1010
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1011
    invoke-static {p1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1014
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1015
    if-eqz v0, :cond_1

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v2, :cond_1

    .line 1016
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1022
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 1023
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/bP;->l:I

    .line 1024
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    iget v1, p0, Lcom/dropbox/android/widget/bP;->l:I

    invoke-virtual {v0, v3, v3, p1, v1}, Landroid/view/View;->layout(IIII)V

    .line 1026
    :cond_0
    return-void

    .line 1019
    :cond_1
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(IILcom/dropbox/android/widget/SweetListView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1042
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    iget-boolean v0, v0, Lcom/dropbox/android/widget/bG;->b:Z

    if-eqz v0, :cond_0

    .line 1045
    if-ne p1, p2, :cond_3

    .line 1046
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1047
    iget v0, p0, Lcom/dropbox/android/widget/bP;->i:I

    if-ne v0, p1, :cond_1

    .line 1065
    :cond_0
    :goto_0
    return-void

    .line 1053
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1057
    :cond_2
    invoke-virtual {p3, v1}, Lcom/dropbox/android/widget/SweetListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1058
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1059
    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    .line 1060
    iput p1, p0, Lcom/dropbox/android/widget/bP;->i:I

    goto :goto_0

    .line 1062
    :cond_3
    invoke-direct {p0}, Lcom/dropbox/android/widget/bP;->d()V

    goto :goto_0
.end method

.method private a(IILcom/dropbox/android/widget/SweetListView;I)V
    .locals 2

    .prologue
    .line 1031
    iget v0, p0, Lcom/dropbox/android/widget/bP;->f:I

    if-eq p1, v0, :cond_0

    .line 1032
    invoke-static {p3}, Lcom/dropbox/android/widget/SweetListView;->a(Lcom/dropbox/android/widget/SweetListView;)Lcom/dropbox/android/widget/bH;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    invoke-virtual {v0, p1, v1, p3}, Lcom/dropbox/android/widget/bH;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    .line 1034
    iput p1, p0, Lcom/dropbox/android/widget/bP;->f:I

    .line 1035
    invoke-direct {p0, p4}, Lcom/dropbox/android/widget/bP;->a(I)V

    .line 1037
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/widget/bP;->a(IILcom/dropbox/android/widget/SweetListView;)V

    .line 1038
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/bP;)Lcom/dropbox/android/widget/bO;
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->a:Lcom/dropbox/android/widget/bO;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    iget-boolean v0, v0, Lcom/dropbox/android/widget/bG;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1070
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/bP;->h:Landroid/view/View;

    .line 1071
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/bP;->i:I

    .line 1073
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 995
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bP;->d:Z

    if-eqz v0, :cond_1

    .line 996
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 997
    iget-boolean v1, p0, Lcom/dropbox/android/widget/bP;->e:Z

    if-eqz v1, :cond_0

    .line 998
    iget v1, p0, Lcom/dropbox/android/widget/bP;->j:I

    int-to-float v1, v1

    invoke-virtual {p1, v4, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 999
    iget-object v1, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    iget-boolean v1, v1, Lcom/dropbox/android/widget/bG;->c:Z

    if-eqz v1, :cond_0

    .line 1000
    iget-object v1, p0, Lcom/dropbox/android/widget/bP;->o:Landroid/graphics/RectF;

    int-to-float v2, p2

    iget v3, p0, Lcom/dropbox/android/widget/bP;->l:I

    int-to-float v3, v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1001
    iget-object v1, p0, Lcom/dropbox/android/widget/bP;->o:Landroid/graphics/RectF;

    iget v2, p0, Lcom/dropbox/android/widget/bP;->m:I

    const/16 v3, 0x1f

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    .line 1004
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/widget/bP;->g:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1005
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1007
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;II)V
    .locals 3

    .prologue
    .line 988
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    iget-boolean v0, v0, Lcom/dropbox/android/widget/bG;->b:Z

    if-nez v0, :cond_0

    .line 989
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->n:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iget v2, p0, Lcom/dropbox/android/widget/bP;->k:I

    invoke-virtual {v0, v1, v2, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 990
    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->n:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 992
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/bO;ILcom/dropbox/android/widget/SweetListView;I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 944
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bP;->d:Z

    .line 946
    iget v2, p1, Lcom/dropbox/android/widget/bO;->a:I

    invoke-direct {p0, v2, p2, p3, p4}, Lcom/dropbox/android/widget/bP;->a(IILcom/dropbox/android/widget/SweetListView;I)V

    .line 950
    iget v2, p1, Lcom/dropbox/android/widget/bO;->b:I

    if-lt v2, v0, :cond_1

    iget v2, p1, Lcom/dropbox/android/widget/bO;->b:I

    add-int/lit8 v2, v2, -0x1

    if-ne v2, p2, :cond_1

    .line 954
    invoke-virtual {p3, v1}, Lcom/dropbox/android/widget/SweetListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    iput v2, p0, Lcom/dropbox/android/widget/bP;->k:I

    .line 955
    iget v2, p0, Lcom/dropbox/android/widget/bP;->k:I

    iget v3, p0, Lcom/dropbox/android/widget/bP;->l:I

    if-ge v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bP;->e:Z

    .line 961
    :goto_1
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bP;->e:Z

    if-nez v0, :cond_2

    .line 962
    iput v1, p0, Lcom/dropbox/android/widget/bP;->j:I

    .line 963
    iget v0, p0, Lcom/dropbox/android/widget/bP;->l:I

    iput v0, p0, Lcom/dropbox/android/widget/bP;->k:I

    .line 964
    const/16 v0, 0xff

    iput v0, p0, Lcom/dropbox/android/widget/bP;->m:I

    .line 972
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 955
    goto :goto_0

    .line 957
    :cond_1
    iput-boolean v1, p0, Lcom/dropbox/android/widget/bP;->e:Z

    goto :goto_1

    .line 970
    :cond_2
    iget v0, p0, Lcom/dropbox/android/widget/bP;->k:I

    iget v1, p0, Lcom/dropbox/android/widget/bP;->l:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/dropbox/android/widget/bP;->j:I

    .line 971
    iget v0, p0, Lcom/dropbox/android/widget/bP;->k:I

    mul-int/lit16 v0, v0, 0xff

    iget v1, p0, Lcom/dropbox/android/widget/bP;->l:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/dropbox/android/widget/bP;->m:I

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 939
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bP;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bP;->c:Lcom/dropbox/android/widget/bG;

    iget-boolean v0, v0, Lcom/dropbox/android/widget/bG;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 975
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/bP;->d:Z

    .line 976
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/bP;->f:I

    .line 977
    invoke-direct {p0}, Lcom/dropbox/android/widget/bP;->d()V

    .line 978
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 983
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/bP;->f:I

    .line 984
    return-void
.end method
