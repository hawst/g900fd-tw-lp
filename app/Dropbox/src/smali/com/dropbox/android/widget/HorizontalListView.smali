.class public Lcom/dropbox/android/widget/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field private A:I

.field private B:Z

.field private C:Z

.field private D:Landroid/view/View$OnClickListener;

.field private E:Landroid/database/DataSetObserver;

.field private F:Ljava/lang/Runnable;

.field protected a:Landroid/widget/Scroller;

.field protected b:Landroid/widget/ListAdapter;

.field protected c:I

.field protected d:I

.field private final e:Lcom/dropbox/android/widget/aL;

.field private f:Landroid/view/GestureDetector;

.field private g:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Landroid/graphics/Rect;

.field private k:Landroid/view/View;

.field private l:I

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Ljava/lang/Integer;

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Lcom/dropbox/android/widget/aP;

.field private t:I

.field private u:Z

.field private v:Lcom/dropbox/android/widget/aN;

.field private w:Lcom/dropbox/android/widget/aO;

.field private x:Lcom/dropbox/android/widget/aM;

.field private y:Landroid/support/v4/widget/l;

.field private z:Landroid/support/v4/widget/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 230
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    .line 106
    new-instance v0, Lcom/dropbox/android/widget/aL;

    invoke-direct {v0, p0, v3}, Lcom/dropbox/android/widget/aL;-><init>(Lcom/dropbox/android/widget/HorizontalListView;Lcom/dropbox/android/widget/aI;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->e:Lcom/dropbox/android/widget/aL;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->h:Ljava/util/List;

    .line 121
    iput-boolean v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->i:Z

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    .line 127
    iput-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    .line 130
    iput v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    .line 133
    iput-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->m:Landroid/graphics/drawable/Drawable;

    .line 142
    iput-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->n:Ljava/lang/Integer;

    .line 145
    const v0, 0x7fffffff

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    .line 159
    iput-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->s:Lcom/dropbox/android/widget/aP;

    .line 164
    iput v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->t:I

    .line 169
    iput-boolean v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->u:Z

    .line 174
    iput-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->v:Lcom/dropbox/android/widget/aN;

    .line 179
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->w:Lcom/dropbox/android/widget/aO;

    .line 181
    iput-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->x:Lcom/dropbox/android/widget/aM;

    .line 197
    iput-boolean v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->B:Z

    .line 200
    iput-boolean v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->C:Z

    .line 381
    new-instance v0, Lcom/dropbox/android/widget/aJ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aJ;-><init>(Lcom/dropbox/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->E:Landroid/database/DataSetObserver;

    .line 651
    new-instance v0, Lcom/dropbox/android/widget/aK;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aK;-><init>(Lcom/dropbox/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->F:Ljava/lang/Runnable;

    .line 231
    new-instance v0, Landroid/support/v4/widget/l;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    .line 232
    new-instance v0, Landroid/support/v4/widget/l;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    .line 233
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->e:Lcom/dropbox/android/widget/aL;

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->f:Landroid/view/GestureDetector;

    .line 234
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->b()V

    .line 235
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->c()V

    .line 236
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 237
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/HorizontalListView;->setWillNotDraw(Z)V

    .line 238
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    const v1, 0x3c1374bc    # 0.009f

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFriction(F)V

    .line 239
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/HorizontalListView;II)I
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/HorizontalListView;->c(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/HorizontalListView;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->f:Landroid/view/GestureDetector;

    return-object v0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 757
    :goto_0
    add-int v0, p1, p2

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 758
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    .line 761
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    if-gez v0, :cond_0

    .line 762
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/HorizontalListView;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 767
    const/4 v0, -0x1

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/view/View;I)V

    .line 770
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr p1, v0

    .line 773
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->l()V

    goto :goto_0

    .line 770
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    goto :goto_1

    .line 775
    :cond_2
    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 483
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 486
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 287
    if-eqz p2, :cond_2

    .line 288
    sget-object v0, Lcom/dropbox/android/j;->HorizontalListView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 291
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 292
    if-eqz v1, :cond_0

    .line 294
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 298
    :cond_0
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 299
    if-eqz v1, :cond_1

    .line 300
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->setDividerWidth(I)V

    .line 303
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 305
    :cond_2
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 885
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 887
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 888
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getHeight()I

    move-result v1

    .line 890
    const/high16 v2, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v2, v3, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 891
    neg-int v1, v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 893
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->h()I

    move-result v2

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->i()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/widget/l;->a(II)V

    .line 894
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/l;->a(Landroid/graphics/Canvas;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 895
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->invalidate()V

    .line 898
    :cond_0
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 913
    :cond_1
    :goto_0
    return-void

    .line 899
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 901
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 902
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getWidth()I

    move-result v1

    .line 904
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {p1, v2, v3, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 905
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 906
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->h()I

    move-result v2

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->i()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/widget/l;->a(II)V

    .line 907
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/l;->a(Landroid/graphics/Canvas;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 908
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->invalidate()V

    .line 911
    :cond_3
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 965
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 966
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 968
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 505
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->b(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 506
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->A:I

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 509
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v2, :cond_0

    .line 510
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 515
    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 516
    return-void

    .line 512
    :cond_0
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->b(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 495
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 496
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/view/View;)V

    .line 497
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/HorizontalListView;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->j(I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/HorizontalListView;Lcom/dropbox/android/widget/aO;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/HorizontalListView;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/widget/aO;)V
    .locals 1

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->w:Lcom/dropbox/android/widget/aO;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->v:Lcom/dropbox/android/widget/aN;

    if-eqz v0, :cond_0

    .line 1245
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->v:Lcom/dropbox/android/widget/aN;

    invoke-interface {v0, p1}, Lcom/dropbox/android/widget/aN;->a(Lcom/dropbox/android/widget/aO;)V

    .line 1248
    :cond_0
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->w:Lcom/dropbox/android/widget/aO;

    .line 1249
    return-void
.end method

.method private a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->C:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_1

    move-object v0, p0

    .line 267
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 269
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/ListView;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/ScrollView;

    if-eqz v1, :cond_2

    .line 270
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 271
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->C:Z

    .line 278
    :cond_1
    return-void

    .line 275
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/HorizontalListView;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->i:Z

    return p1
.end method

.method private b(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 520
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 521
    if-nez v0, :cond_0

    .line 523
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 526
    :cond_0
    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 244
    new-instance v0, Lcom/dropbox/android/widget/aI;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aI;-><init>(Lcom/dropbox/android/widget/HorizontalListView;)V

    .line 252
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 253
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 451
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->h:Ljava/util/List;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 454
    :cond_0
    return-void
.end method

.method private b(II)V
    .locals 3

    .prologue
    .line 779
    :goto_0
    add-int v0, p1, p2

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    .line 780
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    .line 781
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/HorizontalListView;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 782
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/view/View;I)V

    .line 785
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_1
    sub-int/2addr p1, v0

    .line 788
    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    add-int v0, p1, p2

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_2
    sub-int v0, v2, v0

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    goto :goto_0

    .line 785
    :cond_0
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    .line 788
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    .line 790
    :cond_2
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 917
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    .line 920
    iget-object v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    .line 921
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 922
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->h()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 925
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_4

    .line 927
    add-int/lit8 v3, v1, -0x1

    if-ne v0, v3, :cond_0

    iget v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    invoke-direct {p0, v3}, Lcom/dropbox/android/widget/HorizontalListView;->i(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 928
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 930
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    iput v4, v2, Landroid/graphics/Rect;->left:I

    .line 931
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    add-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->right:I

    .line 934
    iget v4, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 935
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v4

    iput v4, v2, Landroid/graphics/Rect;->left:I

    .line 939
    :cond_1
    iget v4, v2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_2

    .line 940
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->right:I

    .line 944
    :cond_2
    invoke-direct {p0, p1, v2}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 948
    if-nez v0, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v5

    if-le v4, v5, :cond_3

    .line 949
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v4

    iput v4, v2, Landroid/graphics/Rect;->left:I

    .line 950
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 951
    invoke-direct {p0, p1, v2}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 925
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 955
    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->j()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/HorizontalListView;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->u:Z

    return p1
.end method

.method private c(II)I
    .locals 4

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    .line 851
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 852
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 853
    iget-object v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->j:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 858
    :goto_1
    return v0

    .line 851
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 858
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private c(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 465
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 469
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 364
    iput v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    .line 365
    iput v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    .line 366
    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    .line 367
    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    .line 368
    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    .line 369
    const v0, 0x7fffffff

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    .line 370
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    .line 371
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->d()V

    return-void
.end method

.method private d()V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->c()V

    .line 376
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 377
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->requestLayout()V

    .line 378
    return-void
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/HorizontalListView;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->B:Z

    return v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/HorizontalListView;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    return v0
.end method

.method private e(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 701
    .line 702
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->g()Landroid/view/View;

    move-result-object v0

    .line 703
    if-eqz v0, :cond_1

    .line 704
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 708
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->a(II)V

    .line 712
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->f()Landroid/view/View;

    move-result-object v0

    .line 713
    if-eqz v0, :cond_0

    .line 714
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 718
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/dropbox/android/widget/HorizontalListView;->b(II)V

    .line 719
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private e()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 675
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->i(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 676
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->g()Landroid/view/View;

    move-result-object v1

    .line 678
    if-eqz v1, :cond_1

    .line 679
    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    .line 682
    iget v3, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v1, v4

    add-int/2addr v1, v3

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->i()I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    .line 685
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    if-gez v1, :cond_0

    .line 686
    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    .line 689
    :cond_0
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    if-eq v1, v2, :cond_1

    .line 690
    const/4 v0, 0x1

    .line 695
    :cond_1
    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/HorizontalListView;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->D:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private f()Landroid/view/View;
    .locals 1

    .prologue
    .line 819
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private f(I)V
    .locals 4

    .prologue
    .line 722
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->f()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 725
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v0

    add-int/2addr v0, p1

    if-gtz v0, :cond_1

    .line 729
    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    .line 732
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->a(ILandroid/view/View;)V

    .line 735
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 738
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    .line 741
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->f()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 729
    :cond_0
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_1

    .line 744
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->g()Landroid/view/View;

    move-result-object v0

    .line 747
    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 748
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(ILandroid/view/View;)V

    .line 749
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 750
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    .line 751
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->g()Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 753
    :cond_2
    return-void
.end method

.method private g()Landroid/view/View;
    .locals 1

    .prologue
    .line 824
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private g(I)V
    .locals 8

    .prologue
    .line 794
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildCount()I

    move-result v2

    .line 796
    if-lez v2, :cond_0

    .line 797
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    .line 798
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->g:I

    .line 801
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 802
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 803
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v4

    add-int/2addr v4, v1

    .line 804
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v5

    .line 805
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v4

    .line 806
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v5

    .line 809
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 812
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 801
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 815
    :cond_0
    return-void
.end method

.method private h()I
    .locals 2

    .prologue
    .line 868
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private h(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 832
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    if-gt p1, v0, :cond_0

    .line 833
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    .line 836
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private i()I
    .locals 2

    .prologue
    .line 873
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private i(I)Z
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1027
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1028
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->refreshDrawableState()V

    .line 1031
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    .line 1033
    :cond_0
    return-void
.end method

.method private j(I)V
    .locals 3

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    if-nez v0, :cond_1

    .line 1294
    :cond_0
    :goto_0
    return-void

    .line 1262
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    add-int/2addr v0, p1

    .line 1265
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1267
    :cond_2
    if-gez v0, :cond_3

    .line 1270
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1273
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->i()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/l;->a(F)Z

    .line 1276
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->c()Z

    goto :goto_0

    .line 1279
    :cond_3
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    if-le v0, v1, :cond_0

    .line 1283
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1286
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->i()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/l;->a(F)Z

    .line 1289
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1290
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->c()Z

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    if-eqz v0, :cond_0

    .line 1134
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->c()Z

    .line 1137
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    if-eqz v0, :cond_1

    .line 1138
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->c()Z

    .line 1140
    :cond_1
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->s:Lcom/dropbox/android/widget/aP;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->t:I

    if-ge v0, v1, :cond_0

    .line 1181
    iget-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->u:Z

    if-nez v0, :cond_0

    .line 1182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->u:Z

    .line 1183
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->s:Lcom/dropbox/android/widget/aP;

    invoke-interface {v0}, Lcom/dropbox/android/widget/aP;->a()V

    .line 1186
    :cond_0
    return-void
.end method

.method private m()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1301
    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1306
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method protected final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 997
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->B:Z

    .line 1000
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 1001
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    .line 1003
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->j()V

    .line 1005
    iget-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->B:Z

    if-nez v0, :cond_0

    .line 1007
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v0, v2}, Lcom/dropbox/android/widget/HorizontalListView;->c(II)I

    move-result v0

    .line 1008
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1010
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    .line 1012
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1015
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->refreshDrawableState()V

    .line 1020
    :cond_0
    return v1

    .line 997
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 989
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    neg-float v3, p3

    float-to-int v3, v3

    iget v6, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 990
    sget-object v0, Lcom/dropbox/android/widget/aO;->c:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    .line 991
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->requestLayout()V

    .line 992
    const/4 v0, 0x1

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 978
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 979
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;)V

    .line 980
    return-void
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0

    .prologue
    .line 986
    return-void
.end method

.method public synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->p:I

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->q:I

    return v0
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 2

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 623
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    if-nez v1, :cond_0

    .line 624
    const/4 v0, 0x0

    .line 630
    :goto_0
    return v0

    .line 625
    :cond_0
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    if-ge v1, v0, :cond_1

    .line 627
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 630
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 3

    .prologue
    .line 636
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 639
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    if-ne v1, v2, :cond_0

    .line 640
    const/4 v0, 0x0

    .line 646
    :goto_0
    return v0

    .line 641
    :cond_0
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    sub-int/2addr v1, v2

    if-ge v1, v0, :cond_1

    .line 643
    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 646
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->r:I

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->h(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 972
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onDraw(Landroid/graphics/Canvas;)V

    .line 973
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->b(Landroid/graphics/Canvas;)V

    .line 974
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 532
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 534
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->invalidate()V

    .line 542
    iget-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->i:Z

    if-eqz v0, :cond_2

    .line 543
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    .line 544
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->c()V

    .line 545
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 546
    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    .line 547
    iput-boolean v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->i:Z

    .line 551
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 552
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->n:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    .line 553
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->n:Ljava/lang/Integer;

    .line 557
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 559
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    .line 563
    :cond_4
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    if-gez v0, :cond_7

    .line 564
    iput v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    .line 567
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 568
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->y:Landroid/support/v4/widget/l;

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/l;->a(I)Z

    .line 571
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 572
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    .line 587
    :cond_6
    :goto_1
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    sub-int/2addr v0, v1

    .line 588
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->f(I)V

    .line 589
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->e(I)V

    .line 590
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->g(I)V

    .line 593
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    .line 596
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 598
    invoke-virtual/range {p0 .. p5}, Lcom/dropbox/android/widget/HorizontalListView;->onLayout(ZIIII)V

    goto :goto_0

    .line 573
    :cond_7
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    iget v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    if-le v0, v1, :cond_6

    .line 575
    iget v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->o:I

    iput v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->d:I

    .line 578
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    invoke-virtual {v0}, Landroid/support/v4/widget/l;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 579
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->z:Landroid/support/v4/widget/l;

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/l;->a(I)Z

    .line 582
    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 583
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    goto :goto_1

    .line 603
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 605
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->w:Lcom/dropbox/android/widget/aO;

    sget-object v1, Lcom/dropbox/android/widget/aO;->c:Lcom/dropbox/android/widget/aO;

    if-ne v0, v1, :cond_a

    .line 606
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    .line 613
    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->x:Lcom/dropbox/android/widget/aM;

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->x:Lcom/dropbox/android/widget/aM;

    invoke-interface {v0}, Lcom/dropbox/android/widget/aM;->a()V

    goto/16 :goto_0

    .line 610
    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->F:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/T;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 660
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onMeasure(II)V

    .line 663
    iput p2, p0, Lcom/dropbox/android/widget/HorizontalListView;->A:I

    .line 664
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 322
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 323
    check-cast p1, Landroid/os/Bundle;

    .line 326
    const-string v0, "BUNDLE_ID_CURRENT_X"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->n:Ljava/lang/Integer;

    .line 329
    const-string v0, "BUNDLE_ID_PARENT_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 331
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 309
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 312
    const-string v1, "BUNDLE_ID_PARENT_STATE"

    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 315
    const-string v1, "BUNDLE_ID_CURRENT_X"

    iget v2, p0, Lcom/dropbox/android/widget/HorizontalListView;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 317
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1110
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1112
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1113
    :cond_0
    sget-object v0, Lcom/dropbox/android/widget/aO;->a:Lcom/dropbox/android/widget/aO;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Lcom/dropbox/android/widget/aO;)V

    .line 1117
    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Ljava/lang/Boolean;)V

    .line 1119
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->k()V

    .line 1128
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1120
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 1121
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->j()V

    .line 1122
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->k()V

    .line 1125
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->a(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 88
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->E:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 426
    :cond_0
    if-eqz p1, :cond_1

    .line 428
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->u:Z

    .line 430
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    .line 431
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/dropbox/android/widget/HorizontalListView;->E:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->b(I)V

    .line 435
    invoke-direct {p0}, Lcom/dropbox/android/widget/HorizontalListView;->d()V

    .line 436
    return-void
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 340
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->m:Landroid/graphics/drawable/Drawable;

    .line 342
    if-eqz p1, :cond_0

    .line 343
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->setDividerWidth(I)V

    .line 347
    :goto_0
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/HorizontalListView;->setDividerWidth(I)V

    goto :goto_0
.end method

.method public setDividerWidth(I)V
    .locals 0

    .prologue
    .line 356
    iput p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->l:I

    .line 359
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->requestLayout()V

    .line 360
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HorizontalListView;->invalidate()V

    .line 361
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1195
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->D:Landroid/view/View$OnClickListener;

    .line 1196
    return-void
.end method

.method public setOnLayoutChangedListener(Lcom/dropbox/android/widget/aM;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->x:Lcom/dropbox/android/widget/aM;

    .line 227
    return-void
.end method

.method public setOnScrollStateChangedListener(Lcom/dropbox/android/widget/aN;)V
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->v:Lcom/dropbox/android/widget/aN;

    .line 1236
    return-void
.end method

.method public setRunningOutOfDataListener(Lcom/dropbox/android/widget/aP;I)V
    .locals 0

    .prologue
    .line 1159
    iput-object p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->s:Lcom/dropbox/android/widget/aP;

    .line 1160
    iput p2, p0, Lcom/dropbox/android/widget/HorizontalListView;->t:I

    .line 1161
    return-void
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 412
    iput p1, p0, Lcom/dropbox/android/widget/HorizontalListView;->r:I

    .line 413
    return-void
.end method
