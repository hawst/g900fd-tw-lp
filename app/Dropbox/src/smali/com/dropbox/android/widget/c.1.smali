.class final Lcom/dropbox/android/widget/c;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/b;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/b;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/dropbox/android/widget/c;->a:Lcom/dropbox/android/widget/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/dropbox/android/filemanager/w;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 28
    iget-object v1, p0, Lcom/dropbox/android/widget/c;->a:Lcom/dropbox/android/widget/b;

    invoke-static {v1}, Lcom/dropbox/android/widget/b;->a(Lcom/dropbox/android/widget/b;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 29
    iget-object v1, p0, Lcom/dropbox/android/widget/c;->a:Lcom/dropbox/android/widget/b;

    invoke-static {v1}, Lcom/dropbox/android/widget/b;->b(Lcom/dropbox/android/widget/b;)Landroid/database/Cursor;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v1, v2}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    .line 30
    sget-object v2, Lcom/dropbox/android/widget/d;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 45
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :pswitch_0
    iget-object v1, p0, Lcom/dropbox/android/widget/c;->a:Lcom/dropbox/android/widget/b;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/b;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 34
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 35
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 36
    invoke-static {v1}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;

    move-result-object v3

    .line 37
    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 38
    new-instance v0, Lcom/dropbox/android/filemanager/w;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v4, v3}, Lcom/dropbox/android/filemanager/w;-><init>(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)V

    .line 40
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 43
    :pswitch_1
    return-object v0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
