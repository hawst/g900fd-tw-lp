.class public final Lcom/dropbox/android/widget/aD;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/GalleryView;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dropbox/android/widget/ay;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/database/Cursor;

.field private final d:Lcom/dropbox/android/activity/bq;

.field private final e:Ldbxyzptlk/db231222/v/n;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/GalleryView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 379
    iput-object p1, p0, Lcom/dropbox/android/widget/aD;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Landroid/util/SparseArray;

    .line 381
    iput-object v2, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    .line 382
    iput-object v2, p0, Lcom/dropbox/android/widget/aD;->d:Lcom/dropbox/android/activity/bq;

    .line 383
    iput-object v2, p0, Lcom/dropbox/android/widget/aD;->e:Ldbxyzptlk/db231222/v/n;

    .line 384
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/widget/GalleryView;Landroid/database/Cursor;Lcom/dropbox/android/activity/bq;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    .line 368
    iput-object p1, p0, Lcom/dropbox/android/widget/aD;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    new-instance v0, Landroid/util/SparseArray;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Landroid/util/SparseArray;

    .line 370
    iput-object p2, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    .line 371
    iput-object p3, p0, Lcom/dropbox/android/widget/aD;->d:Lcom/dropbox/android/activity/bq;

    .line 372
    iput-object p4, p0, Lcom/dropbox/android/widget/aD;->e:Ldbxyzptlk/db231222/v/n;

    .line 373
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Lcom/dropbox/android/widget/ay;
    .locals 5

    .prologue
    .line 388
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 389
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 392
    sget-object v0, Lcom/dropbox/android/widget/aw;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->d:Lcom/dropbox/android/activity/bq;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 404
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 394
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 407
    :goto_0
    new-instance v1, Lcom/dropbox/android/widget/aC;

    invoke-direct {v1}, Lcom/dropbox/android/widget/aC;-><init>()V

    .line 408
    iput-object v0, v1, Lcom/dropbox/android/widget/aC;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 409
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->c:Lcom/dropbox/android/activity/bq;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->d:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_1

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/AlbumItemEntry;

    move-result-object v0

    iput-object v0, v1, Lcom/dropbox/android/widget/aC;->b:Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Landroid/util/SparseArray;

    new-instance v2, Lcom/dropbox/android/widget/ay;

    iget-object v3, p0, Lcom/dropbox/android/widget/aD;->a:Lcom/dropbox/android/widget/GalleryView;

    iget-object v4, p0, Lcom/dropbox/android/widget/aD;->e:Ldbxyzptlk/db231222/v/n;

    invoke-direct {v2, v3, v1, v4}, Lcom/dropbox/android/widget/ay;-><init>(Lcom/dropbox/android/widget/GalleryView;Lcom/dropbox/android/widget/aC;Ldbxyzptlk/db231222/v/n;)V

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ay;

    return-object v0

    .line 397
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/dropbox/android/provider/PhotosProvider;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    goto :goto_0

    .line 401
    :pswitch_2
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    goto :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected final finalize()V
    .locals 2

    .prologue
    .line 433
    invoke-static {}, Lcom/dropbox/android/widget/GalleryView;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImagesWrapper finalized!"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 435
    return-void
.end method
