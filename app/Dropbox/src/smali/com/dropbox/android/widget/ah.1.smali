.class public final Lcom/dropbox/android/widget/ah;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/bd;


# instance fields
.field final a:Lcom/dropbox/android/filemanager/LocalEntry;

.field final b:Lcom/dropbox/android/widget/as;

.field private final c:Ldbxyzptlk/db231222/r/d;

.field private final d:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337
    iput-object p1, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 338
    iput-object p2, p0, Lcom/dropbox/android/widget/ah;->b:Lcom/dropbox/android/widget/as;

    .line 339
    invoke-virtual {p3}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/ah;->d:Lcom/dropbox/android/filemanager/I;

    .line 340
    iput-object p3, p0, Lcom/dropbox/android/widget/ah;->c:Ldbxyzptlk/db231222/r/d;

    .line 341
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)[Lcom/dropbox/android/widget/quickactions/a;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 345
    iget-object v2, p0, Lcom/dropbox/android/widget/ah;->d:Lcom/dropbox/android/filemanager/I;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2, v3}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/db231222/j/g;

    move-result-object v2

    instance-of v2, v2, Ldbxyzptlk/db231222/j/d;

    .line 347
    if-eqz v2, :cond_0

    .line 348
    new-instance v2, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v4, p0, Lcom/dropbox/android/widget/ah;->d:Lcom/dropbox/android/filemanager/I;

    invoke-direct {v2, v3, v4}, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/I;)V

    .line 349
    new-array v0, v0, [Lcom/dropbox/android/widget/quickactions/a;

    aput-object v2, v0, v1

    .line 376
    :goto_0
    return-object v0

    .line 351
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 352
    new-instance v3, Lcom/dropbox/android/widget/quickactions/ButtonShare;

    iget-object v4, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v3, v4}, Lcom/dropbox/android/widget/quickactions/ButtonShare;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-nez v3, :cond_1

    .line 354
    new-instance v3, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;

    iget-object v4, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v5, p0, Lcom/dropbox/android/widget/ah;->d:Lcom/dropbox/android/filemanager/I;

    invoke-direct {v3, v4, v5}, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/I;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    :cond_1
    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->b:Lcom/dropbox/android/widget/as;

    sget-object v4, Lcom/dropbox/android/widget/as;->c:Lcom/dropbox/android/widget/as;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->b:Lcom/dropbox/android/widget/as;

    sget-object v4, Lcom/dropbox/android/widget/as;->e:Lcom/dropbox/android/widget/as;

    if-ne v3, v4, :cond_7

    .line 359
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 360
    iget-object v1, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/dropbox/android/widget/ah;->d:Lcom/dropbox/android/filemanager/I;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1, v3}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/db231222/j/g;

    move-result-object v1

    instance-of v1, v1, Ldbxyzptlk/db231222/j/a;

    if-nez v1, :cond_4

    .line 361
    :cond_3
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonDelete;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v4, p0, Lcom/dropbox/android/widget/ah;->d:Lcom/dropbox/android/filemanager/I;

    invoke-direct {v1, v3, v4}, Lcom/dropbox/android/widget/quickactions/ButtonDelete;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/I;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    iget-object v1, p0, Lcom/dropbox/android/widget/ah;->b:Lcom/dropbox/android/widget/as;

    sget-object v3, Lcom/dropbox/android/widget/as;->d:Lcom/dropbox/android/widget/as;

    if-eq v1, v3, :cond_4

    .line 363
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonRename;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v3}, Lcom/dropbox/android/widget/quickactions/ButtonRename;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonMove;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v3}, Lcom/dropbox/android/widget/quickactions/ButtonMove;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    iget-object v1, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-nez v1, :cond_4

    .line 367
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonExport;

    iget-object v3, p0, Lcom/dropbox/android/widget/ah;->c:Ldbxyzptlk/db231222/r/d;

    iget-object v4, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v3, v4}, Lcom/dropbox/android/widget/quickactions/ButtonExport;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    :cond_4
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/widget/ah;->b:Lcom/dropbox/android/widget/as;

    sget-object v1, Lcom/dropbox/android/widget/as;->d:Lcom/dropbox/android/widget/as;

    if-ne v0, v1, :cond_6

    .line 373
    :cond_5
    new-instance v0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;

    iget-object v1, p0, Lcom/dropbox/android/widget/ah;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    :cond_6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/dropbox/android/widget/quickactions/a;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/quickactions/a;

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 357
    goto :goto_1
.end method
