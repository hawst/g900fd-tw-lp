.class public Lcom/dropbox/android/widget/AlbumOverviewListItem;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/view/View;

.field private final d:Landroid/widget/ImageView;

.field private e:Lcom/dropbox/android/util/n;

.field private f:I

.field private final g:Lcom/dropbox/android/filemanager/y;

.field private h:Lcom/dropbox/android/filemanager/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    .line 90
    new-instance v0, Lcom/dropbox/android/widget/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/a;-><init>(Lcom/dropbox/android/widget/AlbumOverviewListItem;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->g:Lcom/dropbox/android/filemanager/y;

    .line 37
    const v0, 0x7f030016

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 38
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->addView(Landroid/view/View;)V

    .line 40
    const v0, 0x7f070038

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f070037

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    .line 42
    const v0, 0x7f070039

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->b:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f07003a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->c:Landroid/view/View;

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/AlbumOverviewListItem;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    return v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 73
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->h:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_1

    .line 74
    iget v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    if-eq v0, v4, :cond_0

    .line 75
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->h:Lcom/dropbox/android/filemanager/n;

    iget v1, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->g:Lcom/dropbox/android/filemanager/y;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/n;->c(ILcom/dropbox/android/filemanager/y;)V

    .line 76
    iput v4, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    .line 78
    :cond_0
    iput-object v3, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->h:Lcom/dropbox/android/filemanager/n;

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_3

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 85
    iput-object v3, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    .line 87
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    return-void
.end method

.method private a(Lcom/dropbox/android/util/n;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47
    iput-object p1, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    .line 48
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->a()V

    .line 49
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    invoke-virtual {v2}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 51
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->e:Lcom/dropbox/android/util/n;

    invoke-virtual {v2}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 52
    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 57
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    if-eqz p2, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 60
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 61
    iget-object v1, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 63
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 50
    goto :goto_0

    .line 54
    :cond_2
    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->d:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/AlbumOverviewListItem;Lcom/dropbox/android/util/n;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a(Lcom/dropbox/android/util/n;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/albums/Album;ILcom/dropbox/android/filemanager/n;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105
    invoke-direct {p0}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a()V

    .line 107
    iput-object p3, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->h:Lcom/dropbox/android/filemanager/n;

    .line 108
    iput p2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 117
    :goto_0
    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->h:Lcom/dropbox/android/filemanager/n;

    iget v1, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->f:I

    iget-object v2, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->g:Lcom/dropbox/android/filemanager/y;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/n;->a(ILcom/dropbox/android/filemanager/y;)Lcom/dropbox/android/util/n;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a(Lcom/dropbox/android/util/n;Z)V

    .line 121
    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 124
    :cond_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/AlbumOverviewListItem;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 68
    invoke-direct {p0}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a()V

    .line 69
    return-void
.end method
