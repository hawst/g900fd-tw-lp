.class public Lcom/dropbox/android/widget/SweetListView;
.super Landroid/widget/ListView;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected a:Lcom/dropbox/android/widget/bN;

.field private c:Landroid/widget/AdapterView$OnItemClickListener;

.field private d:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private e:I

.field private f:I

.field private g:Ljava/lang/reflect/Method;

.field private h:Ljava/lang/Object;

.field private i:Lcom/dropbox/android/widget/bC;

.field private j:Lcom/dropbox/android/widget/bP;

.field private k:Lcom/dropbox/android/widget/bL;

.field private l:Landroid/view/ContextMenu$ContextMenuInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/SweetListView;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 324
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 292
    new-instance v0, Lcom/dropbox/android/widget/bN;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/bN;-><init>(Lcom/dropbox/android/widget/SweetListView;Lcom/dropbox/android/widget/bD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->e:I

    .line 302
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->f:I

    .line 305
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    .line 306
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->h:Ljava/lang/Object;

    .line 309
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->i:Lcom/dropbox/android/widget/bC;

    .line 312
    new-instance v0, Lcom/dropbox/android/widget/bP;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/bP;-><init>(Lcom/dropbox/android/widget/bD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    .line 313
    new-instance v0, Lcom/dropbox/android/widget/bL;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bL;-><init>(Lcom/dropbox/android/widget/SweetListView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->k:Lcom/dropbox/android/widget/bL;

    .line 325
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 320
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 292
    new-instance v0, Lcom/dropbox/android/widget/bN;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/bN;-><init>(Lcom/dropbox/android/widget/SweetListView;Lcom/dropbox/android/widget/bD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->e:I

    .line 302
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->f:I

    .line 305
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    .line 306
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->h:Ljava/lang/Object;

    .line 309
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->i:Lcom/dropbox/android/widget/bC;

    .line 312
    new-instance v0, Lcom/dropbox/android/widget/bP;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/bP;-><init>(Lcom/dropbox/android/widget/bD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    .line 313
    new-instance v0, Lcom/dropbox/android/widget/bL;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bL;-><init>(Lcom/dropbox/android/widget/SweetListView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->k:Lcom/dropbox/android/widget/bL;

    .line 321
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 316
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 292
    new-instance v0, Lcom/dropbox/android/widget/bN;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/bN;-><init>(Lcom/dropbox/android/widget/SweetListView;Lcom/dropbox/android/widget/bD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->e:I

    .line 302
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->f:I

    .line 305
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    .line 306
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->h:Ljava/lang/Object;

    .line 309
    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->i:Lcom/dropbox/android/widget/bC;

    .line 312
    new-instance v0, Lcom/dropbox/android/widget/bP;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/bP;-><init>(Lcom/dropbox/android/widget/bD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    .line 313
    new-instance v0, Lcom/dropbox/android/widget/bL;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bL;-><init>(Lcom/dropbox/android/widget/SweetListView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->k:Lcom/dropbox/android/widget/bL;

    .line 317
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/SweetListView;)Lcom/dropbox/android/widget/bH;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 499
    sget-object v0, Lcom/dropbox/android/widget/SweetListView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calling setSelectionIntReflected for position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :try_start_0
    const-class v0, Landroid/widget/ListView;

    .line 503
    const-string v1, "setSelectionInt"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 504
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 505
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    :goto_0
    return-void

    .line 506
    :catch_0
    move-exception v0

    .line 507
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 868
    if-gtz p2, :cond_1

    .line 886
    :cond_0
    :goto_0
    return-void

    .line 871
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    .line 872
    if-eqz v0, :cond_0

    .line 878
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    .line 879
    invoke-static {v1}, Lcom/dropbox/android/widget/bP;->b(Lcom/dropbox/android/widget/bP;)Lcom/dropbox/android/widget/bO;

    move-result-object v2

    .line 880
    invoke-virtual {v0, v2, p1}, Lcom/dropbox/android/widget/bH;->a(Lcom/dropbox/android/widget/bO;I)V

    .line 881
    if-eqz v2, :cond_2

    iget v0, v2, Lcom/dropbox/android/widget/bO;->a:I

    if-ltz v0, :cond_2

    .line 882
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v2, p1, p0, v1}, Lcom/dropbox/android/widget/bP;->a(Lcom/dropbox/android/widget/bO;ILcom/dropbox/android/widget/SweetListView;I)V

    goto :goto_0

    .line 884
    :cond_2
    invoke-virtual {v1}, Lcom/dropbox/android/widget/bP;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/SweetListView;II)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/SweetListView;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/SweetListView;Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->f()Lcom/dropbox/android/widget/bM;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/widget/bM;->a:Lcom/dropbox/android/widget/bM;

    if-ne v0, v1, :cond_3

    .line 532
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->f()V

    .line 557
    :goto_0
    return-void

    .line 539
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v0

    if-le p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 540
    :cond_1
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/SweetListView;->c(I)V

    goto :goto_0

    .line 542
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->g()V

    goto :goto_0

    .line 545
    :cond_3
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/SweetListView;->a(I)V

    .line 549
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->i()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v0

    if-lt p1, v0, :cond_4

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v0

    if-le p1, v0, :cond_5

    .line 551
    :cond_4
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->f()V

    goto :goto_0

    .line 555
    :cond_5
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->g()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/SweetListView;Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/dropbox/android/widget/SweetListView;->b:Ljava/lang/String;

    return-object v0
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 574
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->f()V

    .line 606
    :goto_0
    return-void

    .line 579
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->h()I

    move-result v0

    .line 582
    if-lez v0, :cond_5

    .line 583
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    div-int v0, v2, v0

    move v2, p1

    .line 585
    :goto_1
    if-ltz v2, :cond_1

    sub-int v3, p1, v0

    if-lt v2, v3, :cond_1

    .line 588
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/dropbox/android/widget/bH;->a(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 589
    add-int/lit8 v0, v0, 0x1

    .line 595
    :cond_1
    :goto_2
    sub-int v0, p1, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/SweetListView;->a(I)V

    .line 598
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v0

    if-lt p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v0

    if-le p1, v0, :cond_4

    .line 599
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->f()V

    goto :goto_0

    .line 585
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 605
    :cond_4
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->g()V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method private d()Lcom/dropbox/android/widget/bH;
    .locals 1

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bH;

    return-object v0
.end method

.method private e()I
    .locals 3

    .prologue
    .line 387
    sget-object v0, Lcom/dropbox/android/widget/bD;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bN;->f()Lcom/dropbox/android/widget/bM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 395
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bC;->g()Landroid/database/Cursor;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 397
    :cond_0
    const/4 v0, -0x1

    .line 401
    :goto_0
    return v0

    .line 389
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->j()I

    move-result v0

    goto :goto_0

    .line 392
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->k()I

    move-result v0

    goto :goto_0

    .line 398
    :cond_1
    sget-object v1, Lcom/dropbox/android/widget/bM;->b:Lcom/dropbox/android/widget/bM;

    iget-object v2, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bN;->f()Lcom/dropbox/android/widget/bM;

    move-result-object v2

    if-ne v1, v2, :cond_2

    .line 399
    const/4 v0, 0x0

    goto :goto_0

    .line 401
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 387
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f()V
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->g()I

    move-result v0

    const/16 v1, 0x19

    if-ge v0, v1, :cond_0

    .line 517
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bN;->a(Z)V

    .line 518
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->h()V

    .line 519
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->requestLayout()V

    .line 521
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 614
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 616
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/dropbox/android/widget/bC;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->n()V

    .line 621
    :cond_1
    return-void
.end method

.method private h()I
    .locals 3

    .prologue
    .line 630
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getChildCount()I

    move-result v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    if-ge v0, v1, :cond_1

    .line 631
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v1

    add-int/2addr v1, v0

    .line 633
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/widget/bH;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 634
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 635
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/SweetListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 639
    :goto_1
    return v0

    .line 630
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 639
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 703
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->h:Ljava/lang/Object;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    if-nez v1, :cond_1

    .line 705
    :try_start_0
    const-string v1, "android.widget.AbsListView"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 706
    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredClasses()[Ljava/lang/Class;

    move-result-object v4

    .line 707
    const/4 v1, 0x0

    .line 708
    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v0, v4, v2

    .line 709
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "android.widget.AbsListView$RecycleBin"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 708
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 713
    :cond_0
    const-string v0, "mRecycler"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 714
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 716
    const-string v2, "pruneScrapViews"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    .line 717
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 718
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->h:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 727
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->g:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->h:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 732
    return-void

    .line 719
    :catch_0
    move-exception v0

    .line 720
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 721
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find recycler."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 728
    :catch_1
    move-exception v0

    .line 729
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 730
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to run recycler."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/widget/bC;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->i:Lcom/dropbox/android/widget/bC;

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/widget/bE;)Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 791
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v1

    .line 792
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bH;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 795
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 796
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 800
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/widget/bH;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    if-gt v2, v3, :cond_0

    .line 801
    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/bH;->b(I)I

    move-result v0

    iput v0, p1, Lcom/dropbox/android/widget/bE;->a:I

    .line 802
    invoke-virtual {v1}, Lcom/dropbox/android/widget/bH;->b()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {v1, v3}, Lcom/dropbox/android/widget/bH;->b(I)I

    move-result v2

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bH;->d()I

    move-result v1

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p1, Lcom/dropbox/android/widget/bE;->b:I

    .line 804
    const/4 v0, 0x1

    .line 811
    :goto_0
    return v0

    .line 809
    :cond_0
    iput v5, p1, Lcom/dropbox/android/widget/bE;->a:I

    .line 810
    iput v5, p1, Lcom/dropbox/android/widget/bE;->b:I

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/android/util/aW;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/android/util/aW",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 820
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v2

    .line 821
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bH;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 822
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/SweetListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 823
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v4

    .line 824
    new-instance v1, Lcom/dropbox/android/util/aW;

    invoke-virtual {v2, v4}, Lcom/dropbox/android/widget/bH;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/util/aW;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    move-object v0, v1

    .line 828
    :goto_1
    return-object v0

    .line 824
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0

    .line 828
    :cond_1
    new-instance v1, Lcom/dropbox/android/util/aW;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/util/aW;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    move-object v0, v1

    goto :goto_1
.end method

.method protected detachViewFromParent(I)V
    .locals 0

    .prologue
    .line 660
    invoke-super {p0, p1}, Landroid/widget/ListView;->detachViewFromParent(I)V

    .line 661
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->j()V

    .line 662
    return-void
.end method

.method protected detachViewFromParent(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 666
    invoke-super {p0, p1}, Landroid/widget/ListView;->detachViewFromParent(Landroid/view/View;)V

    .line 667
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->j()V

    .line 668
    return-void
.end method

.method protected detachViewsFromParent(II)V
    .locals 0

    .prologue
    .line 654
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->detachViewsFromParent(II)V

    .line 655
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->j()V

    .line 656
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 851
    const/4 v0, 0x0

    .line 853
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bP;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 854
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 855
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getHeight()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/dropbox/android/widget/bP;->a(Landroid/graphics/Canvas;II)V

    .line 858
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 860
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bP;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 861
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 862
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getWidth()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/android/widget/bP;->a(Landroid/graphics/Canvas;I)V

    .line 864
    :cond_1
    return-void
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 840
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->l:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method protected getTopFadingEdgeStrength()F
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->getTopFadingEdgeStrength()F

    move-result v0

    goto :goto_0
.end method

.method protected layoutChildren()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 416
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 418
    iget v0, p0, Lcom/dropbox/android/widget/SweetListView;->e:I

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getWidth()I

    move-result v4

    if-eq v0, v4, :cond_0

    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 419
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/widget/SweetListView;->f:I

    invoke-virtual {v0, v4, v5}, Lcom/dropbox/android/widget/bH;->a(II)V

    .line 420
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/SweetListView;->e:I

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    const-string v0, "scroll is set, mode="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-static {v0}, Lcom/dropbox/android/widget/bN;->a(Lcom/dropbox/android/widget/bN;)Lcom/dropbox/android/widget/bM;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "null"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 436
    const-string v0, "getAdapter().getCount()==0 "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->n()V

    move v0, v1

    .line 456
    :goto_1
    if-eqz v0, :cond_1

    .line 457
    const-string v0, "shouldScrollAndHighlight=true "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->e()I

    move-result v0

    .line 459
    if-ltz v0, :cond_6

    .line 460
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/bH;->c(I)I

    move-result v1

    .line 461
    const-string v2, "doScrollAndHighlight() fauxPos="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "actualPosForScrollTo="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/SweetListView;->b(I)V

    .line 474
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getSelectedItemPosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 475
    invoke-virtual {p0}, Lcom/dropbox/android/widget/SweetListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/SweetListView;->a(I)V

    .line 479
    :cond_2
    :try_start_0
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    return-void

    .line 426
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-static {v0}, Lcom/dropbox/android/widget/bN;->a(Lcom/dropbox/android/widget/bN;)Lcom/dropbox/android/widget/bM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bM;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 439
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 440
    const-string v0, "mPendingScrollToAction.enteredPostSection()=false "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->c()V

    .line 448
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/SweetListView;->a(I)V

    move v0, v2

    goto :goto_1

    .line 449
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 450
    const-string v0, "mPendingScrollToAction.shouldRetry()=true "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bN;->a(Z)V

    move v0, v2

    goto/16 :goto_1

    .line 467
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->n()V

    goto :goto_2

    .line 480
    :catch_0
    move-exception v0

    .line 481
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "super.layoutChildren() raised ArrayIndexOutOfBoundsException: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/SweetListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 329
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SweetListView requires a SweetListAdapter. Call setSweetAdapter instead."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setContextMenuInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V
    .locals 0

    .prologue
    .line 835
    iput-object p1, p0, Lcom/dropbox/android/widget/SweetListView;->l:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 836
    return-void
.end method

.method public setDelayedRestorePositionFromTop(I)V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/bN;->a(I)V

    .line 741
    return-void
.end method

.method public setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;I)V
    .locals 2

    .prologue
    .line 752
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/dropbox/android/widget/bN;->a(Lcom/dropbox/android/util/DropboxPath;IZ)V

    .line 753
    return-void
.end method

.method public setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 764
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/widget/bN;->a(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V

    .line 765
    return-void
.end method

.method public setDelayedScrollToBottom()V
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->m()V

    .line 773
    return-void
.end method

.method public setDelayedScrollToTop()V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->a:Lcom/dropbox/android/widget/bN;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bN;->l()V

    .line 769
    return-void
.end method

.method public setMinCols(I)V
    .locals 0

    .prologue
    .line 337
    iput p1, p0, Lcom/dropbox/android/widget/SweetListView;->f:I

    .line 338
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 676
    iput-object p1, p0, Lcom/dropbox/android/widget/SweetListView;->c:Landroid/widget/AdapterView$OnItemClickListener;

    .line 677
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 678
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/bH;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 680
    :cond_0
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1

    .prologue
    .line 688
    iput-object p1, p0, Lcom/dropbox/android/widget/SweetListView;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 689
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 690
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/bH;->a(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 692
    :cond_0
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/dropbox/android/widget/SweetListView;->k:Lcom/dropbox/android/widget/bL;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/bL;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 379
    return-void
.end method

.method public setSweetAdapter(Lcom/dropbox/android/widget/bC;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 347
    invoke-direct {p0}, Lcom/dropbox/android/widget/SweetListView;->d()Lcom/dropbox/android/widget/bH;

    move-result-object v1

    .line 348
    if-eqz v1, :cond_0

    .line 349
    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/bH;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 350
    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/bH;->a(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 351
    iget-object v2, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-static {v2}, Lcom/dropbox/android/widget/bP;->a(Lcom/dropbox/android/widget/bP;)Landroid/database/DataSetObserver;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/bH;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 354
    :cond_0
    if-nez p1, :cond_3

    .line 357
    :goto_0
    if-eqz v0, :cond_4

    .line 358
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-virtual {p1}, Lcom/dropbox/android/widget/bC;->b()Lcom/dropbox/android/widget/bG;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/widget/bP;->a(Lcom/dropbox/android/widget/bP;Lcom/dropbox/android/widget/bG;)Lcom/dropbox/android/widget/bG;

    .line 360
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->c:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v1, :cond_1

    .line 361
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->c:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bH;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 363
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    if-eqz v1, :cond_2

    .line 364
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bH;->a(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 366
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-static {v1}, Lcom/dropbox/android/widget/bP;->a(Lcom/dropbox/android/widget/bP;)Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bH;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 367
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-static {v1}, Lcom/dropbox/android/widget/bP;->a(Lcom/dropbox/android/widget/bP;)Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/DataSetObserver;->onChanged()V

    .line 372
    :goto_1
    iput-object p1, p0, Lcom/dropbox/android/widget/SweetListView;->i:Lcom/dropbox/android/widget/bC;

    .line 373
    invoke-super {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 374
    return-void

    .line 354
    :cond_3
    new-instance v0, Lcom/dropbox/android/widget/bH;

    invoke-direct {v0, p1}, Lcom/dropbox/android/widget/bH;-><init>(Lcom/dropbox/android/widget/bC;)V

    goto :goto_0

    .line 369
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/widget/SweetListView;->j:Lcom/dropbox/android/widget/bP;

    invoke-static {v1}, Lcom/dropbox/android/widget/bP;->a(Lcom/dropbox/android/widget/bP;)Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/DataSetObserver;->onInvalidated()V

    goto :goto_1
.end method
