.class final Lcom/dropbox/android/widget/bk;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/u;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Lcom/dropbox/android/taskqueue/ag;

.field private f:Z

.field private g:Ldbxyzptlk/db231222/j/l;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/widget/bj;)V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0}, Lcom/dropbox/android/widget/bk;-><init>()V

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 352
    if-nez p1, :cond_0

    .line 353
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/widget/bk;->a:Ljava/lang/String;

    .line 354
    iput-object v7, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    .line 392
    :goto_0
    return-void

    .line 358
    :cond_0
    const-string v2, "camera_upload_local_uri"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 359
    iget-object v3, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 360
    iget-object v3, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    iput-object v3, p0, Lcom/dropbox/android/widget/bk;->a:Ljava/lang/String;

    .line 361
    iput-object v2, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    .line 362
    const-string v2, "camera_upload_local_mime_type"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/widget/bk;->c:Ljava/lang/String;

    .line 365
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v3

    .line 366
    const-string v2, "camera_upload_pending_paths_json"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 368
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v2, v1

    .line 369
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 370
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v3, v5, v6}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 372
    :catch_0
    move-exception v0

    .line 374
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 379
    :cond_1
    const-string v2, "camera_upload_num_remaining"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/dropbox/android/widget/bk;->d:I

    .line 381
    const-string v2, "camera_upload_status"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 382
    invoke-static {v2}, Lcom/dropbox/android/taskqueue/ag;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/ag;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/widget/bk;->e:Lcom/dropbox/android/taskqueue/ag;

    .line 383
    const-string v2, "id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 384
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_2

    .line 385
    new-instance v4, Ldbxyzptlk/db231222/j/l;

    invoke-direct {v4, v2, v3}, Ldbxyzptlk/db231222/j/l;-><init>(J)V

    iput-object v4, p0, Lcom/dropbox/android/widget/bk;->g:Ldbxyzptlk/db231222/j/l;

    .line 390
    :goto_2
    const-string v2, "camera_upload_initial_scan"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 391
    if-ne v2, v0, :cond_3

    :goto_3
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bk;->f:Z

    goto/16 :goto_0

    .line 387
    :cond_2
    iput-object v7, p0, Lcom/dropbox/android/widget/bk;->g:Ldbxyzptlk/db231222/j/l;

    goto :goto_2

    :cond_3
    move v0, v1

    .line 391
    goto :goto_3
.end method

.method static synthetic a(Lcom/dropbox/android/widget/bk;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bk;->a(Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->a:Ljava/lang/String;

    .line 306
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/android/widget/bk;->a:Ljava/lang/String;

    .line 307
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/dropbox/android/widget/bk;->d:I

    return v0
.end method

.method public final f()Lcom/dropbox/android/taskqueue/ag;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->e:Lcom/dropbox/android/taskqueue/ag;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bk;->f:Z

    return v0
.end method

.method public final h()Ldbxyzptlk/db231222/j/l;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/dropbox/android/widget/bk;->g:Ldbxyzptlk/db231222/j/l;

    return-object v0
.end method
