.class public Lcom/dropbox/android/widget/UnjaggyImageView;
.super Landroid/widget/ImageView;
.source "panda.py"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 36
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 106
    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 107
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 114
    :goto_0
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 115
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 116
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 117
    :cond_0
    return-void

    .line 108
    :cond_1
    instance-of v0, p1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 109
    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 47
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getWidth()I

    move-result v0

    .line 48
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getHeight()I

    move-result v1

    .line 50
    iget-boolean v2, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 51
    iput-boolean v5, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 54
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 56
    if-eqz v3, :cond_1

    .line 57
    if-eqz v2, :cond_0

    .line 58
    invoke-virtual {v3, v5, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 60
    :cond_0
    invoke-static {p1, v3}, Lcom/dropbox/android/widget/UnjaggyImageView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 63
    :cond_1
    if-eqz v4, :cond_3

    .line 64
    if-eqz v2, :cond_2

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getPaddingLeft()I

    move-result v2

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getPaddingRight()I

    move-result v3

    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getPaddingTop()I

    move-result v5

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/widget/UnjaggyImageView;->getPaddingBottom()I

    move-result v6

    .line 69
    sub-int/2addr v0, v3

    sub-int/2addr v1, v6

    invoke-virtual {v4, v2, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 72
    :cond_2
    invoke-static {p1, v4}, Lcom/dropbox/android/widget/UnjaggyImageView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 75
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 42
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 80
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onScrollChanged(IIII)V

    .line 81
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 101
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 102
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 94
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    return-void
.end method

.method protected setFrame(IIII)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/UnjaggyImageView;->a:Z

    .line 86
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->setFrame(IIII)Z

    move-result v0

    return v0
.end method
