.class public final Lcom/dropbox/android/widget/aY;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/dropbox/android/widget/aY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/aY;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 4

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 248
    if-eqz p2, :cond_5

    .line 249
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_2

    .line 250
    :cond_0
    invoke-static {p5}, Lcom/dropbox/android/widget/aY;->a(Landroid/view/View;)V

    .line 251
    if-eqz p3, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    :goto_0
    if-eqz p1, :cond_7

    invoke-static {p1}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 274
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 275
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x106000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 281
    :goto_1
    invoke-virtual {p4, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 282
    invoke-virtual {p4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 283
    invoke-virtual {p5, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 284
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p6, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 285
    invoke-virtual {p6, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 286
    return-void

    .line 253
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f02015e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 255
    const v2, 0x7f0701d5

    invoke-virtual {p4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/TransitionDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 256
    const v2, 0x7f0701d6

    invoke-virtual {v0, v2, p1}, Landroid/graphics/drawable/TransitionDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 257
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 258
    const/16 v2, 0x96

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 262
    if-nez p3, :cond_4

    invoke-virtual {p7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 263
    invoke-static {p7}, Lcom/dropbox/android/widget/aY;->b(Landroid/view/View;)V

    :cond_3
    :goto_2
    move-object p1, v0

    .line 268
    goto :goto_0

    .line 264
    :cond_4
    if-eqz p3, :cond_3

    invoke-virtual {p7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_3

    .line 265
    invoke-static {p7}, Lcom/dropbox/android/widget/aY;->a(Landroid/view/View;)V

    .line 266
    invoke-virtual {p7, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 270
    :cond_5
    if-eqz p3, :cond_6

    move v0, v1

    :cond_6
    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 277
    :cond_7
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 278
    const/4 v0, 0x0

    invoke-static {p4, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 290
    .line 291
    if-nez p3, :cond_3

    .line 292
    const-string v0, "page_white"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 305
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 306
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 307
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    :cond_1
    if-eqz p2, :cond_2

    .line 311
    invoke-virtual {p2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 312
    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 313
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 315
    :cond_2
    return-void

    .line 294
    :cond_3
    invoke-static {p0, p3}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 295
    if-nez v0, :cond_0

    .line 296
    sget-object v0, Lcom/dropbox/android/widget/aY;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load media icon type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    if-eqz p3, :cond_4

    const-string v0, "folder"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 298
    const-string v0, "folder"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 300
    :cond_4
    const-string v0, "page_white"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/j/g;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 178
    if-eqz p1, :cond_3

    .line 179
    if-eqz p2, :cond_0

    instance-of v0, p1, Ldbxyzptlk/db231222/j/m;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/m;->e()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 181
    check-cast p1, Ldbxyzptlk/db231222/j/m;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/j/m;->e()F

    move-result v0

    .line 182
    float-to-int v0, v0

    invoke-virtual {p3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 183
    invoke-virtual {p3, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 184
    invoke-virtual {p4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-virtual {p1, p0}, Ldbxyzptlk/db231222/j/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    invoke-virtual {p3, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 189
    invoke-virtual {p4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 206
    instance-of v0, p1, Ldbxyzptlk/db231222/j/n;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Ldbxyzptlk/db231222/j/n;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/n;->c()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    if-eq v0, v1, :cond_1

    check-cast p1, Ldbxyzptlk/db231222/j/n;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/j/n;->c()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->m:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v1, :cond_2

    .line 209
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 211
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080091

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 215
    :cond_3
    invoke-virtual {p3, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 216
    invoke-virtual {p4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 222
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 223
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 224
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 225
    return-void
.end method

.method public static b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 228
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 229
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 230
    new-instance v1, Lcom/dropbox/android/widget/aZ;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/aZ;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 242
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 243
    return-void
.end method
