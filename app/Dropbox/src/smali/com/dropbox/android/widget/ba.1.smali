.class public final Lcom/dropbox/android/widget/ba;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/FrameLayout;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/dropbox/android/util/o;

.field private final g:Landroid/content/Context;

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/dropbox/android/widget/bb;

.field private j:I

.field private k:Lcom/dropbox/android/util/n;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/util/o;Landroid/content/Context;Landroid/os/Handler;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/FrameLayout;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/dropbox/android/widget/bb;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/bb;-><init>(Lcom/dropbox/android/widget/ba;Lcom/dropbox/android/widget/aZ;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/ba;->i:Lcom/dropbox/android/widget/bb;

    .line 64
    iput v2, p0, Lcom/dropbox/android/widget/ba;->j:I

    .line 65
    iput-object v1, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    .line 66
    iput-object v1, p0, Lcom/dropbox/android/widget/ba;->l:Ljava/lang/String;

    .line 67
    iput-boolean v2, p0, Lcom/dropbox/android/widget/ba;->m:Z

    .line 73
    iput-object p1, p0, Lcom/dropbox/android/widget/ba;->f:Lcom/dropbox/android/util/o;

    .line 74
    iput-object p2, p0, Lcom/dropbox/android/widget/ba;->g:Landroid/content/Context;

    .line 75
    iput-object p3, p0, Lcom/dropbox/android/widget/ba;->h:Landroid/os/Handler;

    .line 76
    iput-object p4, p0, Lcom/dropbox/android/widget/ba;->a:Landroid/widget/ImageView;

    .line 77
    iput-object p5, p0, Lcom/dropbox/android/widget/ba;->b:Landroid/widget/ImageView;

    .line 78
    iput-object p6, p0, Lcom/dropbox/android/widget/ba;->c:Landroid/widget/FrameLayout;

    .line 79
    iput-object p7, p0, Lcom/dropbox/android/widget/ba;->d:Landroid/widget/ImageView;

    .line 80
    iput-object p8, p0, Lcom/dropbox/android/widget/ba;->e:Landroid/widget/ImageView;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ba;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/dropbox/android/widget/ba;->j:I

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ba;Lcom/dropbox/android/util/n;)Lcom/dropbox/android/util/n;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    return-object p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/ba;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->l:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 145
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/ba;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/ba;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ba;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/ba;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->c:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->a:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/ba;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/ba;)Lcom/dropbox/android/util/o;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->f:Lcom/dropbox/android/util/o;

    return-object v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/ba;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->h:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    .line 88
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 95
    iput-object v3, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    .line 98
    :cond_0
    if-nez p3, :cond_1

    .line 99
    invoke-static {p2}, Lcom/dropbox/android/util/ab;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 102
    :cond_1
    invoke-static {p3}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ba;->m:Z

    .line 104
    invoke-static {p3}, Lcom/dropbox/android/util/ab;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->f:Lcom/dropbox/android/util/o;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/o;->a(Ljava/lang/String;)Lcom/dropbox/android/util/n;

    move-result-object v3

    .line 107
    if-eqz v3, :cond_2

    .line 108
    iput-object v3, p0, Lcom/dropbox/android/widget/ba;->k:Lcom/dropbox/android/util/n;

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/widget/ba;->g:Landroid/content/Context;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/dropbox/android/widget/ba;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v4, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-boolean v3, p0, Lcom/dropbox/android/widget/ba;->m:Z

    iget-object v4, p0, Lcom/dropbox/android/widget/ba;->b:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/dropbox/android/widget/ba;->c:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/dropbox/android/widget/ba;->a:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/dropbox/android/widget/ba;->d:Landroid/widget/ImageView;

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 138
    :goto_0
    return-void

    .line 119
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/widget/ba;->b()V

    .line 120
    iput-object p1, p0, Lcom/dropbox/android/widget/ba;->l:Ljava/lang/String;

    .line 121
    iget v0, p0, Lcom/dropbox/android/widget/ba;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/ba;->j:I

    .line 122
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/dropbox/android/widget/ba;->l:Ljava/lang/String;

    iget v2, p0, Lcom/dropbox/android/widget/ba;->j:I

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/dropbox/android/widget/ba;->i:Lcom/dropbox/android/widget/bb;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/filemanager/X;->b(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ah;

    goto :goto_0

    .line 128
    :cond_3
    invoke-static {p2}, Lcom/dropbox/android/util/ab;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    if-nez v0, :cond_4

    if-eqz p3, :cond_4

    .line 130
    invoke-static {p3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_4

    .line 132
    invoke-static {v1}, Lcom/dropbox/android/util/ab;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_4
    invoke-direct {p0}, Lcom/dropbox/android/widget/ba;->b()V

    .line 136
    iget-object v1, p0, Lcom/dropbox/android/widget/ba;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/widget/ba;->e:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3, v0}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method
