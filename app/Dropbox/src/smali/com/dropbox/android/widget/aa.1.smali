.class final Lcom/dropbox/android/widget/aa;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/DbxVideoView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/DbxVideoView;)V
    .locals 0

    .prologue
    .line 543
    iput-object p1, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 548
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0, p3}, Lcom/dropbox/android/widget/DbxVideoView;->f(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 549
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0, p4}, Lcom/dropbox/android/widget/DbxVideoView;->g(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 550
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->m(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 551
    :goto_0
    iget-object v3, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v3}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v3

    if-ne v3, p3, :cond_3

    iget-object v3, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v3}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v3

    if-ne v3, p4, :cond_3

    .line 552
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxVideoView;->h(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 553
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->j(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->j(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(I)V

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    .line 558
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 550
    goto :goto_0

    :cond_3
    move v1, v2

    .line 551
    goto :goto_1
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 564
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->q(Lcom/dropbox/android/widget/DbxVideoView;)V

    .line 565
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 572
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aa;->a:Lcom/dropbox/android/widget/DbxVideoView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->e(Lcom/dropbox/android/widget/DbxVideoView;Z)V

    .line 574
    return-void
.end method
