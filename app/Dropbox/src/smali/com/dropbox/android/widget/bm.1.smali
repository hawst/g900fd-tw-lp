.class final enum Lcom/dropbox/android/widget/bm;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/widget/bm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/bm;

.field public static final enum b:Lcom/dropbox/android/widget/bm;

.field public static final enum c:Lcom/dropbox/android/widget/bm;

.field public static final enum d:Lcom/dropbox/android/widget/bm;

.field public static final enum e:Lcom/dropbox/android/widget/bm;

.field public static final enum f:Lcom/dropbox/android/widget/bm;

.field private static final synthetic g:[Lcom/dropbox/android/widget/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/dropbox/android/widget/bm;

    const-string v1, "SEPARATOR"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bm;->a:Lcom/dropbox/android/widget/bm;

    .line 43
    new-instance v0, Lcom/dropbox/android/widget/bm;

    const-string v1, "CAMERA_STATUS"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/widget/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bm;->b:Lcom/dropbox/android/widget/bm;

    .line 44
    new-instance v0, Lcom/dropbox/android/widget/bm;

    const-string v1, "TURN_ON"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/widget/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bm;->c:Lcom/dropbox/android/widget/bm;

    .line 45
    new-instance v0, Lcom/dropbox/android/widget/bm;

    const-string v1, "PREV_PAGE_ITEM"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/widget/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bm;->d:Lcom/dropbox/android/widget/bm;

    .line 46
    new-instance v0, Lcom/dropbox/android/widget/bm;

    const-string v1, "NEXT_PAGE_ITEM"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/android/widget/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bm;->e:Lcom/dropbox/android/widget/bm;

    .line 47
    new-instance v0, Lcom/dropbox/android/widget/bm;

    const-string v1, "REMOTE_INSTALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/bm;->f:Lcom/dropbox/android/widget/bm;

    .line 41
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dropbox/android/widget/bm;

    sget-object v1, Lcom/dropbox/android/widget/bm;->a:Lcom/dropbox/android/widget/bm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/bm;->b:Lcom/dropbox/android/widget/bm;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/widget/bm;->c:Lcom/dropbox/android/widget/bm;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/widget/bm;->d:Lcom/dropbox/android/widget/bm;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/widget/bm;->e:Lcom/dropbox/android/widget/bm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/widget/bm;->f:Lcom/dropbox/android/widget/bm;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/widget/bm;->g:[Lcom/dropbox/android/widget/bm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/bm;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/dropbox/android/widget/bm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bm;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/bm;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/dropbox/android/widget/bm;->g:[Lcom/dropbox/android/widget/bm;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/bm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/bm;

    return-object v0
.end method
