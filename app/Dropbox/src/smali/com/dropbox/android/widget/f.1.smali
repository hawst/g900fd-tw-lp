.class public final Lcom/dropbox/android/widget/f;
.super Lcom/dropbox/android/widget/bX;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/taskqueue/D;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/widget/bX;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/taskqueue/D;)V

    .line 19
    invoke-virtual {p0, p2}, Lcom/dropbox/android/widget/f;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 20
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4

    .prologue
    .line 59
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 60
    new-instance v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/f;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/widget/f;->b:Lcom/dropbox/android/widget/bY;

    iget-object v3, p0, Lcom/dropbox/android/widget/f;->f:Lcom/dropbox/android/widget/aG;

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/dropbox/android/widget/ThumbGridItemView;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/aG;)V

    return-object v0

    .line 62
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)Lcom/dropbox/android/filemanager/w;
    .locals 6

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 71
    const/4 v0, 0x0

    .line 73
    iget-object v2, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 74
    iget-object v2, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/f;->e(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 78
    iget-object v3, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 79
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 80
    new-instance v0, Lcom/dropbox/android/filemanager/w;

    new-instance v4, Lcom/dropbox/android/util/DropboxPath;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v0, v4, v3}, Lcom/dropbox/android/filemanager/w;-><init>(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)V

    .line 83
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 85
    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 51
    check-cast p2, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v0, p0, Lcom/dropbox/android/widget/f;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {p2, p1, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(Landroid/database/Cursor;Lcom/dropbox/android/filemanager/n;)V

    .line 55
    return-void

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 24
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/Z;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 30
    const/4 v0, -0x1

    return v0
.end method

.method public final b()Lcom/dropbox/android/widget/bG;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    new-instance v0, Lcom/dropbox/android/widget/bG;

    invoke-direct {v0, v1, v1, v1}, Lcom/dropbox/android/widget/bG;-><init>(ZZZ)V

    return-object v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public final k_()Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/widget/f;->d:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
