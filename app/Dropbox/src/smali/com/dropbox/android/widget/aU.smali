.class public final Lcom/dropbox/android/widget/aU;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:[Landroid/content/Intent;

.field private final d:[Landroid/content/Intent;

.field private final e:Lcom/dropbox/android/util/c;

.field private f:Lcom/dropbox/android/widget/aX;

.field private g:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/aU;->a:Ljava/lang/ref/WeakReference;

    .line 69
    iput-object p2, p0, Lcom/dropbox/android/widget/aU;->b:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Lcom/dropbox/android/widget/aU;->c:[Landroid/content/Intent;

    .line 71
    iput-object p4, p0, Lcom/dropbox/android/widget/aU;->d:[Landroid/content/Intent;

    .line 72
    iput-object p5, p0, Lcom/dropbox/android/widget/aU;->e:Lcom/dropbox/android/util/c;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/aU;)Lcom/dropbox/android/widget/aX;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dropbox/android/widget/aU;->f:Lcom/dropbox/android/widget/aX;

    return-object v0
.end method

.method private b()Landroid/content/Context;
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/dropbox/android/widget/aU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 82
    if-nez v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {}, Lcom/dropbox/android/util/bn;->d()I

    move-result v2

    invoke-direct {v1, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 89
    invoke-direct {p0}, Lcom/dropbox/android/widget/aU;->b()Landroid/content/Context;

    move-result-object v6

    .line 90
    if-nez v6, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/aU;->g:Landroid/content/pm/PackageManager;

    .line 95
    const-string v0, "layout_inflater"

    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 96
    new-instance v0, Lcom/dropbox/android/widget/aW;

    iget-object v2, p0, Lcom/dropbox/android/widget/aU;->g:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lcom/dropbox/android/widget/aU;->c:[Landroid/content/Intent;

    iget-object v4, p0, Lcom/dropbox/android/widget/aU;->d:[Landroid/content/Intent;

    iget-object v5, p0, Lcom/dropbox/android/widget/aU;->e:Lcom/dropbox/android/util/c;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/aW;-><init>(Landroid/view/LayoutInflater;Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;)V

    .line 99
    invoke-virtual {v0}, Lcom/dropbox/android/widget/aW;->getCount()I

    move-result v2

    .line 100
    if-le v2, v7, :cond_2

    .line 101
    const v2, 0x7f030045

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 102
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 105
    iget-object v3, p0, Lcom/dropbox/android/widget/aU;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 106
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 107
    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 109
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 111
    new-instance v3, Lcom/dropbox/android/widget/aV;

    invoke-direct {v3, p0, v0, v2}, Lcom/dropbox/android/widget/aV;-><init>(Lcom/dropbox/android/widget/aU;Lcom/dropbox/android/widget/aW;Landroid/app/Dialog;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 122
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 123
    :cond_2
    if-ne v2, v7, :cond_3

    .line 124
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aW;->a(I)Landroid/content/Intent;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/dropbox/android/widget/aU;->f:Lcom/dropbox/android/widget/aX;

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/dropbox/android/widget/aU;->f:Lcom/dropbox/android/widget/aX;

    invoke-interface {v1, v0}, Lcom/dropbox/android/widget/aX;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/aU;->f:Lcom/dropbox/android/widget/aX;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/widget/aU;->f:Lcom/dropbox/android/widget/aX;

    invoke-interface {v0}, Lcom/dropbox/android/widget/aX;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/widget/aX;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/dropbox/android/widget/aU;->f:Lcom/dropbox/android/widget/aX;

    .line 77
    return-void
.end method
