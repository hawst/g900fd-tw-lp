.class final Lcom/dropbox/android/widget/bN;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/SweetListView;

.field private b:Lcom/dropbox/android/widget/bM;

.field private c:Lcom/dropbox/android/util/DropboxPath;

.field private d:I

.field private e:I

.field private f:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Z

.field private i:Z


# direct methods
.method private constructor <init>(Lcom/dropbox/android/widget/SweetListView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 102
    iput-object p1, p0, Lcom/dropbox/android/widget/bN;->a:Lcom/dropbox/android/widget/SweetListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object v1, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    .line 111
    iput-object v1, p0, Lcom/dropbox/android/widget/bN;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 118
    iput v0, p0, Lcom/dropbox/android/widget/bN;->d:I

    .line 126
    iput v0, p0, Lcom/dropbox/android/widget/bN;->e:I

    .line 131
    iput-object v1, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    .line 136
    iput v0, p0, Lcom/dropbox/android/widget/bN;->g:I

    .line 142
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bN;->h:Z

    .line 145
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bN;->i:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/widget/SweetListView;Lcom/dropbox/android/widget/bD;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bN;-><init>(Lcom/dropbox/android/widget/SweetListView;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/bN;)Lcom/dropbox/android/widget/bM;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bN;->n()V

    .line 235
    sget-object v0, Lcom/dropbox/android/widget/bM;->d:Lcom/dropbox/android/widget/bM;

    iput-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    .line 236
    iput p1, p0, Lcom/dropbox/android/widget/bN;->e:I

    .line 237
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bN;->n()V

    .line 218
    sget-object v0, Lcom/dropbox/android/widget/bM;->a:Lcom/dropbox/android/widget/bM;

    iput-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    .line 219
    iput-object p1, p0, Lcom/dropbox/android/widget/bN;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 221
    if-lez p2, :cond_1

    .line 222
    iput p2, p0, Lcom/dropbox/android/widget/bN;->d:I

    .line 227
    :goto_0
    if-eqz p3, :cond_0

    .line 228
    iput-object p3, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    .line 231
    :cond_0
    return-void

    .line 224
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/bN;->d:I

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;IZ)V
    .locals 1

    .prologue
    .line 202
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 204
    if-eqz p3, :cond_0

    .line 205
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/dropbox/android/widget/bN;->a(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V

    .line 209
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/dropbox/android/widget/bN;->i:Z

    .line 185
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bN;->h:Z

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/bN;->h:Z

    .line 157
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/dropbox/android/widget/bM;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/dropbox/android/widget/bN;->g:I

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/dropbox/android/widget/bN;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/bN;->g:I

    .line 177
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bN;->i:Z

    return v0
.end method

.method public final j()I
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    sget-object v1, Lcom/dropbox/android/widget/bM;->a:Lcom/dropbox/android/widget/bM;

    if-eq v0, v1, :cond_0

    .line 193
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Function only support on entry scroll requests"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->a:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bN;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/widget/bN;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/bC;->a(Ljava/lang/String;I)I

    move-result v0

    .line 198
    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    sget-object v1, Lcom/dropbox/android/widget/bM;->d:Lcom/dropbox/android/widget/bM;

    if-eq v0, v1, :cond_0

    .line 244
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Function only supports positional scroll requests"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_0
    iget v0, p0, Lcom/dropbox/android/widget/bN;->e:I

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bN;->n()V

    .line 252
    sget-object v0, Lcom/dropbox/android/widget/bM;->b:Lcom/dropbox/android/widget/bM;

    iput-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    .line 253
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bN;->n()V

    .line 257
    sget-object v0, Lcom/dropbox/android/widget/bM;->c:Lcom/dropbox/android/widget/bM;

    iput-object v0, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    .line 258
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 261
    iput-object v1, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    .line 262
    iput-object v1, p0, Lcom/dropbox/android/widget/bN;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 263
    iput v0, p0, Lcom/dropbox/android/widget/bN;->d:I

    .line 264
    iput v0, p0, Lcom/dropbox/android/widget/bN;->e:I

    .line 265
    iput v0, p0, Lcom/dropbox/android/widget/bN;->g:I

    .line 266
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bN;->h:Z

    .line 267
    iput-object v1, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    .line 268
    iput-boolean v0, p0, Lcom/dropbox/android/widget/bN;->i:Z

    .line 269
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[mode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bN;->b:Lcom/dropbox/android/widget/bM;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bM;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    iget-object v1, p0, Lcom/dropbox/android/widget/bN;->c:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v1, :cond_1

    .line 276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " path:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/bN;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " startPos:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/widget/bN;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 277
    iget-object v1, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    if-eqz v1, :cond_1

    .line 278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pathsToHighlight: {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    iget-object v1, p0, Lcom/dropbox/android/widget/bN;->f:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 280
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 281
    goto :goto_0

    .line 282
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 286
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 288
    return-object v0
.end method
