.class public final Lcom/dropbox/android/widget/b;
.super Landroid/support/v4/widget/a;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/filemanager/n;

.field private final b:Lcom/dropbox/android/activity/di;

.field private final c:Lcom/dropbox/android/taskqueue/D;

.field private final d:Lcom/dropbox/android/filemanager/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/dropbox/android/activity/di;Lcom/dropbox/android/taskqueue/D;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 25
    new-instance v0, Lcom/dropbox/android/widget/c;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/c;-><init>(Lcom/dropbox/android/widget/b;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/b;->d:Lcom/dropbox/android/filemanager/v;

    .line 53
    iput-object p4, p0, Lcom/dropbox/android/widget/b;->b:Lcom/dropbox/android/activity/di;

    .line 54
    iput-object p5, p0, Lcom/dropbox/android/widget/b;->c:Lcom/dropbox/android/taskqueue/D;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/b;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/b;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/n;->a(Z)V

    .line 136
    :cond_0
    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 75
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p3, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 76
    sget-object v1, Lcom/dropbox/android/widget/d;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 88
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :pswitch_0
    invoke-static {p3}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    .line 79
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 80
    iget-object v2, p0, Lcom/dropbox/android/widget/b;->b:Lcom/dropbox/android/activity/di;

    invoke-interface {v2}, Lcom/dropbox/android/activity/di;->a()I

    move-result v2

    .line 81
    iget-object v3, p0, Lcom/dropbox/android/widget/b;->b:Lcom/dropbox/android/activity/di;

    invoke-interface {v3}, Lcom/dropbox/android/activity/di;->b()I

    move-result v3

    .line 82
    iget-object v4, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    sub-int/2addr v3, v2

    invoke-virtual {v4, v2, v3}, Lcom/dropbox/android/filemanager/n;->a(II)V

    .line 83
    check-cast p1, Lcom/dropbox/android/widget/AlbumOverviewListItem;

    iget-object v2, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {p1, v0, v1, v2}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a(Lcom/dropbox/android/albums/Album;ILcom/dropbox/android/filemanager/n;)V

    .line 86
    :pswitch_1
    return-void

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 65
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->mCursor:Landroid/database/Cursor;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/dropbox/android/provider/Z;->values()[Lcom/dropbox/android/provider/Z;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 94
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p2, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 95
    sget-object v1, Lcom/dropbox/android/widget/d;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 101
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 97
    :pswitch_0
    new-instance v0, Lcom/dropbox/android/widget/AlbumOverviewListItem;

    invoke-direct {v0, p1}, Lcom/dropbox/android/widget/AlbumOverviewListItem;-><init>(Landroid/content/Context;)V

    .line 99
    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {p1}, Lcom/dropbox/android/widget/e;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 107
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/n;->a()V

    .line 110
    :cond_0
    if-eqz p1, :cond_1

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    if-nez v0, :cond_2

    .line 112
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/b;->d:Lcom/dropbox/android/filemanager/v;

    invoke-static {}, Lcom/dropbox/android/util/bn;->g()Ldbxyzptlk/db231222/v/n;

    move-result-object v3

    invoke-static {}, Lcom/dropbox/android/util/bn;->j()I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/widget/b;->c:Lcom/dropbox/android/taskqueue/D;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Ldbxyzptlk/db231222/v/n;ILcom/dropbox/android/taskqueue/D;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    .line 123
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 120
    :cond_2
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/b;->d:Lcom/dropbox/android/filemanager/v;

    iget-object v3, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Lcom/dropbox/android/filemanager/n;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/b;->a:Lcom/dropbox/android/filemanager/n;

    goto :goto_0
.end method
