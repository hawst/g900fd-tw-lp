.class final Lcom/dropbox/android/widget/K;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/DbxMediaController;

.field private b:J


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 2

    .prologue
    .line 743
    iput-object p1, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 763
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/widget/K;->b:J

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8

    .prologue
    .line 767
    if-nez p3, :cond_1

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 773
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->h(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/O;

    move-result-object v0

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->c()I

    move-result v0

    int-to-long v0, v0

    .line 774
    const-wide/16 v2, 0x7530

    cmp-long v2, v0, v2

    if-gtz v2, :cond_2

    .line 775
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    const/16 v3, 0x64

    invoke-static {v2, v3}, Lcom/dropbox/android/widget/DbxMediaController;->a(Lcom/dropbox/android/widget/DbxMediaController;I)V

    .line 778
    :cond_2
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->i(Lcom/dropbox/android/widget/DbxMediaController;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->m(Lcom/dropbox/android/widget/DbxMediaController;)I

    move-result v2

    if-le p2, v2, :cond_4

    .line 779
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->m(Lcom/dropbox/android/widget/DbxMediaController;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 780
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Capping at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v4}, Lcom/dropbox/android/widget/DbxMediaController;->m(Lcom/dropbox/android/widget/DbxMediaController;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " instead of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 782
    iget-wide v4, p0, Lcom/dropbox/android/widget/K;->b:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0xfa0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    .line 783
    iget-object v4, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v4}, Lcom/dropbox/android/widget/DbxMediaController;->n(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d0277

    invoke-static {v4, v5}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 784
    iput-wide v2, p0, Lcom/dropbox/android/widget/K;->b:J

    .line 785
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->O()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "max.seek"

    iget-object v4, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v4}, Lcom/dropbox/android/widget/DbxMediaController;->m(Lcom/dropbox/android/widget/DbxMediaController;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "duration"

    invoke-virtual {v2, v3, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 788
    :cond_3
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->m(Lcom/dropbox/android/widget/DbxMediaController;)I

    move-result p2

    .line 791
    :cond_4
    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 792
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->h(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/O;

    move-result-object v2

    long-to-int v3, v0

    invoke-interface {v2, v3}, Lcom/dropbox/android/widget/O;->a(I)V

    .line 793
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->o(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 794
    iget-object v2, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxMediaController;->o(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    long-to-int v0, v0

    invoke-static {v3, v0}, Lcom/dropbox/android/widget/DbxMediaController;->b(Lcom/dropbox/android/widget/DbxMediaController;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 795
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->p(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/R;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->p(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/R;

    move-result-object v0

    invoke-interface {v0}, Lcom/dropbox/android/widget/R;->a()V

    goto/16 :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 746
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 748
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(Lcom/dropbox/android/widget/DbxMediaController;Z)Z

    .line 755
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->l(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 756
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 802
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(Lcom/dropbox/android/widget/DbxMediaController;Z)Z

    .line 803
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->f(Lcom/dropbox/android/widget/DbxMediaController;)I

    .line 804
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->q(Lcom/dropbox/android/widget/DbxMediaController;)V

    .line 805
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 810
    iget-object v0, p0, Lcom/dropbox/android/widget/K;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->l(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 811
    return-void
.end method
