.class public Lcom/dropbox/android/widget/quickactions/ButtonDelete;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final d:Lcom/dropbox/android/filemanager/LocalEntry;

.field private final e:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/I;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 21
    iput-object p2, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->e:Lcom/dropbox/android/filemanager/I;

    .line 22
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/quickactions/ButtonDelete;)Lcom/dropbox/android/filemanager/I;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->e:Lcom/dropbox/android/filemanager/I;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/quickactions/ButtonDelete;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method private c(Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 42
    new-instance v0, Lcom/dropbox/android/widget/quickactions/c;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/quickactions/c;-><init>(Lcom/dropbox/android/widget/quickactions/ButtonDelete;Landroid/content/Context;)V

    .line 60
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 61
    const v3, 0x7f0d00a6

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 62
    const v0, 0x7f0d0013

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d00ae

    .line 65
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 70
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 71
    return-void

    .line 64
    :cond_0
    const v0, 0x7f0d00a3

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f030083

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->c(Landroid/support/v4/app/Fragment;)V

    .line 38
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f0d01e9

    return v0
.end method
