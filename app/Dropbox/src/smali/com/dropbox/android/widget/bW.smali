.class final Lcom/dropbox/android/widget/bW;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/ThumbGridItemView;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/ThumbGridItemView;)V
    .locals 1

    .prologue
    .line 309
    iput-object p1, p0, Lcom/dropbox/android/widget/bW;->a:Lcom/dropbox/android/widget/ThumbGridItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/bW;->b:Z

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/dropbox/android/widget/bW;->b:Z

    if-eqz v0, :cond_0

    .line 334
    :goto_0
    return-void

    .line 327
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/bW;->b:Z

    .line 332
    invoke-virtual {p1}, Landroid/view/animation/Animation;->cancel()V

    .line 333
    iget-object v0, p0, Lcom/dropbox/android/widget/bW;->a:Lcom/dropbox/android/widget/ThumbGridItemView;

    invoke-static {v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->c(Lcom/dropbox/android/widget/ThumbGridItemView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 314
    return-void
.end method
