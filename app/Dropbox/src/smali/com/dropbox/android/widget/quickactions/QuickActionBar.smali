.class public final Lcom/dropbox/android/widget/quickactions/QuickActionBar;
.super Lcom/dropbox/android/widget/n;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Landroid/widget/CheckBox;

.field private final b:Landroid/support/v4/app/Fragment;

.field private c:Lcom/dropbox/android/widget/quickactions/f;

.field private d:[Lcom/dropbox/android/widget/quickactions/a;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 42
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f02021b

    const v2, 0x7f020219

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/android/widget/n;-><init>(Landroid/content/Context;II)V

    .line 47
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b:Landroid/support/v4/app/Fragment;

    .line 49
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 50
    const v1, 0x7f030080

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Landroid/view/View;)V

    .line 53
    const v0, 0x7f0c0060

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(I)V

    .line 55
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Z)V

    .line 56
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b(Z)V

    .line 57
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c(Z)V

    .line 59
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(II)V

    .line 61
    invoke-virtual {p0, p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Landroid/view/View$OnTouchListener;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b:Landroid/support/v4/app/Fragment;

    return-object v0
.end method


# virtual methods
.method public final varargs a(Landroid/widget/CheckBox;[Lcom/dropbox/android/widget/quickactions/a;)V
    .locals 10

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 98
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    .line 100
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 101
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 104
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 105
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    .line 106
    const-wide/high16 v5, 0x4054000000000000L    # 80.0

    float-to-double v7, v0

    mul-double/2addr v5, v7

    double-to-int v0, v5

    .line 108
    int-to-float v3, v0

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 109
    array-length v3, p2

    if-gt v3, v1, :cond_1

    .line 112
    array-length v3, p2

    if-ge v3, v1, :cond_0

    .line 113
    div-int/lit8 v1, v0, 0x8

    add-int/2addr v0, v1

    .line 116
    :cond_0
    array-length v1, p2

    move v9, v1

    move v1, v0

    move v0, v9

    :goto_0
    move v3, v2

    .line 121
    :goto_1
    if-ge v3, v0, :cond_2

    .line 122
    aget-object v5, p2, v3

    .line 123
    invoke-virtual {p0, v5, v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/a;I)V

    .line 121
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 118
    :cond_1
    add-int/lit8 v1, v1, -0x1

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_0

    .line 126
    :cond_2
    array-length v3, p2

    if-ge v0, v3, :cond_3

    .line 127
    array-length v3, p2

    sub-int/2addr v3, v0

    .line 128
    new-array v5, v3, [Lcom/dropbox/android/widget/quickactions/a;

    .line 129
    invoke-static {p2, v0, v5, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    new-instance v0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;

    invoke-direct {v0, p0, v5, p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;-><init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;[Lcom/dropbox/android/widget/quickactions/a;Lcom/dropbox/android/widget/quickactions/QuickActionBar;)V

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/a;I)V

    .line 134
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 135
    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getHeight()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v3, v0, 0x2

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Landroid/view/View;IIII)V

    .line 140
    :goto_2
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->g()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c:Lcom/dropbox/android/widget/quickactions/f;

    if-eqz v0, :cond_4

    .line 143
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c:Lcom/dropbox/android/widget/quickactions/f;

    invoke-interface {v0}, Lcom/dropbox/android/widget/quickactions/f;->b_()V

    .line 145
    :cond_4
    return-void

    .line 137
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getHeight()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v0, v2, v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Landroid/view/View;II)V

    goto :goto_2
.end method

.method protected final a(Lcom/dropbox/android/widget/quickactions/a;I)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, p0}, Lcom/dropbox/android/widget/quickactions/a;->a(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/quickactions/QuickActionBar;)V

    .line 66
    invoke-virtual {p1}, Lcom/dropbox/android/widget/quickactions/a;->c()Landroid/view/View;

    move-result-object v1

    .line 67
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v0, p2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 69
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 70
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/quickactions/f;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c:Lcom/dropbox/android/widget/quickactions/f;

    .line 176
    return-void
.end method

.method public final a([Lcom/dropbox/android/widget/quickactions/a;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->d:[Lcom/dropbox/android/widget/quickactions/a;

    .line 82
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->h()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 167
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/widget/n;->c()V

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 171
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a:Landroid/widget/CheckBox;

    .line 172
    return-void
.end method

.method public final e()[Lcom/dropbox/android/widget/quickactions/a;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->d:[Lcom/dropbox/android/widget/quickactions/a;

    .line 92
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->d:[Lcom/dropbox/android/widget/quickactions/a;

    .line 93
    return-object v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 150
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 151
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a()Landroid/view/View;

    move-result-object v1

    .line 154
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c()V

    .line 156
    const/4 v0, 0x1

    .line 159
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
