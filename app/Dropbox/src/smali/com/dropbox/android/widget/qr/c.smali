.class final Lcom/dropbox/android/widget/qr/c;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Landroid/graphics/Point;

.field private b:Landroid/graphics/Point;

.field private c:Landroid/graphics/Point;

.field private d:Z

.field private e:F

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/graphics/Point;Landroid/graphics/Point;Z)F
    .locals 2

    .prologue
    .line 238
    if-eqz p2, :cond_0

    .line 242
    iget v0, p0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 245
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method static a(Landroid/graphics/Point;IIZ)Landroid/graphics/Point;
    .locals 6

    .prologue
    .line 196
    iget v0, p0, Landroid/graphics/Point;->x:I

    .line 197
    iget v1, p0, Landroid/graphics/Point;->y:I

    .line 198
    if-eqz p3, :cond_2

    .line 206
    :goto_0
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 207
    int-to-float v1, p2

    int-to-float v2, p1

    div-float/2addr v1, v2

    .line 208
    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    .line 212
    int-to-float v1, p1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p2

    .line 230
    :goto_1
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0

    .line 213
    :cond_0
    float-to-double v1, v0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    .line 217
    int-to-float v1, p2

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p1

    goto :goto_1

    .line 227
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 228
    int-to-float v1, p1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p2

    goto :goto_1

    :cond_2
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method

.method static a(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;
    .locals 11

    .prologue
    .line 164
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    .line 165
    const/4 v3, 0x0

    .line 166
    if-eqz v0, :cond_3

    .line 167
    const/4 v2, 0x0

    .line 168
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 171
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v5, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v1, v5

    .line 172
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v5, v5

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 173
    if-le v1, v2, :cond_2

    const v6, 0xfa000

    if-gt v1, v6, :cond_2

    float-to-double v6, v5

    const-wide v8, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v6, v6, v8

    if-lez v6, :cond_2

    float-to-double v5, v5

    const-wide v7, 0x3ff59999a28f5c29L    # 1.350000033378601

    cmpg-double v5, v5, v7

    if-gez v5, :cond_2

    move v10, v1

    move-object v1, v0

    move v0, v10

    :goto_1
    move v2, v0

    move-object v3, v1

    .line 179
    goto :goto_0

    :cond_0
    move-object v0, v3

    .line 182
    :goto_2
    if-nez v0, :cond_1

    .line 184
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 187
    :cond_1
    return-object v0

    :cond_2
    move v0, v2

    move-object v1, v3

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2
.end method

.method private static varargs a(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 255
    if-eqz p0, :cond_1

    .line 256
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 257
    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 262
    :goto_1
    return-object v0

    .line 256
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method final a()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/c;->b:Landroid/graphics/Point;

    return-object v0
.end method

.method final a(Landroid/hardware/Camera;Landroid/hardware/Camera$CameraInfo;III)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 101
    mul-int/lit8 v1, p5, 0x5a

    .line 102
    iget v2, p2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v2, v0, :cond_0

    .line 103
    iget v2, p2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v1, v2

    rem-int/lit16 v1, v1, 0x168

    iput v1, p0, Lcom/dropbox/android/widget/qr/c;->f:I

    .line 104
    iget v1, p0, Lcom/dropbox/android/widget/qr/c;->f:I

    rsub-int v1, v1, 0x168

    rem-int/lit16 v1, v1, 0x168

    iput v1, p0, Lcom/dropbox/android/widget/qr/c;->f:I

    .line 109
    :goto_0
    rem-int/lit8 v1, p5, 0x2

    if-nez v1, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/c;->d:Z

    .line 110
    invoke-virtual {p1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/widget/qr/c;->a(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 111
    new-instance v1, Landroid/graphics/Point;

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/dropbox/android/widget/qr/c;->b:Landroid/graphics/Point;

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/c;->b:Landroid/graphics/Point;

    iget-boolean v1, p0, Lcom/dropbox/android/widget/qr/c;->d:Z

    invoke-static {v0, p3, p4, v1}, Lcom/dropbox/android/widget/qr/c;->a(Landroid/graphics/Point;IIZ)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/c;->a:Landroid/graphics/Point;

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/c;->b:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/c;->a:Landroid/graphics/Point;

    iget-boolean v2, p0, Lcom/dropbox/android/widget/qr/c;->d:Z

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/widget/qr/c;->a(Landroid/graphics/Point;Landroid/graphics/Point;Z)F

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/qr/c;->e:F

    .line 119
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/c;->a:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/qr/c;->a:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {p4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/c;->c:Landroid/graphics/Point;

    .line 122
    return-void

    .line 106
    :cond_0
    iget v2, p2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int v1, v2, v1

    add-int/lit16 v1, v1, 0x168

    rem-int/lit16 v1, v1, 0x168

    iput v1, p0, Lcom/dropbox/android/widget/qr/c;->f:I

    goto :goto_0

    .line 109
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a(Landroid/hardware/Camera;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 130
    invoke-virtual {p1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 132
    if-nez v1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 137
    :cond_0
    if-eqz p2, :cond_2

    .line 138
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "auto"

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/dropbox/android/widget/qr/c;->a(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    :goto_1
    if-eqz v0, :cond_1

    .line 153
    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/c;->b:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/dropbox/android/widget/qr/c;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v0, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 156
    invoke-virtual {p1, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 157
    iget v0, p0, Lcom/dropbox/android/widget/qr/c;->f:I

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "auto"

    aput-object v3, v2, v4

    const-string v3, "macro"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "edof"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/dropbox/android/widget/qr/c;->a(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method final b()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/c;->a:Landroid/graphics/Point;

    return-object v0
.end method

.method final c()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/c;->c:Landroid/graphics/Point;

    return-object v0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/c;->d:Z

    return v0
.end method

.method final e()F
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/dropbox/android/widget/qr/c;->e:F

    return v0
.end method
