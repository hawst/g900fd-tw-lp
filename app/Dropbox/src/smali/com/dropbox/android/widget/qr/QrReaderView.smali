.class public final Lcom/dropbox/android/widget/qr/QrReaderView;
.super Landroid/widget/FrameLayout;
.source "panda.py"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private a:Landroid/view/SurfaceView;

.field private b:Lcom/dropbox/android/widget/qr/d;

.field private c:Lcom/dropbox/android/widget/qr/j;

.field private d:Lcom/dropbox/android/widget/qr/k;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method private a(Landroid/view/SurfaceHolder;)V
    .locals 6

    .prologue
    .line 161
    if-nez p1, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No SurfaceHolder provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    new-instance v2, Lcom/dropbox/android/widget/qr/l;

    invoke-direct {v2, p0}, Lcom/dropbox/android/widget/qr/l;-><init>(Lcom/dropbox/android/widget/qr/QrReaderView;)V

    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->getHeight()I

    move-result v4

    invoke-direct {p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->d()I

    move-result v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/qr/d;->a(Landroid/view/SurfaceHolder;Landroid/os/Handler;III)V

    .line 166
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/qr/QrReaderView;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->c()V

    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 170
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/d;->e()Landroid/graphics/Point;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 172
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    const/16 v4, 0x11

    invoke-direct {v2, v3, v0, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->c:Lcom/dropbox/android/widget/qr/j;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/j;->a()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private d()I
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->c:Lcom/dropbox/android/widget/qr/j;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/j;->b()V

    .line 124
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/d;->b()V

    .line 125
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->f:Z

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 128
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->f:Z

    .line 78
    new-instance v0, Lcom/dropbox/android/widget/qr/d;

    invoke-direct {v0}, Lcom/dropbox/android/widget/qr/d;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    .line 79
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    .line 80
    new-instance v0, Lcom/dropbox/android/widget/qr/j;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/qr/j;-><init>(Lcom/dropbox/android/widget/qr/QrReaderView;Lcom/dropbox/android/widget/qr/d;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->c:Lcom/dropbox/android/widget/qr/j;

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/d;->a()V

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->addView(Landroid/view/View;)V

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->setKeepScreenOn(Z)V

    .line 89
    return-void
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 145
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->co()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 147
    const/4 v0, 0x0

    .line 157
    :goto_0
    return v0

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->d:Lcom/dropbox/android/widget/qr/k;

    invoke-interface {v1, v0}, Lcom/dropbox/android/widget/qr/k;->a(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 154
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 155
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 157
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->b:Lcom/dropbox/android/widget/qr/d;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/d;->a()V

    .line 133
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->f:Z

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Landroid/view/SurfaceHolder;)V

    .line 142
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 140
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    goto :goto_0
.end method

.method public final setCallback(Lcom/dropbox/android/widget/qr/k;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->d:Lcom/dropbox/android/widget/qr/k;

    .line 93
    return-void
.end method

.method public final setQrPrefix(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->e:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->f:Z

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->f:Z

    .line 112
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Landroid/view/SurfaceHolder;)V

    .line 114
    :cond_0
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/QrReaderView;->f:Z

    .line 119
    return-void
.end method
