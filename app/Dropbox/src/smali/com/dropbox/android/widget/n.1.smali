.class public Lcom/dropbox/android/widget/n;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final E:[I


# instance fields
.field private A:Z

.field private B:Lcom/dropbox/android/widget/p;

.field private C:Z

.field private D:I

.field private F:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private G:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field private H:I

.field private I:I

.field private a:Landroid/content/Context;

.field private b:Landroid/view/WindowManager;

.field private c:Z

.field private d:Z

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Z

.field private h:I

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Landroid/view/View$OnTouchListener;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:[I

.field private w:[I

.field private x:Landroid/graphics/Rect;

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100aa

    aput v2, v0, v1

    sput-object v0, Lcom/dropbox/android/widget/n;->E:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput v1, p0, Lcom/dropbox/android/widget/n;->h:I

    .line 95
    iput-boolean v0, p0, Lcom/dropbox/android/widget/n;->j:Z

    .line 96
    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->k:Z

    .line 97
    iput-boolean v0, p0, Lcom/dropbox/android/widget/n;->l:Z

    .line 111
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    .line 112
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->w:[I

    .line 113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->x:Landroid/graphics/Rect;

    .line 121
    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->C:Z

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/n;->D:I

    .line 130
    new-instance v0, Lcom/dropbox/android/widget/o;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/o;-><init>(Lcom/dropbox/android/widget/n;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->G:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 153
    iput-object p1, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    .line 154
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    .line 156
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->y:Landroid/graphics/drawable/Drawable;

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->z:Landroid/graphics/drawable/Drawable;

    .line 158
    return-void
.end method

.method private a(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;
    .locals 3

    .prologue
    .line 684
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 689
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 690
    iget v1, p0, Lcom/dropbox/android/widget/n;->o:I

    iput v1, p0, Lcom/dropbox/android/widget/n;->p:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 691
    iget v1, p0, Lcom/dropbox/android/widget/n;->r:I

    iput v1, p0, Lcom/dropbox/android/widget/n;->s:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 692
    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 693
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/n;->d(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 694
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 695
    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 696
    iget v1, p0, Lcom/dropbox/android/widget/n;->i:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PopupWindow:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 699
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/n;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->F:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Landroid/view/View;ZIIZII)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1087
    invoke-virtual {p0}, Lcom/dropbox/android/widget/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1119
    :cond_0
    :goto_0
    return-void

    .line 1091
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->F:Ljava/lang/ref/WeakReference;

    .line 1092
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2

    if-eqz p2, :cond_3

    iget v0, p0, Lcom/dropbox/android/widget/n;->H:I

    if-ne v0, p3, :cond_2

    iget v0, p0, Lcom/dropbox/android/widget/n;->I:I

    if-eq v0, p4, :cond_3

    .line 1094
    :cond_2
    invoke-direct {p0, p1, p3, p4}, Lcom/dropbox/android/widget/n;->b(Landroid/view/View;II)V

    .line 1097
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1099
    if-eqz p5, :cond_4

    .line 1100
    if-ne p6, v1, :cond_5

    .line 1101
    iget p6, p0, Lcom/dropbox/android/widget/n;->t:I

    .line 1105
    :goto_1
    if-ne p7, v1, :cond_6

    .line 1106
    iget p7, p0, Lcom/dropbox/android/widget/n;->u:I

    .line 1112
    :cond_4
    :goto_2
    if-eqz p2, :cond_7

    .line 1113
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/dropbox/android/widget/n;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->A:Z

    .line 1118
    :goto_3
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0, v1, v0, p6, p7}, Lcom/dropbox/android/widget/n;->a(IIII)V

    goto :goto_0

    .line 1103
    :cond_5
    iput p6, p0, Lcom/dropbox/android/widget/n;->t:I

    goto :goto_1

    .line 1108
    :cond_6
    iput p7, p0, Lcom/dropbox/android/widget/n;->u:I

    goto :goto_2

    .line 1115
    :cond_7
    iget v1, p0, Lcom/dropbox/android/widget/n;->H:I

    iget v2, p0, Lcom/dropbox/android/widget/n;->I:I

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/dropbox/android/widget/n;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->A:Z

    goto :goto_3
.end method

.method private a(Landroid/view/WindowManager$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v0, -0x2

    .line 631
    iget-object v2, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    if-nez v2, :cond_1

    .line 632
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You must specify a valid content view by calling setContentView() before attempting to show the popup."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 636
    :cond_1
    iget-object v2, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 638
    if-eqz v2, :cond_2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v2, v0, :cond_2

    .line 645
    :goto_0
    new-instance v2, Lcom/dropbox/android/widget/q;

    iget-object v3, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/dropbox/android/widget/q;-><init>(Lcom/dropbox/android/widget/n;Landroid/content/Context;)V

    .line 646
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 651
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->y:Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 653
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    invoke-virtual {v2, v0, v3}, Lcom/dropbox/android/widget/q;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 655
    iput-object v2, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    .line 657
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v0, p0, Lcom/dropbox/android/widget/n;->t:I

    .line 658
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v0, p0, Lcom/dropbox/android/widget/n;->u:I

    .line 659
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z
    .locals 9

    .prologue
    const/4 v6, -0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 760
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 761
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    aget v0, v0, v2

    add-int/2addr v0, p3

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 762
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    aget v0, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, p4

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 766
    const/16 v0, 0x33

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 768
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->w:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 769
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 770
    invoke-virtual {p1, v4}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 774
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    iget v1, p0, Lcom/dropbox/android/widget/n;->n:I

    iget v5, p0, Lcom/dropbox/android/widget/n;->q:I

    invoke-virtual {v0, v1, v5}, Landroid/view/View;->measure(II)V

    .line 775
    iget v0, p0, Lcom/dropbox/android/widget/n;->u:I

    if-ne v0, v6, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 776
    :goto_0
    iget v1, p0, Lcom/dropbox/android/widget/n;->t:I

    if-ne v1, v6, :cond_2

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 778
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    .line 779
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v0, v6

    iget v6, v4, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v6, :cond_0

    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v0, v1

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_5

    .line 783
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v0

    .line 784
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v1

    .line 787
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v1

    invoke-direct {v6, v0, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 789
    invoke-virtual {p1, v6, v2}, Landroid/view/View;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    .line 793
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 794
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    aget v0, v0, v2

    add-int/2addr v0, p3

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 795
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->v:[I

    aget v0, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, p4

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 798
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->w:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 800
    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->w:[I

    aget v1, v1, v3

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, p4

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->w:[I

    aget v1, v1, v3

    sub-int/2addr v1, p4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v4

    if-ge v0, v1, :cond_3

    move v0, v3

    .line 802
    :goto_2
    if-eqz v0, :cond_4

    .line 803
    const/16 v1, 0x53

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 804
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/n;->v:[I

    aget v2, v2, v3

    sub-int/2addr v1, v2

    add-int/2addr v1, p4

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 810
    :goto_3
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 812
    return v0

    .line 775
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/n;->u:I

    goto/16 :goto_0

    .line 776
    :cond_2
    iget v1, p0, Lcom/dropbox/android/widget/n;->t:I

    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 800
    goto :goto_2

    .line 806
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/widget/n;->v:[I

    aget v1, v1, v3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, p4

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method static synthetic a(Lcom/dropbox/android/widget/n;Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/widget/n;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/n;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/dropbox/android/widget/n;->A:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/n;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 1145
    invoke-direct {p0}, Lcom/dropbox/android/widget/n;->f()V

    .line 1147
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->F:Ljava/lang/ref/WeakReference;

    .line 1148
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1149
    if-eqz v0, :cond_0

    .line 1150
    iget-object v1, p0, Lcom/dropbox/android/widget/n;->G:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1153
    :cond_0
    iput p2, p0, Lcom/dropbox/android/widget/n;->H:I

    .line 1154
    iput p3, p0, Lcom/dropbox/android/widget/n;->I:I

    .line 1155
    return-void
.end method

.method private b(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    .line 671
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 672
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/n;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/dropbox/android/widget/n;->H:I

    return v0
.end method

.method private d(I)I
    .locals 4

    .prologue
    const/high16 v3, 0x20000

    .line 703
    const v0, -0x68219

    and-int/2addr v0, p1

    .line 710
    iget-boolean v1, p0, Lcom/dropbox/android/widget/n;->C:Z

    if-eqz v1, :cond_0

    .line 711
    const v1, 0x8000

    or-int/2addr v0, v1

    .line 713
    :cond_0
    iget-boolean v1, p0, Lcom/dropbox/android/widget/n;->g:Z

    if-nez v1, :cond_5

    .line 714
    or-int/lit8 v0, v0, 0x8

    .line 715
    iget v1, p0, Lcom/dropbox/android/widget/n;->h:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 716
    or-int/2addr v0, v3

    .line 721
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/dropbox/android/widget/n;->j:Z

    if-nez v1, :cond_2

    .line 722
    or-int/lit8 v0, v0, 0x10

    .line 724
    :cond_2
    iget-boolean v1, p0, Lcom/dropbox/android/widget/n;->k:Z

    if-eqz v1, :cond_3

    .line 725
    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    .line 727
    :cond_3
    iget-boolean v1, p0, Lcom/dropbox/android/widget/n;->l:Z

    if-nez v1, :cond_4

    .line 728
    or-int/lit16 v0, v0, 0x200

    .line 730
    :cond_4
    return v0

    .line 718
    :cond_5
    iget v1, p0, Lcom/dropbox/android/widget/n;->h:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 719
    or-int/2addr v0, v3

    goto :goto_0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/n;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/dropbox/android/widget/n;->I:I

    return v0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/dropbox/android/widget/n;->E:[I

    return-object v0
.end method

.method private e()I
    .locals 2

    .prologue
    .line 734
    iget v0, p0, Lcom/dropbox/android/widget/n;->D:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 735
    iget-boolean v0, p0, Lcom/dropbox/android/widget/n;->d:Z

    if-eqz v0, :cond_0

    .line 736
    const v0, 0x7f0c0060

    .line 740
    :goto_0
    return v0

    .line 738
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 740
    :cond_1
    iget v0, p0, Lcom/dropbox/android/widget/n;->D:I

    goto :goto_0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/n;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/dropbox/android/widget/n;->A:Z

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/n;)Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->m:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1132
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->F:Ljava/lang/ref/WeakReference;

    .line 1134
    if-eqz v0, :cond_1

    .line 1135
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1137
    :goto_0
    if-eqz v0, :cond_0

    .line 1138
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1139
    iget-object v2, p0, Lcom/dropbox/android/widget/n;->G:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1141
    :cond_0
    iput-object v1, p0, Lcom/dropbox/android/widget/n;->F:Ljava/lang/ref/WeakReference;

    .line 1142
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/n;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 199
    iput p1, p0, Lcom/dropbox/android/widget/n;->D:I

    .line 200
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 445
    iput p1, p0, Lcom/dropbox/android/widget/n;->n:I

    .line 446
    iput p2, p0, Lcom/dropbox/android/widget/n;->q:I

    .line 447
    return-void
.end method

.method public final a(IIII)V
    .locals 6

    .prologue
    .line 979
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/n;->a(IIIIZ)V

    .line 980
    return-void
.end method

.method public final a(IIIIZ)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    .line 996
    if-eq p3, v4, :cond_0

    .line 997
    iput p3, p0, Lcom/dropbox/android/widget/n;->p:I

    .line 998
    invoke-virtual {p0, p3}, Lcom/dropbox/android/widget/n;->c(I)V

    .line 1001
    :cond_0
    if-eq p4, v4, :cond_1

    .line 1002
    iput p4, p0, Lcom/dropbox/android/widget/n;->s:I

    .line 1003
    invoke-virtual {p0, p4}, Lcom/dropbox/android/widget/n;->b(I)V

    .line 1006
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/n;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    if-nez v0, :cond_3

    .line 1051
    :cond_2
    :goto_0
    return-void

    .line 1010
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1014
    iget v2, p0, Lcom/dropbox/android/widget/n;->n:I

    if-gez v2, :cond_9

    iget v2, p0, Lcom/dropbox/android/widget/n;->n:I

    .line 1015
    :goto_1
    if-eq p3, v4, :cond_4

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    if-eq v3, v2, :cond_4

    .line 1016
    iput v2, p0, Lcom/dropbox/android/widget/n;->p:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    move p5, v1

    .line 1020
    :cond_4
    iget v2, p0, Lcom/dropbox/android/widget/n;->q:I

    if-gez v2, :cond_a

    iget v2, p0, Lcom/dropbox/android/widget/n;->q:I

    .line 1021
    :goto_2
    if-eq p4, v4, :cond_5

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    if-eq v3, v2, :cond_5

    .line 1022
    iput v2, p0, Lcom/dropbox/android/widget/n;->s:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    move p5, v1

    .line 1026
    :cond_5
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    if-eq v2, p1, :cond_6

    .line 1027
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    move p5, v1

    .line 1031
    :cond_6
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    if-eq v2, p2, :cond_7

    .line 1032
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move p5, v1

    .line 1036
    :cond_7
    invoke-direct {p0}, Lcom/dropbox/android/widget/n;->e()I

    move-result v2

    .line 1037
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    if-eq v2, v3, :cond_8

    .line 1038
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    move p5, v1

    .line 1042
    :cond_8
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/n;->d(I)I

    move-result v2

    .line 1043
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    if-eq v2, v3, :cond_b

    .line 1044
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1048
    :goto_3
    if-eqz v1, :cond_2

    .line 1049
    iget-object v1, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1014
    :cond_9
    iget v2, p0, Lcom/dropbox/android/widget/n;->p:I

    goto :goto_1

    .line 1020
    :cond_a
    iget v2, p0, Lcom/dropbox/android/widget/n;->s:I

    goto :goto_2

    :cond_b
    move v1, p5

    goto :goto_3
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/dropbox/android/widget/n;->m:Landroid/view/View$OnTouchListener;

    .line 247
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/dropbox/android/widget/n;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iput-object p1, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    .line 236
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->a:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 577
    invoke-virtual {p0}, Lcom/dropbox/android/widget/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 581
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/widget/n;->b(Landroid/view/View;II)V

    .line 583
    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->c:Z

    .line 584
    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->d:Z

    .line 586
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/n;->a(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 589
    iget v1, p0, Lcom/dropbox/android/widget/n;->q:I

    if-gez v1, :cond_2

    iget v1, p0, Lcom/dropbox/android/widget/n;->q:I

    iput v1, p0, Lcom/dropbox/android/widget/n;->s:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 590
    :cond_2
    iget v1, p0, Lcom/dropbox/android/widget/n;->n:I

    if-gez v1, :cond_3

    iget v1, p0, Lcom/dropbox/android/widget/n;->n:I

    iput v1, p0, Lcom/dropbox/android/widget/n;->p:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 592
    :cond_3
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/n;->a(Landroid/view/WindowManager$LayoutParams;)V

    .line 593
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/dropbox/android/widget/n;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/n;->A:Z

    .line 595
    iget-boolean v1, p0, Lcom/dropbox/android/widget/n;->A:Z

    if-eqz v1, :cond_4

    .line 596
    iget-object v1, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    iget-object v2, p0, Lcom/dropbox/android/widget/n;->y:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 600
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->refreshDrawableState()V

    .line 602
    invoke-direct {p0}, Lcom/dropbox/android/widget/n;->e()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 604
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/n;->b(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0

    .line 598
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    iget-object v2, p0, Lcom/dropbox/android/widget/n;->z:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 1081
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, v2

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/widget/n;->a(Landroid/view/View;ZIIZII)V

    .line 1082
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/p;)V
    .locals 0

    .prologue
    .line 916
    iput-object p1, p0, Lcom/dropbox/android/widget/n;->B:Lcom/dropbox/android/widget/p;

    .line 917
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 277
    iput-boolean p1, p0, Lcom/dropbox/android/widget/n;->g:Z

    .line 278
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 472
    iput p1, p0, Lcom/dropbox/android/widget/n;->r:I

    .line 473
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 357
    iput-boolean p1, p0, Lcom/dropbox/android/widget/n;->j:Z

    .line 358
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/dropbox/android/widget/n;->c:Z

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 893
    invoke-virtual {p0}, Lcom/dropbox/android/widget/n;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 894
    invoke-direct {p0}, Lcom/dropbox/android/widget/n;->f()V

    .line 896
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 898
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 899
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/dropbox/android/widget/n;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 901
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/n;->f:Landroid/view/View;

    .line 902
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/n;->c:Z

    .line 904
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->B:Lcom/dropbox/android/widget/p;

    if-eqz v0, :cond_1

    .line 905
    iget-object v0, p0, Lcom/dropbox/android/widget/n;->B:Lcom/dropbox/android/widget/p;

    invoke-interface {v0}, Lcom/dropbox/android/widget/p;->c_()V

    .line 908
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 498
    iput p1, p0, Lcom/dropbox/android/widget/n;->o:I

    .line 499
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 390
    iput-boolean p1, p0, Lcom/dropbox/android/widget/n;->k:Z

    .line 391
    return-void
.end method
