.class public Lcom/dropbox/android/widget/DbxVideoView;
.super Landroid/view/SurfaceView;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/O;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private final D:Ljava/lang/Object;

.field private E:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final F:Landroid/media/MediaPlayer$OnInfoListener;

.field private G:Landroid/media/MediaPlayer$OnErrorListener;

.field private H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field b:Landroid/media/MediaPlayer$OnPreparedListener;

.field c:Landroid/view/SurfaceHolder$Callback;

.field private e:Landroid/net/Uri;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/view/SurfaceHolder;

.field private k:Landroid/media/MediaPlayer;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Lcom/dropbox/android/widget/DbxMediaController;

.field private q:Landroid/media/MediaPlayer$OnCompletionListener;

.field private r:Landroid/media/MediaPlayer$OnPreparedListener;

.field private s:Landroid/media/MediaPlayer$OnInfoListener;

.field private t:I

.field private u:Landroid/media/MediaPlayer$OnErrorListener;

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 95
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 56
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 72
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 73
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 76
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    .line 77
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 85
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    .line 183
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    .line 184
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    .line 289
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    .line 290
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    .line 291
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    .line 333
    new-instance v0, Lcom/dropbox/android/widget/T;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/T;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 358
    new-instance v0, Lcom/dropbox/android/widget/U;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/U;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->b:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 410
    new-instance v0, Lcom/dropbox/android/widget/V;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/V;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->E:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 428
    new-instance v0, Lcom/dropbox/android/widget/W;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/W;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->F:Landroid/media/MediaPlayer$OnInfoListener;

    .line 440
    new-instance v0, Lcom/dropbox/android/widget/X;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/X;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    .line 494
    new-instance v0, Lcom/dropbox/android/widget/Z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/Z;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 542
    new-instance v0, Lcom/dropbox/android/widget/aa;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aa;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->c:Landroid/view/SurfaceHolder$Callback;

    .line 96
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->p()V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/widget/DbxVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->p()V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 72
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 73
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 76
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    .line 77
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 85
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    .line 183
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    .line 184
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    .line 289
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    .line 290
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    .line 291
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    .line 333
    new-instance v0, Lcom/dropbox/android/widget/T;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/T;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 358
    new-instance v0, Lcom/dropbox/android/widget/U;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/U;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->b:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 410
    new-instance v0, Lcom/dropbox/android/widget/V;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/V;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->E:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 428
    new-instance v0, Lcom/dropbox/android/widget/W;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/W;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->F:Landroid/media/MediaPlayer$OnInfoListener;

    .line 440
    new-instance v0, Lcom/dropbox/android/widget/X;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/X;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    .line 494
    new-instance v0, Lcom/dropbox/android/widget/Z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/Z;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 542
    new-instance v0, Lcom/dropbox/android/widget/aa;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aa;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->c:Landroid/view/SurfaceHolder$Callback;

    .line 106
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->p()V

    .line 107
    return-void
.end method

.method private a(Landroid/media/MediaPlayer;)I
    .locals 3

    .prologue
    .line 311
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_0
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 313
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    monitor-exit v1

    .line 316
    :goto_0
    return v0

    .line 315
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    goto :goto_0

    .line 315
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Landroid/media/MediaPlayer;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 581
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 583
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 584
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 585
    if-eqz p1, :cond_0

    .line 586
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 589
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->w:Z

    return p1
.end method

.method private b(Landroid/media/MediaPlayer;)I
    .locals 3

    .prologue
    .line 325
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 326
    :try_start_0
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 327
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    monitor-exit v1

    .line 330
    :goto_0
    return v0

    .line 329
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Landroid/media/MediaPlayer;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->x:Z

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxVideoView;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->y:Z

    return p1
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    return p1
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    return p1
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    return v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->t:I

    return p1
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxVideoView;Z)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Z)V

    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->n:I

    return p1
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DbxVideoView;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->o:I

    return p1
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->r:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    return v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->n:I

    return v0
.end method

.method static synthetic l(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->o:I

    return v0
.end method

.method static synthetic m(Lcom/dropbox/android/widget/DbxVideoView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    return v0
.end method

.method static synthetic n(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->q:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic o(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnInfoListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->u:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 162
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    .line 163
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    .line 164
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->c:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 167
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 168
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setFocusable(Z)V

    .line 169
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setFocusableInTouchMode(Z)V

    .line 170
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->requestFocus()Z

    .line 171
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 172
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 173
    return-void
.end method

.method private q()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 233
    invoke-direct {p0, v5}, Lcom/dropbox/android/widget/DbxVideoView;->a(Z)V

    .line 235
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->b:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->F:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 238
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 241
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->E:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 242
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 243
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 244
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->t:I

    .line 247
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 248
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 249
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 250
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 251
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 254
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 255
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->r()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 256
    :catch_0
    move-exception v0

    .line 257
    sget-object v1, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 258
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 259
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 260
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1, v6, v5}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0

    .line 262
    :catch_1
    move-exception v0

    .line 263
    sget-object v1, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 264
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 265
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 266
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1, v6, v5}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0
.end method

.method static synthetic q(Lcom/dropbox/android/widget/DbxVideoView;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->q()V

    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/widget/DbxMediaController;->setMediaPlayer(Lcom/dropbox/android/widget/O;)V

    .line 282
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 284
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/DbxMediaController;->setAnchorView(Landroid/view/View;)V

    .line 285
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->setEnabled(Z)V

    .line 287
    :cond_0
    return-void

    :cond_1
    move-object v0, p0

    .line 282
    goto :goto_0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    .line 655
    :goto_0
    return-void

    .line 653
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 659
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 661
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 663
    :cond_0
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 664
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 731
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    .line 735
    :goto_0
    return-void

    .line 733
    :cond_0
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 294
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 295
    :try_start_0
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    .line 296
    iput p2, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    .line 297
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    .line 299
    iput p2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    .line 300
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 301
    sget-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forced video size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    return-void

    .line 297
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 668
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 671
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 674
    :cond_0
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 675
    return-void
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 708
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    if-gtz v0, :cond_0

    .line 709
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 711
    sget-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 716
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 724
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 745
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->t:I

    .line 747
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 760
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->w:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 765
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->x:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 770
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->y:Z

    return v0
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 215
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 216
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 218
    :cond_0
    return-void
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 682
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 686
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxVideoView;->a(Z)V

    .line 687
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->q()V

    .line 691
    return-void
.end method

.method public final n()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 752
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 610
    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x18

    if-eq p1, v0, :cond_2

    const/16 v0, 0x19

    if-eq p1, v0, :cond_2

    const/16 v0, 0xa4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x52

    if-eq p1, v0, :cond_2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_2

    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    move v0, v1

    .line 617
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_8

    .line 618
    const/16 v0, 0x4f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x55

    if-ne p1, v0, :cond_4

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 621
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->b()V

    .line 622
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    .line 646
    :cond_1
    :goto_1
    return v1

    .line 610
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 624
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    .line 625
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_1

    .line 628
    :cond_4
    const/16 v0, 0x7e

    if-ne p1, v0, :cond_5

    .line 629
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 630
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    .line 631
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_1

    .line 634
    :cond_5
    const/16 v0, 0x56

    if-eq p1, v0, :cond_6

    const/16 v0, 0x7f

    if-ne p1, v0, :cond_7

    .line 636
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->b()V

    .line 638
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    goto :goto_1

    .line 642
    :cond_7
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->s()V

    .line 646
    :cond_8
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 112
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->getDefaultSize(II)I

    move-result v1

    .line 113
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    invoke-static {v0, p2}, Lcom/dropbox/android/widget/DbxVideoView;->getDefaultSize(II)I

    move-result v0

    .line 114
    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    if-lez v2, :cond_0

    .line 115
    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_1

    .line 117
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    div-int/2addr v0, v2

    .line 128
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/widget/DbxVideoView;->setMeasuredDimension(II)V

    .line 129
    return-void

    .line 118
    :cond_1
    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    .line 120
    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    div-int/2addr v1, v2

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_0

    .line 594
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->s()V

    .line 596
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_0

    .line 602
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->s()V

    .line 604
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setAllowSeek(Z)V
    .locals 2

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    if-eqz v0, :cond_0

    .line 193
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setAllowSeek() called too late; video already prepared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_0
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    .line 196
    return-void
.end method

.method public setDuration(I)V
    .locals 0

    .prologue
    .line 697
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 698
    return-void
.end method

.method public setMediaController(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    .line 275
    :cond_0
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    .line 276
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->r()V

    .line 277
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    .prologue
    .line 521
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->q:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 522
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->u:Landroid/media/MediaPlayer$OnErrorListener;

    .line 535
    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    .line 540
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->r:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 511
    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 177
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/widget/DbxVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V

    .line 181
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    .line 203
    iput-object p2, p0, Lcom/dropbox/android/widget/DbxVideoView;->f:Ljava/util/Map;

    .line 204
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    .line 205
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->q()V

    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->requestLayout()V

    .line 207
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->invalidate()V

    .line 208
    return-void
.end method
