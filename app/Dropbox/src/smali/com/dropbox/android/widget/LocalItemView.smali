.class public Lcom/dropbox/android/widget/LocalItemView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# static fields
.field private static final c:Ljava/lang/String;

.field private static n:Landroid/graphics/ColorMatrixColorFilter;

.field private static o:Landroid/graphics/ColorMatrixColorFilter;


# instance fields
.field a:Landroid/os/Handler;

.field b:Lcom/dropbox/android/widget/bg;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/CheckBox;

.field private final k:Z

.field private l:Z

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x14

    .line 45
    const-class v0, Lcom/dropbox/android/widget/LocalItemView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/LocalItemView;->c:Ljava/lang/String;

    .line 183
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    sput-object v0, Lcom/dropbox/android/widget/LocalItemView;->n:Landroid/graphics/ColorMatrixColorFilter;

    .line 190
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    sput-object v0, Lcom/dropbox/android/widget/LocalItemView;->o:Landroid/graphics/ColorMatrixColorFilter;

    return-void

    .line 183
    nop

    :array_0
    .array-data 4
        0x3e63d70a    # 0.2225f
        0x3f3786c2    # 0.7169f
        0x3d7837b5    # 0.0606f
        0x0
        0x0
        0x3e63d70a    # 0.2225f
        0x3f3786c2    # 0.7169f
        0x3d7837b5    # 0.0606f
        0x0
        0x0
        0x3e63d70a    # 0.2225f
        0x3f3786c2    # 0.7169f
        0x3d7837b5    # 0.0606f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 190
    :array_1
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3e4ccccd    # 0.2f
        0x3ba3d70a    # 0.005f
        0x0
        0x0
        0x3dcccccd    # 0.1f
        0x3e4ccccd    # 0.2f
        0x3ba3d70a    # 0.005f
        0x0
        0x0
        0x3dcccccd    # 0.1f
        0x3e4ccccd    # 0.2f
        0x3ba3d70a    # 0.005f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/LocalItemView;->m:I

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->a:Landroid/os/Handler;

    .line 64
    new-instance v0, Lcom/dropbox/android/widget/bg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/bg;-><init>(Lcom/dropbox/android/widget/LocalItemView;Lcom/dropbox/android/widget/bf;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->b:Lcom/dropbox/android/widget/bg;

    .line 68
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/LocalItemView;->a(Landroid/content/Context;)V

    .line 69
    iput-boolean p2, p0, Lcom/dropbox/android/widget/LocalItemView;->k:Z

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/LocalItemView;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/dropbox/android/widget/LocalItemView;->m:I

    return v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    invoke-static {p1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 89
    const/4 v3, 0x3

    invoke-static {v2}, Lcom/dropbox/android/util/au;->b(Landroid/content/res/Resources;)Ljava/util/Locale;

    move-result-object v2

    invoke-static {v3, v2}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v2

    new-instance v3, Ljava/sql/Date;

    invoke-direct {v3, v0, v1}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 98
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->f:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 105
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 73
    const v0, 0x7f030055

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/LocalItemView;->addView(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const v1, 0x7f0700e6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const v1, 0x7f0700e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->f:Landroid/widget/ImageView;

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const v1, 0x7f0700e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const v1, 0x7f0700e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->h:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const v1, 0x7f0700e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->i:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    const v1, 0x7f0700e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->j:Landroid/widget/CheckBox;

    .line 82
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 222
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 223
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 224
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 225
    iget-object v1, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 231
    invoke-static {p1}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 233
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 239
    :goto_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 241
    iget-boolean v1, p0, Lcom/dropbox/android/widget/LocalItemView;->k:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/dropbox/android/widget/LocalItemView;->l:Z

    if-nez v1, :cond_2

    .line 242
    sget-object v1, Lcom/dropbox/android/widget/LocalItemView;->o:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 243
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->f:Landroid/widget/ImageView;

    sget-object v1, Lcom/dropbox/android/widget/LocalItemView;->o:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    return-void

    .line 235
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/LocalItemView;Landroid/graphics/Bitmap;Z)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/LocalItemView;->a(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 199
    if-nez v0, :cond_0

    .line 200
    sget-object v0, Lcom/dropbox/android/widget/LocalItemView;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load media icon type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    if-eqz p1, :cond_1

    const-string v0, "folder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "folder"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 208
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 209
    iget-object v1, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 211
    iget-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->k:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->l:Z

    if-nez v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    sget-object v1, Lcom/dropbox/android/widget/LocalItemView;->o:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 219
    :goto_1
    return-void

    .line 204
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "page_white"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/an;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    sget-object v1, Lcom/dropbox/android/widget/LocalItemView;->n:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_1

    .line 217
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;ZLjava/util/Set;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Z",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Lcom/dropbox/android/widget/LocalItemView;->a()V

    .line 109
    iget v0, p0, Lcom/dropbox/android/widget/LocalItemView;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/LocalItemView;->m:I

    .line 111
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 112
    sget-object v2, Lcom/dropbox/android/widget/bf;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 126
    new-instance v0, Ljava/io/File;

    const-string v2, "path"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 127
    const-string v0, "filename"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 128
    const-string v0, "is_dir"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->l:Z

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->l:Z

    if-nez v0, :cond_0

    .line 133
    const-string v0, "size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 134
    const-string v0, "modified"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/LocalItemView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v4, v5, v8}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    .line 138
    if-nez v6, :cond_3

    .line 144
    :goto_0
    iget-object v4, p0, Lcom/dropbox/android/widget/LocalItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    iget-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->k:Z

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 152
    :cond_0
    if-eqz p2, :cond_1

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 154
    iget-object v4, p0, Lcom/dropbox/android/widget/LocalItemView;->j:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->l:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->j:Landroid/widget/CheckBox;

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 158
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/widget/LocalItemView;->l:Z

    if-eqz v0, :cond_5

    .line 159
    const-string v0, "folder"

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/LocalItemView;->b(Ljava/lang/String;)V

    .line 175
    :cond_2
    :goto_2
    return-void

    .line 115
    :pswitch_0
    const-string v0, "_up_folder"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0058

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 118
    iget-object v2, p0, Lcom/dropbox/android/widget/LocalItemView;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    const v2, 0x7f02010f

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->e:Landroid/widget/ImageView;

    sget-object v1, Lcom/dropbox/android/widget/LocalItemView;->n:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_2

    .line 141
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/widget/LocalItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d007d

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v1

    aput-object v6, v7, v8

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 154
    goto :goto_1

    .line 160
    :cond_5
    invoke-static {v3}, Lcom/dropbox/android/util/ab;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 162
    iget-object v0, p0, Lcom/dropbox/android/widget/LocalItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    .line 164
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/dropbox/android/widget/LocalItemView;->m:I

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/dropbox/android/widget/LocalItemView;->b:Lcom/dropbox/android/widget/bg;

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_2

    .line 167
    iget-object v0, v0, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/LocalItemView;->a(Landroid/graphics/Bitmap;Z)V

    goto :goto_2

    .line 170
    :cond_6
    invoke-static {v3}, Lcom/dropbox/android/util/ab;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/LocalItemView;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
