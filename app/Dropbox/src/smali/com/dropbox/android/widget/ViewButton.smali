.class public Lcom/dropbox/android/widget/ViewButton;
.super Landroid/widget/LinearLayout;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ViewButton;->a()V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ViewButton;->a()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ViewButton;->a()V

    .line 20
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 33
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ViewButton;->setFocusable(Z)V

    .line 34
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/ViewButton;->setClickable(Z)V

    .line 35
    return-void
.end method
