.class public Lcom/dropbox/android/widget/ao;
.super Landroid/support/v4/widget/a;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected final a:Lcom/dropbox/android/util/o;

.field private c:Lcom/dropbox/android/widget/aq;

.field private final d:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field private final e:Landroid/support/v4/app/Fragment;

.field private final f:Lcom/dropbox/android/widget/as;

.field private final g:Ldbxyzptlk/db231222/r/d;

.field private final h:Lcom/dropbox/android/filemanager/I;

.field private i:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/ao;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/I;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 72
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v8, v2}, Landroid/support/v4/widget/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 60
    new-instance v0, Lcom/dropbox/android/widget/aq;

    invoke-direct {v0}, Lcom/dropbox/android/widget/aq;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/ao;->c:Lcom/dropbox/android/widget/aq;

    .line 221
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/ao;->i:Ljava/util/concurrent/BlockingQueue;

    .line 223
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/dropbox/android/widget/ao;->i:Ljava/util/concurrent/BlockingQueue;

    new-instance v7, Lcom/dropbox/android/widget/ar;

    invoke-direct {v7, v8}, Lcom/dropbox/android/widget/ar;-><init>(Lcom/dropbox/android/widget/ap;)V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/ao;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 73
    iput-object p1, p0, Lcom/dropbox/android/widget/ao;->d:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 74
    iput-object p2, p0, Lcom/dropbox/android/widget/ao;->e:Landroid/support/v4/app/Fragment;

    .line 75
    iput-object p3, p0, Lcom/dropbox/android/widget/ao;->f:Lcom/dropbox/android/widget/as;

    .line 76
    new-instance v0, Lcom/dropbox/android/util/o;

    invoke-direct {v0}, Lcom/dropbox/android/util/o;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/util/o;

    .line 77
    iput-object p4, p0, Lcom/dropbox/android/widget/ao;->g:Ldbxyzptlk/db231222/r/d;

    .line 78
    iput-object p5, p0, Lcom/dropbox/android/widget/ao;->h:Lcom/dropbox/android/filemanager/I;

    .line 79
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/dropbox/android/widget/ao;->d:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/dropbox/android/widget/ao;->d:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c()V

    .line 198
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/widget/ao;->c:Lcom/dropbox/android/widget/aq;

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/aq;->a(Lcom/dropbox/android/widget/aq;I)V

    .line 83
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 155
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p3, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/dropbox/android/widget/ao;->c:Lcom/dropbox/android/widget/aq;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/aq;->a(I)Z

    move-result v4

    .line 157
    sget-object v1, Lcom/dropbox/android/widget/ap;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 170
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 159
    :pswitch_1
    check-cast p1, Lcom/dropbox/android/widget/UpFolderView;

    invoke-virtual {p1, p3}, Lcom/dropbox/android/widget/UpFolderView;->a(Landroid/database/Cursor;)V

    .line 172
    :goto_0
    return-void

    .line 162
    :pswitch_2
    check-cast p1, Lcom/dropbox/android/widget/InProgressUploadView;

    iget-object v0, p0, Lcom/dropbox/android/widget/ao;->d:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/ao;->e:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, p3, v0, v1, v4}, Lcom/dropbox/android/widget/InProgressUploadView;->a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Z)V

    goto :goto_0

    :pswitch_3
    move-object v0, p1

    .line 166
    check-cast v0, Lcom/dropbox/android/widget/DropboxEntryView;

    iget-object v2, p0, Lcom/dropbox/android/widget/ao;->d:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    iget-object v3, p0, Lcom/dropbox/android/widget/ao;->e:Landroid/support/v4/app/Fragment;

    iget-object v5, p0, Lcom/dropbox/android/widget/ao;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;ZLjava/util/concurrent/ThreadPoolExecutor;)V

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/dropbox/android/widget/ao;->a()V

    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/widget/a;->changeCursor(Landroid/database/Cursor;)V

    .line 178
    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ao;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 236
    return-void

    .line 234
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ao;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 119
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 120
    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 121
    sget-object v1, Lcom/dropbox/android/widget/ap;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 131
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 123
    :pswitch_0
    const/4 v0, 0x0

    .line 129
    :goto_0
    return v0

    .line 125
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 127
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 129
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x6

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/dropbox/android/widget/ao;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/ao;->mDataValid:Z

    if-nez v0, :cond_1

    .line 205
    :cond_0
    const/4 v0, 0x0

    .line 208
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Landroid/support/v4/widget/a;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/dropbox/android/widget/ao;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 88
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 90
    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v2, v3}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v3

    .line 91
    sget-object v4, Lcom/dropbox/android/widget/ap;->a:[I

    invoke-virtual {v3}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 107
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 93
    :pswitch_1
    invoke-static {v2}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    .line 94
    iget-object v3, p0, Lcom/dropbox/android/widget/ao;->f:Lcom/dropbox/android/widget/as;

    sget-object v4, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    if-ne v3, v4, :cond_1

    iget-boolean v3, v2, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/dropbox/android/widget/ao;->h:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v3, v2}, Lcom/dropbox/android/filemanager/I;->c(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v2, v0

    .line 96
    :goto_1
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    .line 94
    goto :goto_1

    .line 101
    :pswitch_2
    new-instance v3, Ldbxyzptlk/db231222/j/l;

    const-string v4, "id"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ldbxyzptlk/db231222/j/l;-><init>(J)V

    .line 102
    iget-object v2, p0, Lcom/dropbox/android/widget/ao;->h:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v2

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v2

    .line 105
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ldbxyzptlk/db231222/j/g;->c()Lcom/dropbox/android/taskqueue/w;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 139
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p2, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 140
    sget-object v1, Lcom/dropbox/android/widget/ap;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 149
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :pswitch_1
    new-instance v0, Lcom/dropbox/android/widget/UpFolderView;

    invoke-direct {v0, p1}, Lcom/dropbox/android/widget/UpFolderView;-><init>(Landroid/content/Context;)V

    .line 147
    :goto_0
    return-object v0

    .line 144
    :pswitch_2
    new-instance v0, Lcom/dropbox/android/widget/InProgressUploadView;

    iget-object v1, p0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/util/o;

    iget-object v2, p0, Lcom/dropbox/android/widget/ao;->h:Lcom/dropbox/android/filemanager/I;

    invoke-direct {v0, p1, v1, v2}, Lcom/dropbox/android/widget/InProgressUploadView;-><init>(Landroid/content/Context;Lcom/dropbox/android/util/o;Lcom/dropbox/android/filemanager/I;)V

    goto :goto_0

    .line 147
    :pswitch_3
    new-instance v0, Lcom/dropbox/android/widget/DropboxEntryView;

    iget-object v1, p0, Lcom/dropbox/android/widget/ao;->f:Lcom/dropbox/android/widget/as;

    iget-object v2, p0, Lcom/dropbox/android/widget/ao;->g:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v0, p1, v1, v2}, Lcom/dropbox/android/widget/DropboxEntryView;-><init>(Landroid/content/Context;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onContentChanged()V
    .locals 2

    .prologue
    .line 188
    sget-object v0, Lcom/dropbox/android/widget/ao;->b:Ljava/lang/String;

    const-string v1, "onContentChanged"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-direct {p0}, Lcom/dropbox/android/widget/ao;->a()V

    .line 190
    invoke-super {p0}, Landroid/support/v4/widget/a;->onContentChanged()V

    .line 192
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/dropbox/android/widget/ao;->a()V

    .line 183
    invoke-super {p0, p1}, Landroid/support/v4/widget/a;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
