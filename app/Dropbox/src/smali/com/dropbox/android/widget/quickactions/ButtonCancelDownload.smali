.class public Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final d:Lcom/dropbox/android/filemanager/LocalEntry;

.field private final e:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/I;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 18
    iput-object p2, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;->e:Lcom/dropbox/android/filemanager/I;

    .line 19
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f030081

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;->e:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v1}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Ljava/lang/String;)Z

    .line 25
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f0d01ec

    return v0
.end method
