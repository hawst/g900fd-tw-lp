.class public Lcom/dropbox/android/widget/PhotosTabHouseAdBar;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/widget/ViewButton;

.field private final b:Lcom/dropbox/android/widget/bY;

.field private final c:Lcom/dropbox/android/widget/bZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/br;)V
    .locals 6

    .prologue
    .line 76
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v0, Lcom/dropbox/android/widget/bn;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bn;-><init>(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->c:Lcom/dropbox/android/widget/bZ;

    .line 78
    const v0, 0x7f030070

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 79
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->addView(Landroid/view/View;)V

    .line 81
    const v0, 0x7f070144

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    const v1, 0x7f070145

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 83
    const v2, 0x7f070142

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 85
    const v3, 0x7f070141

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/dropbox/android/widget/ViewButton;

    iput-object v3, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a:Lcom/dropbox/android/widget/ViewButton;

    .line 86
    iget-object v3, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a:Lcom/dropbox/android/widget/ViewButton;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/dropbox/android/widget/ViewButton;->setClickable(Z)V

    .line 89
    new-instance v3, Lcom/dropbox/android/widget/bo;

    invoke-direct {v3, p0}, Lcom/dropbox/android/widget/bo;-><init>(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;)V

    invoke-virtual {v4, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 98
    sget-object v3, Lcom/dropbox/android/widget/br;->a:Lcom/dropbox/android/widget/br;

    invoke-virtual {p5, v3}, Lcom/dropbox/android/widget/br;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 99
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cp()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "location"

    const-string v5, "photos-tab"

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 100
    iget-object v3, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a:Lcom/dropbox/android/widget/ViewButton;

    new-instance v4, Lcom/dropbox/android/widget/bp;

    invoke-direct {v4, p0, p1}, Lcom/dropbox/android/widget/bp;-><init>(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Lcom/dropbox/android/widget/ViewButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    const v3, 0x7f0d032c

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 109
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    const v0, 0x7f02021d

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 125
    :cond_0
    :goto_0
    iput-object p4, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b:Lcom/dropbox/android/widget/bY;

    .line 127
    invoke-virtual {p2, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 128
    return-void

    .line 111
    :cond_1
    sget-object v3, Lcom/dropbox/android/widget/br;->b:Lcom/dropbox/android/widget/br;

    invoke-virtual {p5, v3}, Lcom/dropbox/android/widget/br;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 112
    iget-object v3, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a:Lcom/dropbox/android/widget/ViewButton;

    new-instance v4, Lcom/dropbox/android/widget/bq;

    invoke-direct {v4, p0, p1, p3}, Lcom/dropbox/android/widget/bq;-><init>(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;Landroid/content/Context;Ldbxyzptlk/db231222/n/P;)V

    invoke-virtual {v3, v4}, Lcom/dropbox/android/widget/ViewButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v3, 0x7f0d0161

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 121
    const v0, 0x7f0d017d

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 122
    const v0, 0x7f0200e7

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;)Lcom/dropbox/android/widget/bY;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b:Lcom/dropbox/android/widget/bY;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150
    invoke-virtual {p0}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3eb33333    # 0.35f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 152
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 153
    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 154
    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 155
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 156
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->setEnabled(Z)V

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a:Lcom/dropbox/android/widget/ViewButton;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/ViewButton;->setClickable(Z)V

    .line 159
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 162
    invoke-virtual {p0}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3eb33333    # 0.35f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 164
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 165
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 166
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 167
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 168
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->setEnabled(Z)V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a:Lcom/dropbox/android/widget/ViewButton;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/ViewButton;->setClickable(Z)V

    .line 171
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->a()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/PhotosTabHouseAdBar;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b()V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 133
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b:Lcom/dropbox/android/widget/bY;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b:Lcom/dropbox/android/widget/bY;

    iget-object v1, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->c:Lcom/dropbox/android/widget/bZ;

    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/bY;->a(Lcom/dropbox/android/widget/bZ;)V

    .line 136
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b:Lcom/dropbox/android/widget/bY;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->b:Lcom/dropbox/android/widget/bY;

    iget-object v1, p0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;->c:Lcom/dropbox/android/widget/bZ;

    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/bY;->b(Lcom/dropbox/android/widget/bZ;)V

    .line 144
    :cond_0
    return-void
.end method
