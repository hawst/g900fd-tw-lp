.class final Lcom/dropbox/android/widget/af;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/util/DropboxPath;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ldbxyzptlk/db231222/v/n;

.field final synthetic d:Lcom/dropbox/android/widget/DropboxEntryView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/DropboxEntryView;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/dropbox/android/widget/af;->d:Lcom/dropbox/android/widget/DropboxEntryView;

    iput-object p2, p0, Lcom/dropbox/android/widget/af;->a:Lcom/dropbox/android/util/DropboxPath;

    iput-object p3, p0, Lcom/dropbox/android/widget/af;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/dropbox/android/widget/af;->c:Ldbxyzptlk/db231222/v/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 297
    iget-object v0, p0, Lcom/dropbox/android/widget/af;->d:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/widget/DropboxEntryView;)Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    iget-object v2, p0, Lcom/dropbox/android/widget/af;->a:Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p0, Lcom/dropbox/android/widget/af;->b:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Landroid/util/Pair;

    move-result-object v0

    .line 299
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 300
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/dropbox/android/widget/af;->d:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/DropboxEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 301
    iget-object v0, p0, Lcom/dropbox/android/widget/af;->d:Lcom/dropbox/android/widget/DropboxEntryView;

    new-instance v2, Lcom/dropbox/android/widget/ag;

    invoke-direct {v2, p0, v1}, Lcom/dropbox/android/widget/ag;-><init>(Lcom/dropbox/android/widget/af;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/DropboxEntryView;->post(Ljava/lang/Runnable;)Z

    .line 321
    :cond_0
    return-void
.end method
