.class public Lcom/dropbox/android/widget/HeightMatchingSquareLayout;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field private final a:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->a:Landroid/graphics/Rect;

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->a:Landroid/graphics/Rect;

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->a:Landroid/graphics/Rect;

    .line 23
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getChildCount()I

    move-result v9

    move v8, v3

    move v6, v3

    move v7, v3

    .line 47
    :goto_0
    if-ge v8, v9, :cond_1

    .line 48
    invoke-virtual {p0, v8}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 49
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getMeasureAllChildren()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_4

    :cond_0
    move-object v0, p0

    move v2, p1

    move v4, p1

    move v5, v3

    .line 50
    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 51
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 52
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 47
    :goto_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v6, v0

    move v7, v1

    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 57
    iget-object v0, p0, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->a:Landroid/graphics/Rect;

    .line 60
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 61
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 63
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    add-int/2addr v1, v6

    .line 64
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    add-int/2addr v0, v7

    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 72
    if-eqz v1, :cond_3

    .line 73
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    .line 74
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 77
    :cond_3
    invoke-static {v0, p1}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/widget/HeightMatchingSquareLayout;->setMeasuredDimension(II)V

    .line 79
    return-void

    :cond_4
    move v0, v6

    move v1, v7

    goto :goto_1
.end method
