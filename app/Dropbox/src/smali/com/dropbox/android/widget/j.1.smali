.class final enum Lcom/dropbox/android/widget/j;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/widget/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/j;

.field public static final enum b:Lcom/dropbox/android/widget/j;

.field public static final enum c:Lcom/dropbox/android/widget/j;

.field private static final synthetic d:[Lcom/dropbox/android/widget/j;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/dropbox/android/widget/j;

    const-string v1, "SEPARATOR"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/j;

    .line 27
    new-instance v0, Lcom/dropbox/android/widget/j;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/j;->b:Lcom/dropbox/android/widget/j;

    .line 28
    new-instance v0, Lcom/dropbox/android/widget/j;

    const-string v1, "EXPAND_LIGHTWEIGHT_SHARES"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/widget/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/j;->c:Lcom/dropbox/android/widget/j;

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/widget/j;

    sget-object v1, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/widget/j;->b:Lcom/dropbox/android/widget/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/j;->c:Lcom/dropbox/android/widget/j;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/widget/j;->d:[Lcom/dropbox/android/widget/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/j;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/dropbox/android/widget/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/j;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/j;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/dropbox/android/widget/j;->d:[Lcom/dropbox/android/widget/j;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/j;

    return-object v0
.end method
