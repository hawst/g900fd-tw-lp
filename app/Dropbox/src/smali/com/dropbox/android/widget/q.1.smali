.class final Lcom/dropbox/android/widget/q;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/n;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/n;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1159
    iput-object p1, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    .line 1160
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1161
    return-void
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1177
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x52

    if-ne v1, v2, :cond_2

    .line 1178
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 1180
    invoke-virtual {p0}, Lcom/dropbox/android/widget/q;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 1181
    if-eqz v1, :cond_2

    .line 1182
    invoke-virtual {v1, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 1192
    :goto_0
    return v0

    .line 1185
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1188
    iget-object v1, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/n;->c()V

    goto :goto_0

    .line 1192
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-static {v0}, Lcom/dropbox/android/widget/n;->f(Lcom/dropbox/android/widget/n;)Landroid/view/View$OnTouchListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-static {v0}, Lcom/dropbox/android/widget/n;->f(Lcom/dropbox/android/widget/n;)Landroid/view/View$OnTouchListener;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1198
    const/4 v0, 0x1

    .line 1200
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-static {v0}, Lcom/dropbox/android/widget/n;->e(Lcom/dropbox/android/widget/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1167
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1168
    invoke-static {}, Lcom/dropbox/android/widget/n;->d()[I

    move-result-object v1

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    .line 1171
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1205
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 1206
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 1208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    if-ltz v1, :cond_0

    invoke-super {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    if-ge v1, v3, :cond_0

    if-ltz v2, :cond_0

    invoke-super {p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    if-lt v2, v1, :cond_1

    .line 1210
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/n;->c()V

    .line 1216
    :goto_0
    return v0

    .line 1212
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 1213
    iget-object v1, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/n;->c()V

    goto :goto_0

    .line 1216
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final sendAccessibilityEvent(I)V
    .locals 1

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-static {v0}, Lcom/dropbox/android/widget/n;->g(Lcom/dropbox/android/widget/n;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/dropbox/android/widget/q;->a:Lcom/dropbox/android/widget/n;

    invoke-static {v0}, Lcom/dropbox/android/widget/n;->g(Lcom/dropbox/android/widget/n;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1228
    :goto_0
    return-void

    .line 1226
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method
