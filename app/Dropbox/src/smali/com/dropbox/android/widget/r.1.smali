.class public final Lcom/dropbox/android/widget/r;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 38
    const v0, 0x7f03001e

    invoke-virtual {p0, v0, p1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/RotatableFrameLayout;

    .line 39
    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setRotationMaxBackport(F)V

    .line 40
    new-instance v3, Lcom/dropbox/android/widget/t;

    invoke-direct {v3, v9}, Lcom/dropbox/android/widget/t;-><init>(Lcom/dropbox/android/widget/s;)V

    .line 41
    iput-object v0, v3, Lcom/dropbox/android/widget/t;->a:Lcom/dropbox/android/widget/RotatableFrameLayout;

    .line 42
    const v1, 0x7f07005b

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v3, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    .line 43
    const v1, 0x7f07005d

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v3, Lcom/dropbox/android/widget/t;->c:Landroid/view/View;

    .line 44
    const v1, 0x7f07005c

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v3, Lcom/dropbox/android/widget/t;->d:Landroid/view/View;

    .line 45
    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/dropbox/android/widget/t;->e:Landroid/widget/TextView;

    .line 46
    iput p3, v3, Lcom/dropbox/android/widget/t;->f:I

    .line 47
    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setTag(Ljava/lang/Object;)V

    .line 50
    iget-object v1, v3, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    .line 51
    iget-object v2, v3, Lcom/dropbox/android/widget/t;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, v8}, Landroid/graphics/drawable/BitmapDrawable;->setFilterBitmap(Z)V

    .line 52
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 53
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/NinePatchDrawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 54
    iget-object v1, v3, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget v6, v2, Landroid/graphics/Rect;->right:I

    iget v7, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 55
    iget-object v1, v3, Lcom/dropbox/android/widget/t;->d:Landroid/view/View;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget v6, v2, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v4, v5, v6, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 59
    invoke-virtual {p1}, Landroid/view/ViewGroup;->isHardwareAccelerated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, v3, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v8, v9}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 61
    iget-object v1, v3, Lcom/dropbox/android/widget/t;->c:Landroid/view/View;

    invoke-virtual {v1, v8, v9}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 67
    :cond_0
    invoke-static {v0, v10}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;Z)V

    .line 69
    return-object v0
.end method

.method public static a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/t;

    iget-object v0, v0, Lcom/dropbox/android/widget/t;->a:Lcom/dropbox/android/widget/RotatableFrameLayout;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setRotationBackport(F)V

    .line 74
    return-void
.end method

.method public static a(Landroid/view/View;II)V
    .locals 5

    .prologue
    .line 77
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/t;

    .line 78
    iget v1, v0, Lcom/dropbox/android/widget/t;->f:I

    if-gt p2, v1, :cond_0

    iget v1, v0, Lcom/dropbox/android/widget/t;->f:I

    if-le p1, v1, :cond_1

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid padding offset, bigger than max random padding"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    iget v1, v0, Lcom/dropbox/android/widget/t;->f:I

    sub-int/2addr v1, p1

    .line 82
    iget v2, v0, Lcom/dropbox/android/widget/t;->f:I

    sub-int/2addr v2, v1

    .line 83
    iget v3, v0, Lcom/dropbox/android/widget/t;->f:I

    sub-int/2addr v3, p2

    .line 84
    iget v4, v0, Lcom/dropbox/android/widget/t;->f:I

    sub-int/2addr v4, v3

    .line 85
    iget-object v0, v0, Lcom/dropbox/android/widget/t;->a:Lcom/dropbox/android/widget/RotatableFrameLayout;

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/dropbox/android/widget/RotatableFrameLayout;->setPadding(IIII)V

    .line 86
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;ZJZ)V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 110
    if-nez p1, :cond_1

    .line 111
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 114
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/t;

    .line 115
    iget-object v1, v0, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    iget-object v4, v0, Lcom/dropbox/android/widget/t;->c:Landroid/view/View;

    if-eqz p2, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    if-eqz p2, :cond_2

    .line 118
    iget-object v1, v0, Lcom/dropbox/android/widget/t;->e:Landroid/widget/TextView;

    cmp-long v4, p3, v5

    if-eqz v4, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    cmp-long v1, p3, v5

    if-eqz v1, :cond_2

    .line 120
    iget-object v1, v0, Lcom/dropbox/android/widget/t;->e:Landroid/widget/TextView;

    invoke-static {p3, p4}, Lcom/dropbox/android/util/UIHelpers;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :cond_2
    if-eqz p5, :cond_0

    .line 125
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 126
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 127
    iget-object v0, v0, Lcom/dropbox/android/widget/t;->a:Lcom/dropbox/android/widget/RotatableFrameLayout;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/RotatableFrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_3
    move v1, v3

    .line 116
    goto :goto_1

    :cond_4
    move v2, v3

    .line 118
    goto :goto_2
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 94
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/t;

    .line 95
    if-eqz p1, :cond_0

    .line 96
    iget-object v1, v0, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    const/16 v2, 0xb3

    invoke-static {v2, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 97
    iget-object v0, v0, Lcom/dropbox/android/widget/t;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x55

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v1, v0, Lcom/dropbox/android/widget/t;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 100
    iget-object v0, v0, Lcom/dropbox/android/widget/t;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xaa

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;ZLandroid/view/animation/Animation$AnimationListener;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 143
    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_0

    .line 144
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 146
    if-eqz p1, :cond_1

    .line 147
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/high16 v2, 0x41a00000    # 20.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 149
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 150
    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 151
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 152
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    move-object v0, v1

    .line 157
    :goto_0
    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 158
    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 159
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 161
    :cond_0
    return-void

    :cond_1
    move-object v0, v7

    .line 155
    goto :goto_0
.end method

.method public static a(Landroid/view/View;)[I
    .locals 5

    .prologue
    .line 89
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/t;

    .line 90
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, v0, Lcom/dropbox/android/widget/t;->f:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v0, v0, Lcom/dropbox/android/widget/t;->f:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    sub-int/2addr v0, v3

    aput v0, v1, v2

    return-object v1
.end method
