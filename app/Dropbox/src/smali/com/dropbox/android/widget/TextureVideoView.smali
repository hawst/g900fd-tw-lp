.class public Lcom/dropbox/android/widget/TextureVideoView;
.super Landroid/view/TextureView;
.source "panda.py"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Landroid/net/Uri;

.field private d:Landroid/view/Surface;

.field private e:Landroid/media/MediaPlayer;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Landroid/media/MediaPlayer$OnPreparedListener;

.field private m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private n:Landroid/media/MediaPlayer$OnErrorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 136
    new-instance v0, Lcom/dropbox/android/widget/bS;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bS;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->l:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 168
    new-instance v0, Lcom/dropbox/android/widget/bT;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bT;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 180
    new-instance v0, Lcom/dropbox/android/widget/bU;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bU;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->n:Landroid/media/MediaPlayer$OnErrorListener;

    .line 41
    iput-object p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->b:Landroid/content/Context;

    .line 42
    invoke-virtual {p0, p0}, Lcom/dropbox/android/widget/TextureVideoView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 136
    new-instance v0, Lcom/dropbox/android/widget/bS;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bS;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->l:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 168
    new-instance v0, Lcom/dropbox/android/widget/bT;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bT;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 180
    new-instance v0, Lcom/dropbox/android/widget/bU;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bU;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->n:Landroid/media/MediaPlayer$OnErrorListener;

    .line 47
    iput-object p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->b:Landroid/content/Context;

    .line 48
    invoke-virtual {p0, p0}, Lcom/dropbox/android/widget/TextureVideoView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 136
    new-instance v0, Lcom/dropbox/android/widget/bS;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bS;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->l:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 168
    new-instance v0, Lcom/dropbox/android/widget/bT;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bT;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 180
    new-instance v0, Lcom/dropbox/android/widget/bU;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bU;-><init>(Lcom/dropbox/android/widget/TextureVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->n:Landroid/media/MediaPlayer$OnErrorListener;

    .line 53
    iput-object p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->b:Landroid/content/Context;

    .line 54
    invoke-virtual {p0, p0}, Lcom/dropbox/android/widget/TextureVideoView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/TextureVideoView;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/TextureVideoView;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->k:Z

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/TextureVideoView;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/TextureVideoView;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/TextureVideoView;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dropbox/android/widget/TextureVideoView;->e()V

    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/TextureVideoView;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/TextureVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 193
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->d:Landroid/view/Surface;

    if-nez v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 198
    sget-object v0, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    const-string v1, "MediaPlayer tried to openVideo twice"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_2
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->k:Z

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    .line 205
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->d:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->l:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->n:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 212
    :catch_0
    move-exception v0

    .line 213
    sget-object v1, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 215
    :catch_1
    move-exception v0

    .line 216
    sget-object v1, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 218
    :catch_2
    move-exception v0

    .line 219
    sget-object v1, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 221
    :catch_3
    move-exception v0

    .line 222
    sget-object v1, Lcom/dropbox/android/widget/TextureVideoView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    if-nez v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    .line 236
    iput-boolean v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->k:Z

    .line 241
    :cond_0
    :goto_0
    iput-boolean v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->j:Z

    .line 242
    iput-boolean v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->i:Z

    .line 243
    return-void

    .line 237
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    if-eqz v0, :cond_0

    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->k:Z

    goto :goto_0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/TextureVideoView;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->j:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 112
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->j:Z

    .line 113
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 121
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 123
    :cond_0
    iput-boolean v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->j:Z

    .line 124
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/TextureVideoView;->getDefaultSize(II)I

    move-result v1

    .line 62
    iget v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    invoke-static {v0, p2}, Lcom/dropbox/android/widget/TextureVideoView;->getDefaultSize(II)I

    move-result v0

    .line 64
    iget v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    if-lez v2, :cond_0

    .line 65
    iget v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_1

    .line 66
    iget v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    div-int/2addr v0, v2

    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/widget/TextureVideoView;->setMeasuredDimension(II)V

    .line 73
    return-void

    .line 67
    :cond_1
    iget v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    .line 68
    iget v1, p0, Lcom/dropbox/android/widget/TextureVideoView;->f:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/dropbox/android/widget/TextureVideoView;->g:I

    div-int/2addr v1, v2

    goto :goto_0
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->d:Landroid/view/Surface;

    .line 78
    invoke-direct {p0}, Lcom/dropbox/android/widget/TextureVideoView;->d()V

    .line 79
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/dropbox/android/widget/TextureVideoView;->e()V

    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public setLooping(Z)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->h:Z

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/widget/TextureVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 133
    :cond_0
    iput-boolean p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->i:Z

    .line 134
    return-void
.end method

.method public setVideoUri(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/dropbox/android/widget/TextureVideoView;->c:Landroid/net/Uri;

    .line 101
    invoke-direct {p0}, Lcom/dropbox/android/widget/TextureVideoView;->d()V

    .line 102
    return-void
.end method
