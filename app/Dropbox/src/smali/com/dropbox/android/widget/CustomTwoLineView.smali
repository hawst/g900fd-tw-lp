.class public Lcom/dropbox/android/widget/CustomTwoLineView;
.super Landroid/widget/LinearLayout;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030028

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 51
    if-eqz p2, :cond_0

    .line 52
    sget-object v0, Lcom/dropbox/android/j;->CustomTwoLineView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setTitle(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    .line 55
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setBottomBarVisible(Z)V

    .line 56
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setTopBarVisible(Z)V

    .line 57
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 58
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setCheckboxVisible(Z)V

    .line 59
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setAppearanceDisabled(Z)V

    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f070088

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setAppearanceDisabled(Z)V
    .locals 3

    .prologue
    const v2, 0x7f080084

    .line 114
    if-eqz p1, :cond_0

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->setEnabled(Z)V

    .line 116
    const v0, 0x7f070088

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/CustomTwoLineView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 117
    const v0, 0x7f070089

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/CustomTwoLineView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 119
    :cond_0
    return-void
.end method

.method public setBottomBarVisible(Z)V
    .locals 2

    .prologue
    .line 84
    const v0, 0x7f07004b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 86
    return-void

    .line 85
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setCheckboxVisible(Z)V
    .locals 2

    .prologue
    .line 89
    const v0, 0x7f07008a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 90
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 91
    return-void

    .line 90
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 126
    const v0, 0x7f07008a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    const v0, 0x7f070089

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 94
    const v0, 0x7f070087

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 96
    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    const v0, 0x7f070089

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CustomTwoLineView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 104
    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f070088

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 66
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void
.end method

.method public setTopBarVisible(Z)V
    .locals 2

    .prologue
    .line 79
    const v0, 0x7f070086

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 80
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 81
    return-void

    .line 80
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
