.class final Lcom/dropbox/android/widget/ae;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/graphics/drawable/Drawable;

.field final synthetic b:Lcom/dropbox/android/widget/ad;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/ad;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iput-object p2, p0, Lcom/dropbox/android/widget/ae;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 245
    iget-object v0, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v0, v0, Lcom/dropbox/android/widget/ad;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v1, v1, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DropboxEntryView;->b(Lcom/dropbox/android/widget/DropboxEntryView;)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v0, v0, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/ae;->a:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v3, v3, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v3}, Lcom/dropbox/android/widget/DropboxEntryView;->c(Lcom/dropbox/android/widget/DropboxEntryView;)Z

    move-result v3

    iget-object v4, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v4, v4, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v4}, Lcom/dropbox/android/widget/DropboxEntryView;->d(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v5, v5, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v5}, Lcom/dropbox/android/widget/DropboxEntryView;->e(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/FrameLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v6, v6, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v6}, Lcom/dropbox/android/widget/DropboxEntryView;->f(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/widget/ae;->b:Lcom/dropbox/android/widget/ad;

    iget-object v7, v7, Lcom/dropbox/android/widget/ad;->b:Lcom/dropbox/android/widget/DropboxEntryView;

    invoke-static {v7}, Lcom/dropbox/android/widget/DropboxEntryView;->g(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 255
    :cond_0
    return-void
.end method
