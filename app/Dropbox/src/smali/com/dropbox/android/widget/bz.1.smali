.class public abstract Lcom/dropbox/android/widget/bz;
.super Landroid/widget/BaseAdapter;
.source "panda.py"


# instance fields
.field protected final a:Landroid/content/pm/PackageManager;

.field private final b:[Landroid/content/Intent;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/widget/bB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;Ljava/util/Comparator;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "[",
            "Landroid/content/Intent;",
            "[",
            "Landroid/content/Intent;",
            "Lcom/dropbox/android/util/c;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/dropbox/android/widget/bB;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 78
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    .line 79
    invoke-virtual {p2}, [Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    iput-object v0, p0, Lcom/dropbox/android/widget/bz;->b:[Landroid/content/Intent;

    .line 80
    iput-object p1, p0, Lcom/dropbox/android/widget/bz;->a:Landroid/content/pm/PackageManager;

    .line 82
    if-nez p5, :cond_0

    .line 83
    new-instance p5, Lcom/dropbox/android/widget/bA;

    invoke-direct {p5, p0}, Lcom/dropbox/android/widget/bA;-><init>(Lcom/dropbox/android/widget/bz;)V

    .line 94
    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 97
    if-eqz p3, :cond_6

    .line 98
    array-length v5, p3

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_6

    aget-object v1, p3, v3

    .line 99
    if-nez v1, :cond_2

    .line 98
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v6

    .line 103
    if-nez v6, :cond_3

    .line 104
    const-string v0, "ResolveListAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No activity found for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 107
    :cond_3
    if-eqz p4, :cond_4

    invoke-virtual {p4, v6}, Lcom/dropbox/android/util/c;->a(Landroid/content/pm/ComponentInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    :cond_4
    new-instance v7, Landroid/content/pm/ResolveInfo;

    invoke-direct {v7}, Landroid/content/pm/ResolveInfo;-><init>()V

    .line 111
    iput-object v6, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 112
    instance-of v0, v1, Landroid/content/pm/LabeledIntent;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 113
    check-cast v0, Landroid/content/pm/LabeledIntent;

    .line 114
    invoke-virtual {v0}, Landroid/content/pm/LabeledIntent;->getSourcePackage()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    .line 115
    invoke-virtual {v0}, Landroid/content/pm/LabeledIntent;->getLabelResource()I

    move-result v8

    iput v8, v7, Landroid/content/pm/ResolveInfo;->labelRes:I

    .line 116
    invoke-virtual {v0}, Landroid/content/pm/LabeledIntent;->getNonLocalizedLabel()Ljava/lang/CharSequence;

    move-result-object v8

    iput-object v8, v7, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    .line 117
    invoke-virtual {v0}, Landroid/content/pm/LabeledIntent;->getIconResource()I

    move-result v0

    iput v0, v7, Landroid/content/pm/ResolveInfo;->icon:I

    .line 119
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    new-instance v8, Lcom/dropbox/android/widget/bB;

    iget-object v9, p0, Lcom/dropbox/android/widget/bz;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v9}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-direct {v8, v7, v9, v1}, Lcom/dropbox/android/widget/bB;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v0, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 124
    :cond_6
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 126
    array-length v5, p2

    :goto_2
    if-ge v2, v5, :cond_b

    aget-object v6, p2, v2

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->a:Landroid/content/pm/PackageManager;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v6, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 129
    if-nez v0, :cond_8

    .line 126
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 133
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 134
    invoke-virtual {p0, v0, v4, p4}, Lcom/dropbox/android/widget/bz;->a(Ljava/util/List;Ljava/util/HashSet;Lcom/dropbox/android/util/c;)V

    .line 137
    :cond_9
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 138
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 139
    if-nez v1, :cond_a

    .line 140
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 142
    :cond_a
    new-instance v8, Lcom/dropbox/android/widget/bB;

    invoke-direct {v8, v0, v1, v6}, Lcom/dropbox/android/widget/bB;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 146
    :cond_b
    invoke-static {v3, p5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 148
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 202
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bB;

    .line 207
    new-instance v2, Landroid/content/Intent;

    iget-object v1, v0, Lcom/dropbox/android/widget/bB;->d:Landroid/content/Intent;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/dropbox/android/widget/bB;->d:Landroid/content/Intent;

    :goto_1
    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 208
    iget-object v0, v0, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 209
    new-instance v1, Landroid/content/ComponentName;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-object v0, v2

    .line 210
    goto :goto_0

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/widget/bz;->b:[Landroid/content/Intent;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    goto :goto_1
.end method

.method final a(Ljava/util/List;Ljava/util/HashSet;Lcom/dropbox/android/util/c;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/dropbox/android/util/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 161
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 163
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 164
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 166
    if-nez v1, :cond_1

    move-object v1, v0

    .line 170
    :cond_1
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_2

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 172
    :goto_1
    iget-object v4, v2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    .line 174
    invoke-virtual {p2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 175
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 170
    :cond_2
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto :goto_1

    .line 179
    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p3, v2}, Lcom/dropbox/android/util/c;->a(Landroid/content/pm/ComponentInfo;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 180
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 184
    :cond_4
    if-eq v0, v1, :cond_6

    .line 185
    iget v2, v1, Landroid/content/pm/ResolveInfo;->priority:I

    iget v5, v0, Landroid/content/pm/ResolveInfo;->priority:I

    if-ne v2, v5, :cond_5

    iget-boolean v2, v1, Landroid/content/pm/ResolveInfo;->isDefault:Z

    iget-boolean v0, v0, Landroid/content/pm/ResolveInfo;->isDefault:Z

    if-eq v2, v0, :cond_6

    .line 187
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 188
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 190
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 196
    :cond_6
    invoke-virtual {p2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    :cond_7
    return-void
.end method

.method public final b(I)Lcom/dropbox/android/widget/bB;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bB;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/bz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bz;->b(I)Lcom/dropbox/android/widget/bB;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 225
    int-to-long v0, p1

    return-wide v0
.end method
