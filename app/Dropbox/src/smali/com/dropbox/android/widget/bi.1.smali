.class public final Lcom/dropbox/android/widget/bi;
.super Lcom/dropbox/android/widget/bX;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/v;


# static fields
.field private static final g:I

.field private static final h:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/dropbox/android/provider/Z;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dropbox/android/provider/Z;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final j:Lcom/dropbox/android/widget/bk;

.field private final k:Lcom/dropbox/android/widget/bl;

.field private final l:Ldbxyzptlk/db231222/j/i;

.field private final m:Ldbxyzptlk/db231222/n/P;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/dropbox/android/widget/bm;->values()[Lcom/dropbox/android/widget/bm;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/dropbox/android/widget/bi;->g:I

    .line 58
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/dropbox/android/provider/Z;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    .line 59
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->e:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/bm;->a:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->d:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/bm;->b:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->f:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/bm;->c:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->l:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/bm;->d:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->k:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/bm;->e:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->m:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/bm;->f:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/dropbox/android/provider/Z;->d:Lcom/dropbox/android/provider/Z;

    sget-object v1, Lcom/dropbox/android/provider/Z;->k:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/provider/Z;->l:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/bi;->i:Ljava/util/EnumSet;

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/filemanager/I;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-virtual {p5}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/dropbox/android/widget/bX;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/taskqueue/D;)V

    .line 76
    new-instance v0, Lcom/dropbox/android/widget/bk;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/bk;-><init>(Lcom/dropbox/android/widget/bj;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bi;->j:Lcom/dropbox/android/widget/bk;

    .line 122
    new-instance v0, Lcom/dropbox/android/widget/bl;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/bl;-><init>(Lcom/dropbox/android/widget/bj;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bi;->k:Lcom/dropbox/android/widget/bl;

    .line 134
    invoke-virtual {p5}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/bi;->l:Ldbxyzptlk/db231222/j/i;

    .line 135
    iput-object p4, p0, Lcom/dropbox/android/widget/bi;->m:Ldbxyzptlk/db231222/n/P;

    .line 136
    invoke-virtual {p0, p2}, Lcom/dropbox/android/widget/bi;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 137
    return-void
.end method

.method private f(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 270
    if-eqz p1, :cond_0

    .line 271
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 272
    if-eqz v0, :cond_1

    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/Z;->d:Lcom/dropbox/android/provider/Z;

    if-ne v0, v1, :cond_1

    .line 273
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->j:Lcom/dropbox/android/widget/bk;

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/bk;->a(Lcom/dropbox/android/widget/bk;Landroid/database/Cursor;)V

    .line 277
    :goto_0
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 279
    :cond_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->j:Lcom/dropbox/android/widget/bk;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/bk;->a(Lcom/dropbox/android/widget/bk;Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 159
    sget v0, Lcom/dropbox/android/widget/bi;->g:I

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 6

    .prologue
    .line 224
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 225
    new-instance v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/widget/bi;->b:Lcom/dropbox/android/widget/bY;

    iget-object v3, p0, Lcom/dropbox/android/widget/bi;->f:Lcom/dropbox/android/widget/aG;

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/dropbox/android/widget/ThumbGridItemView;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/aG;)V

    .line 242
    :goto_0
    return-object v0

    .line 227
    :cond_0
    invoke-static {}, Lcom/dropbox/android/widget/bm;->values()[Lcom/dropbox/android/widget/bm;

    move-result-object v0

    aget-object v0, v0, p2

    .line 228
    sget-object v1, Lcom/dropbox/android/widget/bj;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 245
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->c:Landroid/content/Context;

    const v1, 0x7f03006f

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 233
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->c:Landroid/content/Context;

    const v1, 0x7f030052

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 235
    :pswitch_2
    new-instance v0, Lcom/dropbox/android/widget/CameraUploadItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/widget/bi;->b:Lcom/dropbox/android/widget/bY;

    iget-object v3, p0, Lcom/dropbox/android/widget/bi;->l:Ldbxyzptlk/db231222/j/i;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/widget/CameraUploadItemView;-><init>(Landroid/content/Context;Lcom/dropbox/android/widget/bY;Ldbxyzptlk/db231222/j/i;)V

    .line 236
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 239
    :pswitch_3
    new-instance v0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/dropbox/android/widget/bi;->m:Ldbxyzptlk/db231222/n/P;

    iget-object v4, p0, Lcom/dropbox/android/widget/bi;->b:Lcom/dropbox/android/widget/bY;

    sget-object v5, Lcom/dropbox/android/widget/br;->b:Lcom/dropbox/android/widget/br;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/br;)V

    goto :goto_0

    .line 242
    :pswitch_4
    new-instance v0, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/dropbox/android/widget/bi;->m:Ldbxyzptlk/db231222/n/P;

    iget-object v4, p0, Lcom/dropbox/android/widget/bi;->b:Lcom/dropbox/android/widget/bY;

    sget-object v5, Lcom/dropbox/android/widget/br;->a:Lcom/dropbox/android/widget/br;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/PhotosTabHouseAdBar;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/br;)V

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final a(I)Lcom/dropbox/android/filemanager/w;
    .locals 5

    .prologue
    .line 254
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 256
    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 257
    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/bi;->e(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 261
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    const/16 v2, 0x8

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 262
    new-instance v0, Lcom/dropbox/android/filemanager/w;

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v0, v3, v2}, Lcom/dropbox/android/filemanager/w;-><init>(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)V

    .line 266
    :goto_0
    return-object v0

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 266
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/database/Cursor;Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 175
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    move-object v0, p2

    .line 176
    check-cast v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/bi;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Landroid/database/Cursor;Lcom/dropbox/android/filemanager/n;)V

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->k:Lcom/dropbox/android/widget/bl;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/widget/bl;->a(Landroid/database/Cursor;Landroid/view/View;)V

    .line 220
    :goto_0
    :pswitch_0
    return-void

    .line 179
    :cond_0
    invoke-static {}, Lcom/dropbox/android/widget/bm;->values()[Lcom/dropbox/android/widget/bm;

    move-result-object v0

    aget-object v0, v0, p3

    .line 180
    sget-object v1, Lcom/dropbox/android/widget/bj;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 217
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :pswitch_1
    const-string v0, "_separator_text"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    const v0, 0x7f0700df

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->k:Lcom/dropbox/android/widget/bl;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bl;->a(I)V

    goto :goto_0

    .line 189
    :pswitch_2
    check-cast p2, Lcom/dropbox/android/widget/CameraUploadItemView;

    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->j:Lcom/dropbox/android/widget/bk;

    invoke-virtual {p2, v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->a(Lcom/dropbox/android/widget/u;)V

    goto :goto_0

    .line 199
    :pswitch_3
    sget-object v1, Lcom/dropbox/android/widget/bm;->d:Lcom/dropbox/android/widget/bm;

    if-ne v0, v1, :cond_1

    .line 200
    const v1, 0x7f0d02a9

    .line 201
    const v0, 0x7f02017d

    .line 206
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bi;->f()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 207
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v2, v5, v5, v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 208
    const v0, 0x7f070140

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 209
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 210
    invoke-virtual {v0, v4, v4, v2, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 203
    :cond_1
    const v1, 0x7f0d02aa

    .line 204
    const v0, 0x7f02017e

    goto :goto_1

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/bi;->e(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/dropbox/android/widget/bi;->i:Ljava/util/EnumSet;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    .line 154
    sget-object v0, Lcom/dropbox/android/widget/bi;->h:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final b()Lcom/dropbox/android/widget/bG;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 169
    new-instance v0, Lcom/dropbox/android/widget/bG;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/dropbox/android/widget/bG;-><init>(ZZZ)V

    return-object v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/dropbox/android/widget/bm;->a:Lcom/dropbox/android/widget/bm;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bm;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 4

    .prologue
    .line 397
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 398
    const/4 v0, 0x0

    .line 400
    iget-object v2, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v2, v3}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v2, v3, :cond_0

    .line 401
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 404
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 406
    return-object v0
.end method

.method public final d(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/dropbox/android/widget/bX;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 142
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bi;->f(Landroid/database/Cursor;)V

    .line 143
    return-object v0
.end method

.method public final k_()Ljava/lang/String;
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/dropbox/android/widget/bi;->d:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 415
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
