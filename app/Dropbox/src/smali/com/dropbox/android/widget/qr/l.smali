.class final Lcom/dropbox/android/widget/qr/l;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/widget/qr/QrReaderView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/qr/QrReaderView;)V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 196
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/l;->a:Ljava/lang/ref/WeakReference;

    .line 197
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/l;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/qr/QrReaderView;

    .line 202
    if-eqz v0, :cond_0

    .line 203
    invoke-static {v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->a(Lcom/dropbox/android/widget/qr/QrReaderView;)V

    .line 205
    :cond_0
    return-void
.end method
