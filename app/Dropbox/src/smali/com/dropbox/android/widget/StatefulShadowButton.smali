.class public Lcom/dropbox/android/widget/StatefulShadowButton;
.super Landroid/widget/Button;
.source "panda.py"


# instance fields
.field private a:Landroid/content/res/ColorStateList;

.field private b:F

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/widget/StatefulShadowButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/widget/StatefulShadowButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 64
    iget v0, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->d:F

    iget v1, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->b:F

    iget v2, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->c:F

    iget-object v3, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/StatefulShadowButton;->getDrawableState()[I

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/dropbox/android/widget/StatefulShadowButton;->setShadowLayer(FFFI)V

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/widget/StatefulShadowButton;->invalidate()V

    .line 66
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 48
    sget-object v0, Lcom/dropbox/android/j;->StatefulShadowButton:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->a:Landroid/content/res/ColorStateList;

    .line 50
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->b:F

    .line 51
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->c:F

    .line 52
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/dropbox/android/widget/StatefulShadowButton;->d:F

    .line 53
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 54
    invoke-direct {p0}, Lcom/dropbox/android/widget/StatefulShadowButton;->a()V

    .line 55
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/Button;->drawableStateChanged()V

    .line 60
    invoke-direct {p0}, Lcom/dropbox/android/widget/StatefulShadowButton;->a()V

    .line 61
    return-void
.end method
