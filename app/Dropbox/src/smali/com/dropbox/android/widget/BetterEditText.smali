.class public Lcom/dropbox/android/widget/BetterEditText;
.super Landroid/widget/EditText;
.source "panda.py"


# instance fields
.field private a:Z

.field private b:Landroid/view/ActionMode$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/BetterEditText;->a:Z

    .line 32
    invoke-direct {p0}, Lcom/dropbox/android/widget/BetterEditText;->a()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/BetterEditText;->a:Z

    .line 37
    invoke-direct {p0}, Lcom/dropbox/android/widget/BetterEditText;->a()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/BetterEditText;->a:Z

    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/widget/BetterEditText;->a()V

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/dropbox/android/widget/BetterEditText;->b:Landroid/view/ActionMode$Callback;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/dropbox/android/widget/k;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/k;-><init>(Lcom/dropbox/android/widget/BetterEditText;)V

    invoke-super {p0, v0}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 90
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/BetterEditText;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/dropbox/android/widget/BetterEditText;->a:Z

    return p1
.end method


# virtual methods
.method public getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dropbox/android/widget/BetterEditText;->b:Landroid/view/ActionMode$Callback;

    return-object v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 179
    instance-of v0, p1, Lcom/dropbox/android/widget/BetterEditText$SavedState;

    if-nez v0, :cond_1

    .line 180
    invoke-super {p0, p1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    check-cast p1, Lcom/dropbox/android/widget/BetterEditText$SavedState;

    .line 185
    invoke-virtual {p1}, Lcom/dropbox/android/widget/BetterEditText$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 187
    iget v0, p1, Lcom/dropbox/android/widget/BetterEditText$SavedState;->a:I

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/dropbox/android/widget/BetterEditText$SavedState;->b:I

    if-ltz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/dropbox/android/widget/BetterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    .line 193
    new-instance v1, Lcom/dropbox/android/widget/l;

    invoke-direct {v1, p0, v0, p1}, Lcom/dropbox/android/widget/l;-><init>(Lcom/dropbox/android/widget/BetterEditText;Landroid/text/Editable;Lcom/dropbox/android/widget/BetterEditText$SavedState;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/BetterEditText;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 145
    .line 148
    iget-boolean v1, p0, Lcom/dropbox/android/widget/BetterEditText;->a:Z

    if-nez v1, :cond_3

    .line 149
    invoke-virtual {p0}, Lcom/dropbox/android/widget/BetterEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 150
    if-eqz v4, :cond_3

    .line 151
    invoke-virtual {p0}, Lcom/dropbox/android/widget/BetterEditText;->getSelectionStart()I

    move-result v1

    .line 152
    invoke-virtual {p0}, Lcom/dropbox/android/widget/BetterEditText;->getSelectionEnd()I

    move-result v0

    .line 153
    if-gez v1, :cond_0

    if-ltz v0, :cond_1

    .line 154
    :cond_0
    const/4 v3, 0x1

    .line 160
    invoke-static {v4, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    move v2, v3

    .line 165
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v3

    .line 167
    if-eqz v2, :cond_2

    .line 168
    new-instance v2, Lcom/dropbox/android/widget/BetterEditText$SavedState;

    invoke-direct {v2, v3}, Lcom/dropbox/android/widget/BetterEditText$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 169
    iput v1, v2, Lcom/dropbox/android/widget/BetterEditText$SavedState;->a:I

    .line 170
    iput v0, v2, Lcom/dropbox/android/widget/BetterEditText$SavedState;->b:I

    move-object v0, v2

    .line 174
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v3

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/dropbox/android/widget/BetterEditText;->b:Landroid/view/ActionMode$Callback;

    .line 95
    return-void
.end method
