.class final Lcom/dropbox/android/widget/aE;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/widget/aF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    .line 210
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/widget/au;)V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/dropbox/android/widget/aE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 238
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/aF;)V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 218
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 219
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_0

    .line 222
    :cond_0
    return-void
.end method

.method public final b(Lcom/dropbox/android/widget/aF;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    move v0, v1

    .line 233
    :goto_0
    return v0

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/aE;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/aF;

    .line 229
    if-eq v0, p1, :cond_1

    move v0, v1

    .line 230
    goto :goto_0

    .line 233
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
