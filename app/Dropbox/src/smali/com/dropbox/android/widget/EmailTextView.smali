.class public Lcom/dropbox/android/widget/EmailTextView;
.super Landroid/widget/AutoCompleteTextView;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/EmailTextView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/EmailTextView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dropbox/android/widget/EmailTextView;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 41
    iput-object p1, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    .line 42
    iput-object p2, p0, Lcom/dropbox/android/widget/EmailTextView;->b:Ljava/lang/String;

    .line 44
    invoke-virtual {p0}, Lcom/dropbox/android/widget/EmailTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/Z;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 46
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->u()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "which"

    iget-object v3, p0, Lcom/dropbox/android/widget/EmailTextView;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "num"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 48
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/EmailTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x109000a

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 51
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/EmailTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 53
    invoke-virtual {p0, p0}, Lcom/dropbox/android/widget/EmailTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 54
    invoke-virtual {p0, p0}, Lcom/dropbox/android/widget/EmailTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 55
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 59
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/dropbox/android/widget/EmailTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/Z;->a(Ljava/lang/String;)Lcom/dropbox/android/util/aa;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_2

    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/widget/EmailTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0033

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/dropbox/android/util/aa;->a:Ljava/lang/String;

    aput-object v4, v3, v6

    iget-object v4, v0, Lcom/dropbox/android/util/aa;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 65
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 66
    const-wide/16 v3, 0x1f4

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 67
    invoke-virtual {v2, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 69
    iget-object v3, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 70
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->w()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "which"

    iget-object v5, p0, Lcom/dropbox/android/widget/EmailTextView;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 73
    :cond_0
    iget-object v3, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 74
    iget-object v2, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v1, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v1, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    new-instance v2, Lcom/dropbox/android/widget/an;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/widget/an;-><init>(Lcom/dropbox/android/widget/EmailTextView;Lcom/dropbox/android/util/aa;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    :cond_1
    :goto_0
    return-void

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/widget/EmailTextView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->v()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "which"

    iget-object v2, p0, Lcom/dropbox/android/widget/EmailTextView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 96
    return-void
.end method
