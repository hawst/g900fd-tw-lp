.class final Lcom/dropbox/android/widget/S;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Z

.field private static b:J


# instance fields
.field private final c:Ldbxyzptlk/db231222/z/M;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/widget/DbxMediaController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 947
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/widget/S;->a:Z

    .line 948
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/dropbox/android/widget/S;->b:J

    return-void
.end method

.method constructor <init>(Lcom/dropbox/android/widget/DbxMediaController;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 955
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 956
    iput-object p2, p0, Lcom/dropbox/android/widget/S;->c:Ldbxyzptlk/db231222/z/M;

    .line 957
    iput-object p3, p0, Lcom/dropbox/android/widget/S;->d:Ljava/lang/String;

    .line 958
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/S;->e:Ljava/lang/ref/WeakReference;

    .line 959
    return-void
.end method

.method public static a(Lcom/dropbox/android/widget/DbxMediaController;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 995
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 996
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 997
    sget-boolean v2, Lcom/dropbox/android/widget/S;->a:Z

    if-nez v2, :cond_0

    sget-wide v2, Lcom/dropbox/android/widget/S;->b:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 998
    new-instance v2, Lcom/dropbox/android/widget/S;

    invoke-direct {v2, p0, p1, p2}, Lcom/dropbox/android/widget/S;-><init>(Lcom/dropbox/android/widget/DbxMediaController;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/dropbox/android/widget/S;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 999
    const/4 v2, 0x1

    sput-boolean v2, Lcom/dropbox/android/widget/S;->a:Z

    .line 1000
    sput-wide v0, Lcom/dropbox/android/widget/S;->b:J

    .line 1003
    :cond_0
    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 964
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetching progress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/S;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    iget-object v0, p0, Lcom/dropbox/android/widget/S;->c:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/widget/S;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;)I

    move-result v0

    .line 966
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 967
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 972
    :goto_0
    return-object v0

    .line 969
    :catch_0
    move-exception v0

    .line 970
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TranscodeProgressAsyncTask"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 972
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Integer;)V
    .locals 4

    .prologue
    .line 977
    iget-object v0, p0, Lcom/dropbox/android/widget/S;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/DbxMediaController;

    .line 978
    if-eqz v0, :cond_0

    .line 979
    if-eqz p1, :cond_1

    .line 980
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(Lcom/dropbox/android/widget/DbxMediaController;I)V

    .line 987
    :cond_0
    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/widget/S;->a:Z

    .line 988
    return-void

    .line 984
    :cond_1
    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->e()V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 945
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/S;->a([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 945
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/S;->a(Ljava/lang/Integer;)V

    return-void
.end method
