.class final Lcom/dropbox/android/widget/bT;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/TextureVideoView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/TextureVideoView;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/dropbox/android/widget/bT;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/dropbox/android/widget/bT;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/TextureVideoView;->a(Lcom/dropbox/android/widget/TextureVideoView;I)I

    .line 173
    iget-object v0, p0, Lcom/dropbox/android/widget/bT;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/TextureVideoView;->b(Lcom/dropbox/android/widget/TextureVideoView;I)I

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/widget/bT;->a:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/TextureVideoView;->requestLayout()V

    .line 177
    return-void
.end method
