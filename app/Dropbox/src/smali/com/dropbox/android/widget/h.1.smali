.class final Lcom/dropbox/android/widget/h;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/g;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/g;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/dropbox/android/widget/h;->a:Lcom/dropbox/android/widget/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/dropbox/android/filemanager/w;
    .locals 5

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lcom/dropbox/android/widget/h;->a:Lcom/dropbox/android/widget/g;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/g;->g()Landroid/database/Cursor;

    move-result-object v1

    .line 55
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 56
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 57
    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v1, v3}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v3

    sget-object v4, Lcom/dropbox/android/provider/Z;->h:Lcom/dropbox/android/provider/Z;

    if-ne v3, v4, :cond_0

    .line 58
    invoke-static {v1}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;

    move-result-object v3

    .line 59
    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 60
    new-instance v0, Lcom/dropbox/android/filemanager/w;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v4, v3}, Lcom/dropbox/android/filemanager/w;-><init>(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;)V

    .line 63
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 64
    return-object v0
.end method
