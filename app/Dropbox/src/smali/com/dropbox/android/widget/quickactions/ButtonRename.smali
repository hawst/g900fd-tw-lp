.class public Lcom/dropbox/android/widget/quickactions/ButtonRename;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final d:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonRename;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f030088

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonRename;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    move-result-object v0

    .line 33
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 34
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 35
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f0d01e7

    return v0
.end method
