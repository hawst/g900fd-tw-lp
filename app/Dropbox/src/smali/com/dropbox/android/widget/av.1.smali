.class final Lcom/dropbox/android/widget/av;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/GalleryView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/GalleryView;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/GalleryView;Z)Z

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/ax;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 143
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->af()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->c(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/G;->c()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v0}, Lcom/dropbox/android/widget/GalleryView;->d(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/aD;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/GalleryView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(I)Lcom/dropbox/android/widget/ay;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v1}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/ax;

    move-result-object v1

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ay;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {v2}, Lcom/dropbox/android/widget/GalleryView;->c(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/dropbox/android/widget/ax;->a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/analytics/ChainInfo;)V

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/widget/av;->a:Lcom/dropbox/android/widget/GalleryView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/GalleryView;Lcom/dropbox/android/util/analytics/ChainInfo;)Lcom/dropbox/android/util/analytics/ChainInfo;

    .line 152
    :cond_0
    return-void
.end method
