.class final Lcom/dropbox/android/widget/aW;
.super Lcom/dropbox/android/widget/bz;
.source "panda.py"


# instance fields
.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;)V
    .locals 6

    .prologue
    .line 142
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/bz;-><init>(Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;Ljava/util/Comparator;)V

    .line 144
    iput-object p1, p0, Lcom/dropbox/android/widget/aW;->b:Landroid/view/LayoutInflater;

    .line 145
    return-void
.end method

.method private final a(Landroid/view/View;Lcom/dropbox/android/widget/bB;)V
    .locals 3

    .prologue
    .line 161
    const v0, 0x7f0700d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 162
    const v1, 0x7f0700d2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 163
    iget-object v2, p2, Lcom/dropbox/android/widget/bB;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p2, Lcom/dropbox/android/widget/bB;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p2, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    iget-object v2, p0, Lcom/dropbox/android/widget/aW;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p2, Lcom/dropbox/android/widget/bB;->c:Landroid/graphics/drawable/Drawable;

    .line 167
    :cond_0
    iget-object v0, p2, Lcom/dropbox/android/widget/bB;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 168
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 150
    if-nez p2, :cond_0

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/widget/aW;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030046

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 156
    :cond_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/aW;->b(I)Lcom/dropbox/android/widget/bB;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/dropbox/android/widget/aW;->a(Landroid/view/View;Lcom/dropbox/android/widget/bB;)V

    .line 157
    return-object p2
.end method
