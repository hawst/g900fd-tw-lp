.class final Lcom/dropbox/android/widget/qr/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# instance fields
.field private final a:Lcom/dropbox/android/widget/qr/c;

.field private b:Landroid/os/Handler;

.field private c:I


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/qr/c;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/i;->a:Lcom/dropbox/android/widget/qr/c;

    .line 38
    return-void
.end method


# virtual methods
.method final a(Landroid/os/Handler;I)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/i;->b:Landroid/os/Handler;

    .line 45
    iput p2, p0, Lcom/dropbox/android/widget/qr/i;->c:I

    .line 46
    return-void
.end method

.method public final onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/i;->b:Landroid/os/Handler;

    .line 52
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/i;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/qr/c;->a()Landroid/graphics/Point;

    move-result-object v1

    .line 53
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 54
    iget v2, p0, Lcom/dropbox/android/widget/qr/i;->c:I

    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v2, v3, v1, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/i;->b:Landroid/os/Handler;

    .line 59
    :cond_0
    return-void
.end method
