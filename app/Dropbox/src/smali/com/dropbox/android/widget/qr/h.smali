.class final Lcom/dropbox/android/widget/qr/h;
.super Ljava/lang/Thread;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/widget/qr/d;

.field private final b:Lcom/dropbox/android/widget/qr/j;

.field private final c:Ljava/util/concurrent/CountDownLatch;

.field private d:Lcom/dropbox/android/widget/qr/g;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/qr/d;Lcom/dropbox/android/widget/qr/j;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/h;->a:Lcom/dropbox/android/widget/qr/d;

    .line 37
    iput-object p2, p0, Lcom/dropbox/android/widget/qr/h;->b:Lcom/dropbox/android/widget/qr/j;

    .line 38
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/h;->c:Ljava/util/concurrent/CountDownLatch;

    .line 39
    return-void
.end method


# virtual methods
.method final a()Lcom/dropbox/android/widget/qr/g;
    .locals 1

    .prologue
    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/h;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/h;->d:Lcom/dropbox/android/widget/qr/g;

    return-object v0

    .line 48
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 56
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 57
    new-instance v0, Lcom/dropbox/android/widget/qr/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/h;->a:Lcom/dropbox/android/widget/qr/d;

    iget-object v2, p0, Lcom/dropbox/android/widget/qr/h;->b:Lcom/dropbox/android/widget/qr/j;

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/qr/g;-><init>(Lcom/dropbox/android/widget/qr/d;Lcom/dropbox/android/widget/qr/j;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/h;->d:Lcom/dropbox/android/widget/qr/g;

    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/h;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 59
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 60
    return-void
.end method
