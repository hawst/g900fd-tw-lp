.class final Lcom/dropbox/android/widget/qr/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# instance fields
.field private final a:Z

.field private final b:Landroid/hardware/Camera;

.field private final c:Ljava/util/Timer;

.field private d:Z

.field private e:Ljava/util/TimerTask;


# direct methods
.method constructor <init>(Landroid/hardware/Camera;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/a;->b:Landroid/hardware/Camera;

    .line 46
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1, v0}, Ljava/util/Timer;-><init>(Z)V

    iput-object v1, p0, Lcom/dropbox/android/widget/qr/a;->c:Ljava/util/Timer;

    .line 47
    invoke-virtual {p1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v1

    .line 48
    const-string v2, "auto"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "macro"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->a:Z

    .line 50
    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/a;->a()V

    .line 51
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/qr/a;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->d:Z

    return v0
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->a:Z

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/a;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, p0}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 77
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final declared-synchronized b()V
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 90
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/a;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/a;->e:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/a;->e:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/a;->e:Ljava/util/TimerTask;

    .line 100
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 91
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final declared-synchronized onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/a;->d:Z

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lcom/dropbox/android/widget/qr/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/qr/b;-><init>(Lcom/dropbox/android/widget/qr/a;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/a;->e:Ljava/util/TimerTask;

    .line 65
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/a;->c:Ljava/util/Timer;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/a;->e:Ljava/util/TimerTask;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
