.class public final enum Lcom/dropbox/android/widget/br;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/widget/br;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/br;

.field public static final enum b:Lcom/dropbox/android/widget/br;

.field private static final synthetic d:[Lcom/dropbox/android/widget/br;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/dropbox/android/widget/br;

    const-string v1, "REMOTE_INSTALL"

    const-string v2, "_remote_install"

    invoke-direct {v0, v1, v3, v2}, Lcom/dropbox/android/widget/br;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/widget/br;->a:Lcom/dropbox/android/widget/br;

    .line 44
    new-instance v0, Lcom/dropbox/android/widget/br;

    const-string v1, "TURN_ON_CAMERA_UPLOAD"

    const-string v2, "turn_camera_upload_on"

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/widget/br;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/widget/br;->b:Lcom/dropbox/android/widget/br;

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/widget/br;

    sget-object v1, Lcom/dropbox/android/widget/br;->a:Lcom/dropbox/android/widget/br;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/br;->b:Lcom/dropbox/android/widget/br;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/widget/br;->d:[Lcom/dropbox/android/widget/br;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, Lcom/dropbox/android/widget/br;->c:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/br;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/dropbox/android/widget/br;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/br;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/br;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/dropbox/android/widget/br;->d:[Lcom/dropbox/android/widget/br;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/br;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/br;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dropbox/android/widget/br;->c:Ljava/lang/String;

    return-object v0
.end method
