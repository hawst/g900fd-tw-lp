.class public abstract Lcom/dropbox/android/widget/bX;
.super Lcom/dropbox/android/widget/bC;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/v;


# static fields
.field private static final g:Ljava/lang/String;

.field private static final i:Lcom/dropbox/android/widget/bE;


# instance fields
.field protected a:Lcom/dropbox/android/filemanager/n;

.field protected final b:Lcom/dropbox/android/widget/bY;

.field private final h:Lcom/dropbox/android/taskqueue/D;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/dropbox/android/widget/bX;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/bX;->g:Ljava/lang/String;

    .line 91
    new-instance v0, Lcom/dropbox/android/widget/bE;

    invoke-direct {v0}, Lcom/dropbox/android/widget/bE;-><init>()V

    sput-object v0, Lcom/dropbox/android/widget/bX;->i:Lcom/dropbox/android/widget/bE;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/taskqueue/D;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bC;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p4, p0, Lcom/dropbox/android/widget/bX;->h:Lcom/dropbox/android/taskqueue/D;

    .line 44
    iput-object p3, p0, Lcom/dropbox/android/widget/bX;->b:Lcom/dropbox/android/widget/bY;

    .line 45
    invoke-virtual {p0, p2}, Lcom/dropbox/android/widget/bX;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 46
    return-void
.end method

.method private a(Lcom/dropbox/android/widget/SweetListView;)V
    .locals 4

    .prologue
    .line 94
    sget-object v0, Lcom/dropbox/android/widget/bX;->i:Lcom/dropbox/android/widget/bE;

    invoke-virtual {p1, v0}, Lcom/dropbox/android/widget/SweetListView;->a(Lcom/dropbox/android/widget/bE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    sget-object v1, Lcom/dropbox/android/widget/bX;->i:Lcom/dropbox/android/widget/bE;

    iget v1, v1, Lcom/dropbox/android/widget/bE;->a:I

    sget-object v2, Lcom/dropbox/android/widget/bX;->i:Lcom/dropbox/android/widget/bE;

    iget v2, v2, Lcom/dropbox/android/widget/bE;->b:I

    sget-object v3, Lcom/dropbox/android/widget/bX;->i:Lcom/dropbox/android/widget/bE;

    iget v3, v3, Lcom/dropbox/android/widget/bE;->a:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/n;->a(II)V

    .line 98
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 117
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 119
    add-int/lit8 v0, p2, -0x1

    .line 121
    iget-object v3, p0, Lcom/dropbox/android/widget/bX;->d:Landroid/database/Cursor;

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 123
    :cond_0
    iget-object v3, p0, Lcom/dropbox/android/widget/bX;->d:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    add-int/lit8 v0, v0, 0x1

    .line 126
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bX;->k_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    const/4 v1, 0x1

    .line 132
    :cond_1
    iget-object v3, p0, Lcom/dropbox/android/widget/bX;->d:Landroid/database/Cursor;

    invoke-interface {v3, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 134
    if-eqz v1, :cond_2

    .line 138
    :goto_0
    return v0

    .line 137
    :cond_2
    sget-object v0, Lcom/dropbox/android/widget/bX;->g:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "search() failed to find key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " searchHint="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Lcom/dropbox/android/widget/SweetListView;IZZZ)Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p3}, Lcom/dropbox/android/widget/bX;->a(Lcom/dropbox/android/widget/SweetListView;)V

    .line 83
    invoke-super/range {p0 .. p7}, Lcom/dropbox/android/widget/bC;->a(ILandroid/view/View;Lcom/dropbox/android/widget/SweetListView;IZZZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/n;->a(Z)V

    .line 74
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 103
    invoke-virtual {p0}, Lcom/dropbox/android/widget/bX;->i()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public d(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/n;->a()V

    .line 54
    :cond_0
    if-eqz p1, :cond_1

    .line 55
    iget-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    if-nez v0, :cond_2

    .line 56
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {}, Lcom/dropbox/android/util/bn;->f()Ldbxyzptlk/db231222/v/n;

    move-result-object v3

    invoke-static {}, Lcom/dropbox/android/util/bn;->i()I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/widget/bX;->h:Lcom/dropbox/android/taskqueue/D;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Ldbxyzptlk/db231222/v/n;ILcom/dropbox/android/taskqueue/D;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    .line 67
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/dropbox/android/widget/bC;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 63
    :cond_2
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    invoke-direct {v0, v1, p0, v2}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Lcom/dropbox/android/filemanager/n;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/bX;->a:Lcom/dropbox/android/filemanager/n;

    goto :goto_0
.end method

.method public abstract k_()Ljava/lang/String;
.end method
