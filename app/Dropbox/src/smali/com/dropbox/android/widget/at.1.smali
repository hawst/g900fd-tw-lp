.class public final Lcom/dropbox/android/widget/at;
.super Lcom/dropbox/android/widget/bC;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/k;


# static fields
.field private static final g:Lcom/dropbox/android/widget/bE;


# instance fields
.field a:Lcom/dropbox/android/filemanager/i;

.field protected final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/dropbox/android/widget/bE;

    invoke-direct {v0}, Lcom/dropbox/android/widget/bE;-><init>()V

    sput-object v0, Lcom/dropbox/android/widget/at;->g:Lcom/dropbox/android/widget/bE;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bC;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object p3, p0, Lcom/dropbox/android/widget/at;->b:Ljava/util/Set;

    .line 39
    invoke-virtual {p0, p2}, Lcom/dropbox/android/widget/at;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 40
    return-void
.end method

.method private a(Landroid/database/Cursor;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 79
    const/4 v0, 0x0

    .line 81
    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/Z;->e:Lcom/dropbox/android/provider/Z;

    if-ne v1, v2, :cond_2

    .line 82
    const-string v0, "_separator_text"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 83
    const-string v1, "_sep_camera_roll"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->c:Landroid/content/Context;

    const v1, 0x7f0d01a6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 92
    :goto_0
    const v0, 0x7f0700df

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-void

    .line 85
    :cond_0
    const-string v1, "_sep_other_media"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->c:Landroid/content/Context;

    const v1, 0x7f0d01a7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 88
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected seperator used in GalleryPickerListAdapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private a(Lcom/dropbox/android/widget/SweetListView;)V
    .locals 4

    .prologue
    .line 96
    sget-object v0, Lcom/dropbox/android/widget/at;->g:Lcom/dropbox/android/widget/bE;

    invoke-virtual {p1, v0}, Lcom/dropbox/android/widget/SweetListView;->a(Lcom/dropbox/android/widget/bE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    sget-object v1, Lcom/dropbox/android/widget/at;->g:Lcom/dropbox/android/widget/bE;

    iget v1, v1, Lcom/dropbox/android/widget/bE;->a:I

    sget-object v2, Lcom/dropbox/android/widget/at;->g:Lcom/dropbox/android/widget/bE;

    iget v2, v2, Lcom/dropbox/android/widget/bE;->b:I

    sget-object v3, Lcom/dropbox/android/widget/at;->g:Lcom/dropbox/android/widget/bE;

    iget v3, v3, Lcom/dropbox/android/widget/bE;->a:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/i;->a(II)V

    .line 100
    :cond_0
    return-void
.end method

.method private b(Landroid/database/Cursor;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 157
    const-string v0, "content_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/dropbox/android/widget/at;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 160
    check-cast p2, Lcom/dropbox/android/widget/GalleryItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    invoke-virtual {p2, p1, v0, v1}, Lcom/dropbox/android/widget/GalleryItemView;->a(Landroid/database/Cursor;ZLcom/dropbox/android/filemanager/i;)V

    .line 161
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public final a(ILandroid/view/View;Lcom/dropbox/android/widget/SweetListView;IZZZ)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p3}, Lcom/dropbox/android/widget/at;->a(Lcom/dropbox/android/widget/SweetListView;)V

    .line 75
    invoke-super/range {p0 .. p7}, Lcom/dropbox/android/widget/bC;->a(ILandroid/view/View;Lcom/dropbox/android/widget/SweetListView;IZZZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 131
    packed-switch p2, :pswitch_data_0

    .line 137
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :pswitch_0
    new-instance v0, Lcom/dropbox/android/widget/GalleryItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/at;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/widget/GalleryItemView;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 135
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->c:Landroid/content/Context;

    const v1, 0x7f030051

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Lcom/dropbox/android/filemanager/l;
    .locals 6

    .prologue
    .line 172
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 174
    iget-object v1, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 175
    iget-object v1, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/at;->e(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    const-string v2, "thumb_path"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 179
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    const-string v3, "content_uri"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 180
    const-string v0, "_tag_video"

    iget-object v3, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    iget-object v4, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    const-string v5, "_cursor_type_tag"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 181
    new-instance v0, Lcom/dropbox/android/filemanager/l;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/l;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 185
    :goto_0
    return-object v0

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 185
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/database/Cursor;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 143
    packed-switch p3, :pswitch_data_0

    .line 151
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/at;->b(Landroid/database/Cursor;Landroid/view/View;)V

    .line 153
    :goto_0
    return-void

    .line 148
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/at;->a(Landroid/database/Cursor;Landroid/view/View;)V

    goto :goto_0

    .line 143
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/at;->e(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    const-string v1, "_cursor_type_tag"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/dropbox/android/widget/at;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 106
    const-string v1, "_tag_photo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "_tag_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    :cond_0
    const/4 v0, -0x1

    .line 109
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/android/widget/bG;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 125
    new-instance v0, Lcom/dropbox/android/widget/bG;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/dropbox/android/widget/bG;-><init>(ZZZ)V

    return-object v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 120
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 166
    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public final d(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/i;->a()V

    .line 48
    :cond_0
    if-eqz p1, :cond_1

    .line 49
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 52
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    if-nez v1, :cond_3

    .line 53
    new-instance v1, Lcom/dropbox/android/filemanager/i;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2, p0, v0}, Lcom/dropbox/android/filemanager/i;-><init>(ILcom/dropbox/android/filemanager/k;I)V

    iput-object v1, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    .line 59
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/dropbox/android/widget/bC;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 49
    :cond_2
    const/4 v0, 0x3

    goto :goto_0

    .line 55
    :cond_3
    new-instance v0, Lcom/dropbox/android/filemanager/i;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    invoke-direct {v0, v1, p0, v2}, Lcom/dropbox/android/filemanager/i;-><init>(ILcom/dropbox/android/filemanager/k;Lcom/dropbox/android/filemanager/i;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/at;->a:Lcom/dropbox/android/filemanager/i;

    goto :goto_1
.end method
