.class final Lcom/dropbox/android/widget/bL;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/SweetListView;

.field private b:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/SweetListView;)V
    .locals 1

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/dropbox/android/widget/bL;->a:Lcom/dropbox/android/widget/SweetListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1081
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/bL;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 1084
    invoke-static {p1, p0}, Lcom/dropbox/android/widget/SweetListView;->a(Lcom/dropbox/android/widget/SweetListView;Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1085
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/dropbox/android/widget/bL;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 1089
    iget-object v0, p0, Lcom/dropbox/android/widget/bL;->a:Lcom/dropbox/android/widget/SweetListView;

    invoke-static {v0, p0}, Lcom/dropbox/android/widget/SweetListView;->b(Lcom/dropbox/android/widget/SweetListView;Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1090
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 1101
    iget-object v0, p0, Lcom/dropbox/android/widget/bL;->a:Lcom/dropbox/android/widget/SweetListView;

    invoke-static {v0, p2, p3}, Lcom/dropbox/android/widget/SweetListView;->a(Lcom/dropbox/android/widget/SweetListView;II)V

    .line 1102
    iget-object v0, p0, Lcom/dropbox/android/widget/bL;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1103
    iget-object v0, p0, Lcom/dropbox/android/widget/bL;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 1105
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/dropbox/android/widget/bL;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1095
    iget-object v0, p0, Lcom/dropbox/android/widget/bL;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 1097
    :cond_0
    return-void
.end method
