.class final Lcom/dropbox/android/widget/U;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/DbxVideoView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/DbxVideoView;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 361
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->c(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 363
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0, v3}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;Z)Z

    .line 364
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v2, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxVideoView;->f(Lcom/dropbox/android/widget/DbxVideoView;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/dropbox/android/widget/DbxVideoView;->c(Lcom/dropbox/android/widget/DbxVideoView;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;Z)Z

    .line 365
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0, v3}, Lcom/dropbox/android/widget/DbxVideoView;->d(Lcom/dropbox/android/widget/DbxVideoView;Z)Z

    .line 367
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->g(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->g(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->h(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/DbxMediaController;->setEnabled(Z)V

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 374
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1, p1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 376
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->j(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    .line 377
    if-eqz v0, :cond_2

    .line 378
    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/DbxVideoView;->a(I)V

    .line 380
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    if-eqz v1, :cond_6

    .line 382
    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v2

    iget-object v3, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v3}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 383
    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->k(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->l(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 387
    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->m(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    if-ne v1, v4, :cond_4

    .line 388
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    .line 389
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 390
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    .line 407
    :cond_3
    :goto_0
    return-void

    .line 392
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/DbxVideoView;->e()Z

    move-result v1

    if-nez v1, :cond_3

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d()I

    move-result v0

    if-lez v0, :cond_3

    .line 394
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 396
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    goto :goto_0

    .line 403
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->m(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 404
    iget-object v0, p0, Lcom/dropbox/android/widget/U;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    goto :goto_0
.end method
