.class final Lcom/dropbox/android/widget/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/BetterEditText;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/BetterEditText;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    .line 87
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    iget-object v2, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v2, v1}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;Z)Z

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;Z)Z

    move v0, v1

    .line 78
    goto :goto_0
.end method

.method public final onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;Z)Z

    .line 66
    return-void
.end method

.method public final onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->a:Lcom/dropbox/android/widget/BetterEditText;

    invoke-static {v0}, Lcom/dropbox/android/widget/BetterEditText;->a(Lcom/dropbox/android/widget/BetterEditText;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
