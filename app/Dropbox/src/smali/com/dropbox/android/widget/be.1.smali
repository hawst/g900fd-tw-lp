.class public final Lcom/dropbox/android/widget/be;
.super Landroid/support/v4/widget/a;
.source "panda.py"


# instance fields
.field protected final a:Z

.field private final b:Z

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;IZZLjava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "IZZ",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 23
    iput-boolean p4, p0, Lcom/dropbox/android/widget/be;->a:Z

    .line 24
    iput-boolean p5, p0, Lcom/dropbox/android/widget/be;->b:Z

    .line 25
    iput-object p6, p0, Lcom/dropbox/android/widget/be;->c:Ljava/util/Set;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/dropbox/android/widget/be;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 62
    if-eqz v1, :cond_1

    .line 63
    const/4 v2, -0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 64
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    sget-object v2, Lcom/dropbox/android/provider/Z;->b:Lcom/dropbox/android/provider/Z;

    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v1, v3}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v3

    if-eq v2, v3, :cond_0

    .line 71
    :cond_1
    :goto_0
    return v0

    .line 69
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 35
    check-cast p1, Lcom/dropbox/android/widget/LocalItemView;

    .line 36
    iget-boolean v0, p0, Lcom/dropbox/android/widget/be;->b:Z

    iget-object v1, p0, Lcom/dropbox/android/widget/be;->c:Ljava/util/Set;

    invoke-virtual {p1, p3, v0, v1}, Lcom/dropbox/android/widget/LocalItemView;->a(Landroid/database/Cursor;ZLjava/util/Set;)V

    .line 37
    return-void
.end method

.method public final isEnabled(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/widget/be;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 42
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 44
    sget-object v2, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v1, v2}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v2

    .line 46
    sget-object v3, Lcom/dropbox/android/provider/Z;->b:Lcom/dropbox/android/provider/Z;

    if-ne v2, v3, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    const-string v2, "is_dir"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 52
    iget-boolean v2, p0, Lcom/dropbox/android/widget/be;->a:Z

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 53
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/dropbox/android/widget/LocalItemView;

    iget-boolean v1, p0, Lcom/dropbox/android/widget/be;->a:Z

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/widget/LocalItemView;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method
