.class public Lcom/dropbox/android/widget/DropboxEntryView;
.super Lcom/dropbox/android/widget/QuickActionView;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/P;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/FrameLayout;

.field private i:Landroid/widget/ImageView;

.field private j:Ljava/util/concurrent/ThreadPoolExecutor;

.field private k:Landroid/widget/ProgressBar;

.field private l:Ldbxyzptlk/db231222/j/g;

.field private m:Lcom/dropbox/android/util/DropboxPath;

.field private n:Z

.field private final o:Lcom/dropbox/android/widget/as;

.field private final p:Landroid/os/Handler;

.field private final q:Lcom/dropbox/android/widget/ai;

.field private final r:Ldbxyzptlk/db231222/r/d;

.field private final s:Lcom/dropbox/android/filemanager/I;

.field private final t:Lcom/dropbox/android/taskqueue/D;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/QuickActionView;-><init>(Landroid/content/Context;)V

    .line 72
    iput-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->n:Z

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->p:Landroid/os/Handler;

    .line 76
    new-instance v0, Lcom/dropbox/android/widget/ai;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/ai;-><init>(Lcom/dropbox/android/widget/DropboxEntryView;Lcom/dropbox/android/widget/ad;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/ai;

    .line 83
    const v0, 0x7f03004f

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 84
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/DropboxEntryView;->addView(Landroid/view/View;)V

    .line 86
    iput-object p2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/as;

    .line 87
    const v0, 0x7f0700d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->a:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0700d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->c:Landroid/widget/CheckBox;

    .line 89
    const v0, 0x7f0700d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0700d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    .line 92
    const v0, 0x7f0700d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    .line 93
    const v0, 0x7f0700da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    .line 94
    const v0, 0x7f0700d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    .line 95
    const v0, 0x7f07005e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->i:Landroid/widget/ImageView;

    .line 96
    const v0, 0x7f0700db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    .line 97
    const v0, 0x7f0700de

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    .line 98
    iput-object p3, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Ldbxyzptlk/db231222/r/d;

    .line 99
    invoke-virtual {p3}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/filemanager/I;

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->t:Lcom/dropbox/android/taskqueue/D;

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DropboxEntryView;)Lcom/dropbox/android/taskqueue/D;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->t:Lcom/dropbox/android/taskqueue/D;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 7

    .prologue
    const/16 v1, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    .line 196
    invoke-direct {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->c()V

    .line 222
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 208
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->c()J

    move-result-wide v1

    invoke-static {v0, v1, v2, v6}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/as;

    sget-object v2, Lcom/dropbox/android/widget/as;->c:Lcom/dropbox/android/widget/as;

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 212
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d007c

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 218
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d007d

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-wide v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/support/v4/app/Fragment;ZLcom/dropbox/android/widget/quickactions/QuickActionBar;)V
    .locals 10

    .prologue
    const/16 v9, 0xb3

    const/16 v8, 0xff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/as;

    sget-object v3, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    if-ne v0, v3, :cond_4

    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-nez v0, :cond_4

    move v0, v1

    .line 158
    :goto_0
    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v3, p1}, Lcom/dropbox/android/filemanager/I;->c(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v1

    .line 159
    :goto_1
    iget-object v4, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v4, p1}, Lcom/dropbox/android/filemanager/I;->d(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v1

    .line 161
    :goto_2
    iget-object v5, p0, Lcom/dropbox/android/widget/DropboxEntryView;->a:Landroid/widget/TextView;

    iget-object v6, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 165
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DropboxEntryView;->b(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 167
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DropboxEntryView;->c(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 169
    iget-object v5, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v5, p1}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/db231222/j/g;

    move-result-object v5

    instance-of v5, v5, Ldbxyzptlk/db231222/j/d;

    .line 170
    iget-object v6, p0, Lcom/dropbox/android/widget/DropboxEntryView;->c:Landroid/widget/CheckBox;

    if-nez v0, :cond_7

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    if-eqz v5, :cond_7

    :cond_0
    move v4, v1

    :goto_3
    invoke-virtual {v6, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 171
    iget-object v4, p0, Lcom/dropbox/android/widget/DropboxEntryView;->c:Landroid/widget/CheckBox;

    new-instance v5, Lcom/dropbox/android/widget/ah;

    iget-object v6, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/as;

    iget-object v7, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v5, p1, v6, v7}, Lcom/dropbox/android/widget/ah;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V

    invoke-virtual {p0, p4, v4, p2, v5}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/widget/CheckBox;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/bd;)V

    .line 174
    if-nez v0, :cond_1

    if-eqz v3, :cond_8

    :cond_1
    move v0, v1

    .line 175
    :goto_4
    if-nez v0, :cond_9

    :goto_5
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/DropboxEntryView;->setEnabled(Z)V

    .line 176
    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    invoke-static {v9, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    invoke-static {v9, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 181
    :cond_2
    if-eqz p3, :cond_3

    .line 182
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->a()V

    .line 184
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 157
    goto :goto_0

    :cond_5
    move v3, v2

    .line 158
    goto :goto_1

    :cond_6
    move v4, v2

    .line 159
    goto :goto_2

    :cond_7
    move v4, v2

    .line 170
    goto :goto_3

    :cond_8
    move v0, v2

    .line 174
    goto :goto_4

    :cond_9
    move v1, v2

    .line 175
    goto :goto_5
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DropboxEntryView;)Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 141
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 143
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 144
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    return-void
.end method

.method private b(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 4

    .prologue
    .line 225
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 227
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    .line 228
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->t:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/ref/WeakReference;)V

    .line 230
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/dropbox/android/widget/ad;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/ad;-><init>(Lcom/dropbox/android/widget/DropboxEntryView;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 268
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    .line 262
    if-nez v0, :cond_1

    .line 263
    const-string v0, ""

    .line 266
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3, v0}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    instance-of v0, v0, Ldbxyzptlk/db231222/j/n;

    if-eqz v0, :cond_0

    move v0, v1

    .line 190
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    if-nez v0, :cond_1

    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-static {v3, v4, v1, v0, v2}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Ldbxyzptlk/db231222/j/g;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;)V

    .line 192
    return-void

    :cond_0
    move v0, v2

    .line 189
    goto :goto_0

    :cond_1
    move v1, v2

    .line 190
    goto :goto_1
.end method

.method private c(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 4

    .prologue
    const v0, 0x7f02022e

    const/4 v1, 0x0

    .line 271
    .line 272
    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/as;

    sget-object v3, Lcom/dropbox/android/widget/as;->c:Lcom/dropbox/android/widget/as;

    if-ne v2, v3, :cond_3

    .line 273
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 283
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 284
    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 285
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 287
    :cond_1
    return-void

    .line 276
    :cond_2
    const v0, 0x7f02022f

    goto :goto_0

    .line 279
    :cond_3
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DropboxEntryView;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->n:Z

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/DropboxEntryView;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->c()V

    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->p:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;ZLjava/util/concurrent/ThreadPoolExecutor;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/ai;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 108
    iput-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->t:Lcom/dropbox/android/taskqueue/D;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V

    .line 112
    iput-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    .line 115
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->b()V

    .line 117
    iput-object p5, p0, Lcom/dropbox/android/widget/DropboxEntryView;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 119
    invoke-static {p1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->n:Z

    .line 121
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v1

    new-instance v2, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-direct {v2, v3}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    .line 123
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    if-eqz v1, :cond_2

    .line 124
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/ai;

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/j/g;->a(Ldbxyzptlk/db231222/j/h;)V

    .line 128
    :cond_2
    invoke-direct {p0, v0, p3, p4, p2}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/support/v4/app/Fragment;ZLcom/dropbox/android/widget/quickactions/QuickActionBar;)V

    .line 129
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p1, v0}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v0

    invoke-virtual {p3, v0}, Ldbxyzptlk/db231222/v/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/dropbox/android/widget/af;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/dropbox/android/widget/af;-><init>(Lcom/dropbox/android/widget/DropboxEntryView;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 324
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0}, Lcom/dropbox/android/widget/QuickActionView;->onDetachedFromWindow()V

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/ai;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/db231222/j/g;

    .line 138
    :cond_0
    return-void
.end method
