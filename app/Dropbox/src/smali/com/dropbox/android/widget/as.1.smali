.class public final enum Lcom/dropbox/android/widget/as;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/widget/as;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/as;

.field public static final enum b:Lcom/dropbox/android/widget/as;

.field public static final enum c:Lcom/dropbox/android/widget/as;

.field public static final enum d:Lcom/dropbox/android/widget/as;

.field public static final enum e:Lcom/dropbox/android/widget/as;

.field private static final synthetic f:[Lcom/dropbox/android/widget/as;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/dropbox/android/widget/as;

    const-string v1, "BROWSER"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/as;->a:Lcom/dropbox/android/widget/as;

    .line 36
    new-instance v0, Lcom/dropbox/android/widget/as;

    const-string v1, "BROWSER_DIRONLY"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    .line 37
    new-instance v0, Lcom/dropbox/android/widget/as;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/widget/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/as;->c:Lcom/dropbox/android/widget/as;

    .line 38
    new-instance v0, Lcom/dropbox/android/widget/as;

    const-string v1, "GRID_GALLERY"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/widget/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/as;->d:Lcom/dropbox/android/widget/as;

    .line 39
    new-instance v0, Lcom/dropbox/android/widget/as;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/widget/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/as;->e:Lcom/dropbox/android/widget/as;

    .line 34
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/widget/as;

    sget-object v1, Lcom/dropbox/android/widget/as;->a:Lcom/dropbox/android/widget/as;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/as;->c:Lcom/dropbox/android/widget/as;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/widget/as;->d:Lcom/dropbox/android/widget/as;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/widget/as;->e:Lcom/dropbox/android/widget/as;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/widget/as;->f:[Lcom/dropbox/android/widget/as;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dropbox/android/widget/as;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/dropbox/android/widget/as;->f:[Lcom/dropbox/android/widget/as;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/as;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/as;

    return-object v0
.end method
