.class final Lcom/dropbox/android/widget/I;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/DbxMediaController;

.field private b:I


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 1

    .prologue
    .line 490
    iput-object p1, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 498
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/I;->b:I

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 504
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 506
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_0

    .line 509
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->f(Lcom/dropbox/android/widget/DbxMediaController;)I

    move-result v0

    .line 510
    iget-object v1, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxMediaController;->g(Lcom/dropbox/android/widget/DbxMediaController;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxMediaController;->b(Lcom/dropbox/android/widget/DbxMediaController;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxMediaController;->h(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/O;

    move-result-object v1

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 511
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/I;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 512
    rem-int/lit16 v0, v0, 0x3e8

    rsub-int v0, v0, 0x3e8

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/dropbox/android/widget/I;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 518
    :pswitch_2
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->i(Lcom/dropbox/android/widget/DbxMediaController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/I;->b:I

    int-to-long v0, v0

    const-wide/16 v2, 0x51

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->a:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->j(Lcom/dropbox/android/widget/DbxMediaController;)V

    .line 520
    iget v0, p0, Lcom/dropbox/android/widget/I;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/I;->b:I

    .line 521
    const/4 v0, 0x3

    const-wide/16 v1, 0x2af8

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/widget/I;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 504
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
