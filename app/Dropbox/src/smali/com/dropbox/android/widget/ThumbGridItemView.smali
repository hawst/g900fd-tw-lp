.class public Lcom/dropbox/android/widget/ThumbGridItemView;
.super Landroid/widget/FrameLayout;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/y;
.implements Lcom/dropbox/android/widget/bZ;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/view/View;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Lcom/dropbox/android/util/n;

.field private final l:Lcom/dropbox/android/widget/bY;

.field private final m:Lcom/dropbox/android/widget/aG;

.field private final n:Lcom/dropbox/android/widget/aH;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/ThumbGridItemView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/aG;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    .line 42
    iput-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->i:Z

    .line 44
    iput-boolean v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    .line 51
    new-instance v0, Lcom/dropbox/android/widget/bV;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bV;-><init>(Lcom/dropbox/android/widget/ThumbGridItemView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->n:Lcom/dropbox/android/widget/aH;

    .line 61
    iput-object p3, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->l:Lcom/dropbox/android/widget/bY;

    .line 62
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->l:Lcom/dropbox/android/widget/bY;

    invoke-interface {v0, p0}, Lcom/dropbox/android/widget/bY;->a(Lcom/dropbox/android/widget/bZ;)V

    .line 64
    iput-object p4, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    .line 66
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->setDuplicateParentStateEnabled(Z)V

    .line 67
    invoke-virtual {p2, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 70
    const v0, 0x7f0701c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->d:Landroid/widget/ImageView;

    .line 71
    const v0, 0x7f0701bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    .line 72
    const v0, 0x7f07005e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->c:Landroid/widget/ImageView;

    .line 73
    const v0, 0x7f0701bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->e:Landroid/view/View;

    .line 74
    return-void
.end method

.method private a(J)J
    .locals 2

    .prologue
    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    long-to-int v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ThumbGridItemView;J)J
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/ThumbGridItemView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/filemanager/n;)V
    .locals 2

    .prologue
    .line 86
    iget v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 87
    iget v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    invoke-virtual {p1, v0, p0}, Lcom/dropbox/android/filemanager/n;->c(ILcom/dropbox/android/filemanager/y;)V

    .line 90
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/ThumbGridItemView;->b()V

    .line 91
    return-void
.end method

.method private a(Lcom/dropbox/android/util/n;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 178
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->c:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 180
    iput-object p1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->a()V

    .line 183
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 185
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    invoke-virtual {v2}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 186
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 187
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/ThumbGridItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x106000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 193
    :goto_2
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    invoke-virtual {v3}, Lcom/dropbox/android/util/n;->d()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 194
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 196
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 198
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 199
    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 201
    :cond_0
    return-void

    .line 178
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    move v0, v1

    .line 183
    goto :goto_1

    .line 189
    :cond_3
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 190
    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->n:Lcom/dropbox/android/widget/aH;

    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/aG;->b(Lcom/dropbox/android/widget/aH;Ljava/lang/String;)V

    .line 115
    :cond_0
    if-eqz p1, :cond_1

    .line 116
    iput-object p1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->n:Lcom/dropbox/android/widget/aH;

    iget-object v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/aG;->a(Lcom/dropbox/android/widget/aH;Ljava/lang/String;)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/ThumbGridItemView;)Lcom/dropbox/android/widget/aG;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 218
    iput-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->k:Lcom/dropbox/android/util/n;

    .line 220
    :cond_1
    return-void
.end method

.method private b(J)V
    .locals 10

    .prologue
    const-wide/16 v0, 0x12c

    const/4 v9, 0x1

    const-wide/16 v4, 0x0

    const-wide/16 v7, 0x1f4

    .line 275
    cmp-long v2, p1, v7

    if-ltz v2, :cond_1

    move-wide v2, v4

    .line 281
    :goto_0
    cmp-long v6, p1, v7

    if-lez v6, :cond_0

    .line 282
    sub-long v6, p1, v7

    sub-long/2addr v0, v6

    .line 287
    :cond_0
    cmp-long v6, v0, v4

    if-gtz v6, :cond_2

    .line 288
    sget-object v0, Lcom/dropbox/android/widget/ThumbGridItemView;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doHighlight() quitting early because skipMs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is longer than the animation duration"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :goto_1
    return-void

    .line 278
    :cond_1
    sub-long v2, v7, p1

    goto :goto_0

    .line 298
    :cond_2
    iget-object v6, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->e:Landroid/view/View;

    invoke-virtual {v6, v9}, Landroid/view/View;->setSelected(Z)V

    .line 300
    long-to-float v6, v0

    const/high16 v7, 0x43960000    # 300.0f

    div-float/2addr v6, v7

    .line 301
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v8, 0x0

    invoke-direct {v7, v6, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 302
    invoke-virtual {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 303
    invoke-virtual {v7, v9}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 305
    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 306
    invoke-virtual {v7, v2, v3}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 309
    :cond_3
    new-instance v0, Lcom/dropbox/android/widget/bW;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/bW;-><init>(Lcom/dropbox/android/widget/ThumbGridItemView;)V

    invoke-virtual {v7, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 338
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->e:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method private b(Lcom/dropbox/android/filemanager/n;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x4

    .line 94
    invoke-direct {p0}, Lcom/dropbox/android/widget/ThumbGridItemView;->c()V

    .line 96
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    invoke-virtual {p1, v0, p0}, Lcom/dropbox/android/filemanager/n;->a(ILcom/dropbox/android/filemanager/y;)Lcom/dropbox/android/util/n;

    move-result-object v0

    .line 97
    :goto_0
    if-eqz v0, :cond_1

    .line 98
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Lcom/dropbox/android/util/n;Z)V

    .line 99
    const/4 v1, -0x1

    iput v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    .line 100
    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 108
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 96
    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/ThumbGridItemView;J)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(J)V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/ThumbGridItemView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->e:Landroid/view/View;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 236
    iget-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    if-nez v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->l:Lcom/dropbox/android/widget/bY;

    invoke-interface {v0}, Lcom/dropbox/android/widget/bY;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->l:Lcom/dropbox/android/widget/bY;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/bY;->a(Ljava/lang/String;)Z

    move-result v0

    .line 244
    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 245
    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 248
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 78
    const v0, 0x7f0300cc

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/dropbox/android/widget/ThumbGridItemView;->c()V

    .line 233
    return-void
.end method

.method public final a(ILcom/dropbox/android/util/n;Z)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 224
    iget v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 225
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Lcom/dropbox/android/util/n;Z)V

    .line 226
    iput v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    .line 228
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/filemanager/n;)V
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0, p2}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Lcom/dropbox/android/filemanager/n;)V

    .line 126
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    .line 127
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    .line 131
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->i:Z

    .line 134
    invoke-direct {p0, p2}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(Lcom/dropbox/android/filemanager/n;)V

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aG;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aG;->b(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(J)V

    .line 139
    :cond_0
    return-void
.end method

.method public final b(Landroid/database/Cursor;Lcom/dropbox/android/filemanager/n;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x5

    const/4 v2, 0x1

    .line 142
    invoke-direct {p0, p2}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Lcom/dropbox/android/filemanager/n;)V

    .line 144
    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/provider/Z;->g:Lcom/dropbox/android/provider/Z;

    if-ne v1, v3, :cond_2

    move v1, v2

    .line 146
    :goto_0
    if-nez v1, :cond_3

    .line 147
    invoke-static {p1}, Lcom/dropbox/android/albums/AlbumItemEntry;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/AlbumItemEntry;

    move-result-object v1

    .line 149
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    .line 150
    invoke-virtual {v1}, Lcom/dropbox/android/albums/AlbumItemEntry;->c()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    .line 152
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Ljava/lang/String;)V

    .line 153
    invoke-virtual {v1}, Lcom/dropbox/android/albums/AlbumItemEntry;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    .line 154
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->i:Z

    .line 156
    invoke-virtual {v1}, Lcom/dropbox/android/albums/AlbumItemEntry;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    .line 167
    :goto_1
    invoke-direct {p0, p2}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(Lcom/dropbox/android/filemanager/n;)V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aG;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->m:Lcom/dropbox/android/widget/aG;

    iget-object v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aG;->b(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(J)V

    .line 172
    :cond_1
    return-void

    .line 144
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 160
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    .line 161
    iput-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    .line 162
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Ljava/lang/String;)V

    .line 163
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->i:Z

    .line 165
    iput-boolean v2, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->j:Z

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 205
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 206
    invoke-direct {p0}, Lcom/dropbox/android/widget/ThumbGridItemView;->b()V

    .line 209
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ThumbGridItemView mCurrentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mCurrentPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mCurrentPath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 346
    return-object v0

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->f:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/ThumbGridItemView;->h:Ljava/lang/String;

    goto :goto_1
.end method
