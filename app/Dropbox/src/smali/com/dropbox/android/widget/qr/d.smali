.class final Lcom/dropbox/android/widget/qr/d;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/widget/qr/c;

.field private b:Ljava/lang/Thread;

.field private c:Landroid/hardware/Camera;

.field private d:Lcom/dropbox/android/widget/qr/a;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;

.field private g:Z

.field private h:Z

.field private final i:Lcom/dropbox/android/widget/qr/i;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcom/dropbox/android/widget/qr/c;

    invoke-direct {v0}, Lcom/dropbox/android/widget/qr/c;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    .line 59
    new-instance v0, Lcom/dropbox/android/widget/qr/i;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/qr/i;-><init>(Lcom/dropbox/android/widget/qr/c;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->i:Lcom/dropbox/android/widget/qr/i;

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/qr/d;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/qr/d;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;

    return-object v0
.end method

.method private declared-synchronized a(Landroid/view/SurfaceHolder;III)V
    .locals 6

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 130
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v0, :cond_3

    .line 175
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 136
    :cond_2
    :try_start_3
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 152
    :goto_2
    :try_start_5
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 153
    const/4 v0, 0x0

    invoke-static {v0, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 154
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/qr/c;->a(Landroid/hardware/Camera;Landroid/hardware/Camera$CameraInfo;III)V

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 159
    if-nez v0, :cond_4

    const/4 v0, 0x0

    .line 161
    :goto_3
    :try_start_6
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    iget-object v2, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/widget/qr/c;->a(Landroid/hardware/Camera;Z)V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 162
    :catch_0
    move-exception v1

    .line 164
    if-eqz v0, :cond_1

    .line 165
    :try_start_7
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 166
    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->unflatten(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 168
    :try_start_8
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/qr/c;->a(Landroid/hardware/Camera;Z)V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 170
    :catch_1
    move-exception v0

    goto :goto_1

    .line 159
    :cond_4
    :try_start_9
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    goto :goto_3

    .line 147
    :catch_2
    move-exception v0

    goto :goto_2

    .line 131
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/qr/d;Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/widget/qr/d;->a(Landroid/view/SurfaceHolder;III)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/qr/d;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->h:Z

    return v0
.end method


# virtual methods
.method public final a([BII)Ldbxyzptlk/db231222/I/i;
    .locals 9

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/d;->g()Landroid/graphics/Rect;

    move-result-object v1

    .line 313
    if-nez v1, :cond_0

    .line 314
    const/4 v0, 0x0

    .line 317
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/I/i;

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v7

    const/4 v8, 0x0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v8}, Ldbxyzptlk/db231222/I/i;-><init>([BIIIIIIZ)V

    goto :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/widget/qr/e;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/qr/e;-><init>(Lcom/dropbox/android/widget/qr/d;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :cond_0
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/os/Handler;I)V
    .locals 2

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->g:Z

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->i:Lcom/dropbox/android/widget/qr/i;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/widget/qr/i;->a(Landroid/os/Handler;I)V

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->i:Lcom/dropbox/android/widget/qr/i;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :cond_0
    monitor-exit p0

    return-void

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/view/SurfaceHolder;Landroid/os/Handler;III)V
    .locals 8

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->h:Z

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->h:Z

    .line 101
    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Lcom/dropbox/android/widget/qr/f;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/qr/f;-><init>(Lcom/dropbox/android/widget/qr/d;Landroid/view/SurfaceHolder;IIILandroid/os/Handler;)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_0
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 184
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->b:Ljava/lang/Thread;

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->e:Landroid/graphics/Rect;

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->f:Landroid/graphics/Rect;

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->g:Z

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->g:Z

    .line 198
    new-instance v0, Lcom/dropbox/android/widget/qr/a;

    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/qr/a;-><init>(Landroid/hardware/Camera;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->d:Lcom/dropbox/android/widget/qr/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_0
    monitor-exit p0

    return-void

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->d:Lcom/dropbox/android/widget/qr/a;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->d:Lcom/dropbox/android/widget/qr/a;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/a;->b()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->d:Lcom/dropbox/android/widget/qr/a;

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->g:Z

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->i:Lcom/dropbox/android/widget/qr/i;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/qr/i;->a(Landroid/os/Handler;I)V

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/qr/d;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :cond_1
    monitor-exit p0

    return-void

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/c;->b()Landroid/graphics/Point;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->e:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/c;->b()Landroid/graphics/Point;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/qr/c;->c()Landroid/graphics/Point;

    move-result-object v1

    .line 253
    iget-object v2, p0, Lcom/dropbox/android/widget/qr/d;->c:Landroid/hardware/Camera;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 255
    :cond_0
    const/4 v0, 0x0

    .line 266
    :goto_0
    monitor-exit p0

    return-object v0

    .line 260
    :cond_1
    :try_start_1
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 261
    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 262
    new-instance v3, Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v4, v2

    iget v1, v1, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v0

    invoke-direct {v3, v2, v0, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lcom/dropbox/android/widget/qr/d;->e:Landroid/graphics/Rect;

    .line 266
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->e:Landroid/graphics/Rect;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 274
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->f:Landroid/graphics/Rect;

    if-nez v1, :cond_2

    .line 275
    invoke-virtual {p0}, Lcom/dropbox/android/widget/qr/d;->f()Landroid/graphics/Rect;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 276
    if-nez v1, :cond_1

    .line 298
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 279
    :cond_1
    :try_start_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 280
    iget-object v1, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/qr/c;->b()Landroid/graphics/Point;

    move-result-object v1

    .line 281
    iget-object v3, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/qr/c;->e()F

    move-result v3

    .line 282
    if-eqz v1, :cond_0

    .line 286
    iget v0, v2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    .line 287
    iget v0, v2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 288
    iget v0, v2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->top:I

    .line 289
    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->a:Lcom/dropbox/android/widget/qr/c;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/c;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293
    new-instance v0, Landroid/graphics/Rect;

    iget v1, v2, Landroid/graphics/Rect;->top:I

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Rect;->right:I

    invoke-direct {v0, v1, v3, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/dropbox/android/widget/qr/d;->f:Landroid/graphics/Rect;

    .line 298
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/widget/qr/d;->f:Landroid/graphics/Rect;

    goto :goto_0

    .line 295
    :cond_3
    iput-object v2, p0, Lcom/dropbox/android/widget/qr/d;->f:Landroid/graphics/Rect;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
