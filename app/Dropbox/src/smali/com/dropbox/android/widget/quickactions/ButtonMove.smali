.class public Lcom/dropbox/android/widget/quickactions/ButtonMove;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final d:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonMove;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f030087

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/activity/MoveActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    const-string v1, "com.dropbox.android.activity.ENTRY"

    iget-object v2, p0, Lcom/dropbox/android/widget/quickactions/ButtonMove;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 33
    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 34
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f0d01e8

    return v0
.end method
