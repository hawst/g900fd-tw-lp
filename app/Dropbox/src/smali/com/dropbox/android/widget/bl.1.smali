.class final Lcom/dropbox/android/widget/bl;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:I

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/bl;->a:I

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/bl;->b:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/widget/bj;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/dropbox/android/widget/bl;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/dropbox/android/widget/bl;->a:I

    if-le p1, v0, :cond_0

    .line 88
    iput p1, p0, Lcom/dropbox/android/widget/bl;->a:I

    .line 90
    :cond_0
    return-void
.end method

.method protected final a(Landroid/database/Cursor;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/dropbox/android/widget/bl;->b:Ljava/util/Map;

    invoke-static {p1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method
