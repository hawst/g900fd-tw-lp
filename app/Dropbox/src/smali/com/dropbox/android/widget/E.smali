.class public final Lcom/dropbox/android/widget/E;
.super Lcom/android/ex/chips/a;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/service/A;

.field private b:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/service/A;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/android/ex/chips/a;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p2, p0, Lcom/dropbox/android/widget/E;->a:Lcom/dropbox/android/service/A;

    .line 20
    iput-object p3, p0, Lcom/dropbox/android/widget/E;->b:[Ljava/lang/String;

    .line 21
    return-void
.end method

.method private a(Lcom/dropbox/android/service/C;Ljava/util/regex/Pattern;)Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/dropbox/android/service/C;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/dropbox/android/service/C;->b:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 30
    new-instance v2, Landroid/database/MatrixCursor;

    iget-object v0, p0, Lcom/dropbox/android/widget/E;->b:[Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".*(^|\\s|\\+|@|\\.|<)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 37
    iget-object v0, p0, Lcom/dropbox/android/widget/E;->a:Lcom/dropbox/android/service/A;

    invoke-virtual {v0}, Lcom/dropbox/android/service/A;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/C;

    .line 38
    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/widget/E;->a(Lcom/dropbox/android/service/C;Ljava/util/regex/Pattern;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    const-string v1, ""

    .line 40
    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/dropbox/android/service/C;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lcom/dropbox/android/service/C;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    iget v6, v0, Lcom/dropbox/android/service/C;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x3

    aput-object v1, v5, v6

    const/4 v1, 0x4

    iget-wide v6, v0, Lcom/dropbox/android/service/C;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x5

    iget-wide v6, v0, Lcom/dropbox/android/service/C;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x6

    iget-object v1, v0, Lcom/dropbox/android/service/C;->g:Landroid/net/Uri;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    aput-object v1, v5, v6

    const/4 v1, 0x7

    iget v0, v0, Lcom/dropbox/android/service/C;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/dropbox/android/service/C;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 53
    :cond_2
    return-object v2
.end method
