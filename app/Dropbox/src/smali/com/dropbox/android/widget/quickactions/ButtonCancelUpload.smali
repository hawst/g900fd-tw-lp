.class public Lcom/dropbox/android/widget/quickactions/ButtonCancelUpload;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field protected final d:J

.field private final e:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(JLcom/dropbox/android/filemanager/I;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 14
    iput-wide p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelUpload;->d:J

    .line 15
    iput-object p3, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelUpload;->e:Lcom/dropbox/android/filemanager/I;

    .line 16
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/quickactions/ButtonCancelUpload;)Lcom/dropbox/android/filemanager/I;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelUpload;->e:Lcom/dropbox/android/filemanager/I;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f030081

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/widget/quickactions/b;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/quickactions/b;-><init>(Lcom/dropbox/android/widget/quickactions/ButtonCancelUpload;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 27
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f0d01ec

    return v0
.end method
