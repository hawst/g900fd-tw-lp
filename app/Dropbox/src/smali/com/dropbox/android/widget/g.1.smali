.class public final Lcom/dropbox/android/widget/g;
.super Lcom/dropbox/android/widget/bC;
.source "panda.py"


# static fields
.field private static final a:I

.field private static final b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/dropbox/android/provider/Z;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private g:Lcom/dropbox/android/filemanager/n;

.field private final h:Lcom/dropbox/android/activity/di;

.field private final i:Lcom/dropbox/android/taskqueue/D;

.field private final j:Lcom/dropbox/android/filemanager/v;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    invoke-static {}, Lcom/dropbox/android/widget/j;->values()[Lcom/dropbox/android/widget/j;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/dropbox/android/widget/g;->a:I

    .line 39
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/dropbox/android/provider/Z;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/dropbox/android/widget/g;->b:Ljava/util/EnumMap;

    .line 40
    sget-object v0, Lcom/dropbox/android/widget/g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->e:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/j;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/j;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/dropbox/android/widget/g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->h:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/j;->b:Lcom/dropbox/android/widget/j;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/j;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/dropbox/android/widget/g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->j:Lcom/dropbox/android/provider/Z;

    sget-object v2, Lcom/dropbox/android/widget/j;->c:Lcom/dropbox/android/widget/j;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/j;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/activity/di;Lcom/dropbox/android/taskqueue/D;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/bC;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v0, Lcom/dropbox/android/widget/h;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/h;-><init>(Lcom/dropbox/android/widget/g;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/g;->j:Lcom/dropbox/android/filemanager/v;

    .line 145
    iput-object p2, p0, Lcom/dropbox/android/widget/g;->h:Lcom/dropbox/android/activity/di;

    .line 146
    iput-object p3, p0, Lcom/dropbox/android/widget/g;->i:Lcom/dropbox/android/taskqueue/D;

    .line 147
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 191
    sget v0, Lcom/dropbox/android/widget/g;->a:I

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 97
    invoke-static {}, Lcom/dropbox/android/widget/j;->values()[Lcom/dropbox/android/widget/j;

    move-result-object v0

    aget-object v0, v0, p2

    .line 98
    sget-object v1, Lcom/dropbox/android/widget/i;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/widget/j;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 108
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->c:Landroid/content/Context;

    const v1, 0x7f030052

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    .line 102
    :pswitch_1
    new-instance v0, Lcom/dropbox/android/widget/AlbumOverviewListItem;

    iget-object v1, p0, Lcom/dropbox/android/widget/g;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/AlbumOverviewListItem;-><init>(Landroid/content/Context;)V

    .line 103
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 106
    :pswitch_2
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->c:Landroid/content/Context;

    const v1, 0x7f03002f

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/database/Cursor;Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 114
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/dropbox/android/widget/g;->h:Lcom/dropbox/android/activity/di;

    invoke-interface {v1}, Lcom/dropbox/android/activity/di;->a()I

    move-result v1

    .line 116
    iget-object v2, p0, Lcom/dropbox/android/widget/g;->h:Lcom/dropbox/android/activity/di;

    invoke-interface {v2}, Lcom/dropbox/android/activity/di;->b()I

    move-result v2

    .line 117
    iget-object v3, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    sub-int/2addr v2, v1

    invoke-virtual {v3, v1, v2}, Lcom/dropbox/android/filemanager/n;->a(II)V

    .line 119
    invoke-static {}, Lcom/dropbox/android/widget/j;->values()[Lcom/dropbox/android/widget/j;

    move-result-object v1

    aget-object v1, v1, p3

    .line 120
    sget-object v2, Lcom/dropbox/android/widget/i;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/widget/j;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 132
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :pswitch_0
    const-string v0, "_separator_text"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 123
    const v0, 0x7f0700df

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_0
    :pswitch_1
    return-void

    .line 126
    :pswitch_2
    invoke-static {p1}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;

    move-result-object v1

    .line 127
    check-cast p2, Lcom/dropbox/android/widget/AlbumOverviewListItem;

    iget-object v2, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {p2, v1, v0, v2}, Lcom/dropbox/android/widget/AlbumOverviewListItem;->a(Lcom/dropbox/android/albums/Album;ILcom/dropbox/android/filemanager/n;)V

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/n;->a(Z)V

    .line 73
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 173
    sget-object v0, Lcom/dropbox/android/widget/i;->b:[I

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 179
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 176
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    .line 186
    sget-object v0, Lcom/dropbox/android/widget/g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final b()Lcom/dropbox/android/widget/bG;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 138
    new-instance v0, Lcom/dropbox/android/widget/bG;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/dropbox/android/widget/bG;-><init>(ZZZ)V

    return-object v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/j;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/j;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 82
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/Z;->h:Lcom/dropbox/android/provider/Z;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 87
    const v0, 0x7f020079

    return v0
.end method

.method public final d(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 151
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/n;->a()V

    .line 154
    :cond_0
    if-eqz p1, :cond_1

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    if-nez v0, :cond_2

    .line 156
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/g;->j:Lcom/dropbox/android/filemanager/v;

    invoke-static {}, Lcom/dropbox/android/util/bn;->g()Ldbxyzptlk/db231222/v/n;

    move-result-object v3

    invoke-static {}, Lcom/dropbox/android/util/bn;->j()I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/widget/g;->i:Lcom/dropbox/android/taskqueue/D;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Ldbxyzptlk/db231222/v/n;ILcom/dropbox/android/taskqueue/D;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    .line 167
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/dropbox/android/widget/bC;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 163
    :cond_2
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/g;->j:Lcom/dropbox/android/filemanager/v;

    iget-object v3, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Lcom/dropbox/android/filemanager/n;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/g;->g:Lcom/dropbox/android/filemanager/n;

    goto :goto_0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/dropbox/android/widget/g;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method
