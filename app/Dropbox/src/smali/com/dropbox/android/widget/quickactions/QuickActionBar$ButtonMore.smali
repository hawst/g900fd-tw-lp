.class public Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field protected final d:[Lcom/dropbox/android/widget/quickactions/a;

.field protected final e:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field final synthetic f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;[Lcom/dropbox/android/widget/quickactions/a;Lcom/dropbox/android/widget/quickactions/QuickActionBar;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 192
    iput-object p2, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->d:[Lcom/dropbox/android/widget/quickactions/a;

    .line 193
    iput-object p3, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->e:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 194
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 217
    const v0, 0x7f030086

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->e:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->d:[Lcom/dropbox/android/widget/quickactions/a;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a([Lcom/dropbox/android/widget/quickactions/a;)V

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-static {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-static {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-static {v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openContextMenu(Landroid/view/View;)V

    .line 213
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 198
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->e()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "which"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 201
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->c:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->a(Landroid/support/v4/app/Fragment;)V

    .line 202
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->d()V

    .line 203
    return-void
.end method
