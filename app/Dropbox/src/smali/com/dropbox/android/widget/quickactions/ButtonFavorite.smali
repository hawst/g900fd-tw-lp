.class public Lcom/dropbox/android/widget/quickactions/ButtonFavorite;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final d:Lcom/dropbox/android/filemanager/LocalEntry;

.field private final e:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/I;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 17
    iput-object p2, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->e:Lcom/dropbox/android/filemanager/I;

    .line 19
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t create a favorite button for a directory!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    const v0, 0x7f030085

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 59
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->e:Lcom/dropbox/android/filemanager/I;

    iget-object v2, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/LocalEntry;Z)V

    .line 60
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 32
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    check-cast p1, Landroid/widget/Button;

    .line 34
    const v0, 0x7f0d01ee

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    .line 36
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->d:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const v0, 0x7f0d01ee

    .line 52
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0d01e5

    goto :goto_0
.end method
