.class public Lcom/dropbox/android/widget/DbxMediaController;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private A:Lcom/dropbox/android/widget/P;

.field private final B:Lcom/dropbox/android/widget/N;

.field private C:Lcom/dropbox/android/widget/R;

.field private final D:Ljava/lang/String;

.field private E:I

.field private final F:Ldbxyzptlk/db231222/z/M;

.field private G:Landroid/view/View$OnLayoutChangeListener;

.field private H:Landroid/view/View$OnTouchListener;

.field private final I:Landroid/os/Handler;

.field private J:Landroid/view/View$OnClickListener;

.field private K:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private L:Landroid/view/View$OnClickListener;

.field private M:Landroid/view/View$OnClickListener;

.field private N:Lcom/dropbox/android/widget/Q;

.field a:Ljava/lang/StringBuilder;

.field b:Ljava/util/Formatter;

.field private d:Lcom/dropbox/android/widget/O;

.field private e:Landroid/content/Context;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/view/WindowManager;

.field private i:Landroid/view/Window;

.field private j:Landroid/view/View;

.field private k:Landroid/view/WindowManager$LayoutParams;

.field private l:Landroid/widget/ProgressBar;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Landroid/view/View$OnClickListener;

.field private u:Landroid/view/View$OnClickListener;

.field private v:Landroid/widget/ImageButton;

.field private w:Landroid/widget/ImageButton;

.field private x:Landroid/widget/ImageButton;

.field private y:Landroid/widget/ImageButton;

.field private z:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;ZLcom/dropbox/android/widget/N;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->C:Lcom/dropbox/android/widget/R;

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    .line 257
    new-instance v0, Lcom/dropbox/android/widget/F;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/F;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->G:Landroid/view/View$OnLayoutChangeListener;

    .line 270
    new-instance v0, Lcom/dropbox/android/widget/G;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/G;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/view/View$OnTouchListener;

    .line 490
    new-instance v0, Lcom/dropbox/android/widget/I;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/I;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    .line 673
    new-instance v0, Lcom/dropbox/android/widget/J;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/J;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->J:Landroid/view/View$OnClickListener;

    .line 743
    new-instance v0, Lcom/dropbox/android/widget/K;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/K;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->K:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 838
    new-instance v0, Lcom/dropbox/android/widget/L;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/L;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->L:Landroid/view/View$OnClickListener;

    .line 850
    new-instance v0, Lcom/dropbox/android/widget/M;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/M;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Landroid/view/View$OnClickListener;

    .line 180
    iput-object p2, p0, Lcom/dropbox/android/widget/DbxMediaController;->F:Ldbxyzptlk/db231222/z/M;

    .line 181
    iput-object p4, p0, Lcom/dropbox/android/widget/DbxMediaController;->B:Lcom/dropbox/android/widget/N;

    .line 182
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    .line 183
    iput-boolean p3, p0, Lcom/dropbox/android/widget/DbxMediaController;->q:Z

    .line 184
    iput-object p5, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    .line 185
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->i()V

    .line 186
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->h()V

    .line 187
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/view/Window;
    .locals 5

    .prologue
    .line 192
    :try_start_0
    const-string v0, "com.android.internal.policy.PolicyManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 193
    const-string v1, "makeNewWindow"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 194
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 197
    :catch_1
    move-exception v0

    .line 198
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 199
    :catch_2
    move-exception v0

    .line 200
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 201
    :catch_3
    move-exception v0

    .line 202
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 335
    const v0, 0x7f0700fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    .line 336
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 338
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->J:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    :cond_0
    const v0, 0x7f0700ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    .line 342
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_1

    .line 345
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->q:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 349
    :cond_1
    const v0, 0x7f0700fe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    .line 350
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 351
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->L:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_2

    .line 353
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->q:Z

    if-eqz v3, :cond_8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 358
    :cond_2
    const v0, 0x7f070100

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    .line 359
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->s:Z

    if-nez v0, :cond_3

    .line 360
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 362
    :cond_3
    const v0, 0x7f0700fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    .line 363
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->s:Z

    if-nez v0, :cond_4

    .line 364
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 367
    :cond_4
    const v0, 0x7f070102

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    .line 368
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_6

    .line 369
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    instance-of v0, v0, Landroid/widget/SeekBar;

    if-eqz v0, :cond_5

    .line 370
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/SeekBar;

    .line 371
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->K:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 373
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020261

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 376
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 379
    :cond_6
    const v0, 0x7f0700fc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/widget/H;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/H;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 388
    const v0, 0x7f070103

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->m:Landroid/widget/TextView;

    .line 389
    const v0, 0x7f070101

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->a:Ljava/lang/StringBuilder;

    .line 391
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->a:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->b:Ljava/util/Formatter;

    .line 393
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->p()V

    .line 394
    return-void

    :cond_7
    move v0, v2

    .line 345
    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 353
    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->j()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxMediaController;I)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxMediaController;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxMediaController;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->p:Z

    return p1
.end method

.method private b(I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 548
    div-int/lit16 v0, p1, 0x3e8

    .line 550
    rem-int/lit8 v1, v0, 0x3c

    .line 551
    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    .line 552
    div-int/lit16 v0, v0, 0xe10

    .line 554
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 555
    if-lez v0, :cond_0

    .line 556
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->b:Ljava/util/Formatter;

    const-string v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 558
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->b:Ljava/util/Formatter;

    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-virtual {v0, v3, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxMediaController;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxMediaController;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxMediaController;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 716
    mul-int/lit8 v0, p1, 0xa

    .line 717
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    if-le v0, v1, :cond_0

    .line 718
    iput v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    .line 720
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/view/WindowManager$LayoutParams;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/view/WindowManager;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DbxMediaController;)I
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->l()I

    move-result v0

    return v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DbxMediaController;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->p:Z

    return v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/O;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(Landroid/content/Context;)Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    invoke-virtual {v0, v1, v3, v3}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    invoke-virtual {v0, p0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 218
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setVolumeControlStream(I)V

    .line 220
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setFocusable(Z)V

    .line 221
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setFocusableInTouchMode(Z)V

    .line 222
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->setDescendantFocusability(I)V

    .line 223
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->requestFocus()Z

    .line 224
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 230
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    .line 231
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    .line 232
    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 233
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 234
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 235
    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 236
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 237
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v2, 0x820020

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 240
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 241
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 242
    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/widget/DbxMediaController;)Z
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v0

    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 247
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 248
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 250
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    .line 251
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 254
    return-void
.end method

.method static synthetic j(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->q()V

    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 410
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 414
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 417
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->i()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 422
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :cond_3
    :goto_0
    return-void

    .line 424
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->n()V

    return-void
.end method

.method private l()I
    .locals 6

    .prologue
    .line 563
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->p:Z

    if-eqz v0, :cond_2

    .line 564
    :cond_0
    const/4 v0, 0x0

    .line 591
    :cond_1
    :goto_0
    return v0

    .line 566
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->d()I

    move-result v0

    .line 567
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->c()I

    move-result v1

    .line 569
    int-to-long v2, v1

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 570
    const/16 v2, 0x64

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;->c(I)V

    .line 573
    :cond_3
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_5

    .line 574
    if-lez v1, :cond_4

    .line 576
    const-wide/16 v2, 0x3e8

    int-to-long v4, v0

    mul-long/2addr v2, v4

    int-to-long v4, v1

    div-long/2addr v2, v4

    .line 577
    iget-object v4, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 578
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v4

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_4

    .line 579
    long-to-int v2, v2

    iput v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    .line 582
    :cond_4
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v2}, Lcom/dropbox/android/widget/O;->f()I

    move-result v2

    .line 583
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 586
    :cond_5
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->m:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 587
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->m:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    :cond_6
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 589
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic l(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic m(Lcom/dropbox/android/widget/DbxMediaController;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    return v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 686
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 687
    const v0, 0x7f02024e

    .line 693
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 690
    :cond_2
    const v0, 0x7f02024f

    goto :goto_1
.end method

.method static synthetic n(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 698
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 699
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->b()V

    .line 700
    sget-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    const-string v1, "PAUSED"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->N:Lcom/dropbox/android/widget/Q;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->N:Lcom/dropbox/android/widget/Q;

    invoke-interface {v0}, Lcom/dropbox/android/widget/Q;->b()V

    .line 711
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 712
    return-void

    .line 705
    :cond_1
    sget-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    const-string v1, "RESUMED"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v0}, Lcom/dropbox/android/widget/O;->a()V

    .line 707
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->N:Lcom/dropbox/android/widget/Q;

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->N:Lcom/dropbox/android/widget/Q;

    invoke-interface {v0}, Lcom/dropbox/android/widget/Q;->a()V

    goto :goto_0
.end method

.method static synthetic o(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 727
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic p(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/R;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->C:Lcom/dropbox/android/widget/R;

    return-object v0
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 865
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 866
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 867
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 871
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 872
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 874
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 867
    goto :goto_0

    :cond_3
    move v1, v2

    .line 872
    goto :goto_1
.end method

.method private q()V
    .locals 2

    .prologue
    .line 935
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->F:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/widget/S;->a(Lcom/dropbox/android/widget/DbxMediaController;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V

    .line 938
    :cond_0
    return-void
.end method

.method static synthetic q(Lcom/dropbox/android/widget/DbxMediaController;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    return-void
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 314
    const v1, 0x7f03005a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    .line 316
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(Landroid/view/View;)V

    .line 318
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    return-object v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 439
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->q()V

    .line 441
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 442
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->l()I

    .line 443
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 446
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->k()V

    .line 447
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->j()V

    .line 448
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    iput-boolean v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    .line 451
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 456
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 458
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 459
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 460
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 461
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 463
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 401
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 402
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    if-eqz v0, :cond_0

    .line 478
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 479
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 483
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    .line 484
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->A:Lcom/dropbox/android/widget/P;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->A:Lcom/dropbox/android/widget/P;

    invoke-interface {v0}, Lcom/dropbox/android/widget/P;->a()V

    goto :goto_0

    .line 480
    :catch_0
    move-exception v0

    .line 481
    const-string v0, "MediaController"

    const-string v1, "already removed"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/16 v4, 0xbb8

    const/4 v0, 0x1

    .line 608
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 610
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    move v1, v0

    .line 612
    :goto_0
    const/16 v3, 0x4f

    if-eq v2, v3, :cond_0

    const/16 v3, 0x55

    if-eq v2, v3, :cond_0

    const/16 v3, 0x3e

    if-ne v2, v3, :cond_3

    .line 615
    :cond_0
    if-eqz v1, :cond_1

    .line 616
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->n()V

    .line 617
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 618
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1

    .line 619
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 670
    :cond_1
    :goto_1
    return v0

    .line 610
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 623
    :cond_3
    const/16 v3, 0x7e

    if-ne v2, v3, :cond_4

    .line 624
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 625
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->a()V

    .line 626
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 627
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    goto :goto_1

    .line 630
    :cond_4
    const/16 v3, 0x56

    if-eq v2, v3, :cond_5

    const/16 v3, 0x7f

    if-ne v2, v3, :cond_6

    .line 632
    :cond_5
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 633
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    invoke-interface {v1}, Lcom/dropbox/android/widget/O;->b()V

    .line 634
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 635
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    goto :goto_1

    .line 638
    :cond_6
    const/16 v3, 0x19

    if-eq v2, v3, :cond_7

    const/16 v3, 0x18

    if-eq v2, v3, :cond_7

    const/16 v3, 0xa4

    if-ne v2, v3, :cond_8

    .line 642
    :cond_7
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    .line 643
    :cond_8
    const/16 v3, 0x52

    if-ne v2, v3, :cond_9

    .line 644
    if-eqz v1, :cond_1

    .line 645
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_1

    .line 650
    :cond_9
    const/4 v1, 0x4

    if-ne v2, v1, :cond_c

    .line 651
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_a

    .line 652
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 653
    if-eqz v1, :cond_1

    .line 654
    invoke-virtual {v1, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    goto :goto_1

    .line 657
    :cond_a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_c

    .line 658
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 659
    if-eqz v1, :cond_b

    .line 660
    invoke-virtual {v1, p1}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 662
    :cond_b
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_c

    .line 663
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->B:Lcom/dropbox/android/widget/N;

    invoke-interface {v1}, Lcom/dropbox/android/widget/N;->onBackPressed()V

    goto/16 :goto_1

    .line 669
    :cond_c
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 670
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 534
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 542
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 543
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 544
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(Landroid/view/View;)V

    .line 175
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 596
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 597
    const/4 v0, 0x1

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 602
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 603
    const/4 v0, 0x0

    return v0
.end method

.method public setAnchorView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->G:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 296
    :cond_0
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->G:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 301
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->removeAllViews()V

    .line 302
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->a()Landroid/view/View;

    move-result-object v0

    .line 303
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->addView(Landroid/view/View;)V

    .line 304
    return-void
.end method

.method public setEnabled(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 816
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 820
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 822
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 823
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 825
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 826
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 828
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 829
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz p1, :cond_7

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_7

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 831
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_5

    .line 832
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setEnabled(Z)V

    .line 834
    :cond_5
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->k()V

    .line 835
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 836
    return-void

    :cond_6
    move v0, v2

    .line 826
    goto :goto_0

    :cond_7
    move v1, v2

    .line 829
    goto :goto_1
.end method

.method public setMediaPlayer(Lcom/dropbox/android/widget/O;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/O;

    .line 284
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 285
    return-void
.end method

.method public setOnHideListener(Lcom/dropbox/android/widget/P;)V
    .locals 0

    .prologue
    .line 894
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->A:Lcom/dropbox/android/widget/P;

    .line 895
    return-void
.end method

.method public setOnPlayPauseListener(Lcom/dropbox/android/widget/Q;)V
    .locals 0

    .prologue
    .line 902
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->N:Lcom/dropbox/android/widget/Q;

    .line 903
    return-void
.end method

.method public setOnUserSeekListener(Lcom/dropbox/android/widget/R;)V
    .locals 0

    .prologue
    .line 898
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->C:Lcom/dropbox/android/widget/R;

    .line 899
    return-void
.end method

.method public setPrevNextListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 877
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    .line 878
    iput-object p2, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    .line 879
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->s:Z

    .line 881
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 882
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->p()V

    .line 884
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 891
    :cond_1
    return-void
.end method
