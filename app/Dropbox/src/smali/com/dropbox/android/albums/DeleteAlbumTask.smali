.class Lcom/dropbox/android/albums/DeleteAlbumTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 22
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/DeleteAlbumTask;->a:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/DeleteAlbumTask;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lcom/dropbox/android/albums/DeleteAlbumTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/DeleteAlbumTask;->d:I

    .line 45
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/DeleteAlbumTask;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->f(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/albums/DeleteAlbumTask;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 50
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 54
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->h()V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_0

    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 64
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_0
    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    const-string v1, "collectionDelete"

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
