.class final Lcom/dropbox/android/albums/p;
.super Lcom/dropbox/android/albums/r;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dropbox/android/taskqueue/u;",
        ">",
        "Lcom/dropbox/android/albums/r",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/android/albums/PhotosModel;

.field private final b:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/t;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/PhotosModel;",
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 684
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/albums/r;-><init>(Lcom/dropbox/android/albums/l;)V

    .line 685
    iput-object p1, p0, Lcom/dropbox/android/albums/p;->a:Lcom/dropbox/android/albums/PhotosModel;

    .line 686
    iput-object p2, p0, Lcom/dropbox/android/albums/p;->b:Lcom/dropbox/android/albums/t;

    .line 687
    iput-object p3, p0, Lcom/dropbox/android/albums/p;->c:Ljava/lang/String;

    .line 688
    return-void
.end method


# virtual methods
.method protected final a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 692
    iget-object v0, p0, Lcom/dropbox/android/albums/p;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/albums/p;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    .line 693
    iget-object v1, p0, Lcom/dropbox/android/albums/p;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 694
    return-void
.end method

.method protected final a(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/dropbox/android/taskqueue/w;",
            ")V"
        }
    .end annotation

    .prologue
    .line 698
    iget-object v0, p0, Lcom/dropbox/android/albums/p;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/albums/p;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    .line 699
    iget-object v1, p0, Lcom/dropbox/android/albums/p;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 700
    return-void
.end method
