.class Lcom/dropbox/android/albums/PhotosModel$CreateTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Ljava/lang/String;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private g:Ldbxyzptlk/db231222/z/p;


# direct methods
.method constructor <init>(ZZLjava/lang/String;Ljava/util/List;Lcom/dropbox/android/albums/PhotosModel;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/provider/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/albums/PhotosModel;",
            "Ldbxyzptlk/db231222/z/M;",
            "Lcom/dropbox/android/provider/j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 610
    invoke-direct {p0, p5, p7, p6}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 611
    iput-boolean p1, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->b:Z

    .line 612
    iput-boolean p2, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a:Z

    .line 613
    iput-object p3, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->c:Ljava/lang/String;

    .line 614
    iput-object p4, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->e:Ljava/util/List;

    .line 615
    const-string v0, "\\"

    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->e:Ljava/util/List;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 616
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->f:Ljava/lang/String;

    .line 617
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 621
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 631
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 6

    .prologue
    .line 639
    iget v0, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->d:I

    .line 640
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->r()V

    .line 642
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->b:Z

    if-eqz v0, :cond_0

    sget-object v1, Ldbxyzptlk/db231222/z/y;->b:Ldbxyzptlk/db231222/z/y;

    .line 644
    :goto_0
    iget-boolean v0, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->b:Z

    if-eqz v0, :cond_1

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 645
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->e:Ljava/util/List;

    iget-boolean v4, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a:Z

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/z/M;->a(Ldbxyzptlk/db231222/z/y;Ljava/lang/String;Ljava/util/List;ZLjava/util/Date;)Ldbxyzptlk/db231222/z/h;

    move-result-object v0

    .line 647
    iget-object v1, v0, Ldbxyzptlk/db231222/z/h;->a:Ldbxyzptlk/db231222/z/p;

    iput-object v1, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->g:Ldbxyzptlk/db231222/z/p;

    .line 648
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 649
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 651
    :try_start_1
    iget-object v2, v0, Ldbxyzptlk/db231222/z/h;->a:Ldbxyzptlk/db231222/z/p;

    iget-boolean v3, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->b:Z

    invoke-static {v1, v2, v3}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V

    .line 652
    iget-object v2, v0, Ldbxyzptlk/db231222/z/h;->a:Ldbxyzptlk/db231222/z/p;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/h;->b:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    .line 653
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 657
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->h()V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 668
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 670
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_2
    return-object v0

    .line 642
    :cond_0
    :try_start_3
    sget-object v1, Ldbxyzptlk/db231222/z/y;->a:Ldbxyzptlk/db231222/z/y;

    goto :goto_0

    .line 644
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 655
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 658
    :catch_0
    move-exception v0

    .line 660
    const-string v1, "collectionCreate"

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 661
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_2

    .line 662
    :catch_1
    move-exception v0

    .line 663
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V

    .line 664
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_2
.end method

.method public final d()Ldbxyzptlk/db231222/z/p;
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->g:Ldbxyzptlk/db231222/z/p;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
