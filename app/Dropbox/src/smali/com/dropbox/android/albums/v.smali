.class final Lcom/dropbox/android/albums/v;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/q;


# instance fields
.field private final a:Ldbxyzptlk/db231222/j/i;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/j/i;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/dropbox/android/albums/v;->a:Ldbxyzptlk/db231222/j/i;

    .line 19
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/albums/v;)Ldbxyzptlk/db231222/j/i;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/dropbox/android/albums/v;->a:Ldbxyzptlk/db231222/j/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 5

    .prologue
    .line 27
    instance-of v0, p1, Lcom/dropbox/android/albums/AddTask;

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 31
    check-cast v0, Lcom/dropbox/android/albums/AddTask;

    .line 33
    new-instance v2, Lcom/dropbox/android/albums/a;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/AddTask;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/dropbox/android/albums/a;-><init>(Ljava/util/ArrayList;)V

    .line 35
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->b()Ljava/util/List;

    move-result-object v0

    .line 36
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 38
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 39
    iget-object v4, p0, Lcom/dropbox/android/albums/v;->a:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v4, v0, v2}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 40
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 42
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 43
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 44
    iget-object v4, p0, Lcom/dropbox/android/albums/v;->a:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v4, v0, v2}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    goto :goto_2

    .line 46
    :cond_1
    throw v1

    .line 49
    :cond_2
    new-instance v0, Lcom/dropbox/android/albums/w;

    invoke-direct {v0, p0, v2, v3}, Lcom/dropbox/android/albums/w;-><init>(Lcom/dropbox/android/albums/v;Ldbxyzptlk/db231222/j/g;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Lcom/dropbox/android/taskqueue/u;->a(Lcom/dropbox/android/taskqueue/v;)V

    goto :goto_0
.end method
