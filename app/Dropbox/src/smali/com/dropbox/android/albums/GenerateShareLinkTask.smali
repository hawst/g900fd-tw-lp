.class public Lcom/dropbox/android/albums/GenerateShareLinkTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->c:Ljava/lang/String;

    .line 28
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a:Ljava/lang/String;

    .line 29
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->b:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStatusPath() doesn\'t make sense for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 3

    .prologue
    .line 53
    iget v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->d:I

    .line 55
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->d(Ljava/lang/String;)Ldbxyzptlk/db231222/z/p;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :try_start_1
    iget-boolean v2, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->b:Z

    invoke-static {v1, v0, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V

    .line 60
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 64
    iget-object v0, v0, Ldbxyzptlk/db231222/z/p;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->c:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->h()V

    .line 67
    iget-object v1, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->c(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_0

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_0
    return-object v0

    .line 62
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    const-string v1, "shareCollection"

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    instance-of v1, v0, Ldbxyzptlk/db231222/w/i;

    if-eqz v1, :cond_0

    .line 72
    check-cast v0, Ldbxyzptlk/db231222/w/i;

    iget v0, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    .line 73
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_0
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/albums/GenerateShareLinkTask;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/albums/GenerateShareLinkTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
