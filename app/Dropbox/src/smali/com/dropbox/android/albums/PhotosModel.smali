.class public Lcom/dropbox/android/albums/PhotosModel;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final A:[Ljava/lang/String;

.field private static final B:Ljava/lang/String;

.field public static final a:I

.field private static final i:Ljava/lang/String;

.field private static final s:[Ljava/lang/String;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/lang/String;

.field private static final w:[Ljava/lang/String;

.field private static final x:[Ljava/lang/String;

.field private static final y:I

.field private static final z:[Ljava/lang/String;


# instance fields
.field public final b:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/dropbox/android/provider/j;

.field private final k:Ldbxyzptlk/db231222/z/M;

.field private final l:Ldbxyzptlk/db231222/n/P;

.field private final m:Landroid/content/ContentResolver;

.field private final n:Lcom/dropbox/android/taskqueue/TaskQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/albums/PhotosTask;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ldbxyzptlk/db231222/g/R;

.field private final p:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/provider/L;",
            ">;>;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/provider/L;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/dropbox/android/albums/q;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 70
    const-class v0, Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    .line 171
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 173
    invoke-static {}, Lcom/dropbox/android/provider/f;->a()[Lcom/dropbox/android/provider/c;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 174
    invoke-virtual {v5}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    const-string v0, "revision"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    const-string v0, "dropbox.path"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->s:[Ljava/lang/String;

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "albums LEFT JOIN dropbox ON ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/provider/f;->e:Lcom/dropbox/android/provider/c;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "dropbox"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "canon_path"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->t:Ljava/lang/String;

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ASC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->u:Ljava/lang/String;

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/h;->c:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ASC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->v:Ljava/lang/String;

    .line 263
    new-array v0, v7, [Ljava/lang/String;

    sget-object v2, Lcom/dropbox/android/provider/h;->c:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sget-object v2, Lcom/dropbox/android/provider/h;->e:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->x:[Ljava/lang/String;

    .line 269
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    sget-object v2, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sget-object v1, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->z:[Ljava/lang/String;

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 281
    sget-object v1, Lcom/dropbox/android/albums/PhotosModel;->x:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 282
    sget-object v1, Lcom/dropbox/android/albums/PhotosModel;->z:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 284
    const-string v1, "path"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const-string v2, "dropbox.path"

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 286
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->w:[Ljava/lang/String;

    .line 289
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->w:[Ljava/lang/String;

    sget-object v1, Lcom/dropbox/android/provider/h;->e:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    sput v0, Lcom/dropbox/android/albums/PhotosModel;->y:I

    .line 290
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->w:[Ljava/lang/String;

    sget-object v1, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    sput v0, Lcom/dropbox/android/albums/PhotosModel;->a:I

    .line 292
    sget-object v0, Lcom/dropbox/android/provider/V;->a:[Ljava/lang/String;

    sget-object v1, Lcom/dropbox/android/provider/h;->c:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/aa/a;->c([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->A:[Ljava/lang/String;

    .line 953
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DELETE FROM album_item WHERE NOT EXISTS (SELECT * FROM albums WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/PhotosModel;->B:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/j/i;)V
    .locals 4

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    .line 522
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    .line 524
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    .line 525
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->d:Lcom/dropbox/android/albums/t;

    .line 526
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    .line 527
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->f:Lcom/dropbox/android/albums/t;

    .line 528
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->g:Lcom/dropbox/android/albums/t;

    .line 529
    new-instance v0, Lcom/dropbox/android/albums/t;

    invoke-direct {v0}, Lcom/dropbox/android/albums/t;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->h:Lcom/dropbox/android/albums/t;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    new-instance v1, Lcom/dropbox/android/albums/v;

    invoke-direct {v1, p5}, Lcom/dropbox/android/albums/v;-><init>(Ldbxyzptlk/db231222/j/i;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v1, Lcom/dropbox/android/albums/x;

    invoke-direct {v1, p5}, Lcom/dropbox/android/albums/x;-><init>(Ldbxyzptlk/db231222/j/i;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    new-instance v1, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    const/4 v2, 0x1

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;-><init>(IILjava/util/List;)V

    iput-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    .line 98
    iput-object p3, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    .line 99
    iput-object p2, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    .line 100
    iput-object p4, p0, Lcom/dropbox/android/albums/PhotosModel;->l:Ldbxyzptlk/db231222/n/P;

    .line 101
    new-instance v0, Ldbxyzptlk/db231222/g/R;

    invoke-direct {v0, p0, p4, p3, p2}, Ldbxyzptlk/db231222/g/R;-><init>(Lcom/dropbox/android/albums/PhotosModel;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    iput-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->o:Ldbxyzptlk/db231222/g/R;

    .line 102
    iput-object p1, p0, Lcom/dropbox/android/albums/PhotosModel;->m:Landroid/content/ContentResolver;

    .line 103
    return-void
.end method

.method private a(Z)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(ZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(ZLjava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 213
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 214
    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 215
    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->s:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    invoke-virtual {v4}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    const-string v6, "1"

    :goto_0
    aput-object v6, v4, v7

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/dropbox/android/provider/f;->h:Lcom/dropbox/android/provider/c;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " DESC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v6, v5

    move-object v8, p2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 217
    new-instance v1, Lcom/dropbox/android/provider/L;

    invoke-direct {v1, v0}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;)V

    .line 218
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    return-object v1

    .line 215
    :cond_0
    const-string v6, "0"

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 3

    .prologue
    .line 364
    invoke-static {p0}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 367
    sget v1, Lcom/dropbox/android/albums/PhotosModel;->y:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/LocalEntry;->b(J)V

    .line 369
    return-object v0
.end method

.method private a(Lcom/dropbox/android/albums/DeleteItemsTask;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 728
    if-eqz p2, :cond_0

    .line 729
    new-instance v0, Lcom/dropbox/android/albums/m;

    invoke-direct {v0, p0}, Lcom/dropbox/android/albums/m;-><init>(Lcom/dropbox/android/albums/PhotosModel;)V

    invoke-virtual {p1, v0}, Lcom/dropbox/android/albums/DeleteItemsTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 741
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/DeleteItemsTask;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 742
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/DeleteItemsTask;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 744
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 745
    invoke-virtual {p1}, Lcom/dropbox/android/albums/DeleteItemsTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ZZLjava/lang/String;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/albums/u;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 555
    new-instance v0, Lcom/dropbox/android/albums/PhotosModel$CreateTask;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v6, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    iget-object v7, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;-><init>(ZZLjava/lang/String;Ljava/util/List;Lcom/dropbox/android/albums/PhotosModel;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/provider/j;)V

    .line 557
    new-instance v1, Lcom/dropbox/android/albums/l;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/albums/l;-><init>(Lcom/dropbox/android/albums/PhotosModel;Z)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 570
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 571
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p5}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 572
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 573
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(ILjava/util/List;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 399
    const/4 v0, 0x0

    .line 400
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 401
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 402
    add-int v1, v0, p0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 403
    invoke-interface {p1, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 405
    goto :goto_0

    .line 406
    :cond_0
    return-object v2
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 948
    const-string v0, "album_item"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 949
    const-string v0, "albums"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 950
    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 922
    new-instance v2, Landroid/content/ContentValues;

    const/16 v0, 0x9

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 923
    sget-object v0, Lcom/dropbox/android/provider/f;->c:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/z/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    sget-object v0, Lcom/dropbox/android/provider/f;->d:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-wide v3, p1, Ldbxyzptlk/db231222/z/p;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 925
    sget-object v0, Lcom/dropbox/android/provider/f;->e:Lcom/dropbox/android/provider/c;

    iget-object v3, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/n;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    sget-object v0, Lcom/dropbox/android/provider/f;->f:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/z/p;->f:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    sget-object v0, Lcom/dropbox/android/provider/f;->g:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/z/p;->e:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 928
    sget-object v0, Lcom/dropbox/android/provider/f;->h:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/z/p;->d:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 929
    sget-object v0, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    sget-object v0, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    iget-object v3, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 932
    const-string v0, "albums"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 933
    return-void

    :cond_0
    move-object v0, v1

    .line 925
    goto :goto_0

    .line 930
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/l;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 889
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upserting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " collection items."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    new-instance v1, Landroid/content/ContentValues;

    const/4 v0, 0x5

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 891
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/l;

    .line 892
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 893
    sget-object v3, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    sget-object v3, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v4, v0, Ldbxyzptlk/db231222/z/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    sget-object v3, Lcom/dropbox/android/provider/e;->e:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v4, v0, Ldbxyzptlk/db231222/z/l;->c:Ldbxyzptlk/db231222/z/n;

    iget-object v4, v4, Ldbxyzptlk/db231222/z/n;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    sget-object v3, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/l;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    const-string v0, "album_item"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 899
    :cond_0
    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 936
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 937
    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Deleting: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    new-array v2, v6, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 939
    const-string v0, "albums"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    iget-object v4, v4, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 940
    if-eq v0, v6, :cond_0

    .line 941
    sget-object v3, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Instead of 1 album, deleted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    :cond_0
    const-string v0, "album_item"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    iget-object v4, v4, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 945
    :cond_1
    return-void
.end method

.method static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 977
    if-eqz p2, :cond_0

    .line 978
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 982
    :goto_0
    return-void

    .line 980
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 296
    if-eqz p0, :cond_0

    const-string v0, " LEFT OUTER JOIN "

    .line 297
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "album_item"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dropbox"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ON ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/e;->e:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dropbox"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "canon_path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photos"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/h;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "dropbox"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "canon_path"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 296
    :cond_0
    const-string v0, " JOIN "

    goto/16 :goto_0
.end method

.method static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 957
    const-string v0, "albums"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 958
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 959
    return-void
.end method

.method static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 902
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 903
    const/4 v1, 0x0

    .line 905
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DELETE FROM album_item WHERE "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = ? AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = ?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 907
    const/4 v0, 0x1

    invoke-virtual {v1, v0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 908
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 909
    const/4 v3, 0x2

    invoke-virtual {v1, v3, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 910
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 914
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 915
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_0
    throw v0

    .line 912
    :cond_1
    :try_start_1
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 914
    if-eqz v1, :cond_2

    .line 915
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 919
    :cond_2
    return-void
.end method

.method static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 962
    const-string v0, "albums"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 963
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 964
    return-void
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 413
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 450
    :goto_0
    return v9

    .line 417
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 418
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 419
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 431
    :cond_1
    const/16 v0, 0x3e7

    invoke-static {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->a(ILjava/util/List;)Ljava/util/Collection;

    move-result-object v1

    .line 434
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 435
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v8, v9

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/util/List;

    .line 437
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/e;->e:Lcom/dropbox/android/provider/c;

    iget-object v2, v2, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    const-string v3, ", "

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v2, v3, v6}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 438
    const-string v1, "album_item"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "count(DISTINCT "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/dropbox/android/provider/e;->e:Lcom/dropbox/android/provider/c;

    iget-object v7, v7, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v9

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 446
    invoke-interface {v2, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 447
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    add-int/2addr v1, v8

    .line 448
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move v8, v1

    .line 449
    goto :goto_2

    :cond_2
    move v9, v8

    .line 450
    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 312
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 313
    invoke-static {p2}, Lcom/dropbox/android/albums/PhotosModel;->b(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 314
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->w:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v4}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    sget-object v7, Lcom/dropbox/android/albums/PhotosModel;->u:Ljava/lang/String;

    move-object v6, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 318
    new-instance v1, Lcom/dropbox/android/provider/W;

    const-string v2, "DropboxEntry"

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 319
    new-instance v2, Lcom/dropbox/android/provider/L;

    invoke-direct {v2, v1}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;)V

    .line 322
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    monitor-enter v1

    .line 323
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 324
    if-nez v0, :cond_0

    .line 325
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 326
    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    return-object v2

    .line 328
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/List;)Landroid/database/Cursor;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 373
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 374
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 375
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 376
    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    const/4 v6, 0x0

    invoke-direct {v3, v1, v6}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 378
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photos JOIN dropbox ON ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/h;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dropbox"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "canon_path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 383
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/h;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    const-string v3, ", "

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v2, v3, v6}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 384
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->A:[Ljava/lang/String;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    sget-object v7, Lcom/dropbox/android/albums/PhotosModel;->v:Ljava/lang/String;

    move-object v6, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 393
    new-instance v0, Lcom/dropbox/android/provider/W;

    const-string v2, "_temp_album_item"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 395
    :goto_1
    return-object v0

    :cond_1
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/dropbox/android/albums/PhotosModel;->A:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lcom/dropbox/android/albums/Album;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 224
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 226
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 227
    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 230
    :try_start_0
    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->s:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 231
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    .line 237
    :cond_0
    if-eqz v1, :cond_1

    .line 238
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v9

    :cond_2
    :goto_0
    return-object v0

    .line 234
    :cond_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_4

    move v0, v10

    :goto_1
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 235
    invoke-static {v1}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 237
    if-eqz v1, :cond_2

    .line 238
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    move v0, v11

    .line 234
    goto :goto_1

    .line 237
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v9, :cond_5

    .line 238
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 237
    :catchall_1
    move-exception v0

    move-object v9, v1

    goto :goto_2
.end method

.method public final a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 779
    new-instance v0, Lcom/dropbox/android/albums/DeleteAlbumTask;

    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/dropbox/android/albums/DeleteAlbumTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;)V

    .line 780
    new-instance v1, Lcom/dropbox/android/albums/p;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->g:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/dropbox/android/albums/p;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/DeleteAlbumTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 782
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->g:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 783
    if-eqz p2, :cond_0

    .line 784
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->g:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 786
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 787
    invoke-virtual {v0}, Lcom/dropbox/android/albums/DeleteAlbumTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/albums/Album;Ljava/lang/String;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 582
    invoke-static {p2}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Name must be specified and non-empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585
    :cond_0
    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t rename a lightweight album."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 588
    :cond_1
    new-instance v0, Lcom/dropbox/android/albums/RenameAlbumTask;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/RenameAlbumTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;Ljava/lang/String;)V

    .line 589
    new-instance v1, Lcom/dropbox/android/albums/p;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->h:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/dropbox/android/albums/p;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/RenameAlbumTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 591
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->h:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/RenameAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 592
    if-eqz p3, :cond_2

    .line 593
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->h:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/RenameAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 595
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 596
    invoke-virtual {v0}, Lcom/dropbox/android/albums/RenameAlbumTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/albums/u;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 704
    new-instance v0, Lcom/dropbox/android/albums/AddTask;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/AddTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    .line 705
    new-instance v1, Lcom/dropbox/android/albums/p;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/dropbox/android/albums/p;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/AddTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 707
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/AddTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 708
    if-eqz p3, :cond_0

    .line 709
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/AddTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 711
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 712
    invoke-virtual {v0}, Lcom/dropbox/android/albums/AddTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/taskqueue/D;",
            "Ldbxyzptlk/db231222/k/h;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/albums/u;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 750
    new-instance v0, Lcom/dropbox/android/albums/DeleteItemsTask;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/albums/DeleteItemsTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Ljava/util/Collection;)V

    invoke-direct {p0, v0, p4}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/DeleteItemsTask;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/albums/u;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 537
    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/PhotosModel;->a(ZZLjava/lang/String;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;",
            "Lcom/dropbox/android/albums/u;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 541
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 542
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 543
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 546
    :cond_0
    const-string v3, ""

    move-object v0, p0

    move v2, v1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/PhotosModel;->a(ZZLjava/lang/String;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a()V

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 110
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 112
    :try_start_0
    invoke-static {v1}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 113
    const-string v0, "photos"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 114
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 119
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 120
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    monitor-enter v1

    .line 121
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 122
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 124
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/t;->a()V

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/t;->a()V

    .line 126
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/R;->a()V

    .line 127
    return-void

    .line 116
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 122
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/q;)V
    .locals 4

    .prologue
    .line 143
    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v1

    .line 145
    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    monitor-enter v2

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 147
    if-nez v0, :cond_0

    .line 148
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 149
    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    return-void

    .line 151
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/s;)V
    .locals 3

    .prologue
    .line 759
    new-instance v0, Lcom/dropbox/android/albums/GenerateShareLinkTask;

    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/dropbox/android/albums/GenerateShareLinkTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;)V

    .line 760
    if-eqz p2, :cond_0

    .line 761
    new-instance v1, Lcom/dropbox/android/albums/n;

    invoke-direct {v1, p0, p2}, Lcom/dropbox/android/albums/n;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/s;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 775
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 776
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 876
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/provider/PhotosProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    const-string v1, "Path exists in All Photos, notifying."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel;->i()V

    .line 880
    :cond_0
    return-void
.end method

.method public final varargs a([Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 967
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 968
    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 970
    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/util/Collection;)I

    move-result v0

    if-lez v0, :cond_0

    .line 971
    sget-object v0, Lcom/dropbox/android/albums/PhotosModel;->i:Ljava/lang/String;

    const-string v1, "notifyAllOpenAlbumsIfPathInAlbum calling notifyAllOpenAlbums"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    invoke-virtual {p0}, Lcom/dropbox/android/albums/PhotosModel;->g()V

    .line 974
    :cond_0
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/g/R;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->o:Ldbxyzptlk/db231222/g/R;

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 791
    new-instance v0, Lcom/dropbox/android/albums/UnshareAlbumTask;

    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/dropbox/android/albums/UnshareAlbumTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;)V

    .line 792
    new-instance v1, Lcom/dropbox/android/albums/p;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->f:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/dropbox/android/albums/p;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 794
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->f:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 795
    if-eqz p2, :cond_0

    .line 796
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->f:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 798
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 799
    invoke-virtual {v0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/albums/AlbumItemEntry;",
            ">;",
            "Lcom/dropbox/android/albums/u;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 716
    new-instance v0, Lcom/dropbox/android/albums/RemoveTask;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v3, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/RemoveTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    .line 717
    new-instance v1, Lcom/dropbox/android/albums/p;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->d:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/dropbox/android/albums/p;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/RemoveTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 719
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->d:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/RemoveTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 720
    if-eqz p3, :cond_0

    .line 721
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->d:Lcom/dropbox/android/albums/t;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/RemoveTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 723
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 724
    invoke-virtual {v0}, Lcom/dropbox/android/albums/RemoveTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/q;)V
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 160
    if-nez v0, :cond_0

    .line 161
    monitor-exit v1

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    invoke-virtual {v0, p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 338
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 339
    invoke-static {v9}, Lcom/dropbox/android/albums/PhotosModel;->b(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 341
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ? AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dropbox"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "canon_path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IS NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 343
    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v10

    .line 344
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/albums/PhotosModel;->w:[Ljava/lang/String;

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 348
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    move v0, v9

    .line 350
    :goto_0
    if-eqz v1, :cond_0

    .line 351
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    move v0, v10

    .line 348
    goto :goto_0

    .line 350
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 351
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public final c()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 803
    .line 804
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    monitor-enter v1

    .line 805
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 806
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    if-eqz v0, :cond_0

    .line 808
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/q;

    .line 809
    invoke-interface {v0}, Lcom/dropbox/android/albums/q;->a()V

    goto :goto_0

    .line 806
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 814
    if-eqz v0, :cond_3

    .line 815
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 816
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 817
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dropbox/android/provider/L;

    .line 818
    if-nez v2, :cond_1

    .line 819
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 821
    :cond_1
    invoke-virtual {v2}, Lcom/dropbox/android/provider/L;->a()V

    goto :goto_1

    .line 824
    :cond_2
    invoke-virtual {v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 826
    :cond_3
    return-void
.end method

.method public final d()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 192
    const/4 v1, 0x0

    .line 194
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "1"

    invoke-direct {p0, v2, v3}, Lcom/dropbox/android/albums/PhotosModel;->a(ZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 195
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 197
    :cond_0
    if-eqz v1, :cond_1

    .line 198
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return v0

    .line 197
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 198
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public final e()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->l:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->G()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 836
    iget-object v1, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    monitor-enter v1

    .line 837
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->q:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 838
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 839
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 840
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 841
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/L;

    .line 842
    if-eqz v0, :cond_1

    .line 843
    invoke-virtual {v0}, Lcom/dropbox/android/provider/L;->a()V

    goto :goto_0

    .line 838
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 847
    :cond_2
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 850
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 851
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 852
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/provider/L;

    .line 853
    if-nez v1, :cond_0

    .line 854
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 856
    :cond_0
    invoke-virtual {v1}, Lcom/dropbox/android/provider/L;->a()V

    goto :goto_0

    .line 859
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 860
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 867
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosModel;->m:Landroid/content/ContentResolver;

    sget-object v1, Lcom/dropbox/android/provider/PhotosProvider;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 868
    return-void
.end method

.method public final j()V
    .locals 7

    .prologue
    .line 883
    iget-object v6, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    new-instance v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->l:Ldbxyzptlk/db231222/n/P;

    iget-object v4, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v5, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/AlbumsUpdateTask;-><init>(ZLdbxyzptlk/db231222/n/P;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v6, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 884
    iget-object v6, p0, Lcom/dropbox/android/albums/PhotosModel;->n:Lcom/dropbox/android/taskqueue/TaskQueue;

    new-instance v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dropbox/android/albums/PhotosModel;->l:Ldbxyzptlk/db231222/n/P;

    iget-object v4, p0, Lcom/dropbox/android/albums/PhotosModel;->j:Lcom/dropbox/android/provider/j;

    iget-object v5, p0, Lcom/dropbox/android/albums/PhotosModel;->k:Ldbxyzptlk/db231222/z/M;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/AlbumsUpdateTask;-><init>(ZLdbxyzptlk/db231222/n/P;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v6, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 885
    return-void
.end method
