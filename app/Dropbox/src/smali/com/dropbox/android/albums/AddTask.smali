.class Lcom/dropbox/android/albums/AddTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/albums/Album;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/PhotosModel;",
            "Lcom/dropbox/android/provider/j;",
            "Ldbxyzptlk/db231222/z/M;",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 27
    iput-object p4, p0, Lcom/dropbox/android/albums/AddTask;->a:Lcom/dropbox/android/albums/Album;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/dropbox/android/albums/AddTask;->b:Ljava/util/ArrayList;

    .line 29
    const-string v0, "\\"

    iget-object v1, p0, Lcom/dropbox/android/albums/AddTask;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AddTask;->c:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/AddTask;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/AddTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    new-instance v1, Ldbxyzptlk/db231222/j/l;

    iget-object v2, p0, Lcom/dropbox/android/albums/AddTask;->a:Lcom/dropbox/android/albums/Album;

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/albums/Album;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 4

    .prologue
    .line 52
    iget v0, p0, Lcom/dropbox/android/albums/AddTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/AddTask;->d:I

    .line 55
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AddTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/AddTask;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/albums/AddTask;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;Ljava/util/List;)Ldbxyzptlk/db231222/z/h;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AddTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :try_start_1
    iget-object v2, v0, Ldbxyzptlk/db231222/z/h;->a:Ldbxyzptlk/db231222/z/p;

    iget-object v3, p0, Lcom/dropbox/android/albums/AddTask;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V

    .line 60
    iget-object v2, v0, Ldbxyzptlk/db231222/z/h;->a:Ldbxyzptlk/db231222/z/p;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/h;->b:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    .line 61
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AddTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->h()V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_0

    .line 73
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AddTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 75
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AddTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_0
    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 66
    :catch_0
    move-exception v0

    .line 68
    const-string v1, "collectionItemsAdd"

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/AddTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0
.end method

.method final d()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/dropbox/android/albums/AddTask;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AddTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
