.class final Lcom/dropbox/android/albums/x;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/q;


# instance fields
.field private final a:Ldbxyzptlk/db231222/j/i;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/j/i;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/dropbox/android/albums/x;->a:Ldbxyzptlk/db231222/j/i;

    .line 20
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/albums/x;)Ldbxyzptlk/db231222/j/i;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/dropbox/android/albums/x;->a:Ldbxyzptlk/db231222/j/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 5

    .prologue
    .line 28
    instance-of v0, p1, Lcom/dropbox/android/albums/DeleteItemsTask;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/dropbox/android/albums/RemoveTask;

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 32
    :cond_0
    instance-of v0, p1, Lcom/dropbox/android/albums/DeleteItemsTask;

    if-eqz v0, :cond_1

    new-instance v0, Ldbxyzptlk/db231222/j/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/j/b;-><init>()V

    move-object v1, v0

    .line 34
    :goto_1
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->b()Ljava/util/List;

    move-result-object v0

    .line 35
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 37
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 38
    iget-object v4, p0, Lcom/dropbox/android/albums/x;->a:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v4, v0, v1}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    .line 39
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 41
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 42
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 43
    iget-object v4, p0, Lcom/dropbox/android/albums/x;->a:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v4, v0, v1}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/g;)V

    goto :goto_3

    .line 32
    :cond_1
    new-instance v0, Lcom/dropbox/android/albums/z;

    invoke-direct {v0}, Lcom/dropbox/android/albums/z;-><init>()V

    move-object v1, v0

    goto :goto_1

    .line 45
    :cond_2
    throw v2

    .line 48
    :cond_3
    new-instance v0, Lcom/dropbox/android/albums/y;

    invoke-direct {v0, p0, v1, v3}, Lcom/dropbox/android/albums/y;-><init>(Lcom/dropbox/android/albums/x;Ldbxyzptlk/db231222/j/g;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Lcom/dropbox/android/taskqueue/u;->a(Lcom/dropbox/android/taskqueue/v;)V

    goto :goto_0
.end method
