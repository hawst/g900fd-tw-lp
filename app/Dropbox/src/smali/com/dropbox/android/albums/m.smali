.class final Lcom/dropbox/android/albums/m;
.super Lcom/dropbox/android/albums/r;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/r",
        "<",
        "Lcom/dropbox/android/albums/DeleteItemsTask;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;


# direct methods
.method constructor <init>(Lcom/dropbox/android/albums/PhotosModel;)V
    .locals 1

    .prologue
    .line 729
    iput-object p1, p0, Lcom/dropbox/android/albums/m;->a:Lcom/dropbox/android/albums/PhotosModel;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/albums/r;-><init>(Lcom/dropbox/android/albums/l;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/albums/DeleteItemsTask;)V
    .locals 4

    .prologue
    .line 733
    iget-object v0, p0, Lcom/dropbox/android/albums/m;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v0, v0, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/DeleteItemsTask;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 734
    return-void
.end method

.method public final a(Lcom/dropbox/android/albums/DeleteItemsTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 3

    .prologue
    .line 738
    iget-object v0, p0, Lcom/dropbox/android/albums/m;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v0, v0, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/DeleteItemsTask;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 739
    return-void
.end method

.method public final bridge synthetic a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 729
    check-cast p1, Lcom/dropbox/android/albums/DeleteItemsTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/albums/m;->a(Lcom/dropbox/android/albums/DeleteItemsTask;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 729
    check-cast p1, Lcom/dropbox/android/albums/DeleteItemsTask;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/albums/m;->a(Lcom/dropbox/android/albums/DeleteItemsTask;Lcom/dropbox/android/taskqueue/w;)V

    return-void
.end method
