.class public abstract Lcom/dropbox/android/albums/o;
.super Lcom/dropbox/android/activity/fB;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/dropbox/android/activity/fB",
        "<TT;",
        "Lcom/dropbox/android/albums/Album;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/support/v4/app/Fragment;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1021
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/activity/fB;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    .line 1022
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/albums/t",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/support/v4/app/FragmentActivity;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1026
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/activity/fB;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;I)V

    .line 1027
    return-void
.end method


# virtual methods
.method protected a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1037
    return-void
.end method

.method protected final b(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1031
    invoke-virtual {p1}, Lcom/dropbox/android/activity/fF;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    invoke-virtual {p0, v0, p2}, Lcom/dropbox/android/albums/o;->a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V

    .line 1032
    return-void
.end method
