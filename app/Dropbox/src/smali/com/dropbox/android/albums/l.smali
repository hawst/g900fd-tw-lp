.class final Lcom/dropbox/android/albums/l;
.super Lcom/dropbox/android/albums/r;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/r",
        "<",
        "Lcom/dropbox/android/albums/PhotosModel$CreateTask;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/dropbox/android/albums/PhotosModel;


# direct methods
.method constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Z)V
    .locals 1

    .prologue
    .line 557
    iput-object p1, p0, Lcom/dropbox/android/albums/l;->b:Lcom/dropbox/android/albums/PhotosModel;

    iput-boolean p2, p0, Lcom/dropbox/android/albums/l;->a:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/albums/r;-><init>(Lcom/dropbox/android/albums/l;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/albums/PhotosModel$CreateTask;)V
    .locals 4

    .prologue
    .line 561
    new-instance v0, Lcom/dropbox/android/albums/Album;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->d()Ldbxyzptlk/db231222/z/p;

    move-result-object v1

    iget-boolean v2, p0, Lcom/dropbox/android/albums/l;->a:Z

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/albums/Album;-><init>(Ldbxyzptlk/db231222/z/p;Z)V

    .line 562
    iget-object v1, p0, Lcom/dropbox/android/albums/l;->b:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, v1, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v1, v2, v3, v0}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 563
    return-void
.end method

.method public final a(Lcom/dropbox/android/albums/PhotosModel$CreateTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 3

    .prologue
    .line 567
    iget-object v0, p0, Lcom/dropbox/android/albums/l;->b:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v0, v0, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/PhotosModel$CreateTask;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/w;Ljava/lang/Object;)V

    .line 568
    return-void
.end method

.method public final bridge synthetic a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 557
    check-cast p1, Lcom/dropbox/android/albums/PhotosModel$CreateTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/albums/l;->a(Lcom/dropbox/android/albums/PhotosModel$CreateTask;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 557
    check-cast p1, Lcom/dropbox/android/albums/PhotosModel$CreateTask;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/albums/l;->a(Lcom/dropbox/android/albums/PhotosModel$CreateTask;Lcom/dropbox/android/taskqueue/w;)V

    return-void
.end method
