.class public Lcom/dropbox/android/albums/Album;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Lcom/dropbox/android/util/DropboxPath;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/dropbox/android/albums/b;

    invoke-direct {v0}, Lcom/dropbox/android/albums/b;-><init>()V

    sput-object v0, Lcom/dropbox/android/albums/Album;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-object v0, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    .line 28
    sget-object v0, Lcom/dropbox/android/provider/f;->c:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->a:Ljava/lang/String;

    .line 29
    sget-object v0, Lcom/dropbox/android/provider/f;->d:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/dropbox/android/albums/Album;->b:J

    .line 31
    const-string v0, "path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 32
    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    .line 33
    const-string v0, "revision"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    .line 34
    sget-object v0, Lcom/dropbox/android/provider/f;->f:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->f:Ljava/lang/String;

    .line 35
    sget-object v0, Lcom/dropbox/android/provider/f;->i:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/dropbox/android/albums/Album;->g:Z

    .line 36
    invoke-direct {p0}, Lcom/dropbox/android/albums/Album;->j()V

    .line 37
    return-void

    .line 32
    :cond_0
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, v2, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 35
    goto :goto_1
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->a:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/albums/Album;->b:J

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    .line 146
    const-class v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->f:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/albums/Album;->g:Z

    .line 150
    invoke-direct {p0}, Lcom/dropbox/android/albums/Album;->j()V

    .line 151
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/z/p;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->a:Ljava/lang/String;

    .line 42
    iget-wide v0, p1, Ldbxyzptlk/db231222/z/p;->c:J

    iput-wide v0, p0, Lcom/dropbox/android/albums/Album;->b:J

    .line 43
    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/n;->a:Lcom/dropbox/android/util/DropboxPath;

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    .line 45
    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/n;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    .line 51
    :goto_0
    iget-object v0, p1, Ldbxyzptlk/db231222/z/p;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/albums/Album;->f:Ljava/lang/String;

    .line 52
    iput-boolean p2, p0, Lcom/dropbox/android/albums/Album;->g:Z

    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/albums/Album;->j()V

    .line 54
    return-void

    .line 47
    :cond_0
    iput-object v2, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    .line 48
    iput-object v2, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/dropbox/android/albums/Album;

    invoke-direct {v0, p0}, Lcom/dropbox/android/albums/Album;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    if-eq v0, v1, :cond_2

    .line 59
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Both or neither of path and rev should be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 58
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 61
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/dropbox/android/albums/Album;->b:J

    return-wide v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 66
    instance-of v0, p1, Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_0

    .line 67
    check-cast p1, Lcom/dropbox/android/albums/Album;

    iget-object v0, p1, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/dropbox/android/albums/Album;->g:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-wide v0, p0, Lcom/dropbox/android/albums/Album;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 135
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->d:Lcom/dropbox/android/util/DropboxPath;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 137
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/albums/Album;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    iget-boolean v0, p0, Lcom/dropbox/android/albums/Album;->g:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 140
    return-void

    .line 139
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
