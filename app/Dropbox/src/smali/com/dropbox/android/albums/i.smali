.class public final Lcom/dropbox/android/albums/i;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static a(Landroid/content/res/Resources;Lcom/dropbox/android/albums/k;II)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 21
    if-ltz p2, :cond_0

    if-gez p3, :cond_1

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Doesn\'t support <0 items"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    add-int v0, p2, p3

    if-nez v0, :cond_2

    .line 25
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Doesn\'t support 0 items"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_2
    if-nez p3, :cond_3

    .line 31
    sget-object v0, Lcom/dropbox/android/albums/j;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/albums/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 33
    :pswitch_0
    const v0, 0x7f0f0008

    .line 41
    :goto_0
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_1
    return-object v0

    .line 36
    :pswitch_1
    const v0, 0x7f0f000b

    .line 37
    goto :goto_0

    .line 42
    :cond_3
    if-nez p2, :cond_4

    .line 45
    sget-object v0, Lcom/dropbox/android/albums/j;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/albums/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 47
    :pswitch_2
    const v0, 0x7f0f0009

    .line 55
    :goto_2
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, p3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 50
    :pswitch_3
    const v0, 0x7f0f000c

    .line 51
    goto :goto_2

    .line 59
    :cond_4
    sget-object v0, Lcom/dropbox/android/albums/j;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/albums/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 61
    :pswitch_4
    const v0, 0x7f0f000a

    .line 69
    :goto_3
    add-int v1, p2, p3

    .line 70
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 64
    :pswitch_5
    const v0, 0x7f0f000d

    .line 65
    goto :goto_3

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 45
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 59
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
