.class public Lcom/dropbox/android/albums/UnshareAlbumTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/dropbox/android/albums/UnshareAlbumTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/UnshareAlbumTask;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 26
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    sget-object v0, Lcom/dropbox/android/albums/UnshareAlbumTask;->a:Ljava/lang/String;

    const-string v1, "Attempting to unshare album that isn\'t shared."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    :cond_0
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->b:Ljava/lang/String;

    .line 30
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->c:Z

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStatusPath() doesn\'t make sense for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 3

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->d:I

    .line 52
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->e(Ljava/lang/String;)Ldbxyzptlk/db231222/z/p;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :try_start_1
    iget-boolean v2, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->c:Z

    invoke-static {v1, v0, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V

    .line 58
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_0

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/dropbox/android/albums/UnshareAlbumTask;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->c(Ljava/lang/String;)V

    .line 75
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->h()V

    .line 76
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 78
    invoke-virtual {p0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_0
    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    sget-object v1, Lcom/dropbox/android/albums/UnshareAlbumTask;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    instance-of v1, v0, Ldbxyzptlk/db231222/w/i;

    if-eqz v1, :cond_1

    .line 66
    check-cast v0, Ldbxyzptlk/db231222/w/i;

    iget v0, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x194

    if-ne v0, v1, :cond_1

    .line 67
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_1
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/dropbox/android/albums/UnshareAlbumTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
