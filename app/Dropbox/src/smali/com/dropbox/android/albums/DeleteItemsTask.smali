.class Lcom/dropbox/android/albums/DeleteItemsTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Lcom/dropbox/android/taskqueue/D;

.field private final e:Ldbxyzptlk/db231222/k/h;


# direct methods
.method constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/PhotosModel;",
            "Lcom/dropbox/android/provider/j;",
            "Ldbxyzptlk/db231222/z/M;",
            "Lcom/dropbox/android/taskqueue/D;",
            "Ldbxyzptlk/db231222/k/h;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->a:Ljava/util/ArrayList;

    .line 34
    const-string v0, "\\"

    iget-object v1, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->b:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->c:Lcom/dropbox/android/taskqueue/D;

    .line 37
    iput-object p5, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->e:Ldbxyzptlk/db231222/k/h;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    iget-object v0, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 54
    new-instance v3, Ldbxyzptlk/db231222/j/l;

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_0
    return-object v1
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 4

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->d:I

    .line 64
    :try_start_0
    invoke-static {}, Ldbxyzptlk/db231222/z/M;->c()V

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteItemsTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;

    .line 74
    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v3, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    const-string v1, "batchDelete"

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/DeleteItemsTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    .line 80
    :goto_1
    return-object v0

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteItemsTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->c:Lcom/dropbox/android/taskqueue/D;

    iget-object v3, p0, Lcom/dropbox/android/albums/DeleteItemsTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/filemanager/A;->a(Lcom/dropbox/android/provider/j;Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Ljava/util/Collection;)V

    .line 78
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteItemsTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->i()V

    .line 79
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteItemsTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteItemsTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/dropbox/android/albums/DeleteItemsTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
