.class public abstract Lcom/dropbox/android/albums/PhotosTask;
.super Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/z/M;

.field private final b:Lcom/dropbox/android/provider/j;

.field private final c:Lcom/dropbox/android/albums/PhotosModel;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/dropbox/android/albums/PhotosTask;->c:Lcom/dropbox/android/albums/PhotosModel;

    .line 14
    iput-object p2, p0, Lcom/dropbox/android/albums/PhotosTask;->b:Lcom/dropbox/android/provider/j;

    .line 15
    iput-object p3, p0, Lcom/dropbox/android/albums/PhotosTask;->a:Ldbxyzptlk/db231222/z/M;

    .line 16
    return-void
.end method


# virtual methods
.method protected final e()Ldbxyzptlk/db231222/z/M;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosTask;->a:Ldbxyzptlk/db231222/z/M;

    return-object v0
.end method

.method protected final g()Lcom/dropbox/android/albums/PhotosModel;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosTask;->c:Lcom/dropbox/android/albums/PhotosModel;

    return-object v0
.end method

.method protected final g_()Lcom/dropbox/android/provider/j;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dropbox/android/albums/PhotosTask;->b:Lcom/dropbox/android/provider/j;

    return-object v0
.end method
