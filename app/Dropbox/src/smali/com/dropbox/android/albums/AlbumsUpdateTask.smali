.class public Lcom/dropbox/android/albums/AlbumsUpdateTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/android/albums/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/albums/d;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p2, p3, p4}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 44
    iput-object p1, p0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    .line 45
    return-void
.end method

.method public constructor <init>(ZLdbxyzptlk/db231222/n/P;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V
    .locals 1

    .prologue
    .line 53
    if-eqz p1, :cond_0

    new-instance v0, Lcom/dropbox/android/albums/f;

    invoke-direct {v0, p2}, Lcom/dropbox/android/albums/f;-><init>(Ldbxyzptlk/db231222/n/P;)V

    :goto_0
    invoke-direct {p0, v0, p3, p4, p5}, Lcom/dropbox/android/albums/AlbumsUpdateTask;-><init>(Lcom/dropbox/android/albums/d;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 57
    return-void

    .line 53
    :cond_0
    new-instance v0, Lcom/dropbox/android/albums/e;

    invoke-direct {v0, p2}, Lcom/dropbox/android/albums/e;-><init>(Ldbxyzptlk/db231222/n/P;)V

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/j;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 213
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 214
    iget-object v0, p1, Ldbxyzptlk/db231222/z/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/d;

    .line 215
    iget-object v4, v0, Ldbxyzptlk/db231222/v/d;->b:Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 216
    iget-object v0, v0, Ldbxyzptlk/db231222/v/d;->b:Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 218
    :cond_0
    iget-object v0, v0, Ldbxyzptlk/db231222/v/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p1, Ldbxyzptlk/db231222/z/j;->a:Ldbxyzptlk/db231222/z/p;

    if-eqz v0, :cond_3

    .line 223
    iget-object v0, p1, Ldbxyzptlk/db231222/z/j;->a:Ldbxyzptlk/db231222/z/p;

    .line 224
    iget-object v3, v0, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 225
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "id mismatch."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_2
    invoke-static {p0, v0, p3}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V

    .line 230
    :cond_3
    invoke-static {p0, p2, v1}, Lcom/dropbox/android/albums/PhotosModel;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    .line 232
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 233
    invoke-static {p0, p2, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    .line 235
    :cond_4
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/d",
            "<",
            "Ldbxyzptlk/db231222/z/j;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/d;

    .line 205
    iget-object v1, v0, Ldbxyzptlk/db231222/v/d;->b:Ljava/lang/Object;

    check-cast v1, Ldbxyzptlk/db231222/z/j;

    .line 206
    iget-object v0, v0, Ldbxyzptlk/db231222/v/d;->a:Ljava/lang/String;

    .line 207
    invoke-static {p0, v1, v0, p2}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/j;Ljava/lang/String;Z)V

    goto :goto_0

    .line 209
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v0}, Lcom/dropbox/android/albums/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "-lightweight"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    .line 78
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStatusPath() doesn\'t make sense for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 17

    .prologue
    .line 82
    invoke-super/range {p0 .. p0}, Lcom/dropbox/android/albums/PhotosTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 85
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1}, Lcom/dropbox/android/albums/d;->c()Ljava/lang/String;

    move-result-object v5

    .line 86
    const/4 v4, 0x0

    .line 87
    const/4 v3, 0x0

    .line 88
    const/4 v2, 0x0

    .line 90
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1}, Lcom/dropbox/android/albums/d;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ldbxyzptlk/db231222/z/y;->b:Ldbxyzptlk/db231222/z/y;

    move-object v7, v1

    .line 93
    :goto_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v8

    .line 94
    const/4 v1, 0x1

    move-object v6, v5

    move/from16 v16, v3

    move v3, v4

    move v4, v2

    move/from16 v2, v16

    .line 95
    :goto_1
    if-eqz v1, :cond_12

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    .line 199
    :goto_2
    return-object v1

    .line 90
    :cond_0
    sget-object v1, Ldbxyzptlk/db231222/z/y;->a:Ldbxyzptlk/db231222/z/y;

    move-object v7, v1

    goto :goto_0

    .line 99
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    invoke-virtual {v1, v7, v6}, Ldbxyzptlk/db231222/z/M;->a(Ldbxyzptlk/db231222/z/y;Ljava/lang/String;)Ldbxyzptlk/db231222/v/f;

    move-result-object v11

    .line 101
    add-int/lit8 v5, v4, 0x1

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    goto :goto_2

    .line 106
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    .line 107
    sget-object v1, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Request time: "

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v9, v12, v9

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 110
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 111
    iget-object v1, v11, Ldbxyzptlk/db231222/v/f;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/v/d;

    .line 112
    iget-object v14, v1, Ldbxyzptlk/db231222/v/d;->b:Ljava/lang/Object;

    if-eqz v14, :cond_3

    .line 113
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v1, v3, 0x1

    move/from16 v16, v2

    move v2, v1

    move/from16 v1, v16

    :goto_4
    move v3, v2

    move v2, v1

    .line 119
    goto :goto_3

    .line 116
    :cond_3
    iget-object v1, v1, Ldbxyzptlk/db231222/v/d;->a:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    add-int/lit8 v1, v2, 0x1

    move v2, v3

    goto :goto_4

    .line 120
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 121
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :try_start_1
    iget-boolean v1, v11, Ldbxyzptlk/db231222/v/f;->a:Z

    if-nez v1, :cond_5

    if-nez v6, :cond_8

    :cond_5
    const/4 v1, 0x1

    .line 128
    :goto_5
    if-eqz v1, :cond_9

    .line 129
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1, v4}, Lcom/dropbox/android/albums/d;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 133
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1}, Lcom/dropbox/android/albums/d;->a()Z

    move-result v1

    invoke-static {v4, v10, v1}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Z)V

    .line 134
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->p()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 139
    :try_start_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 181
    :catch_0
    move-exception v2

    .line 182
    instance-of v1, v2, Ldbxyzptlk/db231222/w/i;

    if-eqz v1, :cond_6

    move-object v1, v2

    .line 188
    check-cast v1, Ldbxyzptlk/db231222/w/i;

    iget v1, v1, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v3, 0x190

    if-ne v1, v3, :cond_6

    .line 189
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1}, Lcom/dropbox/android/albums/d;->b()V

    .line 192
    :cond_6
    instance-of v1, v2, Ldbxyzptlk/db231222/w/d;

    if-nez v1, :cond_7

    instance-of v1, v2, Ldbxyzptlk/db231222/w/j;

    if-nez v1, :cond_7

    .line 194
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a(Ljava/lang/Throwable;)V

    .line 196
    :cond_7
    sget-object v1, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a:Ljava/lang/String;

    const-string v3, "Delta failed"

    invoke-static {v1, v3, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 197
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    goto/16 :goto_2

    .line 127
    :cond_8
    const/4 v1, 0x0

    goto :goto_5

    .line 131
    :cond_9
    :try_start_3
    invoke-static {v4, v9}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_6

    .line 139
    :catchall_0
    move-exception v1

    :try_start_4
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 137
    :cond_a
    :try_start_5
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 139
    :try_start_6
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 142
    sget-object v1, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Database time: "

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    sub-long v12, v14, v12

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->p()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 145
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    goto/16 :goto_2

    .line 147
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1}, Lcom/dropbox/android/albums/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 152
    new-instance v2, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cursor doesn\'t match expected value, type="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", null?="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    invoke-interface {v1}, Lcom/dropbox/android/albums/d;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_c

    const/4 v1, 0x1

    :goto_7
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c
    const/4 v1, 0x0

    goto :goto_7

    .line 155
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/albums/AlbumsUpdateTask;->b:Lcom/dropbox/android/albums/d;

    iget-object v4, v11, Ldbxyzptlk/db231222/v/f;->b:Ljava/lang/String;

    invoke-interface {v1, v4}, Lcom/dropbox/android/albums/d;->a(Ljava/lang/String;)V

    .line 156
    iget-boolean v4, v11, Ldbxyzptlk/db231222/v/f;->d:Z

    .line 157
    iget-object v6, v11, Ldbxyzptlk/db231222/v/f;->b:Ljava/lang/String;

    .line 161
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v1

    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 165
    :cond_e
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10

    .line 166
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/albums/PhotosModel;->h()V

    .line 169
    :cond_10
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_8
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/v/d;

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v10

    iget-object v1, v1, Ldbxyzptlk/db231222/v/d;->a:Ljava/lang/String;

    invoke-virtual {v10, v1}, Lcom/dropbox/android/albums/PhotosModel;->c(Ljava/lang/String;)V

    goto :goto_8

    :cond_11
    move v1, v4

    move v4, v5

    .line 173
    goto/16 :goto_1

    .line 175
    :cond_12
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->Y()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v5, "updated"

    int-to-long v9, v3

    invoke-virtual {v1, v5, v9, v10}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v3, "removed"

    int-to-long v5, v2

    invoke-virtual {v1, v3, v5, v6}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "pages"

    int-to-long v3, v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {v7}, Ldbxyzptlk/db231222/z/y;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 199
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    goto/16 :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/albums/AlbumsUpdateTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
