.class final Lcom/dropbox/android/albums/b;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/albums/Album;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/albums/Album;
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/dropbox/android/albums/Album;

    invoke-direct {v0, p1}, Lcom/dropbox/android/albums/Album;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final a(I)[Lcom/dropbox/android/albums/Album;
    .locals 1

    .prologue
    .line 162
    new-array v0, p1, [Lcom/dropbox/android/albums/Album;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0, p1}, Lcom/dropbox/android/albums/b;->a(Landroid/os/Parcel;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0, p1}, Lcom/dropbox/android/albums/b;->a(I)[Lcom/dropbox/android/albums/Album;

    move-result-object v0

    return-object v0
.end method
