.class Lcom/dropbox/android/albums/RemoveTask;
.super Lcom/dropbox/android/albums/PhotosTask;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/albums/AlbumItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/PhotosModel;",
            "Lcom/dropbox/android/provider/j;",
            "Ldbxyzptlk/db231222/z/M;",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/albums/AlbumItemEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/albums/PhotosTask;-><init>(Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    .line 31
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->a:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->b:Ljava/util/ArrayList;

    .line 33
    const-string v0, "\\"

    iget-object v1, p0, Lcom/dropbox/android/albums/RemoveTask;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->c:Ljava/lang/String;

    .line 35
    invoke-virtual {p4}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/albums/RemoveTask;->e:Z

    .line 36
    return-void
.end method

.method private h()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 64
    iget-object v3, p0, Lcom/dropbox/android/albums/RemoveTask;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to remove items from more than one colleciton at same time"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/RemoveTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    iget-object v0, p0, Lcom/dropbox/android/albums/RemoveTask;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 52
    new-instance v3, Ldbxyzptlk/db231222/j/l;

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/albums/AlbumItemEntry;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 54
    :cond_0
    return-object v1
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 4

    .prologue
    .line 74
    iget v0, p0, Lcom/dropbox/android/albums/RemoveTask;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/albums/RemoveTask;->d:I

    .line 76
    :try_start_0
    invoke-direct {p0}, Lcom/dropbox/android/albums/RemoveTask;->h()Ljava/util/ArrayList;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->e()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/albums/RemoveTask;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v1

    .line 78
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->g_()Lcom/dropbox/android/provider/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 79
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :try_start_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/z/p;

    iget-boolean v3, p0, Lcom/dropbox/android/albums/RemoveTask;->e:Z

    invoke-static {v2, v0, v3}, Lcom/dropbox/android/albums/PhotosModel;->a(Landroid/database/sqlite/SQLiteDatabase;Ldbxyzptlk/db231222/z/p;Z)V

    .line 82
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/z/p;

    iget-object v3, v0, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v2, v3, v0}, Lcom/dropbox/android/albums/PhotosModel;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    .line 83
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 87
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/RemoveTask;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->c(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->h()V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_0

    .line 95
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->g()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 97
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    :goto_0
    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "collectionItemsRemove"

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/albums/RemoveTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/dropbox/android/albums/RemoveTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
