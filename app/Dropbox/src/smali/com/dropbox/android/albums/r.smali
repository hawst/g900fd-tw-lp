.class abstract Lcom/dropbox/android/albums/r;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dropbox/android/taskqueue/u;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/taskqueue/v;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/albums/l;)V
    .locals 0

    .prologue
    .line 984
    invoke-direct {p0}, Lcom/dropbox/android/albums/r;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/dropbox/android/taskqueue/u;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public final a(Lcom/dropbox/android/taskqueue/u;JJ)V
    .locals 0

    .prologue
    .line 994
    return-void
.end method

.method protected abstract a(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/dropbox/android/taskqueue/w;",
            ")V"
        }
    .end annotation
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 989
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 1005
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/albums/r;->a(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V

    .line 1006
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 998
    invoke-virtual {p0, p1}, Lcom/dropbox/android/albums/r;->a(Lcom/dropbox/android/taskqueue/u;)V

    .line 999
    return-void
.end method

.method public final d(Lcom/dropbox/android/taskqueue/u;)V
    .locals 3

    .prologue
    .line 1012
    invoke-static {}, Lcom/dropbox/android/albums/PhotosModel;->k()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Canceled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    return-void
.end method
