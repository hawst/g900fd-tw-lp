.class public Lcom/dropbox/android/albums/AlbumItemEntry;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/albums/AlbumItemEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/dropbox/android/util/DropboxPath;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/dropbox/android/albums/c;

    invoke-direct {v0}, Lcom/dropbox/android/albums/c;-><init>()V

    sput-object v0, Lcom/dropbox/android/albums/AlbumItemEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/dropbox/android/provider/e;->b:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    .line 23
    sget-object v0, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    .line 24
    const-string v0, "path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 31
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->e:Ljava/lang/String;

    .line 36
    :goto_0
    sget-object v0, Lcom/dropbox/android/provider/e;->d:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->d:Ljava/lang/String;

    .line 37
    return-void

    .line 33
    :cond_0
    iput-object v1, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 34
    iput-object v1, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    .line 118
    const-class v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->d:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->e:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public static a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/AlbumItemEntry;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/dropbox/android/albums/AlbumItemEntry;

    invoke-direct {v0, p0}, Lcom/dropbox/android/albums/AlbumItemEntry;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41
    instance-of v0, p1, Lcom/dropbox/android/albums/AlbumItemEntry;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/dropbox/android/albums/AlbumItemEntry;

    iget-object v0, v0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/dropbox/android/albums/AlbumItemEntry;

    iget-object v0, p1, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 42
    goto :goto_0

    :cond_1
    move v0, v1

    .line 45
    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/albums/AlbumItemEntry;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    return-void
.end method
