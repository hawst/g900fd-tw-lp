.class final Lcom/dropbox/android/notifications/G;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/l/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/l/i",
        "<",
        "Lcom/dropbox/android/notifications/NotificationKey;",
        "Lcom/dropbox/android/util/DropboxPath;",
        "Ldbxyzptlk/db231222/m/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/dropbox/android/notifications/G;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    iput-object p2, p0, Lcom/dropbox/android/notifications/G;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/notifications/NotificationKey;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 347
    if-eqz p2, :cond_0

    .line 348
    iget-object v0, p0, Lcom/dropbox/android/notifications/G;->a:Landroid/app/Activity;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/DropboxBrowser;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 350
    iget-object v1, p0, Lcom/dropbox/android/notifications/G;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->startActivity(Landroid/content/Intent;)V

    .line 352
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/m/a;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/dropbox/android/notifications/G;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/dropbox/android/notifications/G;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ldbxyzptlk/db231222/m/a;->a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    .line 357
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 344
    check-cast p1, Lcom/dropbox/android/notifications/NotificationKey;

    check-cast p2, Ldbxyzptlk/db231222/m/a;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/G;->a(Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/m/a;)V

    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 344
    check-cast p1, Lcom/dropbox/android/notifications/NotificationKey;

    check-cast p2, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/G;->a(Lcom/dropbox/android/notifications/NotificationKey;Lcom/dropbox/android/util/DropboxPath;)V

    return-void
.end method
