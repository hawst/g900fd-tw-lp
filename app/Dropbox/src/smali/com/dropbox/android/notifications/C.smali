.class final Lcom/dropbox/android/notifications/C;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ldbxyzptlk/db231222/r/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/dropbox/android/notifications/C;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/r/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;",
            "Ldbxyzptlk/db231222/r/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 196
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/c;->a()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/dropbox/android/notifications/C;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/notifications/l;

    move-result-object v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/notifications/l;->a(Ldbxyzptlk/db231222/s/d;)V

    .line 200
    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/dropbox/android/notifications/C;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    .line 188
    :goto_0
    new-instance v1, Ldbxyzptlk/db231222/r/a;

    iget-object v2, p0, Lcom/dropbox/android/notifications/C;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/notifications/C;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v3}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/service/a;

    move-result-object v3

    sget-object v4, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    invoke-direct {v1, v2, v3, v0, v4}, Ldbxyzptlk/db231222/r/a;-><init>(Landroid/content/Context;Lcom/dropbox/android/service/a;Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)V

    return-object v1

    .line 187
    :cond_0
    sget-object v0, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    goto :goto_0

    .line 190
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown loader: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 182
    check-cast p2, Ldbxyzptlk/db231222/r/c;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/C;->a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/r/c;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    return-void
.end method
