.class public Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;
.super Lcom/dropbox/sync/android/DbxNotificationHeader;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/dropbox/android/notifications/L;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/L;-><init>()V

    sput-object v0, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JILjava/lang/String;Ljava/util/Date;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct/range {p0 .. p6}, Lcom/dropbox/sync/android/DbxNotificationHeader;-><init>(JILjava/lang/String;Ljava/util/Date;I)V

    .line 23
    return-void
.end method

.method protected static a(Landroid/os/Parcel;)Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;
    .locals 8

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 33
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 34
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 35
    new-instance v5, Ljava/util/Date;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 36
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 37
    new-instance v0, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;-><init>(JILjava/lang/String;Ljava/util/Date;I)V

    return-object v0
.end method

.method public static a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;
    .locals 7

    .prologue
    .line 26
    new-instance v0, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->b()I

    move-result v3

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->d()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;-><init>(JILjava/lang/String;Ljava/util/Date;I)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->a()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 43
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->d()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    return-void
.end method
