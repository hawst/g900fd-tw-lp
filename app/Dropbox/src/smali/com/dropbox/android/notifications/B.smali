.class final Lcom/dropbox/android/notifications/B;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/dropbox/android/notifications/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Lcom/dropbox/android/notifications/c;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Lcom/dropbox/android/notifications/c;",
            ">;",
            "Lcom/dropbox/android/notifications/c;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 152
    invoke-virtual {p2}, Lcom/dropbox/android/notifications/c;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v1, v0, [J

    .line 155
    invoke-virtual {p2}, Lcom/dropbox/android/notifications/c;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/DbxNotificationHeader;

    .line 156
    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v3

    aput-wide v3, v1, v5

    goto :goto_0

    .line 160
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/sync/android/aJ;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aJ;->a([J)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 170
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/notifications/l;

    move-result-object v0

    invoke-virtual {p2}, Lcom/dropbox/android/notifications/c;->a()Lcom/dropbox/sync/android/aO;

    move-result-object v1

    invoke-virtual {p2}, Lcom/dropbox/android/notifications/c;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/sync/android/aO;I)V

    .line 172
    iget-object v0, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->d(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->e(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 174
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 164
    invoke-static {}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "failed to ack notifications"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 165
    :catch_1
    move-exception v0

    .line 166
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to ack notifications:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Lcom/dropbox/android/notifications/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    if-nez p1, :cond_0

    .line 142
    new-instance v0, Lcom/dropbox/android/notifications/I;

    iget-object v1, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/sync/android/aJ;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/notifications/B;->a:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v3}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->c(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/notifications/a;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/notifications/I;-><init>(Landroid/content/Context;Lcom/dropbox/sync/android/aJ;Lcom/dropbox/android/notifications/K;)V

    return-object v0

    .line 145
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown loader: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 137
    check-cast p2, Lcom/dropbox/android/notifications/c;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/B;->a(Landroid/support/v4/content/p;Lcom/dropbox/android/notifications/c;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Lcom/dropbox/android/notifications/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    return-void
.end method
