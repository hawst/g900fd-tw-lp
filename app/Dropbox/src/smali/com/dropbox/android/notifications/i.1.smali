.class public abstract Lcom/dropbox/android/notifications/i;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    .line 179
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/i;->b()V

    .line 180
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 216
    const-string v0, "|"

    iget-object v1, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-virtual {p0, v0}, Lcom/dropbox/android/notifications/i;->a(Ljava/lang/String;)V

    .line 218
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract a(Ljava/lang/String;)V
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    iget-object v1, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 186
    const-string v1, "\\|"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 189
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    invoke-direct {p0}, Lcom/dropbox/android/notifications/i;->c()V

    .line 196
    :cond_0
    return v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-direct {p0}, Lcom/dropbox/android/notifications/i;->c()V

    .line 204
    :cond_0
    return v0
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/dropbox/android/notifications/i;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
