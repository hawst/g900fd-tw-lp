.class public Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;
.super Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag",
        "<",
        "Lcom/dropbox/android/notifications/NotificationsFeedFragment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 106
    check-cast p1, Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;->a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V
    .locals 3

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 121
    const-string v0, "ARG_NOTIF_KEY"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/notifications/NotificationKey;

    .line 122
    const-string v2, "ARG_INVITE_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 123
    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Lcom/dropbox/android/notifications/NotificationKey;J)V

    .line 124
    return-void
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Lcom/dropbox/android/notifications/NotificationKey;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 112
    const v0, 0x7f0d02fc

    const v1, 0x7f0d0265

    invoke-virtual {p0, p1, p3, v0, v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;II)V

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 114
    const-string v1, "ARG_NOTIF_KEY"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 115
    const-string v1, "ARG_INVITE_ID"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 116
    return-void
.end method
