.class public Lcom/dropbox/android/notifications/d;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/dropbox/android/notifications/h;

.field private static final f:Lcom/dropbox/android/notifications/g;

.field private static final g:Ljava/lang/Object;


# instance fields
.field private final h:Landroid/content/Context;

.field private final i:Ldbxyzptlk/db231222/r/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    const-class v0, Lcom/dropbox/android/notifications/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/notifications/d;->a:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 84
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/dropbox/android/notifications/d;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    .line 103
    new-instance v0, Lcom/dropbox/android/notifications/h;

    invoke-direct {v0, v2}, Lcom/dropbox/android/notifications/h;-><init>(Lcom/dropbox/android/notifications/e;)V

    sput-object v0, Lcom/dropbox/android/notifications/d;->e:Lcom/dropbox/android/notifications/h;

    .line 135
    new-instance v0, Lcom/dropbox/android/notifications/g;

    invoke-direct {v0, v2}, Lcom/dropbox/android/notifications/g;-><init>(Lcom/dropbox/android/notifications/e;)V

    sput-object v0, Lcom/dropbox/android/notifications/d;->f:Lcom/dropbox/android/notifications/g;

    .line 167
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/notifications/d;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    iput-object p1, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    .line 234
    if-nez p2, :cond_3

    .line 235
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 238
    :goto_0
    iput-object v0, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    .line 240
    sget-object v2, Lcom/dropbox/android/notifications/d;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 241
    :try_start_0
    sget-object v3, Lcom/dropbox/android/notifications/d;->e:Lcom/dropbox/android/notifications/h;

    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Lcom/dropbox/android/notifications/h;->a(Ldbxyzptlk/db231222/n/P;)V

    .line 242
    sget-object v0, Lcom/dropbox/android/notifications/d;->f:Lcom/dropbox/android/notifications/g;

    iget-object v3, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/g;->a(Ldbxyzptlk/db231222/n/P;)V

    .line 243
    monitor-exit v2

    .line 244
    return-void

    :cond_1
    move-object v0, v1

    .line 236
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 241
    goto :goto_1

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move-object v0, p2

    goto :goto_0
.end method

.method private a(Landroid/app/PendingIntent;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;Lcom/dropbox/android/notifications/f;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 370
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    const-class v2, Lcom/dropbox/android/service/NotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 371
    const-string v1, "ACTION_NOTIFICATION_ACTED_UPON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    const-string v1, "EXTRA_NOTIFICATION_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const-string v1, "EXTRA_NOTIFICATION_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 374
    const-string v1, "EXTRA_ACTION"

    invoke-virtual {p6}, Lcom/dropbox/android/notifications/f;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    if-eqz p1, :cond_0

    .line 376
    const-string v1, "EXTRA_PENDING_INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 378
    :cond_0
    if-eqz p4, :cond_1

    .line 379
    const-string v1, "EXTRA_NOTIFICATION_TAG"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    :cond_1
    if-eqz p5, :cond_2

    .line 382
    const-string v1, "EXTRA_ACK"

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 384
    :cond_2
    return-object v0
.end method

.method private a(Landroid/app/Notification;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;)V
    .locals 8

    .prologue
    .line 351
    iget v0, p1, Landroid/app/Notification;->flags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 352
    sget-object v6, Lcom/dropbox/android/notifications/f;->a:Lcom/dropbox/android/notifications/f;

    .line 356
    :goto_0
    iget-object v1, p1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/notifications/d;->a(Landroid/app/PendingIntent;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;Lcom/dropbox/android/notifications/f;)Landroid/content/Intent;

    move-result-object v7

    .line 357
    invoke-direct {p0, v7}, Lcom/dropbox/android/notifications/d;->b(Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 358
    iget-object v1, p1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    sget-object v6, Lcom/dropbox/android/notifications/f;->c:Lcom/dropbox/android/notifications/f;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/notifications/d;->a(Landroid/app/PendingIntent;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;Lcom/dropbox/android/notifications/f;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/d;->b(Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 360
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/d;->a()Landroid/app/NotificationManager;

    move-result-object v0

    .line 361
    invoke-virtual {v0, p2, p3, p1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 363
    invoke-direct {p0, p2, p3, v7}, Lcom/dropbox/android/notifications/d;->a(Ljava/lang/String;ILandroid/content/Intent;)V

    .line 365
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    const-class v3, Lcom/dropbox/android/service/NotificationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 366
    return-void

    .line 354
    :cond_0
    sget-object v6, Lcom/dropbox/android/notifications/f;->b:Lcom/dropbox/android/notifications/f;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 514
    sget-object v1, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 515
    :try_start_0
    sget-object v0, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/dropbox/android/notifications/d;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    monitor-exit v1

    .line 517
    return-void

    .line 516
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 389
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    long-to-int v0, v0

    .line 390
    iget-object v1, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, p1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 391
    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 487
    sget-object v1, Lcom/dropbox/android/notifications/d;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 488
    :try_start_0
    sget-object v0, Lcom/dropbox/android/notifications/d;->f:Lcom/dropbox/android/notifications/g;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/g;->c(Ljava/lang/String;)Z

    .line 489
    sget-object v0, Lcom/dropbox/android/notifications/d;->e:Lcom/dropbox/android/notifications/h;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/h;->c(Ljava/lang/String;)Z

    .line 490
    monitor-exit v1

    .line 491
    return-void

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/d;->a()Landroid/app/NotificationManager;

    move-result-object v0

    .line 469
    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 470
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/notifications/d;->d(Ljava/lang/String;I)V

    .line 471
    return-void
.end method

.method private static c(Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 509
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 510
    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private static c(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 500
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 520
    sget-object v1, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 521
    :try_start_0
    sget-object v0, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/dropbox/android/notifications/d;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    monitor-exit v1

    .line 523
    return-void

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/d;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    const-class v3, Lcom/dropbox/android/service/NotificationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 477
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/aH;Ljava/lang/String;Ljava/lang/Long;Landroid/os/Bundle;)I
    .locals 6

    .prologue
    .line 312
    if-eqz p2, :cond_0

    sget-object v0, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const/4 v3, 0x0

    .line 326
    :goto_0
    return v3

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    invoke-virtual {p1, v0, p4}, Lcom/dropbox/android/util/aH;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;

    move-result-object v1

    .line 317
    invoke-virtual {p1}, Lcom/dropbox/android/util/aH;->a()Ljava/lang/String;

    move-result-object v2

    .line 318
    sget-object v0, Lcom/dropbox/android/notifications/d;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v3

    .line 319
    if-eqz p2, :cond_1

    .line 320
    sget-object v0, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Landroid/util/Pair;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    .line 322
    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/notifications/d;->a(Landroid/app/Notification;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;)V

    .line 324
    const-string v0, "show"

    invoke-static {v0, v2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "tag"

    invoke-virtual {v0, v1, p2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method

.method protected final a()Landroid/app/NotificationManager;
    .locals 2

    .prologue
    .line 335
    const-string v0, "notification"

    .line 336
    iget-object v1, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 571
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 572
    :goto_0
    const-string v3, "ACTION_NOTIFICATION_ACTED_UPON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 573
    const-string v0, "EXTRA_NOTIFICATION_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 574
    const-string v0, "EXTRA_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/notifications/f;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/notifications/f;

    move-result-object v4

    .line 575
    sget-object v0, Lcom/dropbox/android/notifications/f;->b:Lcom/dropbox/android/notifications/f;

    if-eq v4, v0, :cond_9

    move v0, v1

    .line 576
    :goto_1
    const-string v1, "EXTRA_NOTIFICATION_TAG"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 578
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 579
    sget-object v5, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    :cond_0
    if-eqz v0, :cond_2

    .line 583
    const-string v0, "EXTRA_NOTIFICATION_ID"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 584
    invoke-direct {p0, v3, v0}, Lcom/dropbox/android/notifications/d;->d(Ljava/lang/String;I)V

    .line 586
    sget-object v2, Lcom/dropbox/android/notifications/d;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 587
    :try_start_0
    sget-object v0, Lcom/dropbox/android/notifications/d;->e:Lcom/dropbox/android/notifications/h;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/notifications/h;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 588
    sget-object v0, Lcom/dropbox/android/notifications/d;->f:Lcom/dropbox/android/notifications/g;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/notifications/g;->b(Ljava/lang/String;)Z

    .line 590
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    :cond_2
    const-string v0, "EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 594
    const-string v0, "EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 596
    :try_start_1
    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_2

    .line 602
    :cond_3
    :goto_2
    const-string v0, "EXTRA_ACK"

    const-wide/16 v5, -0x1

    invoke-virtual {p1, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    .line 603
    cmp-long v0, v5, v8

    if-ltz v0, :cond_4

    .line 604
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    if-nez v0, :cond_a

    .line 605
    sget-object v0, Lcom/dropbox/android/notifications/d;->a:Ljava/lang/String;

    const-string v2, "Notification acked, but no user."

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    :cond_4
    :goto_3
    invoke-virtual {v4}, Lcom/dropbox/android/notifications/f;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 619
    if-eqz v1, :cond_5

    .line 620
    const-string v2, "tag"

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 622
    :cond_5
    cmp-long v1, v5, v8

    if-ltz v1, :cond_6

    .line 623
    const-string v1, "nid"

    invoke-virtual {v0, v1, v5, v6}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 625
    :cond_6
    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 627
    :cond_7
    return-void

    .line 571
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 575
    goto :goto_1

    .line 590
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 608
    :cond_a
    :try_start_3
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/d;->b()Lcom/dropbox/sync/android/aJ;

    move-result-object v0

    .line 609
    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v7, 0x0

    aput-wide v5, v2, v7

    invoke-virtual {v0, v2}, Lcom/dropbox/sync/android/aJ;->a([J)V
    :try_end_3
    .catch Lcom/dropbox/sync/android/aH; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 610
    :catch_0
    move-exception v0

    goto :goto_3

    .line 612
    :catch_1
    move-exception v0

    .line 613
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 597
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public final a(Lcom/dropbox/android/util/aM;)V
    .locals 2

    .prologue
    .line 405
    invoke-virtual {p1}, Lcom/dropbox/android/util/aM;->a()Ljava/lang/String;

    move-result-object v0

    .line 406
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/notifications/d;->b(Ljava/lang/String;I)V

    .line 407
    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/d;->b(Ljava/lang/String;)V

    .line 408
    invoke-direct {p0}, Lcom/dropbox/android/notifications/d;->e()V

    .line 409
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 426
    sget-object v0, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 427
    if-eqz v0, :cond_0

    .line 428
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/notifications/d;->b(Ljava/lang/String;I)V

    .line 430
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/notifications/d;->e()V

    .line 431
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 263
    invoke-virtual {p1}, Lcom/dropbox/android/util/aM;->a()Ljava/lang/String;

    move-result-object v2

    .line 265
    sget-object v1, Lcom/dropbox/android/notifications/d;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 266
    :try_start_0
    sget-object v3, Lcom/dropbox/android/notifications/d;->f:Lcom/dropbox/android/notifications/g;

    invoke-virtual {v3, v2}, Lcom/dropbox/android/notifications/g;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 267
    monitor-exit v1

    .line 283
    :goto_0
    return v0

    .line 269
    :cond_0
    invoke-virtual {p1}, Lcom/dropbox/android/util/aM;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 270
    sget-object v3, Lcom/dropbox/android/notifications/d;->e:Lcom/dropbox/android/notifications/h;

    invoke-virtual {v3, v2}, Lcom/dropbox/android/notifications/h;->b(Ljava/lang/String;)Z

    .line 272
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 276
    sget-object v6, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    monitor-enter v6

    .line 277
    :try_start_1
    invoke-virtual {p1}, Lcom/dropbox/android/util/aM;->c()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v1}, Lcom/dropbox/android/notifications/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    invoke-virtual {p1, v0, p2}, Lcom/dropbox/android/util/aM;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;

    move-result-object v1

    .line 279
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/notifications/d;->a(Landroid/app/Notification;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;)V

    .line 280
    const-string v0, "show"

    invoke-static {v0, v2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 281
    const/4 v0, 0x1

    monitor-exit v6

    goto :goto_0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 272
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 283
    :cond_3
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 530
    sget-object v1, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 531
    :try_start_0
    sget-object v0, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/dropbox/android/notifications/d;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final b()Lcom/dropbox/sync/android/aJ;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->i:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aJ;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aJ;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 444
    sget-object v0, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 445
    sget-object v1, Lcom/dropbox/android/notifications/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 446
    if-eqz v0, :cond_0

    .line 447
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/notifications/d;->b(Ljava/lang/String;I)V

    goto :goto_0

    .line 452
    :cond_1
    sget-object v2, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    monitor-enter v2

    .line 453
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 454
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 455
    invoke-static {v0}, Lcom/dropbox/android/notifications/d;->c(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    .line 456
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/notifications/d;->b(Ljava/lang/String;I)V

    .line 457
    iget-object v0, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    .line 458
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/d;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 464
    iget-object v0, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/dropbox/android/notifications/d;->h:Landroid/content/Context;

    const-class v3, Lcom/dropbox/android/service/NotificationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 465
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 536
    sget-object v1, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 537
    :try_start_0
    sget-object v0, Lcom/dropbox/android/notifications/d;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
