.class final Lcom/dropbox/android/notifications/F;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/notifications/v;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/r/d;

.field final synthetic b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Ldbxyzptlk/db231222/r/d;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    iput-object p2, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 293
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bL()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->l:Lcom/dropbox/android/activity/payment/h;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v3}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->j(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/service/a;->e()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;Lcom/dropbox/android/util/analytics/r;Z)V

    .line 297
    return-void
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationKey;)V
    .locals 4

    .prologue
    .line 315
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bN()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 316
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->g(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    new-instance v1, Ldbxyzptlk/db231222/l/m;

    iget-object v2, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/sync/android/aJ;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Ldbxyzptlk/db231222/l/m;-><init>(Lcom/dropbox/android/notifications/NotificationKey;Lcom/dropbox/sync/android/aJ;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    .line 318
    return-void
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationKey;J)V
    .locals 8

    .prologue
    .line 252
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->g:Lcom/dropbox/android/activity/dialog/o;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/a;->e()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Lcom/dropbox/android/activity/dialog/o;Lcom/dropbox/android/activity/dialog/p;Z)Lcom/dropbox/android/activity/dialog/OverQuotaDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 261
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->g(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/l/k;

    move-result-object v7

    new-instance v0, Ldbxyzptlk/db231222/l/n;

    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v4

    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/sync/android/aJ;

    move-result-object v5

    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v6

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/l/n;-><init>(Lcom/dropbox/android/notifications/NotificationKey;JLcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/sync/android/aJ;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v7, v0}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationKey;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 278
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 279
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 280
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 282
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->startActivity(Landroid/content/Intent;)V

    .line 283
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bR()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "activity_launched"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 285
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->c(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/notifications/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/a;->b(Lcom/dropbox/android/notifications/NotificationKey;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :goto_0
    return-void

    .line 286
    :catch_0
    move-exception v0

    .line 287
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bR()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "activity_launched"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationKey;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 265
    new-instance v0, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;-><init>()V

    .line 266
    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;->a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Lcom/dropbox/android/notifications/NotificationKey;Ljava/lang/String;J)V

    .line 267
    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$SharedFolderDeclineConfirmFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 268
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 301
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bK()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 302
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/dropbox/android/activity/ReferralActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->startActivity(Landroid/content/Intent;)V

    .line 303
    return-void
.end method

.method public final b(Lcom/dropbox/android/notifications/NotificationKey;J)V
    .locals 7

    .prologue
    .line 272
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->i(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/l/k;

    move-result-object v6

    new-instance v0, Ldbxyzptlk/db231222/l/p;

    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    iget-object v1, p0, Lcom/dropbox/android/notifications/F;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v3

    move-object v1, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/l/p;-><init>(Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/provider/MetadataManager;J)V

    invoke-virtual {v6, v0}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    .line 274
    return-void
.end method

.method public final b(Lcom/dropbox/android/notifications/NotificationKey;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 307
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bM()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 308
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->m:Lcom/dropbox/android/activity/payment/h;

    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/J;->a()Z

    move-result v2

    invoke-static {v0, v1, v2, p2}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;ZLjava/lang/String;)V

    .line 311
    return-void
.end method

.method public final b(Lcom/dropbox/android/notifications/NotificationKey;)Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/dropbox/android/notifications/F;->b:Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->g(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/l/k;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
