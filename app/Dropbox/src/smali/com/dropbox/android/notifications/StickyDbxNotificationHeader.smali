.class public Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/dropbox/android/notifications/O;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/O;-><init>()V

    sput-object v0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->a:Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    .line 18
    iput p2, p0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b:I

    .line 19
    return-void
.end method

.method protected static a(Landroid/os/Parcel;)Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;
    .locals 3

    .prologue
    .line 22
    invoke-static {p0}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->a(Landroid/os/Parcel;)Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    move-result-object v0

    .line 23
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 24
    new-instance v2, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    invoke-direct {v2, v0, v1}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;-><init>(Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;I)V

    return-object v2
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->a:Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->a:Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->writeToParcel(Landroid/os/Parcel;I)V

    .line 30
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 31
    return-void
.end method
