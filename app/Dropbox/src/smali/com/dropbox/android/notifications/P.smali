.class public final Lcom/dropbox/android/notifications/P;
.super Lcom/dropbox/android/notifications/j;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/notifications/j",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/notifications/j;-><init>()V

    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 38
    if-lez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->d()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->h()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->h()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 26
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/P;->a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/P;->a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/P;->a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
