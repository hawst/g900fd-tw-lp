.class final Lcom/dropbox/android/notifications/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/notifications/K;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/notifications/K",
        "<",
        "Lcom/dropbox/android/notifications/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/dropbox/android/notifications/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/notifications/j",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/dropbox/android/notifications/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/notifications/j",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/dropbox/android/notifications/P;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/P;-><init>()V

    new-instance v1, Lcom/dropbox/android/notifications/Q;

    invoke-direct {v1}, Lcom/dropbox/android/notifications/Q;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/notifications/a;-><init>(Lcom/dropbox/android/notifications/j;Lcom/dropbox/android/notifications/j;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Lcom/dropbox/android/notifications/a;-><init>()V

    .line 95
    const-string v0, "BUNDLE_KEY_FORCED_UNREAD"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 96
    iget-object v5, p0, Lcom/dropbox/android/notifications/a;->a:Ljava/util/Set;

    check-cast v0, Lcom/dropbox/android/notifications/NotificationKey;

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 98
    :cond_0
    const-string v0, "BUNDLE_KEY_FORCED_STICKY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 99
    check-cast v0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    .line 100
    iget-object v5, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->a()Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    move-result-object v6

    invoke-static {v6}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 102
    :cond_1
    const-string v0, "BUNDLE_KEY_FORCED_VISIBLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 103
    iget-object v4, p0, Lcom/dropbox/android/notifications/a;->c:Ljava/util/Set;

    check-cast v0, Lcom/dropbox/android/notifications/NotificationKey;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 105
    :cond_2
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/notifications/j;Lcom/dropbox/android/notifications/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/notifications/j",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/dropbox/android/notifications/j",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/a;->a:Ljava/util/Set;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/a;->c:Ljava/util/Set;

    .line 86
    iput-object p1, p0, Lcom/dropbox/android/notifications/a;->d:Lcom/dropbox/android/notifications/j;

    .line 87
    iput-object p2, p0, Lcom/dropbox/android/notifications/a;->e:Lcom/dropbox/android/notifications/j;

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 111
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 112
    const-string v2, "BUNDLE_KEY_FORCED_UNREAD"

    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->a:Ljava/util/Set;

    new-array v3, v4, [Lcom/dropbox/android/notifications/NotificationKey;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 114
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 116
    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    .line 117
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_0
    const-string v3, "BUNDLE_KEY_FORCED_STICKY"

    new-array v0, v4, [Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 121
    const-string v2, "BUNDLE_KEY_FORCED_VISIBLE"

    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->c:Ljava/util/Set;

    new-array v3, v4, [Lcom/dropbox/android/notifications/NotificationKey;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 123
    return-object v1
.end method

.method public final a(Lcom/dropbox/sync/android/aO;)Lcom/dropbox/android/notifications/c;
    .locals 9

    .prologue
    .line 144
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 145
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 147
    iget-object v0, p1, Lcom/dropbox/sync/android/aO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;

    .line 148
    invoke-virtual {v0}, Ldbxyzptlk/db231222/A/a;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v2

    .line 149
    invoke-static {v2}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v6

    .line 152
    iget-object v1, p0, Lcom/dropbox/android/notifications/a;->e:Lcom/dropbox/android/notifications/j;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/notifications/j;->a(Ldbxyzptlk/db231222/A/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/notifications/a;->c:Ljava/util/Set;

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    :cond_1
    invoke-virtual {v2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v1

    if-nez v1, :cond_3

    .line 158
    iget-object v1, p0, Lcom/dropbox/android/notifications/a;->a:Ljava/util/Set;

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v1, v2

    move-object v2, v0

    .line 167
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->d:Lcom/dropbox/android/notifications/j;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/notifications/j;->a(Ldbxyzptlk/db231222/A/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 168
    invoke-static {v0}, Lcom/dropbox/android/notifications/P;->a(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 169
    iget-object v7, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 170
    invoke-static {v1}, Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    move-result-object v1

    .line 171
    iget-object v7, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    new-instance v8, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    invoke-direct {v8, v1, v0}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;-><init>(Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;I)V

    invoke-interface {v7, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/dropbox/android/notifications/a;->a:Ljava/util/Set;

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 162
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a(I)Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    .line 163
    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/A/a;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ldbxyzptlk/db231222/A/a;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    .line 178
    :cond_4
    new-instance v0, Lcom/dropbox/android/notifications/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/b;-><init>(Lcom/dropbox/android/notifications/a;)V

    .line 187
    new-instance v1, Lcom/dropbox/android/notifications/M;

    invoke-direct {v1, v0}, Lcom/dropbox/android/notifications/M;-><init>(Lcom/dropbox/android/notifications/N;)V

    .line 188
    invoke-static {v3, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 190
    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    .line 191
    const/4 v2, -0x1

    .line 192
    :cond_5
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 193
    invoke-interface {v5}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    .line 194
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;

    .line 195
    iget-object v6, p0, Lcom/dropbox/android/notifications/a;->b:Ljava/util/Map;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/A/a;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 201
    :goto_2
    new-instance v1, Lcom/dropbox/android/notifications/c;

    new-instance v2, Lcom/dropbox/sync/android/aO;

    iget-boolean v5, p1, Lcom/dropbox/sync/android/aO;->b:Z

    invoke-direct {v2, v3, v5}, Lcom/dropbox/sync/android/aO;-><init>(Ljava/util/List;Z)V

    invoke-direct {v1, v2, v4, v0}, Lcom/dropbox/android/notifications/c;-><init>(Lcom/dropbox/sync/android/aO;Ljava/util/Set;I)V

    return-object v1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move-object v1, v2

    move-object v2, v0

    goto/16 :goto_1
.end method

.method public final a(Lcom/dropbox/android/notifications/NotificationKey;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    return-void
.end method

.method public final synthetic b(Lcom/dropbox/sync/android/aO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lcom/dropbox/android/notifications/a;->a(Lcom/dropbox/sync/android/aO;)Lcom/dropbox/android/notifications/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/notifications/NotificationKey;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/dropbox/android/notifications/a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 140
    return-void
.end method
