.class public final Lcom/dropbox/android/notifications/y;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/notifications/d;

.field private final b:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/aM;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ldbxyzptlk/db231222/A/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/A/b",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/notifications/d;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Ldbxyzptlk/db231222/l/k;->b()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/y;->b:Ldbxyzptlk/db231222/l/k;

    .line 36
    invoke-static {}, Ldbxyzptlk/db231222/l/k;->a()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/y;->c:Ldbxyzptlk/db231222/l/k;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    .line 54
    new-instance v0, Lcom/dropbox/android/notifications/z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/z;-><init>(Lcom/dropbox/android/notifications/y;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/y;->f:Ldbxyzptlk/db231222/A/b;

    .line 79
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/y;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 82
    iput-object p1, p0, Lcom/dropbox/android/notifications/y;->a:Lcom/dropbox/android/notifications/d;

    .line 83
    return-void
.end method

.method private static a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user_notification:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/notifications/NotificationKey;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/y;Ldbxyzptlk/db231222/A/c;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/dropbox/android/notifications/y;->a(Ldbxyzptlk/db231222/A/c;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/y;Ldbxyzptlk/db231222/A/d;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/dropbox/android/notifications/y;->a(Ldbxyzptlk/db231222/A/d;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/y;Ldbxyzptlk/db231222/A/e;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/dropbox/android/notifications/y;->a(Ldbxyzptlk/db231222/A/e;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/util/aH;Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 222
    const-string v0, "notification.show"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "nid"

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "type_id"

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->b()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "tok"

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 227
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    monitor-enter v1

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->a:Lcom/dropbox/android/notifications/d;

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p1, p3, v2, p4}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aH;Ljava/lang/String;Ljava/lang/Long;Landroid/os/Bundle;)I

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 230
    monitor-exit v1

    .line 231
    return-void

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/dropbox/android/util/aM;)V
    .locals 2

    .prologue
    .line 254
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    monitor-enter v1

    .line 255
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->a:Lcom/dropbox/android/notifications/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 256
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 257
    monitor-exit v1

    .line 258
    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/dropbox/android/util/aM;Lcom/dropbox/sync/android/DbxNotificationHeader;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 235
    const-string v0, "notification.show"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "nid"

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "type_id"

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->b()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "tok"

    invoke-virtual {p2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 240
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    monitor-enter v1

    .line 241
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->a:Lcom/dropbox/android/notifications/d;

    invoke-virtual {v0, p1, p3}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 242
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 243
    monitor-exit v1

    .line 244
    return-void

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ldbxyzptlk/db231222/A/c;)V
    .locals 4

    .prologue
    .line 205
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v0

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 207
    const-string v1, "ARG_DEAL_SPACE_GB"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 208
    const-string v1, "ARG_DEAL_PHONE_MAKE"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v1, "ARG_DEAL_EXPIRED"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->i()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 210
    const-string v1, "ARG_DEAL_DAYS_LEFT"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    const-string v1, "ARG_DEAL_WEEKS_LEFT"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 212
    const-string v1, "ARG_DEAL_MONTHS_LEFT"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    const-string v1, "ARG_NID"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 214
    sget-object v1, Lcom/dropbox/android/util/aM;->i:Lcom/dropbox/android/util/aM;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/c;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/android/util/aM;Lcom/dropbox/sync/android/DbxNotificationHeader;Landroid/os/Bundle;)V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    sget-object v0, Lcom/dropbox/android/util/aM;->i:Lcom/dropbox/android/util/aM;

    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/android/util/aM;)V

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/A/d;)V
    .locals 5

    .prologue
    .line 165
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->d()I

    move-result v1

    if-nez v1, :cond_0

    .line 170
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 171
    const-string v2, "ARG_SHARED_BY_NAME"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v2, "ARG_SHARED_BY_SHORT_NAME"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v2, "ARG_SHARED_OBJECT_NAME"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v2, "ARG_NID"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 175
    sget-object v2, Lcom/dropbox/android/util/aH;->c:Lcom/dropbox/android/util/aH;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v3

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/android/util/aH;Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/y;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/A/e;)V
    .locals 5

    .prologue
    .line 182
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v0

    if-nez v0, :cond_2

    .line 184
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->h()I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 186
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 187
    const-string v3, "ARG_SHARED_BY_SHORT_NAME"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v3, "ARG_SHARED_OBJECT_NAME"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v3, "ARG_SHARED_URL"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v3, "ARG_IS_COLLECTION"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 191
    if-eqz v0, :cond_0

    .line 192
    const-string v0, "ARG_COLLECTION_TYPE"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->i()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    :cond_0
    const-string v0, "ARG_SHARED_NUM_PHOTOS"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->j()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 196
    const-string v0, "ARG_SHARED_NUM_VIDEOS"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->k()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    sget-object v0, Lcom/dropbox/android/util/aH;->b:Lcom/dropbox/android/util/aH;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v3

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/android/util/aH;Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 202
    :goto_1
    return-void

    .line 184
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :cond_2
    invoke-direct {p0, v1}, Lcom/dropbox/android/notifications/y;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 247
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    monitor-enter v1

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->a:Lcom/dropbox/android/notifications/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/d;->a(Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 250
    monitor-exit v1

    .line 251
    return-void

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 145
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    monitor-enter v1

    .line 146
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/dropbox/android/notifications/y;->d:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 147
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 149
    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/y;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 152
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    monitor-enter v1

    .line 153
    :try_start_2
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/dropbox/android/notifications/y;->e:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 154
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aM;

    .line 156
    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/y;->a(Lcom/dropbox/android/util/aM;)V

    goto :goto_1

    .line 154
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 158
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/l/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->b:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/a;)V
    .locals 5

    .prologue
    .line 126
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 127
    :goto_0
    if-eqz v0, :cond_1

    .line 129
    const-string v0, "feed_update_item_ignored"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 135
    :goto_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/a;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    .line 136
    const-string v2, "nid"

    invoke-virtual {v1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v2, "type"

    invoke-virtual {v1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->b()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v2, "status"

    invoke-virtual {v1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v2, "tok"

    invoke-virtual {v1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 141
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :cond_1
    const-string v0, "feed_update_item_handled"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/dropbox/android/notifications/y;->f:Ldbxyzptlk/db231222/A/b;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Ldbxyzptlk/db231222/A/a;->a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final b()Ldbxyzptlk/db231222/l/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->c:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 109
    invoke-direct {p0}, Lcom/dropbox/android/notifications/y;->e()V

    .line 110
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/dropbox/android/notifications/y;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 117
    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 118
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
