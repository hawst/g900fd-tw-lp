.class public final Lcom/dropbox/android/notifications/l;
.super Landroid/widget/BaseAdapter;
.source "panda.py"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final j:Lcom/dropbox/android/notifications/x;


# instance fields
.field private a:Lcom/dropbox/sync/android/aO;

.field private b:I

.field private c:Z

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/A/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Landroid/widget/ListView;

.field private final g:Ldbxyzptlk/db231222/V/y;

.field private final h:Lcom/dropbox/android/notifications/v;

.field private final i:Ldbxyzptlk/db231222/A/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/A/b",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ldbxyzptlk/db231222/A/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/A/b",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ldbxyzptlk/db231222/s/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 196
    new-instance v0, Lcom/dropbox/android/notifications/x;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/notifications/x;-><init>(Lcom/dropbox/android/notifications/m;)V

    sput-object v0, Lcom/dropbox/android/notifications/l;->j:Lcom/dropbox/android/notifications/x;

    return-void
.end method

.method constructor <init>(Landroid/view/LayoutInflater;Ldbxyzptlk/db231222/V/y;Landroid/widget/ListView;Lcom/dropbox/android/notifications/v;)V
    .locals 1

    .prologue
    .line 277
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/notifications/l;->b:I

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    .line 106
    new-instance v0, Lcom/dropbox/android/notifications/m;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/m;-><init>(Lcom/dropbox/android/notifications/l;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/l;->i:Ldbxyzptlk/db231222/A/b;

    .line 205
    new-instance v0, Lcom/dropbox/android/notifications/p;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/p;-><init>(Lcom/dropbox/android/notifications/l;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/l;->k:Ldbxyzptlk/db231222/A/b;

    .line 278
    iput-object p3, p0, Lcom/dropbox/android/notifications/l;->f:Landroid/widget/ListView;

    .line 279
    iput-object p1, p0, Lcom/dropbox/android/notifications/l;->e:Landroid/view/LayoutInflater;

    .line 280
    iput-object p2, p0, Lcom/dropbox/android/notifications/l;->g:Ldbxyzptlk/db231222/V/y;

    .line 281
    iput-object p4, p0, Lcom/dropbox/android/notifications/l;->h:Lcom/dropbox/android/notifications/v;

    .line 282
    return-void
.end method

.method private a(Ldbxyzptlk/db231222/A/a;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 340
    iget-object v1, p0, Lcom/dropbox/android/notifications/l;->e:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->i:Ldbxyzptlk/db231222/A/b;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Ldbxyzptlk/db231222/A/a;->a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/l;)Lcom/dropbox/android/notifications/v;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->h:Lcom/dropbox/android/notifications/v;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 296
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    .line 297
    iget-boolean v0, p0, Lcom/dropbox/android/notifications/l;->c:Z

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    new-instance v1, Lcom/dropbox/android/notifications/w;

    invoke-direct {v1}, Lcom/dropbox/android/notifications/w;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->a:Lcom/dropbox/sync/android/aO;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/dropbox/android/notifications/l;->a:Lcom/dropbox/sync/android/aO;

    iget-object v1, v1, Lcom/dropbox/sync/android/aO;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 303
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/l;->notifyDataSetChanged()V

    .line 304
    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 358
    const v2, 0x7f070121

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 359
    const v2, 0x7f070122

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 360
    iget v2, p0, Lcom/dropbox/android/notifications/l;->b:I

    .line 361
    iget-boolean v5, p0, Lcom/dropbox/android/notifications/l;->c:Z

    if-eqz v5, :cond_0

    .line 362
    add-int/lit8 v2, v2, 0x1

    .line 364
    :cond_0
    if-nez p1, :cond_1

    .line 365
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 366
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 375
    :goto_0
    const v2, 0x7f070123

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 376
    iget-object v3, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_3

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 377
    return-void

    .line 367
    :cond_1
    if-ne p1, v2, :cond_2

    .line 368
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 369
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 371
    :cond_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 372
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 376
    goto :goto_1
.end method

.method private a(ILdbxyzptlk/db231222/A/a;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 380
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->i:Ldbxyzptlk/db231222/A/b;

    invoke-virtual {p2, v0, p3}, Ldbxyzptlk/db231222/A/a;->a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    invoke-direct {p0, p1, p3}, Lcom/dropbox/android/notifications/l;->a(ILandroid/view/View;)V

    .line 383
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->k:Ldbxyzptlk/db231222/A/b;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Ldbxyzptlk/db231222/A/a;->a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 394
    if-nez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p3, v0}, Landroid/view/View;->setClickable(Z)V

    .line 400
    invoke-virtual {p3, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 401
    return-void

    :cond_0
    move v0, v1

    .line 394
    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/dropbox/android/notifications/NotificationKey;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 791
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->h:Lcom/dropbox/android/notifications/v;

    invoke-interface {v0, p2}, Lcom/dropbox/android/notifications/v;->b(Lcom/dropbox/android/notifications/NotificationKey;)Z

    move-result v4

    .line 792
    const v0, 0x7f070120

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 797
    const/4 v0, 0x3

    new-array v6, v0, [I

    fill-array-data v6, :array_0

    .line 803
    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_2

    aget v0, v6, v3

    .line 804
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 805
    if-eqz v0, :cond_0

    .line 806
    if-nez v4, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 803
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 806
    goto :goto_1

    .line 809
    :cond_2
    if-eqz v4, :cond_3

    :goto_2
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 810
    return-void

    .line 809
    :cond_3
    const/16 v2, 0x8

    goto :goto_2

    .line 797
    nop

    :array_0
    .array-data 4
        0x7f07012a
        0x7f07011e
        0x7f07011f
    .end array-data
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/l;Ldbxyzptlk/db231222/A/c;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/notifications/l;->a(Ldbxyzptlk/db231222/A/c;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/l;Ldbxyzptlk/db231222/A/d;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/notifications/l;->a(Ldbxyzptlk/db231222/A/d;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/l;Ldbxyzptlk/db231222/A/e;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/notifications/l;->a(Ldbxyzptlk/db231222/A/e;Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/dropbox/sync/android/DbxNotificationHeader;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v0

    if-nez v0, :cond_0

    .line 442
    const v0, 0x7f020185

    .line 446
    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 447
    return-void

    .line 444
    :cond_0
    const v0, 0x7f020184

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/A/c;Landroid/view/View;)V
    .locals 15

    .prologue
    .line 680
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 681
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v6

    .line 683
    const v1, 0x7f07011c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 684
    const v2, 0x7f07011d

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 688
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 689
    const v3, 0x7f0d026d

    .line 690
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->b()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x1

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->d()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual {v5, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 717
    :goto_0
    iget-object v4, p0, Lcom/dropbox/android/notifications/l;->l:Ldbxyzptlk/db231222/s/d;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v7

    iget-object v4, p0, Lcom/dropbox/android/notifications/l;->l:Ldbxyzptlk/db231222/s/d;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v9

    add-long/2addr v7, v9

    .line 718
    iget-object v4, p0, Lcom/dropbox/android/notifications/l;->l:Ldbxyzptlk/db231222/s/d;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->b()I

    move-result v4

    int-to-long v11, v4

    const-wide/32 v13, 0x40000000

    mul-long/2addr v11, v13

    sub-long/2addr v9, v11

    .line 719
    sub-long v11, v9, v7

    .line 720
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->i()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 721
    const v4, 0x7f0d026f

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 747
    :goto_1
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 748
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 750
    const v1, 0x7f07011e

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 751
    new-instance v2, Lcom/dropbox/android/notifications/s;

    move-object/from16 v0, p1

    invoke-direct {v2, p0, v6, v0}, Lcom/dropbox/android/notifications/s;-><init>(Lcom/dropbox/android/notifications/l;Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/A/c;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 758
    const v1, 0x7f07011f

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 759
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->h()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 760
    new-instance v2, Lcom/dropbox/android/notifications/t;

    invoke-direct {v2, p0, v6}, Lcom/dropbox/android/notifications/t;-><init>(Lcom/dropbox/android/notifications/l;Lcom/dropbox/android/notifications/NotificationKey;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 766
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 770
    :goto_2
    return-void

    .line 695
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->g()I

    move-result v3

    if-eqz v3, :cond_1

    .line 696
    const v4, 0x7f0f0023

    .line 697
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->g()I

    move-result v3

    .line 708
    :goto_3
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->b()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->d()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v7, v4, v3, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 698
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->f()I

    move-result v3

    if-eqz v3, :cond_2

    .line 699
    const v4, 0x7f0f0024

    .line 700
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->f()I

    move-result v3

    goto :goto_3

    .line 701
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->e()I

    move-result v3

    if-eqz v3, :cond_3

    .line 702
    const v4, 0x7f0f0025

    .line 703
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->e()I

    move-result v3

    goto :goto_3

    .line 705
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 722
    :cond_4
    const-wide/16 v13, 0x0

    cmp-long v4, v11, v13

    if-gez v4, :cond_7

    .line 724
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->g()I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_5

    .line 725
    const v4, 0x7f0d0273

    .line 731
    :goto_4
    neg-long v7, v11

    .line 732
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const/4 v12, 0x1

    invoke-static {v11, v7, v8, v12}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v10

    invoke-virtual {v5, v4, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 726
    :cond_5
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 727
    const v4, 0x7f0d0274

    goto :goto_4

    .line 729
    :cond_6
    const v4, 0x7f0d0272

    goto :goto_4

    .line 735
    :cond_7
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->g()I

    move-result v4

    const/4 v11, 0x1

    if-ne v4, v11, :cond_8

    .line 736
    const v4, 0x7f0d0270

    .line 742
    :goto_5
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const/4 v14, 0x1

    invoke-static {v13, v7, v8, v14}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v11, v12

    const/4 v7, 0x1

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/4 v12, 0x1

    invoke-static {v8, v9, v10, v12}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v11, v7

    invoke-virtual {v5, v4, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 737
    :cond_8
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 738
    const v4, 0x7f0d0271

    goto :goto_5

    .line 740
    :cond_9
    const v4, 0x7f0d026e

    goto :goto_5

    .line 768
    :cond_a
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method private a(Ldbxyzptlk/db231222/A/d;Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 583
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;Landroid/view/View;)V

    .line 584
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v6

    .line 586
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 589
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 590
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 592
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->d()I

    move-result v1

    .line 593
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->h()Ljava/lang/Integer;

    move-result-object v2

    .line 595
    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 598
    const v1, 0x7f0d02f8

    move v2, v3

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    .line 622
    :goto_0
    new-array v10, v10, [Ljava/lang/Object;

    aput-object v8, v10, v3

    aput-object v9, v10, v4

    invoke-virtual {v7, v0, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 623
    const v0, 0x7f070125

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 624
    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    const v0, 0x7f070129

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 627
    const v0, 0x7f07012b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 628
    if-eqz v2, :cond_5

    move v4, v3

    :goto_1
    invoke-virtual {v7, v4}, Landroid/view/View;->setVisibility(I)V

    .line 629
    if-eqz v2, :cond_6

    move v4, v5

    :goto_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 631
    if-eqz v2, :cond_7

    .line 632
    const v0, 0x7f07012a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 633
    new-instance v2, Lcom/dropbox/android/notifications/q;

    invoke-direct {v2, p0, v6, p1}, Lcom/dropbox/android/notifications/q;-><init>(Lcom/dropbox/android/notifications/l;Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/A/d;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 640
    const v0, 0x7f07011f

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 641
    new-instance v2, Lcom/dropbox/android/notifications/r;

    invoke-direct {v2, p0, v6, p1}, Lcom/dropbox/android/notifications/r;-><init>(Lcom/dropbox/android/notifications/l;Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/A/d;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 656
    :goto_3
    const v0, 0x7f070128

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 657
    if-nez v1, :cond_8

    .line 658
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 664
    :goto_4
    invoke-direct {p0, p2, v6}, Lcom/dropbox/android/notifications/l;->a(Landroid/view/View;Lcom/dropbox/android/notifications/NotificationKey;)V

    .line 665
    return-void

    .line 601
    :cond_0
    if-nez v1, :cond_1

    .line 602
    const v1, 0x7f0d02f7

    move v2, v4

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    .line 604
    goto :goto_0

    .line 605
    :cond_1
    if-ne v1, v4, :cond_2

    .line 607
    const v1, 0x7f0d02f9

    .line 609
    const v0, 0x7f020188

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move v2, v3

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    goto/16 :goto_0

    .line 610
    :cond_2
    if-ne v1, v10, :cond_3

    .line 611
    const v1, 0x7f0d02fa

    .line 613
    const v0, 0x7f020187

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move v2, v3

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    goto/16 :goto_0

    .line 614
    :cond_3
    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 615
    const v1, 0x7f0d02fb

    move v2, v3

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    .line 617
    goto/16 :goto_0

    .line 619
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_5
    move v4, v5

    .line 628
    goto :goto_1

    :cond_6
    move v4, v3

    .line 629
    goto :goto_2

    .line 651
    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/sync/android/DbxNotificationHeader;->d()Ljava/util/Date;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 660
    :cond_8
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 661
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method private a(Ldbxyzptlk/db231222/A/e;Landroid/view/View;)V
    .locals 15

    .prologue
    .line 453
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;Landroid/view/View;)V

    .line 455
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 457
    const v1, 0x7f0700d8

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 458
    const v2, 0x7f0700da

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 459
    const v3, 0x7f0700d9

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    .line 460
    const v4, 0x7f07005e

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 461
    const v5, 0x7f0700db

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 464
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 465
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 466
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v6

    invoke-virtual {v6}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    .line 468
    :goto_0
    const/4 v7, 0x0

    .line 470
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->h()I

    move-result v11

    const/16 v12, 0xa

    if-ne v11, v12, :cond_a

    .line 471
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->i()Ljava/lang/Integer;

    move-result-object v11

    .line 472
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    const/4 v12, 0x3

    if-eq v11, v12, :cond_2

    .line 474
    if-eqz v6, :cond_1

    .line 475
    const v6, 0x7f0d0301

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v9, 0x1

    aput-object v10, v11, v9

    invoke-virtual {v8, v6, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :goto_1
    move-object v14, v7

    move-object v7, v6

    move-object v6, v14

    .line 556
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->f()Ljava/lang/Boolean;

    move-result-object v8

    .line 557
    if-eqz v8, :cond_d

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_d

    const/4 v8, 0x1

    .line 559
    :goto_3
    if-eqz v8, :cond_e

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->e()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_e

    .line 560
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 561
    const v1, 0x7f08001a

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 562
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 563
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 564
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 565
    const/16 v1, 0x8

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 566
    iget-object v1, p0, Lcom/dropbox/android/notifications/l;->g:Ldbxyzptlk/db231222/V/y;

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ldbxyzptlk/db231222/V/y;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/V/M;

    move-result-object v1

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/V/M;->a(Landroid/widget/ImageView;)V

    .line 575
    :goto_4
    const v1, 0x7f070125

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 576
    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    const v1, 0x7f07012b

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 578
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/sync/android/DbxNotificationHeader;->d()Ljava/util/Date;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 580
    return-void

    .line 466
    :cond_0
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 479
    :cond_1
    const v6, 0x7f0d0300

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v9, 0x1

    aput-object v10, v11, v9

    invoke-virtual {v8, v6, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 484
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->j()I

    move-result v11

    .line 485
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->k()I

    move-result v12

    .line 486
    add-int v13, v11, v12

    .line 487
    if-eqz v6, :cond_6

    .line 488
    if-lez v11, :cond_3

    if-lez v12, :cond_3

    .line 490
    const v6, 0x7f0f002e

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v9, v10, v11

    const/4 v9, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v9

    invoke-virtual {v8, v6, v13, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 493
    :cond_3
    if-lez v11, :cond_4

    if-nez v12, :cond_4

    .line 495
    const v6, 0x7f0f002a

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v10, v12

    const/4 v9, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v9

    invoke-virtual {v8, v6, v11, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 498
    :cond_4
    if-nez v11, :cond_5

    if-lez v12, :cond_5

    .line 500
    const v6, 0x7f0f002c

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v9, v10, v11

    const/4 v9, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v9

    invoke-virtual {v8, v6, v12, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 505
    :cond_5
    const v6, 0x7f0d02ff

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v9, 0x1

    aput-object v10, v11, v9

    invoke-virtual {v8, v6, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 510
    :cond_6
    if-lez v11, :cond_7

    if-lez v12, :cond_7

    .line 512
    const v6, 0x7f0f002d

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v9, v10, v11

    const/4 v9, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v9

    invoke-virtual {v8, v6, v13, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 515
    :cond_7
    if-lez v11, :cond_8

    if-nez v12, :cond_8

    .line 517
    const v6, 0x7f0f0029

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v10, v12

    const/4 v9, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v9

    invoke-virtual {v8, v6, v11, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 520
    :cond_8
    if-nez v11, :cond_9

    if-lez v12, :cond_9

    .line 522
    const v6, 0x7f0f002b

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v9, v10, v11

    const/4 v9, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v9

    invoke-virtual {v8, v6, v12, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 527
    :cond_9
    const v6, 0x7f0d02fe

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v9, 0x1

    aput-object v10, v11, v9

    invoke-virtual {v8, v6, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 534
    :cond_a
    if-eqz v6, :cond_b

    .line 535
    const v6, 0x7f0d02ff

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v9, 0x1

    aput-object v10, v11, v9

    invoke-virtual {v8, v6, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 546
    :goto_5
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->h()I

    move-result v8

    .line 547
    invoke-virtual/range {p1 .. p1}, Ldbxyzptlk/db231222/A/e;->g()Ljava/lang/String;

    move-result-object v9

    .line 548
    const/4 v10, 0x6

    if-ne v8, v10, :cond_c

    .line 549
    const-string v7, "folder"

    move-object v14, v7

    move-object v7, v6

    move-object v6, v14

    goto/16 :goto_2

    .line 539
    :cond_b
    const v6, 0x7f0d02fe

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v9, 0x1

    aput-object v10, v11, v9

    invoke-virtual {v8, v6, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    .line 550
    :cond_c
    const/4 v10, 0x5

    if-ne v8, v10, :cond_f

    if-eqz v9, :cond_f

    .line 552
    invoke-static {v9}, Lcom/dropbox/android/util/ab;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v14, v7

    move-object v7, v6

    move-object v6, v14

    goto/16 :goto_2

    .line 557
    :cond_d
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 568
    :cond_e
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 569
    const/4 v1, 0x4

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 570
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 571
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 572
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v5, v2, v6}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_f
    move-object v14, v7

    move-object v7, v6

    move-object v6, v14

    goto/16 :goto_2
.end method

.method private b(Lcom/dropbox/android/notifications/NotificationKey;)Landroid/view/View;
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 266
    iget-object v1, p0, Lcom/dropbox/android/notifications/l;->f:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v2

    move v1, v0

    .line 267
    :goto_0
    if-gt v1, v2, :cond_1

    .line 268
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/A/a;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/NotificationKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 273
    :goto_1
    return-object v0

    .line 267
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 273
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/notifications/NotificationKey;)V
    .locals 1

    .prologue
    .line 783
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 784
    invoke-direct {p0, p1}, Lcom/dropbox/android/notifications/l;->b(Lcom/dropbox/android/notifications/NotificationKey;)Landroid/view/View;

    move-result-object v0

    .line 785
    if-eqz v0, :cond_0

    .line 786
    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/notifications/l;->a(Landroid/view/View;Lcom/dropbox/android/notifications/NotificationKey;)V

    .line 788
    :cond_0
    return-void
.end method

.method final a(Lcom/dropbox/sync/android/aO;I)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/dropbox/android/notifications/l;->a:Lcom/dropbox/sync/android/aO;

    .line 287
    iput p2, p0, Lcom/dropbox/android/notifications/l;->b:I

    .line 288
    invoke-direct {p0}, Lcom/dropbox/android/notifications/l;->a()V

    .line 289
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/s/d;)V
    .locals 4

    .prologue
    .line 307
    iput-object p1, p0, Lcom/dropbox/android/notifications/l;->l:Ldbxyzptlk/db231222/s/d;

    .line 308
    if-eqz p1, :cond_0

    .line 309
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/notifications/l;->c:Z

    .line 310
    invoke-direct {p0}, Lcom/dropbox/android/notifications/l;->a()V

    .line 312
    :cond_0
    return-void

    .line 309
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x1

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 326
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;

    sget-object v1, Lcom/dropbox/android/notifications/l;->j:Lcom/dropbox/android/notifications/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/A/a;->a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;

    .line 332
    if-nez p2, :cond_0

    .line 333
    invoke-direct {p0, v0, p3}, Lcom/dropbox/android/notifications/l;->a(Ldbxyzptlk/db231222/A/a;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 335
    :cond_0
    invoke-direct {p0, p1, v0, p2}, Lcom/dropbox/android/notifications/l;->a(ILdbxyzptlk/db231222/A/a;Landroid/view/View;)V

    .line 336
    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x4

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/dropbox/android/notifications/l;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;

    iget-object v1, p0, Lcom/dropbox/android/notifications/l;->k:Ldbxyzptlk/db231222/A/b;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/A/a;->a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    return-void
.end method
