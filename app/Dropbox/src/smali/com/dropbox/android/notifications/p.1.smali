.class final Lcom/dropbox/android/notifications/p;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/notifications/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/notifications/u",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/notifications/l;


# direct methods
.method constructor <init>(Lcom/dropbox/android/notifications/l;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/dropbox/android/notifications/p;->a:Lcom/dropbox/android/notifications/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/notifications/w;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shouldn\'t be clickable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 254
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shouldn\'t be clickable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 210
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->d()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 211
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/notifications/p;->a:Lcom/dropbox/android/notifications/l;

    invoke-static {v0}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/android/notifications/l;)Lcom/dropbox/android/notifications/v;

    move-result-object v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/d;->b()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/dropbox/android/notifications/v;->b(Lcom/dropbox/android/notifications/NotificationKey;J)V

    .line 216
    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    .line 218
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shouldn\'t be clickable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 228
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 229
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shouldn\'t be clickable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 235
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/notifications/p;->a:Lcom/dropbox/android/notifications/l;

    invoke-static {v0}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/android/notifications/l;)Lcom/dropbox/android/notifications/v;

    move-result-object v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/notifications/NotificationKey;->a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/dropbox/android/notifications/v;->a(Lcom/dropbox/android/notifications/NotificationKey;Ljava/lang/String;)V

    .line 240
    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/dropbox/android/notifications/w;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/p;->a(Lcom/dropbox/android/notifications/w;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/p;->a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/p;->a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/p;->a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
