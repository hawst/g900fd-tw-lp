.class public Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 423
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;
    .locals 3

    .prologue
    .line 428
    new-instance v0, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;-><init>()V

    .line 429
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 430
    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 432
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 439
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 440
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 441
    const v0, 0x7f0d0015

    new-instance v2, Lcom/dropbox/android/notifications/H;

    invoke-direct {v2, p0}, Lcom/dropbox/android/notifications/H;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 447
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 448
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
