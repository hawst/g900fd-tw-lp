.class public Lcom/dropbox/android/notifications/I;
.super Landroid/support/v4/content/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/content/a",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final g:Lcom/dropbox/sync/android/aJ;

.field private final h:Lcom/dropbox/android/notifications/K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/notifications/K",
            "<TT;>;"
        }
    .end annotation
.end field

.field private i:Lcom/dropbox/sync/android/aQ;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/dropbox/android/notifications/I;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/notifications/I;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/sync/android/aJ;Lcom/dropbox/android/notifications/K;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dropbox/sync/android/aJ;",
            "Lcom/dropbox/android/notifications/K",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/support/v4/content/a;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Lcom/dropbox/android/notifications/J;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/J;-><init>(Lcom/dropbox/android/notifications/I;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/I;->i:Lcom/dropbox/sync/android/aQ;

    .line 32
    iput-object p2, p0, Lcom/dropbox/android/notifications/I;->g:Lcom/dropbox/sync/android/aJ;

    .line 33
    iput-object p3, p0, Lcom/dropbox/android/notifications/I;->h:Lcom/dropbox/android/notifications/K;

    .line 34
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/I;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/I;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-super {p0, p1}, Landroid/support/v4/content/a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/notifications/I;->g:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aJ;->d()Lcom/dropbox/sync/android/aO;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 86
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/notifications/I;->h:Lcom/dropbox/android/notifications/K;

    invoke-interface {v1, v0}, Lcom/dropbox/android/notifications/K;->b(Lcom/dropbox/sync/android/aO;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    new-instance v0, Lcom/dropbox/sync/android/aO;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/dropbox/sync/android/aO;-><init>(Ljava/util/List;Z)V

    goto :goto_0

    .line 81
    :catch_1
    move-exception v0

    .line 82
    sget-object v1, Lcom/dropbox/android/notifications/I;->f:Ljava/lang/String;

    const-string v2, "Failed to listNotifications"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to listNotifications:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/notifications/I;->g:Lcom/dropbox/sync/android/aJ;

    iget-object v1, p0, Lcom/dropbox/android/notifications/I;->i:Lcom/dropbox/sync/android/aQ;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aJ;->a(Lcom/dropbox/sync/android/aQ;)V

    .line 47
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/I;->p()V

    .line 48
    return-void
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/notifications/I;->g:Lcom/dropbox/sync/android/aJ;

    iget-object v1, p0, Lcom/dropbox/android/notifications/I;->i:Lcom/dropbox/sync/android/aQ;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aJ;->b(Lcom/dropbox/sync/android/aQ;)V

    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/I;->b()Z

    .line 54
    return-void
.end method

.method protected final h()V
    .locals 0

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/I;->g()V

    .line 59
    return-void
.end method
