.class public final Lcom/dropbox/android/notifications/NotificationKey;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/dropbox/android/notifications/k;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/k;-><init>()V

    sput-object v0, Lcom/dropbox/android/notifications/NotificationKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    .line 26
    iput-object p2, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    .line 27
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    .line 50
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dropbox/android/notifications/k;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/dropbox/android/notifications/NotificationKey;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Lcom/dropbox/android/notifications/NotificationKey;
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 34
    new-instance v0, Lcom/dropbox/android/notifications/NotificationKey;

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->b()I

    move-result v1

    invoke-virtual {p0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/notifications/NotificationKey;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    if-ne p0, p1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 86
    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_3
    check-cast p1, Lcom/dropbox/android/notifications/NotificationKey;

    .line 92
    iget-object v2, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_4
    iget v2, p0, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    iget v3, p1, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 96
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 73
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 76
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    add-int/2addr v0, v1

    .line 77
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/dropbox/android/notifications/NotificationKey;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationKey;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    return-void
.end method
