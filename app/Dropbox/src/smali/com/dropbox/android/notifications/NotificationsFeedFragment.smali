.class public Lcom/dropbox/android/notifications/NotificationsFeedFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/sync/android/aJ;

.field private c:Lcom/dropbox/android/service/a;

.field private d:Lcom/dropbox/android/notifications/l;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ListView;

.field private g:Landroid/widget/ProgressBar;

.field private h:Lcom/dropbox/android/notifications/a;

.field private i:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ldbxyzptlk/db231222/l/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/l",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ldbxyzptlk/db231222/l/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/f",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/dropbox/android/notifications/v;

.field private final n:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/dropbox/android/notifications/c;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 81
    new-instance v0, Lcom/dropbox/android/notifications/a;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/a;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h:Lcom/dropbox/android/notifications/a;

    .line 84
    new-instance v0, Lcom/dropbox/android/notifications/A;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/A;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->j:Ldbxyzptlk/db231222/l/l;

    .line 136
    new-instance v0, Lcom/dropbox/android/notifications/B;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/B;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->n:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 181
    new-instance v0, Lcom/dropbox/android/notifications/C;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/C;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->o:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 423
    return-void
.end method

.method public static a()Lcom/dropbox/android/notifications/NotificationsFeedFragment;
    .locals 2

    .prologue
    .line 208
    new-instance v0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;-><init>()V

    .line 209
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->setArguments(Landroid/os/Bundle;)V

    .line 210
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/notifications/l;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->d:Lcom/dropbox/android/notifications/l;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/notifications/NotificationKey;J)V
    .locals 7

    .prologue
    .line 130
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h:Lcom/dropbox/android/notifications/a;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/a;->a(Lcom/dropbox/android/notifications/NotificationKey;)V

    .line 131
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 132
    iget-object v6, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->i:Ldbxyzptlk/db231222/l/k;

    new-instance v0, Ldbxyzptlk/db231222/l/o;

    iget-object v4, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v5

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/l/o;-><init>(Lcom/dropbox/android/notifications/NotificationKey;JLcom/dropbox/sync/android/aJ;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v6, v0}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    .line 134
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Lcom/dropbox/android/notifications/NotificationKey;J)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a(Lcom/dropbox/android/notifications/NotificationKey;J)V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/r/d;)V
    .locals 5

    .prologue
    .line 218
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aJ;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aJ;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b:Lcom/dropbox/sync/android/aJ;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->c:Lcom/dropbox/android/service/a;

    .line 224
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->A()Lcom/dropbox/android/notifications/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/y;->a()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->i:Ldbxyzptlk/db231222/l/k;

    .line 225
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->A()Lcom/dropbox/android/notifications/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/y;->b()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->k:Ldbxyzptlk/db231222/l/k;

    .line 226
    new-instance v0, Lcom/dropbox/android/notifications/D;

    invoke-direct {v0, p0}, Lcom/dropbox/android/notifications/D;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V

    .line 233
    new-instance v1, Lcom/dropbox/android/notifications/E;

    invoke-direct {v1, p0}, Lcom/dropbox/android/notifications/E;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)V

    .line 239
    new-instance v2, Ldbxyzptlk/db231222/l/f;

    iget-object v3, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->k:Ldbxyzptlk/db231222/l/k;

    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v2, v3, v0, v1, v4}, Ldbxyzptlk/db231222/l/f;-><init>(Ldbxyzptlk/db231222/l/k;Ldbxyzptlk/db231222/l/h;Ldbxyzptlk/db231222/l/g;Landroid/support/v4/app/FragmentManager;)V

    iput-object v2, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->l:Ldbxyzptlk/db231222/l/f;

    .line 243
    new-instance v0, Lcom/dropbox/android/notifications/F;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/notifications/F;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Ldbxyzptlk/db231222/r/d;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->m:Lcom/dropbox/android/notifications/v;

    .line 320
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/sync/android/aJ;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b:Lcom/dropbox/sync/android/aJ;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/notifications/a;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h:Lcom/dropbox/android/notifications/a;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->e:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->g:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Lcom/dropbox/android/service/a;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->c:Lcom/dropbox/android/service/a;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/l/k;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->i:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/l/k;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->k:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/notifications/NotificationsFeedFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 387
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 389
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 390
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->b:Lcom/dropbox/sync/android/aJ;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 391
    if-eqz p1, :cond_1

    const-string v0, "SIS_KEY_DISPLAY_OVERRIDE_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    new-instance v0, Lcom/dropbox/android/notifications/a;

    const-string v2, "SIS_KEY_DISPLAY_OVERRIDE_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/dropbox/android/notifications/a;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h:Lcom/dropbox/android/notifications/a;

    .line 399
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 401
    new-instance v2, Lcom/dropbox/android/notifications/l;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->B()Lcom/dropbox/android/util/aE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/aE;->a()Ldbxyzptlk/db231222/V/y;

    move-result-object v1

    iget-object v3, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->m:Lcom/dropbox/android/notifications/v;

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/dropbox/android/notifications/l;-><init>(Landroid/view/LayoutInflater;Ldbxyzptlk/db231222/V/y;Landroid/widget/ListView;Lcom/dropbox/android/notifications/v;)V

    iput-object v2, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->d:Lcom/dropbox/android/notifications/l;

    .line 404
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->d:Lcom/dropbox/android/notifications/l;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 405
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->d:Lcom/dropbox/android/notifications/l;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 407
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->n:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v5, v6, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 408
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->o:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v6, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 410
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 413
    :cond_0
    return-void

    .line 396
    :cond_1
    new-instance v0, Lcom/dropbox/android/notifications/a;

    invoke-direct {v0}, Lcom/dropbox/android/notifications/a;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h:Lcom/dropbox/android/notifications/a;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 324
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 326
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_0

    .line 328
    invoke-direct {p0, v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 330
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 372
    const v0, 0x7f030060

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 374
    const v0, 0x7f070118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->e:Landroid/view/View;

    .line 375
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->e:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 377
    const v0, 0x7f070119

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f:Landroid/widget/ListView;

    .line 378
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->f:Landroid/widget/ListView;

    const v2, 0x7f07011a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 380
    const v0, 0x7f070117

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->g:Landroid/widget/ProgressBar;

    .line 382
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 364
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onPause()V

    .line 365
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->A()Lcom/dropbox/android/notifications/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/y;->d()V

    .line 366
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->i:Ldbxyzptlk/db231222/l/k;

    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->j:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->b(Ldbxyzptlk/db231222/l/l;)V

    .line 367
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->l:Ldbxyzptlk/db231222/l/f;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/l/f;->a()V

    .line 368
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 334
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onResume()V

    .line 336
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 337
    invoke-virtual {p0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 338
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->A()Lcom/dropbox/android/notifications/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/notifications/y;->c()V

    .line 340
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->i:Ldbxyzptlk/db231222/l/k;

    iget-object v2, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->j:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/l;)V

    .line 343
    new-instance v0, Lcom/dropbox/android/notifications/G;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/notifications/G;-><init>(Lcom/dropbox/android/notifications/NotificationsFeedFragment;Landroid/app/Activity;)V

    .line 359
    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->l:Ldbxyzptlk/db231222/l/f;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/l/f;->a(Ldbxyzptlk/db231222/l/i;)V

    .line 360
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 417
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 418
    iget-object v0, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->d:Lcom/dropbox/android/notifications/l;

    if-eqz v0, :cond_0

    .line 419
    const-string v0, "SIS_KEY_DISPLAY_OVERRIDE_STATE"

    iget-object v1, p0, Lcom/dropbox/android/notifications/NotificationsFeedFragment;->h:Lcom/dropbox/android/notifications/a;

    invoke-virtual {v1}, Lcom/dropbox/android/notifications/a;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 421
    :cond_0
    return-void
.end method
