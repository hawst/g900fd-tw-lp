.class final Lcom/dropbox/android/notifications/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/notifications/NotificationKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/notifications/NotificationKey;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/dropbox/android/notifications/NotificationKey;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/notifications/NotificationKey;-><init>(Landroid/os/Parcel;Lcom/dropbox/android/notifications/k;)V

    return-object v0
.end method

.method public final a(I)[Lcom/dropbox/android/notifications/NotificationKey;
    .locals 1

    .prologue
    .line 67
    new-array v0, p1, [Lcom/dropbox/android/notifications/NotificationKey;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/dropbox/android/notifications/k;->a(Landroid/os/Parcel;)Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/dropbox/android/notifications/k;->a(I)[Lcom/dropbox/android/notifications/NotificationKey;

    move-result-object v0

    return-object v0
.end method
