.class public final Lcom/dropbox/android/notifications/M;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ldbxyzptlk/db231222/A/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/android/notifications/N;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/notifications/N;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/dropbox/android/notifications/M;->a:Lcom/dropbox/android/notifications/N;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/A/a;Ldbxyzptlk/db231222/A/a;)I
    .locals 11

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 38
    invoke-virtual {p1}, Ldbxyzptlk/db231222/A/a;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v1

    .line 39
    invoke-virtual {p2}, Ldbxyzptlk/db231222/A/a;->a()Lcom/dropbox/sync/android/DbxNotificationHeader;

    move-result-object v0

    .line 43
    iget-object v5, p0, Lcom/dropbox/android/notifications/M;->a:Lcom/dropbox/android/notifications/N;

    invoke-interface {v5, p1}, Lcom/dropbox/android/notifications/N;->a(Ldbxyzptlk/db231222/A/a;)Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    move-result-object v7

    .line 44
    iget-object v5, p0, Lcom/dropbox/android/notifications/M;->a:Lcom/dropbox/android/notifications/N;

    invoke-interface {v5, p2}, Lcom/dropbox/android/notifications/N;->a(Ldbxyzptlk/db231222/A/a;)Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;

    move-result-object v8

    .line 45
    if-eqz v7, :cond_1

    move v6, v2

    .line 46
    :goto_0
    if-eqz v8, :cond_2

    move v5, v2

    .line 49
    :goto_1
    if-eqz v6, :cond_3

    if-nez v5, :cond_3

    move v2, v4

    .line 102
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v6, v3

    .line 45
    goto :goto_0

    :cond_2
    move v5, v3

    .line 46
    goto :goto_1

    .line 51
    :cond_3
    if-nez v6, :cond_4

    if-nez v5, :cond_0

    .line 53
    :cond_4
    if-eqz v6, :cond_6

    if-eqz v5, :cond_6

    .line 54
    invoke-virtual {v7}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b()I

    move-result v9

    invoke-virtual {v8}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b()I

    move-result v10

    if-le v9, v10, :cond_5

    move v2, v4

    .line 55
    goto :goto_2

    .line 56
    :cond_5
    invoke-virtual {v7}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b()I

    move-result v9

    invoke-virtual {v8}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->b()I

    move-result v10

    if-lt v9, v10, :cond_0

    .line 62
    :cond_6
    if-eqz v6, :cond_e

    .line 63
    invoke-virtual {v7}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->a()Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    move-result-object v1

    move-object v6, v1

    .line 65
    :goto_3
    if-eqz v5, :cond_7

    .line 66
    invoke-virtual {v8}, Lcom/dropbox/android/notifications/StickyDbxNotificationHeader;->a()Lcom/dropbox/android/notifications/ParcelableDbxNotificationHeader;

    move-result-object v0

    .line 72
    :cond_7
    invoke-virtual {v6}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v1

    if-nez v1, :cond_8

    move v1, v2

    .line 73
    :goto_4
    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->e()I

    move-result v5

    if-nez v5, :cond_9

    move v5, v2

    .line 74
    :goto_5
    if-eqz v1, :cond_a

    if-nez v5, :cond_a

    move v2, v4

    .line 75
    goto :goto_2

    :cond_8
    move v1, v3

    .line 72
    goto :goto_4

    :cond_9
    move v5, v3

    .line 73
    goto :goto_5

    .line 76
    :cond_a
    if-nez v1, :cond_b

    if-nez v5, :cond_0

    .line 84
    :cond_b
    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->d()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dropbox/sync/android/DbxNotificationHeader;->d()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    .line 85
    if-eqz v1, :cond_c

    move v2, v1

    .line 86
    goto :goto_2

    .line 93
    :cond_c
    invoke-virtual {v6}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v5

    .line 94
    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxNotificationHeader;->a()J

    move-result-wide v0

    .line 95
    cmp-long v7, v5, v0

    if-lez v7, :cond_d

    move v2, v4

    .line 96
    goto :goto_2

    .line 97
    :cond_d
    cmp-long v0, v5, v0

    if-ltz v0, :cond_0

    move v2, v3

    .line 102
    goto :goto_2

    :cond_e
    move-object v6, v1

    goto :goto_3
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 17
    check-cast p1, Ldbxyzptlk/db231222/A/a;

    check-cast p2, Ldbxyzptlk/db231222/A/a;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/M;->a(Ldbxyzptlk/db231222/A/a;Ldbxyzptlk/db231222/A/a;)I

    move-result v0

    return v0
.end method
