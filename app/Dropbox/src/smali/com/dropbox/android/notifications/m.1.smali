.class final Lcom/dropbox/android/notifications/m;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/notifications/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/notifications/u",
        "<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/notifications/l;


# direct methods
.method constructor <init>(Lcom/dropbox/android/notifications/l;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/dropbox/android/notifications/m;->a:Lcom/dropbox/android/notifications/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/notifications/w;Landroid/view/View;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 124
    if-eqz p2, :cond_0

    .line 127
    const v0, 0x7f07011e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 128
    new-instance v1, Lcom/dropbox/android/notifications/n;

    invoke-direct {v1, p0}, Lcom/dropbox/android/notifications/n;-><init>(Lcom/dropbox/android/notifications/m;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v0, 0x7f070126

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 136
    new-instance v1, Lcom/dropbox/android/notifications/o;

    invoke-direct {v1, p0}, Lcom/dropbox/android/notifications/o;-><init>(Lcom/dropbox/android/notifications/m;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :cond_0
    const v0, 0x7f030063

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/c;Landroid/view/View;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 163
    if-eqz p2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/notifications/m;->a:Lcom/dropbox/android/notifications/l;

    invoke-static {v0, p1, p2}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/android/notifications/l;Ldbxyzptlk/db231222/A/c;Landroid/view/View;)V

    .line 166
    :cond_0
    const v0, 0x7f030061

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/d;Landroid/view/View;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152
    if-eqz p2, :cond_0

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/notifications/m;->a:Lcom/dropbox/android/notifications/l;

    invoke-static {v0, p1, p2}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/android/notifications/l;Ldbxyzptlk/db231222/A/d;Landroid/view/View;)V

    .line 155
    :cond_0
    const v0, 0x7f030064

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/e;Landroid/view/View;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 113
    if-eqz p2, :cond_0

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/notifications/m;->a:Lcom/dropbox/android/notifications/l;

    invoke-static {v0, p1, p2}, Lcom/dropbox/android/notifications/l;->a(Lcom/dropbox/android/notifications/l;Ldbxyzptlk/db231222/A/e;Landroid/view/View;)V

    .line 116
    :cond_0
    const v0, 0x7f030065

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/dropbox/android/notifications/w;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/m;->a(Lcom/dropbox/android/notifications/w;Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/m;->a(Ldbxyzptlk/db231222/A/c;Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/m;->a(Ldbxyzptlk/db231222/A/d;Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/notifications/m;->a(Ldbxyzptlk/db231222/A/e;Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
