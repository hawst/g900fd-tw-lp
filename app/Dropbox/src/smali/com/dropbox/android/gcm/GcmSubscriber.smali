.class public Lcom/dropbox/android/gcm/GcmSubscriber;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile b:Lcom/dropbox/android/gcm/GcmSubscriber;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ldbxyzptlk/db231222/r/e;

.field private final e:Lcom/dropbox/android/taskqueue/TaskQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/dropbox/android/taskqueue/TaskQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/dropbox/android/gcm/GcmSubscriber;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->a:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/service/G;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;-><init>(IILjava/util/List;)V

    iput-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->f:Lcom/dropbox/android/taskqueue/TaskQueue;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 69
    iput-object p1, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->d:Ldbxyzptlk/db231222/r/e;

    .line 71
    new-instance v0, Lcom/dropbox/android/taskqueue/r;

    invoke-direct {v0, v1, v2, v3, p3}, Lcom/dropbox/android/taskqueue/r;-><init>(IILjava/util/List;Lcom/dropbox/android/service/G;)V

    iput-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->e:Lcom/dropbox/android/taskqueue/TaskQueue;

    .line 73
    return-void
.end method

.method public static a()Lcom/dropbox/android/gcm/GcmSubscriber;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 64
    :cond_0
    sget-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/service/G;)Lcom/dropbox/android/gcm/GcmSubscriber;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/dropbox/android/gcm/GcmSubscriber;

    invoke-direct {v0, p0, p1, p2}, Lcom/dropbox/android/gcm/GcmSubscriber;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;Lcom/dropbox/android/service/G;)V

    sput-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    .line 57
    sget-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->b:Lcom/dropbox/android/gcm/GcmSubscriber;

    return-object v0

    .line 55
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private b(Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->e:Lcom/dropbox/android/taskqueue/TaskQueue;

    new-instance v1, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;

    iget-object v2, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    invoke-direct {v1, v2, p1, p2}, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 135
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/r/d;)V
    .locals 4

    .prologue
    .line 82
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gcm/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 86
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    sget-object v0, Lcom/dropbox/android/gcm/GcmSubscriber;->a:Ljava/lang/String;

    const-string v1, "Registering for GCM"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;)V

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {}, Lcom/dropbox/android/gcm/GcmService;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;[Ljava/lang/String;)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gcm/a;->h(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/gcm/GcmSubscriber;->b(Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 104
    const-string v1, "register_error_security"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "msg"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 105
    :catch_2
    move-exception v0

    .line 106
    const-string v1, "Package manager has died"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    const-string v0, "register_error_pm_died"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 111
    :cond_2
    throw v0
.end method

.method public final a(Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 164
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aJ;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aJ;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->f:Lcom/dropbox/android/taskqueue/TaskQueue;

    new-instance v2, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->A()Lcom/dropbox/android/notifications/y;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;-><init>(Lcom/dropbox/sync/android/aJ;Lcom/dropbox/android/notifications/y;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 170
    const-string v0, "feed_update_scheduled"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "trigger"

    invoke-virtual {v0, v1, p2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 171
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->d:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/gcm/GcmSubscriber;->b(Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V

    .line 127
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->e:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a()V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;Z)V

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gcm/a;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 151
    new-instance v0, Lcom/dropbox/android/gcm/a;

    invoke-direct {v0}, Lcom/dropbox/android/gcm/a;-><init>()V

    throw v0
.end method
