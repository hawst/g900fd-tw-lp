.class public Lcom/dropbox/android/gcm/GcmService;
.super Lcom/google/android/gcm/GCMBaseIntentService;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/dropbox/android/gcm/GcmService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/gcm/GcmService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gcm/GCMBaseIntentService;-><init>()V

    return-void
.end method

.method static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "735665981150"

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 83
    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v0, -0x3

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "too_short"

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lcom/dropbox/android/gcm/GcmService;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received GCM message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v0, "refresh"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    new-instance v0, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    .line 41
    :try_start_0
    const-string v1, "refresh"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/aj/a;

    .line 42
    const-string v1, "user"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/aj/a;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-static {}, Lcom/dropbox/android/gcm/GcmSubscriber;->a()Lcom/dropbox/android/gcm/GcmSubscriber;

    move-result-object v1

    const-string v2, "gcm"

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/gcm/GcmSubscriber;->a(Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    goto :goto_0

    .line 52
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 25
    const-string v0, "reg"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "reg_id_last"

    invoke-static {p2}, Lcom/dropbox/android/gcm/GcmService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 26
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;Z)V

    .line 27
    invoke-static {}, Lcom/dropbox/android/gcm/GcmSubscriber;->a()Lcom/dropbox/android/gcm/GcmSubscriber;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/dropbox/android/gcm/GcmSubscriber;->a(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method protected final a(Landroid/content/Context;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {}, Lcom/dropbox/android/gcm/GcmService;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 32
    const-string v0, "unreg"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "reg_id_last"

    invoke-static {p2}, Lcom/dropbox/android/gcm/GcmService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 33
    return-void
.end method

.method protected final c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 61
    const-string v0, "error"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "error_id"

    invoke-virtual {v0, v1, p2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 62
    return-void
.end method
