.class Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;
.super Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;
.source "panda.py"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ldbxyzptlk/db231222/r/d;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->a:Landroid/content/Context;

    .line 189
    iput-object p2, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->b:Ldbxyzptlk/db231222/r/d;

    .line 190
    iput-object p3, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->c:Ljava/lang/String;

    .line 191
    return-void
.end method

.method private static a(Ldbxyzptlk/db231222/r/d;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gcm-subscribe-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->b:Ldbxyzptlk/db231222/r/d;

    invoke-static {v0}, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->a(Ldbxyzptlk/db231222/r/d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 3

    .prologue
    .line 199
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->g(Ljava/lang/String;)Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->a:Landroid/content/Context;

    const-wide/32 v1, 0x48190800

    invoke-static {v0, v1, v2}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;J)V

    .line 203
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;Z)V

    .line 204
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    .line 219
    :goto_0
    return-object v0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 207
    :catch_1
    move-exception v0

    .line 208
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 209
    :catch_2
    move-exception v0

    .line 210
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x1f4

    if-ge v1, v2, :cond_0

    .line 212
    const-string v1, "subscribe_error"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "err"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/i;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 213
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 215
    :cond_0
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0

    .line 217
    :catch_3
    move-exception v0

    .line 218
    const-string v1, "subscribe_error"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "err"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 219
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    goto :goto_0
.end method

.method public final h_()I
    .locals 1

    .prologue
    .line 225
    const/16 v0, 0xa

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/dropbox/android/gcm/GcmSubscriber$SubscribeTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
