.class Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;
.super Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/sync/android/aJ;

.field private final b:Lcom/dropbox/android/notifications/y;


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/aJ;Lcom/dropbox/android/notifications/y;)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;-><init>()V

    .line 255
    iput-object p1, p0, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;->a:Lcom/dropbox/sync/android/aJ;

    .line 256
    iput-object p2, p0, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;->b:Lcom/dropbox/android/notifications/y;

    .line 257
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    const-string v0, "gcm-update-feed"

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStatusPath() doesn\'t make sense for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 5

    .prologue
    .line 261
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 264
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;->a:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aJ;->f()V

    .line 265
    iget-object v0, p0, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;->a:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aJ;->e()Ljava/util/List;

    move-result-object v0

    .line 267
    const-string v1, "feed_update_received"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "num"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 269
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/A/a;
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 271
    :try_start_1
    iget-object v2, p0, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;->b:Lcom/dropbox/android/notifications/y;

    invoke-virtual {v2, v0}, Lcom/dropbox/android/notifications/y;->a(Ldbxyzptlk/db231222/A/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    :try_start_2
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 274
    const-string v2, "feed_update_item_error"

    invoke-static {v2}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "err"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_2
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 278
    :catch_1
    move-exception v0

    .line 279
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    :goto_1
    return-object v0

    .line 277
    :cond_0
    :try_start_3
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;
    :try_end_3
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/dropbox/android/gcm/GcmSubscriber$UpdateFeedTask;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
