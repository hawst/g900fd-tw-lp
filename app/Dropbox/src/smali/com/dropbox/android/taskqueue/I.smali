.class public final enum Lcom/dropbox/android/taskqueue/I;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/taskqueue/I;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/taskqueue/I;

.field public static final enum b:Lcom/dropbox/android/taskqueue/I;

.field public static final enum c:Lcom/dropbox/android/taskqueue/I;

.field private static final synthetic e:[Lcom/dropbox/android/taskqueue/I;


# instance fields
.field private final d:Lcom/dropbox/android/taskqueue/TaskQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 121
    new-instance v0, Lcom/dropbox/android/taskqueue/I;

    const-string v1, "GALLERY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/dropbox/android/taskqueue/I;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    .line 122
    new-instance v0, Lcom/dropbox/android/taskqueue/I;

    const-string v1, "THUMB"

    invoke-direct {v0, v1, v5, v7, v3}, Lcom/dropbox/android/taskqueue/I;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    .line 123
    new-instance v0, Lcom/dropbox/android/taskqueue/I;

    const-string v1, "THUMB_GALLERY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/dropbox/android/taskqueue/I;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/dropbox/android/taskqueue/I;->c:Lcom/dropbox/android/taskqueue/I;

    .line 120
    new-array v0, v7, [Lcom/dropbox/android/taskqueue/I;

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/taskqueue/I;->c:Lcom/dropbox/android/taskqueue/I;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/taskqueue/I;->e:[Lcom/dropbox/android/taskqueue/I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 128
    new-instance v0, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p4, v1}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;-><init>(IILjava/util/List;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/I;->d:Lcom/dropbox/android/taskqueue/TaskQueue;

    .line 129
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/I;->d:Lcom/dropbox/android/taskqueue/TaskQueue;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/I;
    .locals 1

    .prologue
    .line 120
    const-class v0, Lcom/dropbox/android/taskqueue/I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/I;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/taskqueue/I;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/dropbox/android/taskqueue/I;->e:[Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {v0}, [Lcom/dropbox/android/taskqueue/I;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/taskqueue/I;

    return-object v0
.end method
