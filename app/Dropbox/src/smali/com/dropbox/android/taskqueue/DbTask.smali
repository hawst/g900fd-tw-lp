.class public abstract Lcom/dropbox/android/taskqueue/DbTask;
.super Lcom/dropbox/android/taskqueue/u;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/taskqueue/u;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/dropbox/android/taskqueue/DbTask;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/dropbox/android/util/analytics/m;


# instance fields
.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/dropbox/android/taskqueue/f;

    invoke-direct {v0}, Lcom/dropbox/android/taskqueue/f;-><init>()V

    sput-object v0, Lcom/dropbox/android/taskqueue/DbTask;->a:Lcom/dropbox/android/util/analytics/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/u;-><init>()V

    .line 89
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/DbTask;->b:J

    .line 21
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DbTask;->g()Lcom/dropbox/android/taskqueue/v;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 22
    return-void
.end method

.method private g()Lcom/dropbox/android/taskqueue/v;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/dropbox/android/taskqueue/g;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/g;-><init>(Lcom/dropbox/android/taskqueue/DbTask;)V

    return-object v0
.end method

.method static synthetic o()Lcom/dropbox/android/util/analytics/m;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/dropbox/android/taskqueue/DbTask;->a:Lcom/dropbox/android/util/analytics/m;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/dropbox/android/taskqueue/DbTask;)I
    .locals 3

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/DbTask;->b:J

    long-to-int v0, v0

    iget-wide v1, p1, Lcom/dropbox/android/taskqueue/DbTask;->b:J

    long-to-int v1, v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation
.end method

.method public final b(J)V
    .locals 0

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/dropbox/android/taskqueue/DbTask;->b:J

    .line 93
    return-void
.end method

.method public abstract b(Lcom/dropbox/android/taskqueue/w;)V
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v0

    return v0
.end method

.method protected abstract h()J
.end method

.method public final h_()I
    .locals 1

    .prologue
    .line 101
    const/16 v0, 0x14

    return v0
.end method

.method public abstract j()Landroid/net/Uri;
.end method

.method public abstract l()Ljava/lang/String;
.end method

.method public abstract m()Ljava/lang/String;
.end method

.method public final n()J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/DbTask;->b:J

    return-wide v0
.end method
