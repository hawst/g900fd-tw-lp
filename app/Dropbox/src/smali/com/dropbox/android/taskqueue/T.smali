.class public final Lcom/dropbox/android/taskqueue/T;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Z

.field private b:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/T;->a:Z

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/T;->b:Landroid/os/PowerManager$WakeLock;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/T;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/T;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 35
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reuse of TransferResourceLock is not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/bA;->a()Lcom/dropbox/android/util/bA;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/bA;->b()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/T;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :try_start_1
    invoke-static {}, Lcom/dropbox/android/util/u;->a()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 46
    iput-object v0, p0, Lcom/dropbox/android/taskqueue/T;->b:Landroid/os/PowerManager$WakeLock;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 48
    return-void

    .line 41
    :catchall_0
    move-exception v0

    throw v0

    .line 47
    :catchall_1
    move-exception v0

    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 53
    iget-boolean v1, p0, Lcom/dropbox/android/taskqueue/T;->a:Z

    if-eqz v1, :cond_0

    .line 55
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/bA;->a()Lcom/dropbox/android/util/bA;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/bA;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/T;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 63
    :try_start_1
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/T;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 69
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 70
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error releasing wifi and wake locks"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 72
    :cond_2
    return-void

    .line 64
    :catch_0
    move-exception v0

    goto :goto_1

    .line 56
    :catch_1
    move-exception v0

    goto :goto_0
.end method
