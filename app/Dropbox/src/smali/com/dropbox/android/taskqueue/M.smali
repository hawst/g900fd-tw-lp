.class final Lcom/dropbox/android/taskqueue/M;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/taskqueue/N;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Lcom/dropbox/android/taskqueue/N;

    invoke-direct {v0, p1}, Lcom/dropbox/android/taskqueue/N;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/M;->a:Lcom/dropbox/android/taskqueue/N;

    .line 85
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/L;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/M;->a:Lcom/dropbox/android/taskqueue/N;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/N;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/M;->a:Lcom/dropbox/android/taskqueue/N;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/N;->a()V

    .line 111
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/L;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/M;->a:Lcom/dropbox/android/taskqueue/N;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/N;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 89
    if-nez v0, :cond_0

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 92
    :cond_0
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/M;->a:Lcom/dropbox/android/taskqueue/N;

    invoke-virtual {v1, p1, v0}, Lcom/dropbox/android/taskqueue/N;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/L;)Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/M;->a:Lcom/dropbox/android/taskqueue/N;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/N;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
