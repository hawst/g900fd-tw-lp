.class public final enum Lcom/dropbox/android/taskqueue/ag;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/taskqueue/ag;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum b:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum c:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum d:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum e:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum f:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum g:Lcom/dropbox/android/taskqueue/ag;

.field public static final enum h:Lcom/dropbox/android/taskqueue/ag;

.field private static final synthetic i:[Lcom/dropbox/android/taskqueue/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "WAITING_FOR_WIFI"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->a:Lcom/dropbox/android/taskqueue/ag;

    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "WAITING_FOR_CONNECTION"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->b:Lcom/dropbox/android/taskqueue/ag;

    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "WAITING_FOR_FASTER_NETWORK"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->c:Lcom/dropbox/android/taskqueue/ag;

    .line 54
    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "WAITING_TO_UPLOAD"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->d:Lcom/dropbox/android/taskqueue/ag;

    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "WAITING_FOR_BATTERY"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->e:Lcom/dropbox/android/taskqueue/ag;

    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "WAITING_FOR_QUOTA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->f:Lcom/dropbox/android/taskqueue/ag;

    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "NONE_PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->g:Lcom/dropbox/android/taskqueue/ag;

    .line 55
    new-instance v0, Lcom/dropbox/android/taskqueue/ag;

    const-string v1, "UPLOADING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->h:Lcom/dropbox/android/taskqueue/ag;

    .line 52
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/dropbox/android/taskqueue/ag;

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->a:Lcom/dropbox/android/taskqueue/ag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->b:Lcom/dropbox/android/taskqueue/ag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->c:Lcom/dropbox/android/taskqueue/ag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->d:Lcom/dropbox/android/taskqueue/ag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->e:Lcom/dropbox/android/taskqueue/ag;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/taskqueue/ag;->f:Lcom/dropbox/android/taskqueue/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/taskqueue/ag;->g:Lcom/dropbox/android/taskqueue/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/taskqueue/ag;->h:Lcom/dropbox/android/taskqueue/ag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/taskqueue/ag;->i:[Lcom/dropbox/android/taskqueue/ag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/ag;
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/dropbox/android/taskqueue/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/ag;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/taskqueue/ag;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/dropbox/android/taskqueue/ag;->i:[Lcom/dropbox/android/taskqueue/ag;

    invoke-virtual {v0}, [Lcom/dropbox/android/taskqueue/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/taskqueue/ag;

    return-object v0
.end method
