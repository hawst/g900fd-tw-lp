.class public final Lcom/dropbox/android/taskqueue/an;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/taskqueue/i",
        "<",
        "Lcom/dropbox/android/taskqueue/UserImportUploadTask;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/UserImportUploadTask;
    .locals 8

    .prologue
    .line 38
    new-instance v1, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v1}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    .line 41
    :try_start_0
    invoke-virtual {v1, p3}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ldbxyzptlk/db231222/aj/c;

    move-object v2, v0

    .line 43
    const-string v1, "mLocalUri"

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 44
    const-string v1, "mDestinationFilename"

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 52
    if-nez v1, :cond_0

    .line 53
    invoke-static {v5}, Lcom/dropbox/android/util/ab;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 55
    :goto_0
    new-instance v1, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    new-instance v4, Lcom/dropbox/android/util/DropboxPath;

    const-string v3, "mDropboxDir"

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v7, 0x1

    invoke-direct {v4, v3, v7}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    const-string v3, "mOverwrite"

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    return-object v1

    .line 62
    :catch_0
    move-exception v1

    .line 63
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    move-object v6, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string v0, "com.dropbox.android.taskqueue.UserImportUploadTask"

    return-object v0
.end method

.method public final synthetic b(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/DbTask;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/android/taskqueue/an;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    move-result-object v0

    return-object v0
.end method
