.class public abstract Lcom/dropbox/android/taskqueue/TaskQueue;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/C",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/q;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ThreadPoolExecutor;

.field private g:I

.field private final h:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/TaskQueue;->a:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(IILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->b:Z

    .line 35
    iput v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->g:I

    .line 370
    new-instance v0, Lcom/dropbox/android/taskqueue/A;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/A;-><init>(Lcom/dropbox/android/taskqueue/TaskQueue;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->h:Ljava/lang/Runnable;

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    .line 89
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Thread"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(ILjava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 91
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->e:Ljava/util/List;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/TaskQueue;)Ljava/util/PriorityQueue;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    return-object v0
.end method

.method private static a(ILjava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 7

    .prologue
    .line 408
    .line 412
    new-instance v6, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v6, p0}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    .line 417
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v1, 0x0

    const-wide/16 v3, 0xa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move v2, p0

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 421
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor$DiscardPolicy;

    invoke-direct {v1}, Ljava/util/concurrent/ThreadPoolExecutor$DiscardPolicy;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V

    .line 422
    new-instance v1, Lcom/dropbox/android/taskqueue/B;

    invoke-direct {v1, p1, p2}, Lcom/dropbox/android/taskqueue/B;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->setThreadFactory(Ljava/util/concurrent/ThreadFactory;)V

    .line 430
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)V

    return-void
.end method

.method private b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 352
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->c()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    .line 354
    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZLcom/dropbox/android/taskqueue/w;)V

    .line 365
    :goto_0
    return-void

    .line 357
    :cond_0
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZLcom/dropbox/android/taskqueue/w;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 359
    :catch_0
    move-exception v0

    .line 361
    sget-object v1, Lcom/dropbox/android/taskqueue/TaskQueue;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error running task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in task queue"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V

    .line 363
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZLcom/dropbox/android/taskqueue/w;)V

    goto :goto_0
.end method

.method private declared-synchronized b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZLcom/dropbox/android/taskqueue/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z",
            "Lcom/dropbox/android/taskqueue/w;",
            ")V"
        }
    .end annotation

    .prologue
    .line 333
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {p3}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-eq v0, v1, :cond_0

    .line 335
    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZLcom/dropbox/android/taskqueue/w;)V

    .line 337
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Lcom/dropbox/android/taskqueue/w;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    monitor-exit p0

    return-void

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/taskqueue/TaskQueue;)Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/taskqueue/TaskQueue;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 404
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/C;

    .line 120
    iget-object v0, v0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 122
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 127
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->f()V

    .line 128
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 130
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/dropbox/android/taskqueue/w;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330
    return-void
.end method

.method protected abstract a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZLcom/dropbox/android/taskqueue/w;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z",
            "Lcom/dropbox/android/taskqueue/w;",
            ")V"
        }
    .end annotation
.end method

.method public declared-synchronized a(Lcom/dropbox/android/util/aY;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/aY",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 148
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/C;

    iget-object v0, v0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 150
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aY;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->f()V

    .line 152
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 155
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 313
    const/4 v0, 0x1

    return v0
.end method

.method protected final declared-synchronized a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/TaskQueue;->d(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    const/4 v0, 0x0

    .line 300
    :goto_0
    monitor-exit p0

    return v0

    .line 287
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    new-instance v1, Lcom/dropbox/android/taskqueue/C;

    iget v2, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->g:I

    invoke-direct {v1, p1, p2, v2}, Lcom/dropbox/android/taskqueue/C;-><init>(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZI)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->e:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/q;

    .line 293
    invoke-interface {v0, p1}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/u;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 297
    :cond_1
    :try_start_2
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->b:Z

    if-nez v0, :cond_2

    .line 298
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/TaskQueue;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 300
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 265
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 266
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 275
    :goto_0
    monitor-exit p0

    return v0

    .line 270
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/C;

    .line 271
    iget-object v0, v0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 272
    goto :goto_0

    .line 275
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/C;

    .line 137
    iget-object v0, v0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 138
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    monitor-exit p0

    return-void
.end method

.method public b(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 109
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/dropbox/android/taskqueue/TaskQueue;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Added "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    return-void
.end method

.method public declared-synchronized b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 164
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/C;

    iget-object v0, v0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 166
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/u;->f()V

    .line 183
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/u;->s()Lcom/dropbox/android/taskqueue/w;

    .line 184
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 185
    sget-object v0, Lcom/dropbox/android/taskqueue/TaskQueue;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cancelled "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    const/4 v0, 0x1

    .line 189
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()I
    .locals 2

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lcom/dropbox/android/taskqueue/TaskQueue;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Added "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    return-void
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 199
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 214
    :goto_0
    monitor-exit p0

    return v0

    .line 204
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/TaskQueue;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 205
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 206
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 207
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->f()V

    .line 208
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 209
    sget-object v0, Lcom/dropbox/android/taskqueue/TaskQueue;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cancelled "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 210
    goto :goto_0

    .line 214
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
