.class final Lcom/dropbox/android/taskqueue/o;
.super Ljava/util/TimerTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dropbox/android/taskqueue/u;",
        ">",
        "Ljava/util/TimerTask;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/android/taskqueue/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/p",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dropbox/android/taskqueue/p;Lcom/dropbox/android/taskqueue/u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/taskqueue/p",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/p;

    .line 139
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/o;->b:Ljava/lang/ref/WeakReference;

    .line 140
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/o;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/u;

    .line 145
    if-eqz v0, :cond_0

    .line 146
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/p;

    invoke-interface {v1, v0}, Lcom/dropbox/android/taskqueue/p;->a(Lcom/dropbox/android/taskqueue/u;)V

    .line 148
    :cond_0
    return-void
.end method
