.class public Lcom/dropbox/android/taskqueue/ThumbnailTask;
.super Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Ldbxyzptlk/db231222/k/h;

.field private final e:Lcom/dropbox/android/taskqueue/R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/R",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/dropbox/android/util/DropboxPath;

.field private final g:Ldbxyzptlk/db231222/v/n;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/dropbox/android/taskqueue/TaskQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/taskqueue/R;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/TaskQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/z/M;",
            "Ldbxyzptlk/db231222/k/h;",
            "Lcom/dropbox/android/taskqueue/R",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/v/n;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ldbxyzptlk/db231222/z/M;

    .line 137
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->c:Ldbxyzptlk/db231222/k/h;

    .line 138
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->e:Lcom/dropbox/android/taskqueue/R;

    .line 139
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    .line 140
    iput-object p5, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->g:Ldbxyzptlk/db231222/v/n;

    .line 141
    iput-object p6, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h:Ljava/lang/String;

    .line 142
    iput-object p7, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->i:Ljava/lang/String;

    .line 143
    iput-object p8, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->j:Lcom/dropbox/android/taskqueue/TaskQueue;

    .line 144
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/taskqueue/R;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/taskqueue/Q;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct/range {p0 .. p8}, Lcom/dropbox/android/taskqueue/ThumbnailTask;-><init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/taskqueue/R;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/TaskQueue;)V

    return-void
.end method

.method public static a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->g:Ldbxyzptlk/db231222/v/n;

    invoke-static {v0, v1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 234
    new-instance v1, Ldbxyzptlk/db231222/j/l;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 170
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 172
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->r()V

    .line 173
    const/4 v2, 0x0

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v5, Ldbxyzptlk/db231222/v/m;->a:Ldbxyzptlk/db231222/v/m;

    .line 178
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->c:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->e()Ljava/io/File;

    move-result-object v7

    .line 179
    if-nez v7, :cond_2

    .line 180
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 215
    if-eqz v1, :cond_0

    .line 217
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c

    .line 223
    :cond_0
    :goto_1
    return-object v0

    .line 176
    :cond_1
    :try_start_2
    sget-object v5, Ldbxyzptlk/db231222/v/m;->b:Ldbxyzptlk/db231222/v/m;

    goto :goto_0

    .line 182
    :cond_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->g:Ldbxyzptlk/db231222/v/n;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/n;Ldbxyzptlk/db231222/v/m;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;

    .line 186
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 188
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 190
    invoke-static {v1}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 191
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_12
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_11
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_f
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 218
    :catch_0
    move-exception v1

    goto :goto_1

    .line 195
    :cond_3
    :try_start_5
    invoke-virtual {v7, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 196
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_12
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_11
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_5 .. :try_end_5} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_5 .. :try_end_5} :catch_f
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 218
    :catch_1
    move-exception v1

    goto :goto_1

    .line 215
    :cond_4
    if-eqz v3, :cond_5

    .line 217
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_d

    .line 222
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->i()Lcom/dropbox/android/taskqueue/w;

    .line 223
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    goto :goto_1

    .line 198
    :catch_2
    move-exception v0

    move-object v3, v1

    .line 199
    :goto_3
    :try_start_8
    sget-object v1, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a:Ljava/lang/String;

    const-string v2, "Dropbox Gallery low on memory."

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 200
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->h:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_1

    .line 218
    :catch_3
    move-exception v1

    goto :goto_1

    .line 201
    :catch_4
    move-exception v0

    move-object v3, v1

    .line 202
    :goto_4
    :try_start_a
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_1

    .line 218
    :catch_5
    move-exception v1

    goto :goto_1

    .line 203
    :catch_6
    move-exception v0

    move-object v3, v1

    .line 204
    :goto_5
    :try_start_c
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_6

    .line 205
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_d
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    goto/16 :goto_1

    .line 218
    :catch_7
    move-exception v1

    goto/16 :goto_1

    .line 206
    :cond_6
    :try_start_e
    iget v0, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x19f

    if-ne v0, v1, :cond_7

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->e:Lcom/dropbox/android/taskqueue/R;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/R;->a(Ljava/lang/Object;)V

    .line 208
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_f
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    goto/16 :goto_1

    .line 218
    :catch_8
    move-exception v1

    goto/16 :goto_1

    .line 210
    :cond_7
    :try_start_10
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_11
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9

    goto/16 :goto_1

    .line 218
    :catch_9
    move-exception v1

    goto/16 :goto_1

    .line 212
    :catch_a
    move-exception v0

    move-object v3, v1

    .line 213
    :goto_6
    :try_start_12
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result-object v0

    .line 215
    if-eqz v3, :cond_0

    .line 217
    :try_start_13
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b

    goto/16 :goto_1

    .line 218
    :catch_b
    move-exception v1

    goto/16 :goto_1

    .line 215
    :catchall_0
    move-exception v0

    move-object v3, v1

    :goto_7
    if-eqz v3, :cond_8

    .line 217
    :try_start_14
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_e

    .line 218
    :cond_8
    :goto_8
    throw v0

    :catch_c
    move-exception v1

    goto/16 :goto_1

    :catch_d
    move-exception v0

    goto/16 :goto_2

    :catch_e
    move-exception v1

    goto :goto_8

    .line 215
    :catchall_1
    move-exception v0

    goto :goto_7

    .line 212
    :catch_f
    move-exception v0

    goto :goto_6

    .line 203
    :catch_10
    move-exception v0

    goto :goto_5

    .line 201
    :catch_11
    move-exception v0

    goto :goto_4

    .line 198
    :catch_12
    move-exception v0

    goto/16 :goto_3
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->s()Lcom/dropbox/android/taskqueue/w;

    .line 166
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->f:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public final j()Ldbxyzptlk/db231222/v/n;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->g:Ldbxyzptlk/db231222/v/n;

    return-object v0
.end method

.method public final k()Lcom/dropbox/android/taskqueue/TaskQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->j:Lcom/dropbox/android/taskqueue/TaskQueue;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ThumbnailTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
