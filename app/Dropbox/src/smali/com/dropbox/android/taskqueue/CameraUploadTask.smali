.class public Lcom/dropbox/android/taskqueue/CameraUploadTask;
.super Lcom/dropbox/android/taskqueue/DbTask;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ldbxyzptlk/db231222/r/d;

.field private final e:Lcom/dropbox/android/service/G;

.field private final f:Ljava/io/File;

.field private g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:J

.field private final m:I

.field private final n:J

.field private final o:Ljava/lang/String;

.field private p:Z

.field private q:J

.field private r:J

.field private s:Ldbxyzptlk/db231222/z/aE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/z/aE",
            "<",
            "Ldbxyzptlk/db231222/z/ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V
    .locals 14

    .prologue
    .line 92
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {}, Lcom/dropbox/android/util/ad;->a()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v13}, Lcom/dropbox/android/taskqueue/CameraUploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;Z)V

    .line 94
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DbTask;-><init>()V

    .line 85
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    .line 86
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/db231222/z/aE;

    .line 99
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    .line 102
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:Lcom/dropbox/android/service/G;

    .line 104
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    .line 105
    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    .line 106
    iput-object p8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    .line 107
    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    .line 108
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    .line 109
    iput-object p5, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    .line 110
    iput-wide p6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:J

    .line 111
    iput p9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:I

    .line 112
    iput-wide p10, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:J

    .line 113
    iput-object p12, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:Ljava/lang/String;

    .line 114
    iput-boolean p13, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Z

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    .line 118
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 119
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;ZLcom/dropbox/android/taskqueue/a;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct/range {p0 .. p13}, Lcom/dropbox/android/taskqueue/CameraUploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/CameraUploadTask;)Lcom/dropbox/android/service/G;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:Lcom/dropbox/android/service/G;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/CameraUploadTask;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/service/J;)Lcom/dropbox/android/taskqueue/z;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/service/J;)Lcom/dropbox/android/taskqueue/z;

    move-result-object v0

    return-object v0
.end method

.method private a(Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/service/J;)Lcom/dropbox/android/taskqueue/z;
    .locals 4

    .prologue
    .line 153
    invoke-virtual {p2}, Lcom/dropbox/android/service/J;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    .line 166
    :goto_0
    return-object v0

    .line 155
    :cond_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    const-wide/32 v2, 0x1900000

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 157
    :cond_1
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->b:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0

    .line 158
    :cond_2
    invoke-virtual {p2}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 159
    invoke-virtual {p2}, Lcom/dropbox/android/service/J;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/dropbox/android/service/J;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 160
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0

    .line 162
    :cond_3
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->d:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0

    .line 166
    :cond_4
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->c:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0
.end method

.method private a(Lcom/dropbox/android/provider/j;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 652
    invoke-virtual {p1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 653
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 654
    const-string v2, "uploaded"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 655
    const-string v2, "camera_upload"

    const-string v3, "_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 659
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 261
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    .line 266
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    .line 267
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 268
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 269
    const-string v5, "server_hash"

    iget-object v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v5, "camera_upload"

    const-string v6, "_id = ?"

    new-array v7, v0, [Ljava/lang/String;

    iget-wide v8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 272
    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Z

    .line 273
    const-string v1, "hash_change"

    invoke-static {v1, p0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v3, "oldid"

    int-to-long v4, v2

    invoke-virtual {v1, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 276
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/taskqueue/CameraUploadTask;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method private b(Lcom/dropbox/android/provider/j;)V
    .locals 7

    .prologue
    .line 665
    invoke-virtual {p1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 666
    const-string v1, "camera_upload"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 667
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/taskqueue/CameraUploadTask;)Ldbxyzptlk/db231222/z/aE;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/db231222/z/aE;

    return-object v0
.end method

.method private t()Lcom/dropbox/android/service/I;
    .locals 1

    .prologue
    .line 532
    new-instance v0, Lcom/dropbox/android/taskqueue/c;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/c;-><init>(Lcom/dropbox/android/taskqueue/CameraUploadTask;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/DbTask;)I
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 174
    instance-of v0, p1, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    if-eqz v0, :cond_9

    move-object v0, p1

    .line 175
    check-cast v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    .line 178
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v4

    .line 179
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v5

    .line 180
    if-eqz v4, :cond_1

    if-nez v5, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v1

    .line 182
    :cond_1
    if-nez v4, :cond_2

    if-eqz v5, :cond_2

    move v1, v2

    .line 183
    goto :goto_0

    .line 186
    :cond_2
    iget-wide v5, v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 188
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/n/P;->F()J

    move-result-wide v7

    .line 189
    iget-wide v9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    cmp-long v4, v9, v7

    if-gez v4, :cond_5

    move v4, v1

    .line 190
    :goto_1
    cmp-long v7, v5, v7

    if-gez v7, :cond_3

    move v3, v1

    .line 191
    :cond_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_0

    .line 193
    :cond_4
    if-nez v4, :cond_6

    if-eqz v3, :cond_6

    move v1, v2

    .line 194
    goto :goto_0

    :cond_5
    move v4, v3

    .line 189
    goto :goto_1

    .line 197
    :cond_6
    iget-wide v7, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_8

    .line 198
    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    .line 199
    :goto_2
    iget-wide v2, v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    mul-int/2addr v1, v0

    goto :goto_0

    :cond_7
    move v1, v2

    .line 198
    goto :goto_2

    .line 202
    :cond_8
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v1

    goto :goto_0

    .line 206
    :cond_9
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v1

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    .locals 2

    .prologue
    .line 585
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 586
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    .line 587
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    if-ne p1, v1, :cond_0

    .line 588
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b(Lcom/dropbox/android/provider/j;)V

    .line 593
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    .line 599
    :goto_0
    return-object v0

    .line 596
    :cond_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/provider/j;)V

    .line 599
    :cond_1
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cameraupload~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(J)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 238
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 239
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v2

    if-nez v2, :cond_1

    .line 245
    :cond_0
    :goto_0
    return v0

    .line 244
    :cond_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v1

    .line 245
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v2

    add-long/2addr v2, p1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v4

    const-wide/32 v6, 0x3200000

    sub-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 625
    new-instance v1, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ldbxyzptlk/db231222/j/l;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 626
    return-object v0
.end method

.method public final b(Lcom/dropbox/android/taskqueue/w;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 604
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    if-eq p1, v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 606
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 607
    const-string v2, "ARG_FILENAME"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const-string v0, "ARG_STATUS"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/util/aH;->a:Lcom/dropbox/android/util/aH;

    invoke-virtual {v0, v2, v3, v3, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aH;Ljava/lang/String;Ljava/lang/Long;Landroid/os/Bundle;)I

    .line 611
    :cond_0
    return-void
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 15

    .prologue
    const-wide/16 v2, 0x0

    .line 281
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 283
    new-instance v14, Lcom/dropbox/android/taskqueue/T;

    invoke-direct {v14}, Lcom/dropbox/android/taskqueue/T;-><init>()V

    .line 285
    :try_start_0
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 289
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    :goto_0
    return-object v0

    .line 299
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 300
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 303
    :cond_1
    :try_start_3
    new-instance v10, Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 304
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    .line 305
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 307
    if-eqz v10, :cond_2

    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 308
    :cond_2
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 309
    :cond_3
    :try_start_4
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 315
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 317
    :catch_0
    move-exception v0

    .line 318
    :try_start_5
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 319
    :catch_1
    move-exception v0

    .line 320
    :try_start_6
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->g:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 324
    :cond_4
    :try_start_7
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(J)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result v0

    if-nez v0, :cond_5

    .line 328
    :try_start_8
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 329
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/service/e;->b:Lcom/dropbox/android/service/e;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;

    .line 330
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(J)Z

    move-result v0

    if-nez v0, :cond_5

    .line 331
    iget v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:I

    .line 332
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aM;->a:Lcom/dropbox/android/util/aM;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 333
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->m:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_8
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 336
    :catch_2
    move-exception v0

    .line 337
    :try_start_9
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 338
    :catch_3
    move-exception v0

    .line 339
    :try_start_a
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v2, "Error getting account info for out of quota task."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 341
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 348
    :cond_5
    :try_start_b
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aM;->a:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 350
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r()V

    .line 352
    new-instance v13, Lcom/dropbox/android/taskqueue/a;

    invoke-direct {v13, p0}, Lcom/dropbox/android/taskqueue/a;-><init>(Lcom/dropbox/android/taskqueue/CameraUploadTask;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 360
    :try_start_c
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    .line 362
    monitor-enter p0
    :try_end_c
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_c .. :try_end_c} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_c .. :try_end_c} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_c .. :try_end_c} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_c .. :try_end_c} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_c .. :try_end_c} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_c .. :try_end_c} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_c .. :try_end_c} :catch_d
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 363
    :try_start_d
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 364
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 366
    :cond_6
    :try_start_e
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 370
    :try_start_f
    iget-wide v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J
    :try_end_f
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_f .. :try_end_f} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_f .. :try_end_f} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_f .. :try_end_f} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_f .. :try_end_f} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_f .. :try_end_f} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_f .. :try_end_f} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_f .. :try_end_f} :catch_d
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    const-wide/32 v3, 0x800000

    cmp-long v1, v1, v3

    if-gez v1, :cond_7

    .line 373
    :try_start_10
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-static {v1}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;)Ljava/lang/String;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_10 .. :try_end_10} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_10 .. :try_end_10} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_10 .. :try_end_10} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_10 .. :try_end_10} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_10 .. :try_end_10} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_10 .. :try_end_10} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_10 .. :try_end_10} :catch_d
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    move-result-object v1

    .line 377
    :try_start_11
    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ljava/lang/String;)Z

    .line 378
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    iget-wide v4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:J

    iget-object v8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:Ljava/lang/String;

    iget v9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:I

    iget-wide v11, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    invoke-virtual/range {v0 .. v13}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/InputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/aE;

    move-result-object v0

    .line 402
    :goto_1
    monitor-enter p0
    :try_end_11
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_11 .. :try_end_11} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_11 .. :try_end_11} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_11 .. :try_end_11} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_11 .. :try_end_11} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_11 .. :try_end_11} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_11 .. :try_end_11} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_11 .. :try_end_11} :catch_d
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 403
    :try_start_12
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 404
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 366
    :catchall_0
    move-exception v0

    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :try_start_14
    throw v0
    :try_end_14
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_14 .. :try_end_14} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_14 .. :try_end_14} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_14 .. :try_end_14} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_14 .. :try_end_14} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_14 .. :try_end_14} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_14 .. :try_end_14} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_14 .. :try_end_14} :catch_d
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    .line 434
    :catch_4
    move-exception v0

    .line 435
    :try_start_15
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IO Exception uploading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 436
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 374
    :catch_5
    move-exception v0

    .line 375
    :try_start_16
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_16
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_16 .. :try_end_16} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_16 .. :try_end_16} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_16 .. :try_end_16} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_16 .. :try_end_16} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_16 .. :try_end_16} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_16 .. :try_end_16} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_16 .. :try_end_16} :catch_d
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 383
    :cond_7
    :try_start_17
    iget-wide v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    invoke-static {v10, v1, v2, v13}, Ldbxyzptlk/db231222/z/a;->b(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/g;
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_6
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_17 .. :try_end_17} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_17 .. :try_end_17} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_17 .. :try_end_17} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_17 .. :try_end_17} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_17 .. :try_end_17} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_17 .. :try_end_17} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    move-result-object v11

    .line 388
    :try_start_18
    new-instance v12, Lcom/dropbox/android/taskqueue/b;

    invoke-direct {v12, p0}, Lcom/dropbox/android/taskqueue/b;-><init>(Lcom/dropbox/android/taskqueue/CameraUploadTask;)V

    .line 397
    iget-object v1, v11, Ldbxyzptlk/db231222/z/g;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ljava/lang/String;)Z

    .line 398
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    iget-wide v4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:J

    iget-object v8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:Ljava/lang/String;

    iget v9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:I

    invoke-virtual/range {v0 .. v12}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/FileInputStream;Ldbxyzptlk/db231222/z/g;Ldbxyzptlk/db231222/z/Q;)Ldbxyzptlk/db231222/z/aE;

    move-result-object v0

    goto :goto_1

    .line 384
    :catch_6
    move-exception v0

    .line 385
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v2, "Error while scanning file"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 386
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_18
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_18 .. :try_end_18} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_18 .. :try_end_18} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_18 .. :try_end_18} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_18 .. :try_end_18} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_18 .. :try_end_18} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_18 .. :try_end_18} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_18 .. :try_end_18} :catch_d
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 406
    :cond_8
    :try_start_19
    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/db231222/z/aE;

    .line 407
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    .line 409
    :try_start_1a
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->t()Lcom/dropbox/android/service/I;

    move-result-object v1

    .line 410
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:Lcom/dropbox/android/service/G;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V
    :try_end_1a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_1a .. :try_end_1a} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_1a .. :try_end_1a} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_1a .. :try_end_1a} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_1a .. :try_end_1a} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_1a .. :try_end_1a} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_1a .. :try_end_1a} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1a .. :try_end_1a} :catch_d
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    .line 414
    :try_start_1b
    const-string v0, "net.start"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 415
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/db231222/z/aE;

    invoke-interface {v0}, Ldbxyzptlk/db231222/z/aE;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/ad;

    .line 416
    const-string v2, "net.end"

    invoke-static {v2, p0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    .line 418
    :try_start_1c
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:Lcom/dropbox/android/service/G;

    invoke-virtual {v2, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 421
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v1

    .line 422
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/ad;->a()Ldbxyzptlk/db231222/v/j;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    .line 423
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/ad;->a()Ldbxyzptlk/db231222/v/j;

    move-result-object v3

    iget-object v3, v3, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    .line 424
    sget-object v4, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-static {}, Lcom/dropbox/android/util/bn;->f()Ldbxyzptlk/db231222/v/n;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 425
    sget-object v4, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-static {}, Lcom/dropbox/android/util/bn;->k()Ldbxyzptlk/db231222/v/n;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 426
    sget-object v4, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    :try_end_1c
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_1c .. :try_end_1c} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_1c .. :try_end_1c} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_1c .. :try_end_1c} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_1c .. :try_end_1c} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_1c .. :try_end_1c} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_1c .. :try_end_1c} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1c .. :try_end_1c} :catch_d
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    .line 430
    :try_start_1d
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/ad;->b()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_9

    .line 431
    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/ad;->b()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1d
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_1d} :catch_f
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_1d .. :try_end_1d} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_1d .. :try_end_1d} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_1d .. :try_end_1d} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_1d .. :try_end_1d} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_1d .. :try_end_1d} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_1d .. :try_end_1d} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1d .. :try_end_1d} :catch_d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    .line 519
    :cond_9
    :goto_2
    :try_start_1e
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 521
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i()Lcom/dropbox/android/taskqueue/w;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 407
    :catchall_1
    move-exception v0

    :try_start_1f
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    :try_start_20
    throw v0
    :try_end_20
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_20 .. :try_end_20} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_20 .. :try_end_20} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_20 .. :try_end_20} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_20 .. :try_end_20} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_20 .. :try_end_20} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_20 .. :try_end_20} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_20 .. :try_end_20} :catch_d
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    .line 437
    :catch_7
    move-exception v0

    .line 438
    :try_start_21
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 439
    sget-object v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v1, "Upload canceled"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 418
    :catchall_2
    move-exception v0

    :try_start_22
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:Lcom/dropbox/android/service/G;

    invoke-virtual {v2, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    throw v0
    :try_end_22
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_22 .. :try_end_22} :catch_4
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_22 .. :try_end_22} :catch_7
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_22 .. :try_end_22} :catch_8
    .catch Ldbxyzptlk/db231222/z/ak; {:try_start_22 .. :try_end_22} :catch_9
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_22 .. :try_end_22} :catch_a
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_22 .. :try_end_22} :catch_c
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_22 .. :try_end_22} :catch_d
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    .line 448
    :catch_8
    move-exception v0

    .line 449
    :try_start_23
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-static {v0}, Lcom/dropbox/android/util/Activities;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 450
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 446
    :cond_a
    :try_start_24
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 451
    :catch_9
    move-exception v0

    .line 452
    :try_start_25
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->j:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 453
    :catch_a
    move-exception v0

    .line 454
    :try_start_26
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v2, "Server exception uploading."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    sparse-switch v1, :sswitch_data_0

    .line 502
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 503
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 460
    :sswitch_0
    :try_start_27
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sent bad camera upload hash for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-boolean v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Z

    if-nez v1, :cond_c

    .line 462
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v2, "Rehashing."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_3

    .line 464
    :try_start_28
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-static {v1}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 465
    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 467
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c()Lcom/dropbox/android/taskqueue/w;
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_e
    .catchall {:try_start_28 .. :try_end_28} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 469
    :cond_b
    :try_start_29
    const-string v1, "rehashed_nochange"

    invoke-static {v1, p0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_e
    .catchall {:try_start_29 .. :try_end_29} :catchall_3

    .line 478
    :cond_c
    :goto_3
    :try_start_2a
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v2, "Failing due to bad hash."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 480
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 483
    :sswitch_1
    :try_start_2b
    sget-object v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    const-string v1, "Got a camera upload conflict."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/i;->a()V
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_3

    goto/16 :goto_2

    .line 523
    :catchall_3
    move-exception v0

    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    throw v0

    .line 487
    :sswitch_2
    :try_start_2c
    iget v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:I
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_3

    .line 489
    :try_start_2d
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 490
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    :try_end_2d
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2d .. :try_end_2d} :catch_b
    .catchall {:try_start_2d .. :try_end_2d} :catchall_3

    .line 496
    :try_start_2e
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aM;->a:Lcom/dropbox/android/util/aM;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 497
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 491
    :catch_b
    move-exception v0

    .line 494
    :try_start_2f
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 500
    :sswitch_3
    :try_start_30
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 505
    :catch_c
    move-exception v0

    .line 506
    :try_start_31
    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/f;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "5xx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 507
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 509
    :cond_d
    :try_start_32
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception uploading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 511
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 513
    :catch_d
    move-exception v0

    .line 514
    :try_start_33
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception uploading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->f:Ljava/io/File;

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 516
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_3

    move-result-object v0

    .line 523
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 470
    :catch_e
    move-exception v1

    goto/16 :goto_3

    .line 433
    :catch_f
    move-exception v0

    goto/16 :goto_2

    .line 455
    :sswitch_data_0
    .sparse-switch
        0x199 -> :sswitch_1
        0x19c -> :sswitch_0
        0x1f6 -> :sswitch_3
        0x1f7 -> :sswitch_3
        0x1fb -> :sswitch_2
    .end sparse-switch
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 61
    check-cast p1, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    .line 223
    :try_start_0
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    .line 224
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/db231222/z/aE;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/db231222/z/aE;

    invoke-interface {v0}, Ldbxyzptlk/db231222/z/aE;->a()V

    .line 227
    :cond_0
    monitor-exit p0

    .line 228
    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final h()J
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    return-wide v0
.end method

.method public final i()Lcom/dropbox/android/taskqueue/w;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/provider/j;)V

    .line 580
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    return-object v0
.end method

.method public final i_()Lcom/dropbox/android/taskqueue/z;
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->g:Lcom/dropbox/android/taskqueue/z;

    .line 145
    :cond_0
    :goto_0
    return-object v0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:Lcom/dropbox/android/service/G;

    invoke-virtual {v1}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/service/J;)Lcom/dropbox/android/taskqueue/z;

    move-result-object v0

    .line 130
    sget-object v1, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    if-ne v0, v1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/util/i;->a(Landroid/content/Context;)Lcom/dropbox/android/util/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aM;->c:Lcom/dropbox/android/util/aM;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 136
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->e:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aM;->c:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 141
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 142
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->f:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0

    .line 145
    :cond_3
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0
.end method

.method public final j()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 4

    .prologue
    .line 638
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 639
    const-string v1, "mFilePath"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    const-string v1, "mServerHash"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    const-string v1, "mMimeType"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    const-string v1, "mDbRowId"

    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    const-string v1, "mBatchFileNumber"

    iget v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    const-string v1, "mContentUri"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    const-string v1, "mImportTime"

    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 646
    const-string v1, "mImportTimeoffset"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    const-string v1, "mRehashed"

    iget-boolean v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    invoke-static {v0}, Ldbxyzptlk/db231222/aj/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CameraUploadTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
