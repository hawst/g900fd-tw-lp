.class public final enum Lcom/dropbox/android/taskqueue/w;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/taskqueue/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/taskqueue/w;

.field public static final enum b:Lcom/dropbox/android/taskqueue/w;

.field public static final enum c:Lcom/dropbox/android/taskqueue/w;

.field public static final enum d:Lcom/dropbox/android/taskqueue/w;

.field public static final enum e:Lcom/dropbox/android/taskqueue/w;

.field public static final enum f:Lcom/dropbox/android/taskqueue/w;

.field public static final enum g:Lcom/dropbox/android/taskqueue/w;

.field public static final enum h:Lcom/dropbox/android/taskqueue/w;

.field public static final enum i:Lcom/dropbox/android/taskqueue/w;

.field public static final enum j:Lcom/dropbox/android/taskqueue/w;

.field public static final enum k:Lcom/dropbox/android/taskqueue/w;

.field public static final enum l:Lcom/dropbox/android/taskqueue/w;

.field public static final enum m:Lcom/dropbox/android/taskqueue/w;

.field public static final enum n:Lcom/dropbox/android/taskqueue/w;

.field public static final enum o:Lcom/dropbox/android/taskqueue/w;

.field public static final enum p:Lcom/dropbox/android/taskqueue/w;

.field private static final synthetic s:[Lcom/dropbox/android/taskqueue/w;


# instance fields
.field private final q:Lcom/dropbox/android/taskqueue/y;

.field private final r:Lcom/dropbox/android/taskqueue/x;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 36
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "NONE"

    sget-object v2, Lcom/dropbox/android/taskqueue/y;->c:Lcom/dropbox/android/taskqueue/y;

    sget-object v3, Lcom/dropbox/android/taskqueue/x;->b:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    .line 38
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "SUCCESS"

    sget-object v2, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    sget-object v3, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    .line 40
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "SUCCESS_W_WARNING"

    sget-object v2, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    sget-object v3, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->c:Lcom/dropbox/android/taskqueue/w;

    .line 42
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "NETWORK_ERROR"

    sget-object v2, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v3, Lcom/dropbox/android/taskqueue/x;->b:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    .line 44
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "PERM_NETWORK_ERROR"

    sget-object v2, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v3, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    .line 46
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "STORAGE_ERROR"

    const/4 v2, 0x5

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    .line 48
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "SECURITY_ERROR"

    const/4 v2, 0x6

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->g:Lcom/dropbox/android/taskqueue/w;

    .line 50
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "MEMORY_ERROR"

    const/4 v2, 0x7

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->b:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->h:Lcom/dropbox/android/taskqueue/w;

    .line 52
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "TEMP_SERVER_ERROR"

    const/16 v2, 0x8

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->b:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    .line 54
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "TEMP_LOCAL_ERROR"

    const/16 v2, 0x9

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->b:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->j:Lcom/dropbox/android/taskqueue/w;

    .line 56
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "CANCELED"

    const/16 v2, 0xa

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    .line 58
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "NOT_ENOUGH_QUOTA"

    const/16 v2, 0xb

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->c:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    .line 59
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "ALMOST_NOT_ENOUGH_QUOTA"

    const/16 v2, 0xc

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->c:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->m:Lcom/dropbox/android/taskqueue/w;

    .line 61
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "FAILURE"

    const/16 v2, 0xd

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    .line 63
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "FORBIDDEN"

    const/16 v2, 0xe

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->o:Lcom/dropbox/android/taskqueue/w;

    .line 65
    new-instance v0, Lcom/dropbox/android/taskqueue/w;

    const-string v1, "CONFLICT"

    const/16 v2, 0xf

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    sget-object v4, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/w;-><init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->p:Lcom/dropbox/android/taskqueue/w;

    .line 34
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/dropbox/android/taskqueue/w;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->c:Lcom/dropbox/android/taskqueue/w;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    aput-object v1, v0, v8

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->g:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->h:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->j:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->m:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->o:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->p:Lcom/dropbox/android/taskqueue/w;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/taskqueue/w;->s:[Lcom/dropbox/android/taskqueue/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/dropbox/android/taskqueue/y;Lcom/dropbox/android/taskqueue/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/taskqueue/y;",
            "Lcom/dropbox/android/taskqueue/x;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 86
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/w;->q:Lcom/dropbox/android/taskqueue/y;

    .line 87
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/w;->r:Lcom/dropbox/android/taskqueue/x;

    .line 88
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/w;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/dropbox/android/taskqueue/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/taskqueue/w;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->s:[Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v0}, [Lcom/dropbox/android/taskqueue/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/w;->r:Lcom/dropbox/android/taskqueue/x;

    sget-object v1, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/android/taskqueue/x;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/w;->r:Lcom/dropbox/android/taskqueue/x;

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/y;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/w;->q:Lcom/dropbox/android/taskqueue/y;

    return-object v0
.end method
