.class public Lcom/dropbox/android/taskqueue/D;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/io/File;

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/taskqueue/P;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/dropbox/android/taskqueue/M;

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/dropbox/android/taskqueue/K;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/dropbox/android/provider/j;

.field private final g:Lcom/dropbox/android/taskqueue/S;

.field private final h:Lcom/dropbox/android/taskqueue/v;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/D;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/provider/j;Ljava/io/File;Lcom/dropbox/android/taskqueue/S;)V
    .locals 2

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    .line 115
    new-instance v0, Lcom/dropbox/android/taskqueue/M;

    const/16 v1, 0x1388

    invoke-direct {v0, v1}, Lcom/dropbox/android/taskqueue/M;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    .line 609
    new-instance v0, Lcom/dropbox/android/taskqueue/G;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/G;-><init>(Lcom/dropbox/android/taskqueue/D;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/D;->h:Lcom/dropbox/android/taskqueue/v;

    .line 164
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/D;->f:Lcom/dropbox/android/provider/j;

    .line 165
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/D;->b:Ljava/io/File;

    .line 166
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/D;->g:Lcom/dropbox/android/taskqueue/S;

    .line 167
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/provider/j;Ljava/io/File;Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;)V
    .locals 1
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lcom/dropbox/android/taskqueue/S;

    invoke-direct {v0, p3, p4}, Lcom/dropbox/android/taskqueue/S;-><init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/taskqueue/D;-><init>(Lcom/dropbox/android/provider/j;Ljava/io/File;Lcom/dropbox/android/taskqueue/S;)V

    .line 161
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 468
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 469
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 470
    invoke-static {}, Lcom/dropbox/android/util/bi;->a()[B

    move-result-object v1

    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 475
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 476
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, v2}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 482
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 484
    :goto_0
    return-object v0

    .line 477
    :catch_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 478
    :goto_1
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 482
    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v0

    .line 479
    :catch_1
    move-exception v1

    move-object v1, v0

    .line 482
    :goto_3
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    .line 479
    :catch_2
    move-exception v2

    goto :goto_3

    .line 477
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Lcom/dropbox/android/taskqueue/J;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 175
    invoke-direct {p0, p1, p3}, Lcom/dropbox/android/taskqueue/D;->c(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;

    move-result-object v3

    .line 176
    if-eqz v3, :cond_0

    move v2, v0

    .line 177
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 178
    :goto_1
    new-instance v1, Lcom/dropbox/android/taskqueue/J;

    invoke-direct {v1, p2, v2, v0}, Lcom/dropbox/android/taskqueue/J;-><init>(Ljava/lang/String;ZZ)V

    return-object v1

    :cond_0
    move v2, v1

    .line 176
    goto :goto_0

    :cond_1
    move v0, v1

    .line 177
    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/D;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/D;->c()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/D;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/D;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/L;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 220
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/M;->a(Lcom/dropbox/android/taskqueue/L;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->f:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 222
    new-array v2, v9, [Ljava/lang/String;

    sget-object v1, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    aput-object v1, v2, v7

    sget-object v1, Lcom/dropbox/android/provider/i;->d:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    aput-object v1, v2, v8

    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " like ?  AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/provider/i;->c:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = ? AND substr("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", ?) not like \'%/%\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 226
    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/dropbox/android/taskqueue/L;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "%"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    iget-object v1, p1, Lcom/dropbox/android/taskqueue/L;->second:Ljava/lang/Object;

    check-cast v1, Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/v/n;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    iget-object v1, p1, Lcom/dropbox/android/taskqueue/L;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    .line 228
    const-string v1, "thumbnail_info"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 229
    if-eqz v1, :cond_1

    .line 231
    :try_start_0
    sget-object v0, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 232
    sget-object v0, Lcom/dropbox/android/provider/i;->d:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 233
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 236
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->o(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 237
    iget-object v5, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, p1, v0, v4}, Lcom/dropbox/android/taskqueue/M;->a(Lcom/dropbox/android/taskqueue/L;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 240
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 244
    :cond_1
    return-void
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/H;)V
    .locals 6

    .prologue
    .line 345
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    monitor-enter v3

    .line 346
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 347
    if-eqz v0, :cond_2

    .line 348
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 350
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 351
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dropbox/android/taskqueue/P;

    .line 352
    if-eqz v2, :cond_0

    .line 353
    invoke-interface {p2, v2, p1}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/taskqueue/P;Lcom/dropbox/android/util/DropboxPath;)V

    goto :goto_0

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 355
    :cond_0
    :try_start_1
    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359
    :cond_1
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 360
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 363
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    return-void
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V
    .locals 1

    .prologue
    .line 379
    new-instance v0, Lcom/dropbox/android/taskqueue/F;

    invoke-direct {v0, p0, p2, p3}, Lcom/dropbox/android/taskqueue/F;-><init>(Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V

    .line 386
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/H;)V

    .line 387
    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/v/n;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 318
    invoke-virtual {p0, p2, p3}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;

    move-result-object v3

    .line 319
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->g:Lcom/dropbox/android/taskqueue/S;

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/S;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/TaskQueue;)Lcom/dropbox/android/taskqueue/ThumbnailTask;

    move-result-object v0

    .line 320
    if-nez v0, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 325
    :goto_0
    return v0

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/D;->h:Lcom/dropbox/android/taskqueue/v;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 324
    invoke-virtual {p1, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;)V

    .line 325
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/dropbox/android/taskqueue/D;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->f:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 184
    new-array v2, v7, [Ljava/lang/String;

    sget-object v1, Lcom/dropbox/android/provider/i;->d:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    aput-object v1, v2, v6

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = ? AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/provider/i;->c:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 187
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-virtual {p2}, Ldbxyzptlk/db231222/v/n;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    .line 191
    const-string v1, "thumbnail_info"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 192
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 193
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    sget-object v1, Lcom/dropbox/android/provider/i;->d:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 197
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 199
    return-object v5
.end method

.method private b(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 1

    .prologue
    .line 368
    new-instance v0, Lcom/dropbox/android/taskqueue/E;

    invoke-direct {v0, p0, p2, p3}, Lcom/dropbox/android/taskqueue/E;-><init>(Lcom/dropbox/android/taskqueue/D;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 375
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/H;)V

    .line 376
    return-void
.end method

.method private c(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 203
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->o(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 204
    new-instance v3, Lcom/dropbox/android/taskqueue/L;

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v0, p2}, Lcom/dropbox/android/taskqueue/L;-><init>(Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    .line 206
    invoke-direct {p0, v3}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/L;)V

    .line 208
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/dropbox/android/taskqueue/M;->a(Lcom/dropbox/android/taskqueue/L;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 216
    :goto_0
    return-object v0

    .line 214
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;

    move-result-object v1

    .line 215
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v3, v0, v1}, Lcom/dropbox/android/taskqueue/M;->a(Lcom/dropbox/android/taskqueue/L;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 216
    goto :goto_0
.end method

.method private c()V
    .locals 11

    .prologue
    .line 531
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 566
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->f:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 536
    new-instance v5, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT OR REPLACE INTO thumbnail_info ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/i;->c:Lcom/dropbox/android/provider/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/i;->d:Lcom/dropbox/android/provider/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") VALUES (?, ?, ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    .line 539
    iget-object v7, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    monitor-enter v7

    .line 540
    :try_start_0
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 542
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 543
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/K;

    .line 544
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 546
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 547
    const/4 v9, 0x1

    iget-object v3, v1, Lcom/dropbox/android/taskqueue/K;->first:Ljava/lang/Object;

    check-cast v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v9, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 548
    const/4 v3, 0x2

    iget-object v1, v1, Lcom/dropbox/android/taskqueue/K;->second:Ljava/lang/Object;

    check-cast v1, Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/v/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v3, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 549
    const/4 v1, 0x3

    invoke-virtual {v6, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 551
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v1

    .line 552
    const-wide/16 v9, 0x0

    cmp-long v1, v1, v9

    if-ltz v1, :cond_1

    .line 553
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 558
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 564
    :catchall_1
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 556
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 558
    :try_start_4
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 561
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/K;

    .line 562
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 564
    :cond_3
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 565
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto/16 :goto_0
.end method

.method private c(Ljava/util/Set;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 488
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->f:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 489
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 490
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/D;->b:Ljava/io/File;

    invoke-static {v1}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 491
    const-string v1, "thumbnail_info"

    invoke-virtual {v0, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 528
    :goto_0
    return-void

    .line 493
    :cond_1
    new-instance v8, Ljava/util/Stack;

    invoke-direct {v8}, Ljava/util/Stack;-><init>()V

    .line 495
    const-string v1, "thumbnail_info"

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    aput-object v3, v2, v9

    const-string v3, ""

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 499
    :cond_2
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 500
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v9}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    .line 501
    if-eqz p1, :cond_3

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 502
    :cond_3
    invoke-virtual {v8, v2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 505
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 507
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 509
    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 510
    :cond_5
    :goto_2
    invoke-virtual {v8}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 511
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/util/DropboxPath;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 514
    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    .line 515
    const-string v1, "thumbnail_info"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v4, v4, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 516
    const/4 v3, -0x1

    if-ne v1, v3, :cond_5

    .line 517
    sget-object v1, Lcom/dropbox/android/taskqueue/D;->a:Ljava/lang/String;

    const-string v3, "Failed to clear thumbs from the db"

    invoke-static {v1, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 519
    :catch_0
    move-exception v1

    .line 520
    :try_start_2
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3, v1}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 525
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 523
    :cond_6
    :try_start_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 525
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/taskqueue/I;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/v/n;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/dropbox/android/taskqueue/O;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 276
    invoke-static {p2}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 277
    invoke-static {p3}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    .line 281
    invoke-direct {p0, p2, p3, p4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Lcom/dropbox/android/taskqueue/J;

    move-result-object v1

    .line 283
    new-instance v2, Lcom/dropbox/android/taskqueue/O;

    invoke-direct {v2}, Lcom/dropbox/android/taskqueue/O;-><init>()V

    .line 284
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/J;->a()Z

    move-result v3

    iput-boolean v3, v2, Lcom/dropbox/android/taskqueue/O;->a:Z

    .line 285
    iput-boolean v4, v2, Lcom/dropbox/android/taskqueue/O;->b:Z

    .line 287
    iget-boolean v3, v2, Lcom/dropbox/android/taskqueue/O;->a:Z

    if-eqz v3, :cond_0

    .line 289
    invoke-virtual {p0, p2, p4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;

    move-result-object v3

    .line 291
    :try_start_0
    invoke-direct {p0, v3}, Lcom/dropbox/android/taskqueue/D;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 298
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/J;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, v2, Lcom/dropbox/android/taskqueue/O;->a:Z

    if-nez v3, :cond_2

    .line 299
    :cond_1
    invoke-static {p1}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/J;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, p2, p4, v1}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;)Z

    move-result v1

    .line 300
    iput-boolean v1, v2, Lcom/dropbox/android/taskqueue/O;->b:Z

    .line 303
    :cond_2
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    .line 292
    :catch_0
    move-exception v3

    .line 294
    iput-boolean v4, v2, Lcom/dropbox/android/taskqueue/O;->a:Z

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)Ljava/io/File;
    .locals 3

    .prologue
    .line 433
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/D;->b:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ".png"

    .line 423
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/D;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 424
    invoke-virtual {p2}, Ldbxyzptlk/db231222/v/n;->a()Ljava/lang/String;

    move-result-object v2

    .line 425
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->j()Ljava/lang/String;

    move-result-object v3

    .line 426
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    .line 427
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 428
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x2f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 420
    :cond_0
    const-string v0, ".jpg"

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 569
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/D;->c()V

    .line 570
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/I;)V
    .locals 1

    .prologue
    .line 170
    invoke-static {p1}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b()V

    .line 171
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    .line 329
    invoke-static {p1}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Ljava/lang/String;)Z

    .line 330
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V
    .locals 5

    .prologue
    .line 402
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    monitor-enter v3

    .line 403
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 404
    if-eqz v0, :cond_2

    .line 405
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 406
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dropbox/android/taskqueue/P;

    .line 407
    if-ne v2, p2, :cond_0

    .line 408
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 409
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 410
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    :cond_1
    monitor-exit v3

    .line 417
    :goto_0
    return-void

    .line 416
    :cond_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 574
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    .line 575
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    monitor-enter v1

    .line 576
    :try_start_0
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->o(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 577
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    new-instance v4, Lcom/dropbox/android/taskqueue/L;

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v0, p2}, Lcom/dropbox/android/taskqueue/L;-><init>(Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v0, p3}, Lcom/dropbox/android/taskqueue/M;->a(Lcom/dropbox/android/taskqueue/L;Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    new-instance v2, Lcom/dropbox/android/taskqueue/K;

    invoke-direct {v2, p1, p2}, Lcom/dropbox/android/taskqueue/K;-><init>(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V

    invoke-virtual {v0, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    .line 582
    :goto_0
    if-eqz v0, :cond_0

    .line 583
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/D;->c()V

    .line 585
    :cond_0
    return-void

    .line 579
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 581
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/ref/WeakReference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/taskqueue/P;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 391
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    monitor-enter v1

    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 393
    if-nez v0, :cond_0

    .line 394
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 395
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/D;->c:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 398
    monitor-exit v1

    .line 399
    return-void

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    sget-object v0, Lcom/dropbox/android/taskqueue/I;->a:Lcom/dropbox/android/taskqueue/I;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a()V

    .line 253
    sget-object v0, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a()V

    .line 254
    sget-object v0, Lcom/dropbox/android/taskqueue/I;->c:Lcom/dropbox/android/taskqueue/I;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a()V

    .line 256
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->d:Lcom/dropbox/android/taskqueue/M;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/M;->a()V

    .line 257
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/D;->c()V

    .line 258
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/D;->c(Ljava/util/Set;)V

    .line 259
    return-void
.end method

.method public final b(Ljava/util/Set;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 592
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->b:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;)J

    move-result-wide v3

    .line 599
    const-wide/16 v0, 0x0

    .line 600
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 601
    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/io/File;

    move-result-object v0

    .line 602
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 603
    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;)J

    move-result-wide v6

    add-long v0, v1, v6

    :goto_1
    move-wide v1, v0

    .line 605
    goto :goto_0

    .line 606
    :cond_0
    sub-long v0, v3, v1

    return-wide v0

    :cond_1
    move-wide v0, v1

    goto :goto_1
.end method

.method public final b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    .line 308
    invoke-direct {p0, p2, p3, p4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Lcom/dropbox/android/taskqueue/J;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/J;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    invoke-static {p1}, Lcom/dropbox/android/taskqueue/I;->a(Lcom/dropbox/android/taskqueue/I;)Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/J;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, p2, p4, v0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;)Z

    .line 314
    :cond_0
    return-void
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 441
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/D;->f:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 442
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 443
    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 444
    new-array v1, v2, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/provider/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 445
    const-string v2, "thumbnail_info"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v4, v4, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ESCAPE \'\\\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 449
    if-ne v0, v5, :cond_0

    .line 450
    sget-object v0, Lcom/dropbox/android/taskqueue/D;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to clear folder of thumbs from the db: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 454
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 455
    const-string v2, "thumbnail_info"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/i;->b:Lcom/dropbox/android/provider/c;

    iget-object v4, v4, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 459
    if-ne v0, v5, :cond_0

    .line 460
    sget-object v0, Lcom/dropbox/android/taskqueue/D;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to clear thumb from the db: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
