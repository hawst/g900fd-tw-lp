.class final Lcom/dropbox/android/taskqueue/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/taskqueue/DownloadTask;

.field private b:J

.field private c:J

.field private d:J


# direct methods
.method constructor <init>(Lcom/dropbox/android/taskqueue/DownloadTask;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 87
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/k;->a:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/k;->b:J

    .line 90
    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/k;->c:J

    .line 91
    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/k;->d:J

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/u;JJ)V
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 115
    cmp-long v0, p2, v7

    if-lez v0, :cond_2

    iget-wide v5, p0, Lcom/dropbox/android/taskqueue/k;->d:J

    cmp-long v0, v5, v7

    if-nez v0, :cond_2

    move v0, v1

    .line 128
    :goto_0
    iget-wide v5, p0, Lcom/dropbox/android/taskqueue/k;->b:J

    sub-long v5, p2, v5

    const-wide/32 v7, 0x80000

    cmp-long v5, v5, v7

    if-ltz v5, :cond_3

    iget-wide v5, p0, Lcom/dropbox/android/taskqueue/k;->d:J

    const-wide/16 v7, 0x78

    cmp-long v5, v5, v7

    if-gez v5, :cond_3

    iget-wide v5, p0, Lcom/dropbox/android/taskqueue/k;->c:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x7d0

    cmp-long v5, v5, v7

    if-ltz v5, :cond_3

    .line 131
    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 132
    :cond_0
    const-string v0, "progress"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "progress"

    invoke-virtual {v0, v1, p2, p3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 133
    iput-wide p2, p0, Lcom/dropbox/android/taskqueue/k;->b:J

    .line 134
    iput-wide v3, p0, Lcom/dropbox/android/taskqueue/k;->c:J

    .line 135
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/k;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/k;->d:J

    .line 137
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 115
    goto :goto_0

    :cond_3
    move v1, v2

    .line 128
    goto :goto_1
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 5

    .prologue
    .line 95
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    .line 96
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->g()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 97
    const-string v1, "start"

    invoke-static {v1, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "size"

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->c()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "mime"

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DownloadTask;->j()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 98
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 3

    .prologue
    .line 141
    const-string v0, "error"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DownloadTask;->j()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 142
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/u;)V
    .locals 2

    .prologue
    .line 146
    const-string v0, "success"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DownloadTask;->j()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 147
    return-void
.end method

.method public final d(Lcom/dropbox/android/taskqueue/u;)V
    .locals 2

    .prologue
    .line 151
    const-string v0, "cancel"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DownloadTask;->j()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 152
    return-void
.end method
