.class final Lcom/dropbox/android/taskqueue/g;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/taskqueue/DbTask;

.field private b:J


# direct methods
.method constructor <init>(Lcom/dropbox/android/taskqueue/DbTask;)V
    .locals 2

    .prologue
    .line 35
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/g;->a:Lcom/dropbox/android/taskqueue/DbTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/g;->b:J

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/u;JJ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 56
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/g;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    .line 57
    :goto_0
    if-eqz v0, :cond_0

    .line 58
    const-string v0, "progress"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "progress"

    invoke-virtual {v0, v1, p2, p3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 59
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/g;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/g;->b:J

    .line 61
    :cond_0
    return-void

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 6

    .prologue
    .line 41
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->h()J

    move-result-wide v1

    .line 42
    const-wide/32 v3, 0x800000

    cmp-long v0, v1, v3

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 43
    :goto_0
    const-string v3, "start"

    invoke-static {v3, p1}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "mime"

    check-cast p1, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/DbTask;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "size"

    invoke-virtual {v3, v4, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is.large"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DbTask;->o()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 46
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 4

    .prologue
    .line 65
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v1, "error"

    invoke-static {v1, p1}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "stack_trace"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DbTask;->o()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 69
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/u;)V
    .locals 2

    .prologue
    .line 73
    const-string v0, "success"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DbTask;->o()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 74
    return-void
.end method

.method public final d(Lcom/dropbox/android/taskqueue/u;)V
    .locals 2

    .prologue
    .line 78
    const-string v0, "cancel"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/taskqueue/DbTask;->o()Lcom/dropbox/android/util/analytics/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 79
    return-void
.end method
