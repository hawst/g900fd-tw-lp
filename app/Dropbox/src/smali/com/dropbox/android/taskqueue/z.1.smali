.class public final enum Lcom/dropbox/android/taskqueue/z;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/taskqueue/z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/taskqueue/z;

.field public static final enum b:Lcom/dropbox/android/taskqueue/z;

.field public static final enum c:Lcom/dropbox/android/taskqueue/z;

.field public static final enum d:Lcom/dropbox/android/taskqueue/z;

.field public static final enum e:Lcom/dropbox/android/taskqueue/z;

.field public static final enum f:Lcom/dropbox/android/taskqueue/z;

.field public static final enum g:Lcom/dropbox/android/taskqueue/z;

.field private static final synthetic h:[Lcom/dropbox/android/taskqueue/z;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "YES"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "NEED_WIFI"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->b:Lcom/dropbox/android/taskqueue/z;

    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "NEED_CONNECTION"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->c:Lcom/dropbox/android/taskqueue/z;

    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "NEED_FASTER_NETWORK"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->d:Lcom/dropbox/android/taskqueue/z;

    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "NEED_BATTERY"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->e:Lcom/dropbox/android/taskqueue/z;

    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "NEED_QUOTA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->f:Lcom/dropbox/android/taskqueue/z;

    new-instance v0, Lcom/dropbox/android/taskqueue/z;

    const-string v1, "NEED_UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->g:Lcom/dropbox/android/taskqueue/z;

    .line 108
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dropbox/android/taskqueue/z;

    sget-object v1, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/taskqueue/z;->b:Lcom/dropbox/android/taskqueue/z;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/taskqueue/z;->c:Lcom/dropbox/android/taskqueue/z;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/taskqueue/z;->d:Lcom/dropbox/android/taskqueue/z;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/taskqueue/z;->e:Lcom/dropbox/android/taskqueue/z;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/taskqueue/z;->f:Lcom/dropbox/android/taskqueue/z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/taskqueue/z;->g:Lcom/dropbox/android/taskqueue/z;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/taskqueue/z;->h:[Lcom/dropbox/android/taskqueue/z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/z;
    .locals 1

    .prologue
    .line 108
    const-class v0, Lcom/dropbox/android/taskqueue/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/z;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/taskqueue/z;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->h:[Lcom/dropbox/android/taskqueue/z;

    invoke-virtual {v0}, [Lcom/dropbox/android/taskqueue/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/taskqueue/z;

    return-object v0
.end method
