.class final Lcom/dropbox/android/taskqueue/ak;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/dropbox/android/taskqueue/DbTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1001
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/ak;->a:Ljava/util/ArrayList;

    .line 1005
    if-eqz p1, :cond_0

    .line 1006
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ak;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1008
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/DbTask;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1013
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/DbTask;->q()I

    move-result v0

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/DbTask;->h_()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    if-lt v0, v3, :cond_2

    move v0, v1

    .line 1014
    :goto_0
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/DbTask;->q()I

    move-result v3

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/DbTask;->h_()I

    move-result v4

    div-int/lit8 v4, v4, 0x4

    if-lt v3, v4, :cond_0

    move v2, v1

    .line 1015
    :cond_0
    if-eqz v0, :cond_3

    if-nez v2, :cond_3

    .line 1028
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 1013
    goto :goto_0

    .line 1017
    :cond_3
    if-nez v0, :cond_4

    if-eqz v2, :cond_4

    .line 1018
    const/4 v1, -0x1

    goto :goto_1

    .line 1022
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ak;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ak;->a:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    sub-int v1, v0, v1

    .line 1023
    if-nez v1, :cond_1

    .line 1028
    invoke-virtual {p1, p2}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 998
    check-cast p1, Lcom/dropbox/android/taskqueue/DbTask;

    check-cast p2, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/taskqueue/ak;->a(Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v0

    return v0
.end method
