.class public Lcom/dropbox/android/taskqueue/U;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Lcom/dropbox/android/service/e;

.field private static final k:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/aj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private final f:Lcom/dropbox/android/taskqueue/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/m",
            "<",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/dropbox/android/taskqueue/h;

.field private final h:Ldbxyzptlk/db231222/r/d;

.field private i:Lcom/dropbox/android/service/d;

.field private final j:I

.field private final l:Lcom/dropbox/android/provider/j;

.field private final m:Landroid/database/ContentObservable;

.field private final n:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/dropbox/android/service/e;->d:Lcom/dropbox/android/service/e;

    sput-object v0, Lcom/dropbox/android/taskqueue/U;->a:Lcom/dropbox/android/service/e;

    .line 901
    const-class v0, Lcom/dropbox/android/taskqueue/U;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/U;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/taskqueue/h;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ldbxyzptlk/db231222/r/d;",
            "Lcom/dropbox/android/taskqueue/h;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/aj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    const-wide/16 v5, 0x3a98

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/taskqueue/U;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/taskqueue/h;Ljava/util/List;J)V

    .line 200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/taskqueue/h;Ljava/util/List;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ldbxyzptlk/db231222/r/d;",
            "Lcom/dropbox/android/taskqueue/h;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/aj;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/U;->e:Z

    .line 522
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/U;->j:I

    .line 903
    new-instance v0, Landroid/database/ContentObservable;

    invoke-direct {v0}, Landroid/database/ContentObservable;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    .line 141
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/U;->g:Lcom/dropbox/android/taskqueue/h;

    .line 142
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/U;->h:Ldbxyzptlk/db231222/r/d;

    .line 143
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/dropbox/android/taskqueue/ak;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->g:Lcom/dropbox/android/taskqueue/h;

    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/h;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/taskqueue/ak;-><init>(Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->h:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->l:Lcom/dropbox/android/provider/j;

    .line 146
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/U;->n:Landroid/content/Context;

    .line 147
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    .line 149
    new-instance v2, Lcom/dropbox/android/taskqueue/V;

    invoke-direct {v2, p0}, Lcom/dropbox/android/taskqueue/V;-><init>(Lcom/dropbox/android/taskqueue/U;)V

    .line 181
    new-instance v0, Lcom/dropbox/android/taskqueue/m;

    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v1

    const-wide/32 v5, 0x5265c00

    move-wide v3, p5

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/taskqueue/m;-><init>(Lcom/dropbox/android/service/G;Lcom/dropbox/android/taskqueue/p;JJ)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->f:Lcom/dropbox/android/taskqueue/m;

    .line 184
    new-instance v0, Lcom/dropbox/android/util/i;

    new-instance v1, Lcom/dropbox/android/taskqueue/X;

    invoke-direct {v1, p0}, Lcom/dropbox/android/taskqueue/X;-><init>(Lcom/dropbox/android/taskqueue/U;)V

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/i;-><init>(Landroid/content/Context;Lcom/dropbox/android/util/l;)V

    .line 194
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->k()V

    .line 195
    return-void
.end method

.method private static a(Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;ILandroid/database/MatrixCursor;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "I",
            "Landroid/database/MatrixCursor;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 683
    .line 684
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 685
    instance-of v4, v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    if-eqz v4, :cond_0

    .line 688
    check-cast v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    .line 689
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 690
    :cond_1
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    add-int v5, p2, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->n()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->j()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->l()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->k()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->u()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 697
    invoke-virtual {p3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 698
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 700
    goto :goto_0

    .line 701
    :cond_2
    return v1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/U;)Landroid/database/ContentObservable;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    return-object v0
.end method

.method private declared-synchronized a(Lcom/dropbox/android/util/aY;)Lcom/dropbox/android/taskqueue/DbTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/aY",
            "<",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)",
            "Lcom/dropbox/android/taskqueue/DbTask;"
        }
    .end annotation

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 328
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aY;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 338
    :goto_0
    monitor-exit p0

    return-object v0

    .line 333
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 334
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aY;->a(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 338
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ILjava/lang/Class;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 967
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 968
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 969
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 970
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 971
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lt v0, p1, :cond_0

    move-object v0, v1

    .line 976
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 967
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 2

    .prologue
    .line 608
    monitor-enter p0

    .line 609
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 610
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Very unexpected, a task that finished executing, was not in our list"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 653
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 614
    :cond_0
    :try_start_1
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_3

    .line 615
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 616
    invoke-virtual {p1, p2}, Lcom/dropbox/android/taskqueue/DbTask;->b(Lcom/dropbox/android/taskqueue/w;)V

    .line 643
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 645
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->m()V

    .line 647
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/U;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->i:Lcom/dropbox/android/service/d;

    if-eqz v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->h:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/U;->i:Lcom/dropbox/android/service/d;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/d;)V

    .line 650
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->i:Lcom/dropbox/android/service/d;

    .line 653
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_2

    .line 656
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->b(Lcom/dropbox/android/taskqueue/u;)V

    .line 658
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->j()V

    .line 659
    return-void

    .line 622
    :cond_3
    :try_start_2
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/DbTask;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/dropbox/android/taskqueue/x;->a:Lcom/dropbox/android/taskqueue/x;

    .line 624
    :goto_1
    sget-object v1, Lcom/dropbox/android/taskqueue/W;->b:[I

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/x;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 637
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 638
    invoke-virtual {p1, p2}, Lcom/dropbox/android/taskqueue/DbTask;->b(Lcom/dropbox/android/taskqueue/w;)V

    goto :goto_0

    .line 622
    :cond_4
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->b()Lcom/dropbox/android/taskqueue/x;

    move-result-object v0

    goto :goto_1

    .line 626
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->b(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 627
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/U;->e:Z

    .line 628
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->f:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/m;->a(Lcom/dropbox/android/taskqueue/u;)V

    goto :goto_0

    .line 634
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->b(Lcom/dropbox/android/taskqueue/DbTask;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 624
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/w;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/w;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 1

    .prologue
    .line 203
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/Collection;)V

    .line 204
    return-void
.end method

.method private a(Lcom/dropbox/android/util/aY;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/aY",
            "<",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 422
    sget-object v0, Lcom/dropbox/android/taskqueue/U;->k:Ljava/lang/String;

    const-string v2, "canceled task"

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 427
    monitor-enter p0

    .line 428
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v4

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 429
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aY;->a(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 430
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    .line 432
    if-eqz p2, :cond_1

    .line 433
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 434
    monitor-exit p0

    .line 462
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    :goto_2
    move v2, v0

    .line 437
    goto :goto_0

    .line 439
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v4

    .line 440
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 441
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 442
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aY;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 443
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    .line 444
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->s()Lcom/dropbox/android/taskqueue/w;

    .line 445
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 446
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 449
    if-eqz p2, :cond_4

    move v3, v1

    .line 454
    :goto_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    if-eqz v1, :cond_3

    .line 457
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    invoke-static {v0, v4}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 459
    :cond_3
    if-eqz v3, :cond_0

    .line 460
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->j()V

    goto :goto_1

    :cond_4
    move v0, v1

    move v2, v1

    :goto_5
    move v3, v2

    move v2, v0

    .line 453
    goto :goto_3

    .line 454
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v2

    move v2, v3

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method private a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/dropbox/android/taskqueue/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/aj;

    .line 211
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/u;

    .line 212
    invoke-interface {v0, p0, v1}, Lcom/dropbox/android/taskqueue/aj;->a(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/u;)V

    goto :goto_0

    .line 216
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/U;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/taskqueue/U;->e:Z

    return p1
.end method

.method private declared-synchronized b(Lcom/dropbox/android/taskqueue/DbTask;)V
    .locals 2

    .prologue
    .line 321
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 322
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    monitor-exit p0

    return-void

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/taskqueue/U;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->m()V

    return-void
.end method

.method private b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/aj;

    .line 221
    invoke-interface {v0, p0, p1}, Lcom/dropbox/android/taskqueue/aj;->b(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/u;)V

    goto :goto_0

    .line 224
    :cond_0
    return-void
.end method

.method private declared-synchronized b(Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 907
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->l:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 909
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO pending_uploads ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/provider/g;->b:Lcom/dropbox/android/provider/c;

    iget-object v2, v2, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/provider/g;->c:Lcom/dropbox/android/provider/c;

    iget-object v2, v2, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") VALUES (?, ?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 915
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 918
    :try_start_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 919
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/U;->g:Lcom/dropbox/android/taskqueue/h;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dropbox/android/taskqueue/h;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    .line 921
    sget-object v5, Lcom/dropbox/android/taskqueue/U;->k:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Task "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " adding to task DB"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 923
    const/4 v5, 0x1

    invoke-virtual {v2, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 924
    const/4 v4, 0x2

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 925
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v4

    .line 926
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    .line 927
    sget-object v0, Lcom/dropbox/android/taskqueue/U;->k:Ljava/lang/String;

    const-string v4, "Error inserting upload entry into db!"

    invoke-static {v0, v4}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 935
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 936
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 907
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 929
    :cond_0
    :try_start_3
    invoke-virtual {v0, v4, v5}, Lcom/dropbox/android/taskqueue/DbTask;->b(J)V

    goto :goto_0

    .line 933
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 935
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 936
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 938
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized c(Ljava/lang/Class;)Lcom/dropbox/android/taskqueue/DbTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)",
            "Lcom/dropbox/android/taskqueue/DbTask;"
        }
    .end annotation

    .prologue
    .line 953
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 954
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 963
    :goto_0
    monitor-exit p0

    return-object v0

    .line 958
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 959
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 963
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 953
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Lcom/dropbox/android/taskqueue/DbTask;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 942
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->l:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 943
    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/DbTask;->n()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 944
    const-string v2, "pending_uploads"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/g;->a:Lcom/dropbox/android/provider/c;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 946
    if-eq v0, v5, :cond_0

    .line 947
    sget-object v1, Lcom/dropbox/android/taskqueue/U;->k:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error deleting task entry from db table pending_uploads , deleted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    :cond_0
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 227
    monitor-enter p0

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->f:Lcom/dropbox/android/taskqueue/m;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/m;->a(I)V

    .line 229
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/aj;

    .line 232
    invoke-interface {v0, p0}, Lcom/dropbox/android/taskqueue/aj;->a(Lcom/dropbox/android/taskqueue/U;)V

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 235
    :cond_0
    return-void
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->l:Lcom/dropbox/android/provider/j;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 239
    const-string v1, "pending_uploads"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 241
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    sget-object v1, Lcom/dropbox/android/provider/g;->a:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 243
    sget-object v3, Lcom/dropbox/android/provider/g;->b:Lcom/dropbox/android/provider/c;

    iget-object v3, v3, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 244
    sget-object v4, Lcom/dropbox/android/provider/g;->c:Lcom/dropbox/android/provider/c;

    iget-object v4, v4, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 246
    iget-object v5, p0, Lcom/dropbox/android/taskqueue/U;->g:Lcom/dropbox/android/taskqueue/h;

    iget-object v6, p0, Lcom/dropbox/android/taskqueue/U;->n:Landroid/content/Context;

    iget-object v7, p0, Lcom/dropbox/android/taskqueue/U;->h:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v5, v3, v6, v7, v4}, Lcom/dropbox/android/taskqueue/h;->a(Ljava/lang/String;Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v3

    .line 247
    invoke-virtual {v3, v1, v2}, Lcom/dropbox/android/taskqueue/DbTask;->b(J)V

    .line 248
    invoke-direct {p0, v3}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/taskqueue/u;)V

    .line 249
    invoke-direct {p0, v3}, Lcom/dropbox/android/taskqueue/U;->b(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_0

    .line 251
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 253
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->m()V

    .line 254
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->j()V

    .line 255
    return-void
.end method

.method private declared-synchronized l()Lcom/dropbox/android/taskqueue/DbTask;
    .locals 4

    .prologue
    .line 525
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 526
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 528
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->i_()Lcom/dropbox/android/taskqueue/z;

    move-result-object v2

    .line 529
    sget-object v3, Lcom/dropbox/android/taskqueue/W;->a:[I

    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/z;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 531
    :pswitch_0
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    :goto_1
    monitor-exit p0

    return-object v0

    .line 536
    :pswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->o()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 542
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized m()V
    .locals 7

    .prologue
    .line 580
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/U;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 581
    const-class v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/U;->b(Ljava/lang/Class;)I

    move-result v0

    .line 582
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v1

    .line 584
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v2

    .line 586
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->T()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "num.camera.tasks"

    int-to-long v5, v0

    invoke-virtual {v3, v4, v5, v6}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v3, "num.all.tasks"

    int-to-long v4, v1

    invoke-virtual {v0, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 589
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->l()Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v0

    .line 590
    if-eqz v0, :cond_0

    .line 591
    new-instance v1, Lcom/dropbox/android/taskqueue/ah;

    new-instance v2, Lcom/dropbox/android/taskqueue/ad;

    invoke-direct {v2, p0}, Lcom/dropbox/android/taskqueue/ad;-><init>(Lcom/dropbox/android/taskqueue/U;)V

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/taskqueue/ah;-><init>(Lcom/dropbox/android/taskqueue/ai;Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 599
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/ah;->setPriority(I)V

    .line 601
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/ah;->start()V

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    monitor-exit p0

    return-void

    .line 580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()Lcom/dropbox/android/taskqueue/ag;
    .locals 2

    .prologue
    .line 872
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/U;->a(Z)Landroid/database/Cursor;

    move-result-object v1

    .line 875
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 876
    const-string v0, "camera_upload_status"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 878
    invoke-static {v0}, Lcom/dropbox/android/taskqueue/ag;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 881
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 884
    :goto_0
    return-object v0

    .line 881
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 884
    const/4 v0, 0x0

    goto :goto_0

    .line 881
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private declared-synchronized o()V
    .locals 3

    .prologue
    .line 1062
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->i:Lcom/dropbox/android/service/d;

    if-nez v0, :cond_0

    .line 1063
    new-instance v0, Lcom/dropbox/android/taskqueue/af;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/taskqueue/af;-><init>(Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/taskqueue/V;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/U;->i:Lcom/dropbox/android/service/d;

    .line 1064
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->h:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/U;->a:Lcom/dropbox/android/service/e;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->i:Lcom/dropbox/android/service/d;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1066
    :cond_0
    monitor-exit p0

    return-void

    .line 1062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/dropbox/android/util/DropboxPath;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 708
    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "intended_db_path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "destination_filename"

    aput-object v2, v0, v1

    .line 710
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 712
    const/4 v0, 0x0

    .line 715
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-static {v2, p1, v0, v1}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;ILandroid/database/MatrixCursor;)I

    move-result v0

    add-int/2addr v0, v3

    .line 716
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-static {v2, p1, v0, v1}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;ILandroid/database/MatrixCursor;)I

    .line 718
    new-instance v0, Lcom/dropbox/android/provider/L;

    invoke-direct {v0, v1}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;)V

    .line 719
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/L;->a(Landroid/database/ContentObservable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 721
    monitor-exit p0

    return-object v0

    .line 708
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)Landroid/database/Cursor;
    .locals 19

    .prologue
    .line 747
    monitor-enter p0

    const/16 v1, 0x9

    :try_start_0
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "camera_upload_status"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "camera_upload_initial_scan"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "camera_upload_num_remaining"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "camera_upload_local_uri"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "camera_upload_local_mime_type"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "camera_upload_file_path"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "camera_upload_pending_paths_json"

    aput-object v3, v1, v2

    .line 756
    new-instance v10, Landroid/database/MatrixCursor;

    invoke-direct {v10, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 758
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/U;->h:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v11

    .line 759
    invoke-virtual {v11}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 760
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->g:Lcom/dropbox/android/taskqueue/ag;

    .line 761
    const/4 v9, 0x0

    .line 762
    const-class v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/U;->b(Ljava/lang/Class;)I

    move-result v12

    .line 763
    const/4 v7, 0x0

    .line 764
    const/4 v6, 0x0

    .line 765
    const/4 v5, 0x0

    .line 766
    const-wide/16 v3, -0x1

    .line 767
    const/4 v2, 0x0

    .line 769
    invoke-virtual {v11}, Ldbxyzptlk/db231222/n/P;->r()Ldbxyzptlk/db231222/n/q;

    move-result-object v1

    sget-object v13, Ldbxyzptlk/db231222/n/q;->c:Ldbxyzptlk/db231222/n/q;

    if-ne v1, v13, :cond_0

    .line 770
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 771
    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->d:Lcom/dropbox/android/taskqueue/ag;

    .line 775
    :goto_0
    const/4 v9, 0x1

    move-object v8, v1

    .line 778
    :cond_0
    if-lez v12, :cond_3

    .line 779
    if-nez v9, :cond_d

    .line 780
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/DbTask;

    .line 781
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    const-class v15, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {v14, v15}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 783
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->h:Lcom/dropbox/android/taskqueue/ag;

    .line 784
    const/4 v2, 0x1

    .line 785
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->n()J

    move-result-wide v3

    move-object/from16 v18, v1

    move v1, v2

    move-object/from16 v2, v18

    .line 792
    :goto_1
    if-nez v2, :cond_2

    .line 794
    const-class v2, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/dropbox/android/taskqueue/U;->c(Ljava/lang/Class;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v2

    .line 795
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->n()J

    move-result-wide v3

    .line 797
    if-nez v1, :cond_2

    .line 798
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 800
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->d:Lcom/dropbox/android/taskqueue/ag;

    .line 828
    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    .line 829
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->j()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 830
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->l()Ljava/lang/String;

    move-result-object v5

    .line 831
    move-object v0, v2

    check-cast v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k()Ljava/lang/String;

    move-result-object v1

    move-object v7, v6

    move-object v6, v5

    move-object v5, v1

    .line 835
    :cond_3
    const/16 v1, 0x8

    const-class v9, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v9}, Lcom/dropbox/android/taskqueue/U;->a(ILjava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 836
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 837
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_4
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/DbTask;

    .line 841
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->n()J

    move-result-wide v14

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->n()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-eqz v14, :cond_4

    .line 842
    :cond_5
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->j()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v9, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 747
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 773
    :cond_6
    :try_start_1
    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->b:Lcom/dropbox/android/taskqueue/ag;

    goto/16 :goto_0

    .line 803
    :cond_7
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->i_()Lcom/dropbox/android/taskqueue/z;

    move-result-object v1

    .line 804
    sget-object v8, Lcom/dropbox/android/taskqueue/W;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/z;->ordinal()I

    move-result v1

    aget v1, v8, v1

    packed-switch v1, :pswitch_data_0

    .line 821
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->d:Lcom/dropbox/android/taskqueue/ag;

    goto :goto_2

    .line 806
    :pswitch_0
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->b:Lcom/dropbox/android/taskqueue/ag;

    goto :goto_2

    .line 809
    :pswitch_1
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->a:Lcom/dropbox/android/taskqueue/ag;

    goto :goto_2

    .line 812
    :pswitch_2
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->c:Lcom/dropbox/android/taskqueue/ag;

    goto :goto_2

    .line 815
    :pswitch_3
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->e:Lcom/dropbox/android/taskqueue/ag;

    goto :goto_2

    .line 818
    :pswitch_4
    sget-object v8, Lcom/dropbox/android/taskqueue/ag;->f:Lcom/dropbox/android/taskqueue/ag;

    goto :goto_2

    .line 846
    :cond_8
    invoke-virtual {v11}, Ldbxyzptlk/db231222/n/P;->q()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v11}, Ldbxyzptlk/db231222/n/P;->C()Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    move v2, v1

    .line 847
    :goto_4
    if-eqz p1, :cond_b

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->g:Lcom/dropbox/android/taskqueue/ag;

    if-ne v8, v1, :cond_b

    if-nez v2, :cond_b

    const/4 v1, 0x1

    .line 850
    :goto_5
    if-nez v1, :cond_9

    .line 851
    const/16 v1, 0x9

    new-array v11, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v1

    const/4 v1, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v11, v1

    const/4 v1, 0x2

    invoke-virtual {v8}, Lcom/dropbox/android/taskqueue/ag;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v11, v1

    const/4 v3, 0x3

    if-eqz v2, :cond_c

    const/4 v1, 0x1

    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v11, v3

    const/4 v1, 0x4

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v11, v1

    const/4 v1, 0x5

    aput-object v7, v11, v1

    const/4 v1, 0x6

    aput-object v6, v11, v1

    const/4 v1, 0x7

    aput-object v5, v11, v1

    const/16 v1, 0x8

    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v1

    invoke-virtual {v10, v11}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 865
    :cond_9
    new-instance v1, Lcom/dropbox/android/provider/L;

    invoke-direct {v1, v10}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;)V

    .line 866
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/provider/L;->a(Landroid/database/ContentObservable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 868
    monitor-exit p0

    return-object v1

    .line 846
    :cond_a
    const/4 v1, 0x0

    move v2, v1

    goto :goto_4

    .line 847
    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    .line 851
    :cond_c
    const/4 v1, 0x0

    goto :goto_6

    :cond_d
    move v1, v9

    goto/16 :goto_1

    .line 804
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/dropbox/android/taskqueue/Z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/Z;-><init>(Lcom/dropbox/android/taskqueue/U;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/util/aY;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    monitor-exit p0

    return-void

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 2

    .prologue
    .line 406
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/dropbox/android/taskqueue/ab;

    invoke-direct {v0, p0, p1, p2}, Lcom/dropbox/android/taskqueue/ab;-><init>(Lcom/dropbox/android/taskqueue/U;J)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/util/aY;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    monitor-exit p0

    return-void

    .line 406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/taskqueue/DbTask;)V
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/taskqueue/DbTask;Z)V

    .line 259
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/DbTask;Z)V
    .locals 1

    .prologue
    .line 262
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/List;Z)V

    .line 263
    return-void
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 343
    monitor-enter p0

    .line 344
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 346
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 347
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 348
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    goto :goto_0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 352
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 353
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 354
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 355
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 356
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    .line 357
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->s()Lcom/dropbox/android/taskqueue/w;

    .line 358
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 359
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 362
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 369
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_2

    .line 372
    :cond_4
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->j()V

    .line 373
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 374
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/List;Z)V

    .line 267
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 270
    monitor-enter p0

    .line 273
    if-eqz p2, :cond_3

    .line 275
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 276
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 277
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->a()Ljava/lang/String;

    move-result-object v5

    .line 278
    new-instance v1, Lcom/dropbox/android/taskqueue/Y;

    invoke-direct {v1, p0, v5}, Lcom/dropbox/android/taskqueue/Y;-><init>(Lcom/dropbox/android/taskqueue/U;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/util/aY;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v1

    .line 284
    if-nez v1, :cond_0

    .line 286
    const/4 v3, 0x0

    .line 287
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/DbTask;

    .line 288
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    const/4 v1, 0x1

    .line 293
    :goto_1
    if-nez v1, :cond_0

    .line 294
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object p1, v2

    .line 302
    :cond_3
    :try_start_1
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->b(Ljava/util/Collection;)V

    .line 304
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 305
    const-string v2, "enqueue"

    invoke-static {v2, v0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 306
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->b(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_2

    .line 309
    :cond_4
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->m()V

    .line 310
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/Collection;)V

    .line 317
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->j()V

    .line 318
    return-void

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method public final declared-synchronized b(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/u;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 505
    monitor-enter p0

    const/4 v1, 0x0

    .line 507
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/u;

    .line 508
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 509
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 511
    goto :goto_0

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 514
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 515
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 517
    goto :goto_2

    .line 519
    :cond_1
    monitor-exit p0

    return v1

    .line 505
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 466
    monitor-enter p0

    .line 467
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 469
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/U;->e:Z

    .line 473
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 474
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 475
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    goto :goto_0

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 478
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 479
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 481
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    .line 482
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->s()Lcom/dropbox/android/taskqueue/w;

    .line 483
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 484
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 486
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 493
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_2

    .line 496
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->j()V

    .line 497
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    invoke-static {v0, v3}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 498
    return-void
.end method

.method public final declared-synchronized b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 393
    new-instance v2, Lcom/dropbox/android/taskqueue/aa;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/taskqueue/aa;-><init>(Lcom/dropbox/android/taskqueue/U;Ldbxyzptlk/db231222/j/l;)V

    const/4 v0, 0x1

    invoke-direct {p0, v2, v0}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/util/aY;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 400
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 561
    new-instance v0, Lcom/dropbox/android/taskqueue/ac;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/ac;-><init>(Lcom/dropbox/android/taskqueue/U;)V

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/ac;->start()V

    .line 571
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/U;->m:Landroid/database/ContentObservable;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/t;->a(Landroid/database/ContentObservable;Z)V

    .line 576
    return-void
.end method

.method public final declared-synchronized f()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 726
    monitor-enter p0

    :try_start_0
    const-class v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->c(Ljava/lang/Class;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    .line 727
    if-eqz v0, :cond_0

    .line 728
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->k()Lcom/dropbox/android/util/DropboxPath;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 730
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 726
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 891
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->n()Lcom/dropbox/android/taskqueue/ag;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->f:Lcom/dropbox/android/taskqueue/ag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 898
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/U;->n()Lcom/dropbox/android/taskqueue/ag;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/ag;->a:Lcom/dropbox/android/taskqueue/ag;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 988
    new-instance v0, Lcom/dropbox/android/taskqueue/ae;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/ae;-><init>(Lcom/dropbox/android/taskqueue/U;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/U;->a(Lcom/dropbox/android/util/aY;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v0

    .line 995
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
