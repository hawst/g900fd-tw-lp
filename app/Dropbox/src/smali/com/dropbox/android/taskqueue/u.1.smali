.class public abstract Lcom/dropbox/android/taskqueue/u;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/taskqueue/v;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field protected d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/dropbox/android/taskqueue/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/u;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput v1, p0, Lcom/dropbox/android/taskqueue/u;->d:I

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    .line 18
    iput-boolean v1, p0, Lcom/dropbox/android/taskqueue/u;->c:Z

    .line 108
    return-void
.end method


# virtual methods
.method public a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    .locals 3

    .prologue
    .line 187
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 188
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/v;

    .line 189
    invoke-interface {v0, p0, p1}, Lcom/dropbox/android/taskqueue/v;->b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V

    goto :goto_0

    .line 191
    :cond_0
    sget-object v0, Lcom/dropbox/android/taskqueue/u;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error in task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-object p1
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public final a(JJ)V
    .locals 7

    .prologue
    .line 164
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 165
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/v;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 166
    invoke-interface/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/v;->a(Lcom/dropbox/android/taskqueue/u;JJ)V

    goto :goto_0

    .line 168
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/v;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    return-void
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation
.end method

.method public final b(Lcom/dropbox/android/taskqueue/v;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 131
    return-void
.end method

.method public c()Lcom/dropbox/android/taskqueue/w;
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/dropbox/android/taskqueue/u;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/u;->d:I

    .line 153
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/u;->c:Z

    .line 117
    return-void
.end method

.method public h_()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method public i()Lcom/dropbox/android/taskqueue/w;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 172
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/v;

    .line 173
    invoke-interface {v0, p0}, Lcom/dropbox/android/taskqueue/v;->c(Lcom/dropbox/android/taskqueue/u;)V

    goto :goto_0

    .line 175
    :cond_0
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method

.method public i_()Lcom/dropbox/android/taskqueue/z;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/u;->c:Z

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/dropbox/android/taskqueue/u;->d:I

    return v0
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 157
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 158
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/v;

    .line 159
    invoke-interface {v0, p0}, Lcom/dropbox/android/taskqueue/v;->b(Lcom/dropbox/android/taskqueue/u;)V

    goto :goto_0

    .line 161
    :cond_0
    return-void
.end method

.method public final s()Lcom/dropbox/android/taskqueue/w;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/u;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 180
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/v;

    .line 181
    invoke-interface {v0, p0}, Lcom/dropbox/android/taskqueue/v;->d(Lcom/dropbox/android/taskqueue/u;)V

    goto :goto_0

    .line 183
    :cond_0
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method
