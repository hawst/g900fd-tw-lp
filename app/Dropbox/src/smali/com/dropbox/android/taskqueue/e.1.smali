.class public final Lcom/dropbox/android/taskqueue/e;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/taskqueue/i",
        "<",
        "Lcom/dropbox/android/taskqueue/CameraUploadTask;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/CameraUploadTask;
    .locals 15

    .prologue
    .line 673
    invoke-static/range {p3 .. p3}, Ldbxyzptlk/db231222/aj/d;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ldbxyzptlk/db231222/aj/c;

    .line 677
    const-string v0, "mBatchFileNumber"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 678
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v9, v2

    .line 680
    :goto_0
    new-instance v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    new-instance v3, Ljava/io/File;

    const-string v2, "mFilePath"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, "mServerHash"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v2, "mMimeType"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v2, "mDbRowId"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-string v2, "mContentUri"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v2, "mImportTime"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-string v2, "mImportTimeoffset"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const-string v2, "mRehashed"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/aj/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v14}, Lcom/dropbox/android/taskqueue/CameraUploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;ZLcom/dropbox/android/taskqueue/a;)V

    return-object v0

    .line 678
    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 696
    const-string v0, "com.dropbox.android.taskqueue.CameraUploadTask"

    return-object v0
.end method

.method public final synthetic b(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/DbTask;
    .locals 1

    .prologue
    .line 669
    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/android/taskqueue/e;->a(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-result-object v0

    return-object v0
.end method
