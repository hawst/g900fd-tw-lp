.class public Lcom/dropbox/android/taskqueue/DownloadTask;
.super Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final i:Lcom/dropbox/android/util/analytics/m;


# instance fields
.field protected final a:Lcom/dropbox/android/filemanager/LocalEntry;

.field private final c:Ldbxyzptlk/db231222/r/d;

.field private final e:Ldbxyzptlk/db231222/k/h;

.field private final f:Lcom/dropbox/android/util/DropboxPath;

.field private g:Ljava/io/File;

.field private h:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/dropbox/android/taskqueue/j;

    invoke-direct {v0}, Lcom/dropbox/android/taskqueue/j;-><init>()V

    sput-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->i:Lcom/dropbox/android/util/analytics/m;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->h:Ljava/io/OutputStream;

    .line 66
    iget-boolean v0, p2, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 67
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 69
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->e:Ldbxyzptlk/db231222/k/h;

    .line 71
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 72
    iput-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    .line 73
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->k()Lcom/dropbox/android/taskqueue/v;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 74
    return-void
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ldbxyzptlk/db231222/v/j;)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/provider/MetadataManager;->b(Ldbxyzptlk/db231222/v/j;)V

    .line 197
    :cond_0
    return-void
.end method

.method static synthetic j()Lcom/dropbox/android/util/analytics/m;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->i:Lcom/dropbox/android/util/analytics/m;

    return-object v0
.end method

.method private k()Lcom/dropbox/android/taskqueue/v;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/dropbox/android/taskqueue/k;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/k;-><init>(Lcom/dropbox/android/taskqueue/DownloadTask;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 432
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 433
    new-instance v1, Ldbxyzptlk/db231222/j/l;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    return-object v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 201
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 203
    new-instance v9, Lcom/dropbox/android/taskqueue/T;

    invoke-direct {v9}, Lcom/dropbox/android/taskqueue/T;-><init>()V

    .line 205
    :try_start_0
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->a()V

    .line 207
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->r()V

    .line 209
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    .line 213
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 214
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    :goto_0
    return-object v0

    .line 223
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_2

    move v6, v7

    .line 229
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;

    move-result-object v2

    .line 230
    invoke-direct {p0, v2}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Ldbxyzptlk/db231222/v/j;)V

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    if-eqz v0, :cond_3

    iget-object v0, v2, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 238
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->i()Lcom/dropbox/android/taskqueue/w;
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ldbxyzptlk/db231222/w/e; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    :cond_2
    move v6, v0

    .line 223
    goto :goto_1

    .line 242
    :cond_3
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ldbxyzptlk/db231222/w/e; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 243
    :try_start_4
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 244
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 249
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->e()Ljava/io/File;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v1

    .line 251
    if-nez v1, :cond_5

    .line 252
    :try_start_6
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->f()V

    .line 253
    sget-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    const-string v2, "Couldn\'t create temp file for download."

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto :goto_0

    .line 257
    :cond_5
    :try_start_8
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 276
    :try_start_9
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_9
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v8

    .line 280
    :goto_2
    :try_start_a
    new-instance v0, Ljava/security/DigestOutputStream;

    invoke-direct {v0, v3, v8}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->h:Ljava/io/OutputStream;

    .line 281
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 284
    :try_start_b
    new-instance v0, Lcom/dropbox/android/taskqueue/l;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/l;-><init>(Lcom/dropbox/android/taskqueue/DownloadTask;)V

    .line 291
    const-wide/16 v3, 0x0

    iget-object v5, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/LocalEntry;->c()J

    move-result-wide v10

    invoke-virtual {p0, v3, v4, v10, v11}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(JJ)V

    .line 292
    const-string v3, "net.start"

    invoke-static {v3, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 293
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    iget-object v5, v2, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    iget-object v10, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->h:Ljava/io/OutputStream;

    invoke-virtual {v3, v4, v5, v10, v0}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;

    move-result-object v0

    .line 294
    const-string v3, "net.end"

    invoke-static {v3, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 296
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->p()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 297
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 298
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 300
    :cond_6
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_b
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_b .. :try_end_b} :catch_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_b .. :try_end_b} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_b .. :try_end_b} :catch_4
    .catch Ldbxyzptlk/db231222/w/e; {:try_start_b .. :try_end_b} :catch_5
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_b .. :try_end_b} :catch_8
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 258
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 261
    :goto_3
    :try_start_c
    sget-object v2, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while downloading file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 263
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->f()V

    .line 264
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 265
    sget-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t create new file, USB or no SD: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 269
    :cond_7
    :try_start_d
    sget-object v2, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in download: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 277
    :catch_1
    move-exception v0

    .line 278
    :try_start_e
    sget-object v4, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    const-string v5, "Error with MD5 "

    invoke-static {v4, v5, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 281
    :catchall_0
    move-exception v0

    :goto_4
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :try_start_f
    throw v0
    :try_end_f
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_f .. :try_end_f} :catch_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_f .. :try_end_f} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_f .. :try_end_f} :catch_4
    .catch Ldbxyzptlk/db231222/w/e; {:try_start_f .. :try_end_f} :catch_5
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_f .. :try_end_f} :catch_8
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 330
    :catch_2
    move-exception v0

    .line 332
    if-nez v6, :cond_d

    .line 337
    :try_start_10
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 305
    :cond_8
    :try_start_11
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/v/g;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dropbox/android/filemanager/LocalEntry;->b(Ljava/lang/String;)V

    .line 307
    invoke-virtual {v0}, Ldbxyzptlk/db231222/v/g;->a()Ljava/lang/String;

    move-result-object v3

    .line 309
    if-eqz v3, :cond_c

    .line 310
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v4, v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ljava/lang/String;)V

    .line 315
    :cond_9
    :goto_5
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/v/g;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/filemanager/LocalEntry;->a(J)V

    .line 317
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/math/BigInteger;-><init>([B)V

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 319
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    if-nez v3, :cond_a

    .line 320
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v3, v4}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v3

    iput-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    .line 323
    :cond_a
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    .line 324
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->j()Ldbxyzptlk/db231222/k/c;

    move-result-object v3

    .line 325
    if-eqz v3, :cond_b

    .line 326
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v3, v4}, Ldbxyzptlk/db231222/k/c;->a(Ljava/io/File;)V

    .line 328
    :cond_b
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    .line 329
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_11
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_11 .. :try_end_11} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_11 .. :try_end_11} :catch_4
    .catch Ldbxyzptlk/db231222/w/e; {:try_start_11 .. :try_end_11} :catch_5
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_11 .. :try_end_11} :catch_8
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_11 .. :try_end_11} :catch_7
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 385
    :try_start_12
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 386
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->p()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 387
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 388
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 311
    :cond_c
    :try_start_13
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_9

    .line 312
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/v/g;->d()Ldbxyzptlk/db231222/v/j;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/v/j;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/dropbox/android/util/ab;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ljava/lang/String;)V
    :try_end_13
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_13 .. :try_end_13} :catch_2
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_13 .. :try_end_13} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_13 .. :try_end_13} :catch_4
    .catch Ldbxyzptlk/db231222/w/e; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_13 .. :try_end_13} :catch_7
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_5

    .line 341
    :catch_3
    move-exception v0

    .line 342
    :try_start_14
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-static {v0}, Lcom/dropbox/android/util/Activities;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 343
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 339
    :cond_d
    :try_start_15
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->i()Lcom/dropbox/android/taskqueue/w;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 344
    :catch_4
    move-exception v0

    .line 345
    :try_start_16
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x130

    if-ne v1, v2, :cond_e

    .line 346
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 347
    :cond_e
    :try_start_17
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x194

    if-eq v1, v2, :cond_f

    .line 348
    sget-object v1, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Interesting HTTP code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Ldbxyzptlk/db231222/w/i;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 351
    :cond_f
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 352
    :catch_5
    move-exception v0

    .line 353
    :try_start_18
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 354
    :catch_6
    move-exception v0

    move-object v1, v8

    .line 363
    :goto_6
    if-eqz v1, :cond_10

    :try_start_19
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 364
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 367
    :cond_10
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->p()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 368
    sget-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    const-string v1, "Download canceled by user, stopped after partial completion."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 371
    :cond_11
    if-nez v6, :cond_12

    .line 374
    :try_start_1a
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 376
    :cond_12
    :try_start_1b
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->i()Lcom/dropbox/android/taskqueue/w;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 379
    :catch_7
    move-exception v0

    .line 381
    :try_start_1c
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 382
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 391
    :cond_13
    :try_start_1d
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 392
    const-string v3, "_data"

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string v3, "revision"

    iget-object v4, v2, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v3, "local_revision"

    iget-object v2, v2, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v2, "local_bytes"

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 396
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 397
    const-string v4, "local_modified"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 398
    const-string v4, "local_hash"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 401
    const-string v0, "encoding"

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_14
    sget-object v0, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Downloaded local file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " modified is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Landroid/content/ContentValues;)I

    move-result v0

    .line 408
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    .line 409
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 410
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/dropbox/android/util/DropboxPath;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->a([Lcom/dropbox/android/util/DropboxPath;)V

    .line 412
    if-eq v0, v7, :cond_15

    .line 413
    sget-object v1, Lcom/dropbox/android/taskqueue/DownloadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t successfully update entry with: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Changed is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_15
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->i()Lcom/dropbox/android/taskqueue/w;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    .line 419
    :cond_16
    :try_start_1e
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    move-result-object v0

    .line 422
    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    invoke-virtual {v9}, Lcom/dropbox/android/taskqueue/T;->b()V

    throw v0

    .line 354
    :catch_8
    move-exception v0

    goto/16 :goto_6

    .line 281
    :catchall_2
    move-exception v0

    move-object v1, v8

    goto/16 :goto_4

    .line 258
    :catch_9
    move-exception v0

    goto/16 :goto_3
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 181
    monitor-enter p0

    .line 182
    :try_start_0
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue$SingleAttemptTask;->f()V

    .line 183
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->h:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 185
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->h:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0

    .line 189
    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 186
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method public final h()Ljava/io/File;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/DownloadTask;->g:Ljava/io/File;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DownloadTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
