.class public Lcom/dropbox/android/taskqueue/h;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/taskqueue/i",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/h;->a:Ljava/lang/Object;

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/h;->b:Ljava/util/HashMap;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/h;->c:Ljava/util/ArrayList;

    .line 33
    return-void
.end method

.method private static b(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 71
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/DbTask;
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/h;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/i;

    .line 101
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    invoke-static {v0, p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-interface {v0, p2, p3, p4}, Lcom/dropbox/android/taskqueue/i;->b(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v0

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {p1}, Lcom/dropbox/android/taskqueue/h;->b(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/h;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    monitor-exit v1

    return-object v0

    .line 64
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to get a canonical name for a class that hasn\'t been registered."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/h;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 113
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Class;Lcom/dropbox/android/taskqueue/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dropbox/android/taskqueue/DbTask;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/dropbox/android/taskqueue/i",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {p1}, Lcom/dropbox/android/taskqueue/h;->b(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-interface {p2}, Lcom/dropbox/android/taskqueue/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "Restorer name doesn\'t match class name."

    invoke-static {v1, v2}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/h;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 87
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/h;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    monitor-exit v1

    .line 90
    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
