.class final Lcom/dropbox/android/taskqueue/A;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/taskqueue/TaskQueue;


# direct methods
.method constructor <init>(Lcom/dropbox/android/taskqueue/TaskQueue;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 376
    .line 378
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue;)Ljava/util/PriorityQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->b(Lcom/dropbox/android/taskqueue/TaskQueue;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    :cond_0
    monitor-exit v1

    .line 394
    :goto_1
    return-void

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue;)Ljava/util/PriorityQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/C;

    .line 383
    iget-object v2, v0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 384
    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/C;->b:Z

    .line 385
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-static {v3}, Lcom/dropbox/android/taskqueue/TaskQueue;->c(Lcom/dropbox/android/taskqueue/TaskQueue;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 387
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    :try_start_2
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/A;->a:Lcom/dropbox/android/taskqueue/TaskQueue;

    invoke-static {v1, v2, v0}, Lcom/dropbox/android/taskqueue/TaskQueue;->a(Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;Z)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v0

    .line 392
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 387
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
.end method
