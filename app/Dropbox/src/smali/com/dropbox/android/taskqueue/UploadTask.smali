.class public Lcom/dropbox/android/taskqueue/UploadTask;
.super Lcom/dropbox/android/taskqueue/DbTask;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ldbxyzptlk/db231222/r/d;

.field private final e:Ldbxyzptlk/db231222/k/h;

.field private final f:Lcom/dropbox/android/util/DropboxPath;

.field private g:Lcom/dropbox/android/util/DropboxPath;

.field private final h:Landroid/net/Uri;

.field private final i:Ljava/lang/String;

.field private j:J

.field private final k:Z

.field private l:Ldbxyzptlk/db231222/z/aE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/z/aE",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/dropbox/android/taskqueue/UploadTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DbTask;-><init>()V

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    .line 80
    iput-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/db231222/z/aE;

    .line 512
    iput-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;

    .line 94
    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 95
    invoke-static {p5}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 96
    const-string v0, "/"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 97
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/UploadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    .line 99
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    .line 102
    invoke-static {p4}, Lcom/dropbox/android/util/ab;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    .line 103
    iput-object p5, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    .line 105
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    .line 107
    iput-boolean p6, p0, Lcom/dropbox/android/taskqueue/UploadTask;->k:Z

    .line 108
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/FileInputStream;JZLjava/lang/String;)Ldbxyzptlk/db231222/z/aE;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/io/FileInputStream;",
            "JZ",
            "Ljava/lang/String;",
            ")",
            "Ldbxyzptlk/db231222/z/aE",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    new-instance v8, Lcom/dropbox/android/taskqueue/al;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/dropbox/android/taskqueue/al;-><init>(Lcom/dropbox/android/taskqueue/UploadTask;)V

    .line 183
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v3

    .line 184
    const-wide/32 v4, 0x800000

    cmp-long v4, p3, v4

    if-lez v4, :cond_0

    .line 185
    move-object/from16 v0, p2

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v8}, Ldbxyzptlk/db231222/z/a;->a(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/g;

    move-result-object v8

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-virtual/range {v3 .. v8}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/FileInputStream;ZLjava/lang/String;Ldbxyzptlk/db231222/z/g;)Ldbxyzptlk/db231222/z/aE;

    move-result-object v3

    .line 190
    :goto_0
    return-object v3

    .line 187
    :cond_0
    if-eqz p5, :cond_1

    .line 188
    new-instance v9, Ldbxyzptlk/db231222/z/an;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/InputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;

    move-result-object v3

    invoke-direct {v9, v3}, Ldbxyzptlk/db231222/z/an;-><init>(Ldbxyzptlk/db231222/v/o;)V

    move-object v3, v9

    goto :goto_0

    .line 190
    :cond_1
    new-instance v4, Ldbxyzptlk/db231222/z/an;

    move-object v9, v3

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-wide/from16 v12, p3

    move-object/from16 v14, p6

    move-object v15, v8

    invoke-virtual/range {v9 .. v15}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;

    move-result-object v3

    invoke-direct {v4, v3}, Ldbxyzptlk/db231222/z/an;-><init>(Ldbxyzptlk/db231222/v/o;)V

    move-object v3, v4

    goto :goto_0
.end method

.method private a(J)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 159
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 160
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v2

    if-nez v2, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v1

    .line 166
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v2

    add-long/2addr v2, p1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->k()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 538
    new-instance v1, Ldbxyzptlk/db231222/j/l;

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->k()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    return-object v0
.end method

.method public final b(Lcom/dropbox/android/taskqueue/w;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 484
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    if-eq p1, v0, :cond_0

    .line 485
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 486
    const-string v1, "ARG_FILENAME"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const-string v1, "ARG_STATUS"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const-string v1, "ARG_INTENDED_FOLDER"

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 489
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aH;->a:Lcom/dropbox/android/util/aH;

    invoke-virtual {v1, v2, v3, v3, v0}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aH;Ljava/lang/String;Ljava/lang/Long;Landroid/os/Bundle;)I

    .line 491
    :cond_0
    return-void
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 17

    .prologue
    .line 195
    invoke-super/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/DbTask;->c()Lcom/dropbox/android/taskqueue/w;

    .line 197
    new-instance v14, Lcom/dropbox/android/taskqueue/T;

    invoke-direct {v14}, Lcom/dropbox/android/taskqueue/T;-><init>()V

    .line 198
    const/4 v3, 0x0

    .line 200
    :try_start_0
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->a()V

    .line 202
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uploading file from URI: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->k()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v15

    .line 207
    const/4 v11, 0x0

    .line 208
    const/4 v2, 0x0

    .line 209
    const/4 v4, 0x0

    .line 210
    const/4 v1, 0x0

    .line 213
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    .line 214
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    .line 215
    if-eqz v7, :cond_3

    if-eqz v5, :cond_3

    const-string v6, "content"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "com.dropbox.android.Dropbox"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    move v6, v5

    .line 217
    :goto_0
    if-eqz v7, :cond_4

    const-string v5, "file"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ldbxyzptlk/db231222/k/h;->a(Ljava/io/File;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    .line 219
    :goto_1
    if-nez v6, :cond_0

    if-eqz v5, :cond_1d

    .line 221
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v5, v6, v7}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Ljava/io/File;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v13

    .line 222
    if-eqz v13, :cond_1c

    .line 223
    :try_start_2
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_21
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 224
    :try_start_3
    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    .line 225
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v3, v13}, Ldbxyzptlk/db231222/k/h;->a(Ljava/io/File;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_22
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_20
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result v1

    if-eqz v1, :cond_5

    const/4 v9, 0x1

    .line 226
    :goto_2
    if-eqz v9, :cond_1b

    .line 227
    :try_start_4
    invoke-virtual {v15}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_23
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_20
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-object v10

    .line 228
    :try_start_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 229
    if-nez v11, :cond_1a

    .line 230
    const-string v2, "dropbox"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "local_revision"

    aput-object v5, v3, v4

    const-string v4, "canon_path = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v10, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 235
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 236
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_24
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_20
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result-object v1

    .line 238
    :goto_3
    :try_start_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_25
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_20
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-object v2, v10

    move-object v3, v1

    move-object v4, v12

    move v1, v9

    :goto_4
    move v10, v1

    move-object v8, v13

    move-object v11, v2

    move-object v7, v3

    move-object v2, v4

    .line 249
    :goto_5
    const/4 v1, 0x0

    .line 250
    if-nez v2, :cond_18

    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v3, v4, v5}, Lcom/dropbox/android/util/ab;->b(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    move-result v3

    if-nez v3, :cond_18

    .line 252
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    const-string v5, "r"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v4

    .line 253
    if-eqz v4, :cond_1

    .line 254
    sget-object v3, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AssetFileDescriptor: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", length = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v12

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1c
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    move-result-object v3

    .line 256
    :try_start_9
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    .line 257
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const-wide/16 v12, -0x1

    cmp-long v2, v4, v12

    if-nez v2, :cond_17

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/k/h;->e()Ljava/io/File;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1d
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_18
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_14
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-result-object v2

    .line 261
    if-eqz v2, :cond_6

    .line 262
    const/4 v1, 0x0

    .line 264
    :try_start_a
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 265
    :try_start_b
    invoke-static {v3, v4}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 266
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 267
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1f
    .catchall {:try_start_b .. :try_end_b} :catchall_a

    .line 268
    :try_start_c
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1f
    .catchall {:try_start_c .. :try_end_c} :catchall_b

    .line 273
    :try_start_d
    invoke-static {v4}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1e
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_d} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_d} :catch_15
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    :goto_6
    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    :cond_1
    :goto_7
    move-object v9, v1

    .line 291
    :goto_8
    if-nez v2, :cond_16

    .line 293
    :try_start_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v1, v3, v4}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 294
    if-eqz v3, :cond_15

    .line 295
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_7

    .line 296
    :cond_2
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    :goto_9
    return-object v1

    .line 215
    :cond_3
    const/4 v5, 0x0

    move v6, v5

    goto/16 :goto_0

    .line 217
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 225
    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 242
    :catch_0
    move-exception v5

    move v9, v1

    move-object v10, v2

    move-object v12, v3

    move-object v1, v4

    :goto_a
    move-object v8, v1

    move-object v7, v11

    move-object v2, v12

    move-object v11, v10

    move v10, v9

    .line 245
    goto/16 :goto_5

    .line 243
    :catch_1
    move-exception v1

    move-object v12, v3

    .line 244
    :goto_b
    :try_start_f
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->g:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v12}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_9

    .line 269
    :catch_2
    move-exception v3

    move-object v3, v1

    .line 270
    :goto_c
    const/4 v1, 0x0

    .line 271
    const-wide/16 v4, -0x1

    :try_start_10
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_c

    .line 273
    :try_start_11
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_11 .. :try_end_11} :catch_1a
    .catch Ljava/lang/SecurityException; {:try_start_11 .. :try_end_11} :catch_16
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    goto :goto_6

    .line 281
    :catch_3
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    :goto_d
    move-object v9, v1

    .line 287
    goto :goto_8

    .line 273
    :catchall_0
    move-exception v4

    move-object/from16 v16, v4

    move-object v4, v1

    move-object/from16 v1, v16

    :goto_e
    :try_start_12
    invoke-static {v4}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v1
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_12 .. :try_end_12} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_12} :catch_17
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    .line 281
    :catch_4
    move-exception v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_d

    .line 276
    :cond_6
    const/4 v1, 0x0

    .line 277
    const-wide/16 v3, -0x1

    :try_start_13
    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_13 .. :try_end_13} :catch_1a
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_13} :catch_16
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_7

    .line 282
    :catch_5
    move-exception v3

    :goto_f
    move-object v9, v1

    .line 287
    goto/16 :goto_8

    .line 285
    :catch_6
    move-exception v1

    .line 286
    :goto_10
    :try_start_14
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->g:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_9

    .line 299
    :cond_7
    :try_start_15
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_7
    .catch Ljava/lang/SecurityException; {:try_start_15 .. :try_end_15} :catch_8
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    .line 300
    :try_start_16
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_13
    .catch Ljava/lang/SecurityException; {:try_start_16 .. :try_end_16} :catch_12
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    :goto_11
    move-object v3, v1

    .line 309
    :goto_12
    if-eqz v3, :cond_8

    :try_start_17
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-gez v1, :cond_9

    .line 310
    :cond_8
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 302
    :catch_7
    move-exception v1

    :goto_13
    move-object v3, v2

    .line 305
    goto :goto_12

    .line 303
    :catch_8
    move-exception v1

    .line 304
    :goto_14
    :try_start_18
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->g:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_8

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 314
    :cond_9
    :try_start_19
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/UploadTask;->a(J)Z
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    move-result v1

    if-nez v1, :cond_a

    .line 318
    :try_start_1a
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v1

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    sget-object v4, Lcom/dropbox/android/service/e;->b:Lcom/dropbox/android/service/e;

    invoke-virtual {v2, v4, v1}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;

    .line 320
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/UploadTask;->a(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 321
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:I

    .line 322
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aM;->f:Lcom/dropbox/android/util/aM;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aM;->b:Lcom/dropbox/android/util/aM;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 324
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_1a
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1a .. :try_end_1a} :catch_9
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 327
    :catch_9
    move-exception v1

    .line 329
    :try_start_1b
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 332
    :cond_a
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aM;->b:Lcom/dropbox/android/util/aM;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 334
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->p()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 338
    :cond_b
    :try_start_1d
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->r()V

    .line 340
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uploading uri "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    .line 344
    :try_start_1e
    monitor-enter p0
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_1e .. :try_end_1e} :catch_b
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_1e .. :try_end_1e} :catch_c
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_1e .. :try_end_1e} :catch_d
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_1e .. :try_end_1e} :catch_e
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_1e .. :try_end_1e} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1e .. :try_end_1e} :catch_11
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    .line 345
    :try_start_1f
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->p()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 346
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 348
    :cond_c
    :try_start_20
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    .line 352
    :try_start_21
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->k:Z

    if-nez v1, :cond_d

    if-eqz v10, :cond_e

    invoke-static {v7}, Lcom/dropbox/android/util/bh;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 357
    :cond_d
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v1, p0

    move-object v2, v15

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/FileInputStream;JZLjava/lang/String;)Ldbxyzptlk/db231222/z/aE;

    move-result-object v1

    .line 365
    :goto_15
    monitor-enter p0
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_21 .. :try_end_21} :catch_b
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_21 .. :try_end_21} :catch_c
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_21 .. :try_end_21} :catch_d
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_21 .. :try_end_21} :catch_e
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_21 .. :try_end_21} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_21 .. :try_end_21} :catch_11
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    .line 366
    :try_start_22
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->p()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 367
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    monitor-exit p0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_2

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 348
    :catchall_1
    move-exception v1

    :try_start_23
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    :try_start_24
    throw v1
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_24 .. :try_end_24} :catch_b
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_24 .. :try_end_24} :catch_c
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_24 .. :try_end_24} :catch_d
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_24 .. :try_end_24} :catch_e
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_24 .. :try_end_24} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_24 .. :try_end_24} :catch_11
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    .line 375
    :catch_a
    move-exception v1

    .line 376
    :try_start_25
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    const-string v4, "IO exception while starting upload"

    invoke-static {v2, v4, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 377
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 362
    :cond_e
    :try_start_26
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object v2, v15

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/FileInputStream;JZLjava/lang/String;)Ldbxyzptlk/db231222/z/aE;
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_26 .. :try_end_26} :catch_b
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_26 .. :try_end_26} :catch_c
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_26 .. :try_end_26} :catch_d
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_26 .. :try_end_26} :catch_e
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_26 .. :try_end_26} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_26 .. :try_end_26} :catch_11
    .catchall {:try_start_26 .. :try_end_26} :catchall_3

    move-result-object v1

    goto :goto_15

    .line 369
    :cond_f
    :try_start_27
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/db231222/z/aE;

    .line 370
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_2

    .line 372
    :try_start_28
    const-string v1, "net.start"

    move-object/from16 v0, p0

    invoke-static {v1, v0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 373
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/db231222/z/aE;

    invoke-interface {v1}, Ldbxyzptlk/db231222/z/aE;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/v/j;

    .line 374
    const-string v2, "net.end"

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_28 .. :try_end_28} :catch_b
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_28 .. :try_end_28} :catch_c
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_28 .. :try_end_28} :catch_d
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_28 .. :try_end_28} :catch_e
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_28 .. :try_end_28} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_28 .. :try_end_28} :catch_11
    .catchall {:try_start_28 .. :try_end_28} :catchall_3

    .line 422
    if-eqz v9, :cond_10

    .line 423
    :try_start_29
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 429
    :cond_10
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v2, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->g:Lcom/dropbox/android/util/DropboxPath;

    .line 430
    if-eqz v10, :cond_14

    .line 431
    new-instance v2, Ldbxyzptlk/db231222/k/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-direct {v2, v4, v8}, Ldbxyzptlk/db231222/k/f;-><init>(Ldbxyzptlk/db231222/k/h;Ljava/io/File;)V

    invoke-virtual {v2}, Ldbxyzptlk/db231222/k/f;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    .line 432
    iget-object v4, v1, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->g:Lcom/dropbox/android/util/DropboxPath;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v2, v4}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v2

    .line 435
    invoke-virtual {v8, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 442
    :goto_16
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 443
    const-string v5, "_data"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 444
    const-string v5, "local_hash"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 445
    const-string v5, "local_modified"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 446
    const-string v5, "local_revision"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 448
    const-string v6, "dropbox"

    const-string v7, "canon_path = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v11, v8, v9

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 456
    :goto_17
    invoke-static {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->b(Ldbxyzptlk/db231222/v/j;)Landroid/content/ContentValues;

    move-result-object v4

    .line 457
    if-eqz v10, :cond_11

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 458
    const-string v5, "_data"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v5, "local_hash"

    invoke-static {v2}, Ldbxyzptlk/db231222/k/a;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    .line 461
    const-string v2, "local_modified"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 462
    const-string v2, "local_revision"

    iget-object v1, v1, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v4, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Uploaded file modified at: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->g:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1, v2, v4}, Lcom/dropbox/android/provider/MetadataManager;->a(Lcom/dropbox/android/util/DropboxPath;Landroid/content/ContentValues;)I

    .line 467
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->i()Lcom/dropbox/android/taskqueue/w;
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 370
    :catchall_2
    move-exception v1

    :try_start_2a
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_2

    :try_start_2b
    throw v1
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2b} :catch_a
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_2b .. :try_end_2b} :catch_b
    .catch Ldbxyzptlk/db231222/w/g; {:try_start_2b .. :try_end_2b} :catch_c
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_2b .. :try_end_2b} :catch_d
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_2b .. :try_end_2b} :catch_e
    .catch Ldbxyzptlk/db231222/w/f; {:try_start_2b .. :try_end_2b} :catch_10
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2b .. :try_end_2b} :catch_11
    .catchall {:try_start_2b .. :try_end_2b} :catchall_3

    .line 378
    :catch_b
    move-exception v1

    .line 379
    :try_start_2c
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IO Exception uploading: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v5}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 380
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 381
    :catch_c
    move-exception v1

    .line 382
    :try_start_2d
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    const-string v2, "Upload canceled"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Lcom/dropbox/android/taskqueue/w;
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 384
    :catch_d
    move-exception v1

    .line 385
    :try_start_2e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-static {v1}, Lcom/dropbox/android/util/Activities;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 386
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 387
    :catch_e
    move-exception v1

    .line 388
    :try_start_2f
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    const-string v4, "Server exception uploading."

    invoke-static {v2, v4}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget v2, v1, Ldbxyzptlk/db231222/w/i;->b:I

    packed-switch v2, :pswitch_data_0

    .line 406
    :pswitch_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 407
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 391
    :pswitch_1
    :try_start_30
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:I
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_3

    .line 393
    :try_start_31
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v1

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    sget-object v4, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    invoke-virtual {v2, v4, v1}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    :try_end_31
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_31 .. :try_end_31} :catch_f
    .catchall {:try_start_31 .. :try_end_31} :catchall_3

    .line 400
    :try_start_32
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aM;->b:Lcom/dropbox/android/util/aM;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 401
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 395
    :catch_f
    move-exception v1

    .line 398
    :try_start_33
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 404
    :pswitch_2
    :try_start_34
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 409
    :catch_10
    move-exception v1

    .line 410
    :try_start_35
    invoke-virtual {v1}, Ldbxyzptlk/db231222/w/f;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v4, "5xx"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 411
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 413
    :cond_12
    :try_start_36
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 414
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 416
    :catch_11
    move-exception v1

    .line 417
    :try_start_37
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception uploading: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v5}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 419
    sget-object v1, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_3

    move-result-object v1

    .line 469
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_9

    .line 438
    :cond_13
    :try_start_38
    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_3

    move-object v2, v8

    goto/16 :goto_16

    .line 469
    :catchall_3
    move-exception v1

    :goto_18
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/T;->b()V

    .line 470
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v1

    .line 469
    :catchall_4
    move-exception v1

    move-object v3, v12

    goto :goto_18

    :catchall_5
    move-exception v1

    move-object v3, v2

    goto :goto_18

    :catchall_6
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    goto :goto_18

    :catchall_7
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    goto :goto_18

    :catchall_8
    move-exception v1

    move-object v3, v2

    goto :goto_18

    :catchall_9
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    goto :goto_18

    .line 303
    :catch_12
    move-exception v2

    move-object v2, v1

    goto/16 :goto_14

    .line 302
    :catch_13
    move-exception v2

    move-object v2, v1

    goto/16 :goto_13

    .line 285
    :catch_14
    move-exception v1

    move-object v2, v3

    goto/16 :goto_10

    :catch_15
    move-exception v2

    move-object v2, v1

    goto/16 :goto_10

    :catch_16
    move-exception v2

    move-object v2, v1

    goto/16 :goto_10

    :catch_17
    move-exception v1

    move-object v2, v3

    goto/16 :goto_10

    .line 282
    :catch_18
    move-exception v2

    move-object v2, v3

    goto/16 :goto_f

    :catch_19
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_f

    :catch_1a
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_f

    .line 281
    :catch_1c
    move-exception v3

    goto/16 :goto_d

    :catch_1d
    move-exception v2

    move-object v2, v3

    goto/16 :goto_d

    :catch_1e
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_d

    .line 273
    :catchall_a
    move-exception v1

    goto/16 :goto_e

    :catchall_b
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v1

    move-object/from16 v1, v16

    goto/16 :goto_e

    :catchall_c
    move-exception v4

    move-object/from16 v16, v4

    move-object v4, v3

    move-object v3, v1

    move-object/from16 v1, v16

    goto/16 :goto_e

    .line 269
    :catch_1f
    move-exception v1

    move-object v3, v4

    goto/16 :goto_c

    .line 243
    :catch_20
    move-exception v1

    goto/16 :goto_b

    .line 242
    :catch_21
    move-exception v4

    move v9, v1

    move-object v10, v2

    move-object v12, v3

    move-object v1, v13

    goto/16 :goto_a

    :catch_22
    move-exception v3

    move v9, v1

    move-object v10, v2

    move-object v1, v13

    goto/16 :goto_a

    :catch_23
    move-exception v1

    move-object v1, v13

    move-object v10, v2

    goto/16 :goto_a

    :catch_24
    move-exception v1

    move-object v1, v13

    goto/16 :goto_a

    :catch_25
    move-exception v2

    move-object v11, v1

    move-object v1, v13

    goto/16 :goto_a

    :cond_14
    move-object v2, v8

    goto/16 :goto_17

    :cond_15
    move-object v1, v2

    goto/16 :goto_11

    :cond_16
    move-object v3, v2

    goto/16 :goto_12

    :cond_17
    move-object v2, v3

    goto/16 :goto_7

    :cond_18
    move-object v9, v1

    goto/16 :goto_8

    :cond_19
    move-object v1, v11

    goto/16 :goto_3

    :cond_1a
    move v1, v9

    move-object v2, v10

    move-object v3, v11

    move-object v4, v12

    goto/16 :goto_4

    :cond_1b
    move v1, v9

    move-object v3, v11

    move-object v4, v12

    goto/16 :goto_4

    :cond_1c
    move-object v4, v3

    move-object v3, v11

    goto/16 :goto_4

    :cond_1d
    move v10, v1

    move-object v8, v4

    move-object v7, v11

    move-object v11, v2

    move-object v2, v3

    goto/16 :goto_5

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1f6
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 150
    monitor-enter p0

    .line 151
    :try_start_0
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->f()V

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/db231222/z/aE;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/db231222/z/aE;

    invoke-interface {v0}, Ldbxyzptlk/db231222/z/aE;->a()V

    .line 155
    :cond_0
    monitor-exit p0

    .line 156
    return-void

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method protected final h()J
    .locals 2

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    return-wide v0
.end method

.method public final i_()Lcom/dropbox/android/taskqueue/z;
    .locals 4

    .prologue
    .line 127
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->c:Lcom/dropbox/android/taskqueue/z;

    .line 136
    :goto_0
    return-object v0

    .line 132
    :cond_0
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->f:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0

    .line 136
    :cond_1
    sget-object v0, Lcom/dropbox/android/taskqueue/z;->a:Lcom/dropbox/android/taskqueue/z;

    goto :goto_0
.end method

.method public final j()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    return-object v0
.end method

.method public final k()Lcom/dropbox/android/util/DropboxPath;
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 524
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 525
    :catch_0
    move-exception v0

    .line 528
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 529
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 3

    .prologue
    .line 495
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 496
    const-string v1, "mDropboxDir"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->f:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    const-string v1, "mLocalUri"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    const-string v1, "mDestinationFilename"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    const-string v1, "mOverwrite"

    iget-boolean v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    invoke-static {v0}, Ldbxyzptlk/db231222/aj/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->g:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UploadTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    return-object v0
.end method
