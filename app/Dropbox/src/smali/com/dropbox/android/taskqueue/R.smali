.class public final Lcom/dropbox/android/taskqueue/R;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TT;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/R;->a:Ljava/util/HashMap;

    .line 61
    iput-wide p1, p0, Lcom/dropbox/android/taskqueue/R;->b:J

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/R;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/R;->a:Ljava/util/HashMap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    monitor-exit v1

    .line 68
    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/R;->a:Ljava/util/HashMap;

    monitor-enter v3

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/R;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 79
    if-nez v0, :cond_0

    .line 80
    monitor-exit v3

    move v0, v1

    .line 87
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/R;->b:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v2

    .line 84
    :goto_1
    if-eqz v0, :cond_1

    .line 85
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/R;->a:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_1
    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    monitor-exit v3

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    .line 83
    goto :goto_1

    :cond_3
    move v0, v1

    .line 87
    goto :goto_2
.end method
