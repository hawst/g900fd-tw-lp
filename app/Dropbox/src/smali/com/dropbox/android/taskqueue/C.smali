.class final Lcom/dropbox/android/taskqueue/C;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/dropbox/android/taskqueue/C",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final b:Z

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ZI)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    .line 59
    iput-boolean p2, p0, Lcom/dropbox/android/taskqueue/C;->b:Z

    .line 60
    iput p3, p0, Lcom/dropbox/android/taskqueue/C;->c:I

    .line 61
    return-void
.end method

.method private static a(II)I
    .locals 1

    .prologue
    .line 64
    if-ge p0, p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    if-le p0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/C;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/taskqueue/C",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->q()I

    move-result v0

    iget-object v1, p1, Lcom/dropbox/android/taskqueue/C;->a:Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/TaskQueue$BaseTask;->q()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/taskqueue/C;->a(II)I

    move-result v0

    .line 71
    if-eqz v0, :cond_0

    .line 80
    :goto_0
    return v0

    .line 75
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/C;->b:Z

    iget-boolean v1, p1, Lcom/dropbox/android/taskqueue/C;->b:Z

    if-eq v0, v1, :cond_2

    .line 76
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/C;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 80
    :cond_2
    iget v0, p0, Lcom/dropbox/android/taskqueue/C;->c:I

    iget v1, p1, Lcom/dropbox/android/taskqueue/C;->c:I

    invoke-static {v0, v1}, Lcom/dropbox/android/taskqueue/C;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 49
    check-cast p1, Lcom/dropbox/android/taskqueue/C;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/C;->a(Lcom/dropbox/android/taskqueue/C;)I

    move-result v0

    return v0
.end method
