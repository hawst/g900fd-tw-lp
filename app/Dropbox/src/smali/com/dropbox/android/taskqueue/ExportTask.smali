.class public Lcom/dropbox/android/taskqueue/ExportTask;
.super Lcom/dropbox/android/taskqueue/DownloadTask;
.source "panda.py"


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field protected final b:Ljava/io/File;

.field protected final c:Z

.field private final f:Lcom/dropbox/android/filemanager/I;

.field private final g:Ldbxyzptlk/db231222/k/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/dropbox/android/taskqueue/ExportTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/ExportTask;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 26
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/ExportTask;->b:Ljava/io/File;

    .line 27
    iput-boolean p4, p0, Lcom/dropbox/android/taskqueue/ExportTask;->c:Z

    .line 28
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/ExportTask;->f:Lcom/dropbox/android/filemanager/I;

    .line 29
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/ExportTask;->g:Ldbxyzptlk/db231222/k/h;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;
    .locals 5

    .prologue
    .line 48
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d00ca

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/ExportTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/bl;->a(I[Ljava/lang/Object;)V

    .line 49
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/w;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/dropbox/android/taskqueue/w;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 34
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ExportTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ExportTask;->g:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v1

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ExportTask;->f:Lcom/dropbox/android/filemanager/I;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/ExportTask;->b:Ljava/io/File;

    iget-boolean v3, p0, Lcom/dropbox/android/taskqueue/ExportTask;->c:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/I;->a(Ljava/io/File;Ljava/io/File;Z)Ljava/io/File;

    .line 37
    const-string v0, "export.success"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/ExportTask;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0, v2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 38
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v2, 0x7f0d00cb

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/util/bl;->a(I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->i()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    return-object v0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    sget-object v2, Lcom/dropbox/android/taskqueue/ExportTask;->e:Ljava/lang/String;

    const-string v3, "exportCachedFile failed"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v2, 0x7f0d00ca

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/util/bl;->a(I[Ljava/lang/Object;)V

    goto :goto_0
.end method
