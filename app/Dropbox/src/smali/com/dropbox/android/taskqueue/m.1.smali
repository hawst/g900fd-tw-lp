.class public Lcom/dropbox/android/taskqueue/m;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/dropbox/android/taskqueue/u;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/android/service/G;

.field private final c:J

.field private final d:J

.field private final e:Lcom/dropbox/android/service/I;

.field private final f:Lcom/dropbox/android/util/bc;

.field private final g:Lcom/dropbox/android/taskqueue/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/p",
            "<TT;>;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/service/G;Lcom/dropbox/android/taskqueue/p;JJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/service/G;",
            "Lcom/dropbox/android/taskqueue/p",
            "<TT;>;JJ)V"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v7, Lcom/dropbox/android/util/bc;

    invoke-direct {v7}, Lcom/dropbox/android/util/bc;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/taskqueue/m;-><init>(Lcom/dropbox/android/service/G;Lcom/dropbox/android/taskqueue/p;JJLcom/dropbox/android/util/bc;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/service/G;Lcom/dropbox/android/taskqueue/p;JJLcom/dropbox/android/util/bc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/service/G;",
            "Lcom/dropbox/android/taskqueue/p",
            "<TT;>;JJ",
            "Lcom/dropbox/android/util/bc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/dropbox/android/taskqueue/n;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/n;-><init>(Lcom/dropbox/android/taskqueue/m;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/service/I;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/m;->h:Z

    .line 85
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/service/G;

    .line 86
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/m;->g:Lcom/dropbox/android/taskqueue/p;

    .line 87
    iput-wide p3, p0, Lcom/dropbox/android/taskqueue/m;->c:J

    .line 88
    iput-wide p5, p0, Lcom/dropbox/android/taskqueue/m;->d:J

    .line 89
    iput-object p7, p0, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/util/bc;

    .line 90
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/p;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/m;->g:Lcom/dropbox/android/taskqueue/p;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 97
    monitor-enter p0

    if-lez p1, :cond_1

    .line 98
    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/m;->h:Z

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/service/G;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/m;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 103
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/m;->h:Z

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/service/G;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/m;->h:Z

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/util/bc;

    invoke-virtual {v0}, Lcom/dropbox/android/util/bc;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/dropbox/android/taskqueue/u;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/m;->c:J

    long-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->q()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 116
    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/m;->d:J

    long-to-double v2, v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-long v0, v0

    .line 117
    sget-object v2, Lcom/dropbox/android/taskqueue/m;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Task "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " transiently failed. rescheduling for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millis"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/util/bc;

    new-instance v3, Lcom/dropbox/android/taskqueue/o;

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/m;->g:Lcom/dropbox/android/taskqueue/p;

    invoke-direct {v3, v4, p1}, Lcom/dropbox/android/taskqueue/o;-><init>(Lcom/dropbox/android/taskqueue/p;Lcom/dropbox/android/taskqueue/u;)V

    invoke-virtual {v2, v3, v0, v1}, Lcom/dropbox/android/util/bc;->a(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
