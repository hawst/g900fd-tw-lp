.class public final Lcom/dropbox/android/taskqueue/S;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/z/M;

.field private final b:Ldbxyzptlk/db231222/k/h;

.field private final c:Lcom/dropbox/android/taskqueue/R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/taskqueue/R",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;)V
    .locals 3

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-instance v0, Lcom/dropbox/android/taskqueue/R;

    const-wide/32 v1, 0x5265c00

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/R;-><init>(J)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/S;->c:Lcom/dropbox/android/taskqueue/R;

    .line 114
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/S;->a:Ldbxyzptlk/db231222/z/M;

    .line 115
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/S;->b:Ldbxyzptlk/db231222/k/h;

    .line 116
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/TaskQueue;)Lcom/dropbox/android/taskqueue/ThumbnailTask;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/v/n;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/taskqueue/TaskQueue",
            "<",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;",
            ">;)",
            "Lcom/dropbox/android/taskqueue/ThumbnailTask;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/S;->c:Lcom/dropbox/android/taskqueue/R;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/R;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    :goto_0
    return-object v9

    :cond_0
    new-instance v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/S;->a:Ldbxyzptlk/db231222/z/M;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/S;->b:Ldbxyzptlk/db231222/k/h;

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/S;->c:Lcom/dropbox/android/taskqueue/R;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v9}, Lcom/dropbox/android/taskqueue/ThumbnailTask;-><init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/taskqueue/R;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/TaskQueue;Lcom/dropbox/android/taskqueue/Q;)V

    move-object v9, v0

    goto :goto_0
.end method
