.class final Lcom/dropbox/android/taskqueue/G;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/taskqueue/D;


# direct methods
.method constructor <init>(Lcom/dropbox/android/taskqueue/D;)V
    .locals 0

    .prologue
    .line 609
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/G;->a:Lcom/dropbox/android/taskqueue/D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/u;JJ)V
    .locals 0

    .prologue
    .line 648
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 644
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 3

    .prologue
    .line 638
    check-cast p1, Lcom/dropbox/android/taskqueue/ThumbnailTask;

    .line 639
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/G;->a:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->j()Ldbxyzptlk/db231222/v/n;

    move-result-object v2

    invoke-static {v0, v1, v2, p2}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/D;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V

    .line 640
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/u;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 613
    check-cast p1, Lcom/dropbox/android/taskqueue/ThumbnailTask;

    .line 615
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->j()Ldbxyzptlk/db231222/v/n;

    move-result-object v1

    .line 616
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->g()Ljava/lang/String;

    move-result-object v2

    .line 618
    if-nez v2, :cond_0

    .line 619
    invoke-static {}, Lcom/dropbox/android/taskqueue/D;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Completed thumb task, but unknown revision! Can\'t update thumbnail store db without knowing the revision!"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :goto_0
    return-void

    .line 623
    :cond_0
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/G;->a:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v2}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Ljava/lang/String;)V

    .line 624
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->k()Lcom/dropbox/android/taskqueue/TaskQueue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/TaskQueue;->c()I

    move-result v3

    if-gt v3, v0, :cond_2

    .line 625
    :goto_1
    if-eqz v0, :cond_1

    .line 626
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/G;->a:Lcom/dropbox/android/taskqueue/D;

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/D;)V

    .line 629
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/G;->a:Lcom/dropbox/android/taskqueue/D;

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-static {v0, v3, v2, v1}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/D;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    goto :goto_0

    .line 624
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d(Lcom/dropbox/android/taskqueue/u;)V
    .locals 0

    .prologue
    .line 634
    return-void
.end method
