.class public abstract Lcom/dropbox/android/activity/base/BaseDialogFragment;
.super Lcom/actionbarsherlock/app/SherlockDialogFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/base/m;
.implements Lcom/dropbox/android/activity/base/n;


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;-><init>()V

    .line 22
    new-instance v0, Lcom/dropbox/android/activity/base/c;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/base/c;-><init>(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/activity/base/m;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    return-void
.end method

.method public static final a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/dropbox/android/activity/base/BaseDialogFragment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 100
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public final a(Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/c;->a(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->e()V

    .line 161
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/activity/base/c;->a(IILandroid/content/Intent;)V

    .line 83
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 135
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->a()V

    .line 136
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 70
    const-string v0, "cancel"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/DialogFragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 71
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 72
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->c()V

    .line 148
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/base/c;->d()V

    .line 154
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDestroy()V

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->k()V

    .line 179
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 30
    :cond_0
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDestroyView()V

    .line 31
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->j()V

    .line 32
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDetach()V

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->b()V

    .line 142
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "dismiss"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/DialogFragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 77
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 78
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onPause()V

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->h()V

    .line 167
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onResume()V

    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->g()V

    .line 38
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onStart()V

    .line 107
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    new-instance v1, Lcom/dropbox/android/activity/base/b;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/base/b;-><init>(Lcom/dropbox/android/activity/base/BaseDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->f()V

    .line 130
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onStop()V

    .line 172
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->i()V

    .line 173
    return-void
.end method

.method public final show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 56
    const-string v0, "show"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/DialogFragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 57
    invoke-super {p0, p1, p2}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    const-string v0, "show"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/DialogFragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 51
    invoke-super {p0, p1, p2}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 52
    return-void
.end method
