.class public final Lcom/dropbox/android/activity/lock/n;
.super Landroid/app/DialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)V
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lcom/dropbox/android/activity/lock/n;

    invoke-direct {v0}, Lcom/dropbox/android/activity/lock/n;-><init>()V

    .line 186
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/lock/n;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 187
    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/n;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;

    .line 208
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    .line 209
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 191
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/n;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;

    .line 192
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 193
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 194
    const v2, 0x7f0d0014

    new-instance v3, Lcom/dropbox/android/activity/lock/o;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/lock/o;-><init>(Lcom/dropbox/android/activity/lock/n;Lcom/dropbox/android/activity/lock/LockCodeActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 200
    invoke-static {v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(Lcom/dropbox/android/activity/lock/LockCodeActivity;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea5f

    add-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    long-to-int v2, v2

    .line 201
    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0016

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 202
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
