.class public abstract enum Lcom/dropbox/android/activity/lock/f;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/lock/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/lock/f;

.field public static final enum b:Lcom/dropbox/android/activity/lock/f;

.field public static final enum c:Lcom/dropbox/android/activity/lock/f;

.field public static final enum d:Lcom/dropbox/android/activity/lock/f;

.field public static final enum e:Lcom/dropbox/android/activity/lock/f;

.field private static final f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/dropbox/android/activity/lock/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic h:[Lcom/dropbox/android/activity/lock/f;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lcom/dropbox/android/activity/lock/g;

    const-string v1, "LOCK_SCREEN"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/lock/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->a:Lcom/dropbox/android/activity/lock/f;

    .line 68
    new-instance v0, Lcom/dropbox/android/activity/lock/h;

    const-string v1, "NEW_CODE"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/lock/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->b:Lcom/dropbox/android/activity/lock/f;

    .line 88
    new-instance v0, Lcom/dropbox/android/activity/lock/i;

    const-string v1, "REMOVE_CODE"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/lock/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->c:Lcom/dropbox/android/activity/lock/f;

    .line 104
    new-instance v0, Lcom/dropbox/android/activity/lock/j;

    const-string v1, "CHANGE_CODE"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/lock/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->d:Lcom/dropbox/android/activity/lock/f;

    .line 131
    new-instance v0, Lcom/dropbox/android/activity/lock/k;

    const-string v1, "REMOVE_ERASE_ON_FAILURE"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/activity/lock/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->e:Lcom/dropbox/android/activity/lock/f;

    .line 52
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/activity/lock/f;

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->a:Lcom/dropbox/android/activity/lock/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->b:Lcom/dropbox/android/activity/lock/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->c:Lcom/dropbox/android/activity/lock/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->d:Lcom/dropbox/android/activity/lock/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->e:Lcom/dropbox/android/activity/lock/f;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->h:[Lcom/dropbox/android/activity/lock/f;

    .line 149
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/lock/f;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 155
    const-class v0, Lcom/dropbox/android/activity/lock/f;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/f;

    .line 156
    sget-object v2, Lcom/dropbox/android/activity/lock/f;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/f;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 158
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 161
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/f;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/dropbox/android/activity/lock/f;->g:I

    .line 162
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/lock/a;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/lock/f;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/dropbox/android/activity/lock/f;
    .locals 2

    .prologue
    .line 168
    sget-object v0, Lcom/dropbox/android/activity/lock/f;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/f;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/lock/f;
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/dropbox/android/activity/lock/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/f;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/lock/f;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/dropbox/android/activity/lock/f;->h:[Lcom/dropbox/android/activity/lock/f;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/lock/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/lock/f;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/dropbox/android/activity/lock/f;->g:I

    return v0
.end method

.method public abstract a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I
.end method

.method public abstract a(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)V
.end method
