.class public final Lcom/dropbox/android/activity/base/c;
.super Lcom/dropbox/android/activity/base/a;
.source "panda.py"


# instance fields
.field private a:Landroid/support/v4/app/Fragment;

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private c:J


# direct methods
.method constructor <init>(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/activity/base/m;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p2}, Lcom/dropbox/android/activity/base/a;-><init>(Lcom/dropbox/android/activity/base/m;)V

    .line 15
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/c;->b:Ljava/util/Queue;

    .line 20
    iput-object p1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    .line 21
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "attach"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 25
    return-void
.end method

.method final a(Ljava/lang/Runnable;)Z
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isResumed()Z

    move-result v0

    .line 87
    if-eqz v0, :cond_0

    .line 88
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 93
    :goto_0
    return v0

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->b:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "detach"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 29
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    .line 32
    const-string v0, "create"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 33
    return-void
.end method

.method final d()V
    .locals 2

    .prologue
    .line 36
    const-string v0, "create.view"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 37
    return-void
.end method

.method final e()V
    .locals 2

    .prologue
    .line 40
    const-string v0, "activity.created"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 41
    return-void
.end method

.method final f()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "start"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 45
    return-void
.end method

.method final g()V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/activity/base/c;->c:J

    .line 49
    const-string v0, "resume"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 51
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/base/c;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/dropbox/android/activity/base/c;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method final h()V
    .locals 4

    .prologue
    .line 57
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dropbox/android/activity/base/c;->c:J

    sub-long/2addr v0, v2

    .line 58
    const-string v2, "pause"

    iget-object v3, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "resumed_duration_millis"

    invoke-virtual {v2, v3, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 61
    return-void
.end method

.method final i()V
    .locals 2

    .prologue
    .line 64
    const-string v0, "stop"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 65
    return-void
.end method

.method final j()V
    .locals 2

    .prologue
    .line 68
    const-string v0, "destroy.view"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 69
    return-void
.end method

.method final k()V
    .locals 2

    .prologue
    .line 72
    const-string v0, "destroy"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/c;->a:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 73
    return-void
.end method
