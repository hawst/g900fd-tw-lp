.class public Lcom/dropbox/android/activity/ReferralActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/dropbox/android/service/E;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/ListView;

.field private h:Lcom/dropbox/android/widget/bv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->b:Z

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/ReferralActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 87
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 88
    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->f:Landroid/widget/LinearLayout;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 93
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    mul-int/lit8 v1, p1, 0x64

    rsub-int/lit8 v1, v1, 0x64

    int-to-float v1, v1

    mul-int/lit8 v2, p1, 0x64

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 94
    iget-boolean v1, p0, Lcom/dropbox/android/activity/ReferralActivity;->b:Z

    if-eqz v1, :cond_1

    .line 95
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 99
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 100
    new-instance v1, Lcom/dropbox/android/activity/ew;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/ew;-><init>(Lcom/dropbox/android/activity/ReferralActivity;I)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 127
    iget-object v1, p0, Lcom/dropbox/android/activity/ReferralActivity;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 97
    :cond_1
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/ReferralActivity;)Lcom/dropbox/android/widget/bv;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    return-object v0
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 132
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bv;->b()I

    move-result v2

    .line 133
    const-wide v0, 0xbb800000L

    const-wide/32 v3, 0x1f400000

    int-to-long v5, v2

    mul-long/2addr v3, v5

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 135
    const-wide/32 v3, 0x3e800000

    cmp-long v3, v0, v3

    if-ltz v3, :cond_0

    .line 136
    const-wide v3, 0x3ff0624dd2f1a9fcL    # 1.024

    long-to-double v0, v0

    mul-double/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    .line 138
    :cond_0
    if-lez v2, :cond_1

    .line 139
    const v3, 0x7f0d02ae

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v0, v1, v7}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/dropbox/android/activity/ReferralActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/dropbox/android/activity/ReferralActivity;->e:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    :cond_1
    if-nez v2, :cond_2

    .line 145
    invoke-direct {p0, v8}, Lcom/dropbox/android/activity/ReferralActivity;->a(I)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_2
    invoke-direct {p0, v7}, Lcom/dropbox/android/activity/ReferralActivity;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 180
    const v0, 0x7f07016a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/bv;->a(Ljava/util/List;)V

    .line 182
    iput-object p2, p0, Lcom/dropbox/android/activity/ReferralActivity;->a:Ljava/util/List;

    .line 184
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bH()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v3, "count"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v0, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 185
    const v0, 0x7f07016f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 186
    const v0, 0x7f07016b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 187
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->d()V

    .line 188
    iput-boolean v1, p0, Lcom/dropbox/android/activity/ReferralActivity;->b:Z

    .line 189
    return-void

    :cond_0
    move v0, v2

    .line 185
    goto :goto_0

    :cond_1
    move v2, v1

    .line 186
    goto :goto_1
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/dropbox/android/activity/ReferralActivity;->e()V

    .line 157
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->invalidateOptionsMenu()V

    .line 158
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 199
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->finish()V

    .line 245
    :goto_0
    return-void

    .line 204
    :cond_0
    const v0, 0x7f03008c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->setContentView(I)V

    .line 205
    const v0, 0x7f0d02ac

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->setTitle(I)V

    .line 207
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 209
    const v0, 0x7f07016e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->e:Landroid/widget/TextView;

    .line 210
    const v0, 0x7f07016d

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->f:Landroid/widget/LinearLayout;

    .line 213
    if-eqz p1, :cond_1

    .line 214
    const-string v0, "checkboxes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    .line 219
    :goto_1
    const v0, 0x7f07016c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ReferralActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->g:Landroid/widget/ListView;

    .line 220
    new-instance v0, Lcom/dropbox/android/widget/bv;

    iget-object v2, p0, Lcom/dropbox/android/activity/ReferralActivity;->g:Landroid/widget/ListView;

    invoke-direct {v0, p0, v1, v2}, Lcom/dropbox/android/widget/bv;-><init>(Lcom/dropbox/android/activity/ReferralActivity;Ljava/util/ArrayList;Landroid/widget/ListView;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/activity/ex;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ex;-><init>(Lcom/dropbox/android/activity/ReferralActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/activity/ey;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ey;-><init>(Lcom/dropbox/android/activity/ReferralActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 242
    invoke-direct {p0}, Lcom/dropbox/android/activity/ReferralActivity;->e()V

    .line 244
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    goto :goto_0

    .line 216
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 307
    if-nez p1, :cond_0

    .line 308
    invoke-static {p0}, Lcom/dropbox/android/util/Activities;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    .line 310
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 168
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    .line 169
    new-instance v2, Lcom/dropbox/android/service/F;

    new-instance v3, Lcom/dropbox/android/util/bu;

    const/4 v4, 0x5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v3, v4, v5, v1, v0}, Lcom/dropbox/android/util/bu;-><init>(ILjava/util/Locale;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/n/K;)V

    invoke-direct {v2, p0, v3}, Lcom/dropbox/android/service/F;-><init>(Landroid/content/Context;Lcom/dropbox/android/service/D;)V

    return-object v2
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 255
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bv;->b()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 256
    :goto_0
    const v3, 0x7f0d02b3

    invoke-interface {p1, v2, v2, v2, v3}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    const v3, 0x7f0201dd

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/actionbarsherlock/view/MenuItem;->setVisible(Z)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 260
    return v1

    :cond_0
    move v0, v2

    .line 255
    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/ReferralActivity;->a(Landroid/support/v4/content/p;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bv;->clear()V

    .line 176
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 266
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 301
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 268
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bv;->b()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v5

    .line 269
    goto :goto_0

    .line 271
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 274
    new-instance v7, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bv;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move v1, v2

    move v3, v2

    move v4, v2

    .line 275
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 276
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 278
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 279
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    const/4 v0, 0x5

    if-ge v1, v0, :cond_2

    .line 281
    add-int/lit8 v4, v4, 0x1

    .line 275
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 283
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 288
    :cond_3
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bG()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_recommended_selected"

    int-to-long v7, v4

    invoke-virtual {v0, v1, v7, v8}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_others_selected"

    int-to-long v3, v3

    invoke-virtual {v0, v1, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_recommended"

    const-wide/16 v3, 0x5

    invoke-virtual {v0, v1, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 295
    new-instance v0, Ldbxyzptlk/db231222/g/ah;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ReferralActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/g/ah;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    .line 296
    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/g/ah;->a(I)V

    .line 297
    new-array v1, v5, [Ljava/util/List;

    aput-object v6, v1, v2

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/ah;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v5

    .line 299
    goto/16 :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bv;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 250
    const-string v0, "checkboxes"

    iget-object v1, p0, Lcom/dropbox/android/activity/ReferralActivity;->h:Lcom/dropbox/android/widget/bv;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/bv;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 251
    return-void
.end method
