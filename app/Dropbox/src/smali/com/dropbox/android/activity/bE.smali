.class final Lcom/dropbox/android/activity/bE;
.super Ldbxyzptlk/db231222/g/i;
.source "panda.py"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/dropbox/android/util/DropboxPath;

.field private final d:Ldbxyzptlk/db231222/z/M;

.field private final e:Ldbxyzptlk/db231222/k/b;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/ae;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/io/File;


# direct methods
.method constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 329
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/db231222/g/i;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 324
    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->f:Ljava/util/List;

    .line 326
    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->g:Ljava/io/File;

    .line 330
    const-class v0, Lcom/dropbox/android/activity/bG;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 331
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->d:Ldbxyzptlk/db231222/z/M;

    .line 332
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/h;->c()Ldbxyzptlk/db231222/k/b;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->e:Ldbxyzptlk/db231222/k/b;

    .line 333
    invoke-virtual {p3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 334
    iput-object p4, p0, Lcom/dropbox/android/activity/bE;->b:Ljava/lang/String;

    .line 335
    return-void
.end method

.method private static a(Ldbxyzptlk/db231222/k/b;Ljava/io/File;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 372
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 375
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/k/b;->a(Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 377
    if-eqz v0, :cond_0

    .line 378
    :try_start_1
    invoke-static {p1, v0}, Ldbxyzptlk/db231222/X/b;->a(Ljava/io/File;Ljava/io/File;)V

    .line 379
    invoke-virtual {v0}, Ljava/io/File;->setReadOnly()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 386
    :goto_0
    return-object v0

    .line 382
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 383
    :goto_1
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    .line 384
    invoke-static {v2}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    :cond_0
    move-object v0, v1

    .line 386
    goto :goto_0

    .line 382
    :catch_1
    move-exception v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_1
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;
    .locals 4

    .prologue
    .line 340
    const-string v0, "direct"

    .line 342
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/activity/bE;->d:Ldbxyzptlk/db231222/z/M;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/dropbox/android/util/DropboxPath;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/bE;->c:Lcom/dropbox/android/util/DropboxPath;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/bE;->b:Ljava/lang/String;

    const-string v3, "direct"

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->f:Ljava/util/List;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/g/i;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    .line 343
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 344
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    .line 345
    instance-of v2, v1, Ldbxyzptlk/db231222/w/d;

    if-eqz v2, :cond_1

    .line 346
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    .line 350
    :cond_0
    :goto_1
    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/taskqueue/w;

    .line 351
    const/4 v0, 0x0

    goto :goto_0

    .line 347
    :cond_1
    instance-of v1, v1, Ldbxyzptlk/db231222/w/i;

    if-eqz v1, :cond_0

    .line 348
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    goto :goto_1
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 314
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bE;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/dropbox/android/activity/bE;->e:Ldbxyzptlk/db231222/k/b;

    invoke-static {v0, p1}, Lcom/dropbox/android/activity/bE;->a(Ldbxyzptlk/db231222/k/b;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/bE;->g:Ljava/io/File;

    .line 361
    return-void
.end method

.method protected final a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 392
    instance-of v0, p3, Lcom/dropbox/android/activity/bG;

    if-eqz v0, :cond_1

    move-object v0, p3

    .line 393
    check-cast v0, Lcom/dropbox/android/activity/bG;

    .line 395
    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->g:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    iget-object p1, p0, Lcom/dropbox/android/activity/bE;->g:Ljava/io/File;

    .line 400
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->f:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/z/ae;

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/dropbox/android/activity/bG;->a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;Ldbxyzptlk/db231222/z/ae;)V

    .line 402
    :cond_1
    return-void
.end method
