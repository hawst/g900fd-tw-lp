.class final Lcom/dropbox/android/activity/dg;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Z

.field private final u:Lcom/dropbox/android/albums/PhotosModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Z)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 194
    iput-boolean p3, p0, Lcom/dropbox/android/activity/dg;->l:Z

    .line 195
    iput-object p2, p0, Lcom/dropbox/android/activity/dg;->u:Lcom/dropbox/android/albums/PhotosModel;

    .line 196
    return-void
.end method


# virtual methods
.method protected final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dg;->y()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final y()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 200
    const/4 v0, 0x0

    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 204
    new-instance v2, Lcom/dropbox/android/provider/W;

    iget-object v3, p0, Lcom/dropbox/android/activity/dg;->u:Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/PhotosModel;->c()Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "_album"

    invoke-direct {v2, v3, v4}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 205
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dg;->k()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0d0298

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/provider/Y;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    new-instance v3, Lcom/dropbox/android/provider/W;

    iget-object v4, p0, Lcom/dropbox/android/activity/dg;->u:Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v4}, Lcom/dropbox/android/albums/PhotosModel;->e()Landroid/database/Cursor;

    move-result-object v4

    const-string v5, "_album"

    invoke-direct {v3, v4, v5}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 212
    iget-boolean v4, p0, Lcom/dropbox/android/activity/dg;->l:Z

    if-eqz v4, :cond_2

    .line 213
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dg;->k()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f0d0299

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/provider/Y;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 230
    const-string v3, "EXTRA_SILENCE_LIGHTWEIGHT_ALBUMS"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 231
    new-instance v3, Lcom/dropbox/android/activity/dh;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/database/Cursor;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/Cursor;

    invoke-direct {v3, v0, v2}, Lcom/dropbox/android/activity/dh;-><init>([Landroid/database/Cursor;Landroid/os/Bundle;)V

    .line 232
    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/dg;->a(Landroid/database/Cursor;)V

    .line 233
    return-object v3

    .line 218
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-eqz v4, :cond_4

    .line 219
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 222
    const-string v0, "_expand_lightweight_shared"

    invoke-static {v0}, Lcom/dropbox/android/provider/Y;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_3
    const/4 v0, 0x1

    .line 226
    :cond_4
    new-instance v2, Lcom/dropbox/android/util/z;

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/z;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
