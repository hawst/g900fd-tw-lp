.class final Lcom/dropbox/android/activity/fb;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/TextEditActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/TextEditActivity;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/dropbox/android/activity/fb;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/dropbox/android/activity/fb;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->f(Lcom/dropbox/android/activity/TextEditActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/fb;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->g(Lcom/dropbox/android/activity/TextEditActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/dropbox/android/activity/fb;->a:Lcom/dropbox/android/activity/TextEditActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->b(Lcom/dropbox/android/activity/TextEditActivity;Z)Z

    .line 414
    :cond_0
    return-void
.end method
