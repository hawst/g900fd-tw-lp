.class public Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;
.super Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    return-void
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;-><init>()V

    .line 33
    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->b(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 34
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method private b(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 39
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    if-eqz p1, :cond_0

    .line 45
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->b(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 47
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const v5, 0x7f0d00fb

    .line 51
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 52
    const v2, 0x7f030035

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 55
    const v0, 0x7f0700ad

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/CustomTwoLineView;

    .line 56
    new-instance v3, Lcom/dropbox/android/activity/dialog/D;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/dialog/D;-><init>(Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;)V

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v0, 0x7f0700ae

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/CustomTwoLineView;

    .line 68
    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 71
    const v1, 0x7f0d00fe

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setTitle(Ljava/lang/String;)V

    .line 72
    const v1, 0x7f0d00ff

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f02022c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 74
    const-class v1, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    .line 98
    :goto_0
    new-instance v3, Lcom/dropbox/android/activity/dialog/E;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/dialog/E;-><init>(Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 110
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 75
    :cond_0
    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-eqz v3, :cond_1

    .line 77
    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setTitle(Ljava/lang/String;)V

    .line 78
    const v1, 0x7f0d00fd

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f02016b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 80
    const-class v1, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02016d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 83
    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setTitle(Ljava/lang/String;)V

    .line 84
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setAppearanceDisabled(Z)V

    .line 85
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setEnabled(Z)V

    .line 87
    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->h:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 89
    const v3, 0x7f0d0103

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_2
    const v3, 0x7f0d0102

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 116
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 117
    return-void
.end method
