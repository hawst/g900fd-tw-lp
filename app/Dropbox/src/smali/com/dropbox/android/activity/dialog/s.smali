.class Lcom/dropbox/android/activity/dialog/s;
.super Ldbxyzptlk/db231222/g/aa;
.source "panda.py"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Lcom/dropbox/android/service/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 292
    const-class v0, Lcom/dropbox/android/activity/dialog/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/s;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Lcom/dropbox/android/service/a;)V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/g/aa;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V

    .line 298
    iput-object p5, p0, Lcom/dropbox/android/activity/dialog/s;->d:Lcom/dropbox/android/service/a;

    .line 299
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/dropbox/android/activity/dialog/s;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/dialog/s;->b(Landroid/content/Context;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;Landroid/content/Context;)V

    .line 312
    return-void
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/K;)V
    .locals 5

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/dialog/s;->b(Landroid/content/Context;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    move-result-object v1

    .line 321
    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    .line 326
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->p:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v2, :cond_0

    .line 327
    const v0, 0x7f0d00d1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/s;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 328
    invoke-virtual {v1, p1, v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 384
    :goto_0
    return-void

    .line 330
    :cond_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v2, :cond_2

    .line 332
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/s;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_1

    .line 333
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->e:Lcom/dropbox/android/activity/dialog/o;

    .line 337
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/s;->d:Lcom/dropbox/android/service/a;

    invoke-virtual {v2}, Lcom/dropbox/android/service/a;->e()Z

    move-result v2

    .line 339
    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Lcom/dropbox/android/activity/dialog/o;Lcom/dropbox/android/activity/dialog/p;Z)Lcom/dropbox/android/activity/dialog/OverQuotaDialog;

    move-result-object v0

    .line 340
    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 335
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->d:Lcom/dropbox/android/activity/dialog/o;

    goto :goto_1

    .line 342
    :cond_2
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    if-eq v0, v2, :cond_3

    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->c:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v2, :cond_7

    .line 343
    :cond_3
    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_4

    instance-of v2, v0, Lcom/dropbox/android/widget/quickactions/e;

    if-eqz v2, :cond_4

    .line 347
    check-cast v0, Lcom/dropbox/android/widget/quickactions/e;

    .line 354
    :goto_2
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    if-ne v2, v3, :cond_6

    .line 355
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->b()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    .line 358
    new-instance v3, Lcom/dropbox/android/activity/dialog/t;

    invoke-direct {v3, p0, v0, v2}, Lcom/dropbox/android/activity/dialog/t;-><init>(Lcom/dropbox/android/activity/dialog/s;Lcom/dropbox/android/widget/quickactions/e;Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v3}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Ljava/lang/Runnable;)Z

    .line 381
    :goto_3
    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 348
    :cond_4
    instance-of v0, p1, Lcom/dropbox/android/widget/quickactions/e;

    if-eqz v0, :cond_5

    .line 349
    check-cast p1, Lcom/dropbox/android/widget/quickactions/e;

    move-object v0, p1

    goto :goto_2

    .line 351
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Target fragment or activity must be of type RenameCallback"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_6
    new-instance v2, Lcom/dropbox/android/activity/dialog/u;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/dialog/u;-><init>(Lcom/dropbox/android/activity/dialog/s;Lcom/dropbox/android/widget/quickactions/e;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 374
    :cond_7
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v2, :cond_8

    .line 375
    const v0, 0x7f0d005b

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    goto :goto_3

    .line 378
    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/s;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_9

    const v0, 0x7f0d00bc

    .line 379
    :goto_4
    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    goto :goto_3

    .line 378
    :cond_9
    const v0, 0x7f0d00b9

    goto :goto_4
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/dialog/s;->b(Landroid/content/Context;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    .line 389
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 290
    check-cast p2, Ldbxyzptlk/db231222/g/K;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/dialog/s;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/K;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;
    .locals 2

    .prologue
    .line 302
    const-class v0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 303
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 304
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    .line 306
    return-object v0
.end method
