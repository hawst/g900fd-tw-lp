.class public Lcom/dropbox/android/activity/SsoLoginFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/cy;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/SsoLoginFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/SsoLoginFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 23
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SsoLoginFragment;->setArguments(Landroid/os/Bundle;)V

    .line 24
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/SsoLoginFragment;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lcom/dropbox/android/activity/SsoLoginFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/SsoLoginFragment;-><init>()V

    .line 28
    invoke-virtual {v0}, Lcom/dropbox/android/activity/SsoLoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    return-object v0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 4

    .prologue
    .line 80
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/SsoLoginFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/cy;

    .line 82
    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 87
    const-string v2, ""

    .line 88
    const-string v2, ""

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/dropbox/android/activity/cy;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/SsoLoginFragment;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/SsoLoginFragment;->a(Landroid/widget/TextView;)V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/cy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    const-class v0, Lcom/dropbox/android/activity/cy;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoLoginFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 35
    const v0, 0x7f0300b0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 37
    const v0, 0x7f0701b2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/EmailTextView;

    .line 38
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoLoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "ARG_EMAIL"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/EmailTextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    const v1, 0x7f0700f6

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 42
    const-string v3, "login"

    invoke-virtual {v0, v1, v3}, Lcom/dropbox/android/widget/EmailTextView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 44
    new-instance v1, Lcom/dropbox/android/activity/eX;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/eX;-><init>(Lcom/dropbox/android/activity/SsoLoginFragment;Lcom/dropbox/android/widget/EmailTextView;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/EmailTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 56
    const v1, 0x7f0701b3

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 57
    new-instance v3, Lcom/dropbox/android/activity/eY;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/eY;-><init>(Lcom/dropbox/android/activity/SsoLoginFragment;Lcom/dropbox/android/widget/EmailTextView;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    if-nez p3, :cond_0

    .line 69
    invoke-virtual {v0}, Lcom/dropbox/android/widget/EmailTextView;->requestFocus()Z

    .line 72
    :cond_0
    return-object v2
.end method
