.class public Lcom/dropbox/android/activity/UserSweetListFragment;
.super Lcom/dropbox/android/activity/SweetListFragment;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AdapterType:",
        "Lcom/dropbox/android/widget/bC;",
        ">",
        "Lcom/dropbox/android/activity/SweetListFragment",
        "<TAdapterType;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/activity/SweetListFragment;-><init>()V

    .line 18
    new-instance v0, Lcom/dropbox/android/activity/base/k;

    invoke-direct {v0}, Lcom/dropbox/android/activity/base/k;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/UserSweetListFragment;->a:Lcom/dropbox/android/activity/base/k;

    return-void
.end method


# virtual methods
.method protected final h()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dropbox/android/activity/UserSweetListFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/k;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->onAttach(Landroid/app/Activity;)V

    .line 23
    iget-object v0, p0, Lcom/dropbox/android/activity/UserSweetListFragment;->a:Lcom/dropbox/android/activity/base/k;

    check-cast p1, Lcom/dropbox/android/activity/base/BaseUserActivity;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;)V

    .line 24
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseUserActivity;

    .line 30
    iget-object v1, p0, Lcom/dropbox/android/activity/UserSweetListFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v1, v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lcom/dropbox/android/activity/UserSweetListFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method
