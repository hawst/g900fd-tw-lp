.class final Lcom/dropbox/android/activity/bp;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/activity/GalleryActivity;

.field private final u:Lcom/dropbox/android/provider/MetadataManager;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/provider/MetadataManager;)V
    .locals 0

    .prologue
    .line 1345
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 1346
    iput-object p1, p0, Lcom/dropbox/android/activity/bp;->l:Lcom/dropbox/android/activity/GalleryActivity;

    .line 1347
    iput-object p2, p0, Lcom/dropbox/android/activity/bp;->u:Lcom/dropbox/android/provider/MetadataManager;

    .line 1348
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1339
    invoke-virtual {p0}, Lcom/dropbox/android/activity/bp;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/dropbox/android/activity/bp;->u:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/MetadataManager;->a()Lcom/dropbox/android/provider/H;

    move-result-object v1

    .line 1355
    iget-object v0, p0, Lcom/dropbox/android/activity/bp;->l:Lcom/dropbox/android/activity/GalleryActivity;

    new-instance v2, Lcom/dropbox/android/activity/bu;

    sget-object v3, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    iget-object v4, p0, Lcom/dropbox/android/activity/bp;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/GalleryActivity;->m(Lcom/dropbox/android/activity/GalleryActivity;)I

    move-result v4

    iget-boolean v5, v1, Lcom/dropbox/android/provider/H;->b:Z

    invoke-direct {v2, v3, v4, v5}, Lcom/dropbox/android/activity/bu;-><init>(Ldbxyzptlk/db231222/n/r;IZ)V

    invoke-static {v0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/activity/bu;)Lcom/dropbox/android/activity/bu;

    .line 1356
    iget-object v0, p0, Lcom/dropbox/android/activity/bp;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->n(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1357
    iget-object v0, p0, Lcom/dropbox/android/activity/bp;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_LOCAL_ENTRY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 1358
    iget-object v2, p0, Lcom/dropbox/android/activity/bp;->l:Lcom/dropbox/android/activity/GalleryActivity;

    iget-boolean v3, v1, Lcom/dropbox/android/provider/H;->b:Z

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    :goto_0
    invoke-static {v2, v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1362
    :cond_0
    new-instance v0, Lcom/dropbox/android/provider/r;

    iget-object v1, v1, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-static {}, Lcom/dropbox/android/util/ab;->b()Lcom/dropbox/android/util/aY;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/provider/r;-><init>(Landroid/database/Cursor;Lcom/dropbox/android/util/aY;)V

    .line 1363
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/bp;->a(Landroid/database/Cursor;)V

    .line 1364
    return-object v0

    .line 1358
    :cond_1
    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    goto :goto_0
.end method
