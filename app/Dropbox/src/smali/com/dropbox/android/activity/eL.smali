.class final Lcom/dropbox/android/activity/eL;
.super Landroid/widget/BaseAdapter;
.source "panda.py"


# instance fields
.field private a:Landroid/database/Cursor;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/dropbox/android/widget/bY;

.field private final d:Lcom/dropbox/android/filemanager/v;

.field private final e:Lcom/dropbox/android/widget/aG;

.field private f:Lcom/dropbox/android/filemanager/n;

.field private final g:Lcom/dropbox/android/taskqueue/D;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/taskqueue/D;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    new-instance v0, Lcom/dropbox/android/activity/eM;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eM;-><init>(Lcom/dropbox/android/activity/eL;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/eL;->c:Lcom/dropbox/android/widget/bY;

    .line 47
    new-instance v0, Lcom/dropbox/android/activity/eN;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eN;-><init>(Lcom/dropbox/android/activity/eL;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/eL;->d:Lcom/dropbox/android/filemanager/v;

    .line 65
    new-instance v0, Lcom/dropbox/android/widget/aG;

    invoke-direct {v0}, Lcom/dropbox/android/widget/aG;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/eL;->e:Lcom/dropbox/android/widget/aG;

    .line 70
    iput-object p1, p0, Lcom/dropbox/android/activity/eL;->b:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Lcom/dropbox/android/activity/eL;->g:Lcom/dropbox/android/taskqueue/D;

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/eL;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/n;->a(Z)V

    .line 151
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    sub-int v1, p2, p1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/android/filemanager/n;->a(II)V

    .line 145
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 120
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/n;->a()V

    .line 125
    :cond_0
    if-eqz p1, :cond_1

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    if-nez v0, :cond_2

    .line 127
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/activity/eL;->d:Lcom/dropbox/android/filemanager/v;

    invoke-static {}, Lcom/dropbox/android/util/bn;->f()Ldbxyzptlk/db231222/v/n;

    move-result-object v3

    invoke-static {}, Lcom/dropbox/android/util/bn;->i()I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/activity/eL;->g:Lcom/dropbox/android/taskqueue/D;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Ldbxyzptlk/db231222/v/n;ILcom/dropbox/android/taskqueue/D;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    .line 138
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    .line 139
    return-void

    .line 134
    :cond_2
    new-instance v0, Lcom/dropbox/android/filemanager/n;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/activity/eL;->d:Lcom/dropbox/android/filemanager/v;

    iget-object v3, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/n;-><init>(ILcom/dropbox/android/filemanager/v;Lcom/dropbox/android/filemanager/n;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/n;->a(Z)V

    .line 157
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This method should not be called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemId(I)J
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 93
    if-nez p2, :cond_0

    .line 94
    new-instance p2, Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 96
    new-instance v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v1, p0, Lcom/dropbox/android/activity/eL;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/activity/eL;->c:Lcom/dropbox/android/widget/bY;

    iget-object v3, p0, Lcom/dropbox/android/activity/eL;->e:Lcom/dropbox/android/widget/aG;

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/dropbox/android/widget/ThumbGridItemView;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/widget/aG;)V

    .line 97
    iget-object v1, p0, Lcom/dropbox/android/activity/eL;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 99
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/ThumbGridItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v1, p0, Lcom/dropbox/android/activity/eL;->a:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/activity/eL;->f:Lcom/dropbox/android/filemanager/n;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ThumbGridItemView;->b(Landroid/database/Cursor;Lcom/dropbox/android/filemanager/n;)V

    .line 105
    return-object p2

    .line 101
    :cond_0
    check-cast p2, Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method
