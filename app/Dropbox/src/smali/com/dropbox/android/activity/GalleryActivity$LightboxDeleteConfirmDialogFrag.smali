.class public Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;
.super Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities",
        "<",
        "Lcom/dropbox/android/activity/GalleryActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;Lcom/dropbox/android/activity/bs;Z)Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;
    .locals 3

    .prologue
    .line 698
    sget-object v0, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    if-ne p1, v0, :cond_1

    const v0, 0x7f0d00aa

    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 702
    if-eqz p2, :cond_0

    .line 703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 704
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    if-ne p1, v0, :cond_2

    const v0, 0x7f0d00ac

    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 709
    :cond_0
    new-instance v1, Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;

    invoke-direct {v1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;-><init>()V

    .line 710
    const v2, 0x7f0d00a6

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;->a(Ljava/lang/String;I)V

    .line 711
    return-object v1

    .line 698
    :cond_1
    const v0, 0x7f0d00ab

    goto :goto_0

    .line 704
    :cond_2
    const v0, 0x7f0d00ad

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 695
    check-cast p1, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;->a(Lcom/dropbox/android/activity/GalleryActivity;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 4

    .prologue
    .line 716
    new-instance v0, Lcom/dropbox/android/activity/bo;

    invoke-static {p1}, Lcom/dropbox/android/activity/GalleryActivity;->i(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/activity/bq;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    invoke-static {p1}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/dropbox/android/activity/bo;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/activity/bq;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/DropboxPath;)V

    .line 721
    invoke-virtual {v0}, Lcom/dropbox/android/activity/bo;->f()V

    .line 722
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/dropbox/android/filemanager/LocalEntry;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/bo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 723
    return-void
.end method
