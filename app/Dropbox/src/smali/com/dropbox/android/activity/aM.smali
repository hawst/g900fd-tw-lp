.class final Lcom/dropbox/android/activity/aM;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/am;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/FavoritesFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/FavoritesFragment;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/dropbox/android/activity/aM;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/Z;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 309
    iget-object v0, p0, Lcom/dropbox/android/activity/aM;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 310
    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p2, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    .line 311
    sget-object v2, Lcom/dropbox/android/activity/aN;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 328
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :pswitch_0
    invoke-static {p2}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    .line 314
    iget-boolean v2, v1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v2, :cond_0

    .line 315
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not expecting directory in favorites view"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_0
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/ab;->a(Lcom/dropbox/android/filemanager/LocalEntry;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 318
    new-instance v2, Lcom/dropbox/android/util/HistoryEntry$DropboxFavoritesEntry;

    invoke-direct {v2}, Lcom/dropbox/android/util/HistoryEntry$DropboxFavoritesEntry;-><init>()V

    sget-object v3, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-static {v0, v2, v3, v1, v4}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/app/Activity;Lcom/dropbox/android/util/HistoryEntry;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/filemanager/LocalEntry;I)Landroid/content/Intent;

    move-result-object v0

    .line 321
    iget-object v1, p0, Lcom/dropbox/android/activity/aM;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/FavoritesFragment;->startActivity(Landroid/content/Intent;)V

    .line 330
    :goto_0
    return-void

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/aM;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/activity/aM;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/FavoritesFragment;->e(Lcom/dropbox/android/activity/FavoritesFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/util/bs;->a:Lcom/dropbox/android/util/bs;

    invoke-static {v0, v2, v1, v3}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;)V

    goto :goto_0

    .line 311
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    .line 304
    const/4 v0, 0x1

    return v0
.end method
