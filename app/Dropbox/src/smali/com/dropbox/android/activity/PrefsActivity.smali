.class public Lcom/dropbox/android/activity/PrefsActivity;
.super Lcom/dropbox/android/activity/base/BasePreferenceActivity;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Ldbxyzptlk/db231222/r/d;

.field private d:Ldbxyzptlk/db231222/r/d;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/PreferenceCategory;

.field private g:Landroid/preference/Preference;

.field private h:Landroid/preference/ListPreference;

.field private i:Landroid/preference/ListPreference;

.field private j:Landroid/preference/ListPreference;

.field private k:Landroid/preference/CheckBoxPreference;

.field private l:Landroid/preference/PreferenceCategory;

.field private m:Landroid/preference/CheckBoxPreference;

.field private n:Landroid/preference/Preference;

.field private o:Landroid/preference/Preference;

.field private p:Landroid/preference/Preference;

.field private q:Landroid/preference/Preference;

.field private r:Landroid/preference/Preference;

.field private s:Landroid/preference/Preference;

.field private t:Ldbxyzptlk/db231222/r/o;

.field private final u:Ldbxyzptlk/db231222/n/z;

.field private final v:Ldbxyzptlk/db231222/g/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/dropbox/android/activity/PrefsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/PrefsActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->t:Ldbxyzptlk/db231222/r/o;

    .line 95
    new-instance v0, Lcom/dropbox/android/activity/dH;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dH;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->u:Ldbxyzptlk/db231222/n/z;

    .line 102
    new-instance v0, Lcom/dropbox/android/activity/dR;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dR;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->v:Ldbxyzptlk/db231222/g/m;

    .line 679
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 746
    const-string v0, ""

    .line 748
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 749
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 753
    :goto_0
    const v1, 0x7f0d01b9

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 755
    const v0, 0x7f0d01ba

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 756
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v3

    .line 759
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 760
    if-nez v0, :cond_0

    .line 761
    const-string v0, "0"

    .line 765
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n\n\n\nInfo: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v3, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v3, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v3, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 768
    const/16 v0, 0x3e8

    invoke-static {v0}, Ldbxyzptlk/db231222/i/a;->a(I)Ljava/util/List;

    move-result-object v0

    .line 769
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 770
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 771
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v5, 0xa

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 763
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 774
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 776
    const-string v2, "android-log@dropbox.com"

    invoke-static {p0, v2, v1, v0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    return-void

    .line 750
    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 459
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 460
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 461
    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 462
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 464
    if-eqz p1, :cond_0

    .line 465
    const-string v2, "android.intent.extra.EMAIL"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    :cond_0
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 468
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 469
    const-string v1, "message/rfc822"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 472
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    :goto_0
    return-void

    .line 473
    :catch_0
    move-exception v0

    .line 474
    sget-object v0, Lcom/dropbox/android/activity/PrefsActivity;->b:Ljava/lang/String;

    const-string v1, "Mail app not found."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/dropbox/android/activity/PrefsActivity;->e()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/db231222/n/P;)V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/n/P;)V
    .locals 2

    .prologue
    .line 407
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/Preference;

    const v1, 0x7f0d0162

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 410
    invoke-static {}, Lcom/dropbox/android/util/S;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 411
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "3g"

    .line 412
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 416
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 417
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "limit"

    .line 418
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 419
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 420
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 430
    :goto_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "photos_and_videos"

    .line 431
    :goto_3
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 432
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 440
    :goto_4
    return-void

    .line 411
    :cond_0
    const-string v0, "wifi"

    goto :goto_0

    .line 417
    :cond_1
    const-string v0, "nolimit"

    goto :goto_1

    .line 422
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 426
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 427
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 430
    :cond_4
    const-string v0, "photos_only"

    goto :goto_3

    .line 435
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/Preference;

    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 436
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 437
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 438
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_4
.end method

.method private static a(Ldbxyzptlk/db231222/r/d;Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 395
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->k()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    if-nez v0, :cond_1

    .line 396
    :cond_0
    const v0, 0x7f0d015a

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 398
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->d:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/PrefsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/PrefsActivity;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/PrefsActivity;)Landroid/preference/PreferenceCategory;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 443
    invoke-static {}, Ldbxyzptlk/db231222/g/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    const v1, 0x7f0d021b

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 445
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 456
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/K;->e()J

    move-result-wide v0

    .line 448
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x2

    const/4 v5, 0x3

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatSameDayTime(JJII)Ljava/lang/CharSequence;

    move-result-object v0

    .line 451
    const v1, 0x7f0d021a

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 453
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 454
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    invoke-virtual {v0, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 479
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->t()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 480
    new-instance v0, Ldbxyzptlk/db231222/n/N;

    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    new-instance v4, Lcom/dropbox/android/activity/dQ;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/dQ;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    move-object v1, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/n/N;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;ZLdbxyzptlk/db231222/n/O;Z)V

    .line 488
    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/n/N;->a(I)V

    .line 489
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/N;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 490
    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/dropbox/android/activity/PrefsActivity;->f()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 401
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, p2, v2}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    .line 402
    const v1, 0x7f0d0154

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 403
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->r:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 404
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 114
    const-wide/16 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->requestWindowFeature(J)V

    .line 116
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v1

    .line 119
    if-nez v1, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->finish()V

    .line 392
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->t:Ldbxyzptlk/db231222/r/o;

    .line 125
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    .line 126
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->d:Ldbxyzptlk/db231222/r/d;

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->d:Ldbxyzptlk/db231222/r/d;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 136
    invoke-static {}, Lcom/dropbox/android/util/aC;->a()Lcom/dropbox/android/util/aC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/aC;->b()V

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v2

    .line 141
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->addPreferencesFromResource(I)V

    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 146
    const-string v0, "account_category"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 147
    const-string v3, "personal_details"

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 148
    const-string v4, "business_details"

    invoke-virtual {p0, v4}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 151
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->c()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 153
    const-string v5, "settings_name"

    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 154
    const-string v5, "settings_space"

    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 155
    const-string v5, "payments_upgrade"

    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 158
    sget-object v5, Ldbxyzptlk/db231222/s/k;->b:Ldbxyzptlk/db231222/s/k;

    invoke-virtual {v1, v5}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/db231222/r/d;Landroid/preference/Preference;)V

    .line 159
    sget-object v5, Ldbxyzptlk/db231222/s/k;->c:Ldbxyzptlk/db231222/s/k;

    invoke-virtual {v1, v5}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/db231222/r/d;Landroid/preference/Preference;)V

    .line 162
    new-instance v1, Lcom/dropbox/android/activity/dS;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dS;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 175
    new-instance v1, Lcom/dropbox/android/activity/dT;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dT;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 195
    :goto_1
    const-string v1, "settings_referral"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/Preference;

    .line 196
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/Preference;

    new-instance v3, Lcom/dropbox/android/activity/dU;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/dU;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 204
    const-string v1, "configure_category"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    .line 205
    const-string v1, "about_category"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/PreferenceCategory;

    .line 207
    const-string v1, "camera_upload_on_off"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/Preference;

    .line 208
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/Preference;

    new-instance v3, Lcom/dropbox/android/activity/dV;

    invoke-direct {v3, p0, v2}, Lcom/dropbox/android/activity/dV;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 234
    const-string v1, "camera_upload_connection"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    .line 235
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    new-instance v3, Lcom/dropbox/android/activity/dW;

    invoke-direct {v3, p0, v2}, Lcom/dropbox/android/activity/dW;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 246
    const-string v1, "camera_upload_3g_limit"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    .line 247
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/ListPreference;

    new-instance v3, Lcom/dropbox/android/activity/dX;

    invoke-direct {v3, p0, v2}, Lcom/dropbox/android/activity/dX;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 258
    const-string v1, "camera_upload_media_type"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    .line 259
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/ListPreference;

    new-instance v2, Lcom/dropbox/android/activity/dY;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dY;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 271
    invoke-static {}, Lcom/dropbox/android/util/S;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 272
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 275
    :cond_1
    const-string v1, "settings_lock_code"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->k:Landroid/preference/CheckBoxPreference;

    .line 276
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->t:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v1}, Ldbxyzptlk/db231222/r/o;->a()Z

    move-result v1

    .line 277
    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->k:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 278
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->k:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/dropbox/android/activity/dI;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dI;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 287
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v2

    .line 289
    const-string v1, "settings_externally_updateable"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/CheckBoxPreference;

    .line 290
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/K;->d()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 291
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/CheckBoxPreference;

    new-instance v3, Lcom/dropbox/android/activity/dJ;

    invoke-direct {v3, p0, v2}, Lcom/dropbox/android/activity/dJ;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/K;)V

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 306
    const-string v1, "settings_check_for_update"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    .line 307
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    new-instance v3, Lcom/dropbox/android/activity/dK;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/dK;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 317
    invoke-direct {p0}, Lcom/dropbox/android/activity/PrefsActivity;->e()V

    .line 319
    invoke-static {p0}, Lcom/dropbox/android/filemanager/au;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 320
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 321
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 326
    :cond_2
    :goto_2
    const-string v1, "settings_unlink"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->o:Landroid/preference/Preference;

    .line 327
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->o:Landroid/preference/Preference;

    new-instance v2, Lcom/dropbox/android/activity/dL;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dL;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 335
    const-string v1, "settings_version"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->p:Landroid/preference/Preference;

    .line 336
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->p:Landroid/preference/Preference;

    invoke-static {p0}, Lcom/dropbox/android/util/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 339
    const-string v1, "settings_qr_auth"

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 340
    invoke-static {}, Lcom/dropbox/android/util/S;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/util/analytics/r;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 341
    new-instance v0, Lcom/dropbox/android/activity/dM;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dM;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 356
    :goto_3
    const-string v0, "settings_legal"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/activity/dN;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dN;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 364
    const-string v0, "settings_send_feedback"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->q:Landroid/preference/Preference;

    .line 365
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->q:Landroid/preference/Preference;

    new-instance v1, Lcom/dropbox/android/activity/dO;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dO;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 374
    const-string v0, "settings_cache"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->r:Landroid/preference/Preference;

    .line 375
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->a(J)V

    .line 377
    const-string v0, "settings_clear_cache"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->s:Landroid/preference/Preference;

    .line 378
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->s:Landroid/preference/Preference;

    new-instance v1, Lcom/dropbox/android/activity/dP;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dP;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 391
    const v0, 0x7f0d0144

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 188
    :cond_3
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 189
    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 192
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    iget-object v3, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-static {p0, v1, v3}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;)V

    goto/16 :goto_1

    .line 322
    :cond_4
    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/K;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 323
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    .line 352
    :cond_5
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 709
    packed-switch p1, :pswitch_data_0

    .line 713
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected dialog id in PrefsActivity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 711
    :pswitch_0
    invoke-static {p0}, Lcom/dropbox/android/util/Activities;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    .line 709
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->k:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->t:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v1}, Ldbxyzptlk/db231222/r/o;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 720
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 721
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/db231222/n/P;)V

    .line 722
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onResume()V

    .line 726
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->setSupportProgressBarIndeterminateVisibility(Z)V

    .line 727
    new-instance v0, Ldbxyzptlk/db231222/g/aj;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/g/aj;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    .line 728
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/aj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 729
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 733
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onStart()V

    .line 734
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->u:Ldbxyzptlk/db231222/n/z;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/K;->a(Ldbxyzptlk/db231222/n/z;)V

    .line 735
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->v:Ldbxyzptlk/db231222/g/m;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/k;->a(Ldbxyzptlk/db231222/g/m;)V

    .line 736
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 740
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->u:Ldbxyzptlk/db231222/n/z;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/K;->b(Ldbxyzptlk/db231222/n/z;)V

    .line 741
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->v:Ldbxyzptlk/db231222/g/m;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/k;->b(Ldbxyzptlk/db231222/g/m;)V

    .line 742
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onStop()V

    .line 743
    return-void
.end method
