.class public abstract enum Lcom/dropbox/android/activity/auth/a;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/auth/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/auth/a;

.field public static final enum b:Lcom/dropbox/android/activity/auth/a;

.field public static final enum c:Lcom/dropbox/android/activity/auth/a;

.field public static final enum d:Lcom/dropbox/android/activity/auth/a;

.field public static final enum e:Lcom/dropbox/android/activity/auth/a;

.field private static final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dropbox/android/activity/auth/a;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic h:[Lcom/dropbox/android/activity/auth/a;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/dropbox/android/activity/auth/b;

    const-string v1, "NEW_ACCT_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/auth/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->a:Lcom/dropbox/android/activity/auth/a;

    .line 28
    new-instance v0, Lcom/dropbox/android/activity/auth/c;

    const-string v1, "LOGIN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/auth/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->b:Lcom/dropbox/android/activity/auth/a;

    .line 38
    new-instance v0, Lcom/dropbox/android/activity/auth/d;

    const-string v1, "REQUEST_PASSWORD_RESET_PROGRESS"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/auth/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->c:Lcom/dropbox/android/activity/auth/a;

    .line 45
    new-instance v0, Lcom/dropbox/android/activity/auth/e;

    const-string v1, "REQUEST_RESEND_TWOFACTOR_CODE"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/auth/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->d:Lcom/dropbox/android/activity/auth/a;

    .line 55
    new-instance v0, Lcom/dropbox/android/activity/auth/f;

    const-string v1, "VERIFY_CODE_PROGRESS"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/activity/auth/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->e:Lcom/dropbox/android/activity/auth/a;

    .line 13
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/activity/auth/a;

    sget-object v1, Lcom/dropbox/android/activity/auth/a;->a:Lcom/dropbox/android/activity/auth/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/auth/a;->b:Lcom/dropbox/android/activity/auth/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/auth/a;->c:Lcom/dropbox/android/activity/auth/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/auth/a;->d:Lcom/dropbox/android/activity/auth/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/auth/a;->e:Lcom/dropbox/android/activity/auth/a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->h:[Lcom/dropbox/android/activity/auth/a;

    .line 63
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/auth/a;->f:Landroid/util/SparseArray;

    .line 70
    const-class v0, Lcom/dropbox/android/activity/auth/a;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/auth/a;

    .line 71
    sget-object v2, Lcom/dropbox/android/activity/auth/a;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/a;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/dropbox/android/activity/auth/a;->g:I

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/auth/b;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/auth/a;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/dropbox/android/activity/auth/a;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/dropbox/android/activity/auth/a;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/auth/a;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/auth/a;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/dropbox/android/activity/auth/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/auth/a;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/auth/a;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/dropbox/android/activity/auth/a;->h:[Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/auth/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/auth/a;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/dropbox/android/activity/auth/a;->g:I

    return v0
.end method

.method public final a(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 91
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 92
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 94
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/auth/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 96
    return-object v0
.end method

.method abstract a(Landroid/content/Context;)Ljava/lang/String;
.end method
