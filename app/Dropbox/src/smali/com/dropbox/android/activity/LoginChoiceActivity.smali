.class public Lcom/dropbox/android/activity/LoginChoiceActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/cn;


# static fields
.field private static final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/dropbox/android/activity/cB;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 109
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/dropbox/android/activity/auth/DropboxAuth;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/dropbox/android/activity/DropboxSendTo;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/dropbox/android/activity/DropboxGetFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/dropbox/android/activity/DropboxChooserActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/dropbox/android/activity/TourActivity;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/activity/LoginChoiceActivity;->b:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/high16 v4, 0x2000000

    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot redirect to this package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 70
    invoke-static {p1}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid redirect target: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 74
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/LoginChoiceActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    if-eqz p3, :cond_0

    .line 76
    invoke-virtual {v1, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    :cond_0
    if-eqz p2, :cond_1

    .line 79
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 86
    :cond_1
    const-string v2, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 87
    return-object v1
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 154
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const-string v2, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 158
    if-eqz v0, :cond_0

    .line 159
    const-string v2, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 162
    :cond_0
    const/high16 v0, 0x2000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 163
    return-object v1
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 96
    if-nez v1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    .line 99
    :cond_1
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 100
    sget-object v3, Lcom/dropbox/android/activity/LoginChoiceActivity;->b:[Ljava/lang/Class;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 101
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 102
    const/4 v0, 0x1

    goto :goto_0

    .line 100
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 178
    packed-switch p1, :pswitch_data_0

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 180
    :pswitch_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 181
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->d()V

    goto :goto_0

    .line 182
    :cond_1
    if-nez p2, :cond_2

    .line 183
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->e()V

    goto :goto_0

    .line 184
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->finish()V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x29
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 195
    const-string v0, "com.dropbox.intent.action.DROPBOX_LOGIN"

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 196
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->startActivity(Landroid/content/Intent;)V

    .line 197
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 202
    const-string v0, "com.dropbox.intent.action.DROPBOX_SIGNUP"

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 203
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->startActivity(Landroid/content/Intent;)V

    .line 204
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 126
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/cB;->a(Ljava/lang/String;Lcom/dropbox/android/activity/cB;)Lcom/dropbox/android/activity/cB;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    .line 127
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    .line 128
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aD()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "initial_type"

    iget-object v3, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/cB;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is_tablet"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is_oobe"

    iget-object v3, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/cB;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 130
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    sget-object v2, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    if-eq v1, v2, :cond_0

    .line 131
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aE()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is_oobe"

    iget-object v3, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/cB;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "new_account"

    iget-object v3, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/cB;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    sget-object v2, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_2

    .line 135
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 136
    const-class v1, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 137
    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->startActivity(Landroid/content/Intent;)V

    .line 139
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->finish()V

    .line 150
    :goto_0
    return-void

    .line 142
    :cond_2
    const v0, 0x7f030057

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->setContentView(I)V

    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 144
    invoke-static {}, Lcom/dropbox/android/activity/LoginBrandFragment;->b()Lcom/dropbox/android/activity/LoginBrandFragment;

    move-result-object v1

    .line 145
    sget-object v2, Lcom/dropbox/android/activity/LoginBrandFragment;->a:Ljava/lang/String;

    .line 146
    const v3, 0x7f0700ef

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 147
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginChoiceActivity;->a:Lcom/dropbox/android/activity/cB;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/dropbox/android/util/Activities;->a(Lcom/dropbox/android/activity/cB;Landroid/app/Activity;Lcom/dropbox/android/util/analytics/r;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->finish()V

    .line 173
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onStart()V

    .line 174
    return-void
.end method
