.class final Lcom/dropbox/android/activity/bK;
.super Lcom/dropbox/android/util/L;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/util/L",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-direct {p0, p2}, Lcom/dropbox/android/util/L;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p()V

    .line 572
    return-void
.end method

.method protected final a(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/16 v4, -0x3e7

    .line 576
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/widget/DropboxItemListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 580
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/database/Cursor;)V

    .line 582
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 584
    iget-object v1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v1, p1, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/database/Cursor;Lcom/dropbox/android/util/HistoryEntry;)V

    .line 586
    iget-object v1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    iget-object v2, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c(Ljava/lang/String;)V

    .line 595
    iget-object v1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596
    iput v4, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 599
    :cond_0
    iget v1, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    if-ltz v1, :cond_3

    .line 601
    iget-object v1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/widget/DropboxItemListView;

    move-result-object v1

    iget v2, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    iget v3, v0, Lcom/dropbox/android/util/HistoryEntry;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/widget/DropboxItemListView;->setListViewScrollState(II)V

    .line 607
    iput v4, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 615
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V

    .line 618
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 619
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    iget-object v1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/net/Uri;Landroid/database/Cursor;)Z

    .line 620
    iget-object v0, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/util/DropboxPath;

    .line 622
    :cond_2
    return-void

    .line 608
    :cond_3
    iget v1, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    if-eq v1, v4, :cond_1

    .line 611
    iget-object v1, p0, Lcom/dropbox/android/activity/bK;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/widget/DropboxItemListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DropboxItemListView;->setSelection(I)V

    .line 612
    iput v4, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    goto :goto_0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 567
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/bK;->a(Landroid/database/Cursor;)V

    return-void
.end method
