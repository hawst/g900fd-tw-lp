.class final Lcom/dropbox/android/activity/cK;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/U;


# instance fields
.field private final a:Lcom/dropbox/android/service/a;

.field private final b:Ldbxyzptlk/db231222/n/P;

.field private final c:Ldbxyzptlk/db231222/r/e;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/service/a;Ldbxyzptlk/db231222/r/e;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput-object p1, p0, Lcom/dropbox/android/activity/cK;->b:Ldbxyzptlk/db231222/n/P;

    .line 159
    iput-object p2, p0, Lcom/dropbox/android/activity/cK;->a:Lcom/dropbox/android/service/a;

    .line 160
    iput-object p3, p0, Lcom/dropbox/android/activity/cK;->c:Ldbxyzptlk/db231222/r/e;

    .line 161
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 184
    const-string v0, "EXTRA_SHOW_RI_ENTRY_POINT"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/cK;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    return-void
.end method

.method final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 168
    iget-object v1, p0, Lcom/dropbox/android/activity/cK;->b:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/activity/cK;->b:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/android/activity/cK;->a:Lcom/dropbox/android/service/a;

    sget-object v2, Lcom/dropbox/android/service/e;->b:Lcom/dropbox/android/service/e;

    iget-object v3, p0, Lcom/dropbox/android/activity/cK;->c:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 175
    :goto_0
    if-eqz v1, :cond_0

    .line 176
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->D()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->B()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 179
    :cond_0
    return v0

    .line 172
    :catch_0
    move-exception v1

    .line 173
    iget-object v1, p0, Lcom/dropbox/android/activity/cK;->a:Lcom/dropbox/android/service/a;

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    goto :goto_0
.end method
