.class final Lcom/dropbox/android/activity/eE;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/SendFeedbackFragment;

.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/SendFeedbackFragment;Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    .line 214
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 215
    iput-object p3, p0, Lcom/dropbox/android/activity/eE;->b:Ldbxyzptlk/db231222/z/M;

    .line 216
    iput-object p4, p0, Lcom/dropbox/android/activity/eE;->c:Ljava/lang/String;

    .line 217
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 208
    check-cast p2, [Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eE;->a(Landroid/content/Context;[Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs a(Landroid/content/Context;[Ljava/lang/String;)Ljava/lang/Void;
    .locals 9

    .prologue
    .line 221
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v4

    .line 222
    const-string v6, ""

    .line 223
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->c(Lcom/dropbox/android/activity/SendFeedbackFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 225
    iget-object v1, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/SendFeedbackFragment;->d(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 226
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    .line 234
    :cond_0
    const-string v7, "market"

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->b:Ldbxyzptlk/db231222/z/M;

    const-string v1, "android"

    iget-object v2, v4, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    iget-object v3, v4, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    iget-object v4, v4, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/dropbox/android/activity/eE;->c:Ljava/lang/String;

    iget-object v8, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v8}, Lcom/dropbox/android/activity/SendFeedbackFragment;->e(Lcom/dropbox/android/activity/SendFeedbackFragment;)Lcom/dropbox/android/activity/eD;

    move-result-object v8

    invoke-virtual {v8}, Lcom/dropbox/android/activity/eD;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 257
    instance-of v0, p2, Ldbxyzptlk/db231222/w/j;

    if-eqz v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 259
    :cond_0
    instance-of v0, p2, Ldbxyzptlk/db231222/w/d;

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->f(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0d01b8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 266
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->g(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->f(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    new-instance v1, Lcom/dropbox/android/activity/eG;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/eG;-><init>(Lcom/dropbox/android/activity/eE;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/SendFeedbackFragment;->a(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 263
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 264
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->f(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0d008b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 208
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eE;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/dropbox/android/activity/eE;->a:Lcom/dropbox/android/activity/SendFeedbackFragment;

    new-instance v1, Lcom/dropbox/android/activity/eF;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/eF;-><init>(Lcom/dropbox/android/activity/eE;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/SendFeedbackFragment;->a(Ljava/lang/Runnable;)Z

    .line 251
    const v0, 0x7f0d01b7

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    .line 253
    return-void
.end method
