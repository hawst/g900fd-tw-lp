.class public Lcom/dropbox/android/activity/GalleryActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dialog/e;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/provider/MetadataManager;

.field private e:Lcom/dropbox/android/widget/GalleryView;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Z

.field private i:Landroid/view/ViewGroup;

.field private j:Landroid/view/ViewGroup;

.field private k:Landroid/view/View;

.field private final l:Landroid/os/Handler;

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/view/animation/AlphaAnimation;

.field private o:Lcom/dropbox/android/activity/bq;

.field private p:Lcom/dropbox/android/activity/bu;

.field private q:I

.field private r:Lcom/dropbox/android/albums/Album;

.field private s:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Lcom/dropbox/android/albums/h;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private u:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Lcom/dropbox/android/albums/g;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcom/dropbox/android/util/as;

.field private w:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/GalleryActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 117
    iput-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    .line 118
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    .line 125
    iput-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    .line 149
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->q:I

    .line 152
    iput-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->r:Lcom/dropbox/android/albums/Album;

    .line 1370
    new-instance v0, Lcom/dropbox/android/activity/bc;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bc;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->w:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method public static a(Landroid/database/Cursor;ZILjava/lang/String;JZI)I
    .locals 8

    .prologue
    .line 1108
    .line 1109
    const/4 v4, -0x1

    .line 1111
    const/4 v3, 0x0

    .line 1112
    const/4 v1, 0x1

    .line 1115
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p7, v0, :cond_c

    .line 1116
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    move v5, v4

    .line 1119
    :goto_0
    if-ltz v2, :cond_8

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 1120
    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 1121
    if-nez v0, :cond_0

    .line 1122
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected failure in moving cursor"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1125
    :cond_0
    if-eqz p3, :cond_2

    .line 1126
    if-eqz p6, :cond_1

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 1133
    :goto_1
    if-eqz p1, :cond_b

    .line 1134
    neg-int v0, v0

    move v4, v0

    .line 1137
    :goto_2
    if-nez v4, :cond_3

    move v0, v2

    .line 1165
    :goto_3
    return v0

    .line 1126
    :cond_1
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1130
    :cond_2
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    goto :goto_1

    .line 1141
    :cond_3
    if-eqz v1, :cond_6

    .line 1142
    if-gez v4, :cond_5

    const/4 v0, 0x1

    :goto_4
    move v3, v0

    .line 1153
    :cond_4
    add-int/2addr v2, v3

    .line 1156
    if-eqz v1, :cond_a

    .line 1157
    const/4 v0, 0x0

    :goto_5
    move v1, v0

    move v5, v4

    .line 1159
    goto :goto_0

    .line 1142
    :cond_5
    const/4 v0, -0x1

    goto :goto_4

    .line 1143
    :cond_6
    mul-int v0, v4, v5

    if-gez v0, :cond_4

    .line 1146
    if-lez v3, :cond_7

    .line 1147
    neg-int v0, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 1149
    :cond_7
    add-int/lit8 v0, v2, 0x1

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 1162
    :cond_8
    if-gez v2, :cond_9

    .line 1163
    const/4 v0, -0x1

    goto :goto_3

    .line 1165
    :cond_9
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_5

    :cond_b
    move v4, v0

    goto :goto_2

    :cond_c
    move v2, p7

    move v5, v4

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;J)J
    .locals 0

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    return-wide p1
.end method

.method public static a(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;JLcom/dropbox/android/activity/bq;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 253
    sget-object v0, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-eq p5, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Use getLaunchIntentFromBrowser() for folders."

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 254
    if-nez p1, :cond_1

    .line 255
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Need a uri for opening gallery files."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 260
    if-eqz p2, :cond_2

    .line 261
    const-string v1, "EXTRA_SELECTED_SORT_KEY_STRING_VALUE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    :cond_2
    const-wide/16 v1, -0x1

    cmp-long v1, p3, v1

    if-eqz v1, :cond_3

    .line 264
    const-string v1, "EXTRA_SELECTED_SORT_KEY_LONG_VALUE"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 266
    :cond_3
    const-string v1, "TYPE"

    invoke-virtual {p5}, Lcom/dropbox/android/activity/bq;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    const-string v1, "SEARCH_BEGIN"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 270
    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/dropbox/android/util/HistoryEntry;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/filemanager/LocalEntry;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 276
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 277
    const-string v1, "TYPE"

    sget-object v2, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/bq;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v1, "EXTRA_HISTORY_ENTRY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 279
    const-string v1, "EXTRA_SORT_ORDER"

    invoke-virtual {p2}, Ldbxyzptlk/db231222/n/r;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const-string v1, "EXTRA_LOCAL_ENTRY"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 281
    const-string v1, "SEARCH_BEGIN"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 282
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/activity/bu;)Lcom/dropbox/android/activity/bu;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/albums/o;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->t:Lcom/dropbox/android/albums/o;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;
    .locals 7

    .prologue
    .line 163
    new-instance v0, Lcom/dropbox/android/activity/aR;

    const-string v2, "SIS_KEY_WaitingForAddToAlbum"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d027d

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/aR;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->s:Lcom/dropbox/android/albums/o;

    .line 189
    new-instance v0, Lcom/dropbox/android/activity/be;

    const-string v2, "SIS_KEY_WaitingForCreateLws"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d0279

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/be;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->t:Lcom/dropbox/android/albums/o;

    .line 221
    new-instance v0, Lcom/dropbox/android/activity/bf;

    const-string v2, "SIS_KEY_WaitingForRemove"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->d:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d027c

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/bf;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->u:Lcom/dropbox/android/albums/o;

    .line 238
    new-instance v0, Lcom/dropbox/android/util/as;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/dropbox/android/util/at;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->s:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->t:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->u:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/as;-><init>([Lcom/dropbox/android/util/at;)V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    .line 1052
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1053
    sget-object v0, Lcom/dropbox/android/activity/GalleryActivity;->a:Ljava/lang/String;

    const-string v1, "No images left in gallery, finishing."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->finish()V

    .line 1086
    :goto_0
    return-void

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->d()Landroid/database/Cursor;

    move-result-object v0

    .line 1059
    const/4 v3, 0x0

    .line 1060
    const-wide/16 v4, -0x1

    .line 1061
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget-boolean v1, v1, Lcom/dropbox/android/activity/bu;->b:Z

    if-eqz v1, :cond_2

    .line 1062
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/bu;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1067
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget-boolean v1, v0, Lcom/dropbox/android/activity/bu;->d:Z

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget v2, v0, Lcom/dropbox/android/activity/bu;->a:I

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget-boolean v6, v0, Lcom/dropbox/android/activity/bu;->c:Z

    iget v7, p0, Lcom/dropbox/android/activity/GalleryActivity;->q:I

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/database/Cursor;ZILjava/lang/String;JZI)I

    move-result v0

    .line 1071
    if-gez v0, :cond_1

    .line 1072
    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    .line 1073
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->c()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 1075
    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1082
    :goto_2
    sget-object v1, Lcom/dropbox/android/activity/GalleryActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Was looking for a search key, and failed to find it. Closest match was pos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    invoke-static {}, Lcom/dropbox/android/util/bn;->k()Ldbxyzptlk/db231222/v/n;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/dropbox/android/widget/GalleryView;->a(Landroid/database/Cursor;Lcom/dropbox/android/activity/bq;Ldbxyzptlk/db231222/v/n;I)V

    goto :goto_0

    .line 1064
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget-wide v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/bu;->a(Landroid/database/Cursor;Ljava/lang/Long;)J

    move-result-wide v4

    goto :goto_1

    .line 1080
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2
.end method

.method private a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 390
    new-instance v0, Lcom/dropbox/android/activity/bg;

    invoke-direct {v0, p0, p2}, Lcom/dropbox/android/activity/bg;-><init>(Lcom/dropbox/android/activity/GalleryActivity;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 398
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;Z)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 856
    iget-boolean v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-ne p1, v0, :cond_0

    .line 862
    :goto_0
    return-void

    .line 860
    :cond_0
    if-eqz p1, :cond_1

    const-wide/16 v0, 0x0

    .line 861
    :goto_1
    invoke-direct {p0, p1, v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(ZJ)V

    goto :goto_0

    .line 860
    :cond_1
    const-wide/16 v0, 0x12c

    goto :goto_1
.end method

.method private a(ZJ)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 865
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v4}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    if-nez v4, :cond_0

    .line 927
    :goto_0
    return-void

    .line 869
    :cond_0
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v4}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    .line 871
    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v5}, Landroid/view/animation/AlphaAnimation;->hasStarted()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v5}, Landroid/view/animation/AlphaAnimation;->hasEnded()Z

    move-result v5

    if-nez v5, :cond_1

    .line 872
    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v5}, Landroid/view/animation/AlphaAnimation;->cancel()V

    .line 875
    :cond_1
    const-wide/16 v5, 0x0

    cmp-long v5, p2, v5

    if-lez v5, :cond_7

    .line 877
    if-eqz p1, :cond_5

    move v3, v1

    .line 878
    :goto_1
    if-eqz p1, :cond_6

    .line 880
    :goto_2
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v3, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    .line 881
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 882
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 886
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Lcom/dropbox/android/activity/bb;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/bb;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 904
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 905
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 906
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 909
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 910
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 911
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 922
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v1, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v1, :cond_4

    .line 923
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 926
    :cond_4
    iput-boolean p1, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    goto :goto_0

    :cond_5
    move v3, v0

    .line 877
    goto :goto_1

    :cond_6
    move v0, v1

    .line 878
    goto :goto_2

    .line 915
    :cond_7
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    if-eqz p1, :cond_8

    move v0, v2

    :goto_4
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 917
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 918
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz p1, :cond_9

    :goto_5
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move v0, v3

    .line 915
    goto :goto_4

    :cond_9
    move v2, v3

    .line 918
    goto :goto_5
.end method

.method static synthetic b(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/albums/Album;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->r:Lcom/dropbox/android/albums/Album;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/dropbox/android/activity/GalleryActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryActivity;->n()V

    return-void
.end method

.method static synthetic h(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryActivity;->o()V

    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/activity/bq;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic k(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 355
    const v0, 0x7f0700c0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    .line 359
    sget-object v0, Lcom/dropbox/android/activity/bd;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 384
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :pswitch_0
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0300b7

    .line 363
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 364
    iput-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    .line 387
    :goto_1
    return-void

    .line 361
    :cond_0
    const v0, 0x7f030039

    goto :goto_0

    .line 368
    :pswitch_1
    const v0, 0x7f03003a

    .line 369
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 370
    iput-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    goto :goto_1

    .line 374
    :pswitch_2
    const v0, 0x7f03003b

    .line 375
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 376
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    const v1, 0x7f0700b9

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    goto :goto_1

    .line 380
    :pswitch_3
    iput-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    goto :goto_1

    .line 359
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic l(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/provider/MetadataManager;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->b:Lcom/dropbox/android/provider/MetadataManager;

    return-object v0
.end method

.method static synthetic m(Lcom/dropbox/android/activity/GalleryActivity;)I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->q:I

    return v0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 453
    const v0, 0x7f0700bb

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    .line 455
    new-instance v0, Lcom/dropbox/android/activity/bh;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bh;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    .line 464
    new-instance v1, Lcom/dropbox/android/activity/bi;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/bi;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    .line 479
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 480
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 484
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    .line 485
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    new-instance v2, Lcom/dropbox/android/activity/bj;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bj;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/GalleryView;->setTouchListener(Lcom/dropbox/android/widget/ax;)V

    .line 509
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->b:Lcom/dropbox/android/activity/bq;

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_2

    .line 511
    :cond_1
    const v0, 0x7f0700b6

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 512
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 513
    new-instance v2, Lcom/dropbox/android/activity/bk;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bk;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    const v2, 0x7f0d0294

    invoke-direct {p0, v0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/view/View;I)V

    .line 530
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->b:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_3

    .line 531
    const v0, 0x7f0700b7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 532
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 533
    new-instance v2, Lcom/dropbox/android/activity/bl;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bl;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    const v2, 0x7f0d0293

    invoke-direct {p0, v0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/view/View;I)V

    .line 548
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->c:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_4

    .line 549
    const v0, 0x7f0700b4

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 551
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 552
    new-instance v2, Lcom/dropbox/android/activity/aS;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aS;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->b:Lcom/dropbox/android/activity/bq;

    if-eq v0, v2, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_6

    .line 569
    :cond_5
    const v0, 0x7f0700b8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 571
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 574
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v3, Lcom/dropbox/android/activity/bq;->b:Lcom/dropbox/android/activity/bq;

    if-ne v0, v3, :cond_9

    .line 575
    new-instance v0, Lcom/dropbox/android/activity/aT;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aT;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    .line 604
    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 605
    const v0, 0x7f0d0292

    invoke-direct {p0, v2, v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/view/View;I)V

    .line 609
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_7

    .line 610
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 611
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/aV;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aV;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/aW;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aW;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 650
    :cond_7
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->d:Lcom/dropbox/android/activity/bq;

    if-eq v0, v2, :cond_8

    .line 651
    const v0, 0x7f0700b5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 653
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 654
    new-instance v1, Lcom/dropbox/android/activity/aX;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/aX;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 688
    :cond_8
    return-void

    .line 592
    :cond_9
    new-instance v0, Lcom/dropbox/android/activity/aU;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aU;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    goto :goto_0
.end method

.method static synthetic n(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 847
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/bs;->b:Lcom/dropbox/android/util/bs;

    invoke-static {p0, v0, v1, v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;)V

    .line 849
    return-void
.end method

.method static synthetic o(Lcom/dropbox/android/activity/GalleryActivity;)J
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    return-wide v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 852
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/Activities;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 853
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/albums/Album;)V
    .locals 4

    .prologue
    .line 824
    if-nez p1, :cond_0

    .line 825
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 826
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 828
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->c:Lcom/dropbox/android/activity/base/d;

    new-instance v2, Lcom/dropbox/android/activity/ba;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/ba;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Ljava/util/HashSet;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/base/d;->a(Ljava/lang/Runnable;)Z

    .line 843
    :goto_0
    return-void

    .line 836
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 838
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 839
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 841
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->s:Lcom/dropbox/android/albums/o;

    new-instance v3, Lcom/dropbox/android/albums/h;

    invoke-direct {v3, p1, v0}, Lcom/dropbox/android/albums/h;-><init>(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 930
    iget-boolean v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    .line 931
    return-void

    .line 930
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 985
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 986
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->f()Lcom/dropbox/android/albums/AlbumItemEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 987
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->u:Lcom/dropbox/android/albums/o;

    new-instance v2, Lcom/dropbox/android/albums/g;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->r:Lcom/dropbox/android/albums/Album;

    invoke-direct {v2, v3, v0}, Lcom/dropbox/android/albums/g;-><init>(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 988
    return-void
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 1227
    const/4 v0, 0x0

    .line 1228
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1229
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "FINAL_IMAGE_PATH"

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "FINAL_IMAGE_INDEX"

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/GalleryView;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 1233
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/activity/GalleryActivity;->setResult(ILandroid/content/Intent;)V

    .line 1234
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->finish()V

    .line 1235
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/GalleryView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 981
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 982
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 287
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 289
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->finish()V

    .line 348
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->b:Lcom/dropbox/android/provider/MetadataManager;

    .line 296
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "TYPE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/bq;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v3, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-eq v0, v3, :cond_1

    .line 298
    new-instance v0, Lcom/dropbox/android/activity/bu;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    invoke-direct {v0, v3}, Lcom/dropbox/android/activity/bu;-><init>(Lcom/dropbox/android/activity/bq;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    .line 301
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "SEARCH_BEGIN"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->q:I

    .line 303
    const v0, 0x7f03003d

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->setContentView(I)V

    .line 305
    const v0, 0x7f0700ba

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/GalleryView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    .line 306
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/GalleryView;->setThumbnailStore(Lcom/dropbox/android/taskqueue/D;)V

    .line 307
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/GalleryView;->setType(Lcom/dropbox/android/activity/bq;)V

    .line 310
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->f()Landroid/net/Uri;

    move-result-object v0

    .line 311
    if-nez v0, :cond_7

    move-object v0, v2

    .line 313
    :goto_1
    if-eqz v0, :cond_2

    .line 314
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->r:Lcom/dropbox/android/albums/Album;

    .line 315
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->r:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/GalleryView;->setAlbum(Lcom/dropbox/android/albums/Album;)V

    .line 318
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryActivity;->k()V

    .line 319
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryActivity;->m()V

    .line 321
    if-eqz p1, :cond_8

    .line 322
    const-string v0, "SIS_KEY_SavedInstanceStringSortKeyValue"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 323
    const-string v0, "SIS_KEY_SavedInstanceStringSortKeyValue"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    .line 325
    :cond_3
    const-string v0, "SIS_KEY_SavedInstanceLongSortKeyValue"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326
    const-string v0, "SIS_KEY_SavedInstanceLongSortKeyValue"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    .line 328
    :cond_4
    const-string v0, "SIS_KEY_SelectedScale"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 329
    const-string v3, "SIS_KEY_SelectedCenterX"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v3

    .line 330
    const-string v4, "SIS_KEY_SelectedCenterY"

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 331
    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v5, v3, v4, v0}, Lcom/dropbox/android/widget/GalleryView;->setCurrentImagePosScale(FFF)V

    .line 341
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->w:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 342
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-wide v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    cmp-long v0, v2, v6

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->a:Lcom/dropbox/android/activity/bq;

    if-ne v0, v2, :cond_a

    :cond_6
    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 346
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->v:Lcom/dropbox/android/util/as;

    .line 347
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->v:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->a(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 311
    :cond_7
    const-string v3, "album_gid"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 333
    :cond_8
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_SELECTED_SORT_KEY_STRING_VALUE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 334
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_SELECTED_SORT_KEY_STRING_VALUE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    .line 336
    :cond_9
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_SELECTED_SORT_KEY_LONG_VALUE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 337
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_SELECTED_SORT_KEY_LONG_VALUE"

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:J

    goto :goto_2

    :cond_a
    move v0, v1

    .line 342
    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 967
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onDestroy()V

    .line 969
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->v:Lcom/dropbox/android/util/as;

    if-eqz v0, :cond_0

    .line 970
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->v:Lcom/dropbox/android/util/as;

    invoke-virtual {v0}, Lcom/dropbox/android/util/as;->a()V

    .line 973
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 974
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/D;->a()V

    .line 976
    :cond_1
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1171
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1172
    sparse-switch p1, :sswitch_data_0

    .line 1219
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1177
    :sswitch_0
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1178
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    goto :goto_0

    .line 1184
    :sswitch_1
    iget-boolean v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-eqz v2, :cond_1

    .line 1186
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1187
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    goto :goto_0

    .line 1192
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->i()V

    move v0, v1

    .line 1193
    goto :goto_0

    .line 1196
    :sswitch_2
    iget-boolean v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-eqz v2, :cond_2

    .line 1198
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1199
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    goto :goto_0

    .line 1204
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->j()V

    move v0, v1

    .line 1205
    goto :goto_0

    .line 1209
    :sswitch_3
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1210
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->d()V

    move v0, v1

    .line 1211
    goto :goto_0

    .line 1172
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_0
        0x52 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 956
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onPause()V

    .line 957
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 961
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 962
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 963
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 935
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 937
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->d()Landroid/database/Cursor;

    move-result-object v0

    .line 938
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    if-eqz v1, :cond_0

    .line 939
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget-boolean v1, v1, Lcom/dropbox/android/activity/bu;->b:Z

    if-eqz v1, :cond_1

    .line 940
    const-string v1, "SIS_KEY_SavedInstanceStringSortKeyValue"

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget v2, v2, Lcom/dropbox/android/activity/bu;->a:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    :cond_0
    :goto_0
    const-string v0, "SIS_KEY_SelectedScale"

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->g()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 947
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->e:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->h()[F

    move-result-object v0

    .line 948
    const-string v1, "SIS_KEY_SelectedCenterX"

    const/4 v2, 0x0

    aget v2, v0, v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 949
    const-string v1, "SIS_KEY_SelectedCenterY"

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 951
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->v:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->b(Landroid/os/Bundle;)V

    .line 952
    return-void

    .line 942
    :cond_1
    const-string v1, "SIS_KEY_SavedInstanceLongSortKeyValue"

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->p:Lcom/dropbox/android/activity/bu;

    iget v2, v2, Lcom/dropbox/android/activity/bu;->a:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method
