.class public final enum Lcom/dropbox/android/activity/eD;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/eD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/eD;

.field public static final enum b:Lcom/dropbox/android/activity/eD;

.field public static final enum c:Lcom/dropbox/android/activity/eD;

.field public static final enum d:Lcom/dropbox/android/activity/eD;

.field private static final synthetic f:[Lcom/dropbox/android/activity/eD;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/dropbox/android/activity/eD;

    const-string v1, "FEEDBACK_LIKE"

    const-string v2, "like"

    invoke-direct {v0, v1, v3, v2}, Lcom/dropbox/android/activity/eD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/eD;->a:Lcom/dropbox/android/activity/eD;

    .line 41
    new-instance v0, Lcom/dropbox/android/activity/eD;

    const-string v1, "FEEDBACK_DISLIKE"

    const-string v2, "dislike"

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/activity/eD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/eD;->b:Lcom/dropbox/android/activity/eD;

    .line 42
    new-instance v0, Lcom/dropbox/android/activity/eD;

    const-string v1, "FEEDBACK_BUG"

    const-string v2, "bug"

    invoke-direct {v0, v1, v5, v2}, Lcom/dropbox/android/activity/eD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/eD;->c:Lcom/dropbox/android/activity/eD;

    .line 43
    new-instance v0, Lcom/dropbox/android/activity/eD;

    const-string v1, "FEEDBACK_HELP"

    const-string v2, "help"

    invoke-direct {v0, v1, v6, v2}, Lcom/dropbox/android/activity/eD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/eD;->d:Lcom/dropbox/android/activity/eD;

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/activity/eD;

    sget-object v1, Lcom/dropbox/android/activity/eD;->a:Lcom/dropbox/android/activity/eD;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/eD;->b:Lcom/dropbox/android/activity/eD;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/eD;->c:Lcom/dropbox/android/activity/eD;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/eD;->d:Lcom/dropbox/android/activity/eD;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/activity/eD;->f:[Lcom/dropbox/android/activity/eD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, Lcom/dropbox/android/activity/eD;->e:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/eD;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/dropbox/android/activity/eD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/eD;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/eD;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/dropbox/android/activity/eD;->f:[Lcom/dropbox/android/activity/eD;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/eD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/eD;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/activity/eD;->e:Ljava/lang/String;

    return-object v0
.end method
