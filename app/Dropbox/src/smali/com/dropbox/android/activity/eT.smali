.class final Lcom/dropbox/android/activity/eT;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/albums/Album;

.field final synthetic c:Lcom/dropbox/android/widget/HorizontalListView;

.field final synthetic d:Lcom/dropbox/android/activity/ShareViaEmailFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/dropbox/android/activity/eT;->d:Lcom/dropbox/android/activity/ShareViaEmailFragment;

    iput-object p2, p0, Lcom/dropbox/android/activity/eT;->a:Lcom/dropbox/android/albums/PhotosModel;

    iput-object p3, p0, Lcom/dropbox/android/activity/eT;->b:Lcom/dropbox/android/albums/Album;

    iput-object p4, p0, Lcom/dropbox/android/activity/eT;->c:Lcom/dropbox/android/widget/HorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 336
    iget-object v0, p0, Lcom/dropbox/android/activity/eT;->d:Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Lcom/dropbox/android/activity/eL;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/dropbox/android/activity/eL;->a(Landroid/database/Cursor;)V

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/activity/eT;->c:Lcom/dropbox/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/HorizontalListView;->requestLayout()V

    .line 338
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    new-instance v0, Lcom/dropbox/android/activity/eV;

    iget-object v1, p0, Lcom/dropbox/android/activity/eT;->d:Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/eT;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v3, p0, Lcom/dropbox/android/activity/eT;->b:Lcom/dropbox/android/albums/Album;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/eV;-><init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;)V

    return-object v0
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 328
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eT;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 342
    iget-object v0, p0, Lcom/dropbox/android/activity/eT;->d:Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Lcom/dropbox/android/activity/eL;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/eL;->a(Landroid/database/Cursor;)V

    .line 343
    return-void
.end method
