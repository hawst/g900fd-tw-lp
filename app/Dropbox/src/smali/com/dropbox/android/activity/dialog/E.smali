.class final Lcom/dropbox/android/activity/dialog/E;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic b:Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/E;->b:Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    iput-object p2, p0, Lcom/dropbox/android/activity/dialog/E;->a:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/E;->b:Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->dismiss()V

    .line 102
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/E;->b:Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/E;->a:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 103
    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/E;->b:Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 104
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/E;->b:Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 105
    return-void
.end method
