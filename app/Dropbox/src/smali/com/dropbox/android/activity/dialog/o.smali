.class public final enum Lcom/dropbox/android/activity/dialog/o;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/dialog/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/dialog/o;

.field public static final enum b:Lcom/dropbox/android/activity/dialog/o;

.field public static final enum c:Lcom/dropbox/android/activity/dialog/o;

.field public static final enum d:Lcom/dropbox/android/activity/dialog/o;

.field public static final enum e:Lcom/dropbox/android/activity/dialog/o;

.field public static final enum f:Lcom/dropbox/android/activity/dialog/o;

.field public static final enum g:Lcom/dropbox/android/activity/dialog/o;

.field private static final synthetic k:[Lcom/dropbox/android/activity/dialog/o;


# instance fields
.field private final h:Lcom/dropbox/android/activity/payment/h;

.field private final i:I

.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    const v7, 0x7f0d0067

    .line 34
    new-instance v0, Lcom/dropbox/android/activity/dialog/o;

    const-string v1, "MANUAL_UPLOAD"

    sget-object v3, Lcom/dropbox/android/activity/payment/h;->e:Lcom/dropbox/android/activity/payment/h;

    const v4, 0x7f0d0068

    const v5, 0x7f0d006a

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v0, Lcom/dropbox/android/activity/dialog/o;->a:Lcom/dropbox/android/activity/dialog/o;

    .line 38
    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    const-string v4, "MOVE_FILE"

    sget-object v6, Lcom/dropbox/android/activity/payment/h;->f:Lcom/dropbox/android/activity/payment/h;

    const v8, 0x7f0d006b

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v3, Lcom/dropbox/android/activity/dialog/o;->b:Lcom/dropbox/android/activity/dialog/o;

    .line 42
    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    const-string v4, "MOVE_FOLDER"

    sget-object v6, Lcom/dropbox/android/activity/payment/h;->g:Lcom/dropbox/android/activity/payment/h;

    const v8, 0x7f0d006c

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v3, Lcom/dropbox/android/activity/dialog/o;->c:Lcom/dropbox/android/activity/dialog/o;

    .line 46
    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    const-string v4, "RENAME_FILE"

    sget-object v6, Lcom/dropbox/android/activity/payment/h;->h:Lcom/dropbox/android/activity/payment/h;

    const v8, 0x7f0d006d

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v3, Lcom/dropbox/android/activity/dialog/o;->d:Lcom/dropbox/android/activity/dialog/o;

    .line 50
    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    const-string v4, "RENAME_FOLDER"

    const/4 v5, 0x4

    sget-object v6, Lcom/dropbox/android/activity/payment/h;->i:Lcom/dropbox/android/activity/payment/h;

    const v8, 0x7f0d006e

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v3, Lcom/dropbox/android/activity/dialog/o;->e:Lcom/dropbox/android/activity/dialog/o;

    .line 54
    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    const-string v4, "NEW_FOLDER"

    const/4 v5, 0x5

    sget-object v6, Lcom/dropbox/android/activity/payment/h;->j:Lcom/dropbox/android/activity/payment/h;

    const v8, 0x7f0d006f

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v3, Lcom/dropbox/android/activity/dialog/o;->f:Lcom/dropbox/android/activity/dialog/o;

    .line 58
    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    const-string v4, "SHARED_FOLDER"

    const/4 v5, 0x6

    sget-object v6, Lcom/dropbox/android/activity/payment/h;->k:Lcom/dropbox/android/activity/payment/h;

    const v8, 0x7f0d0070

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/activity/dialog/o;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V

    sput-object v3, Lcom/dropbox/android/activity/dialog/o;->g:Lcom/dropbox/android/activity/dialog/o;

    .line 33
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dropbox/android/activity/dialog/o;

    sget-object v1, Lcom/dropbox/android/activity/dialog/o;->a:Lcom/dropbox/android/activity/dialog/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/dialog/o;->b:Lcom/dropbox/android/activity/dialog/o;

    aput-object v1, v0, v9

    sget-object v1, Lcom/dropbox/android/activity/dialog/o;->c:Lcom/dropbox/android/activity/dialog/o;

    aput-object v1, v0, v10

    sget-object v1, Lcom/dropbox/android/activity/dialog/o;->d:Lcom/dropbox/android/activity/dialog/o;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/activity/dialog/o;->e:Lcom/dropbox/android/activity/dialog/o;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/activity/dialog/o;->f:Lcom/dropbox/android/activity/dialog/o;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/activity/dialog/o;->g:Lcom/dropbox/android/activity/dialog/o;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/activity/dialog/o;->k:[Lcom/dropbox/android/activity/dialog/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/payment/h;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/payment/h;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput-object p3, p0, Lcom/dropbox/android/activity/dialog/o;->h:Lcom/dropbox/android/activity/payment/h;

    .line 68
    iput p4, p0, Lcom/dropbox/android/activity/dialog/o;->j:I

    .line 69
    iput p5, p0, Lcom/dropbox/android/activity/dialog/o;->i:I

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/o;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/dropbox/android/activity/dialog/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/o;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/dialog/o;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->k:[Lcom/dropbox/android/activity/dialog/o;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/dialog/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/dialog/o;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/activity/payment/h;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/o;->h:Lcom/dropbox/android/activity/payment/h;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/dropbox/android/activity/dialog/o;->i:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/dropbox/android/activity/dialog/o;->j:I

    return v0
.end method
