.class public final enum Lcom/dropbox/android/activity/bH;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/bH;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/bH;

.field public static final enum b:Lcom/dropbox/android/activity/bH;

.field public static final enum c:Lcom/dropbox/android/activity/bH;

.field public static final enum d:Lcom/dropbox/android/activity/bH;

.field private static final synthetic e:[Lcom/dropbox/android/activity/bH;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/dropbox/android/activity/bH;

    const-string v1, "FILE"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/bH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bH;->a:Lcom/dropbox/android/activity/bH;

    .line 71
    new-instance v0, Lcom/dropbox/android/activity/bH;

    const-string v1, "CHOOSER_PREVIEW_LINK"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/bH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bH;->b:Lcom/dropbox/android/activity/bH;

    .line 76
    new-instance v0, Lcom/dropbox/android/activity/bH;

    const-string v1, "CHOOSER_DIRECT_LINK"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/bH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bH;->c:Lcom/dropbox/android/activity/bH;

    .line 81
    new-instance v0, Lcom/dropbox/android/activity/bH;

    const-string v1, "CHOOSER_FILE"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/bH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bH;->d:Lcom/dropbox/android/activity/bH;

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/activity/bH;

    sget-object v1, Lcom/dropbox/android/activity/bH;->a:Lcom/dropbox/android/activity/bH;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/bH;->b:Lcom/dropbox/android/activity/bH;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/bH;->c:Lcom/dropbox/android/activity/bH;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/bH;->d:Lcom/dropbox/android/activity/bH;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/activity/bH;->e:[Lcom/dropbox/android/activity/bH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bH;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/dropbox/android/activity/bH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/bH;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/bH;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/dropbox/android/activity/bH;->e:[Lcom/dropbox/android/activity/bH;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/bH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/bH;

    return-object v0
.end method
