.class public Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    .line 14
    new-instance v0, Lcom/dropbox/android/activity/base/k;

    invoke-direct {v0}, Lcom/dropbox/android/activity/base/k;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->a:Lcom/dropbox/android/activity/base/k;

    return-void
.end method


# virtual methods
.method protected final a()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/k;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 19
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->a:Lcom/dropbox/android/activity/base/k;

    check-cast p1, Lcom/dropbox/android/activity/base/BaseUserActivity;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;)V

    .line 20
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseUserActivity;

    .line 26
    iget-object v1, p0, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v1, v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;Landroid/os/Bundle;)V

    .line 27
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 32
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method
