.class public Lcom/dropbox/android/activity/RecoverAccountFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/ev;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private c:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/RecoverAccountFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/RecoverAccountFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/RecoverAccountFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/RecoverAccountFragment;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/dropbox/android/activity/RecoverAccountFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/RecoverAccountFragment;-><init>()V

    .line 35
    invoke-virtual {v0}, Lcom/dropbox/android/activity/RecoverAccountFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/RecoverAccountFragment;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dropbox/android/activity/RecoverAccountFragment;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 78
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ev;

    .line 80
    if-eqz v0, :cond_0

    .line 81
    iget-object v1, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/ev;->f(Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bj()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/ev;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    const-class v0, Lcom/dropbox/android/activity/ev;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ev;

    const v1, 0x7f03008b

    const v2, 0x7f0300ae

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/dropbox/android/activity/ev;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v1

    .line 58
    const v0, 0x7f070167

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->c:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/RecoverAccountFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_EMAIL"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ev;

    invoke-interface {v0}, Lcom/dropbox/android/activity/ev;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 63
    :goto_0
    new-instance v2, Lcom/dropbox/android/activity/eu;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/eu;-><init>(Lcom/dropbox/android/activity/RecoverAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-object v1

    .line 61
    :cond_0
    const v0, 0x7f070169

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onResume()V

    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/RecoverAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ev;

    const v1, 0x7f0d022b

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/ev;->a(I)V

    .line 47
    return-void
.end method
