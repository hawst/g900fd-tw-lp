.class abstract enum Lcom/dropbox/android/activity/al;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/al;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/al;

.field public static final enum b:Lcom/dropbox/android/activity/al;

.field public static final enum c:Lcom/dropbox/android/activity/al;

.field public static final enum d:Lcom/dropbox/android/activity/al;

.field public static final enum e:Lcom/dropbox/android/activity/al;

.field private static final synthetic g:[Lcom/dropbox/android/activity/al;


# instance fields
.field private f:Landroid/support/v4/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 86
    new-instance v0, Lcom/dropbox/android/activity/am;

    const-string v1, "BROWSER"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    .line 92
    new-instance v0, Lcom/dropbox/android/activity/an;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/an;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/al;

    .line 98
    new-instance v0, Lcom/dropbox/android/activity/ao;

    const-string v1, "PHOTOS"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    .line 104
    new-instance v0, Lcom/dropbox/android/activity/ap;

    const-string v1, "ALBUMS"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/al;->d:Lcom/dropbox/android/activity/al;

    .line 110
    new-instance v0, Lcom/dropbox/android/activity/aq;

    const-string v1, "NOTIFICATIONS"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/activity/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/al;->e:Lcom/dropbox/android/activity/al;

    .line 85
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/activity/al;

    sget-object v1, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/al;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/al;->d:Lcom/dropbox/android/activity/al;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/al;->e:Lcom/dropbox/android/activity/al;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/activity/al;->g:[Lcom/dropbox/android/activity/al;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/ac;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/al;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .locals 6

    .prologue
    .line 137
    invoke-static {}, Lcom/dropbox/android/activity/al;->values()[Lcom/dropbox/android/activity/al;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 138
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/activity/al;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    iput-object v4, v3, Lcom/dropbox/android/activity/al;->f:Landroid/support/v4/app/Fragment;

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/al;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/dropbox/android/activity/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/al;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/al;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/dropbox/android/activity/al;->g:[Lcom/dropbox/android/activity/al;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/al;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/activity/al;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/dropbox/android/activity/al;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/al;->f:Landroid/support/v4/app/Fragment;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->f:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KEY_FRAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/al;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->f:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract d()Landroid/support/v4/app/Fragment;
.end method
