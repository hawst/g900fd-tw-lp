.class final Lcom/dropbox/android/activity/ff;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/TextEditActivity;

.field final synthetic b:Lcom/dropbox/android/activity/fd;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/fd;Lcom/dropbox/android/activity/TextEditActivity;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/dropbox/android/activity/ff;->b:Lcom/dropbox/android/activity/fd;

    iput-object p2, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->b(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0, v2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Z)Z

    .line 116
    iget-object v0, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/fi;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 122
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/TextEditActivity;->b(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;Z)Z

    .line 119
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bg()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/activity/ff;->a:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    goto :goto_0
.end method
