.class final Lcom/dropbox/android/activity/dA;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/util/DropboxPath;

.field final synthetic b:I

.field final synthetic c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Lcom/dropbox/android/util/DropboxPath;I)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    iput-object p2, p0, Lcom/dropbox/android/activity/dA;->a:Lcom/dropbox/android/util/DropboxPath;

    iput p3, p0, Lcom/dropbox/android/activity/dA;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 315
    iget-object v0, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bC;->g()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->m:Lcom/dropbox/android/util/aW;

    .line 318
    iget-object v0, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/dA;->a:Lcom/dropbox/android/util/DropboxPath;

    iget v2, p0, Lcom/dropbox/android/activity/dA;->b:I

    iget-object v3, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    invoke-static {v3}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->b(Lcom/dropbox/android/activity/PhotoGridFragmentBase;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;I)V

    .line 319
    iget-object v0, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->requestLayout()V

    .line 324
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    sget-object v1, Lcom/dropbox/android/activity/dD;->c:Lcom/dropbox/android/activity/dD;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Lcom/dropbox/android/activity/dD;)Lcom/dropbox/android/activity/dD;

    .line 322
    iget-object v0, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/dropbox/android/activity/dA;->a:Lcom/dropbox/android/util/DropboxPath;

    iget v3, p0, Lcom/dropbox/android/activity/dA;->b:I

    iget-object v4, p0, Lcom/dropbox/android/activity/dA;->c:Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    invoke-static {v4}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->b(Lcom/dropbox/android/activity/PhotoGridFragmentBase;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Landroid/util/Pair;)Landroid/util/Pair;

    goto :goto_0
.end method
