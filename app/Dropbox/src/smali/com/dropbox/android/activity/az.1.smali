.class public Lcom/dropbox/android/activity/az;
.super Lcom/dropbox/android/util/D;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/DropboxWebViewActivity;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 221
    iput-object p1, p0, Lcom/dropbox/android/activity/az;->a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

    .line 222
    invoke-direct {p0, p2}, Lcom/dropbox/android/util/D;-><init>(Landroid/app/Activity;)V

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/az;->b:Z

    .line 223
    return-void
.end method


# virtual methods
.method public final a(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/dropbox/android/activity/az;->a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-static {v0, p2}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a(Lcom/dropbox/android/activity/DropboxWebViewActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lcom/dropbox/android/activity/az;->a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->setSupportProgressBarVisibility(Z)V

    .line 244
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/dropbox/android/activity/az;->a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a(Lcom/dropbox/android/activity/DropboxWebViewActivity;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/az;->b:Z

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/activity/az;->a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a(Lcom/dropbox/android/activity/DropboxWebViewActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 238
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/az;->b:Z

    .line 230
    iget-object v0, p0, Lcom/dropbox/android/activity/az;->a:Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-static {v0, p3}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 231
    return-void
.end method
