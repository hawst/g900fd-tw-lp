.class final Lcom/dropbox/android/activity/auth/o;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/dropbox/android/activity/auth/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/auth/DropboxAuth;

.field private final b:Ldbxyzptlk/db231222/r/d;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    .line 431
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 432
    iput-object p3, p0, Lcom/dropbox/android/activity/auth/o;->b:Ldbxyzptlk/db231222/r/d;

    .line 433
    return-void
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/String;)Lcom/dropbox/android/activity/auth/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 440
    aget-object v0, p2, v4

    .line 441
    iget-object v1, p0, Lcom/dropbox/android/activity/auth/o;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    .line 442
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 443
    invoke-interface {v1, v2}, Ldbxyzptlk/db231222/y/n;->a(Lorg/apache/http/HttpRequest;)V

    .line 446
    :try_start_0
    invoke-static {v1, v2}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 456
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 458
    new-instance v2, Lcom/dropbox/android/activity/auth/n;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/dropbox/android/activity/auth/n;-><init>(Lcom/dropbox/android/activity/auth/g;)V

    .line 459
    iput-object v0, v2, Lcom/dropbox/android/activity/auth/n;->a:Ljava/lang/String;

    .line 460
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/dropbox/android/activity/auth/n;->b:Ljava/lang/String;

    .line 461
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v4

    iput-object v0, v2, Lcom/dropbox/android/activity/auth/n;->c:Ljava/lang/String;

    .line 462
    return-object v2

    .line 447
    :catch_0
    move-exception v0

    .line 451
    iget-object v1, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v2, v4}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    .line 452
    iget-object v2, p0, Lcom/dropbox/android/activity/auth/o;->b:Ldbxyzptlk/db231222/r/d;

    iget-object v3, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v1, v2, v3}, Lcom/dropbox/android/util/Activities;->a(Landroid/content/Intent;Ldbxyzptlk/db231222/r/d;Landroid/app/Activity;)V

    .line 453
    iget-object v1, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    .line 454
    throw v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 426
    check-cast p2, [Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/auth/o;->a(Landroid/content/Context;[Ljava/lang/String;)Lcom/dropbox/android/activity/auth/n;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/activity/auth/n;)V
    .locals 6

    .prologue
    .line 467
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->g(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p2, Lcom/dropbox/android/activity/auth/n;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/dropbox/android/activity/auth/n;->b:Ljava/lang/String;

    iget-object v3, p2, Lcom/dropbox/android/activity/auth/n;->c:Ljava/lang/String;

    const-string v4, "utf-8"

    iget-object v5, p2, Lcom/dropbox/android/activity/auth/n;->a:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 472
    instance-of v0, p2, Ldbxyzptlk/db231222/w/i;

    if-eqz v0, :cond_0

    check-cast p2, Ldbxyzptlk/db231222/w/i;

    iget v0, p2, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    .line 473
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->h(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    .line 479
    :goto_0
    return-void

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->c(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/o;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->b(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 426
    check-cast p2, Lcom/dropbox/android/activity/auth/n;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/auth/o;->a(Landroid/content/Context;Lcom/dropbox/android/activity/auth/n;)V

    return-void
.end method
