.class public abstract Lcom/dropbox/android/activity/dialog/PhotoBatchRemoveConfirmDialogFrag;
.super Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TargetFrag:",
        "Landroid/support/v4/app/Fragment;",
        ">",
        "Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag",
        "<TTargetFrag;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/res/Resources;Landroid/support/v4/app/Fragment;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "TTargetFrag;II)V"
        }
    .end annotation

    .prologue
    .line 15
    add-int v0, p3, p4

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This dlg only supports 1 or more."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    sget-object v0, Lcom/dropbox/android/albums/k;->b:Lcom/dropbox/android/albums/k;

    invoke-static {p1, v0, p3, p4}, Lcom/dropbox/android/albums/i;->a(Landroid/content/res/Resources;Lcom/dropbox/android/albums/k;II)Ljava/lang/String;

    move-result-object v0

    .line 20
    const v1, 0x7f0d00a7

    invoke-virtual {p0, p2, v0, v1}, Lcom/dropbox/android/activity/dialog/PhotoBatchRemoveConfirmDialogFrag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 21
    return-void
.end method
