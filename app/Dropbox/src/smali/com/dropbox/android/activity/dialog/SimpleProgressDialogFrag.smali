.class public Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a()Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;-><init>()V

    return-object v0
.end method

.method public static b(Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 24
    const-class v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 27
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 28
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 30
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/Activities;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
