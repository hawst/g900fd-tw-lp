.class final Lcom/dropbox/android/activity/cO;
.super Ldbxyzptlk/db231222/g/J;
.source "panda.py"


# instance fields
.field private final c:Lcom/dropbox/android/service/a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/service/a;)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/g/J;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;)V

    .line 199
    iput-object p5, p0, Lcom/dropbox/android/activity/cO;->c:Lcom/dropbox/android/service/a;

    .line 200
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 205
    const v0, 0x7f0d0073

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 206
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 207
    return-void
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/K;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 222
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 224
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/cO;->b(Landroid/content/Context;)Lcom/dropbox/android/activity/MoveToFragment;

    move-result-object v2

    .line 227
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v1, :cond_1

    .line 228
    const v0, 0x7f0d005b

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 229
    invoke-virtual {v2}, Lcom/dropbox/android/activity/MoveToFragment;->d()V

    .line 282
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 233
    check-cast v0, Landroid/app/Activity;

    .line 235
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    if-ne v1, v3, :cond_3

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/activity/cO;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_2

    .line 238
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->c:Lcom/dropbox/android/activity/dialog/o;

    .line 243
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/activity/cO;->c:Lcom/dropbox/android/service/a;

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->e()Z

    move-result v1

    .line 244
    invoke-static {v0, v2, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Lcom/dropbox/android/activity/dialog/o;Lcom/dropbox/android/activity/dialog/p;Z)Lcom/dropbox/android/activity/dialog/OverQuotaDialog;

    move-result-object v0

    invoke-virtual {v2}, Lcom/dropbox/android/activity/MoveToFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 245
    invoke-virtual {v2}, Lcom/dropbox/android/activity/MoveToFragment;->d()V

    .line 274
    :goto_2
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->c:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v1, :cond_0

    .line 275
    new-instance v0, Lcom/dropbox/android/activity/cP;

    invoke-direct {v0, p0, v2}, Lcom/dropbox/android/activity/cP;-><init>(Lcom/dropbox/android/activity/cO;Lcom/dropbox/android/activity/MoveToFragment;)V

    invoke-virtual {v2, v0}, Lcom/dropbox/android/activity/MoveToFragment;->a(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 240
    :cond_2
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->b:Lcom/dropbox/android/activity/dialog/o;

    goto :goto_1

    .line 248
    :cond_3
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    if-eq v1, v3, :cond_4

    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    sget-object v3, Lcom/dropbox/android/taskqueue/w;->c:Lcom/dropbox/android/taskqueue/w;

    if-ne v1, v3, :cond_7

    .line 251
    :cond_4
    invoke-virtual {p2}, Ldbxyzptlk/db231222/g/K;->b()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 252
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v3

    .line 255
    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/activity/cO;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v5, v5, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 256
    const v4, 0x7f0d00bf

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    aput-object v3, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 268
    :goto_3
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 269
    invoke-static {p1, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 258
    :cond_5
    iget-object v4, p0, Lcom/dropbox/android/activity/cO;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v4, v4, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v4, :cond_6

    .line 259
    const v4, 0x7f0d00c1

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    aput-object v3, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 261
    :cond_6
    const v4, 0x7f0d00c0

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    aput-object v3, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 265
    :cond_7
    iget-object v1, p0, Lcom/dropbox/android/activity/cO;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_8

    const v1, 0x7f0d00be

    .line 266
    :goto_4
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 265
    :cond_8
    const v1, 0x7f0d00bd

    goto :goto_4
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 291
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/activity/cO;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d00be

    move v1, v0

    :goto_0
    move-object v0, p1

    .line 295
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 296
    invoke-static {p1, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 297
    return-void

    .line 294
    :cond_0
    const v0, 0x7f0d00bd

    move v1, v0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 192
    check-cast p2, Ldbxyzptlk/db231222/g/K;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/cO;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/K;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;)Lcom/dropbox/android/activity/MoveToFragment;
    .locals 2

    .prologue
    .line 210
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 211
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "TAG_MOVE"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MoveToFragment;

    return-object v0
.end method
