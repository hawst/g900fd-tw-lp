.class final Lcom/dropbox/android/activity/em;
.super Ljava/lang/Thread;
.source "panda.py"


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/r/d;

.field final synthetic b:Ldbxyzptlk/db231222/r/e;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/dropbox/android/activity/QrAuthActivity$QrAlarmReceiver;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/QrAuthActivity$QrAlarmReceiver;Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/r/e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/dropbox/android/activity/em;->d:Lcom/dropbox/android/activity/QrAuthActivity$QrAlarmReceiver;

    iput-object p2, p0, Lcom/dropbox/android/activity/em;->a:Ldbxyzptlk/db231222/r/d;

    iput-object p3, p0, Lcom/dropbox/android/activity/em;->b:Ldbxyzptlk/db231222/r/e;

    iput-object p4, p0, Lcom/dropbox/android/activity/em;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/activity/em;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    iget-object v2, p0, Lcom/dropbox/android/activity/em;->b:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    :goto_0
    return-void

    .line 158
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 159
    const-string v1, "com.dropbox.intent.extra.REMINDER_SOURCE"

    iget-object v2, p0, Lcom/dropbox/android/activity/em;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/dropbox/android/activity/em;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aH;->d:Lcom/dropbox/android/util/aH;

    const-string v3, "com.dropbox.android.notifications.TAG.ri_notification"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aH;Ljava/lang/String;Ljava/lang/Long;Landroid/os/Bundle;)I
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    invoke-static {}, Lcom/dropbox/android/activity/QrAuthActivity;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error getting account info or sending notification"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
