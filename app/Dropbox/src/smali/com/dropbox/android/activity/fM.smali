.class final Lcom/dropbox/android/activity/fM;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/VideoPlayerActivity;

.field private b:I


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .locals 1

    .prologue
    .line 113
    iput-object p1, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/fM;->b:I

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d()I

    move-result v0

    .line 136
    iget v1, p0, Lcom/dropbox/android/activity/fM;->b:I

    if-le v0, v1, :cond_0

    iget v1, p0, Lcom/dropbox/android/activity/fM;->b:I

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->b(Lcom/dropbox/android/activity/VideoPlayerActivity;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 138
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->M()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "wait"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/VideoPlayerActivity;->e(Lcom/dropbox/android/activity/VideoPlayerActivity;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->c(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->f(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/fM;->b:I

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ANALYTICS_CHAIN_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 152
    :goto_0
    return-void

    .line 149
    :cond_0
    iput v0, p0, Lcom/dropbox/android/activity/fM;->b:I

    .line 150
    iget-object v0, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->h(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fM;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->g(Lcom/dropbox/android/activity/VideoPlayerActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
