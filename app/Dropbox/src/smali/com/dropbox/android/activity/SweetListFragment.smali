.class public abstract Lcom/dropbox/android/activity/SweetListFragment;
.super Lcom/dropbox/android/activity/base/BaseListFragment;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AdapterType:",
        "Lcom/dropbox/android/widget/bC;",
        ">",
        "Lcom/dropbox/android/activity/base/BaseListFragment;"
    }
.end annotation


# instance fields
.field protected k:Lcom/dropbox/android/widget/SweetListView;

.field protected l:Lcom/dropbox/android/widget/bC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TAdapterType;"
        }
    .end annotation
.end field

.field protected m:Lcom/dropbox/android/util/aW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/util/aW",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected final n:Landroid/widget/AdapterView$OnItemClickListener;

.field protected final o:Landroid/widget/AdapterView$OnItemLongClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseListFragment;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->m:Lcom/dropbox/android/util/aW;

    .line 48
    new-instance v0, Lcom/dropbox/android/activity/eZ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eZ;-><init>(Lcom/dropbox/android/activity/SweetListFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->n:Landroid/widget/AdapterView$OnItemClickListener;

    .line 55
    new-instance v0, Lcom/dropbox/android/activity/fa;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fa;-><init>(Lcom/dropbox/android/activity/SweetListFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->o:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SweetListFragment;->g()Lcom/dropbox/android/util/aW;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->m:Lcom/dropbox/android/util/aW;

    .line 107
    const-string v0, "key_ScrollState"

    iget-object v1, p0, Lcom/dropbox/android/activity/SweetListFragment;->m:Lcom/dropbox/android/util/aW;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 108
    return-void
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    const-string v0, "key_ScrollState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "key_ScrollState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aW;

    iput-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->m:Lcom/dropbox/android/util/aW;

    .line 117
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->c(Landroid/os/Bundle;)V

    .line 121
    return-void
.end method

.method protected a(Lcom/dropbox/android/widget/SweetListView;Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method protected final g()Lcom/dropbox/android/util/aW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/android/util/aW",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 40
    iget-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/dropbox/android/activity/SweetListFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->b()Lcom/dropbox/android/util/aW;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/dropbox/android/util/aW;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aW;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 100
    if-eqz p1, :cond_0

    .line 101
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->a(Landroid/os/Bundle;)V

    .line 103
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 28
    if-eqz p1, :cond_0

    .line 29
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->a(Landroid/os/Bundle;)V

    .line 31
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseListFragment;->onDestroyView()V

    .line 94
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SweetListFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->b(Landroid/os/Bundle;)V

    .line 89
    :cond_0
    return-void
.end method
