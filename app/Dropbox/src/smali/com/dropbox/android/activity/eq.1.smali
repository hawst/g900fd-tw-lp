.class final Lcom/dropbox/android/activity/eq;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/l/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/l/l",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/QrAuthFragment;

.field private b:Lcom/dropbox/android/util/analytics/w;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/QrAuthFragment;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/dropbox/android/activity/eq;->a:Lcom/dropbox/android/activity/QrAuthFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 364
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/eq;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 364
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eq;->b(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 374
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/eq;->b:Lcom/dropbox/android/util/analytics/w;

    .line 377
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 381
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cn()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/eq;->b:Lcom/dropbox/android/util/analytics/w;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "status"

    const-string v2, "success"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 383
    iget-object v0, p0, Lcom/dropbox/android/activity/eq;->a:Lcom/dropbox/android/activity/QrAuthFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/QrAuthFragment;->a(Lcom/dropbox/android/activity/QrAuthFragment;)Lcom/dropbox/android/activity/et;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/er;->d:Lcom/dropbox/android/activity/er;

    const/4 v2, 0x1

    sget-object v3, Lcom/dropbox/android/activity/er;->d:Lcom/dropbox/android/activity/er;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/dropbox/android/activity/et;->a(Lcom/dropbox/android/activity/er;ZLjava/lang/String;)V

    .line 384
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 364
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eq;->a(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 388
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->c()Lcom/dropbox/android/service/J;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 389
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cn()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/eq;->b:Lcom/dropbox/android/util/analytics/w;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "status"

    const-string v2, "disconnected"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 391
    iget-object v0, p0, Lcom/dropbox/android/activity/eq;->a:Lcom/dropbox/android/activity/QrAuthFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/QrAuthFragment;->a(Lcom/dropbox/android/activity/QrAuthFragment;)Lcom/dropbox/android/activity/et;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/er;->e:Lcom/dropbox/android/activity/er;

    const/4 v2, 0x1

    sget-object v3, Lcom/dropbox/android/activity/er;->e:Lcom/dropbox/android/activity/er;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/dropbox/android/activity/et;->a(Lcom/dropbox/android/activity/er;ZLjava/lang/String;)V

    .line 402
    :goto_0
    return-void

    .line 398
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cn()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/eq;->b:Lcom/dropbox/android/util/analytics/w;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "status"

    const-string v2, "error"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/activity/eq;->a:Lcom/dropbox/android/activity/QrAuthFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/QrAuthFragment;->c(Lcom/dropbox/android/activity/QrAuthFragment;)Lcom/dropbox/android/widget/qr/QrReaderView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->b()V

    goto :goto_0
.end method
