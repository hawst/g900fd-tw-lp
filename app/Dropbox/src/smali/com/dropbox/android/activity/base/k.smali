.class public final Lcom/dropbox/android/activity/base/k;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ldbxyzptlk/db231222/r/d;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/k;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 79
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/k;->b:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "FragmentUserManager.SIS_USER_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 65
    const-string v0, "FragmentUserManager.SIS_USER_ID"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/base/BaseUserActivity;)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/base/BaseUserActivity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/k;->b:Z

    .line 40
    if-eqz p2, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->j()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    .line 48
    const-string v0, "FragmentUserManager.SIS_USER_ID"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    const-string v0, "User id mismatch."

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/k;->a:Ldbxyzptlk/db231222/r/d;

    .line 59
    :cond_1
    return-void
.end method
