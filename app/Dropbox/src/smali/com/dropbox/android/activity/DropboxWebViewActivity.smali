.class public Lcom/dropbox/android/activity/DropboxWebViewActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# instance fields
.field private a:Landroid/webkit/WebView;

.field private b:Landroid/widget/Button;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/Boolean;

.field private final h:Lcom/dropbox/android/util/e;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->b:Landroid/widget/Button;

    .line 38
    iput-object v1, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->f:Z

    .line 42
    iput-object v1, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->g:Ljava/lang/Boolean;

    .line 44
    new-instance v0, Lcom/dropbox/android/util/e;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/e;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->h:Lcom/dropbox/android/util/e;

    .line 220
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxWebViewActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->b:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxWebViewActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    return-object p1
.end method

.method private a(Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 175
    invoke-virtual {p1}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/D;->d()Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-virtual {p1, v1}, Ldbxyzptlk/db231222/z/M;->c(Ljava/lang/String;)[B

    move-result-object v1

    .line 184
    if-eqz v1, :cond_0

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/web_session_login"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    iget-object v2, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v0, v1}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    .line 191
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected final d()Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method protected e()Landroid/webkit/WebViewClient;
    .locals 1

    .prologue
    .line 249
    new-instance v0, Lcom/dropbox/android/activity/az;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/az;-><init>(Lcom/dropbox/android/activity/DropboxWebViewActivity;Landroid/app/Activity;)V

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 51
    const-wide/16 v5, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->requestWindowFeature(J)V

    .line 52
    const-string v0, "EXTRA_HAS_SPINNER"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-wide/16 v5, 0x5

    invoke-virtual {p0, v5, v6}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->requestWindowFeature(J)V

    .line 56
    :cond_0
    const-string v0, "EXTRA_HAS_BUTTONS"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->f:Z

    .line 60
    const-string v0, "EXTRA_REQUIRES_AUTH"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->g:Ljava/lang/Boolean;

    .line 62
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    if-eqz p1, :cond_1

    .line 64
    const-string v0, "SIS_KEY_URL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    .line 67
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->f:Z

    if-eqz v0, :cond_5

    .line 68
    const v0, 0x7f0300d8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->setContentView(I)V

    .line 74
    :goto_1
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->setSupportProgressBarVisibility(Z)V

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 79
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 80
    if-eqz v3, :cond_6

    .line 81
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget-object v5, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->h:Lcom/dropbox/android/util/e;

    invoke-virtual {v5, v3}, Lcom/dropbox/android/util/e;->a(Landroid/net/Uri;)V

    :goto_2
    move-object v3, v0

    .line 94
    :goto_3
    const v0, 0x7f0700cd

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    .line 98
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    new-instance v1, Lcom/dropbox/android/activity/aw;

    invoke-direct {v1, p0, p0}, Lcom/dropbox/android/activity/aw;-><init>(Lcom/dropbox/android/activity/DropboxWebViewActivity;Lcom/dropbox/android/activity/base/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e()Landroid/webkit/WebViewClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 118
    if-eqz v3, :cond_8

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_a

    .line 121
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_a

    .line 123
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a(Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)Z

    move-result v0

    .line 127
    :goto_4
    if-nez v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 136
    :cond_2
    :goto_5
    const-string v0, "EXTRA_TITLE"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 137
    const-string v0, "EXTRA_TITLE"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    :goto_6
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 143
    iget-boolean v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->f:Z

    if-eqz v0, :cond_3

    .line 144
    const v0, 0x7f0701d3

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/ax;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ax;-><init>(Lcom/dropbox/android/activity/DropboxWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    const v0, 0x7f0701d4

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->b:Landroid/widget/Button;

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/ay;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ay;-><init>(Lcom/dropbox/android/activity/DropboxWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 60
    goto/16 :goto_0

    .line 70
    :cond_5
    const v0, 0x7f030043

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->setContentView(I)V

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_1

    .line 88
    :cond_6
    sget-object v0, Lcom/dropbox/android/util/bz;->a:Lcom/dropbox/android/util/bz;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/bz;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 91
    :cond_7
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    move-object v3, v0

    goto/16 :goto_3

    .line 132
    :cond_8
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->finish()V

    goto :goto_5

    .line 139
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    goto :goto_6

    :cond_a
    move v0, v2

    goto :goto_4
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 207
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onDestroy()V

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->h:Lcom/dropbox/android/util/e;

    invoke-virtual {v0}, Lcom/dropbox/android/util/e;->a()V

    .line 209
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 197
    const-string v0, "SIS_KEY_URL"

    iget-object v1, p0, Lcom/dropbox/android/activity/DropboxWebViewActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return-void
.end method
