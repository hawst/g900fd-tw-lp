.class final Lcom/dropbox/android/activity/G;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private b:Z

.field private c:Z

.field private d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/activity/K;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/G;->b:Z

    .line 388
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/G;->c:Z

    .line 389
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    .line 392
    iput-object p1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    .line 393
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/G;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 410
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/G;->c:Z

    if-nez v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 412
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/K;

    .line 413
    iget-boolean v1, p0, Lcom/dropbox/android/activity/G;->b:Z

    if-eqz v1, :cond_2

    .line 414
    iget-object v1, v0, Lcom/dropbox/android/activity/K;->a:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    .line 415
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 421
    :goto_0
    iput-boolean v3, p0, Lcom/dropbox/android/activity/G;->b:Z

    .line 422
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/G;->a(Lcom/dropbox/android/activity/K;)V

    .line 458
    :cond_0
    :goto_1
    return-void

    .line 417
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/dropbox/android/activity/K;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    iget-object v1, v0, Lcom/dropbox/android/activity/K;->b:Landroid/content/res/ColorStateList;

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/G;->a(Landroid/content/res/ColorStateList;)V

    .line 419
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 424
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 425
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 426
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 427
    new-instance v2, Lcom/dropbox/android/activity/H;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/H;-><init>(Lcom/dropbox/android/activity/G;Lcom/dropbox/android/activity/K;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 451
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/G;->c:Z

    .line 452
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 454
    :cond_3
    invoke-direct {p0}, Lcom/dropbox/android/activity/G;->b()V

    goto :goto_1
.end method

.method private a(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 485
    if-eqz p1, :cond_0

    .line 486
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 488
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/G;Lcom/dropbox/android/activity/K;)V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/G;->a(Lcom/dropbox/android/activity/K;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/K;)V
    .locals 2

    .prologue
    .line 493
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/G;->c:Z

    .line 494
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 495
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/K;

    .line 496
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 497
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 498
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    invoke-direct {p0}, Lcom/dropbox/android/activity/G;->a()V

    .line 502
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/K;

    .line 462
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/activity/G;->c:Z

    .line 463
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->clearAnimation()V

    .line 464
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 465
    iget-object v1, v0, Lcom/dropbox/android/activity/K;->b:Landroid/content/res/ColorStateList;

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/G;->a(Landroid/content/res/ColorStateList;)V

    .line 466
    iget-object v1, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/dropbox/android/activity/K;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 468
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 469
    new-instance v2, Lcom/dropbox/android/activity/J;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/J;-><init>(Lcom/dropbox/android/activity/G;Lcom/dropbox/android/activity/K;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 481
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 482
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/G;)V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/dropbox/android/activity/G;->b()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Z)V
    .locals 2

    .prologue
    .line 400
    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/K;

    iget-object v0, v0, Lcom/dropbox/android/activity/K;->a:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 403
    :goto_0
    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Ljava/util/LinkedList;

    new-instance v1, Lcom/dropbox/android/activity/K;

    invoke-direct {v1, p1, p2}, Lcom/dropbox/android/activity/K;-><init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 405
    invoke-direct {p0}, Lcom/dropbox/android/activity/G;->a()V

    .line 407
    :cond_1
    return-void

    .line 400
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/dropbox/android/activity/G;->a(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Z)V

    .line 397
    return-void
.end method
