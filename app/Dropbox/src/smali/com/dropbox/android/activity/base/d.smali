.class public final Lcom/dropbox/android/activity/base/d;
.super Lcom/dropbox/android/activity/base/a;
.source "panda.py"


# static fields
.field private static final g:Lcom/dropbox/android/activity/base/i;


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/h;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/dropbox/android/activity/lock/LockReceiver;

.field private d:J

.field private e:Z

.field private f:Z

.field private final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/dropbox/android/activity/base/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/base/i;-><init>(Lcom/dropbox/android/activity/base/e;)V

    sput-object v0, Lcom/dropbox/android/activity/base/d;->g:Lcom/dropbox/android/activity/base/i;

    return-void
.end method

.method constructor <init>(Lcom/dropbox/android/activity/base/h;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 153
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/base/a;-><init>(Lcom/dropbox/android/activity/base/m;)V

    .line 57
    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->e:Z

    .line 58
    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->f:Z

    .line 65
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/d;->h:Ljava/util/Queue;

    .line 128
    new-instance v0, Lcom/dropbox/android/activity/base/e;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/base/e;-><init>(Lcom/dropbox/android/activity/base/d;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/d;->i:Landroid/content/BroadcastReceiver;

    .line 154
    iput-object p1, p0, Lcom/dropbox/android/activity/base/d;->a:Lcom/dropbox/android/activity/base/h;

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->a:Lcom/dropbox/android/activity/base/h;

    invoke-interface {v0}, Lcom/dropbox/android/activity/base/h;->l()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    .line 156
    invoke-static {}, Lcom/dropbox/android/activity/lock/LockReceiver;->a()Lcom/dropbox/android/activity/lock/LockReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/d;->c:Lcom/dropbox/android/activity/lock/LockReceiver;

    .line 157
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/dropbox/android/activity/base/d;->g:Lcom/dropbox/android/activity/base/i;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/i;->d()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/base/d;)Lcom/dropbox/android/activity/base/h;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->a:Lcom/dropbox/android/activity/base/h;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 212
    const-string v0, "network_state"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "count"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 213
    new-instance v1, Lcom/dropbox/android/activity/base/g;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/base/g;-><init>(Lcom/dropbox/android/activity/base/d;Lcom/dropbox/android/util/analytics/l;)V

    invoke-virtual {v1}, Lcom/dropbox/android/activity/base/g;->start()V

    .line 220
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/base/d;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->c:Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Intent;)V

    .line 264
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 160
    const-string v0, "create"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/h/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/h/a;->a(Landroid/app/Activity;)V

    .line 162
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.dropbox.android.filemanager.ApiManager.ACTION_UNLINKED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v1}, Landroid/support/v4/content/s;->a(Landroid/content/Context;)Landroid/support/v4/content/s;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/base/d;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/s;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 165
    if-eqz p1, :cond_0

    const-string v0, "SIS_KEY_ACTIVITY_RESULT_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "SIS_KEY_ACTIVITY_RESULT_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 167
    const-string v0, "SIS_KEY_ACTIVITY_RESULT_DATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    const-string v0, "SIS_KEY_ACTIVITY_RESULT_DATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 169
    iget-object v2, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v2, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 291
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/d;->h()Z

    move-result v0

    .line 292
    if-eqz v0, :cond_0

    .line 293
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 297
    :goto_0
    return v0

    .line 295
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->h:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 198
    sget-object v0, Lcom/dropbox/android/activity/base/d;->g:Lcom/dropbox/android/activity/base/i;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/i;->b()I

    move-result v0

    .line 199
    const-string v1, "start"

    iget-object v2, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "count"

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    .line 200
    if-ne v0, v5, :cond_0

    .line 201
    const-string v2, "time.in.background.ms"

    sget-object v3, Lcom/dropbox/android/activity/base/d;->g:Lcom/dropbox/android/activity/base/i;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/base/i;->c()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 203
    :cond_0
    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 204
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/base/d;->a(I)V

    .line 205
    iput-boolean v5, p0, Lcom/dropbox/android/activity/base/d;->e:Z

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    sget-object v1, Ldbxyzptlk/db231222/q/e;->a:Ldbxyzptlk/db231222/q/e;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/q/a;->a(Landroid/content/Context;Ldbxyzptlk/db231222/q/e;)V

    .line 208
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 178
    :try_start_0
    const-string v0, "android.app.Activity"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 179
    const-string v1, "mResultCode"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 180
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 181
    const-string v2, "mResultData"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 182
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 183
    iget-object v2, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    .line 184
    iget-object v2, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 185
    if-eqz v1, :cond_0

    .line 186
    const-string v2, "SIS_KEY_ACTIVITY_RESULT_CODE"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 188
    :cond_0
    if-eqz v0, :cond_1

    .line 189
    const-string v1, "SIS_KEY_ACTIVITY_RESULT_DATA"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_1
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 193
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find result code or data."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 223
    const-string v0, "resume"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 224
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/h/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/h/a;->c(Landroid/app/Activity;)V

    .line 226
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/activity/base/d;->d:J

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->f:Z

    .line 228
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->a:Lcom/dropbox/android/activity/base/h;

    invoke-interface {v0}, Lcom/dropbox/android/activity/base/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    if-nez v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->c:Lcom/dropbox/android/activity/lock/LockReceiver;

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/dropbox/android/activity/base/d;->a:Lcom/dropbox/android/activity/base/h;

    invoke-interface {v2}, Lcom/dropbox/android/activity/base/h;->a_()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/app/Activity;Z)V

    .line 239
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 242
    const-string v0, "stop"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "count"

    sget-object v2, Lcom/dropbox/android/activity/base/d;->g:Lcom/dropbox/android/activity/base/i;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/base/i;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->e:Z

    .line 244
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 247
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dropbox/android/activity/base/d;->d:J

    sub-long/2addr v0, v2

    .line 248
    const-string v2, "pause"

    iget-object v3, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "resumed_duration_millis"

    invoke-virtual {v2, v3, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->f:Z

    .line 252
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->c:Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/LockReceiver;->c()V

    .line 253
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 256
    const-string v0, "destroy"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 257
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cr()V

    .line 258
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/h/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/h/a;->b(Landroid/app/Activity;)V

    .line 259
    iget-object v0, p0, Lcom/dropbox/android/activity/base/d;->b:Landroid/app/Activity;

    invoke-static {v0}, Landroid/support/v4/content/s;->a(Landroid/content/Context;)Landroid/support/v4/content/s;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/d;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/s;->a(Landroid/content/BroadcastReceiver;)V

    .line 260
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->e:Z

    return v0
.end method

.method protected final h()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/d;->f:Z

    return v0
.end method
