.class final enum Lcom/dropbox/android/activity/bW;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/bW;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/bW;

.field public static final enum b:Lcom/dropbox/android/activity/bW;

.field public static final enum c:Lcom/dropbox/android/activity/bW;

.field public static final enum d:Lcom/dropbox/android/activity/bW;

.field private static final synthetic e:[Lcom/dropbox/android/activity/bW;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lcom/dropbox/android/activity/bW;

    const-string v1, "SPLASH"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/bW;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bW;->a:Lcom/dropbox/android/activity/bW;

    .line 55
    new-instance v0, Lcom/dropbox/android/activity/bW;

    const-string v1, "PHOTOS"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/bW;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bW;->b:Lcom/dropbox/android/activity/bW;

    .line 56
    new-instance v0, Lcom/dropbox/android/activity/bW;

    const-string v1, "DOCS"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/bW;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bW;->c:Lcom/dropbox/android/activity/bW;

    .line 57
    new-instance v0, Lcom/dropbox/android/activity/bW;

    const-string v1, "WRAPUP"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/bW;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bW;->d:Lcom/dropbox/android/activity/bW;

    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/activity/bW;

    sget-object v1, Lcom/dropbox/android/activity/bW;->a:Lcom/dropbox/android/activity/bW;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/bW;->b:Lcom/dropbox/android/activity/bW;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/bW;->c:Lcom/dropbox/android/activity/bW;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/bW;->d:Lcom/dropbox/android/activity/bW;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/activity/bW;->e:[Lcom/dropbox/android/activity/bW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bW;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/dropbox/android/activity/bW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/bW;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/bW;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/dropbox/android/activity/bW;->e:[Lcom/dropbox/android/activity/bW;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/bW;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/bW;

    return-object v0
.end method
