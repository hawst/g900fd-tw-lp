.class public Lcom/dropbox/android/activity/PhotoGridPickerFragment;
.super Lcom/dropbox/android/activity/PhotoGridFragmentBase;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Lcom/dropbox/android/albums/h;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/dropbox/android/util/as;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;-><init>()V

    return-void
.end method

.method private a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;
    .locals 7

    .prologue
    .line 38
    new-instance v0, Lcom/dropbox/android/activity/dF;

    const-string v2, "SIS_KEY_WaitingAddToAlbumId"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d027d

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/dF;-><init>(Lcom/dropbox/android/activity/PhotoGridPickerFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->a:Lcom/dropbox/android/albums/o;

    .line 60
    new-instance v0, Lcom/dropbox/android/util/as;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/dropbox/android/util/at;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->a:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/as;-><init>([Lcom/dropbox/android/util/at;)V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoGridPickerFragment;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->a(Lcom/dropbox/android/util/DropboxPath;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3

    .prologue
    .line 147
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 148
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    .line 149
    const-string v2, "ADDED_PATHS_EXTRA"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 150
    const-string v1, "PATH_TO_SCROLL_TO_EXTRA"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 151
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 152
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 153
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 125
    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->h:Lcom/dropbox/android/activity/dE;

    sget-object v4, Lcom/dropbox/android/activity/dE;->a:Lcom/dropbox/android/activity/dE;

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->e:Landroid/view/View;

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->h:Lcom/dropbox/android/activity/dE;

    sget-object v4, Lcom/dropbox/android/activity/dE;->a:Lcom/dropbox/android/activity/dE;

    if-ne v3, v4, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0185

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0d0187

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 137
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 138
    return-void

    :cond_1
    move v0, v2

    .line 125
    goto :goto_0

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0188

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0d0189

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 173
    invoke-super {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->b()V

    .line 175
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 176
    return-void
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method protected final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/dropbox/android/provider/PhotosProvider;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->setHasOptionsMenu(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->p:Lcom/dropbox/android/util/as;

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->p:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->a(Landroid/os/Bundle;)V

    .line 71
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 93
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V

    .line 94
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0d028f

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 98
    const/high16 v4, 0x41880000    # 17.0f

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 99
    invoke-interface {p1, v3}, Lcom/actionbarsherlock/view/Menu;->add(Ljava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/actionbarsherlock/view/MenuItem;->setActionView(Landroid/view/View;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v7}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 100
    const v2, 0x7f0d0290

    invoke-interface {p1, v1, v1, v1, v2}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020132

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_0

    :goto_0
    invoke-interface {v2, v0}, Lcom/actionbarsherlock/view/MenuItem;->setEnabled(Z)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 101
    return-void

    :cond_0
    move v0, v1

    .line 100
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 82
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 85
    const v1, 0x7f0700ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 86
    const v1, 0x7f07013f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 88
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onDestroy()V

    .line 76
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->p:Lcom/dropbox/android/util/as;

    invoke-virtual {v0}, Lcom/dropbox/android/util/as;->a()V

    .line 77
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 105
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 119
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 109
    :pswitch_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ALBUM_TO_ADD_TO_EXTRA"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    .line 110
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->a(Ljava/util/Collection;)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 112
    if-nez v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/util/DropboxPath;

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->a:Lcom/dropbox/android/albums/o;

    new-instance v3, Lcom/dropbox/android/albums/h;

    iget-object v4, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/dropbox/android/albums/h;-><init>(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 117
    const/4 v0, 0x1

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;->p:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->b(Landroid/os/Bundle;)V

    .line 159
    return-void
.end method
