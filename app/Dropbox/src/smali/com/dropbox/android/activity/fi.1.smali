.class public final Lcom/dropbox/android/activity/fi;
.super Landroid/app/DialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/TextEditActivity;)V
    .locals 3

    .prologue
    .line 137
    new-instance v0, Lcom/dropbox/android/activity/fi;

    invoke-direct {v0}, Lcom/dropbox/android/activity/fi;-><init>()V

    .line 138
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/fi;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 139
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/activity/fi;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/TextEditActivity;

    .line 145
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 146
    const v2, 0x7f0d01f8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 147
    const v2, 0x7f0d01f9

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 149
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 151
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 152
    const v3, 0x7f0d01fa

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 153
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setLines(I)V

    .line 154
    invoke-virtual {v2}, Landroid/widget/EditText;->setSingleLine()V

    .line 156
    new-instance v3, Lcom/dropbox/android/activity/fj;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/fj;-><init>(Lcom/dropbox/android/activity/fi;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 177
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 179
    new-instance v3, Lcom/dropbox/android/activity/fk;

    invoke-direct {v3, p0, v2, v0}, Lcom/dropbox/android/activity/fk;-><init>(Lcom/dropbox/android/activity/fi;Landroid/widget/EditText;Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 224
    const v0, 0x7f0d0014

    invoke-virtual {v1, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 225
    const v0, 0x7f0d0013

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 227
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
