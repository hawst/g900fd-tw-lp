.class public abstract Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TargetFrag:",
        "Landroid/support/v4/app/Fragment;",
        ">",
        "Lcom/dropbox/android/activity/base/BaseDialogFragment;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/support/v4/app/Fragment;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTargetFrag;)V"
        }
    .end annotation
.end method

.method protected final a(Landroid/support/v4/app/Fragment;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTargetFrag;II)V"
        }
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 45
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 46
    const-string v1, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 47
    const-string v1, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method protected final a(Landroid/support/v4/app/Fragment;Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTargetFrag;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 63
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    const-string v1, "ARG_BODY_STRING"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v1, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-void
.end method

.method protected final a(Landroid/support/v4/app/Fragment;Ljava/lang/String;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTargetFrag;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 54
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    const-string v1, "ARG_TITLE_STRING"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v1, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    const-string v1, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 81
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    const-string v2, "ARG_TITLE_MSG_RES_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 84
    const-string v2, "ARG_TITLE_MSG_RES_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 89
    :cond_0
    :goto_0
    const-string v2, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 90
    const-string v2, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 95
    :cond_1
    :goto_1
    const-string v2, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    new-instance v2, Lcom/dropbox/android/activity/dialog/F;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dialog/F;-><init>(Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 107
    const v0, 0x7f0d0013

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 108
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 109
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 85
    :cond_2
    const-string v2, "ARG_TITLE_STRING"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    const-string v2, "ARG_TITLE_STRING"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 91
    :cond_3
    const-string v2, "ARG_BODY_STRING"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    const-string v2, "ARG_BODY_STRING"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
