.class final Lcom/dropbox/android/activity/dialog/r;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 266
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 267
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 269
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The dialog should not allow empty names."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_0
    iget-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 281
    :goto_0
    return-void

    .line 276
    :cond_1
    new-instance v0, Lcom/dropbox/android/activity/dialog/s;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-static {v2}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    iget-object v5, p0, Lcom/dropbox/android/activity/dialog/r;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-static {v5}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;

    move-result-object v5

    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/dialog/s;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Lcom/dropbox/android/service/a;)V

    .line 278
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/s;->f()V

    .line 279
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/s;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
