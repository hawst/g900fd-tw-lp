.class final Lcom/dropbox/android/activity/delegate/h;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/dropbox/android/activity/delegate/h;->a:Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/delegate/h;->a:Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/dropbox/android/activity/delegate/h;->a:Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    invoke-static {v1, v0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;Ljava/lang/String;)Lcom/dropbox/android/activity/delegate/j;

    move-result-object v1

    .line 55
    iget-object v0, v1, Lcom/dropbox/android/activity/delegate/j;->a:Lcom/dropbox/android/activity/delegate/k;

    sget-object v2, Lcom/dropbox/android/activity/delegate/k;->a:Lcom/dropbox/android/activity/delegate/k;

    if-ne v0, v2, :cond_0

    .line 56
    iget-object v0, p0, Lcom/dropbox/android/activity/delegate/h;->a:Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->b(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/delegate/l;

    iget-object v1, v1, Lcom/dropbox/android/activity/delegate/j;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/delegate/l;->d(Landroid/net/Uri;)V

    .line 62
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, v1, Lcom/dropbox/android/activity/delegate/j;->a:Lcom/dropbox/android/activity/delegate/k;

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->b:Lcom/dropbox/android/activity/delegate/k;

    if-ne v0, v1, :cond_1

    .line 58
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d00b4

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->b(I)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d00b6

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->b(I)V

    goto :goto_0
.end method
