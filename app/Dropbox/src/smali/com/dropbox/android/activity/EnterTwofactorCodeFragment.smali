.class public Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/aE;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->c()V

    return-void
.end method

.method public static b()Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;-><init>()V

    .line 44
    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 145
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 146
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aE;

    .line 147
    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 154
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 156
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d0236

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    goto :goto_0

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aE;

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/aE;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/aE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    const-class v0, Lcom/dropbox/android/activity/aE;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aE;

    invoke-interface {v0}, Lcom/dropbox/android/activity/aE;->f()Z

    move-result v1

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aE;

    const v2, 0x7f03002d

    const v3, 0x7f0300a6

    invoke-interface {v0, p1, p2, v2, v3}, Lcom/dropbox/android/activity/aE;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v2

    .line 69
    const v0, 0x7f07009a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->c:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/a;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const v0, 0x7f07009b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    .line 73
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    new-instance v3, Lcom/dropbox/android/activity/aA;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/aA;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 84
    if-nez p3, :cond_0

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 88
    :cond_0
    if-eqz v1, :cond_2

    const v0, 0x7f0701a6

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 90
    new-instance v3, Lcom/dropbox/android/activity/aB;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/aB;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f07009d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 98
    if-eqz v1, :cond_1

    .line 100
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 104
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/k;->m()Ldbxyzptlk/db231222/n/u;

    move-result-object v1

    .line 111
    if-nez v1, :cond_3

    .line 112
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    :goto_1
    return-object v2

    .line 88
    :cond_2
    const v0, 0x7f07009c

    goto :goto_0

    .line 113
    :cond_3
    sget-object v3, Ldbxyzptlk/db231222/n/t;->b:Ldbxyzptlk/db231222/n/t;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/s;->e()Ldbxyzptlk/db231222/n/t;

    move-result-object v1

    invoke-virtual {v3, v1}, Ldbxyzptlk/db231222/n/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 115
    const v1, 0x7f0d023e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 116
    new-instance v1, Lcom/dropbox/android/activity/aC;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/aC;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 126
    :cond_4
    const v1, 0x7f0d0235

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 127
    new-instance v1, Lcom/dropbox/android/activity/aD;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/aD;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onResume()V

    .line 55
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aE;

    const v1, 0x7f0d0231

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/aE;->a(I)V

    .line 56
    return-void
.end method
