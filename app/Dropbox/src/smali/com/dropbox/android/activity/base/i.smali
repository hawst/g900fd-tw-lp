.class final Lcom/dropbox/android/activity/base/i;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:I

.field private b:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    .line 76
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/activity/base/i;->b:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/activity/base/e;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    .line 83
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    if-nez v0, :cond_0

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/activity/base/i;->b:J

    .line 87
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cq()V

    .line 89
    :cond_0
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 96
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    .line 99
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 100
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cq()V

    .line 101
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aR()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 103
    :cond_0
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    return v0
.end method

.method public final c()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 107
    iget-wide v2, p0, Lcom/dropbox/android/activity/base/i;->b:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 112
    :goto_0
    return-wide v0

    .line 111
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 112
    iget-wide v2, p0, Lcom/dropbox/android/activity/base/i;->b:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/dropbox/android/activity/base/i;->a:I

    return v0
.end method
