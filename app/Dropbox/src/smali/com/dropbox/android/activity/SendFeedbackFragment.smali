.class public Lcom/dropbox/android/activity/SendFeedbackFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/activity/eD;

.field private b:Z

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 208
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/activity/eD;)Lcom/dropbox/android/activity/SendFeedbackFragment;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    const-string v1, "ARG_FEEDBACK_TYPE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/eD;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v1, Lcom/dropbox/android/activity/SendFeedbackFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/SendFeedbackFragment;-><init>()V

    .line 74
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    return-object v1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/SendFeedbackFragment;Z)Z
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->c:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/SendFeedbackFragment;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/SendFeedbackFragment;)Lcom/dropbox/android/activity/eD;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->a:Lcom/dropbox/android/activity/eD;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/activity/SendFeedbackFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->g:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 80
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/SendFeedbackFragment;->setHasOptionsMenu(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_FEEDBACK_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/eD;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/eD;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->a:Lcom/dropbox/android/activity/eD;

    .line 84
    if-nez p1, :cond_0

    .line 85
    iput-boolean v2, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string v0, "SIS_KEY_INCLUDE_SCREENSHOT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->f:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 180
    :goto_0
    const v2, 0x7f0d01b5

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/SendFeedbackFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v1, v1, v2}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v1

    const v2, 0x7f02026f

    invoke-interface {v1, v2}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/actionbarsherlock/view/MenuItem;->setEnabled(Z)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v1

    .line 183
    invoke-interface {v1}, Lcom/actionbarsherlock/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v0, :cond_1

    const/16 v0, 0xff

    :goto_1
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 184
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 185
    return-void

    :cond_0
    move v0, v1

    .line 178
    goto :goto_0

    .line 183
    :cond_1
    const/16 v0, 0x80

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 93
    const v0, 0x7f030090

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 97
    sget-object v0, Lcom/dropbox/android/activity/eC;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->a:Lcom/dropbox/android/activity/eD;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/eD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 115
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid feedback type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->a:Lcom/dropbox/android/activity/eD;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/eD;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :pswitch_0
    const v1, 0x7f0d01aa

    .line 100
    const v0, 0x7f0d01ae

    move v2, v1

    move v1, v0

    .line 117
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/actionbarsherlock/app/SherlockFragmentActivity;

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/SherlockFragmentActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 118
    const v0, 0x7f07017c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 121
    const v0, 0x7f070180

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->c:Landroid/widget/LinearLayout;

    .line 122
    const v0, 0x7f070182

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->d:Landroid/widget/ImageView;

    .line 123
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->b(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->e:Landroid/graphics/Bitmap;

    .line 124
    iget-boolean v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 131
    :goto_1
    const v0, 0x7f070181

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 132
    new-instance v1, Lcom/dropbox/android/activity/ez;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ez;-><init>(Lcom/dropbox/android/activity/SendFeedbackFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    const v0, 0x7f07017d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->f:Landroid/widget/EditText;

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->f:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/eA;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/eA;-><init>(Lcom/dropbox/android/activity/SendFeedbackFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 162
    const v0, 0x7f07017e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->g:Landroid/view/View;

    .line 163
    const v0, 0x7f07017f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->h:Landroid/widget/TextView;

    .line 165
    return-object v3

    .line 103
    :pswitch_1
    const v1, 0x7f0d01ab

    .line 104
    const v0, 0x7f0d01af

    move v2, v1

    move v1, v0

    .line 105
    goto/16 :goto_0

    .line 107
    :pswitch_2
    const v1, 0x7f0d01ac

    .line 108
    const v0, 0x7f0d01b0

    move v2, v1

    move v1, v0

    .line 109
    goto/16 :goto_0

    .line 111
    :pswitch_3
    const v1, 0x7f0d01ad

    .line 112
    const v0, 0x7f0d01b1

    move v2, v1

    move v1, v0

    .line 113
    goto/16 :goto_0

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 128
    iput-boolean v4, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    goto :goto_1

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 189
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 191
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->h:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 192
    invoke-interface {p1, v4}, Lcom/actionbarsherlock/view/MenuItem;->setEnabled(Z)Lcom/actionbarsherlock/view/MenuItem;

    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    new-instance v1, Lcom/dropbox/android/activity/eE;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/dropbox/android/activity/eE;-><init>(Lcom/dropbox/android/activity/SendFeedbackFragment;Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v1}, Lcom/dropbox/android/activity/eE;->f()V

    .line 198
    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/eE;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 199
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->be()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "feedback_type"

    iget-object v2, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->a:Lcom/dropbox/android/activity/eD;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/eD;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 202
    const/4 v0, 0x1

    .line 204
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 171
    const-string v0, "SIS_KEY_INCLUDE_SCREENSHOT"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/SendFeedbackFragment;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 172
    return-void
.end method
