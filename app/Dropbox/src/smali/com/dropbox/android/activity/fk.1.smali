.class final Lcom/dropbox/android/activity/fk;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/dropbox/android/activity/TextEditActivity;

.field final synthetic c:Lcom/dropbox/android/activity/fi;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/fi;Landroid/widget/EditText;Lcom/dropbox/android/activity/TextEditActivity;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/dropbox/android/activity/fk;->c:Lcom/dropbox/android/activity/fi;

    iput-object p2, p0, Lcom/dropbox/android/activity/fk;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 182
    iget-object v0, p0, Lcom/dropbox/android/activity/fk;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    const v1, 0x7f0d01fa

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    :cond_0
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    :cond_1
    invoke-static {v0}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 191
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    const/16 v2, 0x2b

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 195
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v3}, Lcom/dropbox/android/activity/TextEditActivity;->b(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 196
    iget-object v2, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 197
    iget-object v2, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/TextEditActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    .line 198
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    .line 199
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v2

    .line 200
    invoke-virtual {v2}, Ldbxyzptlk/db231222/k/h;->j()Ljava/io/File;

    move-result-object v4

    .line 201
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 203
    iget-object v4, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v4, v2, v1}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v4

    .line 204
    new-instance v5, Ldbxyzptlk/db231222/k/f;

    invoke-direct {v5, v2, v4}, Ldbxyzptlk/db231222/k/f;-><init>(Ldbxyzptlk/db231222/k/h;Ljava/io/File;)V

    invoke-virtual {v5}, Ldbxyzptlk/db231222/k/f;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v1, v5}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;Z)V

    .line 208
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->j()Ldbxyzptlk/db231222/k/c;

    move-result-object v2

    invoke-virtual {v2, v4}, Ldbxyzptlk/db231222/k/c;->b(Ljava/io/File;)V

    .line 211
    :cond_2
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bf()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 212
    iget-object v2, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v2, v1}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;)Landroid/net/Uri;

    .line 213
    iget-object v2, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v2, v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->c(Lcom/dropbox/android/activity/TextEditActivity;)Lcom/dropbox/android/util/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/e;->a(Landroid/net/Uri;)V

    .line 215
    iget-object v0, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/TextEditActivity;->d(Lcom/dropbox/android/activity/TextEditActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->e(Lcom/dropbox/android/activity/TextEditActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    iget-object v0, p0, Lcom/dropbox/android/activity/fk;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 221
    :cond_3
    return-void

    .line 193
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method
