.class final Lcom/dropbox/android/activity/payment/q;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/l/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/l/i",
        "<",
        "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
        "Ldbxyzptlk/db231222/z/aB;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ldbxyzptlk/db231222/z/aB;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 148
    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/aB;->b()Ldbxyzptlk/db231222/z/ax;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/z/ax;->b:Ldbxyzptlk/db231222/z/ax;

    if-ne v0, v1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02ec

    const v2, 0x7f0d02ed

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;II)V

    .line 170
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/aB;->c()Ldbxyzptlk/db231222/z/aD;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/z/aD;->b:Ldbxyzptlk/db231222/z/aD;

    if-ne v0, v1, :cond_1

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02f0

    const v2, 0x7f0d02f1

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/aB;->a()Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0, v1, v2, v6, v3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;IIZ[Ljava/lang/Object;)V

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/aB;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0, v5}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Z)Z

    .line 159
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/y;

    invoke-interface {v0, v5}, Lcom/dropbox/android/activity/payment/y;->b(Z)V

    goto :goto_0

    .line 163
    :cond_1
    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/aB;->c()Ldbxyzptlk/db231222/z/aD;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/z/aD;->c:Ldbxyzptlk/db231222/z/aD;

    if-ne v0, v1, :cond_2

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02f2

    const v2, 0x7f0d02f3

    invoke-static {v0, v1, v2, v5}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;IIZ)V

    goto :goto_0

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->d(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 174
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bB()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 175
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/q;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02ec

    const v2, 0x7f0d02ed

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;IIZ)V

    .line 177
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 145
    check-cast p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/payment/q;->a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ljava/lang/Void;)V

    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 145
    check-cast p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    check-cast p2, Ldbxyzptlk/db231222/z/aB;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/payment/q;->a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ldbxyzptlk/db231222/z/aB;)V

    return-void
.end method
