.class public final Lcom/dropbox/android/activity/payment/g;
.super Lcom/dropbox/android/activity/az;
.source "panda.py"


# instance fields
.field final synthetic b:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

.field private d:Z

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;Lcom/dropbox/android/activity/DropboxWebViewActivity;)V
    .locals 1

    .prologue
    .line 247
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/g;->b:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    .line 248
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/az;-><init>(Lcom/dropbox/android/activity/DropboxWebViewActivity;Landroid/app/Activity;)V

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/g;->d:Z

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/g;->e:Ljava/lang/String;

    .line 249
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 298
    new-instance v0, Ldbxyzptlk/db231222/aj/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/aj/c;-><init>()V

    .line 299
    const-string v1, "num"

    iget-object v2, p0, Lcom/dropbox/android/activity/payment/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/aj/c;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/payment/g;->b:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/aj/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/g;->b:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->c(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 303
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 283
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/g;->e:Ljava/lang/String;

    .line 284
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/g;->d:Z

    if-eqz v0, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/g;->a()V

    .line 288
    :cond_0
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 269
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/az;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/g;->d:Z

    .line 271
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/g;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 273
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/g;->a()V

    .line 275
    :cond_0
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/g;->d:Z

    .line 280
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 256
    if-eqz p2, :cond_0

    const-string v1, "/endUpdateWebViewActivity"

    invoke-virtual {p2, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 259
    const-string v2, "EXTRA_PAYMENT_SELECTOR_COMPLETED_UPGRADE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 260
    iget-object v2, p0, Lcom/dropbox/android/activity/payment/g;->c:Landroid/app/Activity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 261
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/g;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 264
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/az;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
