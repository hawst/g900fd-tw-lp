.class public abstract Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TargetActivity:",
        "Landroid/app/Activity;",
        ">",
        "Lcom/dropbox/android/activity/base/BaseDialogFragment;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(II)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    const-string v1, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    const-string v1, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->setArguments(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method public abstract a(Landroid/app/Activity;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTargetActivity;)V"
        }
    .end annotation
.end method

.method protected final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    const-string v1, "ARG_BODY_STRING"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v1, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_TITLE_MSG_RES_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_TITLE_MSG_RES_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_BODY_MSG_RES_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 72
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_CONFIRM_BUTTON_RES_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Lcom/dropbox/android/activity/dialog/G;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dialog/G;-><init>(Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 83
    const v1, 0x7f0d0013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 85
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 68
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_BODY_STRING"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_BODY_STRING"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
