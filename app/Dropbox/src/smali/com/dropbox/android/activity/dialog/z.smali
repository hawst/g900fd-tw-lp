.class final Lcom/dropbox/android/activity/dialog/z;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/dropbox/android/widget/bB;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/pm/PackageManager;

.field final synthetic b:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

.field private final c:Landroid/content/pm/ResolveInfo$DisplayNameComparator;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;Landroid/content/pm/PackageManager;)V
    .locals 2

    .prologue
    .line 518
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/z;->b:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    iput-object p2, p0, Lcom/dropbox/android/activity/dialog/z;->a:Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 519
    new-instance v0, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/z;->a:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/z;->c:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    return-void
.end method

.method private a(Lcom/dropbox/android/widget/bB;)J
    .locals 3

    .prologue
    .line 523
    iget-object v0, p1, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 524
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/z;->b:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/A;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/A;->a(Landroid/content/ComponentName;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/widget/bB;Lcom/dropbox/android/widget/bB;)I
    .locals 5

    .prologue
    .line 531
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/z;->a(Lcom/dropbox/android/widget/bB;)J

    move-result-wide v0

    .line 532
    invoke-direct {p0, p2}, Lcom/dropbox/android/activity/dialog/z;->a(Lcom/dropbox/android/widget/bB;)J

    move-result-wide v2

    .line 533
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 534
    const/4 v0, -0x1

    .line 538
    :goto_0
    return v0

    .line 535
    :cond_0
    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 536
    const/4 v0, 0x1

    goto :goto_0

    .line 538
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/z;->c:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    iget-object v1, p1, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    iget-object v2, p2, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 518
    check-cast p1, Lcom/dropbox/android/widget/bB;

    check-cast p2, Lcom/dropbox/android/widget/bB;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/dialog/z;->a(Lcom/dropbox/android/widget/bB;Lcom/dropbox/android/widget/bB;)I

    move-result v0

    return v0
.end method
