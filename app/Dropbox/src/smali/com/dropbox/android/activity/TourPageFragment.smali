.class public Lcom/dropbox/android/activity/TourPageFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# instance fields
.field protected a:Landroid/widget/TextView;

.field protected b:Landroid/widget/TextView;

.field protected c:Landroid/widget/ImageView;

.field protected d:Landroid/widget/Button;

.field protected e:Landroid/view/View;

.field protected f:Landroid/view/ViewStub;

.field protected g:Lcom/dropbox/android/activity/fy;

.field protected h:Landroid/view/View;

.field protected i:Landroid/view/View;

.field protected j:Landroid/widget/Button;

.field protected k:Landroid/widget/Button;

.field protected l:Landroid/view/View;

.field protected m:Lcom/dropbox/android/activity/fz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->m:Lcom/dropbox/android/activity/fz;

    .line 139
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/fn;I)Lcom/dropbox/android/activity/TourPageFragment;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/dropbox/android/activity/TourPageFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/TourPageFragment;-><init>()V

    .line 64
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 65
    const-string v2, "ARG_PAGE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/fn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v2, "ARG_INDEX"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TourPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 69
    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onAttach(Landroid/app/Activity;)V

    .line 48
    check-cast p1, Lcom/dropbox/android/activity/fz;

    iput-object p1, p0, Lcom/dropbox/android/activity/TourPageFragment;->m:Lcom/dropbox/android/activity/fz;

    .line 49
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    .line 88
    if-nez v2, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 130
    :goto_0
    return-object v0

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_1

    const-string v1, "ARG_PAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ARG_INDEX"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 93
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TourPageFragment expects an arg with the page to show and an arg with that page\'s index in the tour."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_2
    const-string v1, "ARG_PAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/fn;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/fn;

    move-result-object v3

    .line 98
    sget-object v0, Lcom/dropbox/android/activity/fn;->c:Lcom/dropbox/android/activity/fn;

    if-ne v3, v0, :cond_3

    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    const v0, 0x7f0300a2

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 100
    iput-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->l:Landroid/view/View;

    .line 101
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v4, 0x7f0d016a

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 102
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/BaseActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 103
    const v4, 0x7f020284

    invoke-virtual {v0, v4}, Lcom/actionbarsherlock/app/ActionBar;->setIcon(I)V

    move-object v0, v1

    .line 127
    :goto_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->a()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v4, "page"

    invoke-virtual {v3}, Lcom/dropbox/android/activity/fn;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 128
    invoke-virtual {v3, p0, v2, p3}, Lcom/dropbox/android/activity/fn;->a(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/r/d;Landroid/os/Bundle;)Lcom/dropbox/android/activity/fy;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->g:Lcom/dropbox/android/activity/fy;

    goto :goto_0

    .line 105
    :cond_3
    const v0, 0x7f030040

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 106
    const v0, 0x7f0700ca

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->a:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0700cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->b:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0700c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->c:Landroid/widget/ImageView;

    .line 109
    const v0, 0x7f0700c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->d:Landroid/widget/Button;

    .line 110
    const v0, 0x7f0700c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/view/View;

    .line 111
    const v0, 0x7f0700c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/ViewStub;

    .line 112
    const v0, 0x7f0700c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->h:Landroid/view/View;

    .line 113
    const v0, 0x7f070064

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->i:Landroid/view/View;

    .line 114
    const v0, 0x7f07004d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->j:Landroid/widget/Button;

    .line 115
    const v0, 0x7f07004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->k:Landroid/widget/Button;

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->d:Landroid/widget/Button;

    new-instance v4, Lcom/dropbox/android/activity/fm;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/fm;-><init>(Lcom/dropbox/android/activity/TourPageFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    goto/16 :goto_1
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDetach()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->m:Lcom/dropbox/android/activity/fz;

    .line 55
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->g:Lcom/dropbox/android/activity/fy;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->g:Lcom/dropbox/android/activity/fy;

    invoke-interface {v0, p1}, Lcom/dropbox/android/activity/fy;->a(Landroid/os/Bundle;)V

    .line 82
    :cond_0
    return-void
.end method
