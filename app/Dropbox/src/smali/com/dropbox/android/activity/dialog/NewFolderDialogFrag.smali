.class public Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dialog/p;


# instance fields
.field a:Ldbxyzptlk/db231222/g/N;

.field private b:Landroid/text/TextWatcher;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Landroid/text/TextWatcher;

    .line 61
    new-instance v0, Lcom/dropbox/android/activity/dialog/h;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/h;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ldbxyzptlk/db231222/g/N;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Ldbxyzptlk/db231222/g/O;)I
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Ldbxyzptlk/db231222/g/O;)I

    move-result v0

    return v0
.end method

.method private a(Ldbxyzptlk/db231222/g/O;)I
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/dropbox/android/activity/dialog/k;->a:[I

    invoke-virtual {p1}, Ldbxyzptlk/db231222/g/O;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    const v0, 0x7f0d00b5

    :goto_0
    return v0

    .line 47
    :pswitch_0
    const v0, 0x7f0d00b4

    goto :goto_0

    .line 49
    :pswitch_1
    const v0, 0x7f0d00b3

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    const-string v1, "ARG_PARENT_DIR"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 39
    new-instance v1, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-direct {v1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;-><init>()V

    .line 40
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 217
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 218
    const v1, 0x7f070115

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 219
    const v2, 0x7f070116

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 221
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .line 222
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 223
    const/4 v3, -0x2

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .line 224
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 225
    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 226
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 228
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 231
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 232
    const v1, 0x7f070115

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 233
    const v2, 0x7f070116

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 235
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .line 236
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 237
    const/4 v3, -0x2

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .line 238
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 239
    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 240
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/bn;->a(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 241
    const v1, 0x7f0d00b2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 242
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 244
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 124
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 126
    const v1, 0x7f0d00b0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 127
    const v1, 0x7f0d00b1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    const v1, 0x7f0d0013

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005e

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 131
    if-nez p1, :cond_0

    .line 132
    iput-boolean v4, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Z

    .line 137
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 134
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Z

    goto :goto_0
.end method

.method public onStart()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, -0x1

    .line 142
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onStart()V

    .line 144
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_PARENT_DIR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    .line 153
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 155
    const v2, 0x7f070114

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 156
    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Landroid/text/TextWatcher;

    if-eqz v3, :cond_0

    .line 157
    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 158
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Landroid/text/TextWatcher;

    .line 161
    :cond_0
    const v3, 0x7f070115

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    .line 162
    iget-boolean v4, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Z

    if-eqz v4, :cond_1

    .line 163
    invoke-virtual {v3, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 166
    :cond_1
    const v3, 0x7f070116

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 167
    iget-boolean v4, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Z

    if-eqz v4, :cond_2

    .line 168
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    :cond_2
    new-instance v4, Lcom/dropbox/android/activity/dialog/i;

    invoke-direct {v4, p0, v2, v0}, Lcom/dropbox/android/activity/dialog/i;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Landroid/widget/EditText;Lcom/dropbox/android/util/DropboxPath;)V

    .line 187
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 190
    new-instance v0, Lcom/dropbox/android/activity/dialog/j;

    invoke-direct {v0, p0, v1, v3}, Lcom/dropbox/android/activity/dialog/j;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Landroid/app/AlertDialog;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Landroid/text/TextWatcher;

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    return-void

    .line 189
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 59
    return-void
.end method
