.class final Lcom/dropbox/android/activity/aV;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/dropbox/android/activity/aV;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 614
    iget-object v0, p0, Lcom/dropbox/android/activity/aV;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    if-nez v0, :cond_0

    .line 627
    :goto_0
    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/aV;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    .line 618
    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 620
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/activity/aV;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/LocalEntry;Z)V

    .line 622
    iget-object v2, p0, Lcom/dropbox/android/activity/aV;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/GalleryActivity;->f(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setSelected(Z)V

    .line 623
    invoke-virtual {v1, v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 624
    :catch_0
    move-exception v0

    .line 625
    iget-object v0, p0, Lcom/dropbox/android/activity/aV;->a:Lcom/dropbox/android/activity/GalleryActivity;

    const v1, 0x7f0d00d8

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 618
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
