.class public Lcom/dropbox/android/activity/LoginOrNewAcctFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/cI;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginOrNewAcctFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public static b()Lcom/dropbox/android/activity/LoginOrNewAcctFragment;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;-><init>()V

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LoginOrNewAcctFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/LoginOrNewAcctFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->c()Lcom/dropbox/android/service/J;

    move-result-object v0

    .line 94
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bY()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 95
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/cI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    const-class v0, Lcom/dropbox/android/activity/cI;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->c()V

    .line 53
    const v0, 0x7f0300bf

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 55
    const v0, 0x7f0701b8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/dropbox/android/util/E;->b()Lcom/dropbox/android/util/J;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/dropbox/android/util/J;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const v0, 0x7f0701ba

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/dropbox/android/activity/cF;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cF;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v0, 0x7f0701b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/dropbox/android/activity/cG;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cG;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    const v0, 0x7f0701bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    new-instance v2, Lcom/dropbox/android/activity/cH;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cH;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 84
    return-object v1
.end method
