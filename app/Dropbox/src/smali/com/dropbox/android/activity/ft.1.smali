.class final Lcom/dropbox/android/activity/ft;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CompoundButton;

.field final synthetic b:Ldbxyzptlk/db231222/r/d;

.field final synthetic c:Lcom/dropbox/android/activity/TourPageWithThumbsFragment;


# direct methods
.method constructor <init>(Landroid/widget/CompoundButton;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/activity/TourPageWithThumbsFragment;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/dropbox/android/activity/ft;->a:Landroid/widget/CompoundButton;

    iput-object p2, p0, Lcom/dropbox/android/activity/ft;->b:Ldbxyzptlk/db231222/r/d;

    iput-object p3, p0, Lcom/dropbox/android/activity/ft;->c:Lcom/dropbox/android/activity/TourPageWithThumbsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 224
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->m()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 227
    iget-object v0, p0, Lcom/dropbox/android/activity/ft;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    .line 229
    iget-object v1, p0, Lcom/dropbox/android/activity/ft;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 230
    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/activity/ft;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, Ldbxyzptlk/db231222/n/P;->a(ZZZLcom/dropbox/android/service/i;)V

    .line 231
    iget-object v0, p0, Lcom/dropbox/android/activity/ft;->c:Lcom/dropbox/android/activity/TourPageWithThumbsFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->m:Lcom/dropbox/android/activity/fz;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/activity/ft;->c:Lcom/dropbox/android/activity/TourPageWithThumbsFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->m:Lcom/dropbox/android/activity/fz;

    iget-object v1, p0, Lcom/dropbox/android/activity/ft;->c:Lcom/dropbox/android/activity/TourPageWithThumbsFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_INDEX"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/fz;->a(I)V

    .line 234
    :cond_0
    return-void
.end method
