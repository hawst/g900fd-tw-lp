.class public Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;
.super Lcom/dropbox/android/activity/dialog/PhotoBatchDeleteConfirmDialogFrag;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/dialog/PhotoBatchDeleteConfirmDialogFrag",
        "<",
        "Lcom/dropbox/android/activity/PhotoGridFragment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/PhotoBatchDeleteConfirmDialogFrag;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/PhotoGridFragment;IIZ)Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;
    .locals 6

    .prologue
    .line 467
    new-instance v0, Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;-><init>()V

    .line 468
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;->a(Landroid/content/res/Resources;Landroid/support/v4/app/Fragment;IIZ)V

    .line 469
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 464
    check-cast p1, Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;->a(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 5

    .prologue
    .line 474
    invoke-static {p1}, Lcom/dropbox/android/activity/PhotoGridFragment;->f(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    .line 475
    invoke-virtual {p1}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 476
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v2

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    iget-object v3, p1, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {p1}, Lcom/dropbox/android/activity/PhotoGridFragment;->g(Lcom/dropbox/android/activity/PhotoGridFragment;)Lcom/dropbox/android/albums/u;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/taskqueue/D;Ldbxyzptlk/db231222/k/h;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Lcom/dropbox/android/activity/PhotoGridFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 479
    invoke-static {p1}, Lcom/dropbox/android/activity/PhotoGridFragment;->b(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    .line 480
    return-void
.end method
