.class final Lcom/dropbox/android/activity/dialog/y;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 373
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 376
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/B;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/dropbox/android/activity/dialog/B;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 377
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/A;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/A;->b(Landroid/content/ComponentName;)V

    .line 378
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->dismiss()V

    .line 379
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->d(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/y;->a:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-static {v3}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->c(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;->a(Landroid/content/Intent;Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;)V

    goto :goto_0
.end method
