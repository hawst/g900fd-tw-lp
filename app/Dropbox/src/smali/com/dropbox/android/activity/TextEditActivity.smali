.class public Lcom/dropbox/android/activity/TextEditActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/net/Uri;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ScrollView;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private final l:Lcom/dropbox/android/util/e;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/dropbox/android/activity/TextEditActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 78
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    .line 79
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    .line 80
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    .line 81
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Z

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Z

    .line 83
    new-instance v0, Lcom/dropbox/android/util/e;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/e;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Lcom/dropbox/android/util/e;

    .line 84
    iput-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    .line 85
    iput-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    .line 87
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->o:Ljava/lang/String;

    .line 89
    const-string v0, "\r\n"

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->p:Ljava/lang/String;

    .line 761
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    return-object p1
.end method

.method public static a(Ljava/io/InputStream;)Lcom/dropbox/android/activity/fh;
    .locals 17

    .prologue
    .line 545
    new-instance v8, Lcom/dropbox/android/activity/fh;

    invoke-direct {v8}, Lcom/dropbox/android/activity/fh;-><init>()V

    .line 548
    const/4 v6, 0x0

    .line 549
    const/4 v5, 0x0

    .line 551
    new-instance v9, Ldbxyzptlk/db231222/am/h;

    const/4 v1, 0x0

    invoke-direct {v9, v1}, Ldbxyzptlk/db231222/am/h;-><init>(I)V

    .line 554
    new-instance v1, Lcom/dropbox/android/activity/fc;

    invoke-direct {v1, v8}, Lcom/dropbox/android/activity/fc;-><init>(Lcom/dropbox/android/activity/fh;)V

    invoke-virtual {v9, v1}, Ldbxyzptlk/db231222/am/h;->a(Ldbxyzptlk/db231222/am/q;)V

    .line 564
    const/4 v4, 0x1

    .line 565
    const/4 v3, 0x0

    .line 567
    const/4 v1, 0x1

    .line 568
    const/4 v2, 0x0

    .line 570
    const/16 v7, 0x400

    new-array v10, v7, [B

    .line 572
    :cond_0
    :goto_0
    const/4 v7, 0x0

    array-length v11, v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v7, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v11

    const/4 v7, -0x1

    if-eq v11, v7, :cond_8

    if-eqz v3, :cond_1

    if-nez v6, :cond_8

    .line 574
    :cond_1
    const/4 v7, 0x0

    :goto_1
    if-nez v6, :cond_4

    if-ge v7, v11, :cond_4

    .line 575
    aget-byte v12, v10, v7

    packed-switch v12, :pswitch_data_0

    .line 588
    :pswitch_0
    if-eqz v5, :cond_2

    .line 589
    const-string v6, "\r"

    iput-object v6, v8, Lcom/dropbox/android/activity/fh;->b:Ljava/lang/String;

    .line 590
    const/4 v6, 0x1

    .line 574
    :cond_2
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 577
    :pswitch_1
    if-eqz v5, :cond_3

    .line 578
    const-string v6, "\r\n"

    iput-object v6, v8, Lcom/dropbox/android/activity/fh;->b:Ljava/lang/String;

    .line 582
    :goto_3
    const/4 v6, 0x1

    .line 583
    goto :goto_2

    .line 580
    :cond_3
    const-string v6, "\n"

    iput-object v6, v8, Lcom/dropbox/android/activity/fh;->b:Ljava/lang/String;

    goto :goto_3

    .line 585
    :pswitch_2
    const/4 v5, 0x1

    .line 586
    goto :goto_2

    .line 597
    :cond_4
    if-eqz v1, :cond_5

    .line 599
    const/4 v1, 0x2

    new-array v12, v1, [Ldbxyzptlk/db231222/X/a;

    const/4 v1, 0x0

    sget-object v7, Ldbxyzptlk/db231222/X/a;->b:Ldbxyzptlk/db231222/X/a;

    aput-object v7, v12, v1

    const/4 v1, 0x1

    sget-object v7, Ldbxyzptlk/db231222/X/a;->c:Ldbxyzptlk/db231222/X/a;

    aput-object v7, v12, v1

    .line 600
    array-length v13, v12

    const/4 v1, 0x0

    move v7, v1

    :goto_4
    if-ge v7, v13, :cond_d

    aget-object v1, v12, v7

    .line 601
    invoke-virtual {v1}, Ldbxyzptlk/db231222/X/a;->c()[B

    move-result-object v14

    .line 602
    array-length v15, v14

    if-lt v11, v15, :cond_7

    invoke-static {v10, v14}, Lcom/dropbox/android/activity/TextEditActivity;->a([B[B)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 607
    :goto_5
    const/4 v2, 0x0

    move-object/from16 v16, v1

    move v1, v2

    move-object/from16 v2, v16

    .line 611
    :cond_5
    if-eqz v4, :cond_6

    .line 612
    invoke-virtual {v9, v10, v11}, Ldbxyzptlk/db231222/am/h;->a([BI)Z

    move-result v4

    .line 616
    :cond_6
    if-nez v4, :cond_0

    if-nez v3, :cond_0

    .line 617
    const/4 v3, 0x0

    invoke-virtual {v9, v10, v11, v3}, Ldbxyzptlk/db231222/am/h;->a([BIZ)Z

    move-result v3

    goto :goto_0

    .line 600
    :cond_7
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_4

    .line 620
    :cond_8
    invoke-virtual {v9}, Ldbxyzptlk/db231222/am/h;->b()V

    .line 625
    invoke-virtual {v9}, Ldbxyzptlk/db231222/am/h;->c()[Ljava/lang/String;

    move-result-object v3

    .line 626
    if-eqz v2, :cond_9

    .line 627
    array-length v5, v3

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v5, :cond_9

    aget-object v6, v3, v1

    .line 628
    invoke-virtual {v2}, Ldbxyzptlk/db231222/X/a;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 629
    iput-object v6, v8, Lcom/dropbox/android/activity/fh;->a:Ljava/lang/String;

    .line 635
    :cond_9
    iget-object v1, v8, Lcom/dropbox/android/activity/fh;->a:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 636
    if-eqz v4, :cond_c

    .line 637
    sget-object v1, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    const-string v2, "CHARSET = ASCII"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const-string v1, "us-ascii"

    iput-object v1, v8, Lcom/dropbox/android/activity/fh;->a:Ljava/lang/String;

    .line 644
    :cond_a
    :goto_7
    return-object v8

    .line 627
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 640
    :cond_c
    const-string v1, "UTF-8"

    iput-object v1, v8, Lcom/dropbox/android/activity/fh;->a:Ljava/lang/String;

    goto :goto_7

    :cond_d
    move-object v1, v2

    goto :goto_5

    .line 575
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/activity/fl;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 357
    :try_start_0
    const-string v1, "rw"

    .line 358
    new-instance v0, Lcom/dropbox/android/activity/fl;

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/fl;-><init>(Landroid/os/ParcelFileDescriptor;Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 370
    :goto_0
    return-object v0

    .line 359
    :catch_0
    move-exception v0

    .line 362
    const-string v1, "r"

    .line 363
    new-instance v0, Lcom/dropbox/android/activity/fl;

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/fl;-><init>(Landroid/os/ParcelFileDescriptor;Z)V

    goto :goto_0

    .line 364
    :catch_1
    move-exception v0

    .line 369
    const-string v1, "r"

    .line 370
    new-instance v0, Lcom/dropbox/android/activity/fl;

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/fl;-><init>(Landroid/os/ParcelFileDescriptor;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->g()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 379
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Z

    if-eqz v0, :cond_0

    .line 380
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    .line 381
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    const v1, 0x20001

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setInputType(I)V

    .line 387
    :goto_0
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 390
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 391
    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 392
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 393
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 394
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 395
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const v4, 0x3e800

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 397
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    new-instance v0, Lcom/dropbox/android/activity/fb;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fb;-><init>(Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 416
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 417
    return-void

    .line 383
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    .line 384
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->e()V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 650
    .line 654
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 655
    if-nez p2, :cond_8

    .line 657
    :try_start_1
    const-string v2, "r"

    invoke-virtual {v4, p1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 660
    const v5, 0x7f0d01fe

    :try_start_2
    invoke-static {p0, v5}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 709
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Writer;)V

    .line 710
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    .line 712
    if-eqz v2, :cond_0

    .line 713
    :try_start_3
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 715
    :cond_0
    :goto_0
    return v0

    .line 662
    :catch_0
    move-exception v2

    move-object v2, v3

    .line 667
    :goto_1
    :try_start_4
    iget-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 668
    const-string v5, "UTF-8"

    iput-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    .line 676
    :cond_1
    :goto_2
    const-string v5, "rwt"

    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    .line 677
    :try_start_5
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->createOutputStream()Ljava/io/FileOutputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v4

    .line 678
    :try_start_6
    new-instance v2, Ljava/io/OutputStreamWriter;

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    invoke-direct {v2, v4, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 680
    :try_start_7
    iget-object v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 681
    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->o:Ljava/lang/String;

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->p:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 682
    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->o:Ljava/lang/String;

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->p:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 684
    :cond_2
    invoke-virtual {v2, v3}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 686
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 689
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_8
    .catch Ljava/io/SyncFailedException; {:try_start_8 .. :try_end_8} :catch_a
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 693
    :goto_3
    :try_start_9
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V

    .line 694
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 696
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->r()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 698
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    .line 699
    iget-boolean v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    if-eqz v3, :cond_3

    .line 700
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    .line 702
    :cond_3
    const v3, 0x7f0d01f2

    invoke-static {p0, v3}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 709
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Writer;)V

    .line 710
    invoke-static {v4}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    .line 712
    if-eqz v5, :cond_4

    .line 713
    :try_start_a
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    :cond_4
    :goto_4
    move v0, v1

    .line 715
    goto :goto_0

    .line 670
    :cond_5
    :try_start_b
    iget-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 671
    const-string v6, "ascii"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "us-ascii"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 672
    :cond_6
    const-string v5, "UTF-8"

    iput-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_2

    .line 704
    :catch_1
    move-exception v1

    move-object v4, v2

    move-object v2, v3

    .line 705
    :goto_5
    const v5, 0x7f0d01fd

    :try_start_c
    invoke-static {p0, v5}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 706
    sget-object v5, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    const-string v6, "Problem saving file"

    invoke-static {v5, v6, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 709
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Writer;)V

    .line 710
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    .line 712
    if-eqz v4, :cond_0

    .line 713
    :try_start_d
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2

    goto/16 :goto_0

    .line 715
    :catch_2
    move-exception v1

    goto/16 :goto_0

    .line 709
    :catchall_0
    move-exception v0

    move-object v4, v3

    move-object v5, v3

    :goto_6
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Writer;)V

    .line 710
    invoke-static {v4}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    .line 712
    if-eqz v5, :cond_7

    .line 713
    :try_start_e
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    .line 715
    :cond_7
    :goto_7
    throw v0

    :catch_3
    move-exception v1

    goto/16 :goto_0

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_7

    .line 709
    :catchall_1
    move-exception v0

    move-object v4, v3

    move-object v5, v2

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v4, v3

    goto :goto_6

    :catchall_3
    move-exception v0

    goto :goto_6

    :catchall_4
    move-exception v0

    move-object v3, v2

    goto :goto_6

    :catchall_5
    move-exception v0

    move-object v5, v4

    move-object v4, v2

    goto :goto_6

    .line 704
    :catch_6
    move-exception v1

    move-object v2, v3

    move-object v4, v3

    goto :goto_5

    :catch_7
    move-exception v1

    move-object v2, v3

    move-object v4, v5

    goto :goto_5

    :catch_8
    move-exception v1

    move-object v2, v4

    move-object v4, v5

    goto :goto_5

    :catch_9
    move-exception v1

    move-object v3, v2

    move-object v2, v4

    move-object v4, v5

    goto :goto_5

    .line 690
    :catch_a
    move-exception v3

    goto/16 :goto_3

    .line 662
    :catch_b
    move-exception v5

    goto/16 :goto_1

    :cond_8
    move-object v2, v3

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;Z)Z
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Z

    return p1
.end method

.method private static a([B[B)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 532
    array-length v0, p0

    array-length v2, p1

    if-lt v0, v2, :cond_0

    move v0, v1

    .line 533
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 534
    aget-byte v2, p0, v0

    aget-byte v3, p1, v0

    if-eq v2, v3, :cond_1

    .line 540
    :cond_0
    :goto_1
    return v1

    .line 533
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 538
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/TextEditActivity;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/activity/TextEditActivity;)Lcom/dropbox/android/util/e;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Lcom/dropbox/android/util/e;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/TextEditActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->e:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    .line 376
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/TextEditActivity;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Z

    return v0
.end method

.method private f()Ljava/lang/String;
    .locals 10

    .prologue
    const v9, 0x7f0d01fb

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 424
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 427
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v4, v2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/activity/fl;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v5

    .line 431
    :try_start_1
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v2, v5, Lcom/dropbox/android/activity/fl;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    :try_start_2
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v6, 0x2000

    invoke-direct {v2, v3, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 434
    :try_start_3
    invoke-static {v2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Ljava/io/InputStream;)Lcom/dropbox/android/activity/fh;

    move-result-object v6

    .line 435
    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    if-nez v7, :cond_0

    .line 436
    iget-object v7, v6, Lcom/dropbox/android/activity/fh;->a:Ljava/lang/String;

    iput-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    .line 438
    :cond_0
    iget-object v7, v6, Lcom/dropbox/android/activity/fh;->b:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 439
    iget-object v6, v6, Lcom/dropbox/android/activity/fh;->b:Ljava/lang/String;

    iput-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->p:Ljava/lang/String;

    .line 441
    :cond_1
    iget-boolean v6, v5, Lcom/dropbox/android/activity/fl;->b:Z

    iput-boolean v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 443
    :try_start_4
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 444
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 445
    if-eqz v5, :cond_2

    iget-object v2, v5, Lcom/dropbox/android/activity/fl;->a:Landroid/os/ParcelFileDescriptor;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    if-eqz v2, :cond_2

    .line 447
    :try_start_5
    iget-object v2, v5, Lcom/dropbox/android/activity/fl;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Lcom/dropbox/android/activity/fg; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 457
    :cond_2
    :goto_0
    :try_start_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 459
    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    const-string v3, "r"

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-result-object v4

    .line 463
    :try_start_7
    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 464
    :try_start_8
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    invoke-direct {v2, v3, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 466
    const/16 v6, 0x400

    :try_start_9
    new-array v6, v6, [C

    .line 470
    :goto_1
    invoke-virtual {v2, v6}, Ljava/io/Reader;->read([C)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v7

    .line 471
    if-gez v7, :cond_6

    .line 481
    :try_start_a
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    .line 482
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    .line 483
    if-eqz v4, :cond_3

    .line 485
    :try_start_b
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Lcom/dropbox/android/activity/fg; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    .line 494
    :cond_3
    :goto_2
    :try_start_c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 496
    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 497
    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 500
    :cond_4
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->q()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "editable"

    iget-boolean v4, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "charset"

    iget-object v4, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 522
    :goto_3
    return-object v0

    .line 443
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_4
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 444
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 445
    if-eqz v5, :cond_5

    iget-object v2, v5, Lcom/dropbox/android/activity/fl;->a:Landroid/os/ParcelFileDescriptor;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3

    if-eqz v2, :cond_5

    .line 447
    :try_start_d
    iget-object v2, v5, Lcom/dropbox/android/activity/fl;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catch Lcom/dropbox/android/activity/fg; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3

    .line 452
    :cond_5
    :goto_5
    :try_start_e
    throw v0
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_e .. :try_end_e} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    .line 503
    :catch_0
    move-exception v0

    .line 504
    invoke-static {p0, v9}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 505
    sget-object v2, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Problem opening file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v4}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 506
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    move-object v0, v1

    .line 507
    goto :goto_3

    .line 474
    :cond_6
    add-int/2addr v0, v7

    .line 475
    const v8, 0x3e800

    if-le v0, v8, :cond_8

    .line 476
    :try_start_f
    new-instance v0, Lcom/dropbox/android/activity/fg;

    const/4 v5, 0x0

    invoke-direct {v0, v5}, Lcom/dropbox/android/activity/fg;-><init>(Lcom/dropbox/android/activity/fb;)V

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 481
    :catchall_1
    move-exception v0

    :goto_6
    :try_start_10
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    .line 482
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3

    .line 483
    if-eqz v4, :cond_7

    .line 485
    :try_start_11
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_5
    .catch Lcom/dropbox/android/activity/fg; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3

    .line 490
    :cond_7
    :goto_7
    :try_start_12
    throw v0
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Lcom/dropbox/android/activity/fg; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_12} :catch_2
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_3

    .line 508
    :catch_1
    move-exception v0

    .line 509
    const v0, 0x7f0d01fc

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 510
    sget-object v0, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Too large of a file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    move-object v0, v1

    .line 512
    goto :goto_3

    .line 478
    :cond_8
    const/4 v8, 0x0

    :try_start_13
    invoke-virtual {v5, v6, v8, v7}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_1

    .line 513
    :catch_2
    move-exception v0

    .line 514
    const v2, 0x7f0d01ff

    invoke-static {p0, v2}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 515
    sget-object v2, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Security exception opening: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v4}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 516
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    move-object v0, v1

    .line 517
    goto/16 :goto_3

    .line 518
    :catch_3
    move-exception v0

    .line 519
    invoke-static {p0, v9}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 520
    sget-object v2, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Problem opening file:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v4}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 521
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    move-object v0, v1

    .line 522
    goto/16 :goto_3

    .line 486
    :catch_4
    move-exception v0

    goto/16 :goto_2

    :catch_5
    move-exception v2

    goto/16 :goto_7

    .line 481
    :catchall_2
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_6

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto/16 :goto_6

    .line 448
    :catch_6
    move-exception v2

    goto/16 :goto_5

    .line 443
    :catchall_4
    move-exception v0

    move-object v2, v1

    goto/16 :goto_4

    :catchall_5
    move-exception v0

    goto/16 :goto_4

    .line 448
    :catch_7
    move-exception v2

    goto/16 :goto_0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/TextEditActivity;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Z

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 720
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    if-eqz v0, :cond_0

    .line 721
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 722
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 723
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/activity/TextEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 725
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    .line 726
    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/TextEditActivity;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    return v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 754
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    if-eqz v0, :cond_0

    .line 755
    invoke-static {p0}, Lcom/dropbox/android/activity/fd;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 759
    :goto_0
    return-void

    .line 757
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->g()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 239
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 241
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    .line 314
    :goto_0
    return-void

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 249
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 250
    const-string v3, "android.intent.action.EDIT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 252
    :cond_1
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    .line 254
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 255
    sget-object v0, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t edit this type of file: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const v0, 0x7f0d01fb

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 258
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    .line 289
    :goto_1
    const-string v0, "CHARACTER_SET"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    if-eqz v0, :cond_3

    .line 291
    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    .line 296
    :cond_3
    const v0, 0x7f0300ca

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->setContentView(I)V

    .line 298
    const v0, 0x7f0701bc

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Landroid/widget/ScrollView;

    .line 300
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    if-eqz v0, :cond_7

    .line 301
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->a(Ljava/lang/String;)V

    .line 312
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 313
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    goto :goto_0

    .line 262
    :cond_5
    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 265
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_8

    .line 267
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "EXTRA_OUTPUT_DIR"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 269
    :goto_3
    const-string v3, "An output directory must be specified for ACTION_GET_CONTENT"

    invoke-static {v0, v3}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v3

    invoke-static {v3}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 272
    new-instance v3, Ldbxyzptlk/db231222/k/f;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ldbxyzptlk/db231222/k/f;-><init>(Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/util/DropboxPath;)V

    .line 275
    invoke-virtual {v3}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ldbxyzptlk/db231222/k/f;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    .line 280
    const v0, 0x7f0d01f7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    goto/16 :goto_1

    .line 284
    :cond_6
    sget-object v1, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exiting, unknown action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    goto/16 :goto_0

    .line 303
    :cond_7
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->f()Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Ljava/lang/String;)V

    .line 307
    if-eqz v0, :cond_4

    .line 308
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Lcom/dropbox/android/util/e;

    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/e;->a(Landroid/net/Uri;)V

    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 325
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Z

    if-eqz v0, :cond_0

    .line 326
    const v0, 0x7f0d01f0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v1, v1, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    .line 328
    :cond_0
    const v0, 0x7f0d01f1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v1, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    .line 330
    return v2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 318
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onDestroy()V

    .line 319
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Lcom/dropbox/android/util/e;

    invoke-virtual {v0}, Lcom/dropbox/android/util/e;->a()V

    .line 320
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 731
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 748
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 733
    :pswitch_0
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 734
    invoke-static {p0}, Lcom/dropbox/android/activity/fi;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    goto :goto_0

    .line 736
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Landroid/net/Uri;Z)Z

    .line 737
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bg()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 741
    :pswitch_1
    iget-boolean v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    if-nez v1, :cond_1

    .line 742
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->g()V

    goto :goto_0

    .line 744
    :cond_1
    invoke-static {p0}, Lcom/dropbox/android/activity/fd;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    goto :goto_0

    .line 731
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
