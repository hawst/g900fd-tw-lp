.class public Lcom/dropbox/android/activity/SendFeedbackActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/activity/eD;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 17
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/SendFeedbackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    const-string v1, "EXTRA_FEEDBACK_TYPE"

    invoke-virtual {p1}, Lcom/dropbox/android/activity/eD;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 25
    iput-boolean v1, p0, Lcom/dropbox/android/activity/SendFeedbackActivity;->d:Z

    .line 26
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->finish()V

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 36
    if-nez p1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_FEEDBACK_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/eD;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/eD;

    move-result-object v0

    .line 38
    invoke-static {v0}, Lcom/dropbox/android/activity/SendFeedbackFragment;->a(Lcom/dropbox/android/activity/eD;)Lcom/dropbox/android/activity/SendFeedbackFragment;

    move-result-object v0

    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 40
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 41
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
