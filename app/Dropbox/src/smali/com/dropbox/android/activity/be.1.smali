.class final Lcom/dropbox/android/activity/be;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/dropbox/android/filemanager/LocalEntry;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/dropbox/android/activity/be;->b:Lcom/dropbox/android/activity/GalleryActivity;

    iput-object p6, p0, Lcom/dropbox/android/activity/be;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    check-cast p2, Ljava/util/ArrayList;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/be;->a(Lcom/dropbox/android/albums/u;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/albums/u;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/u;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/be;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v0, p2, p1}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d027a

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->a(I)V

    .line 217
    return-void
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 202
    check-cast p2, Landroid/content/Intent;

    .line 203
    iget-object v0, p0, Lcom/dropbox/android/activity/be;->b:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p2, p1, v1, v2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/dropbox/android/albums/Album;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aN()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num.items"

    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "component.shared.to"

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "create"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 212
    return-void
.end method
