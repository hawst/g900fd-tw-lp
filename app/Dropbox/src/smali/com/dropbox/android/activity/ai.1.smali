.class final Lcom/dropbox/android/activity/ai;
.super Lcom/dropbox/android/activity/ak;
.source "panda.py"


# instance fields
.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Ljava/lang/Integer;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(I[Lcom/dropbox/android/activity/al;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 207
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/ak;-><init>(I[Lcom/dropbox/android/activity/al;)V

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    .line 201
    iput-boolean v1, p0, Lcom/dropbox/android/activity/ai;->g:Z

    .line 204
    iput-boolean v1, p0, Lcom/dropbox/android/activity/ai;->h:Z

    .line 208
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 276
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/ai;->g:Z

    if-eqz v0, :cond_2

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 284
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 285
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ai;->a()Ljava/lang/Integer;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_3

    .line 287
    iget-object v1, p0, Lcom/dropbox/android/activity/ai;->d:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    if-eqz p1, :cond_0

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040003

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/dropbox/android/activity/ai;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 294
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/dropbox/android/activity/ai;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 270
    :cond_0
    const/4 v0, 0x0

    .line 272
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 239
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 240
    :goto_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/ai;->f:Ljava/lang/Integer;

    .line 241
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/ai;->c(Z)V

    .line 242
    return-void

    :cond_0
    move v0, v2

    .line 238
    goto :goto_0

    :cond_1
    move v1, v2

    .line 239
    goto :goto_1
.end method

.method public final a(Lcom/actionbarsherlock/app/ActionBar$Tab;)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 212
    const v0, 0x7f0300b1

    invoke-virtual {p1, v0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setCustomView(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    .line 213
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 220
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 221
    const/16 v2, 0x10

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 222
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    const v0, 0x7f070087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 225
    iget v2, p0, Lcom/dropbox/android/activity/ai;->a:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 226
    iput-object v0, p0, Lcom/dropbox/android/activity/ai;->c:Landroid/view/View;

    .line 227
    const v0, 0x7f0701b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/ai;->d:Landroid/widget/TextView;

    .line 228
    const v0, 0x7f0701b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/ai;->e:Landroid/view/View;

    .line 229
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/dropbox/android/activity/ai;->g:Z

    if-eq v0, p1, :cond_0

    .line 252
    iput-boolean p1, p0, Lcom/dropbox/android/activity/ai;->g:Z

    .line 253
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/ai;->c(Z)V

    .line 255
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/dropbox/android/activity/ai;->h:Z

    .line 260
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/ai;->c(Z)V

    .line 261
    return-void
.end method
