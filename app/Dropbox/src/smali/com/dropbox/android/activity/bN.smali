.class final Lcom/dropbox/android/activity/bN;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/analytics/m;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/dropbox/android/util/HistoryEntry;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/dropbox/android/util/HistoryEntry;)V
    .locals 0

    .prologue
    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    iput-object p1, p0, Lcom/dropbox/android/activity/bN;->a:Ljava/lang/String;

    .line 430
    iput-object p2, p0, Lcom/dropbox/android/activity/bN;->b:Lcom/dropbox/android/util/HistoryEntry;

    .line 431
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/analytics/l;)V
    .locals 3

    .prologue
    .line 435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/activity/bN;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bN;->b:Lcom/dropbox/android/util/HistoryEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/util/HistoryEntry;->a()Lcom/dropbox/android/util/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/al;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 437
    iget-object v0, p0, Lcom/dropbox/android/activity/bN;->b:Lcom/dropbox/android/util/HistoryEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->d()Ljava/lang/String;

    move-result-object v0

    .line 438
    if-eqz v0, :cond_0

    .line 439
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dropbox/android/activity/bN;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_anon_path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 441
    :cond_0
    return-void
.end method
