.class public Lcom/dropbox/android/activity/UserPrefsActivity;
.super Lcom/dropbox/android/activity/base/BasePreferenceActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;-><init>()V

    .line 69
    return-void
.end method

.method private static a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Landroid/preference/PreferenceCategory;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0, p2}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 185
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 187
    :cond_0
    return-void
.end method

.method private static a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Lcom/dropbox/android/service/a;)V
    .locals 3

    .prologue
    .line 118
    const-string v0, "account_category"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 119
    const-string v1, "settings_signin"

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    const-string v2, "payments_upgrade"

    invoke-static {p0, v0, v2}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    .line 152
    :goto_0
    const-string v0, "settings_signout"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_0

    .line 158
    :cond_0
    invoke-static {p0, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Ldbxyzptlk/db231222/s/a;)V

    .line 159
    return-void

    .line 126
    :cond_1
    const-string v0, "payments_upgrade"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 127
    new-instance v2, Lcom/dropbox/android/activity/fJ;

    invoke-direct {v2, p0, p1, p2}, Lcom/dropbox/android/activity/fJ;-><init>(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Lcom/dropbox/android/service/a;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 139
    const-string v0, "settings_space"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 140
    new-instance v2, Lcom/dropbox/android/activity/fK;

    invoke-direct {v2, p0, p1, p2}, Lcom/dropbox/android/activity/fK;-><init>(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Lcom/dropbox/android/service/a;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->k()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Lcom/dropbox/android/service/a;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Ldbxyzptlk/db231222/s/a;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 191
    const-string v0, "settings_space"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 192
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 193
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v3

    .line 195
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v5

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v0

    add-long/2addr v0, v5

    .line 200
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 201
    cmp-long v6, v0, v3

    if-lez v6, :cond_2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v6

    if-nez v6, :cond_2

    .line 203
    new-instance v0, Landroid/text/SpannableString;

    const v1, 0x7f0d0147

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 204
    new-instance v1, Landroid/text/SpannableString;

    const v3, 0x7f0d0149

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 207
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const/high16 v4, -0x10000

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 208
    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v4

    invoke-interface {v0, v3, v8, v4, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 209
    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v4

    invoke-interface {v1, v3, v8, v4, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 212
    invoke-virtual {v2, v9}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 222
    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 223
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 227
    :cond_0
    const-string v0, "settings_name"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/q;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 231
    :cond_1
    return-void

    .line 215
    :cond_2
    invoke-static {v5, v0, v1, v3, v4}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JJ)Ljava/lang/String;

    move-result-object v6

    .line 216
    invoke-static {v5, v3, v4, v8}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v3

    .line 217
    new-instance v0, Landroid/text/SpannableString;

    const v1, 0x7f0d0146

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 218
    new-instance v1, Landroid/text/SpannableString;

    const v4, 0x7f0d0148

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v6, v7, v8

    aput-object v3, v7, v9

    invoke-virtual {v5, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 219
    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private static a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 163
    const-string v0, "account_category"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 164
    const-string v1, "settings_space"

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    .line 165
    const-string v1, "payments_upgrade"

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    .line 166
    const-string v1, "settings_signout"

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    .line 169
    const-string v0, "settings_name"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 175
    :cond_0
    const-string v0, "settings_signin"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_1

    .line 179
    :cond_1
    return-void
.end method

.method private e()Ldbxyzptlk/db231222/r/d;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UserPrefsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 81
    const-string v2, "com.dropbox.activity.extra.ROLE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ldbxyzptlk/db231222/s/k;->a(I)Ldbxyzptlk/db231222/s/k;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_0

    sget-object v2, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    if-ne v1, v2, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v2

    .line 88
    if-eqz v2, :cond_0

    .line 93
    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/UserPrefsActivity;->addPreferencesFromResource(I)V

    .line 50
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UserPrefsActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/UserPrefsActivity;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 54
    if-nez v1, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UserPrefsActivity;->finish()V

    .line 75
    :goto_0
    return-void

    .line 61
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/fL;->a:[I

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->g()Ldbxyzptlk/db231222/s/k;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/k;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 69
    const-string v0, "Expected user to be specified in intent"

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 63
    :pswitch_0
    const v0, 0x7f0d0157

    .line 73
    :goto_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v2

    invoke-static {p0, v2, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;)V

    .line 74
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/UserPrefsActivity;->setTitle(I)V

    goto :goto_0

    .line 66
    :pswitch_1
    const v0, 0x7f0d0158

    .line 67
    goto :goto_1

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
