.class public Lcom/dropbox/android/activity/SendToFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/ImageButton;

.field private final h:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/SendToFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 124
    new-instance v0, Lcom/dropbox/android/activity/eJ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eJ;-><init>(Lcom/dropbox/android/activity/SendToFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->h:Landroid/view/View$OnClickListener;

    .line 45
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SendToFragment;->setArguments(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_HIDE_QUICKACTIONS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 178
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 182
    if-nez v0, :cond_0

    .line 185
    const-string v0, ""

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/k/h;->f()Ljava/io/File;

    move-result-object v1

    .line 192
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 196
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :try_start_1
    new-instance v1, Ljava/io/OutputStreamWriter;

    const-string v5, "UTF-8"

    invoke-direct {v1, v3, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 199
    :try_start_2
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 201
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 204
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_3
    .catch Ljava/io/SyncFailedException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 208
    :goto_0
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V

    .line 209
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 216
    if-eqz v1, :cond_1

    .line 217
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 221
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 222
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 226
    :cond_2
    :goto_2
    new-instance v2, Ljava/util/HashSet;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 227
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 229
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 230
    if-eqz v1, :cond_3

    .line 231
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 233
    check-cast v1, Lcom/dropbox/android/activity/UploadBaseActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/activity/UploadBaseActivity;->a(Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V

    .line 235
    :cond_3
    return-void

    .line 210
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 211
    :goto_3
    :try_start_7
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v5, 0x7f0d0060

    invoke-static {v3, v5}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    .line 213
    sget-object v3, Lcom/dropbox/android/activity/SendToFragment;->d:Ljava/lang/String;

    const-string v5, "IOException creating tmp file for upload"

    invoke-static {v3, v5, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 216
    if-eqz v1, :cond_4

    .line 217
    :try_start_8
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 221
    :cond_4
    :goto_4
    if-eqz v2, :cond_2

    .line 222
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_2

    .line 224
    :catch_1
    move-exception v0

    goto :goto_2

    .line 215
    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 216
    :goto_5
    if-eqz v1, :cond_5

    .line 217
    :try_start_a
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 221
    :cond_5
    :goto_6
    if-eqz v3, :cond_6

    .line 222
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 224
    :cond_6
    :goto_7
    throw v0

    .line 219
    :catch_2
    move-exception v0

    goto :goto_1

    .line 224
    :catch_3
    move-exception v0

    goto :goto_2

    .line 219
    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_6

    .line 224
    :catch_6
    move-exception v1

    goto :goto_7

    .line 215
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_5

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_5

    .line 210
    :catch_7
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_3

    :catch_8
    move-exception v0

    move-object v2, v3

    goto :goto_3

    .line 205
    :catch_9
    move-exception v0

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 82
    const v0, 0x7f030032

    return v0
.end method

.method protected final e()Ldbxyzptlk/db231222/n/r;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 94
    new-instance v0, Lcom/dropbox/android/activity/eH;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eH;-><init>(Lcom/dropbox/android/activity/SendToFragment;)V

    .line 100
    iget-object v1, p0, Lcom/dropbox/android/activity/SendToFragment;->f:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->f:Landroid/widget/Button;

    const v1, 0x7f0d0013

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->e:Landroid/widget/Button;

    const v1, 0x7f0d01d6

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 104
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->e:Landroid/widget/Button;

    iget-object v1, p0, Lcom/dropbox/android/activity/SendToFragment;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->g:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 107
    new-instance v0, Lcom/dropbox/android/activity/eI;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eI;-><init>(Lcom/dropbox/android/activity/SendToFragment;)V

    .line 116
    iget-object v1, p0, Lcom/dropbox/android/activity/SendToFragment;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onAttach(Landroid/app/Activity;)V

    .line 52
    instance-of v0, p1, Lcom/dropbox/android/activity/UploadBaseActivity;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Must be attached to a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/activity/UploadBaseActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 60
    const v0, 0x7f07004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->f:Landroid/widget/Button;

    .line 61
    const v0, 0x7f07004d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->e:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0700ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->g:Landroid/widget/ImageButton;

    .line 63
    return-object v1
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onStop()V

    .line 70
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->n()V

    .line 78
    :cond_0
    return-void
.end method
