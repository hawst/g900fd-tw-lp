.class public Lcom/dropbox/android/activity/lock/LockReceiver;
.super Landroid/content/BroadcastReceiver;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/dropbox/android/activity/lock/LockReceiver;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/dropbox/android/activity/lock/r;

.field private final c:Lcom/dropbox/android/activity/lock/s;

.field private final d:Landroid/support/v4/content/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/lock/LockReceiver;->a:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/lock/LockReceiver;->e:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 68
    new-instance v0, Lcom/dropbox/android/activity/lock/r;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/lock/r;-><init>(Lcom/dropbox/android/activity/lock/q;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    .line 93
    invoke-static {p1}, Landroid/support/v4/content/s;->a(Landroid/content/Context;)Landroid/support/v4/content/s;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->d:Landroid/support/v4/content/s;

    .line 94
    new-instance v0, Lcom/dropbox/android/activity/lock/s;

    invoke-direct {v0, p2}, Lcom/dropbox/android/activity/lock/s;-><init>(Ldbxyzptlk/db231222/r/e;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->c:Lcom/dropbox/android/activity/lock/s;

    .line 95
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/lock/LockReceiver;)Landroid/support/v4/content/s;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->d:Landroid/support/v4/content/s;

    return-object v0
.end method

.method public static a()Lcom/dropbox/android/activity/lock/LockReceiver;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/dropbox/android/activity/lock/LockReceiver;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/LockReceiver;

    .line 88
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 89
    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 8

    .prologue
    .line 285
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 286
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->c:Lcom/dropbox/android/activity/lock/s;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/lock/s;->b()J

    move-result-wide v2

    .line 288
    iget-object v4, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v4}, Lcom/dropbox/android/activity/lock/r;->g(Lcom/dropbox/android/activity/lock/r;)J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/32 v6, 0x493e0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 290
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/lock/LockReceiver;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Locking from activity timeout "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->c(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Context;)V

    .line 298
    :cond_1
    :goto_0
    return-void

    .line 296
    :cond_2
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2, v0, v1}, Lcom/dropbox/android/activity/lock/r;->d(Lcom/dropbox/android/activity/lock/r;J)J

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->c:Lcom/dropbox/android/activity/lock/s;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/s;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 237
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/lock/LockReceiver;->a:Ljava/lang/String;

    const-string v1, "Activity is locked, redirecting to lock code"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 239
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 240
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_2

    .line 241
    instance-of v1, p1, Lcom/dropbox/android/activity/base/BaseActivity;

    if-nez v1, :cond_1

    instance-of v1, p1, Lcom/dropbox/android/activity/base/BasePreferenceActivity;

    if-nez v1, :cond_1

    .line 242
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 252
    :cond_1
    check-cast p1, Landroid/app/Activity;

    const v1, 0xfd91

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 254
    :cond_2
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 255
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;)V
    .locals 4

    .prologue
    .line 98
    sget-object v1, Lcom/dropbox/android/activity/lock/LockReceiver;->e:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 99
    :try_start_0
    new-instance v0, Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/activity/lock/LockReceiver;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/e;)V

    .line 100
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    sget-object v2, Lcom/dropbox/android/activity/lock/LockReceiver;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 103
    monitor-exit v1

    .line 104
    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Z)V
    .locals 6

    .prologue
    .line 136
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->c:Lcom/dropbox/android/activity/lock/s;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/s;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0, p2}, Lcom/dropbox/android/activity/lock/r;->c(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 144
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->d(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 145
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 147
    :cond_1
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 149
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->e(Lcom/dropbox/android/activity/lock/r;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    .line 153
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2, v0, v1}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;J)J

    .line 154
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/r;->c(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 160
    :cond_3
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 189
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_0

    const-class v1, Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PURPOSE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PURPOSE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->a:Lcom/dropbox/android/activity/lock/f;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/lock/f;->a()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/lock/r;->c(Lcom/dropbox/android/activity/lock/r;J)J

    .line 196
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockReceiver;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->d:Landroid/support/v4/content/s;

    new-instance v1, Lcom/dropbox/android/activity/lock/q;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/lock/q;-><init>(Lcom/dropbox/android/activity/lock/LockReceiver;Ljava/lang/Runnable;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "ACTION_UNLOCK"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/s;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 125
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->c:Lcom/dropbox/android/activity/lock/s;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/s;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/r;->c(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/r;->c(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;J)J

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->f(Lcom/dropbox/android/activity/lock/r;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 179
    :cond_0
    return-void
.end method

.method final d()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/lock/r;->d(Lcom/dropbox/android/activity/lock/r;J)J

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 223
    new-instance v0, Landroid/content/Intent;

    const-string v1, "ACTION_UNLOCK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->d:Landroid/support/v4/content/s;

    invoke-virtual {v1, v0}, Landroid/support/v4/content/s;->a(Landroid/content/Intent;)Z

    .line 225
    return-void
.end method

.method final e()V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;J)J

    .line 229
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 108
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 109
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;Z)Z

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 114
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->a(Lcom/dropbox/android/activity/lock/r;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockReceiver;->b:Lcom/dropbox/android/activity/lock/r;

    invoke-static {v2}, Lcom/dropbox/android/activity/lock/r;->b(Lcom/dropbox/android/activity/lock/r;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 118
    sget-object v0, Lcom/dropbox/android/activity/lock/LockReceiver;->a:Ljava/lang/String;

    const-string v1, "Screen has turned off, locking"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Context;)V

    .line 122
    :cond_0
    return-void
.end method
