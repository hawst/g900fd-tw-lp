.class Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;
.super Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 438
    new-instance v0, Lcom/dropbox/android/activity/br;

    invoke-direct {v0}, Lcom/dropbox/android/activity/br;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 434
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    .line 435
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 409
    iput-object p1, p0, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    .line 410
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;)V
    .locals 2

    .prologue
    .line 419
    check-cast p2, Lcom/dropbox/android/activity/GalleryActivity;

    .line 420
    invoke-static {p2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/albums/o;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 421
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 430
    return-void
.end method
