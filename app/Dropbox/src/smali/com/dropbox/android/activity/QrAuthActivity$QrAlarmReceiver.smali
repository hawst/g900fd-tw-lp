.class public Lcom/dropbox/android/activity/QrAuthActivity$QrAlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 117
    const-string v0, "com.dropbox.android.activity.QrAuthFragment.ACTION_RI_REMINDER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 118
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 119
    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 120
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v4

    .line 121
    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 122
    if-nez v0, :cond_2

    .line 167
    :cond_0
    :goto_1
    return-void

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :cond_2
    const-string v2, "com.dropbox.intent.extra.USER_ID"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 127
    const/4 v2, 0x0

    .line 128
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 129
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 134
    :goto_2
    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v2

    .line 140
    const-string v5, "com.dropbox.intent.extra.REMINDER_SOURCE"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 143
    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->b()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->e()Z

    move-result v5

    if-nez v5, :cond_0

    .line 147
    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/n/P;->d(Z)V

    .line 149
    new-instance v1, Lcom/dropbox/android/activity/em;

    invoke-direct {v1, p0, v0, v4, v3}, Lcom/dropbox/android/activity/em;-><init>(Lcom/dropbox/android/activity/QrAuthActivity$QrAlarmReceiver;Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/r/e;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dropbox/android/activity/em;->start()V

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2
.end method
