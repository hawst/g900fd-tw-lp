.class public Lcom/dropbox/android/activity/TourActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/fz;


# instance fields
.field private a:[Lcom/dropbox/android/activity/fn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;[Lcom/dropbox/android/activity/fn;)Landroid/content/Intent;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-static {p0, p1, p2, v0, v0}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;[Lcom/dropbox/android/activity/fn;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;[Lcom/dropbox/android/activity/fn;ZZ)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 48
    array-length v0, p2

    new-array v1, v0, [Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 50
    aget-object v2, p2, v0

    invoke-virtual {v2}, Lcom/dropbox/android/activity/fn;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/TourActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->D()Z

    move-result v2

    if-nez v2, :cond_2

    .line 58
    :cond_1
    const-string v2, "INTRO_TOUR"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 60
    :cond_2
    const-string v2, "EXTRA_TOUR_PAGES"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    if-eqz p3, :cond_3

    .line 62
    const-string v1, "EXTRA_SAMSUNG_STYLE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 63
    const-string v1, "EXTRA_SAMSUNG_DARK"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 66
    :cond_3
    return-object v0
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/TourActivity;->b(I)Lcom/dropbox/android/activity/base/BaseUserFragment;

    move-result-object v0

    .line 133
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 134
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 135
    if-eqz p2, :cond_0

    .line 136
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 138
    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 139
    return-void
.end method

.method private b(I)Lcom/dropbox/android/activity/base/BaseUserFragment;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/fn;

    aget-object v0, v0, p1

    .line 144
    sget-object v1, Lcom/dropbox/android/activity/fn;->a:Lcom/dropbox/android/activity/fn;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/dropbox/android/activity/fn;->b:Lcom/dropbox/android/activity/fn;

    if-ne v0, v1, :cond_1

    .line 145
    :cond_0
    invoke-static {v0, p1}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->b(Lcom/dropbox/android/activity/fn;I)Lcom/dropbox/android/activity/TourPageWithThumbsFragment;

    move-result-object v0

    .line 147
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0, p1}, Lcom/dropbox/android/activity/TourPageFragment;->a(Lcom/dropbox/android/activity/fn;I)Lcom/dropbox/android/activity/TourPageFragment;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 122
    add-int/lit8 v0, p1, 0x1

    .line 123
    iget-object v1, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/fn;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 124
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/TourActivity;->a(IZ)V

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setResult(I)V

    .line 127
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 155
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->b()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "page"

    iget-object v3, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/fn;

    aget-object v0, v3, v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/fn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 156
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 157
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_SAMSUNG_STYLE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 72
    if-eqz v2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_SAMSUNG_DARK"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    const v0, 0x7f0c00da

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setTheme(I)V

    .line 81
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->finish()V

    .line 118
    :cond_1
    :goto_1
    return-void

    .line 77
    :cond_2
    const v0, 0x7f0c00d8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setTheme(I)V

    goto :goto_0

    .line 88
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_TOUR_PAGES"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 89
    if-nez v3, :cond_4

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TourActivity expects an extra with the pages it should show."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_4
    array-length v0, v3

    new-array v0, v0, [Lcom/dropbox/android/activity/fn;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/fn;

    move v0, v1

    .line 94
    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_5

    .line 95
    iget-object v4, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/fn;

    aget-object v5, v3, v0

    invoke-static {v5}, Lcom/dropbox/android/activity/fn;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/fn;

    move-result-object v5

    aput-object v5, v4, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 98
    :cond_5
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_6

    if-nez v2, :cond_6

    .line 100
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setRequestedOrientation(I)V

    .line 103
    :cond_6
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 104
    if-eqz v2, :cond_8

    .line 105
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 106
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->hide()V

    .line 113
    :cond_7
    :goto_3
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setContentView(I)V

    .line 115
    if-nez p1, :cond_1

    .line 116
    invoke-direct {p0, v1, v1}, Lcom/dropbox/android/activity/TourActivity;->a(IZ)V

    goto :goto_1

    .line 109
    :cond_8
    invoke-virtual {v0, v6}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 110
    invoke-virtual {v0, v6}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onDestroy()V

    .line 164
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-static {}, Lcom/dropbox/android/filemanager/aq;->a()Lcom/dropbox/android/filemanager/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aq;->b()V

    .line 167
    :cond_0
    return-void
.end method
