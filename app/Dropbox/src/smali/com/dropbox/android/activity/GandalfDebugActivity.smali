.class public Lcom/dropbox/android/activity/GandalfDebugActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# instance fields
.field a:Lcom/dropbox/android/util/analytics/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 37
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GandalfDebugActivity;->a:Lcom/dropbox/android/util/analytics/r;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v0, 0x7f03003f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GandalfDebugActivity;->setContentView(I)V

    .line 92
    const v0, 0x7f0700c3

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GandalfDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 94
    new-array v1, v6, [Lcom/dropbox/android/activity/bB;

    new-instance v2, Lcom/dropbox/android/activity/bB;

    const-string v3, "mobile-remote-installer"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "ri"

    aput-object v5, v4, v7

    const-string v5, "ri-notf"

    aput-object v5, v4, v6

    const/4 v5, 0x2

    const-string v6, "CONTROL"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "NOT_IN_EXPERIMENT"

    aput-object v6, v4, v5

    invoke-direct {v2, v3, v4}, Lcom/dropbox/android/activity/bB;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v2, v1, v7

    .line 96
    new-instance v2, Lcom/dropbox/android/activity/bz;

    const v3, 0x7f03003e

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/dropbox/android/activity/bz;-><init>(Lcom/dropbox/android/activity/GandalfDebugActivity;Landroid/content/Context;I[Lcom/dropbox/android/activity/bB;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    return-void
.end method
