.class final Lcom/dropbox/android/activity/bu;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field a:I

.field b:Z

.field c:Z

.field d:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/bq;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 997
    iput-boolean v2, p0, Lcom/dropbox/android/activity/bu;->b:Z

    .line 998
    iput-boolean v0, p0, Lcom/dropbox/android/activity/bu;->c:Z

    .line 999
    iput-boolean v0, p0, Lcom/dropbox/android/activity/bu;->d:Z

    .line 1002
    sget-object v0, Lcom/dropbox/android/activity/bd;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/activity/bq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1014
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected gallery type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1005
    :pswitch_0
    sget v0, Lcom/dropbox/android/albums/PhotosModel;->a:I

    iput v0, p0, Lcom/dropbox/android/activity/bu;->a:I

    .line 1016
    :goto_0
    return-void

    .line 1008
    :pswitch_1
    sget v0, Lcom/dropbox/android/provider/PhotosProvider;->e:I

    iput v0, p0, Lcom/dropbox/android/activity/bu;->a:I

    .line 1009
    iput-boolean v2, p0, Lcom/dropbox/android/activity/bu;->d:Z

    goto :goto_0

    .line 1012
    :pswitch_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t create a SortInfo for folders via this constructor."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1002
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/n/r;IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 997
    iput-boolean v1, p0, Lcom/dropbox/android/activity/bu;->b:Z

    .line 998
    iput-boolean v3, p0, Lcom/dropbox/android/activity/bu;->c:Z

    .line 999
    iput-boolean v3, p0, Lcom/dropbox/android/activity/bu;->d:Z

    .line 1022
    iput p2, p0, Lcom/dropbox/android/activity/bu;->a:I

    .line 1023
    sget-object v0, Lcom/dropbox/android/activity/bd;->b:[I

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/r;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1036
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected sort order"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1025
    :pswitch_0
    if-eqz p3, :cond_0

    const/16 v0, 0xe

    :goto_0
    iput v0, p0, Lcom/dropbox/android/activity/bu;->a:I

    .line 1028
    iput-boolean v1, p0, Lcom/dropbox/android/activity/bu;->c:Z

    .line 1038
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 1025
    goto :goto_0

    .line 1031
    :pswitch_1
    const/16 v0, 0xf

    iput v0, p0, Lcom/dropbox/android/activity/bu;->a:I

    .line 1032
    iput-boolean v1, p0, Lcom/dropbox/android/activity/bu;->d:Z

    .line 1033
    iput-boolean v3, p0, Lcom/dropbox/android/activity/bu;->b:Z

    goto :goto_1

    .line 1023
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Ljava/lang/Long;)J
    .locals 2

    .prologue
    .line 1046
    iget-boolean v0, p0, Lcom/dropbox/android/activity/bu;->b:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 1047
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/dropbox/android/activity/bu;->a:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1041
    iget-boolean v0, p0, Lcom/dropbox/android/activity/bu;->b:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 1042
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/dropbox/android/activity/bu;->a:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_0
    return-object p2
.end method
