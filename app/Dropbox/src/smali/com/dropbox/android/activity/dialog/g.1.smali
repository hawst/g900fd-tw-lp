.class final Lcom/dropbox/android/activity/dialog/g;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/g;->a:Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/g;->a:Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/g;->a:Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d01fa

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/g;->a:Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/dropbox/android/activity/SendToFragment;

    if-nez v0, :cond_1

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need a target fragment of type SendToFragment with this dialog."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/g;->a:Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/SendToFragment;->a(Ljava/lang/String;)V

    .line 90
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method
