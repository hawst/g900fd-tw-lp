.class final Lcom/dropbox/android/activity/ar;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/view/aA;
.implements Lcom/actionbarsherlock/app/ActionBar$TabListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/DropboxActionBarActivity;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .locals 1

    .prologue
    .line 743
    iput-object p1, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    .line 744
    invoke-virtual {p1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 745
    return-void
.end method

.method private a()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 889
    :try_start_0
    const-string v0, "android.support.v4.app.FragmentStatePagerAdapter"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 890
    const-string v1, "mFragments"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 891
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 893
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 894
    return-object v0

    .line 895
    :catch_0
    move-exception v0

    .line 896
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 897
    :catch_1
    move-exception v0

    .line 898
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 899
    :catch_2
    move-exception v0

    .line 900
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 906
    :try_start_0
    const-string v0, "android.support.v4.app.FragmentStatePagerAdapter"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 907
    const-string v1, "mSavedState"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 908
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 910
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 912
    return-object v0

    .line 913
    :catch_0
    move-exception v0

    .line 914
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 915
    :catch_1
    move-exception v0

    .line 916
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 917
    :catch_2
    move-exception v0

    .line 918
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()Landroid/support/v4/app/FragmentTransaction;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .prologue
    .line 925
    :try_start_0
    const-string v0, "android.support.v4.app.FragmentStatePagerAdapter"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 926
    const-string v0, "mCurTransaction"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 927
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 928
    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentTransaction;

    .line 929
    if-nez v0, :cond_0

    .line 930
    const-string v0, "mFragmentManager"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 931
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 932
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentManager;

    .line 933
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 934
    invoke-virtual {v2, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 936
    :cond_0
    return-object v0

    .line 937
    :catch_0
    move-exception v0

    .line 938
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 939
    :catch_1
    move-exception v0

    .line 940
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 941
    :catch_2
    move-exception v0

    .line 942
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected field to be present"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 860
    iget-object v0, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 861
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 870
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 872
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 856
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 853
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 840
    iget-object v0, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final getItemPosition(Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 876
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 877
    iget-object v2, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;

    move-result-object v2

    aget-object v3, v2, v0

    move v2, v1

    .line 878
    :goto_1
    iget-object v4, v3, Lcom/dropbox/android/activity/ak;->b:[Lcom/dropbox/android/activity/al;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 879
    iget-object v4, v3, Lcom/dropbox/android/activity/ak;->b:[Lcom/dropbox/android/activity/al;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-ne p1, v4, :cond_1

    .line 880
    invoke-virtual {v3}, Lcom/dropbox/android/activity/ak;->c()I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, -0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_2

    .line 878
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 876
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 884
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "What is this object that\'s not a fragment we know of?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 958
    invoke-direct {p0}, Lcom/dropbox/android/activity/ar;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 959
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 960
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 961
    if-eqz v0, :cond_0

    .line 983
    :goto_0
    return-object v0

    .line 966
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/ar;->c()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 967
    invoke-direct {p0}, Lcom/dropbox/android/activity/ar;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 968
    invoke-virtual {p0, p2}, Lcom/dropbox/android/activity/ar;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 969
    invoke-direct {p0, p2}, Lcom/dropbox/android/activity/ar;->c(I)Ljava/lang/String;

    move-result-object v4

    .line 970
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, p2, :cond_1

    .line 971
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    .line 972
    if-eqz v0, :cond_1

    .line 973
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 976
    :cond_1
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_2

    .line 977
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 979
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 980
    invoke-virtual {v2, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 981
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    invoke-virtual {v3, v0, v1, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-object v0, v1

    .line 983
    goto :goto_0
.end method

.method public final onTabReselected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 3

    .prologue
    .line 786
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ak;

    .line 787
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->d()I

    move-result v1

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->c()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 789
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v0

    .line 790
    sget-object v1, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    if-ne v0, v1, :cond_1

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 798
    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    .line 799
    new-instance v1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    sget-object v2, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    sget-object v1, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 813
    :cond_2
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v1

    .line 814
    invoke-virtual {v1}, Lcom/dropbox/android/activity/al;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 815
    invoke-virtual {v1}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 817
    :cond_3
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/ak;->c(I)V

    .line 818
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v0

    .line 819
    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 820
    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/app/FragmentTransaction;->attach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0

    .line 822
    :cond_4
    const v1, 0x7f0700b2

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method

.method public final onTabSelected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 749
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ak;

    .line 750
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v4

    .line 753
    iget-object v1, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->c(Lcom/dropbox/android/activity/DropboxActionBarActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 754
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bJ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v3, "type"

    invoke-virtual {v4}, Lcom/dropbox/android/activity/al;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    .line 755
    instance-of v1, v0, Lcom/dropbox/android/activity/ai;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 756
    check-cast v1, Lcom/dropbox/android/activity/ai;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/ai;->a()Ljava/lang/Integer;

    move-result-object v1

    .line 757
    if-eqz v1, :cond_0

    .line 758
    const-string v5, "badge_count"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v6, v1

    invoke-virtual {v3, v5, v6, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 761
    :cond_0
    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 764
    :cond_1
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/ak;->c(I)V

    .line 765
    iget-object v1, p0, Lcom/dropbox/android/activity/ar;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v7, v5, v3

    .line 766
    if-ne v0, v7, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v7, v1}, Lcom/dropbox/android/activity/ak;->b(Z)V

    .line 765
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 766
    goto :goto_1

    .line 776
    :cond_3
    invoke-virtual {v4}, Lcom/dropbox/android/activity/al;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 777
    invoke-virtual {v4}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/app/FragmentTransaction;->attach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 782
    :goto_2
    return-void

    .line 779
    :cond_4
    const v0, 0x7f0700b2

    invoke-virtual {v4}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v4}, Lcom/dropbox/android/activity/al;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_2
.end method

.method public final onTabUnselected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 830
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ak;

    .line 832
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/activity/al;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 833
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 836
    :cond_0
    return-void
.end method
