.class public abstract Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CallbackType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/dropbox/android/activity/base/BaseFragment;"
    }
.end annotation


# instance fields
.field protected b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TCallbackType;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TCallbackType;>;"
        }
    .end annotation
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 18
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 19
    iput-object p1, p0, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->b:Ljava/lang/Object;

    .line 20
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 24
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->b:Ljava/lang/Object;

    .line 26
    return-void
.end method
