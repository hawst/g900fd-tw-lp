.class final Lcom/dropbox/android/activity/base/j;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/r/e;

.field private b:Ldbxyzptlk/db231222/r/d;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/r/e;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/j;->b:Ldbxyzptlk/db231222/r/d;

    .line 108
    iput-boolean v1, p0, Lcom/dropbox/android/activity/base/j;->c:Z

    .line 109
    iput-boolean v1, p0, Lcom/dropbox/android/activity/base/j;->d:Z

    .line 114
    iput-boolean v1, p0, Lcom/dropbox/android/activity/base/j;->e:Z

    .line 116
    iput-boolean v1, p0, Lcom/dropbox/android/activity/base/j;->f:Z

    .line 119
    iput-object p1, p0, Lcom/dropbox/android/activity/base/j;->a:Ldbxyzptlk/db231222/r/e;

    .line 120
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->d:Z

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must check shouldFinish() in onCreate!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->e:Z

    if-nez v0, :cond_1

    .line 158
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Forgot to call finish() in onCreate!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_1
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 129
    iput-boolean v3, p0, Lcom/dropbox/android/activity/base/j;->c:Z

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/activity/base/j;->a:Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 131
    if-nez v0, :cond_0

    .line 132
    iput-boolean v3, p0, Lcom/dropbox/android/activity/base/j;->f:Z

    .line 145
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 137
    if-eqz p2, :cond_1

    .line 138
    const-string v1, "ActivityUserManager.SIS_USER_ID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 140
    iput-boolean v3, p0, Lcom/dropbox/android/activity/base/j;->f:Z

    goto :goto_0

    .line 144
    :cond_1
    iput-object v0, p0, Lcom/dropbox/android/activity/base/j;->b:Ldbxyzptlk/db231222/r/d;

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/base/j;->b:Ldbxyzptlk/db231222/r/d;

    if-eqz v0, :cond_0

    .line 149
    const-string v0, "ActivityUserManager.SIS_USER_ID"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/j;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->c:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->d:Z

    .line 165
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->f:Z

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->e:Z

    .line 174
    return-void
.end method

.method public final d()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 178
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->c:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 179
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->d:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 180
    iget-object v0, p0, Lcom/dropbox/android/activity/base/j;->b:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method final e()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 190
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 191
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/j;->c:Z

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/activity/base/j;->b:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method
