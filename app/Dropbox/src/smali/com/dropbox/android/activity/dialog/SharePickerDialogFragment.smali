.class public Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;
.super Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;

.field static final b:Landroid/content/ComponentName;

.field static final c:Landroid/content/ComponentName;

.field static final d:Landroid/content/ComponentName;


# instance fields
.field private e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

.field private f:Z

.field private g:Lcom/dropbox/android/activity/dialog/A;

.field private h:Lcom/dropbox/android/activity/dialog/B;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const-class v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a:Ljava/lang/String;

    .line 67
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.mms"

    const-string v2, "com.android.mms.ui.ComposeMessageActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b:Landroid/content/ComponentName;

    .line 69
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.mms"

    const-string v2, "com.android.mms.ui.ConversationComposer"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->c:Landroid/content/ComponentName;

    .line 71
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.talk"

    const-string v2, "com.google.android.apps.babel.phone.ShareIntentActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->d:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    .line 456
    return-void
.end method

.method private static a(Lcom/dropbox/android/activity/dialog/A;Ljava/util/List;)Landroid/content/ComponentName;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/dialog/A;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 237
    const-wide/16 v2, 0x0

    .line 238
    const/4 v1, 0x0

    .line 240
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 241
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/A;->a(Landroid/content/ComponentName;)J

    move-result-wide v4

    .line 242
    cmp-long v6, v4, v2

    if-lez v6, :cond_1

    move v10, v7

    .line 243
    :goto_1
    cmp-long v6, v4, v2

    if-nez v6, :cond_2

    move v9, v7

    .line 244
    :goto_2
    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I

    move-result v6

    if-gez v6, :cond_3

    move v6, v7

    .line 246
    :goto_3
    if-nez v10, :cond_0

    if-eqz v9, :cond_5

    if-eqz v6, :cond_5

    :cond_0
    move-wide v1, v4

    :goto_4
    move-wide v12, v1

    move-wide v2, v12

    move-object v1, v0

    .line 250
    goto :goto_0

    :cond_1
    move v10, v8

    .line 242
    goto :goto_1

    :cond_2
    move v9, v8

    .line 243
    goto :goto_2

    :cond_3
    move v6, v8

    .line 244
    goto :goto_3

    .line 252
    :cond_4
    return-object v1

    :cond_5
    move-object v0, v1

    move-wide v12, v2

    move-wide v1, v12

    goto :goto_4
.end method

.method static a(Landroid/content/pm/PackageManager;Lcom/dropbox/android/activity/dialog/A;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 298
    invoke-static {p0, p2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 300
    invoke-static {p1, v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/A;Ljava/util/List;)Landroid/content/ComponentName;

    move-result-object v1

    .line 301
    if-eqz v1, :cond_0

    .line 302
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 338
    :goto_0
    return-object v0

    .line 305
    :cond_0
    invoke-static {p0, v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/pm/PackageManager;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 313
    sget-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b:Landroid/content/ComponentName;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    sget-object v1, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 318
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->c:Landroid/content/ComponentName;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 319
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    sget-object v1, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 323
    :cond_2
    sget-object v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->d:Landroid/content/ComponentName;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 324
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    sget-object v1, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->d:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 328
    const/4 v0, 0x0

    goto :goto_0

    .line 331
    :cond_4
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 332
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 333
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_6

    :goto_2
    move-object v1, v0

    .line 336
    goto :goto_1

    .line 338
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/B;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->h:Lcom/dropbox/android/activity/dialog/B;

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;-><init>()V

    .line 196
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->b(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)V

    .line 197
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 153
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->i(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    :goto_0
    return-object v0

    .line 160
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 161
    if-ne v0, v2, :cond_2

    .line 162
    const v0, 0x7f0d02c5

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 164
    :cond_2
    const v1, 0x7f0f0028

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    const/high16 v0, 0x10000

    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 204
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 205
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 208
    :cond_0
    return-object v1
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 262
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 263
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/pm/PackageManager;->getPreferredActivities(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)I

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 266
    invoke-interface {v0, v1}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    .line 267
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    move-object p1, v0

    .line 270
    :cond_0
    return-object p1
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/dropbox/android/albums/Album;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/dropbox/android/activity/ShareViaEmailActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    invoke-static {p1, p3, p2}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/dropbox/android/albums/Album;)V

    .line 191
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 192
    return-void

    .line 186
    :cond_1
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    if-eqz p4, :cond_0

    .line 188
    const-string v0, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/dropbox/android/activity/ShareViaEmailActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    invoke-static {p1, p3, p2}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 178
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 179
    return-void

    .line 173
    :cond_1
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    if-eqz p4, :cond_0

    .line 175
    const-string v0, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/A;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->g:Lcom/dropbox/android/activity/dialog/A;

    return-object v0
.end method

.method private static b(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    const-string v2, "smsto:"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 214
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 215
    invoke-static {p0, v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 216
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 217
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    :cond_0
    invoke-static {p0, p1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 221
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 222
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 223
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 224
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 227
    :cond_2
    return-object v2
.end method

.method static synthetic c(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/s/a;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 450
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->x()Z

    move-result v0

    if-nez v0, :cond_1

    .line 451
    :cond_0
    const/4 v0, 0x0

    .line 453
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/activity/ShareViaEmailActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public final b(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)V
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    .line 446
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 489
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 491
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    .line 493
    if-nez v2, :cond_1

    .line 494
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 555
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    if-eqz p1, :cond_2

    .line 499
    const-string v0, "SIS_KEY_ShowAll"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    .line 500
    const-string v0, "SIS_KEY_ShareBehavior"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    .line 506
    :cond_2
    new-instance v0, Lcom/dropbox/android/activity/dialog/C;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/dropbox/android/activity/dialog/C;-><init>(Ldbxyzptlk/db231222/r/d;Landroid/content/Context;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 508
    new-instance v0, Lcom/dropbox/android/activity/dialog/A;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/dialog/A;-><init>(Ldbxyzptlk/db231222/n/K;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->g:Lcom/dropbox/android/activity/dialog/A;

    .line 509
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 510
    const-string v0, "text/plain"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 511
    const/high16 v0, 0x10000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 512
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 513
    const/4 v0, 0x3

    new-array v3, v0, [Landroid/content/Intent;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Ldbxyzptlk/db231222/s/a;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v3, v8

    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->g:Lcom/dropbox/android/activity/dialog/A;

    invoke-static {v1, v0, v4}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/pm/PackageManager;Lcom/dropbox/android/activity/dialog/A;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v3, v7

    const/4 v0, 0x2

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-class v6, Lcom/dropbox/android/activity/CopyLinkToClipboardActivity;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    aput-object v2, v3, v0

    .line 518
    new-instance v5, Lcom/dropbox/android/activity/dialog/z;

    invoke-direct {v5, p0, v1}, Lcom/dropbox/android/activity/dialog/z;-><init>(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;Landroid/content/pm/PackageManager;)V

    .line 543
    new-instance v0, Lcom/dropbox/android/activity/dialog/B;

    new-array v2, v7, [Landroid/content/Intent;

    aput-object v4, v2, v8

    sget-object v4, Lcom/dropbox/android/util/Activities;->b:Lcom/dropbox/android/util/c;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/dialog/B;-><init>(Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->h:Lcom/dropbox/android/activity/dialog/B;

    .line 548
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->h:Lcom/dropbox/android/activity/dialog/B;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/B;->a()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_3

    .line 549
    iput-boolean v7, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    .line 552
    :cond_3
    iget-boolean v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->h:Lcom/dropbox/android/activity/dialog/B;

    invoke-virtual {v0, v7}, Lcom/dropbox/android/activity/dialog/B;->a(Z)V

    goto/16 :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 483
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/util/bn;->e()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 484
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    return-object v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 345
    const v0, 0x7f030091

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 347
    const v0, 0x7f070183

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 348
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->h:Lcom/dropbox/android/activity/dialog/B;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 350
    const v1, 0x7f070184

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 351
    new-instance v3, Lcom/dropbox/android/activity/dialog/x;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/dialog/x;-><init>(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;Landroid/widget/Button;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 365
    iget-boolean v3, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    if-eqz v3, :cond_0

    .line 366
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 369
    :cond_0
    new-instance v1, Lcom/dropbox/android/activity/dialog/y;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dialog/y;-><init>(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 383
    return-object v2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 559
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 561
    const-string v0, "SIS_KEY_ShareBehavior"

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->e:Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 562
    const-string v0, "SIS_KEY_ShowAll"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 563
    return-void
.end method
