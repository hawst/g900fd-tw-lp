.class public abstract Lcom/dropbox/android/activity/UploadBaseActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/g/Z;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/dropbox/android/activity/UploadBaseActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/UploadBaseActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/UploadBaseActivity;->b:Z

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/UploadBaseActivity;Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->a(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fI;",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 86
    iget-boolean v0, p0, Lcom/dropbox/android/activity/UploadBaseActivity;->b:Z

    if-nez v0, :cond_0

    .line 87
    sget-object v0, Lcom/dropbox/android/activity/fH;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/activity/fI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 93
    :pswitch_0
    iput-boolean v2, p0, Lcom/dropbox/android/activity/UploadBaseActivity;->b:Z

    .line 94
    new-instance v0, Ldbxyzptlk/db231222/g/X;

    sget-object v1, Lcom/dropbox/android/activity/fI;->b:Lcom/dropbox/android/activity/fI;

    if-ne v1, p1, :cond_1

    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v5

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/g/X;-><init>(Landroid/content/Context;ZLjava/util/Collection;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/filemanager/I;)V

    .line 99
    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/X;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 100
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0, p3}, Ldbxyzptlk/db231222/n/P;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 106
    :cond_0
    :pswitch_1
    return-void

    :cond_1
    move v2, v6

    .line 94
    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/dropbox/android/provider/MetadataManager;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/provider/MetadataManager;",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")V"
        }
    .end annotation

    .prologue
    const v6, 0x7f0d0013

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 131
    :try_start_0
    invoke-static {p0, p2}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ljava/util/Collection;)Ljava/util/HashMap;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 140
    sget-object v0, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    invoke-virtual {p1, p3, v0, v5}, Lcom/dropbox/android/provider/MetadataManager;->c(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;

    move-result-object v2

    .line 141
    if-eqz v2, :cond_0

    .line 143
    :try_start_1
    iget-object v0, v2, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->a(Landroid/database/Cursor;)Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 145
    iget-object v2, v2, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 151
    :goto_0
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/dropbox/android/util/bg;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/HashSet;

    move-result-object v2

    .line 152
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/dropbox/android/util/bg;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/HashSet;

    move-result-object v3

    .line 154
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    sget-object v0, Lcom/dropbox/android/activity/fI;->a:Lcom/dropbox/android/activity/fI;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->a(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    .line 231
    :goto_1
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    sget-object v0, Lcom/dropbox/android/activity/UploadBaseActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Security Exception trying to import "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " files to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const v0, 0x7f0d005e

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_1

    .line 145
    :catchall_0
    move-exception v0

    iget-object v1, v2, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 148
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 162
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 164
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 165
    const v0, 0x7f0d00eb

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 166
    const v2, 0x7f0d00ec

    new-array v3, v7, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v8

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v8

    invoke-virtual {p0, v2, v3}, Lcom/dropbox/android/activity/UploadBaseActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 169
    const v0, 0x7f0d00ed

    sget-object v2, Lcom/dropbox/android/activity/fI;->b:Lcom/dropbox/android/activity/fI;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v2, v1, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 175
    sget-object v0, Lcom/dropbox/android/activity/fI;->d:Lcom/dropbox/android/activity/fI;

    invoke-direct {p0, v0, v5, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 229
    :goto_2
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 182
    :cond_2
    const v0, 0x7f0d00f6

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/UploadBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 183
    const v0, 0x7f0d00f7

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 184
    const v0, 0x7f0d00f8

    sget-object v2, Lcom/dropbox/android/activity/fI;->b:Lcom/dropbox/android/activity/fI;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v2, v1, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 190
    sget-object v0, Lcom/dropbox/android/activity/fI;->d:Lcom/dropbox/android/activity/fI;

    invoke-direct {p0, v0, v5, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 200
    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v7, :cond_4

    .line 201
    const v0, 0x7f0d00ee

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/UploadBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    :goto_3
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 207
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v7, :cond_5

    const v0, 0x7f0d00ef

    :goto_4
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 211
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v7, :cond_6

    const v0, 0x7f0d00f0

    :goto_5
    sget-object v5, Lcom/dropbox/android/activity/fI;->b:Lcom/dropbox/android/activity/fI;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-direct {p0, v5, v6, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 219
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v7, :cond_7

    const v0, 0x7f0d00f1

    :goto_6
    sget-object v3, Lcom/dropbox/android/activity/fI;->c:Lcom/dropbox/android/activity/fI;

    invoke-static {v2, v1}, Lcom/dropbox/android/util/bg;->a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v3, v1, p3}, Lcom/dropbox/android/activity/UploadBaseActivity;->b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 203
    :cond_4
    const v0, 0x7f0d00f2

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v0, v5}, Lcom/dropbox/android/activity/UploadBaseActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 207
    :cond_5
    const v0, 0x7f0d00f3

    goto :goto_4

    .line 211
    :cond_6
    const v0, 0x7f0d00f4

    goto :goto_5

    .line 219
    :cond_7
    const v0, 0x7f0d00f5

    goto :goto_6
.end method

.method private b(Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fI;",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")",
            "Landroid/content/DialogInterface$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lcom/dropbox/android/activity/fG;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/dropbox/android/activity/fG;-><init>(Lcom/dropbox/android/activity/UploadBaseActivity;Lcom/dropbox/android/activity/fI;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.dropbox.android.file_added"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 80
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/UploadBaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;->finish()V

    .line 83
    return-void
.end method

.method protected final a(Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/P;->b(Z)V

    .line 69
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    invoke-static {v1}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 70
    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v1

    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 71
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    .line 72
    invoke-direct {p0, v0, p1, p2}, Lcom/dropbox/android/activity/UploadBaseActivity;->a(Lcom/dropbox/android/provider/MetadataManager;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    .line 73
    return-void
.end method
