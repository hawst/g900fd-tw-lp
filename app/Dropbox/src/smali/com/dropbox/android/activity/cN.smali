.class final Lcom/dropbox/android/activity/cN;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/MoveToFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/MoveToFragment;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 144
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MoveToFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 145
    iget-object v1, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/MoveToFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_LOCAL_ENTRY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 146
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 147
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/MoveToFragment;->a(Lcom/dropbox/android/activity/MoveToFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    .line 157
    invoke-virtual {v1, v4}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MoveToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00c2

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-boolean v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MoveToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00c5

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 163
    :cond_2
    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 165
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MoveToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00c4

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 166
    invoke-static {}, Lcom/dropbox/android/activity/MoveToFragment;->s()Ljava/lang/String;

    move-result-object v0

    const-string v1, "move aborted: uploads in progress"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_3
    iget-boolean v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_4

    iget-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v2, v4}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MoveToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00c3

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 171
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/MoveToFragment;->b(Lcom/dropbox/android/activity/MoveToFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MoveToFragment;->q()V

    .line 175
    new-instance v0, Lcom/dropbox/android/activity/cO;

    iget-object v1, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/MoveToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v5, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-static {v5}, Lcom/dropbox/android/activity/MoveToFragment;->c(Lcom/dropbox/android/activity/MoveToFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v5

    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/cO;-><init>(Landroid/app/Activity;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/service/a;)V

    .line 177
    invoke-virtual {v0}, Lcom/dropbox/android/activity/cO;->f()V

    .line 178
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/cO;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/activity/cN;->a:Lcom/dropbox/android/activity/MoveToFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/MoveToFragment;->d(Lcom/dropbox/android/activity/MoveToFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0, v4}, Ldbxyzptlk/db231222/n/P;->b(Lcom/dropbox/android/util/DropboxPath;)V

    goto/16 :goto_0
.end method
