.class final Lcom/dropbox/android/activity/z;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Lcom/dropbox/android/util/DropboxPath;

.field public final b:I

.field public final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object p1, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 141
    iput p2, p0, Lcom/dropbox/android/activity/z;->b:I

    .line 142
    iput-object p3, p0, Lcom/dropbox/android/activity/z;->c:Ljava/util/Collection;

    .line 143
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/util/DropboxPath;IZ)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 147
    iput p2, p0, Lcom/dropbox/android/activity/z;->b:I

    .line 149
    if-eqz p3, :cond_0

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/z;->c:Ljava/util/Collection;

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/activity/z;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/z;->c:Ljava/util/Collection;

    goto :goto_0
.end method
