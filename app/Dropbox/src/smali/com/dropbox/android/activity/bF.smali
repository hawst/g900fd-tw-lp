.class Lcom/dropbox/android/activity/bF;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Ldbxyzptlk/db231222/z/ae;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Lcom/dropbox/android/util/DropboxPath;

.field private final d:Lcom/dropbox/android/activity/bH;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 239
    const-class v0, Lcom/dropbox/android/activity/bF;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/bF;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/activity/bH;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 253
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 254
    const-class v0, Lcom/dropbox/android/activity/bG;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 255
    sget-object v0, Lcom/dropbox/android/activity/bH;->b:Lcom/dropbox/android/activity/bH;

    if-eq p4, v0, :cond_0

    sget-object v0, Lcom/dropbox/android/activity/bH;->c:Lcom/dropbox/android/activity/bH;

    if-ne p4, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 257
    iput-object p2, p0, Lcom/dropbox/android/activity/bF;->b:Ldbxyzptlk/db231222/z/M;

    .line 258
    iput-object p3, p0, Lcom/dropbox/android/activity/bF;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 259
    iput-object p4, p0, Lcom/dropbox/android/activity/bF;->d:Lcom/dropbox/android/activity/bH;

    .line 260
    iput-object p5, p0, Lcom/dropbox/android/activity/bF;->e:Ljava/lang/String;

    .line 262
    invoke-virtual {p0}, Lcom/dropbox/android/activity/bF;->f()V

    .line 263
    const v0, 0x7f0d00c6

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 264
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 265
    return-void

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 237
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bF;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lcom/dropbox/android/activity/bF;->d:Lcom/dropbox/android/activity/bH;

    sget-object v1, Lcom/dropbox/android/activity/bH;->b:Lcom/dropbox/android/activity/bH;

    if-ne v0, v1, :cond_0

    const-string v0, "preview"

    .line 270
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/bF;->b:Ldbxyzptlk/db231222/z/M;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/dropbox/android/util/DropboxPath;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/activity/bF;->c:Lcom/dropbox/android/util/DropboxPath;

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/bF;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 269
    :cond_0
    const-string v0, "direct"

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 295
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 298
    instance-of v0, p2, Ldbxyzptlk/db231222/w/d;

    if-eqz v0, :cond_0

    .line 299
    const v0, 0x7f0d005b

    .line 303
    :goto_0
    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 305
    sget-object v0, Lcom/dropbox/android/activity/bF;->a:Ljava/lang/String;

    const-string v1, "Error in GetLinkAsyncTask"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 306
    return-void

    .line 301
    :cond_0
    const v0, 0x7f0d0240

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 237
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bF;->a(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/ae;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 275
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 276
    instance-of v0, p1, Lcom/dropbox/android/activity/bG;

    if-eqz v0, :cond_0

    .line 277
    check-cast p1, Lcom/dropbox/android/activity/bG;

    .line 278
    sget-object v0, Lcom/dropbox/android/activity/bD;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/bF;->d:Lcom/dropbox/android/activity/bH;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 280
    :pswitch_0
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/ae;

    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/bG;->a(Ldbxyzptlk/db231222/z/ae;)V

    goto :goto_0

    .line 283
    :pswitch_1
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/ae;

    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/bG;->b(Ldbxyzptlk/db231222/z/ae;)V

    goto :goto_0

    .line 288
    :pswitch_2
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
