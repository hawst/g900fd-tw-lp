.class final Lcom/dropbox/android/activity/dialog/h;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/g/N;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/h;->a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/support/v4/app/DialogFragment;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    return-object v0
.end method

.method private c(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/h;->a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/FragmentActivity;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/h;->c(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->d()V

    .line 114
    return-void
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/h;->c(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v0

    .line 97
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/dialog/h;->a(Landroid/support/v4/app/DialogFragment;)Landroid/app/AlertDialog;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 101
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/dropbox/android/activity/bP;

    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/bP;

    .line 108
    :goto_0
    invoke-interface {v0, p2}, Lcom/dropbox/android/activity/bP;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 109
    return-void

    .line 103
    :cond_0
    instance-of v0, p1, Lcom/dropbox/android/activity/bP;

    if-eqz v0, :cond_1

    .line 104
    check-cast p1, Lcom/dropbox/android/activity/bP;

    move-object v0, p1

    goto :goto_0

    .line 106
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Target fragment or activity must be of type DropboxHierarchicalDirBrowserInterface"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/g/O;)V
    .locals 5

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/h;->c(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v2

    .line 75
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/dialog/h;->a(Landroid/support/v4/app/DialogFragment;)Landroid/app/AlertDialog;

    move-result-object v1

    .line 76
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .line 77
    const v0, 0x7f070115

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 78
    const v4, 0x7f070116

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 80
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 81
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 83
    sget-object v0, Ldbxyzptlk/db231222/g/O;->c:Ldbxyzptlk/db231222/g/O;

    if-eq p2, v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/h;->a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-static {v0, p2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Ldbxyzptlk/db231222/g/O;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 85
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080043

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->f:Lcom/dropbox/android/activity/dialog/o;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/h;->a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->e()Z

    move-result v1

    invoke-static {v0, v2, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Lcom/dropbox/android/activity/dialog/o;Lcom/dropbox/android/activity/dialog/p;Z)Lcom/dropbox/android/activity/dialog/OverQuotaDialog;

    move-result-object v0

    invoke-virtual {v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/FragmentActivity;)V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/h;->c(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c()V

    .line 119
    return-void
.end method
