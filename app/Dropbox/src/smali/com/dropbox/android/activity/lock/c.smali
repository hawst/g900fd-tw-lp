.class abstract enum Lcom/dropbox/android/activity/lock/c;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/lock/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/lock/c;

.field private static final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/dropbox/android/activity/lock/c;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic d:[Lcom/dropbox/android/activity/lock/c;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 213
    new-instance v0, Lcom/dropbox/android/activity/lock/d;

    const-string v1, "DIALOG_UNLINK_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/lock/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/lock/c;->a:Lcom/dropbox/android/activity/lock/c;

    .line 212
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dropbox/android/activity/lock/c;

    sget-object v1, Lcom/dropbox/android/activity/lock/c;->a:Lcom/dropbox/android/activity/lock/c;

    aput-object v1, v0, v2

    sput-object v0, Lcom/dropbox/android/activity/lock/c;->d:[Lcom/dropbox/android/activity/lock/c;

    .line 221
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/lock/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 227
    const-class v0, Lcom/dropbox/android/activity/lock/c;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/c;

    .line 228
    sget-object v2, Lcom/dropbox/android/activity/lock/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/c;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 233
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/c;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/dropbox/android/activity/lock/c;->c:I

    .line 234
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/lock/a;)V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/lock/c;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/dropbox/android/activity/lock/c;
    .locals 2

    .prologue
    .line 240
    sget-object v0, Lcom/dropbox/android/activity/lock/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/c;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/lock/c;
    .locals 1

    .prologue
    .line 212
    const-class v0, Lcom/dropbox/android/activity/lock/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/lock/c;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/lock/c;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/dropbox/android/activity/lock/c;->d:[Lcom/dropbox/android/activity/lock/c;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/lock/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/lock/c;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/dropbox/android/activity/lock/c;->c:I

    return v0
.end method

.method abstract a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/app/Dialog;
.end method
