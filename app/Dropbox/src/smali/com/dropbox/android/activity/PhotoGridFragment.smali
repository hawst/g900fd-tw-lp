.class public Lcom/dropbox/android/activity/PhotoGridFragment;
.super Lcom/dropbox/android/activity/PhotoGridFragmentBase;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dialog/e;


# instance fields
.field final a:Lcom/actionbarsherlock/view/ActionMode$Callback;

.field private p:Ljava/lang/String;

.field private q:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Lcom/dropbox/android/albums/h;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcom/dropbox/android/util/as;

.field private final t:Ldbxyzptlk/db231222/n/z;

.field private u:Landroid/view/View;

.field private v:Z

.field private w:Lcom/actionbarsherlock/view/ActionMode;

.field private x:Z

.field private y:Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

.field private z:Lcom/dropbox/android/albums/u;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;-><init>()V

    .line 87
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    .line 89
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->q:Lcom/dropbox/android/albums/o;

    .line 92
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->r:Lcom/dropbox/android/albums/o;

    .line 96
    new-instance v0, Lcom/dropbox/android/activity/dj;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dj;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->t:Ldbxyzptlk/db231222/n/z;

    .line 183
    iput-boolean v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    .line 184
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    .line 190
    iput-boolean v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->x:Z

    .line 380
    new-instance v0, Lcom/dropbox/android/activity/do;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/do;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->a:Lcom/actionbarsherlock/view/ActionMode$Callback;

    .line 490
    return-void
.end method

.method private a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;
    .locals 7

    .prologue
    .line 115
    new-instance v0, Lcom/dropbox/android/activity/dl;

    const-string v2, "SIS_KEY_WaitingForAddToAlbum"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->e:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d027d

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/dl;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->q:Lcom/dropbox/android/albums/o;

    .line 144
    new-instance v0, Lcom/dropbox/android/activity/dm;

    const-string v2, "SIS_KEY_WaitingForCreateLws"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d0279

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/dm;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->r:Lcom/dropbox/android/albums/o;

    .line 177
    new-instance v0, Lcom/dropbox/android/util/as;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/dropbox/android/util/at;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->q:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->r:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/as;-><init>([Lcom/dropbox/android/util/at;)V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoGridFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->l()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->i()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->m()V

    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->j()V

    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->k()V

    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->n()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/PhotoGridFragment;)Lcom/dropbox/android/albums/u;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/activity/PhotoGridFragment;)Lcom/dropbox/android/albums/o;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->r:Lcom/dropbox/android/albums/o;

    return-object v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 299
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 300
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    .line 301
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 302
    iget-object v0, v2, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/fF;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/dropbox/android/activity/fF;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    .line 304
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v3

    sget-object v4, Lcom/dropbox/android/taskqueue/y;->c:Lcom/dropbox/android/taskqueue/y;

    if-eq v3, v4, :cond_6

    .line 305
    :cond_0
    if-eqz v0, :cond_1

    .line 306
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v3

    sget-object v4, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v3, v4, :cond_5

    .line 307
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->l()V

    .line 312
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v3, "DELETE_STATUS_FRAG_TAG"

    invoke-static {v0, v3}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, v2, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/albums/t;->b(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 316
    :cond_2
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    .line 317
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    .line 326
    :cond_3
    :goto_2
    return-void

    :cond_4
    move-object v0, v1

    .line 303
    goto :goto_0

    .line 308
    :cond_5
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v3, Lcom/dropbox/android/taskqueue/y;->b:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v3, :cond_1

    .line 309
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v3, 0x7f0d028d

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/bl;->a(I)V

    goto :goto_1

    .line 320
    :cond_6
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "DELETE_STATUS_FRAG_TAG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_3

    .line 321
    const v0, 0x7f0d027b

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 322
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "DELETE_STATUS_FRAG_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private j()V
    .locals 3

    .prologue
    .line 544
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 545
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 546
    new-instance v1, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    move-result-object v0

    .line 548
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 549
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 598
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    new-instance v0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;-><init>()V

    .line 600
    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 601
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 605
    :goto_0
    return-void

    .line 603
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Lcom/dropbox/android/albums/Album;)V

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 705
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    invoke-virtual {v0}, Lcom/actionbarsherlock/view/ActionMode;->finish()V

    .line 708
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->m()V

    .line 709
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 712
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    .line 713
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 714
    iput-boolean v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    .line 716
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aj()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 717
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 718
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->f()V

    .line 719
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 729
    new-instance v0, Lcom/dropbox/android/activity/dt;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dt;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    .line 745
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 342
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    .line 343
    if-nez v3, :cond_0

    .line 367
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v4, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->h:Lcom/dropbox/android/activity/dE;

    sget-object v5, Lcom/dropbox/android/activity/dE;->a:Lcom/dropbox/android/activity/dE;

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 347
    iget-object v4, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->h:Lcom/dropbox/android/activity/dE;

    sget-object v5, Lcom/dropbox/android/activity/dE;->a:Lcom/dropbox/android/activity/dE;

    if-ne v0, v5, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 348
    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 350
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0183

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 351
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0d0184

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 352
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->d:Landroid/widget/ImageView;

    const v1, 0x7f0200f2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 346
    goto :goto_1

    :cond_2
    move v0, v1

    .line 347
    goto :goto_2

    .line 355
    :cond_3
    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 356
    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->d:Landroid/widget/ImageView;

    const v4, 0x7f0200ee

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 357
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 358
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 360
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 362
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->b:Landroid/widget/TextView;

    const v2, 0x7f0d0161

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 363
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->c:Landroid/widget/TextView;

    const v2, 0x7f0d0245

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 364
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 640
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(IILandroid/content/Intent;)V

    .line 641
    packed-switch p1, :pswitch_data_0

    .line 662
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 643
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 644
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->l()V

    goto :goto_0

    .line 648
    :pswitch_2
    iget-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    if-eqz v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 651
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    iget-object v0, v0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    new-instance v1, Lcom/dropbox/android/activity/ds;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ds;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/d;->a(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 641
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 274
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Landroid/os/Bundle;)V

    .line 276
    const-string v0, "SIS_KEY_IsInMultiSelect"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    .line 277
    iget-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 283
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    .line 332
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 333
    const-string v1, "EXTRA_GALLERY_RAW_QUERY_COUNT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 334
    const-string v1, "EXTRA_GALLERY_RAW_QUERY_COUNT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->x:Z

    .line 337
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 338
    return-void

    .line 334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/albums/Album;)V
    .locals 4

    .prologue
    .line 610
    .line 612
    if-nez p1, :cond_0

    .line 613
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 614
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    iget-object v0, v0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    new-instance v2, Lcom/dropbox/android/activity/dr;

    invoke-direct {v2, p0, v1}, Lcom/dropbox/android/activity/dr;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;Ljava/util/HashSet;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/base/d;->a(Ljava/lang/Runnable;)Z

    .line 636
    :goto_0
    return-void

    .line 624
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Ljava/util/Collection;)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 626
    if-nez v0, :cond_1

    .line 627
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 630
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 631
    const-string v2, "PATH_TO_SCROLL_TO"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 632
    const-string v0, "PATHS_TO_ADD"

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 634
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->q:Lcom/dropbox/android/albums/o;

    new-instance v2, Lcom/dropbox/android/albums/h;

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/dropbox/android/albums/h;-><init>(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 585
    packed-switch p1, :pswitch_data_0

    .line 590
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 587
    :pswitch_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->c()V

    .line 588
    const/4 v0, 0x1

    goto :goto_0

    .line 585
    :pswitch_data_0
    .packed-switch 0x12f
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/dropbox/android/widget/SweetListView;Landroid/view/View;IJ)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 666
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bi;->g()Landroid/database/Cursor;

    move-result-object v0

    .line 667
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 668
    sget-object v2, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v2}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v2

    .line 669
    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v2, v3, :cond_0

    iget-boolean v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    if-nez v3, :cond_0

    .line 670
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Landroid/database/Cursor;)V

    .line 671
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->c()V

    move v0, v1

    .line 677
    :goto_0
    return v0

    .line 673
    :cond_0
    sget-object v0, Lcom/dropbox/android/provider/Z;->d:Lcom/dropbox/android/provider/Z;

    if-eq v2, v0, :cond_1

    sget-object v0, Lcom/dropbox/android/provider/Z;->e:Lcom/dropbox/android/provider/Z;

    if-ne v2, v0, :cond_2

    :cond_1
    move v0, v1

    .line 675
    goto :goto_0

    .line 677
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    .line 372
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/actionbarsherlock/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    invoke-virtual {v0}, Lcom/actionbarsherlock/view/ActionMode;->invalidate()V

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 378
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 689
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    .line 693
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ai()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 694
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->f()V

    .line 695
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->u:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 696
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/actionbarsherlock/app/SherlockFragmentActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->a:Lcom/actionbarsherlock/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/SherlockFragmentActivity;->startActionMode(Lcom/actionbarsherlock/view/ActionMode$Callback;)Lcom/actionbarsherlock/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->w:Lcom/actionbarsherlock/view/ActionMode;

    .line 698
    return-void
.end method

.method protected final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 749
    sget-object v0, Lcom/dropbox/android/provider/PhotosProvider;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 200
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onCreate(Landroid/os/Bundle;)V

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->setHasOptionsMenu(Z)V

    .line 204
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 205
    if-nez v0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 210
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->n()V

    .line 211
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 212
    new-instance v1, Lcom/dropbox/android/activity/dn;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dn;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Ljava/lang/Runnable;)Z

    .line 220
    :cond_1
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 221
    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->t:Ldbxyzptlk/db231222/n/z;

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/z;)V

    .line 222
    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->t:Ldbxyzptlk/db231222/n/z;

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/P;->b(Ldbxyzptlk/db231222/n/z;)V

    .line 224
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->s:Lcom/dropbox/android/util/as;

    .line 225
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->s:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .locals 6

    .prologue
    const v1, 0x7f0d0098

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 553
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V

    .line 554
    iget-boolean v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->x:Z

    if-eqz v0, :cond_0

    .line 556
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f08008c

    const v3, 0x7f020140

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;IIIZZ)Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->y:Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    .line 563
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->y:Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    new-instance v2, Lcom/dropbox/android/activity/dq;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dq;-><init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 569
    const/16 v0, 0x12f

    invoke-interface {p1, v5, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->y:Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setActionView(Landroid/view/View;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 573
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 231
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 233
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Landroid/view/View;Z)V

    .line 234
    const v0, 0x7f0700ce

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->u:Landroid/view/View;

    .line 235
    return-object v1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 254
    invoke-super {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onDestroy()V

    .line 256
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    if-eqz v1, :cond_1

    .line 258
    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/albums/PhotosModel;->b:Lcom/dropbox/android/albums/t;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->z:Lcom/dropbox/android/albums/u;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/albums/t;->b(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 262
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    .line 264
    :cond_1
    if-eqz v0, :cond_2

    .line 265
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->s:Lcom/dropbox/android/util/as;

    invoke-virtual {v1}, Lcom/dropbox/android/util/as;->a()V

    .line 266
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->t:Ldbxyzptlk/db231222/n/z;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->c(Ldbxyzptlk/db231222/n/z;)V

    .line 268
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->t:Ldbxyzptlk/db231222/n/z;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->d(Ldbxyzptlk/db231222/n/z;)V

    .line 270
    :cond_2
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 194
    invoke-super {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onDetach()V

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->y:Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    .line 196
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 67
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 577
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    const/4 v0, 0x1

    .line 580
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 287
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 288
    const-string v0, "SIS_KEY_WaitingFoDeleteId"

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v0, "SIS_KEY_IsInMultiSelect"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment;->s:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->b(Landroid/os/Bundle;)V

    .line 292
    return-void
.end method
