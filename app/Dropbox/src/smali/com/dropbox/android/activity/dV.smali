.class final Lcom/dropbox/android/activity/dV;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/n/P;

.field final synthetic b:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/dropbox/android/activity/dV;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/activity/dV;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/PrefsActivity;->b(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/i;->a(Z)Z

    .line 214
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aI()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 228
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dV;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->a(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V

    .line 229
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->l()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "cameraupload.enabled"

    iget-object v2, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 230
    return v4

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->K()Z

    move-result v0

    if-nez v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/dropbox/android/activity/dV;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/dZ;->a(Lcom/dropbox/android/activity/PrefsActivity;)V

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->B()Z

    move-result v0

    .line 223
    iget-object v1, p0, Lcom/dropbox/android/activity/dV;->a:Ldbxyzptlk/db231222/n/P;

    iget-object v2, p0, Lcom/dropbox/android/activity/dV;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/PrefsActivity;->b(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v2

    invoke-virtual {v1, v4, v3, v0, v2}, Ldbxyzptlk/db231222/n/P;->a(ZZZLcom/dropbox/android/service/i;)V

    goto :goto_0
.end method
