.class final Lcom/dropbox/android/activity/payment/w;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/payments/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;


# direct methods
.method private constructor <init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Lcom/dropbox/android/activity/payment/p;)V
    .locals 0

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/payment/w;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->g(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->d()V

    .line 363
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/payments/u;)V
    .locals 2

    .prologue
    .line 367
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bz()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 368
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Z)Z

    .line 370
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->h(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 372
    return-void
.end method

.method public final a(Lcom/dropbox/android/payments/x;)V
    .locals 4

    .prologue
    .line 376
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bw()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 377
    const-string v1, "source"

    iget-object v2, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->j(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Lcom/dropbox/android/activity/payment/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/activity/payment/h;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 378
    new-instance v0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    sget-object v1, Lcom/dropbox/android/payments/g;->b:Lcom/dropbox/android/payments/g;

    invoke-virtual {p1}, Lcom/dropbox/android/payments/x;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;-><init>(Lcom/dropbox/android/payments/g;Ljava/lang/String;)V

    .line 379
    new-instance v1, Lcom/dropbox/android/payments/d;

    iget-object v2, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->l(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v3

    invoke-direct {v1, v0, p1, v2, v3}, Lcom/dropbox/android/payments/d;-><init>(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Lcom/dropbox/android/payments/x;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/service/a;)V

    .line 381
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->m(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    .line 382
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bx()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 398
    const-string v1, "data_values"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 399
    return-void
.end method

.method public final b(Lcom/dropbox/android/payments/u;)V
    .locals 3

    .prologue
    .line 386
    invoke-virtual {p1}, Lcom/dropbox/android/payments/u;->a()I

    move-result v0

    const/16 v1, -0x3ed

    if-ne v0, v1, :cond_0

    .line 388
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bv()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 393
    :goto_0
    return-void

    .line 391
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->by()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 392
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02ee

    const v2, 0x7f0d02ef

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;II)V

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/payments/x;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 407
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 408
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_1

    .line 409
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bA()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    .line 410
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 411
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/payments/x;

    .line 412
    invoke-virtual {v0}, Lcom/dropbox/android/payments/x;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 414
    :cond_0
    const-string v0, "sub_names"

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 418
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/payments/x;

    .line 419
    new-instance v2, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    sget-object v3, Lcom/dropbox/android/payments/g;->b:Lcom/dropbox/android/payments/g;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/x;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;-><init>(Lcom/dropbox/android/payments/g;Ljava/lang/String;)V

    .line 420
    new-instance v3, Lcom/dropbox/android/payments/d;

    iget-object v4, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v4}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->n(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v5}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->o(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v5

    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v5

    invoke-direct {v3, v2, v0, v4, v5}, Lcom/dropbox/android/payments/d;-><init>(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Lcom/dropbox/android/payments/x;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/service/a;)V

    .line 422
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->m(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    goto :goto_1

    .line 426
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/y;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/payment/y;->b(Z)V

    .line 427
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0, v6}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->c(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Z)Z

    .line 428
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/w;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    .line 429
    return-void
.end method

.method public final c(Lcom/dropbox/android/payments/u;)V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method public final d(Lcom/dropbox/android/payments/u;)V
    .locals 1

    .prologue
    .line 433
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bz()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 434
    return-void
.end method
