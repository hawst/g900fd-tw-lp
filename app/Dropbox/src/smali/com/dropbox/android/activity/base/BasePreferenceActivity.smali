.class public Lcom/dropbox/android/activity/base/BasePreferenceActivity;
.super Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/base/h;


# instance fields
.field protected a:Lcom/dropbox/android/activity/base/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;-><init>()V

    .line 15
    new-instance v0, Lcom/dropbox/android/activity/base/d;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/base/d;-><init>(Lcom/dropbox/android/activity/base/h;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    return-void
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public final a_()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 93
    return-object p0
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/activity/base/d;->a(IILandroid/content/Intent;)V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Landroid/os/Bundle;)V

    .line 21
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onDestroy()V

    .line 56
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->f()V

    .line 57
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 98
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->finish()V

    .line 100
    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onPause()V

    .line 44
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->e()V

    .line 45
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onResume()V

    .line 38
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->c()V

    .line 39
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->b(Landroid/os/Bundle;)V

    .line 27
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onStart()V

    .line 32
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->b()V

    .line 33
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->onStop()V

    .line 50
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->d()V

    .line 51
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Landroid/content/Intent;)V

    .line 63
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/droidfu/BetterPreferenceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 68
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->a:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Landroid/content/Intent;)V

    .line 69
    return-void
.end method
