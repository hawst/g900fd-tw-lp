.class public Lcom/dropbox/android/activity/VideoPlayerActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/N;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/net/Uri;

.field private e:Lcom/dropbox/android/widget/DbxVideoView;

.field private f:Lcom/dropbox/android/widget/DbxMediaController;

.field private g:Landroid/widget/ProgressBar;

.field private h:Lcom/dropbox/android/activity/fW;

.field private i:I

.field private j:Z

.field private final k:Landroid/os/Handler;

.field private l:J

.field private final m:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    .line 83
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    .line 84
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    .line 85
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Landroid/widget/ProgressBar;

    .line 88
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:Lcom/dropbox/android/activity/fW;

    .line 96
    iput v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    .line 101
    iput-boolean v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Landroid/os/Handler;

    .line 110
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->l:J

    .line 113
    new-instance v0, Lcom/dropbox/android/activity/fM;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fM;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->m:Ljava/lang/Runnable;

    .line 458
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 164
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting duration to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->setDuration(I)V

    .line 168
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/VideoPlayerActivity;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/VideoPlayerActivity;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/service/J;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->m()Lcom/dropbox/android/service/J;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/util/analytics/ChainInfo;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->k()Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/VideoPlayerActivity;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->l:J

    return-wide v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 366
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 367
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    const-string v1, "Showing spinner."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->l:J

    .line 369
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 370
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 374
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 375
    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/VideoPlayerActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setSystemUiVisibility(I)V

    .line 451
    return-void
.end method

.method static synthetic h(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->g()V

    return-void
.end method

.method static synthetic j(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->f()V

    return-void
.end method

.method private k()Lcom/dropbox/android/util/analytics/ChainInfo;
    .locals 2

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 527
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ANALYTICS_CHAIN_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/analytics/ChainInfo;

    .line 529
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic k(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->e()V

    return-void
.end method

.method static synthetic l(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxMediaController;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    return-object v0
.end method

.method private m()Lcom/dropbox/android/service/J;
    .locals 1

    .prologue
    .line 538
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->c()Lcom/dropbox/android/service/J;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 173
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 175
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    .line 178
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->finish()V

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    const v0, 0x7f0300d6

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->setContentView(I)V

    .line 185
    const v0, 0x7f0701cd

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Landroid/widget/ProgressBar;

    .line 186
    const v0, 0x7f0701cc

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/DbxVideoView;

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    .line 189
    const-string v0, "EXTRA_CONTAINER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 191
    const-string v0, "EXTRA_PROGRESS_URL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 195
    if-eqz v5, :cond_5

    .line 196
    iput-boolean v7, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    .line 201
    :goto_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0, v7}, Lcom/dropbox/android/widget/DbxVideoView;->setAllowSeek(Z)V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v1, Lcom/dropbox/android/activity/fO;

    invoke-direct {v1, p0, v6}, Lcom/dropbox/android/activity/fO;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 220
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v1, Lcom/dropbox/android/activity/fP;

    invoke-direct {v1, p0, v6}, Lcom/dropbox/android/activity/fP;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v1, Lcom/dropbox/android/activity/fQ;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/fQ;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 249
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v1, Lcom/dropbox/android/activity/fR;

    invoke-direct {v1, p0, v6}, Lcom/dropbox/android/activity/fR;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 262
    new-instance v0, Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    move-object v1, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/DbxMediaController;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;ZLcom/dropbox/android/widget/N;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    .line 264
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    new-instance v1, Lcom/dropbox/android/activity/fS;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/fS;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->setOnHideListener(Lcom/dropbox/android/widget/P;)V

    .line 271
    iget-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    if-eqz v0, :cond_3

    .line 272
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    new-instance v1, Lcom/dropbox/android/activity/fT;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/fT;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->setOnUserSeekListener(Lcom/dropbox/android/widget/R;)V

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    new-instance v1, Lcom/dropbox/android/activity/fU;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/fU;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->setOnPlayPauseListener(Lcom/dropbox/android/widget/Q;)V

    .line 311
    new-instance v0, Lcom/dropbox/android/activity/fV;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fV;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    .line 318
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v2, Lcom/dropbox/android/activity/fN;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/fN;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DbxVideoView;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 335
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_METADATA_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_4

    .line 340
    new-instance v1, Lcom/dropbox/android/activity/fW;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/dropbox/android/activity/fW;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:Lcom/dropbox/android/activity/fW;

    .line 341
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:Lcom/dropbox/android/activity/fW;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/fW;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 344
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setMediaController(Lcom/dropbox/android/widget/DbxMediaController;)V

    .line 346
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 347
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 349
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->H()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "host"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "://"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "can_seek"

    iget-boolean v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "container"

    invoke-virtual {v0, v1, v6}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->k()Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->m()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 356
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "START_POSITION"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    goto/16 :goto_0

    .line 198
    :cond_5
    const-string v0, "EXTRA_CAN_SEEK"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 433
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onDestroy()V

    .line 434
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:Lcom/dropbox/android/activity/fW;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:Lcom/dropbox/android/activity/fW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/fW;->cancel(Z)Z

    .line 438
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:Lcom/dropbox/android/activity/fW;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/fW;->a()V

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->j()V

    .line 441
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 397
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onPause()V

    .line 399
    iget-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Z

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->l()V

    .line 403
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->f()V

    .line 406
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->e()V

    .line 407
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 411
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 412
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->f()V

    .line 413
    iget v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    if-lez v0, :cond_0

    .line 414
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resuming at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    iget v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(I)V

    .line 416
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->m()V

    .line 421
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->e()V

    .line 427
    :goto_1
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->g()V

    .line 428
    return-void

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->e:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    goto :goto_0

    .line 424
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    const-string v1, "Paused, no spinner."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 445
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 446
    const-string v0, "START_POSITION"

    iget v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 447
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 379
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onStart()V

    .line 380
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 382
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 386
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onStop()V

    .line 387
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 392
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ANALYTICS_CHAIN_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 393
    return-void
.end method
