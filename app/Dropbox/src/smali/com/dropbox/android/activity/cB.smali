.class public final enum Lcom/dropbox/android/activity/cB;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/cB;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/cB;

.field public static final enum b:Lcom/dropbox/android/activity/cB;

.field public static final enum c:Lcom/dropbox/android/activity/cB;

.field public static final enum d:Lcom/dropbox/android/activity/cB;

.field public static final enum e:Lcom/dropbox/android/activity/cB;

.field public static final enum f:Lcom/dropbox/android/activity/cB;

.field public static final enum g:Lcom/dropbox/android/activity/cB;

.field public static final enum h:Lcom/dropbox/android/activity/cB;

.field public static final enum i:Lcom/dropbox/android/activity/cB;

.field public static final enum j:Lcom/dropbox/android/activity/cB;

.field private static final synthetic k:[Lcom/dropbox/android/activity/cB;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 478
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "DROPBOX_UNSPECIFIED"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    .line 482
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "OOBE_LOGIN"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->b:Lcom/dropbox/android/activity/cB;

    .line 486
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "OOBE_SIGNUP"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->c:Lcom/dropbox/android/activity/cB;

    .line 490
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "HTC_TOKEN"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->d:Lcom/dropbox/android/activity/cB;

    .line 494
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "SAMSUNG_LOGIN"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->e:Lcom/dropbox/android/activity/cB;

    .line 498
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "SAMSUNG_SIGN_UP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->f:Lcom/dropbox/android/activity/cB;

    .line 499
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "WEB_SESSION_TOKEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->g:Lcom/dropbox/android/activity/cB;

    .line 503
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "STANDARD_OOBE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->h:Lcom/dropbox/android/activity/cB;

    .line 507
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "DROPBOX_LOGIN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->i:Lcom/dropbox/android/activity/cB;

    .line 511
    new-instance v0, Lcom/dropbox/android/activity/cB;

    const-string v1, "DROPBOX_SIGNUP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/cB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/cB;->j:Lcom/dropbox/android/activity/cB;

    .line 474
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/cB;->b:Lcom/dropbox/android/activity/cB;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/cB;->c:Lcom/dropbox/android/activity/cB;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/cB;->d:Lcom/dropbox/android/activity/cB;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/activity/cB;->e:Lcom/dropbox/android/activity/cB;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/activity/cB;->f:Lcom/dropbox/android/activity/cB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/activity/cB;->g:Lcom/dropbox/android/activity/cB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/activity/cB;->h:Lcom/dropbox/android/activity/cB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/activity/cB;->i:Lcom/dropbox/android/activity/cB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/android/activity/cB;->j:Lcom/dropbox/android/activity/cB;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/activity/cB;->k:[Lcom/dropbox/android/activity/cB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 474
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/activity/cB;)Lcom/dropbox/android/activity/cB;
    .locals 1

    .prologue
    .line 514
    const-string v0, "com.dropbox.intent.action.LOGIN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 515
    sget-object p1, Lcom/dropbox/android/activity/cB;->b:Lcom/dropbox/android/activity/cB;

    .line 533
    :cond_0
    :goto_0
    return-object p1

    .line 516
    :cond_1
    const-string v0, "com.dropbox.intent.action.SIGN_UP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 517
    sget-object p1, Lcom/dropbox/android/activity/cB;->c:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 518
    :cond_2
    const-string v0, "com.dropbox.intent.action.HTC_TOKEN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 519
    sget-object p1, Lcom/dropbox/android/activity/cB;->d:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 520
    :cond_3
    const-string v0, "com.dropbox.intent.action.SAMSUNG_LOGIN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 521
    sget-object p1, Lcom/dropbox/android/activity/cB;->e:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 522
    :cond_4
    const-string v0, "com.dropbox.intent.action.SAMSUNG_SIGN_UP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 523
    sget-object p1, Lcom/dropbox/android/activity/cB;->f:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 524
    :cond_5
    const-string v0, "com.dropbox.intent.action.WEB_SESSION_TOKEN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 525
    sget-object p1, Lcom/dropbox/android/activity/cB;->g:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 526
    :cond_6
    const-string v0, "com.dropbox.intent.action.STD_OOBE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 527
    sget-object p1, Lcom/dropbox/android/activity/cB;->h:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 528
    :cond_7
    const-string v0, "com.dropbox.intent.action.DROPBOX_LOGIN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 529
    sget-object p1, Lcom/dropbox/android/activity/cB;->i:Lcom/dropbox/android/activity/cB;

    goto :goto_0

    .line 530
    :cond_8
    const-string v0, "com.dropbox.intent.action.DROPBOX_SIGNUP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    sget-object p1, Lcom/dropbox/android/activity/cB;->j:Lcom/dropbox/android/activity/cB;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/cB;
    .locals 1

    .prologue
    .line 474
    const-class v0, Lcom/dropbox/android/activity/cB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/cB;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/cB;
    .locals 1

    .prologue
    .line 474
    sget-object v0, Lcom/dropbox/android/activity/cB;->k:[Lcom/dropbox/android/activity/cB;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/cB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/cB;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 540
    sget-object v0, Lcom/dropbox/android/activity/cA;->a:[I

    invoke-virtual {p0}, Lcom/dropbox/android/activity/cB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 546
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 544
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 540
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 554
    sget-object v0, Lcom/dropbox/android/activity/cA;->a:[I

    invoke-virtual {p0}, Lcom/dropbox/android/activity/cB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 563
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 561
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 554
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 572
    sget-object v0, Lcom/dropbox/android/activity/cA;->a:[I

    invoke-virtual {p0}, Lcom/dropbox/android/activity/cB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 578
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 576
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 572
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method
