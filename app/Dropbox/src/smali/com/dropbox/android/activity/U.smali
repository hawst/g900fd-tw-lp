.class final Lcom/dropbox/android/activity/U;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/n/P;

.field final synthetic b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Ldbxyzptlk/db231222/n/P;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/U;->a:Ldbxyzptlk/db231222/n/P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 180
    iget-object v0, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CompoundButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 185
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Enabled."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Use 3g: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Upload existing: true"

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v2, p0, Lcom/dropbox/android/activity/U;->a:Ldbxyzptlk/db231222/n/P;

    iget-object v3, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/RadioButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/n/P;->j(Z)V

    .line 189
    iget-object v2, p0, Lcom/dropbox/android/activity/U;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2, v5}, Ldbxyzptlk/db231222/n/P;->n(Z)V

    .line 190
    iget-object v2, p0, Lcom/dropbox/android/activity/U;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2, v6}, Ldbxyzptlk/db231222/n/P;->k(Z)V

    .line 191
    iget-object v2, p0, Lcom/dropbox/android/activity/U;->a:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/n/P;->l(Z)V

    .line 192
    invoke-virtual {v0, v6}, Lcom/dropbox/android/service/i;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aH()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 201
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/U;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->finish()V

    .line 202
    return-void

    .line 196
    :cond_1
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Disabled."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, v5}, Lcom/dropbox/android/service/i;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aI()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method
