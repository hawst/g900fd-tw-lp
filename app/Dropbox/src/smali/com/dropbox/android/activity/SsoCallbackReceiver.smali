.class public Lcom/dropbox/android/activity/SsoCallbackReceiver;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# static fields
.field public static a:Ljava/lang/String;

.field private static e:Z


# instance fields
.field public b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->b:Z

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 87
    const-string v1, "dbx-sso"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 91
    if-nez v0, :cond_2

    .line 92
    sput-object v3, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    .line 109
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->finish()V

    goto :goto_0

    .line 94
    :cond_2
    const-string v1, "not_approved"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    const-string v2, "true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 96
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->n()Lcom/dropbox/android/filemanager/h;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_1

    .line 99
    iget-object v0, v0, Lcom/dropbox/android/filemanager/h;->b:Ldbxyzptlk/db231222/y/m;

    iget-object v0, v0, Ldbxyzptlk/db231222/y/m;->a:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 100
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/n/k;->a(Lcom/dropbox/android/filemanager/h;)V

    .line 101
    sput-object v3, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    goto :goto_1

    .line 104
    :cond_3
    const-string v1, "oauth_verifier"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    .line 105
    const-string v1, "oauth_token"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 150
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    const-string v3, "dbx-sso://"

    .line 152
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 154
    invoke-virtual {v3, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 158
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 159
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v2, :cond_3

    .line 162
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 163
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 175
    :goto_1
    return v0

    .line 170
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 172
    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    move v0, v1

    .line 173
    goto :goto_1

    :cond_4
    move v0, v2

    .line 175
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 59
    invoke-static {p0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0

    .line 63
    :cond_0
    sput-boolean v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->e:Z

    .line 64
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/SsoCallbackReceiver;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    const-string v2, "AUTH_URL"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    if-eqz p1, :cond_0

    .line 79
    const-string v0, "LAUNCH_INTENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->b:Z

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 82
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a(Landroid/content/Intent;)V

    .line 83
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a(Landroid/content/Intent;)V

    .line 73
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onResume()V

    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->b:Z

    if-eqz v0, :cond_2

    .line 129
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->a(Lcom/dropbox/android/filemanager/h;)V

    .line 130
    sput-object v1, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    .line 131
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->finish()V

    goto :goto_0

    .line 132
    :cond_2
    sget-boolean v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->e:Z

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->e:Z

    .line 134
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "AUTH_URL"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 137
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->startActivity(Landroid/content/Intent;)V

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->b:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 145
    const-string v0, "LAUNCH_INTENT"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 146
    return-void
.end method
