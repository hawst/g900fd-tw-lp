.class public Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/dropbox/android/service/a;

.field private final d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 39
    new-instance v0, Lcom/dropbox/android/activity/payment/j;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/j;-><init>(Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 67
    return-void
.end method

.method public static a(Z)Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;
    .locals 3

    .prologue
    .line 102
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 103
    const-string v1, "payment_plan_details_hide_pricing"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 105
    new-instance v1, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;-><init>()V

    .line 106
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 107
    return-object v1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;)Lcom/dropbox/android/service/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->c:Lcom/dropbox/android/service/a;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;Ldbxyzptlk/db231222/s/a;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a(Ldbxyzptlk/db231222/s/a;)V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/s/a;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 111
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f0d02e3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 117
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v1

    .line 119
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v3

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v5

    add-long/2addr v3, v5

    .line 121
    cmp-long v0, v3, v1

    if-lez v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f0d02e4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 123
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 144
    const v0, 0x7f03006a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 146
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 147
    if-nez v0, :cond_0

    move-object v0, v1

    .line 178
    :goto_0
    return-object v0

    .line 153
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->j()Z

    move-result v2

    invoke-static {v2}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 155
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->c:Lcom/dropbox/android/service/a;

    .line 157
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->f()Ljava/lang/String;

    move-result-object v2

    .line 159
    const v0, 0x7f070134

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a:Landroid/widget/TextView;

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const v0, 0x7f070133

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->b:Landroid/widget/TextView;

    .line 163
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->c:Lcom/dropbox/android/service/a;

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_1

    .line 165
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a(Ldbxyzptlk/db231222/s/a;)V

    .line 168
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "payment_plan_details_hide_pricing"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 173
    if-eqz v0, :cond_2

    .line 174
    const v0, 0x7f070135

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    move-object v0, v1

    .line 178
    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 132
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onResume()V

    .line 135
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/BaseActivity;->setSupportProgressBarIndeterminateVisibility(Z)V

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 137
    return-void
.end method
