.class public Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;
.super Lcom/dropbox/android/activity/DropboxWebViewActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private e:Lcom/dropbox/android/activity/payment/g;

.field private final f:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;-><init>()V

    .line 58
    new-instance v0, Lcom/dropbox/android/activity/payment/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/a;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->f:Landroid/os/Handler;

    .line 460
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b:Ljava/lang/String;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;Z)V
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;ZLjava/lang/String;)V

    .line 402
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 413
    invoke-static {p0, p1, p2, p3}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 414
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 415
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->h()V

    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 421
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 422
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 423
    const-string v1, "EXTRA_TITLE"

    const v2, 0x7f0d02d9

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 425
    const-string v1, "EXTRA_REQUIRES_AUTH"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 426
    const-string v1, "EXTRA_HAS_SPINNER"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 428
    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 437
    invoke-virtual {p1}, Lcom/dropbox/android/activity/payment/h;->a()Lcom/dropbox/android/util/bz;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bz;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 438
    invoke-static {}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    const-string v1, "card_scanner"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 441
    :cond_0
    if-eqz p3, :cond_1

    .line 442
    const-string v1, "discount_code"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 444
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 446
    if-eqz p2, :cond_2

    .line 447
    invoke-static {p0, v1}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 452
    :goto_0
    return-object v0

    .line 450
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 451
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->d()Landroid/webkit/WebView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static g()Z
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lio/card/payment/CardIOActivity;->canReadCardWithCamera()Z

    move-result v0

    return v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/dropbox/android/activity/payment/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/b;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)V

    invoke-virtual {v0}, Lcom/dropbox/android/activity/payment/b;->start()V

    .line 184
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 354
    packed-switch p1, :pswitch_data_0

    .line 385
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 356
    :pswitch_0
    sget v0, Lio/card/payment/CardIOActivity;->RESULT_CARD_INFO:I

    if-ne p2, v0, :cond_2

    .line 357
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bq()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 368
    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    const-string v0, "io.card.payment.scanResult"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    const-string v0, "io.card.payment.scanResult"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lio/card/payment/CreditCard;

    .line 373
    iget-object v0, v0, Lio/card/payment/CreditCard;->cardNumber:Ljava/lang/String;

    .line 375
    const-string v1, "[0-9]*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The string passed from card.io was not all digits: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "[0-9]"

    const-string v4, "*"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 379
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->e:Lcom/dropbox/android/activity/payment/g;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/payment/g;->a(Ljava/lang/String;)V

    .line 381
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bq()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 387
    :cond_1
    return-void

    .line 358
    :cond_2
    if-nez p2, :cond_3

    .line 359
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bm()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 360
    :cond_3
    sget v0, Lio/card/payment/CardIOActivity;->RESULT_SCAN_NOT_AVAILABLE:I

    if-ne p2, v0, :cond_4

    .line 361
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bn()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 362
    :cond_4
    sget v0, Lio/card/payment/CardIOActivity;->RESULT_SCAN_SUPPRESSED:I

    if-ne p2, v0, :cond_5

    .line 363
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bp()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 364
    :cond_5
    sget v0, Lio/card/payment/CardIOActivity;->RESULT_ENTRY_CANCELED:I

    if-ne p2, v0, :cond_0

    .line 365
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bo()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    .line 354
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected final e()Landroid/webkit/WebViewClient;
    .locals 1

    .prologue
    .line 391
    new-instance v0, Lcom/dropbox/android/activity/payment/g;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/payment/g;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;Lcom/dropbox/android/activity/DropboxWebViewActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->e:Lcom/dropbox/android/activity/payment/g;

    .line 392
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->e:Lcom/dropbox/android/activity/payment/g;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->finish()V

    .line 226
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->onCreate(Landroid/os/Bundle;)V

    .line 98
    if-eqz p1, :cond_0

    .line 99
    const-string v0, "CARD_IO_JS_CALLBACK_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b:Ljava/lang/String;

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->d()Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/activity/payment/d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/dropbox/android/activity/payment/d;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;Lcom/dropbox/android/activity/payment/a;)V

    const-string v2, "NetworkConnectivity"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->d()Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/activity/payment/i;

    invoke-direct {v1, p0, p0}, Lcom/dropbox/android/activity/payment/i;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;Landroid/app/Activity;)V

    const-string v2, "UpgradeStatusListener"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-static {}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bk()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 117
    new-instance v0, Lcom/dropbox/android/activity/payment/c;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/c;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)V

    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->d()Landroid/webkit/WebView;

    move-result-object v1

    const-string v2, "CardIO"

    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->onResume()V

    .line 211
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "NETWORK_DIALOG_TAG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNetworkDialogFragment;

    .line 214
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    invoke-virtual {v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNetworkDialogFragment;->dismiss()V

    .line 217
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 92
    const-string v0, "CARD_IO_JS_CALLBACK_NAME"

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 230
    invoke-super {p0}, Lcom/dropbox/android/activity/DropboxWebViewActivity;->onStop()V

    .line 234
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->h()V

    .line 235
    return-void
.end method
