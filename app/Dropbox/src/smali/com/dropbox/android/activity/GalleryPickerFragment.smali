.class public Lcom/dropbox/android/activity/GalleryPickerFragment;
.super Lcom/dropbox/android/activity/SweetListFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/SweetListFragment",
        "<",
        "Lcom/dropbox/android/widget/at;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field protected final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field b:I

.field c:I

.field private e:Lcom/dropbox/android/activity/dG;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TextView;

.field private h:Z

.field private i:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/dropbox/android/activity/GalleryPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/GalleryPickerFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/dropbox/android/activity/SweetListFragment;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    .line 179
    iput v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->b:I

    .line 180
    iput v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->c:I

    .line 199
    iput-boolean v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->h:Z

    .line 200
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->i:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryPickerFragment;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->c()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/GalleryPickerFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->i:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 141
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Landroid/widget/Button;

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;I)Ljava/lang/String;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/activity/dG;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/activity/dG;

    invoke-interface {v0}, Lcom/dropbox/android/activity/dG;->d()V

    .line 153
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/activity/dG;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/activity/dG;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/dG;->a(Ljava/util/Set;)V

    .line 120
    :cond_0
    return-void
.end method

.method protected final a(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 204
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 205
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    const-string v1, "_cursor_type_tag"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_tag_video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    const-string v1, "vid_duration"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 208
    const-string v1, "_data"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 212
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 214
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->h:Z

    if-nez v1, :cond_2

    .line 215
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->h:Z

    .line 218
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Lcom/dropbox/android/activity/bx;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/bx;-><init>(Lcom/dropbox/android/activity/GalleryPickerFragment;)V

    invoke-static {v1, v0, v2, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 234
    :cond_2
    return-void
.end method

.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0, p2}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Landroid/database/Cursor;)V

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/at;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/at;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->m:Lcom/dropbox/android/util/aW;

    if-eqz v0, :cond_0

    .line 194
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->m:Lcom/dropbox/android/util/aW;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aW;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedRestorePositionFromTop(I)V

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->m:Lcom/dropbox/android/util/aW;

    .line 197
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/dG;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/activity/dG;

    .line 52
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/SweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 59
    new-instance v0, Lcom/dropbox/android/widget/at;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/widget/at;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    .line 64
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 66
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setSweetAdapter(Lcom/dropbox/android/widget/bC;)V

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->n:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 70
    const v1, 0x7f0d0013

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 71
    new-instance v1, Lcom/dropbox/android/activity/bv;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/bv;-><init>(Lcom/dropbox/android/activity/GalleryPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Landroid/widget/Button;

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070146

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->g:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Landroid/widget/Button;

    const v1, 0x7f0d00e5

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 87
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/bw;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/bw;-><init>(Lcom/dropbox/android/activity/GalleryPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->b()V

    .line 95
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 160
    sget-object v2, Lcom/dropbox/android/provider/ZipperedMediaProvider;->b:Landroid/net/Uri;

    .line 162
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v5, v6

    .line 168
    :goto_0
    new-instance v0, Landroid/support/v4/content/g;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    if-ne v5, v6, :cond_1

    const-string v5, "mini_thumb_path"

    :goto_1
    aput-object v5, v3, v7

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/g;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 162
    :cond_0
    const/4 v0, 0x3

    move v5, v0

    goto :goto_0

    .line 168
    :cond_1
    const-string v5, "micro_thumb_path"

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0, p3}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Landroid/os/Bundle;)V

    .line 110
    const v0, 0x7f03003c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 111
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    .line 112
    return-object v1
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/dropbox/android/activity/SweetListFragment;->onDetach()V

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/at;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/at;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 103
    :cond_0
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/at;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/at;->g()Landroid/database/Cursor;

    move-result-object v0

    .line 126
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 128
    const-string v1, "content_uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/at;

    invoke-virtual {v0, p3, p2}, Lcom/dropbox/android/widget/at;->a(ILandroid/view/View;)V

    .line 137
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->b()V

    .line 138
    return-void

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/at;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/at;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 242
    return-void
.end method
