.class public Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method private a()Landroid/content/Context;
    .locals 3

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_IS_LIGHT_THEME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 59
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x103006f

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static a(ZZZLjava/lang/String;)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;-><init>()V

    .line 43
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_FORCE_UPDATE"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 45
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_USE_PLAY_STORE"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 46
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_IS_LIGHT_THEME"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 47
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_RELEASE_NOTES"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-object v0
.end method

.method private static a(ZZ)[I
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 142
    if-eqz p0, :cond_1

    .line 143
    if-eqz p1, :cond_0

    .line 144
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 152
    :goto_0
    return-object v0

    .line 146
    :cond_0
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    goto :goto_0

    .line 149
    :cond_1
    if-eqz p1, :cond_2

    .line 150
    new-array v0, v0, [I

    fill-array-data v0, :array_2

    goto :goto_0

    .line 152
    :cond_2
    new-array v0, v0, [I

    fill-array-data v0, :array_3

    goto :goto_0

    .line 144
    nop

    :array_0
    .array-data 4
        0x7f0d020a
        0x7f0d020e
    .end array-data

    .line 146
    :array_1
    .array-data 4
        0x7f0d020a
        0x7f0d0210
    .end array-data

    .line 150
    :array_2
    .array-data 4
        0x7f0d0209
        0x7f0d020d
    .end array-data

    .line 152
    :array_3
    .array-data 4
        0x7f0d0209
        0x7f0d020f
    .end array-data
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "EXTRA_FORCE_UPDATE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "EXTRA_USE_PLAY_STORE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 72
    invoke-static {}, Lcom/dropbox/android/activity/base/d;->a()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    .line 74
    :goto_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a()Landroid/content/Context;

    move-result-object v5

    .line 75
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    invoke-static {v3, v4}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a(ZZ)[I

    move-result-object v4

    .line 77
    aget v5, v4, v2

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 79
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {v6, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 85
    :goto_1
    new-instance v2, Lcom/dropbox/android/activity/dialog/J;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dialog/J;-><init>(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;)V

    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "EXTRA_RELEASE_NOTES"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 96
    if-eqz v5, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300d3

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 98
    const v0, 0x7f0701c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 99
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 101
    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 106
    :goto_2
    if-eqz v3, :cond_3

    .line 107
    const v0, 0x7f0d0211

    invoke-virtual {v6, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 113
    :goto_3
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 72
    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 103
    :cond_2
    aget v0, v4, v1

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 109
    :cond_3
    const v0, 0x7f0d0212

    invoke-virtual {v6, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 110
    const v0, 0x7f0d0213

    invoke-virtual {v6, v0, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_3
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 119
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;)V

    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 124
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onStart()V

    .line 129
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;)V

    .line 130
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onStop()V

    .line 135
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;)V

    .line 136
    return-void
.end method
