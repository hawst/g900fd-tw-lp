.class public Lcom/dropbox/android/activity/DropboxSendTo;
.super Lcom/dropbox/android/activity/UploadBaseActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 124
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    .line 125
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    .line 126
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 127
    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/dropbox/android/activity/DropboxSendTo;->checkUriPermission(Landroid/net/Uri;III)I

    move-result v0

    .line 128
    if-nez v0, :cond_0

    move v0, v1

    .line 132
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 94
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 96
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 97
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_0
    :goto_0
    return-object v2

    .line 101
    :cond_1
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 102
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 103
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 105
    if-eqz v0, :cond_0

    .line 106
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_2
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 111
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 114
    instance-of v3, v0, Landroid/net/Uri;

    if-eqz v3, :cond_3

    .line 115
    check-cast v0, Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 137
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    invoke-super {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;->onBackPressed()V

    .line 140
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 29
    invoke-static {p0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/app/Activity;)V

    .line 30
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UploadBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxSendTo;->setContentView(I)V

    .line 37
    const v0, 0x7f0d01d5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxSendTo;->setTitle(I)V

    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 42
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->e()Ljava/util/Collection;

    move-result-object v0

    .line 51
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxSendTo;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    const v0, 0x7f0d0061

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->finish()V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 60
    const-string v1, "share_screenshot"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 61
    const-string v1, "share_favicon"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 63
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxSendTo;->startActivity(Landroid/content/Intent;)V

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->finish()V

    goto :goto_0

    .line 69
    :cond_2
    if-nez p1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->i()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/dropbox/android/activity/SendToFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/SendToFragment;-><init>()V

    .line 73
    new-instance v2, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v2, v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/SendToFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 74
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 75
    const v2, 0x7f0700b2

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 76
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method
