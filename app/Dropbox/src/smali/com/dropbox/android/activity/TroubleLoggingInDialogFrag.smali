.class public Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;
.super Landroid/support/v4/app/DialogFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 17
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 18
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;-><init>()V

    .line 22
    invoke-virtual {v0}, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 29
    const v1, 0x7f0d0029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 30
    const v1, 0x7f0e0008

    new-instance v2, Lcom/dropbox/android/activity/fA;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/fA;-><init>(Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 43
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
