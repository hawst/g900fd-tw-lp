.class public Lcom/dropbox/android/activity/ShareViaEmailActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 15
    return-void
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Lcom/dropbox/android/albums/Album;)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "EXTRA_SHARE_TYPE"

    sget-object v1, Lcom/dropbox/android/activity/eK;->b:Lcom/dropbox/android/activity/eK;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/eK;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v0, "EXTRA_ALBUM"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 79
    return-void
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    .line 70
    const-string v0, "EXTRA_SHARE_TYPE"

    sget-object v1, Lcom/dropbox/android/activity/eK;->a:Lcom/dropbox/android/activity/eK;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/eK;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v0, "EXTRA_LOCALENTRY"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 73
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ShareViaEmailFragment;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a()V

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->e()V

    .line 49
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->finish()V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->setContentView(I)V

    .line 35
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 38
    if-nez p1, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 41
    const v1, 0x7f0700b2

    new-instance v2, Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-direct {v2}, Lcom/dropbox/android/activity/ShareViaEmailFragment;-><init>()V

    sget-object v3, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 42
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 61
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailActivity;->e()V

    .line 63
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
