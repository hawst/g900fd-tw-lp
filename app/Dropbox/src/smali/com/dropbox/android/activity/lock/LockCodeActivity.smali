.class public Lcom/dropbox/android/activity/lock/LockCodeActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/TextView;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/dropbox/android/activity/lock/f;

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:J

.field private q:Ldbxyzptlk/db231222/r/o;

.field private r:Lcom/dropbox/android/activity/lock/LockReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 257
    iput v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->l:I

    .line 258
    iput v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    .line 259
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->n:I

    .line 261
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->p:J

    .line 263
    iput-object v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    .line 265
    iput-object v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->r:Lcom/dropbox/android/activity/lock/LockReceiver;

    .line 727
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->l:I

    return v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 560
    sget-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    const-string v1, "Unlocked Lock Code"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->setResult(I)V

    .line 562
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->r:Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/LockReceiver;->d()V

    .line 567
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->finish()V

    .line 568
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->r:Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/LockReceiver;->e()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    invoke-virtual {v0, p0, p1, p2}, Lcom/dropbox/android/activity/lock/f;->a(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)V

    .line 452
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Ldbxyzptlk/db231222/r/o;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    return-object v0
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 571
    iput p1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->l:I

    .line 572
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e()V

    .line 573
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f()V

    .line 574
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(Ljava/lang/String;Z)V

    return-void
.end method

.method private b(Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    const v4, 0x7f0d01ce

    const/16 v2, 0xa

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 455
    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v3, v0

    .line 488
    :cond_0
    :goto_0
    return v3

    .line 457
    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/K;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    move v3, v0

    .line 458
    goto :goto_0

    .line 459
    :cond_2
    if-eqz p2, :cond_0

    .line 460
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e()V

    .line 461
    iget v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    .line 462
    iget-boolean v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->o:Z

    if-eqz v0, :cond_5

    .line 463
    iget v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    if-ne v0, v2, :cond_3

    .line 464
    const v0, 0x7f0d01d0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(I)V

    .line 465
    sget-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    const-string v1, "Unlinking from too many times with error code."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    new-instance v0, Ldbxyzptlk/db231222/n/N;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    const/4 v4, 0x0

    move-object v1, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/n/N;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;ZLdbxyzptlk/db231222/n/O;Z)V

    .line 468
    sget-object v1, Lcom/dropbox/android/activity/lock/c;->a:Lcom/dropbox/android/activity/lock/c;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/lock/c;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/N;->a(I)V

    .line 469
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/N;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 470
    :cond_3
    iget v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_4

    .line 471
    const v0, 0x7f0d01cf

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(I)V

    goto :goto_0

    .line 473
    :cond_4
    sget-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Now we have "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " retries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-direct {p0, v4}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(I)V

    goto :goto_0

    .line 477
    :cond_5
    iget v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    if-ne v0, v2, :cond_6

    .line 478
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v4, 0x927c0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->p:J

    .line 479
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    iget-wide v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->p:J

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/r/o;->a(J)V

    .line 481
    invoke-static {p0}, Lcom/dropbox/android/activity/lock/n;->a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)V

    goto/16 :goto_0

    .line 483
    :cond_6
    sget-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Now we have "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " retries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    invoke-direct {p0, v4}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(I)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->n:I

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/activity/lock/LockCodeActivity;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->p:J

    return-wide v0
.end method

.method private c(I)V
    .locals 0

    .prologue
    .line 585
    invoke-static {p0, p1}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    .line 586
    return-void
.end method

.method private c(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 492
    if-eqz p2, :cond_0

    .line 493
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 494
    iput-object p1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->j:Ljava/lang/String;

    .line 495
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b(I)V

    .line 500
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    const-string v1, "Got a non-4 character lock code"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private d(I)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 595
    packed-switch p1, :pswitch_data_0

    .line 605
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    :goto_0
    return-object v0

    .line 597
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    goto :goto_0

    .line 599
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    goto :goto_0

    .line 601
    :pswitch_2
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    goto :goto_0

    .line 603
    :pswitch_3
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    goto :goto_0

    .line 595
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic d(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(I)Landroid/widget/EditText;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private d(Ljava/lang/String;Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 504
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 505
    const v1, 0x7f0d01d2

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(I)V

    .line 507
    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->j:Ljava/lang/String;

    iput-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->i:Ljava/lang/String;

    .line 508
    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->i:Ljava/lang/String;

    invoke-interface {v1, v2}, Ldbxyzptlk/db231222/r/o;->a(Ljava/lang/String;)V

    .line 510
    invoke-direct {p0, v4}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a(I)V

    .line 539
    :goto_0
    return v0

    .line 512
    :cond_0
    if-eqz p2, :cond_1

    .line 515
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 519
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 528
    const v2, 0x7f0d01d1

    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->c(I)V

    .line 533
    :goto_2
    iget-object v2, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    sget-object v3, Lcom/dropbox/android/activity/lock/f;->d:Lcom/dropbox/android/activity/lock/f;

    if-ne v2, v3, :cond_2

    .line 534
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b(I)V

    :cond_1
    :goto_3
    move v0, v1

    .line 539
    goto :goto_0

    .line 516
    :catch_0
    move-exception v2

    move v2, v1

    goto :goto_1

    .line 522
    :pswitch_0
    invoke-static {p0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Landroid/content/Context;)V

    .line 523
    invoke-direct {p0, v4}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a(I)V

    goto :goto_2

    .line 536
    :cond_2
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b(I)V

    goto :goto_3

    .line 519
    nop

    :pswitch_data_0
    .packed-switch 0xbc614e
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 553
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 554
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 556
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 557
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/lock/f;->a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I

    move-result v0

    .line 579
    if-eqz v0, :cond_0

    .line 580
    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->n:I

    return v0
.end method


# virtual methods
.method public final a_()Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 544
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 545
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 546
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 547
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 548
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->finish()V

    .line 549
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v13, 0x0

    .line 300
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 302
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->finish()V

    .line 418
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-static {}, Lcom/dropbox/android/activity/lock/LockReceiver;->a()Lcom/dropbox/android/activity/lock/LockReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->r:Lcom/dropbox/android/activity/lock/LockReceiver;

    .line 309
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->setContentView(I)V

    .line 311
    const v0, 0x7f0700ea

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->h:Landroid/widget/TextView;

    .line 312
    const v0, 0x7f0700eb

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    .line 313
    const v0, 0x7f0700ec

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    .line 314
    const v0, 0x7f0700ed

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    .line 315
    const v0, 0x7f0700ee

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    .line 318
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setInputType(I)V

    .line 319
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setInputType(I)V

    .line 320
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setInputType(I)V

    .line 321
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setInputType(I)V

    .line 325
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 326
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 327
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 328
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 331
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/l;

    invoke-direct {v1, p0, v2}, Lcom/dropbox/android/activity/lock/l;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 332
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/l;

    invoke-direct {v1, p0, v3}, Lcom/dropbox/android/activity/lock/l;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 333
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/l;

    invoke-direct {v1, p0, v4}, Lcom/dropbox/android/activity/lock/l;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 334
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/l;

    invoke-direct {v1, p0, v5}, Lcom/dropbox/android/activity/lock/l;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/m;

    invoke-direct {v1, p0, v2}, Lcom/dropbox/android/activity/lock/m;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 338
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/m;

    invoke-direct {v1, p0, v3}, Lcom/dropbox/android/activity/lock/m;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 339
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/m;

    invoke-direct {v1, p0, v4}, Lcom/dropbox/android/activity/lock/m;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 340
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/lock/m;

    invoke-direct {v1, p0, v5}, Lcom/dropbox/android/activity/lock/m;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 343
    const v0, 0x7f070147

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 344
    const v1, 0x7f07014b

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 345
    const v2, 0x7f07014f

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 346
    const v3, 0x7f070148

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 347
    const v4, 0x7f07014c

    invoke-virtual {p0, v4}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 348
    const v5, 0x7f070150

    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 349
    const v6, 0x7f070149

    invoke-virtual {p0, v6}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 350
    const v7, 0x7f07014d

    invoke-virtual {p0, v7}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 351
    const v8, 0x7f070151

    invoke-virtual {p0, v8}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 352
    const v9, 0x7f07014e

    invoke-virtual {p0, v9}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 353
    const v10, 0x7f070152

    invoke-virtual {p0, v10}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 355
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "1"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "2"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "3"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "4"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "5"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "6"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "7"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "8"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v7, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "9"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v8, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    new-instance v11, Lcom/dropbox/android/activity/lock/p;

    const-string v12, "0"

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/p;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v9, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 365
    new-instance v11, Lcom/dropbox/android/activity/lock/b;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/dropbox/android/activity/lock/b;-><init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Lcom/dropbox/android/activity/lock/a;)V

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367
    new-instance v11, Lcom/dropbox/android/activity/lock/e;

    const/4 v12, 0x0

    invoke-direct {v11, v12}, Lcom/dropbox/android/activity/lock/e;-><init>(Lcom/dropbox/android/activity/lock/a;)V

    .line 368
    invoke-virtual {v0, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 369
    invoke-virtual {v1, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 370
    invoke-virtual {v2, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 371
    invoke-virtual {v3, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 372
    invoke-virtual {v4, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 373
    invoke-virtual {v5, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 374
    invoke-virtual {v6, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 375
    invoke-virtual {v7, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 376
    invoke-virtual {v8, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 377
    invoke-virtual {v9, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 378
    invoke-virtual {v10, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 380
    const v0, 0x7f07014a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setEnabled(Z)V

    .line 382
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 383
    if-nez v0, :cond_1

    .line 387
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->finish()V

    goto/16 :goto_0

    .line 390
    :cond_1
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    .line 392
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->i:Ljava/lang/String;

    .line 393
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->o:Z

    .line 394
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->q:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->p:J

    .line 396
    iget-wide v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->p:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 397
    invoke-static {p0}, Lcom/dropbox/android/activity/lock/n;->a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)V

    .line 400
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 401
    const-string v1, "PURPOSE"

    sget-object v2, Lcom/dropbox/android/activity/lock/f;->a:Lcom/dropbox/android/activity/lock/f;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/lock/f;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/f;->a(I)Lcom/dropbox/android/activity/lock/f;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    .line 403
    if-eqz p1, :cond_3

    .line 405
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    const-string v1, "SIS_KEY_LOCK_DIGIT_1"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    const-string v1, "SIS_KEY_LOCK_DIGIT_2"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 407
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    const-string v1, "SIS_KEY_LOCK_DIGIT_3"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    const-string v1, "SIS_KEY_LOCK_DIGIT_4"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 411
    const-string v0, "SIS_KEY_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->l:I

    .line 412
    const-string v0, "SIS_KEY_RETRIES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    .line 413
    const-string v0, "SIS_KEY_FOCUSED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(I)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 414
    const-string v0, "SIS_KEY_NEW_LOCK_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->j:Ljava/lang/String;

    .line 417
    :cond_3
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f()V

    goto/16 :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 590
    invoke-static {p1}, Lcom/dropbox/android/activity/lock/c;->a(I)Lcom/dropbox/android/activity/lock/c;

    move-result-object v0

    .line 591
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/lock/c;->a(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 435
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    sget-object v2, Lcom/dropbox/android/activity/lock/f;->a:Lcom/dropbox/android/activity/lock/f;

    if-eq v1, v2, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->finish()V

    .line 445
    :goto_0
    return v0

    .line 441
    :cond_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a(I)V

    goto :goto_0

    .line 445
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 422
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 427
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->r:Lcom/dropbox/android/activity/lock/LockReceiver;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/LockReceiver;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->k:Lcom/dropbox/android/activity/lock/f;

    sget-object v1, Lcom/dropbox/android/activity/lock/f;->a:Lcom/dropbox/android/activity/lock/f;

    if-ne v0, v1, :cond_0

    .line 428
    sget-object v0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->a:Ljava/lang/String;

    const-string v1, "App has been unlocked in a different place, so finishing"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-virtual {p0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->finish()V

    .line 431
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 282
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 285
    const-string v0, "SIS_KEY_LOCK_DIGIT_1"

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v0, "SIS_KEY_LOCK_DIGIT_2"

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v0, "SIS_KEY_LOCK_DIGIT_3"

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v0, "SIS_KEY_LOCK_DIGIT_4"

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v0, "SIS_KEY_STATE"

    iget v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 292
    const-string v0, "SIS_KEY_RETRIES"

    iget v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 293
    const-string v0, "SIS_KEY_FOCUSED"

    iget v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 294
    const-string v0, "SIS_KEY_NEW_LOCK_CODE"

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/LockCodeActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    return-void
.end method
