.class final Lcom/dropbox/android/activity/eW;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ldbxyzptlk/db231222/z/M;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ldbxyzptlk/db231222/z/M;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 458
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 459
    iput-object p4, p0, Lcom/dropbox/android/activity/eW;->a:Ljava/util/List;

    .line 460
    iput-object p3, p0, Lcom/dropbox/android/activity/eW;->d:Ljava/lang/String;

    .line 461
    iput-object p5, p0, Lcom/dropbox/android/activity/eW;->b:Ljava/lang/String;

    .line 462
    iput-object p2, p0, Lcom/dropbox/android/activity/eW;->c:Ldbxyzptlk/db231222/z/M;

    .line 463
    return-void
.end method

.method private b(Landroid/content/Context;Ljava/lang/Exception;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 490
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 491
    instance-of v1, p2, Ldbxyzptlk/db231222/w/i;

    if-eqz v1, :cond_0

    .line 492
    check-cast p2, Ldbxyzptlk/db231222/w/i;

    .line 493
    invoke-virtual {p2, v0}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 496
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 449
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eW;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 478
    iget-object v0, p0, Lcom/dropbox/android/activity/eW;->c:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/activity/eW;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/activity/eW;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/dropbox/android/activity/eW;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 479
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 467
    const v0, 0x7f0d02bf

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 468
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 469
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 501
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    move-object v0, p1

    .line 502
    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 503
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ShareViaEmailFragment;

    .line 504
    invoke-static {v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/eW;->b(Landroid/content/Context;Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    invoke-static {v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 506
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 449
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/eW;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 1

    .prologue
    .line 484
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 485
    const v0, 0x7f0d02c1

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 486
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 487
    return-void
.end method
