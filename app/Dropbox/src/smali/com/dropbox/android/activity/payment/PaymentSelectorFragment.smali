.class public Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/activity/payment/h;

.field private b:Lcom/dropbox/android/payments/h;

.field private c:Lcom/dropbox/android/payments/v;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Z

.field private l:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/ag;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ldbxyzptlk/db231222/l/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/i",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/ag;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/aB;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ldbxyzptlk/db231222/l/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/i",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/aB;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ldbxyzptlk/db231222/l/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/f",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/aB;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ldbxyzptlk/db231222/l/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/f",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/ag;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 73
    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->d:Z

    .line 74
    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e:Z

    .line 75
    iput-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->f:Ljava/lang/Integer;

    .line 81
    iput-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i:Landroid/view/View;

    .line 83
    iput-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->j:Landroid/view/View;

    .line 85
    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    .line 96
    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->r:Z

    .line 464
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/payment/h;ZZ)Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 101
    const-string v1, "payment_selector_fragment_upgrade_source"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 102
    const-string v1, "EXTRA_CHECK_SUBSCRIPTIONS"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    const-string v1, "EXTRA_SUBSCRIPTION_USED"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104
    new-instance v1, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;-><init>()V

    .line 105
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->setArguments(Landroid/os/Bundle;)V

    .line 106
    return-object v1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->f:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->g:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 296
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bs()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 297
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    .line 298
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a:Lcom/dropbox/android/activity/payment/h;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->b(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 300
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 301
    return-void
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(IIZ)V

    .line 458
    return-void
.end method

.method private a(IIZ)V
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(IIZ[Ljava/lang/Object;)V

    .line 462
    return-void
.end method

.method private varargs a(IIZ[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 507
    invoke-virtual {p0, p2, p4}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 509
    invoke-static {p0, v1, v0, p3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 510
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->d()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;II)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;IIZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(IIZ)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;IIZ[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(IIZ[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Lcom/dropbox/android/payments/h;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->h:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    const v1, 0x7f0d02ee

    .line 307
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->d:Z

    if-eqz v0, :cond_0

    .line 308
    invoke-direct {p0, v1, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(II)V

    .line 321
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 312
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bB()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 313
    const v0, 0x7f0d02e8

    const v1, 0x7f0d02e9

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(II)V

    goto :goto_0

    .line 317
    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bt()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 318
    new-instance v0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    sget-object v1, Lcom/dropbox/android/payments/g;->a:Lcom/dropbox/android/payments/g;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;-><init>(Lcom/dropbox/android/payments/g;Ljava/lang/String;)V

    .line 319
    new-instance v1, Lcom/dropbox/android/payments/e;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/payments/e;-><init>(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ldbxyzptlk/db231222/z/M;)V

    .line 320
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->l:Ldbxyzptlk/db231222/l/k;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(Z)V

    .line 325
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    return p1
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 438
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v1

    .line 439
    :goto_0
    iget-boolean v3, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e:Z

    if-eqz v3, :cond_2

    .line 440
    const v0, 0x7f0d02f0

    const v3, 0x7f0d02f1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->f:Ljava/lang/Integer;

    aput-object v4, v1, v2

    invoke-direct {p0, v0, v3, v2, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(IIZ[Ljava/lang/Object;)V

    .line 454
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 438
    goto :goto_0

    .line 445
    :cond_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    if-eqz v0, :cond_0

    .line 451
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bu()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 452
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/dropbox/android/payments/h;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->c()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/y;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/payment/y;->a(Z)V

    .line 514
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a()V

    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    return v0
.end method

.method static synthetic h(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Lcom/dropbox/android/activity/payment/h;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a:Lcom/dropbox/android/activity/payment/h;

    return-object v0
.end method

.method static synthetic k(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/l/k;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->n:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method static synthetic n(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e()V

    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 329
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/payments/h;->a(IILandroid/content/Intent;)Z

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    if-nez p1, :cond_0

    .line 332
    if-eqz p3, :cond_0

    .line 333
    const-string v0, "EXTRA_PAYMENT_SELECTOR_COMPLETED_UPGRADE"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 334
    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/y;

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/payment/y;->a(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "payment_selector_fragment_upgrade_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/h;

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a:Lcom/dropbox/android/activity/payment/h;

    .line 114
    if-eqz p1, :cond_0

    .line 115
    const-string v0, "EXTRA_CHECK_SUBSCRIPTIONS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    .line 116
    const-string v0, "EXTRA_SUBSCRIPTION_USED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e:Z

    .line 121
    :goto_0
    new-instance v0, Lcom/dropbox/android/activity/payment/p;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/p;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->m:Ldbxyzptlk/db231222/l/i;

    .line 144
    new-instance v0, Lcom/dropbox/android/activity/payment/q;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/q;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->o:Ldbxyzptlk/db231222/l/i;

    .line 180
    new-instance v0, Lcom/dropbox/android/activity/payment/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/payment/w;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Lcom/dropbox/android/activity/payment/p;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->c:Lcom/dropbox/android/payments/v;

    .line 181
    new-instance v0, Lcom/dropbox/android/payments/h;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/payments/DbxSubscriptions;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->c:Lcom/dropbox/android/payments/v;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/payments/h;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/dropbox/android/payments/v;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    .line 184
    new-instance v0, Lcom/dropbox/android/activity/payment/r;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/r;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    .line 192
    new-instance v1, Lcom/dropbox/android/activity/payment/s;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/payment/s;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    .line 199
    new-instance v2, Lcom/dropbox/android/activity/payment/t;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/payment/t;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    .line 205
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    .line 206
    if-nez v3, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/y;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/payment/y;->a(Z)V

    .line 218
    :goto_1
    return-void

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EXTRA_CHECK_SUBSCRIPTIONS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    .line 119
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EXTRA_SUBSCRIPTION_USED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e:Z

    goto :goto_0

    .line 212
    :cond_1
    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->c()Ldbxyzptlk/db231222/l/k;

    move-result-object v4

    iput-object v4, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->l:Ldbxyzptlk/db231222/l/k;

    .line 213
    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->b()Ldbxyzptlk/db231222/l/k;

    move-result-object v3

    iput-object v3, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->n:Ldbxyzptlk/db231222/l/k;

    .line 214
    new-instance v3, Ldbxyzptlk/db231222/l/f;

    iget-object v4, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->n:Ldbxyzptlk/db231222/l/k;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-direct {v3, v4, v0, v2, v5}, Ldbxyzptlk/db231222/l/f;-><init>(Ldbxyzptlk/db231222/l/k;Ldbxyzptlk/db231222/l/h;Ldbxyzptlk/db231222/l/g;Landroid/support/v4/app/FragmentManager;)V

    iput-object v3, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->p:Ldbxyzptlk/db231222/l/f;

    .line 216
    new-instance v2, Ldbxyzptlk/db231222/l/f;

    iget-object v3, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->l:Ldbxyzptlk/db231222/l/k;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v2, v3, v0, v1, v4}, Ldbxyzptlk/db231222/l/f;-><init>(Ldbxyzptlk/db231222/l/k;Ldbxyzptlk/db231222/l/h;Ldbxyzptlk/db231222/l/g;Landroid/support/v4/app/FragmentManager;)V

    iput-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->q:Ldbxyzptlk/db231222/l/f;

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 223
    const v2, 0x7f03006b

    invoke-virtual {p1, v2, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 225
    const v2, 0x7f070136

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i:Landroid/view/View;

    .line 226
    iget-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i:Landroid/view/View;

    new-instance v4, Lcom/dropbox/android/activity/payment/u;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/payment/u;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    const v2, 0x7f070137

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->j:Landroid/view/View;

    .line 237
    iget-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->j:Landroid/view/View;

    new-instance v4, Lcom/dropbox/android/activity/payment/v;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/payment/v;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v2

    const-string v4, "mobile-payment-selector-v1"

    invoke-virtual {v2, v4}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 250
    const/4 v2, 0x0

    .line 252
    const-string v5, "play-first"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 253
    iget-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->j:Landroid/view/View;

    .line 260
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 261
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 263
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 264
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 265
    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 272
    :cond_1
    :goto_1
    return-object v3

    .line 254
    :cond_2
    const-string v5, "cc-first"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 255
    iget-object v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i:Landroid/view/View;

    goto :goto_0

    .line 256
    :cond_3
    const-string v5, "play-only"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    .line 257
    goto :goto_0

    .line 268
    :cond_4
    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 289
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onPause()V

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->r:Z

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->q:Ldbxyzptlk/db231222/l/f;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/l/f;->a()V

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->p:Ldbxyzptlk/db231222/l/f;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/l/f;->a()V

    .line 293
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->r:Z

    if-nez v0, :cond_0

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->r:Z

    .line 281
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onResume()V

    .line 282
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->q:Ldbxyzptlk/db231222/l/f;

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->m:Ldbxyzptlk/db231222/l/i;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/f;->a(Ldbxyzptlk/db231222/l/i;)V

    .line 283
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->p:Ldbxyzptlk/db231222/l/f;

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->o:Ldbxyzptlk/db231222/l/i;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/f;->a(Ldbxyzptlk/db231222/l/i;)V

    .line 285
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 518
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 519
    const-string v0, "EXTRA_CHECK_SUBSCRIPTIONS"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 520
    const-string v0, "EXTRA_SUBSCRIPTION_USED"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 521
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 343
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStart()V

    .line 344
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->c()V

    .line 345
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b:Lcom/dropbox/android/payments/h;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->a()V

    .line 350
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStop()V

    .line 351
    return-void
.end method
