.class public abstract Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/dropbox/android/activity/bP;
.implements Lcom/dropbox/android/widget/p;
.implements Lcom/dropbox/android/widget/quickactions/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/dropbox/android/activity/bP;",
        "Lcom/dropbox/android/widget/p;",
        "Lcom/dropbox/android/widget/quickactions/f;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;

.field private static r:Z

.field private static s:Z


# instance fields
.field protected a:Lcom/dropbox/android/filemanager/I;

.field protected b:Lcom/dropbox/android/provider/MetadataManager;

.field protected c:Lcom/dropbox/android/util/HistoryStack;

.field private e:Landroid/support/v4/content/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/widget/TextView;

.field private g:Lcom/dropbox/android/widget/DropboxItemListView;

.field private h:Lcom/dropbox/android/taskqueue/D;

.field private i:Lcom/dropbox/android/util/DropboxPath;

.field private j:Lcom/dropbox/android/util/HistoryEntry;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/view/View;

.field private n:Lcom/dropbox/android/widget/am;

.field private final o:Lcom/dropbox/android/util/L;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/util/L",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    const-class v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Ljava/lang/String;

    .line 864
    sput-boolean v1, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->r:Z

    .line 865
    sput-boolean v1, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->s:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 97
    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h:Lcom/dropbox/android/taskqueue/D;

    .line 99
    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i:Lcom/dropbox/android/util/DropboxPath;

    .line 101
    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Lcom/dropbox/android/util/HistoryEntry;

    .line 383
    new-instance v0, Lcom/dropbox/android/activity/bJ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bJ;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->n:Lcom/dropbox/android/widget/am;

    .line 567
    new-instance v0, Lcom/dropbox/android/activity/bK;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/bK;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:Lcom/dropbox/android/util/L;

    .line 745
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/util/DropboxPath;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i:Lcom/dropbox/android/util/DropboxPath;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private a(III)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 782
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/MetadataManager;->b()V

    .line 784
    iget v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    iget v3, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->q:I

    add-int/2addr v3, v0

    .line 785
    add-int v5, p1, p2

    .line 787
    iget v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    iget v4, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->q:I

    mul-int/lit8 v4, v4, 0x2

    add-int v6, v0, v4

    .line 788
    mul-int/lit8 v0, p2, 0x2

    add-int v7, p1, v0

    .line 790
    iget v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 791
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 792
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 793
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 794
    invoke-static {v3, p3}, Ljava/lang/Math;->min(II)I

    move-result v8

    move v4, v0

    .line 796
    :goto_0
    if-ge v4, v8, :cond_7

    .line 797
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, v4}, Lcom/dropbox/android/widget/DropboxItemListView;->a(I)Landroid/database/Cursor;

    move-result-object v0

    .line 798
    if-nez v0, :cond_1

    .line 796
    :cond_0
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 802
    :cond_1
    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v3}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v3

    .line 803
    sget-object v9, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v3, v9, :cond_0

    .line 807
    invoke-static {v0}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v9

    .line 810
    iget-boolean v0, v9, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_2

    if-lt v4, p1, :cond_2

    if-ge v4, v5, :cond_2

    .line 811
    iget-object v0, v9, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v9}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/dropbox/android/provider/MetadataManager;->c(Lcom/dropbox/android/util/DropboxPath;)V

    .line 817
    :cond_2
    if-ge v4, p1, :cond_4

    move v3, v1

    .line 818
    :goto_2
    if-gt v7, v4, :cond_5

    move v0, v1

    .line 819
    :goto_3
    if-nez v3, :cond_3

    if-eqz v0, :cond_6

    .line 821
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h:Lcom/dropbox/android/taskqueue/D;

    sget-object v3, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {v9}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v9

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v10

    invoke-virtual {v0, v3, v9, v10}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;)V

    goto :goto_1

    :cond_4
    move v3, v2

    .line 817
    goto :goto_2

    :cond_5
    move v0, v2

    .line 818
    goto :goto_3

    .line 823
    :cond_6
    if-gt p1, v4, :cond_0

    if-gt v6, v4, :cond_0

    if-ge v4, v7, :cond_0

    .line 825
    iget-boolean v0, v9, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h:Lcom/dropbox/android/taskqueue/D;

    sget-object v3, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    invoke-virtual {v9}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v10

    iget-object v9, v9, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v11

    invoke-virtual {v0, v3, v10, v9, v11}, Lcom/dropbox/android/taskqueue/D;->b(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V

    goto :goto_1

    .line 833
    :cond_7
    iput p1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    .line 834
    iput p2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->q:I

    .line 835
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->q()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_0

    .line 526
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryEntry;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/Y;->a(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 529
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;III)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(III)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/database/Cursor;)Z
    .locals 8

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 692
    invoke-static {p1}, Lcom/dropbox/android/b;->a(Landroid/net/Uri;)Z

    move-result v6

    .line 693
    if-eqz v6, :cond_2

    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move v1, v2

    .line 695
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 696
    invoke-interface {p2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 697
    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p2, v3}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v4

    .line 698
    const/4 v3, 0x0

    .line 699
    if-eqz v6, :cond_4

    .line 701
    sget-object v7, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v4, v7, :cond_3

    .line 702
    const-string v4, "path"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 706
    :goto_2
    if-eq v4, v5, :cond_0

    .line 707
    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->h()Ljava/lang/String;

    move-result-object v3

    .line 714
    :cond_0
    :goto_3
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 715
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b(I)V

    .line 716
    const/4 v2, 0x1

    .line 720
    :cond_1
    return v2

    .line 693
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 703
    :cond_3
    sget-object v7, Lcom/dropbox/android/provider/Z;->c:Lcom/dropbox/android/provider/Z;

    if-ne v4, v7, :cond_6

    .line 704
    const-string v4, "intended_db_path"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 710
    :cond_4
    sget-object v7, Lcom/dropbox/android/provider/Z;->c:Lcom/dropbox/android/provider/Z;

    if-ne v4, v7, :cond_0

    .line 711
    const-string v3, "local_uri"

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 695
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    move v4, v5

    goto :goto_2
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 885
    sget-boolean v1, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->r:Z

    if-eqz v1, :cond_0

    .line 886
    sput-boolean v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->s:Z

    .line 893
    :goto_0
    return v0

    .line 889
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 890
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    if-eqz v1, :cond_1

    .line 891
    check-cast v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o()Z

    move-result v0

    goto :goto_0

    .line 893
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/net/Uri;Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/net/Uri;Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 724
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/ao;->a(I)V

    .line 729
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    new-instance v1, Lcom/dropbox/android/activity/bL;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/bL;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;I)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->post(Ljava/lang/Runnable;)Z

    .line 735
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/widget/DropboxItemListView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryEntry;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->r()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->s()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method private q()Lcom/dropbox/android/util/HistoryEntry;
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 515
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->a(I)Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 519
    :goto_0
    return-object v0

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 517
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->e()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    goto :goto_0

    .line 519
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 4

    .prologue
    .line 671
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/a;

    .line 672
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 673
    invoke-virtual {v0}, Landroid/support/v4/widget/a;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 674
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 675
    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 677
    instance-of v3, v1, Landroid/net/Uri;

    if-eqz v3, :cond_0

    .line 678
    check-cast v1, Landroid/net/Uri;

    .line 679
    invoke-virtual {v0}, Landroid/support/v4/widget/a;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/net/Uri;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    const-string v0, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 681
    const/4 v0, 0x1

    .line 687
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 741
    iput v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    .line 742
    iput v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->q:I

    .line 743
    return-void
.end method

.method private u()V
    .locals 0

    .prologue
    .line 917
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i()V

    .line 334
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 336
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 208
    packed-switch p1, :pswitch_data_0

    .line 240
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 213
    :pswitch_0
    if-eqz p3, :cond_0

    .line 214
    const-string v0, "FINAL_IMAGE_PATH"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 220
    const-string v0, "FINAL_IMAGE_PATH"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/a;

    .line 223
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 224
    new-instance v2, Lcom/dropbox/android/activity/bI;

    invoke-direct {v2, p0, v0, v1}, Lcom/dropbox/android/activity/bI;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Landroid/support/v4/widget/a;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Ljava/lang/Runnable;)Z

    .line 242
    :cond_0
    return-void

    .line 208
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/database/Cursor;Lcom/dropbox/android/util/HistoryEntry;)V
    .locals 2

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i()V

    .line 541
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_3

    .line 542
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 543
    const-string v1, "EXTRA_DIR_DOES_NOT_EXIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    const v0, 0x7f0d0057

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    .line 555
    :goto_0
    return-void

    .line 545
    :cond_0
    const-string v1, "EXTRA_NETWORK_ERROR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 546
    const v0, 0x7f0d0051

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    goto :goto_0

    .line 547
    :cond_1
    const-string v1, "EXTRA_NO_FILTER_MATCHES"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    const v0, 0x7f0d0053

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    goto :goto_0

    .line 550
    :cond_2
    invoke-virtual {p2}, Lcom/dropbox/android/util/HistoryEntry;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    goto :goto_0

    .line 553
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->m()V

    goto :goto_0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 851
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    if-nez v0, :cond_0

    .line 853
    if-eqz p1, :cond_1

    const-string v0, "SIS_KEY_HISTORY_STACK"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 854
    const-string v0, "SIS_KEY_HISTORY_STACK"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryStack;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    .line 861
    :cond_0
    :goto_0
    return-void

    .line 857
    :cond_1
    new-instance v0, Lcom/dropbox/android/util/HistoryStack;

    invoke-direct {v0}, Lcom/dropbox/android/util/HistoryStack;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    .line 858
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    new-instance v1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    sget-object v2, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:Lcom/dropbox/android/util/L;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/util/L;->b(Ljava/lang/Object;)V

    .line 642
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Lcom/dropbox/android/util/HistoryEntry;

    .line 643
    return-void
.end method

.method protected a(Lcom/dropbox/android/filemanager/LocalEntry;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 363
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 365
    invoke-static {p1, v3}, Lcom/dropbox/android/util/ab;->a(Lcom/dropbox/android/filemanager/LocalEntry;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f()Lcom/dropbox/android/provider/E;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 369
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t browse gallery with a filter set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e()Ldbxyzptlk/db231222/n/r;

    move-result-object v2

    invoke-static {v0, v1, v2, p1, p2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/app/Activity;Lcom/dropbox/android/util/HistoryEntry;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/filemanager/LocalEntry;I)Landroid/content/Intent;

    move-result-object v0

    .line 376
    invoke-virtual {p0, v0, v3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 381
    :goto_0
    return-void

    .line 378
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 379
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/bs;->a:Lcom/dropbox/android/util/bs;

    invoke-static {v1, v0, p1, v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;)V

    goto :goto_0
.end method

.method public a(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3

    .prologue
    .line 347
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 348
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->s()V

    .line 350
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Browsing directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->a(Lcom/dropbox/android/widget/DropboxItemListView;)V

    .line 354
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    new-instance v1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    .line 355
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    .line 356
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/HistoryEntry;)V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/dropbox/android/util/HistoryStack;

    invoke-direct {v0}, Lcom/dropbox/android/util/HistoryStack;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    .line 121
    :cond_0
    return-void
.end method

.method public abstract b()Lcom/dropbox/android/widget/as;
.end method

.method public final b_()V
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Landroid/support/v4/content/p;

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Landroid/support/v4/content/p;

    invoke-virtual {v0}, Landroid/support/v4/content/p;->q()V

    .line 660
    :cond_0
    return-void
.end method

.method protected abstract c()I
.end method

.method protected final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    return-void
.end method

.method public final c_()V
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Landroid/support/v4/content/p;

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Landroid/support/v4/content/p;

    invoke-virtual {v0}, Landroid/support/v4/content/p;->o()V

    .line 668
    :cond_0
    return-void
.end method

.method protected e()Ldbxyzptlk/db231222/n/r;
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/K;->c()Ldbxyzptlk/db231222/n/r;

    move-result-object v0

    return-object v0
.end method

.method protected f()Lcom/dropbox/android/provider/E;
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    .line 139
    return-void
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    return-void
.end method

.method protected final i()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 183
    return-void
.end method

.method protected final j()Lcom/dropbox/android/util/HistoryEntry;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    return-object v0
.end method

.method protected final k()Lcom/dropbox/android/util/HistoryEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/dropbox/android/util/HistoryEntry;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    return-object v0
.end method

.method public l()V
    .locals 3

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_1

    .line 309
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 311
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Lcom/dropbox/android/util/HistoryEntry;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryEntry;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:Lcom/dropbox/android/util/L;

    invoke-virtual {v0}, Lcom/dropbox/android/util/L;->b()V

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 321
    :cond_1
    return-void
.end method

.method protected final m()V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 340
    return-void
.end method

.method protected n()V
    .locals 6

    .prologue
    .line 445
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Ljava/lang/String;

    const-string v1, "Tried to browseParent with an empty history stack."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    :goto_0
    return-void

    .line 449
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->q()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 450
    if-nez v0, :cond_2

    .line 454
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 455
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->a()Lcom/dropbox/android/util/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/al;->toString()Ljava/lang/String;

    move-result-object v0

    .line 456
    :goto_1
    new-instance v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No parent to browse to; top="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    .line 457
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 455
    :cond_1
    const-string v0, "null"

    goto :goto_1

    .line 461
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v1

    .line 462
    const/4 v2, 0x2

    if-lt v1, v2, :cond_4

    .line 463
    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    add-int/lit8 v3, v1, -0x2

    invoke-virtual {v2, v3}, Lcom/dropbox/android/util/HistoryStack;->a(I)Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/dropbox/android/util/HistoryEntry;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 465
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->d()Lcom/dropbox/android/util/HistoryEntry;

    .line 490
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    goto :goto_0

    .line 472
    :cond_3
    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v2}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v2

    .line 473
    iget-object v3, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v3, v1}, Lcom/dropbox/android/util/HistoryStack;->a(I)Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v1

    .line 474
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ap()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    new-instance v4, Lcom/dropbox/android/activity/bN;

    const-string v5, "top"

    invoke-direct {v4, v5, v2}, Lcom/dropbox/android/activity/bN;-><init>(Ljava/lang/String;Lcom/dropbox/android/util/HistoryEntry;)V

    invoke-virtual {v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    new-instance v3, Lcom/dropbox/android/activity/bN;

    const-string v4, "up"

    invoke-direct {v3, v4, v1}, Lcom/dropbox/android/activity/bN;-><init>(Ljava/lang/String;Lcom/dropbox/android/util/HistoryEntry;)V

    invoke-virtual {v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    new-instance v2, Lcom/dropbox/android/activity/bN;

    const-string v3, "parent"

    invoke-direct {v2, v3, v0}, Lcom/dropbox/android/activity/bN;-><init>(Ljava/lang/String;Lcom/dropbox/android/util/HistoryEntry;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 480
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/HistoryStack;->a(Lcom/dropbox/android/widget/DropboxItemListView;)V

    .line 481
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    goto :goto_2

    .line 486
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1}, Lcom/dropbox/android/util/HistoryStack;->d()Lcom/dropbox/android/util/HistoryEntry;

    .line 487
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    goto :goto_2
.end method

.method protected o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 494
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    :goto_0
    return v0

    .line 496
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v1

    if-le v1, v0, :cond_1

    .line 497
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v1}, Lcom/dropbox/android/util/HistoryStack;->d()Lcom/dropbox/android/util/HistoryEntry;

    .line 498
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    goto :goto_0

    .line 501
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 190
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 191
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_HIDE_QUICKACTIONS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    move v1, v0

    .line 193
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 194
    if-nez v0, :cond_0

    .line 204
    :goto_1
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b()Lcom/dropbox/android/widget/as;

    move-result-object v5

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v6

    move-object v2, p0

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/widget/DropboxItemListView;->a(ZLandroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/p;Lcom/dropbox/android/widget/quickactions/f;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V

    .line 198
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->n:Lcom/dropbox/android/widget/am;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setItemClickListener(Lcom/dropbox/android/widget/am;)V

    .line 199
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    new-instance v1, Lcom/dropbox/android/activity/bO;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/dropbox/android/activity/bO;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Lcom/dropbox/android/activity/bI;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 200
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 202
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    goto :goto_1

    :cond_1
    move v1, v2

    .line 197
    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 285
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 286
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 144
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 147
    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    .line 151
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a:Lcom/dropbox/android/filemanager/I;

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h:Lcom/dropbox/android/taskqueue/D;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 631
    const-class v1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 632
    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    .line 633
    new-instance v0, Lcom/dropbox/android/filemanager/T;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e()Ldbxyzptlk/db231222/n/r;

    move-result-object v6

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f()Lcom/dropbox/android/provider/E;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/j/i;Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;Lcom/dropbox/android/filemanager/U;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 161
    const v0, 0x7f0700a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/DropboxItemListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    .line 163
    const v0, 0x7f0700aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f:Landroid/widget/TextView;

    .line 164
    const v0, 0x7f0700ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l:Landroid/widget/TextView;

    .line 165
    const v0, 0x7f0700a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->m:Landroid/view/View;

    .line 167
    const v0, 0x7f0700a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k:Landroid/view/View;

    .line 168
    return-object v1
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 246
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDetach()V

    .line 247
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:Lcom/dropbox/android/util/L;

    invoke-virtual {v0}, Lcom/dropbox/android/util/L;->c()V

    .line 254
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 67
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 650
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 651
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 268
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onPause()V

    .line 269
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 270
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/MetadataManager;->b()V

    .line 274
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Lcom/dropbox/android/util/HistoryEntry;

    .line 275
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 839
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 842
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->a(Lcom/dropbox/android/widget/DropboxItemListView;)V

    .line 845
    :cond_0
    const-string v0, "SIS_KEY_HISTORY_STACK"

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 846
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 262
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStart()V

    .line 263
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->u()V

    .line 264
    return-void
.end method

.method protected p()V
    .locals 2

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i()V

    .line 562
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 563
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/database/Cursor;)V

    .line 564
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    .line 565
    return-void
.end method
