.class final Lcom/dropbox/android/activity/auth/p;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/z/Z;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/auth/DropboxAuth;

.field private final b:Ldbxyzptlk/db231222/z/M;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;Ldbxyzptlk/db231222/z/M;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    .line 488
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 489
    iput-object p3, p0, Lcom/dropbox/android/activity/auth/p;->b:Ldbxyzptlk/db231222/z/M;

    .line 490
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/z/Z;
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/p;->b:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i(Lcom/dropbox/android/activity/auth/DropboxAuth;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v2}, Lcom/dropbox/android/activity/auth/DropboxAuth;->j(Lcom/dropbox/android/activity/auth/DropboxAuth;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/Z;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 483
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/auth/p;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/z/Z;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/z/Z;)V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0, p2}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Lcom/dropbox/android/activity/auth/DropboxAuth;Ldbxyzptlk/db231222/z/Z;)V

    .line 500
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 504
    instance-of v0, p2, Ldbxyzptlk/db231222/w/i;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Ldbxyzptlk/db231222/w/i;

    iget v0, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    .line 505
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    check-cast p2, Ldbxyzptlk/db231222/w/i;

    iget-object v1, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    const v2, 0x7f0d0203

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Lcom/dropbox/android/activity/auth/DropboxAuth;Ljava/lang/String;)V

    .line 511
    :goto_0
    return-void

    .line 506
    :cond_0
    instance-of v0, p2, Ldbxyzptlk/db231222/w/j;

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    check-cast p2, Ldbxyzptlk/db231222/w/j;

    iget-object v1, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    const v2, 0x7f0d0240

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ldbxyzptlk/db231222/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Lcom/dropbox/android/activity/auth/DropboxAuth;Ljava/lang/String;)V

    goto :goto_0

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/p;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->k(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 483
    check-cast p2, Ldbxyzptlk/db231222/z/Z;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/auth/p;->a(Landroid/content/Context;Ldbxyzptlk/db231222/z/Z;)V

    return-void
.end method
