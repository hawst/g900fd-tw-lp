.class final enum Lcom/dropbox/android/activity/delegate/k;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/delegate/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/delegate/k;

.field public static final enum b:Lcom/dropbox/android/activity/delegate/k;

.field public static final enum c:Lcom/dropbox/android/activity/delegate/k;

.field private static final synthetic d:[Lcom/dropbox/android/activity/delegate/k;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    new-instance v0, Lcom/dropbox/android/activity/delegate/k;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/delegate/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/k;->a:Lcom/dropbox/android/activity/delegate/k;

    .line 110
    new-instance v0, Lcom/dropbox/android/activity/delegate/k;

    const-string v1, "ALREADY_EXISTS"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/delegate/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/k;->b:Lcom/dropbox/android/activity/delegate/k;

    .line 111
    new-instance v0, Lcom/dropbox/android/activity/delegate/k;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/delegate/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/k;->c:Lcom/dropbox/android/activity/delegate/k;

    .line 108
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/activity/delegate/k;

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->a:Lcom/dropbox/android/activity/delegate/k;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->b:Lcom/dropbox/android/activity/delegate/k;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->c:Lcom/dropbox/android/activity/delegate/k;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/activity/delegate/k;->d:[Lcom/dropbox/android/activity/delegate/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/delegate/k;
    .locals 1

    .prologue
    .line 108
    const-class v0, Lcom/dropbox/android/activity/delegate/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/delegate/k;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/delegate/k;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/dropbox/android/activity/delegate/k;->d:[Lcom/dropbox/android/activity/delegate/k;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/delegate/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/delegate/k;

    return-object v0
.end method
