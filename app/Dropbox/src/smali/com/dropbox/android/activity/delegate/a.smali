.class public final Lcom/dropbox/android/activity/delegate/a;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    return-void
.end method


# virtual methods
.method public final a(Lcom/actionbarsherlock/view/Menu;Landroid/content/res/Resources;Lcom/dropbox/android/service/a;)Z
    .locals 5

    .prologue
    const v4, 0x1080049

    const/high16 v3, 0x10000

    const/4 v2, 0x0

    .line 103
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->b:Lcom/dropbox/android/activity/delegate/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/delegate/c;->a()I

    move-result v0

    const v1, 0x7f0d0091

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x1080040

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 113
    invoke-virtual {p3}, Lcom/dropbox/android/service/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->c:Lcom/dropbox/android/activity/delegate/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/delegate/c;->a()I

    move-result v0

    const v1, 0x7f0d0090

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 119
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->a:Lcom/dropbox/android/activity/delegate/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/delegate/c;->a()I

    move-result v0

    const v1, 0x7f0d008f

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 131
    invoke-interface {p2}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/activity/delegate/c;->a(I)Lcom/dropbox/android/activity/delegate/c;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/delegate/c;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;)V

    .line 134
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
