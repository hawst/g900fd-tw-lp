.class final Lcom/dropbox/android/activity/X;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/CreateShortcutFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/CreateShortcutFragment;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/dropbox/android/activity/X;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/X;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/X;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 68
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/dropbox/android/activity/X;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/X;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f020113

    invoke-static {v1, v2, v0, v3}, Lcom/dropbox/android/activity/CreateShortcutFragment;->a(Lcom/dropbox/android/activity/CreateShortcutFragment;Landroid/net/Uri;Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/activity/X;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 74
    :cond_0
    return-void
.end method
