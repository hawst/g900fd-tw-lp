.class final Lcom/dropbox/android/activity/aS;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/dropbox/android/activity/aS;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 555
    iget-object v0, p0, Lcom/dropbox/android/activity/aS;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    .line 559
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/aS;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/aS;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/GalleryActivity;->e(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/albums/Album;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;->a(Landroid/content/res/Resources;Lcom/dropbox/android/activity/bs;Ljava/lang/String;)Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/aS;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 561
    return-void

    .line 555
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/bs;->b:Lcom/dropbox/android/activity/bs;

    goto :goto_0
.end method
