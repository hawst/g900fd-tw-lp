.class final Lcom/dropbox/android/activity/T;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:[Landroid/view/View;

.field final synthetic b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;[Landroid/view/View;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/dropbox/android/activity/T;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/T;->a:[Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 168
    iget-object v1, p0, Lcom/dropbox/android/activity/T;->a:[Landroid/view/View;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 169
    invoke-virtual {v3, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/T;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    const v1, 0x7f070068

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 172
    return-void

    .line 171
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_1
.end method
