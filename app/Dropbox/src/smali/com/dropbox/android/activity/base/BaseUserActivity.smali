.class public Lcom/dropbox/android/activity/base/BaseUserActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 38
    new-instance v0, Lcom/dropbox/android/activity/base/j;

    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/base/j;-><init>(Ldbxyzptlk/db231222/r/e;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    .line 102
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->finish()V

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/j;->c()V

    .line 71
    return-void
.end method

.method protected final h()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/j;->b()Z

    move-result v0

    return v0
.end method

.method public final i()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/j;->d()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method final j()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/j;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0, p0, p1}, Lcom/dropbox/android/activity/base/j;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 49
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/j;->a(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onStart()V

    .line 55
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserActivity;->a:Lcom/dropbox/android/activity/base/j;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/j;->a()V

    .line 56
    return-void
.end method
