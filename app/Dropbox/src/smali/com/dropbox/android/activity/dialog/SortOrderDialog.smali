.class public Lcom/dropbox/android/activity/dialog/SortOrderDialog;
.super Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;-><init>()V

    .line 19
    return-void
.end method

.method public static a(Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/activity/dialog/SortOrderDialog;
    .locals 2

    .prologue
    .line 24
    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/dropbox/android/activity/dialog/I;

    if-nez v0, :cond_1

    .line 25
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must have target frag that implements our callback"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_1
    new-instance v0, Lcom/dropbox/android/activity/dialog/SortOrderDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;-><init>()V

    .line 28
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->setArguments(Landroid/os/Bundle;)V

    .line 29
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 30
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 36
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v4, 0x7f0d009a

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v4, 0x7f0d009b

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    .line 42
    new-array v4, v6, [Ldbxyzptlk/db231222/n/r;

    sget-object v0, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    aput-object v0, v4, v1

    sget-object v0, Ldbxyzptlk/db231222/n/r;->b:Ldbxyzptlk/db231222/n/r;

    aput-object v0, v4, v5

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v5

    .line 47
    invoke-virtual {v5}, Ldbxyzptlk/db231222/n/K;->c()Ldbxyzptlk/db231222/n/r;

    move-result-object v6

    move v0, v1

    .line 50
    :goto_0
    array-length v7, v4

    if-ge v0, v7, :cond_1

    .line 51
    aget-object v7, v4, v0

    if-ne v7, v6, :cond_0

    .line 57
    :goto_1
    new-instance v1, Lcom/dropbox/android/activity/dialog/H;

    invoke-direct {v1, p0, v0, v4, v5}, Lcom/dropbox/android/activity/dialog/H;-><init>(Lcom/dropbox/android/activity/dialog/SortOrderDialog;I[Ldbxyzptlk/db231222/n/r;Ldbxyzptlk/db231222/n/K;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 71
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 50
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
