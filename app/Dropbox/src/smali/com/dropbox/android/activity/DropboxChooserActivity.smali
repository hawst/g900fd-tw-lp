.class public Lcom/dropbox/android/activity/DropboxChooserActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/bG;


# instance fields
.field private a:Landroid/content/Intent;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private final f:Lcom/dropbox/android/service/I;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 53
    new-instance v0, Lcom/dropbox/android/activity/at;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/at;-><init>(Lcom/dropbox/android/activity/DropboxChooserActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->f:Lcom/dropbox/android/service/I;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxChooserActivity;)Lcom/dropbox/android/service/I;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->f:Lcom/dropbox/android/service/I;

    return-object v0
.end method

.method private a(Ljava/lang/String;[Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 272
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->at()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "caller"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 277
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 278
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 279
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setResult(ILandroid/content/Intent;)V

    .line 280
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->finish()V

    .line 281
    return-void

    .line 272
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/DropboxChooserActivity;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    return v0
.end method

.method private c(Ldbxyzptlk/db231222/z/ae;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 257
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 258
    const-string v0, "uri"

    iget-object v1, p1, Ldbxyzptlk/db231222/z/ae;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 259
    const-string v0, "name"

    iget-object v1, p1, Ldbxyzptlk/db231222/z/ae;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 261
    iget-object v0, p1, Ldbxyzptlk/db231222/z/ae;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 262
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 264
    :cond_0
    const-string v0, "thumbnails"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 265
    const-string v0, "icon"

    iget-object v1, p1, Ldbxyzptlk/db231222/z/ae;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 266
    const-string v0, "bytes"

    iget-wide v3, p1, Ldbxyzptlk/db231222/z/ae;->e:J

    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 267
    return-object v2
.end method

.method static synthetic c(Lcom/dropbox/android/activity/DropboxChooserActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->e()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    .line 182
    const v0, 0x7f0d01d8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setTitle(I)V

    .line 184
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->l()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 185
    invoke-static {}, Lcom/dropbox/android/activity/GetFromFragment;->d()Lcom/dropbox/android/activity/GetFromFragment;

    move-result-object v1

    .line 186
    new-instance v2, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v2, v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/GetFromFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/GetFromFragment;->a(Ljava/util/List;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 194
    const-string v2, "com.dropbox.android.intent.action.GET_PREVIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 195
    sget-object v0, Lcom/dropbox/android/activity/bH;->b:Lcom/dropbox/android/activity/bH;

    .line 203
    :goto_0
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/GetFromFragment;->a(Lcom/dropbox/android/activity/bH;)V

    .line 204
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->a:Landroid/content/Intent;

    const-string v2, "EXTRA_APP_KEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/GetFromFragment;->b(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 207
    const v2, 0x7f0700b2

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 208
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 209
    return-void

    .line 196
    :cond_1
    const-string v2, "com.dropbox.android.intent.action.GET_DIRECT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    sget-object v0, Lcom/dropbox/android/activity/bH;->c:Lcom/dropbox/android/activity/bH;

    goto :goto_0

    .line 198
    :cond_2
    const-string v2, "com.dropbox.android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 199
    sget-object v0, Lcom/dropbox/android/activity/bH;->d:Lcom/dropbox/android/activity/bH;

    goto :goto_0

    .line 201
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private f()V
    .locals 3

    .prologue
    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    .line 213
    const v0, 0x7f0d02d5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setTitle(I)V

    .line 215
    invoke-static {}, Lcom/dropbox/android/activity/DropboxChooserOfflineFragment;->a()Lcom/dropbox/android/activity/DropboxChooserOfflineFragment;

    move-result-object v0

    .line 217
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 218
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 219
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 220
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/z/ae;)V
    .locals 4

    .prologue
    .line 246
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/DropboxChooserActivity;->c(Ldbxyzptlk/db231222/z/ae;)Landroid/os/Bundle;

    move-result-object v0

    .line 247
    const-string v1, "EXTRA_CHOOSER_RESULTS"

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/DropboxChooserActivity;->a(Ljava/lang/String;[Landroid/os/Bundle;)V

    .line 248
    return-void
.end method

.method public final a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 232
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 233
    const-string v1, "uri"

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 234
    const-string v1, "EXTRA_CHOOSER_RESULTS"

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/DropboxChooserActivity;->a(Ljava/lang/String;[Landroid/os/Bundle;)V

    .line 235
    return-void
.end method

.method public final a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;Ldbxyzptlk/db231222/z/ae;)V
    .locals 4

    .prologue
    .line 239
    invoke-direct {p0, p4}, Lcom/dropbox/android/activity/DropboxChooserActivity;->c(Ldbxyzptlk/db231222/z/ae;)Landroid/os/Bundle;

    move-result-object v0

    .line 240
    const-string v1, "uri"

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 241
    const-string v1, "EXTRA_CHOOSER_RESULTS"

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/DropboxChooserActivity;->a(Ljava/lang/String;[Landroid/os/Bundle;)V

    .line 242
    return-void
.end method

.method public final b(Ldbxyzptlk/db231222/z/ae;)V
    .locals 4

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/DropboxChooserActivity;->c(Ldbxyzptlk/db231222/z/ae;)Landroid/os/Bundle;

    move-result-object v0

    .line 253
    const-string v1, "EXTRA_CHOOSER_RESULTS"

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/DropboxChooserActivity;->a(Ljava/lang/String;[Landroid/os/Bundle;)V

    .line 254
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 224
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 227
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 83
    invoke-static {p0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/app/Activity;)V

    .line 84
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    const v1, 0x7f030037

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setContentView(I)V

    .line 89
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v1

    .line 91
    const v2, 0x7f020284

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar;->setIcon(I)V

    .line 92
    invoke-virtual {v1, v3}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 94
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_APP_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    const-string v0, "DEVELOPER ERROR: Forgot to supply App Key."

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 99
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setResult(I)V

    .line 100
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->finish()V

    .line 148
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->h()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 105
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_WEB_SESSION_TOKEN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_1

    .line 108
    const-string v0, "com.dropbox.intent.action.WEB_SESSION_TOKEN"

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0, v2, v3, v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 111
    if-eqz v1, :cond_2

    .line 112
    const-string v2, "token"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    :cond_2
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->startActivity(Landroid/content/Intent;)V

    .line 115
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->finish()V

    goto :goto_0

    .line 119
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->a:Landroid/content/Intent;

    .line 120
    iget-object v2, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_EXTENSIONS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->b:Ljava/util/ArrayList;

    .line 122
    if-nez p1, :cond_6

    .line 124
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/J;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 125
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->e()V

    .line 130
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v2

    .line 131
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->as()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "app_key"

    invoke-virtual {v3, v4, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v3, "client_version"

    iget-object v4, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->a:Landroid/content/Intent;

    const-string v5, "EXTRA_SDK_VERSION"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v3, "extensions"

    iget-object v4, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v3, "caller"

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-virtual {v1, v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_0

    .line 127
    :cond_5
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->f()V

    goto :goto_1

    .line 138
    :cond_6
    const-string v0, "SIS_KEY_mShowingOffline"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    .line 142
    iget-boolean v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    if-eqz v0, :cond_7

    .line 143
    const v0, 0x7f0d02d5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setTitle(I)V

    goto/16 :goto_0

    .line 145
    :cond_7
    const v0, 0x7f0d01d8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->setTitle(I)V

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onPause()V

    .line 168
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->f:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 169
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 153
    iget-boolean v0, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    if-eqz v0, :cond_0

    .line 154
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->f:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    .line 158
    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->f:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 160
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxChooserActivity;->e()V

    .line 163
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 174
    const-string v0, "SIS_KEY_mShowingOffline"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/DropboxChooserActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    return-void
.end method
