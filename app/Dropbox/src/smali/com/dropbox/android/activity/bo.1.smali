.class final Lcom/dropbox/android/activity/bo;
.super Ldbxyzptlk/db231222/g/c;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/util/DropboxPath;

.field private final b:Lcom/dropbox/android/activity/bq;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/activity/bq;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 0

    .prologue
    .line 733
    invoke-direct {p0, p1, p3}, Ldbxyzptlk/db231222/g/c;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;)V

    .line 734
    iput-object p4, p0, Lcom/dropbox/android/activity/bo;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 735
    iput-object p2, p0, Lcom/dropbox/android/activity/bo;->b:Lcom/dropbox/android/activity/bq;

    .line 736
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 1

    .prologue
    .line 745
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 746
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 739
    const v0, 0x7f0d027b

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 741
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 742
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 750
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/bo;->b(Landroid/content/Context;)V

    .line 751
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 771
    sget-object v0, Lcom/dropbox/android/activity/bd;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/bo;->b:Lcom/dropbox/android/activity/bq;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 782
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 774
    :pswitch_0
    const v0, 0x7f0d028d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 785
    :goto_0
    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 787
    check-cast p1, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {p1}, Lcom/dropbox/android/activity/bo;->a(Lcom/dropbox/android/activity/GalleryActivity;)V

    .line 788
    return-void

    .line 778
    :pswitch_1
    const v0, 0x7f0d00a4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/bo;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 771
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 726
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bo;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 755
    check-cast p1, Lcom/dropbox/android/activity/GalleryActivity;

    .line 757
    invoke-virtual {p1}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    .line 758
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->i()V

    .line 760
    iget-object v1, p0, Lcom/dropbox/android/activity/bo;->b:Lcom/dropbox/android/activity/bq;

    sget-object v2, Lcom/dropbox/android/activity/bq;->c:Lcom/dropbox/android/activity/bq;

    if-ne v1, v2, :cond_0

    .line 761
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 764
    :cond_0
    invoke-static {p1}, Lcom/dropbox/android/activity/bo;->a(Lcom/dropbox/android/activity/GalleryActivity;)V

    .line 765
    return-void
.end method
