.class Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;
.super Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529
    new-instance v0, Lcom/dropbox/android/activity/dw;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dw;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 523
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 524
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    .line 525
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->b:Ljava/lang/String;

    .line 526
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 495
    iput-object p1, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    .line 496
    iput-object p2, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->b:Ljava/lang/String;

    .line 497
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;)V
    .locals 2

    .prologue
    .line 506
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 507
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/PhotoGridFragment;

    .line 508
    invoke-static {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h(Lcom/dropbox/android/activity/PhotoGridFragment;)Lcom/dropbox/android/albums/o;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 509
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 519
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragment$LightweightSharePickerSpec;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 520
    return-void
.end method
