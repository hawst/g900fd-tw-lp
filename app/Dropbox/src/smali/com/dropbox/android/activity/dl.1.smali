.class final Lcom/dropbox/android/activity/dl;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Lcom/dropbox/android/albums/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/PhotoGridFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PhotoGridFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/dropbox/android/activity/dl;->b:Lcom/dropbox/android/activity/PhotoGridFragment;

    iput-object p6, p0, Lcom/dropbox/android/activity/dl;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/albums/h;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/dropbox/android/activity/dl;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p2, Lcom/dropbox/android/albums/h;->a:Lcom/dropbox/android/albums/Album;

    iget-object v2, p2, Lcom/dropbox/android/albums/h;->b:Ljava/util/Collection;

    invoke-virtual {v0, v1, v2, p1}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    check-cast p2, Lcom/dropbox/android/albums/h;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/dl;->a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/albums/h;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d028c

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->a(I)V

    .line 140
    return-void
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 128
    iget-object v0, p0, Lcom/dropbox/android/activity/dl;->b:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->a(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    .line 130
    check-cast p2, Landroid/os/Bundle;

    .line 131
    const-string v0, "PATHS_TO_ADD"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 132
    const-string v0, "PATH_TO_SCROLL_TO"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 133
    iget-object v2, p0, Lcom/dropbox/android/activity/dl;->b:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v3, p0, Lcom/dropbox/android/activity/dl;->b:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v1}, Lcom/dropbox/android/util/DropboxPath;->b(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v3, p1, v1, v0}, Lcom/dropbox/android/activity/AlbumViewActivity;->a(Landroid/content/Context;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->startActivity(Landroid/content/Intent;)V

    .line 135
    return-void
.end method
