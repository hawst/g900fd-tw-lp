.class public Lcom/dropbox/android/activity/SearchFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dropbox/android/widget/as;->e:Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f030034

    return v0
.end method

.method protected final e()Ldbxyzptlk/db231222/n/r;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Ldbxyzptlk/db231222/n/r;->b:Ldbxyzptlk/db231222/n/r;

    return-object v0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->a()Lcom/dropbox/android/util/al;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/al;->b:Lcom/dropbox/android/util/al;

    if-ne v1, v2, :cond_0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 46
    const v0, 0x7f0d0054

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SearchFragment;->a(I)V

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l()V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->a()Lcom/dropbox/android/util/al;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/al;->b:Lcom/dropbox/android/util/al;

    if-ne v1, v2, :cond_0

    .line 28
    new-instance v1, Lcom/dropbox/android/filemanager/ak;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/SearchFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v4

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/dropbox/android/filemanager/ak;-><init>(Landroid/content/Context;Lcom/dropbox/android/provider/MetadataManager;Ldbxyzptlk/db231222/j/i;Ljava/lang/String;)V

    move-object v0, v1

    .line 32
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;

    move-result-object v0

    goto :goto_0
.end method
