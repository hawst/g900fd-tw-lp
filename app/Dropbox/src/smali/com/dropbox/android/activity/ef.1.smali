.class public final Lcom/dropbox/android/activity/ef;
.super Landroid/app/DialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 583
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 3

    .prologue
    .line 586
    new-instance v0, Lcom/dropbox/android/activity/ef;

    invoke-direct {v0}, Lcom/dropbox/android/activity/ef;-><init>()V

    .line 587
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/ef;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 588
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 592
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ef;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/PrefsActivity;

    .line 593
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 594
    const v2, 0x7f0d0216

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 595
    const v2, 0x7f0d0217

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 596
    const v2, 0x7f0d0218

    new-instance v3, Lcom/dropbox/android/activity/eg;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/eg;-><init>(Lcom/dropbox/android/activity/ef;Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 604
    const v0, 0x7f0d0219

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 605
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
