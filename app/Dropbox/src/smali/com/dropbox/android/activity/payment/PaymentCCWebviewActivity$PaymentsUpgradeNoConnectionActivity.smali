.class public Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/service/I;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;)Lcom/dropbox/android/service/I;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->a:Lcom/dropbox/android/service/I;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 465
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 466
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->finish()V

    .line 508
    :goto_0
    return-void

    .line 470
    :cond_0
    const v0, 0x7f0300d7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->setContentView(I)V

    .line 472
    const v0, 0x7f0d02d9

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->setTitle(I)V

    .line 473
    const v0, 0x7f0701d1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d02da

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 476
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 478
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 480
    new-instance v1, Lcom/dropbox/android/activity/payment/e;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/payment/e;-><init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->a:Lcom/dropbox/android/service/I;

    .line 507
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->a:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 512
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onDestroy()V

    .line 513
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity$PaymentsUpgradeNoConnectionActivity;->a:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 514
    return-void
.end method
