.class final Lcom/dropbox/android/activity/bR;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "panda.py"


# instance fields
.field private a:I

.field private b:Z

.field private final c:[Lcom/dropbox/android/activity/IntroTourFragment;


# direct methods
.method private constructor <init>(Landroid/support/v4/app/FragmentManager;Z)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 107
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/bR;->a:I

    .line 114
    invoke-virtual {p0}, Lcom/dropbox/android/activity/bR;->getCount()I

    move-result v0

    new-array v0, v0, [Lcom/dropbox/android/activity/IntroTourFragment;

    iput-object v0, p0, Lcom/dropbox/android/activity/bR;->c:[Lcom/dropbox/android/activity/IntroTourFragment;

    .line 115
    iput-boolean p2, p0, Lcom/dropbox/android/activity/bR;->b:Z

    .line 116
    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/app/FragmentManager;ZLcom/dropbox/android/activity/bQ;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/bR;-><init>(Landroid/support/v4/app/FragmentManager;Z)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/dropbox/android/activity/IntroTourFragment;
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/dropbox/android/activity/bR;->b:Z

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/IntroTourFragment;->a(IZ)Lcom/dropbox/android/activity/IntroTourFragment;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/dropbox/android/activity/bW;
    .locals 2

    .prologue
    .line 150
    iget v0, p0, Lcom/dropbox/android/activity/bR;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 151
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dropbox/android/activity/bW;->values()[Lcom/dropbox/android/activity/bW;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/activity/bR;->a:I

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 120
    invoke-static {}, Lcom/dropbox/android/activity/bW;->values()[Lcom/dropbox/android/activity/bW;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final synthetic getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/bR;->a(I)Lcom/dropbox/android/activity/IntroTourFragment;

    move-result-object v0

    return-object v0
.end method

.method public final instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/IntroTourFragment;

    .line 132
    iget-object v1, p0, Lcom/dropbox/android/activity/bR;->c:[Lcom/dropbox/android/activity/IntroTourFragment;

    aput-object v0, v1, p2

    .line 133
    return-object v0
.end method

.method public final setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentPagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 139
    iget v0, p0, Lcom/dropbox/android/activity/bR;->a:I

    if-eq p2, v0, :cond_1

    .line 140
    iget v0, p0, Lcom/dropbox/android/activity/bR;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/activity/bR;->c:[Lcom/dropbox/android/activity/IntroTourFragment;

    iget v1, p0, Lcom/dropbox/android/activity/bR;->a:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/dropbox/android/activity/IntroTourFragment;->c()V

    .line 143
    :cond_0
    iput p2, p0, Lcom/dropbox/android/activity/bR;->a:I

    .line 144
    iget-object v0, p0, Lcom/dropbox/android/activity/bR;->c:[Lcom/dropbox/android/activity/IntroTourFragment;

    iget v1, p0, Lcom/dropbox/android/activity/bR;->a:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/dropbox/android/activity/IntroTourFragment;->b()V

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/activity/bR;->a()Lcom/dropbox/android/activity/bW;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bW;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->f(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 147
    :cond_1
    return-void
.end method
