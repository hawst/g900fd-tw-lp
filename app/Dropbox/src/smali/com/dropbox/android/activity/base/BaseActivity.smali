.class public abstract Lcom/dropbox/android/activity/base/BaseActivity;
.super Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/base/h;
.implements Lcom/dropbox/android/activity/base/l;


# instance fields
.field public c:Lcom/dropbox/android/activity/base/d;

.field protected d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;-><init>()V

    .line 14
    new-instance v0, Lcom/dropbox/android/activity/base/d;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/base/d;-><init>(Lcom/dropbox/android/activity/base/h;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->d:Z

    return-void
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public a_()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 89
    return-object p0
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/activity/base/d;->a(IILandroid/content/Intent;)V

    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 72
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Landroid/os/Bundle;)V

    .line 22
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onDestroy()V

    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->f()V

    .line 52
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 94
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->finish()V

    .line 96
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onPause()V

    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->e()V

    .line 40
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onResume()V

    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->c()V

    .line 34
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->b(Landroid/os/Bundle;)V

    .line 116
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onStart()V

    .line 27
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->b()V

    .line 28
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->onStop()V

    .line 45
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->d()V

    .line 46
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 57
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Landroid/content/Intent;)V

    .line 58
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/droidfu/BetterSherlockFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/d;->a(Landroid/content/Intent;)V

    .line 64
    return-void
.end method
