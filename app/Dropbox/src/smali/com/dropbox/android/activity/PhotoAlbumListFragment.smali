.class public Lcom/dropbox/android/activity/PhotoAlbumListFragment;
.super Lcom/dropbox/android/activity/UserSweetListFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/UserSweetListFragment",
        "<",
        "Lcom/dropbox/android/widget/g;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;-><init>()V

    .line 237
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 181
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->an()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->d:Z

    .line 183
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 184
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoAlbumListFragment;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->a()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/albums/PhotosModel;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 138
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 139
    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 140
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "EXTRA_SILENCE_LIGHTWEIGHT_ALBUMS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 141
    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->c:Landroid/view/View;

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 146
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/g;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/g;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 147
    return-void

    :cond_1
    move v0, v1

    .line 141
    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 126
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 127
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 65
    new-instance v1, Lcom/dropbox/android/widget/g;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/dropbox/android/activity/dd;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/dd;-><init>(Lcom/dropbox/android/activity/PhotoAlbumListFragment;)V

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/dropbox/android/widget/g;-><init>(Landroid/content/Context;Lcom/dropbox/android/activity/di;Lcom/dropbox/android/taskqueue/D;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    .line 73
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lcom/dropbox/android/activity/dg;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->d:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/dg;-><init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Z)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 77
    const v0, 0x7f03006c

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Landroid/view/View;Z)V

    .line 81
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/SweetListView;->setSweetAdapter(Lcom/dropbox/android/widget/bC;)V

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->n:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/SweetListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 85
    const v0, 0x7f070061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->a:Landroid/view/View;

    .line 87
    const v0, 0x7f070138

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->b:Landroid/view/View;

    .line 89
    const v0, 0x7f070139

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->c:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->c:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/de;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/de;-><init>(Lcom/dropbox/android/activity/PhotoAlbumListFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 102
    :cond_0
    return-object v1
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/g;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/g;->g()Landroid/database/Cursor;

    move-result-object v1

    .line 157
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/g;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/g;->g()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 160
    :try_start_0
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v1, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 161
    sget-object v3, Lcom/dropbox/android/activity/df;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 172
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected item type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :catchall_0
    move-exception v0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v0

    .line 163
    :pswitch_0
    :try_start_1
    invoke-static {v1}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    .line 164
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 165
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/dropbox/android/activity/AlbumViewActivity;->a(Landroid/content/Context;Lcom/dropbox/android/albums/Album;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 168
    :pswitch_1
    :try_start_2
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onResume()V

    .line 108
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 109
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onStart()V

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/g;->a(Z)V

    .line 115
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onStop()V

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoAlbumListFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/g;->a(Z)V

    .line 121
    return-void
.end method
