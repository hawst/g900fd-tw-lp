.class final Lcom/dropbox/android/activity/dialog/H;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:[Ldbxyzptlk/db231222/n/r;

.field final synthetic c:Ldbxyzptlk/db231222/n/K;

.field final synthetic d:Lcom/dropbox/android/activity/dialog/SortOrderDialog;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/SortOrderDialog;I[Ldbxyzptlk/db231222/n/r;Ldbxyzptlk/db231222/n/K;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/H;->d:Lcom/dropbox/android/activity/dialog/SortOrderDialog;

    iput p2, p0, Lcom/dropbox/android/activity/dialog/H;->a:I

    iput-object p3, p0, Lcom/dropbox/android/activity/dialog/H;->b:[Ldbxyzptlk/db231222/n/r;

    iput-object p4, p0, Lcom/dropbox/android/activity/dialog/H;->c:Ldbxyzptlk/db231222/n/K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 60
    iget v0, p0, Lcom/dropbox/android/activity/dialog/H;->a:I

    if-eq p2, v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/H;->b:[Ldbxyzptlk/db231222/n/r;

    aget-object v0, v0, p2

    .line 62
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/H;->c:Ldbxyzptlk/db231222/n/K;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/K;->c()Ldbxyzptlk/db231222/n/r;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 63
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/H;->c:Ldbxyzptlk/db231222/n/K;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/K;->a(Ldbxyzptlk/db231222/n/r;)V

    .line 64
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/H;->d:Lcom/dropbox/android/activity/dialog/SortOrderDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/H;->d:Lcom/dropbox/android/activity/dialog/SortOrderDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/I;

    invoke-interface {v0}, Lcom/dropbox/android/activity/dialog/I;->d()V

    .line 68
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 69
    return-void
.end method
