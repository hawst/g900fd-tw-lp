.class public Lcom/dropbox/android/activity/ShareViaEmailFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/filemanager/I;

.field private c:Lcom/dropbox/android/filemanager/LocalEntry;

.field private d:Landroid/view/View;

.field private e:Lcom/dropbox/android/activity/eL;

.field private f:Ljava/lang/String;

.field private g:Lcom/dropbox/android/widget/ContactEditTextView;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/TextView;

.field private j:Lcom/actionbarsherlock/view/MenuItem;

.field private final k:Lcom/dropbox/android/taskqueue/P;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 75
    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b:Lcom/dropbox/android/filemanager/I;

    .line 76
    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 77
    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->d:Landroid/view/View;

    .line 259
    new-instance v0, Lcom/dropbox/android/activity/eQ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eQ;-><init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->k:Lcom/dropbox/android/taskqueue/P;

    .line 449
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 3

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 220
    const-string v1, "EXTRA_SHARE_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/activity/eK;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/eK;

    move-result-object v1

    .line 221
    sget-object v2, Lcom/dropbox/android/activity/eK;->b:Lcom/dropbox/android/activity/eK;

    if-ne v1, v2, :cond_0

    .line 222
    const-string v1, "EXTRA_ALBUM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    .line 223
    invoke-direct {p0, p1, v0, p2}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a(Landroid/view/ViewGroup;Lcom/dropbox/android/albums/Album;Landroid/view/LayoutInflater;)V

    .line 230
    :goto_0
    return-void

    .line 224
    :cond_0
    sget-object v2, Lcom/dropbox/android/activity/eK;->a:Lcom/dropbox/android/activity/eK;

    if-ne v1, v2, :cond_1

    .line 225
    const-string v1, "EXTRA_LOCALENTRY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 226
    invoke-direct {p0, p1, v0, p2}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a(Landroid/view/ViewGroup;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/view/LayoutInflater;)V

    goto :goto_0

    .line 228
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unrecognized share type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Landroid/view/ViewGroup;Lcom/dropbox/android/albums/Album;Landroid/view/LayoutInflater;)V
    .locals 6

    .prologue
    .line 305
    const v0, 0x7f030094

    invoke-virtual {p3, v0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 306
    const v0, 0x7f07018b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 307
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    const v0, 0x7f07018a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 310
    invoke-virtual {p2}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    const v0, 0x7f07018c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/HorizontalListView;

    .line 314
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    .line 315
    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->setDividerWidth(I)V

    .line 317
    new-instance v1, Lcom/dropbox/android/activity/eL;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/activity/eL;-><init>(Landroid/content/Context;Lcom/dropbox/android/taskqueue/D;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    .line 319
    iget-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 320
    new-instance v1, Lcom/dropbox/android/activity/eS;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/eS;-><init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;Lcom/dropbox/android/widget/HorizontalListView;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/HorizontalListView;->setOnLayoutChangedListener(Lcom/dropbox/android/widget/aM;)V

    .line 327
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    .line 328
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/dropbox/android/activity/eT;

    invoke-direct {v5, p0, v1, p2, v0}, Lcom/dropbox/android/activity/eT;-><init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/widget/HorizontalListView;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 345
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/view/LayoutInflater;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 233
    const v0, 0x7f030095

    invoke-virtual {p3, v0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 235
    const v0, 0x7f0700db

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 237
    const v1, 0x7f07018b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 238
    iget-object v3, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    const v1, 0x7f07018d

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 241
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iput-object p2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 244
    iget-boolean v1, p2, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v1, :cond_0

    .line 245
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    .line 247
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->k:Lcom/dropbox/android/taskqueue/P;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/ref/WeakReference;)V

    .line 248
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c()V

    .line 257
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-static {v1, v0, v3, v4}, Lcom/dropbox/android/widget/aY;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 251
    const v0, 0x7f0700d9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 252
    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 254
    const v0, 0x7f0700d8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 255
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Lcom/dropbox/android/activity/eL;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 284
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    .line 285
    sget-object v1, Lcom/dropbox/android/taskqueue/I;->b:Lcom/dropbox/android/taskqueue/I;

    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/taskqueue/I;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)Landroid/util/Pair;

    move-result-object v0

    .line 288
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 293
    iget-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->d:Landroid/view/View;

    const v2, 0x7f0700da

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 294
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 295
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    .line 298
    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->d:Landroid/view/View;

    const v1, 0x7f07005e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 300
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 403
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    .line 406
    array-length v0, v1

    if-nez v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    const v1, 0x7f0d02bd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    array-length v2, v1

    move v0, v7

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 414
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    const v1, 0x7f0d02be

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 416
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 413
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 421
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 422
    array-length v2, v1

    move v0, v7

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 423
    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 426
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 428
    new-instance v0, Lcom/dropbox/android/activity/eW;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/eW;-><init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 430
    invoke-virtual {v0}, Lcom/dropbox/android/activity/eW;->f()V

    .line 431
    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/eW;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 433
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    if-eqz v0, :cond_7

    .line 434
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aO()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_recipients"

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "has_message"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v6

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "mime"

    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "extension"

    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_0

    :cond_6
    move v0, v7

    goto :goto_3

    .line 440
    :cond_7
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    if-eqz v0, :cond_0

    .line 441
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aP()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_recipients"

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "has_message"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_8

    :goto_4
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_items"

    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/eL;->getCount()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_0

    :cond_8
    move v6, v7

    goto :goto_4
.end method

.method static synthetic d(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->d()V

    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 157
    invoke-direct {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b()V

    goto :goto_0

    .line 161
    :cond_1
    invoke-static {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment$ConfirmLeaveDialog;->b(Lcom/dropbox/android/activity/ShareViaEmailFragment;)Lcom/dropbox/android/activity/ShareViaEmailFragment$ConfirmLeaveDialog;

    move-result-object v0

    .line 162
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/ShareViaEmailFragment$ConfirmLeaveDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 94
    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b:Lcom/dropbox/android/filemanager/I;

    .line 100
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->setHasOptionsMenu(Z)V

    .line 102
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 103
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 378
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V

    .line 380
    const v2, 0x7f0d02b9

    invoke-interface {p1, v1, v0, v1, v2}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    const v3, 0x7f02026f

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/dropbox/android/activity/eU;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/eU;-><init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setOnMenuItemClickListener(Lcom/actionbarsherlock/view/MenuItem$OnMenuItemClickListener;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->j:Lcom/actionbarsherlock/view/MenuItem;

    .line 389
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->j:Lcom/actionbarsherlock/view/MenuItem;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 393
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    if-eqz v2, :cond_0

    .line 394
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/ContactEditTextView;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 395
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->j:Lcom/actionbarsherlock/view/MenuItem;

    invoke-interface {v1, v0}, Lcom/actionbarsherlock/view/MenuItem;->setEnabled(Z)Lcom/actionbarsherlock/view/MenuItem;

    .line 396
    iget-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->j:Lcom/actionbarsherlock/view/MenuItem;

    invoke-interface {v1}, Lcom/actionbarsherlock/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v0, :cond_2

    const/16 v0, 0xff

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 398
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 394
    goto :goto_0

    .line 396
    :cond_2
    const/16 v0, 0x80

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 167
    const v0, 0x7f030093

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 168
    iput-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->d:Landroid/view/View;

    .line 170
    const v0, 0x7f070189

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    .line 172
    const v0, 0x7f070188

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->h:Landroid/widget/EditText;

    .line 173
    const v0, 0x7f070186

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ContactEditTextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    .line 174
    const v0, 0x7f070187

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    .line 176
    if-eqz p3, :cond_0

    const-string v0, "SIS_KEY_ERROR_MESSAGE"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    const-string v2, "SIS_KEY_ERROR_MESSAGE"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    :cond_0
    const v0, 0x7f070185

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 183
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/ContactEditTextView;->getTextSize()F

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    new-instance v2, Lcom/dropbox/android/activity/eO;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/eO;-><init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/ContactEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 211
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08006e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 212
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 213
    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->g:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v2, v0}, Lcom/dropbox/android/widget/ContactEditTextView;->setHintTextColor(I)V

    .line 215
    return-object v1
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 368
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDestroyView()V

    .line 370
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->b:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->k:Lcom/dropbox/android/taskqueue/P;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/taskqueue/D;->a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/taskqueue/P;)V

    .line 374
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 109
    const-string v0, "SIS_KEY_ERROR_MESSAGE"

    iget-object v1, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStart()V

    .line 116
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/eL;->a()V

    .line 119
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStop()V

    .line 124
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->e:Lcom/dropbox/android/activity/eL;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/eL;->b()V

    .line 127
    :cond_0
    return-void
.end method
