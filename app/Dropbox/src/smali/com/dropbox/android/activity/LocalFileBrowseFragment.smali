.class public Lcom/dropbox/android/activity/LocalFileBrowseFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/util/HistoryStack;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ListView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageButton;

.field private g:Lcom/dropbox/android/widget/be;

.field private h:Lcom/dropbox/android/activity/ce;

.field private i:Landroid/os/Bundle;

.field private final j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/dropbox/android/activity/cf;

.field private final l:Landroid/widget/AdapterView$OnItemClickListener;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 71
    sget-object v0, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    .line 97
    new-instance v0, Lcom/dropbox/android/activity/bY;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bY;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->l:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 272
    if-nez p1, :cond_0

    .line 273
    iget-object p1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Landroid/os/Bundle;

    .line 274
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Landroid/os/Bundle;

    .line 277
    :cond_0
    if-eqz p1, :cond_1

    .line 279
    const-string v0, "key_Mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    :try_start_0
    const-string v0, "key_Mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/ce;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/ce;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 282
    :catch_0
    move-exception v0

    .line 283
    sget-object v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/lang/String;

    const-string v1, "Invalid browse mode, or browse mode extra not specified"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    sget-object v0, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    goto :goto_0

    .line 287
    :cond_2
    sget-object v0, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    goto :goto_0
.end method

.method private a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/be;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 300
    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 302
    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    .line 303
    sget-object v2, Lcom/dropbox/android/activity/cd;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 321
    const-string v1, "is_dir"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 322
    if-eqz v1, :cond_1

    .line 323
    const-string v1, "uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 324
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/net/Uri;)V

    .line 338
    :goto_0
    return-void

    .line 306
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    .line 307
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v0

    .line 311
    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2, v0}, Lcom/dropbox/android/util/HistoryStack;->a(I)Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->d()Lcom/dropbox/android/util/HistoryEntry;

    .line 318
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    goto :goto_0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/HistoryStack;->a(Landroid/widget/ListView;)V

    .line 315
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    new-instance v2, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    invoke-direct {v2, v1}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    goto :goto_1

    .line 326
    :cond_1
    new-instance v1, Ljava/io/File;

    const-string v2, "path"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 327
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 328
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 333
    :goto_2
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, p1, v2, v0}, Lcom/dropbox/android/widget/be;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 334
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i()V

    goto :goto_0

    .line 330
    :cond_2
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LocalFileBrowseFragment;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)Lcom/dropbox/android/util/HistoryStack;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)Lcom/dropbox/android/activity/cf;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    .line 86
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;)Z

    move-result v0

    .line 92
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 93
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 95
    :cond_0
    return-void

    .line 93
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h()V

    return-void
.end method

.method private e()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 218
    sget-object v0, Lcom/dropbox/android/activity/ce;->b:Lcom/dropbox/android/activity/ce;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    if-ne v0, v1, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->k()Landroid/net/Uri;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    .line 222
    :try_start_0
    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    :goto_0
    return-object v0

    .line 225
    :catch_0
    move-exception v0

    .line 230
    :cond_0
    invoke-static {}, Lcom/dropbox/android/provider/FileSystemProvider;->a()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g()V

    return-void
.end method

.method private final f()V
    .locals 3

    .prologue
    .line 234
    new-instance v0, Lcom/dropbox/android/util/HistoryStack;

    invoke-direct {v0}, Lcom/dropbox/android/util/HistoryStack;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    .line 235
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    new-instance v1, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    .line 236
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    if-eqz v0, :cond_0

    .line 246
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 248
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-interface {v1, v0, v2}, Lcom/dropbox/android/activity/cf;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Set;)V

    .line 250
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    .line 266
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/dropbox/android/activity/cf;->a(Landroid/net/Uri;)V

    .line 268
    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 341
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 342
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 345
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;I)Ljava/lang/String;

    move-result-object v0

    .line 346
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    :cond_1
    return-void

    .line 341
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    invoke-interface {v0}, Lcom/dropbox/android/activity/cf;->e_()V

    .line 354
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->a(Landroid/widget/ListView;)V

    .line 240
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    new-instance v1, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;

    .line 241
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 242
    return-void
.end method

.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/16 v4, -0x3e7

    .line 372
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/be;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 373
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d()V

    .line 375
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/be;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ldbxyzptlk/db231222/k/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/TextView;

    const v1, 0x7f0d005a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 384
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 390
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 391
    iget v1, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    if-ltz v1, :cond_3

    .line 392
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    iget v2, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    iget v3, v0, Lcom/dropbox/android/util/HistoryEntry;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 398
    iput v4, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 405
    :cond_0
    :goto_2
    return-void

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/TextView;

    const v1, 0x7f0d0052

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 399
    :cond_3
    iget v1, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    if-eq v1, v4, :cond_0

    .line 402
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 403
    iput v4, v0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    goto :goto_2
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 253
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v2}, Lcom/dropbox/android/util/HistoryStack;->c()I

    move-result v2

    if-gt v2, v1, :cond_0

    .line 260
    :goto_0
    return v0

    .line 257
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v2}, Lcom/dropbox/android/util/HistoryStack;->d()Lcom/dropbox/android/util/HistoryEntry;

    .line 258
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    move v0, v1

    .line 260
    goto :goto_0
.end method

.method protected final b()Z
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    sget-object v1, Lcom/dropbox/android/activity/ce;->b:Lcom/dropbox/android/activity/ce;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c()Z
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    sget-object v1, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 110
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d()V

    .line 114
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 115
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onAttach(Landroid/app/Activity;)V

    .line 121
    const-class v0, Lcom/dropbox/android/activity/cf;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 122
    check-cast p1, Lcom/dropbox/android/activity/cf;

    iput-object p1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    .line 123
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Landroid/os/Bundle;

    .line 137
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/os/Bundle;)V

    .line 139
    new-instance v0, Lcom/dropbox/android/widget/be;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b()Z

    move-result v4

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c()Z

    move-result v5

    iget-object v6, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Ljava/util/HashSet;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/be;-><init>(Landroid/content/Context;Landroid/database/Cursor;IZZLjava/util/Set;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    .line 147
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f()V

    .line 148
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 358
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    .line 359
    new-instance v0, Lcom/dropbox/android/filemanager/W;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v2}, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->f()Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/filemanager/W;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 153
    const v0, 0x7f030054

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 155
    const v0, 0x7f0700ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/ImageButton;

    .line 157
    const v0, 0x7f0700e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    .line 158
    const v0, 0x7f0700aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Landroid/widget/TextView;

    .line 159
    const v0, 0x7f0700e2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/TextView;

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 162
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->l:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 164
    const v0, 0x7f07004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 165
    const v2, 0x7f0d0013

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 166
    new-instance v2, Lcom/dropbox/android/activity/bZ;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bZ;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    const v0, 0x7f07004d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    .line 176
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/ImageButton;

    new-instance v2, Lcom/dropbox/android/activity/ca;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/ca;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    const v2, 0x7f0d00e4

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 186
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/cb;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cb;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    :goto_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d()V

    .line 208
    return-object v1

    .line 193
    :cond_0
    const v0, 0x7f070146

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->n:Landroid/widget/TextView;

    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    const v2, 0x7f0d00e5

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/cc;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cc;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i()V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDetach()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/cf;

    .line 129
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 412
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Lcom/dropbox/android/widget/be;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/be;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 413
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 295
    const-string v0, "key_Mode"

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/activity/ce;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/ce;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Lcom/dropbox/android/util/HistoryStack;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/HistoryStack;->a(Landroid/widget/ListView;)V

    .line 214
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStop()V

    .line 215
    return-void
.end method
