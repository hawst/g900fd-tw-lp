.class final Lcom/dropbox/android/activity/l;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/actionbarsherlock/view/ActionMode$Callback;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(IIIIZ)Landroid/view/View;
    .locals 6

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v5, 0x1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;IIIZZ)Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    move-result-object v0

    .line 1034
    new-instance v1, Lcom/dropbox/android/activity/m;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/m;-><init>(Lcom/dropbox/android/activity/l;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1040
    return-object v0
.end method


# virtual methods
.method public final a(I)Z
    .locals 3

    .prologue
    .line 1059
    packed-switch p1, :pswitch_data_0

    .line 1065
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected menu id"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1061
    :pswitch_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->am()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "target"

    const-string v2, "remove"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1062
    iget-object v0, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->a()V

    .line 1063
    const/4 v0, 0x1

    return v0

    .line 1059
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onActionItemClicked(Lcom/actionbarsherlock/view/ActionMode;Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1055
    invoke-interface {p2}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/l;->a(I)Z

    move-result v0

    return v0
.end method

.method public final onCreateActionMode(Lcom/actionbarsherlock/view/ActionMode;Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1

    .prologue
    .line 1050
    const/4 v0, 0x1

    return v0
.end method

.method public final onDestroyActionMode(Lcom/actionbarsherlock/view/ActionMode;)V
    .locals 1

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->r(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    .line 1046
    return-void
.end method

.method public final onPrepareActionMode(Lcom/actionbarsherlock/view/ActionMode;Lcom/actionbarsherlock/view/Menu;)Z
    .locals 8

    .prologue
    const v2, 0x7f0d0291

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1007
    iget-object v0, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->j(Lcom/dropbox/android/activity/AlbumViewFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v3, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/os/Handler;Landroid/app/Activity;)V

    .line 1008
    iget-object v0, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/activity/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->b()I

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/actionbarsherlock/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 1009
    invoke-interface {p2}, Lcom/actionbarsherlock/view/Menu;->clear()V

    .line 1020
    invoke-interface {p2, v1, v1, v1, v2}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v7

    const v3, 0x7f08008d

    const v4, 0x7f02013e

    iget-object v0, p0, Lcom/dropbox/android/activity/l;->a:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/activity/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->b()I

    move-result v0

    if-lez v0, :cond_0

    move v5, v6

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/l;->a(IIIIZ)Landroid/view/View;

    move-result-object v0

    invoke-interface {v7, v0}, Lcom/actionbarsherlock/view/MenuItem;->setActionView(Landroid/view/View;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 1028
    return v6

    :cond_0
    move v5, v1

    .line 1020
    goto :goto_0
.end method
