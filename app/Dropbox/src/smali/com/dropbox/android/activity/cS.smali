.class final Lcom/dropbox/android/activity/cS;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/graphics/Typeface;

.field final synthetic b:Lcom/dropbox/android/activity/NewAccountFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/NewAccountFragment;Landroid/graphics/Typeface;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/dropbox/android/activity/cS;->b:Lcom/dropbox/android/activity/NewAccountFragment;

    iput-object p2, p0, Lcom/dropbox/android/activity/cS;->a:Landroid/graphics/Typeface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 278
    .line 280
    iget-object v0, p0, Lcom/dropbox/android/activity/cS;->b:Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/NewAccountFragment;->c(Lcom/dropbox/android/activity/NewAccountFragment;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz p2, :cond_0

    const/16 v0, 0x91

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/activity/cS;->b:Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/NewAccountFragment;->c(Lcom/dropbox/android/activity/NewAccountFragment;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/cS;->b:Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/NewAccountFragment;->c(Lcom/dropbox/android/activity/NewAccountFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 282
    iget-object v0, p0, Lcom/dropbox/android/activity/cS;->b:Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/NewAccountFragment;->c(Lcom/dropbox/android/activity/NewAccountFragment;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/cS;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 283
    return-void

    .line 280
    :cond_0
    const/16 v0, 0x81

    goto :goto_0
.end method
