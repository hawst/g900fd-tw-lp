.class public abstract Lcom/dropbox/android/activity/PhotoGridFragmentBase;
.super Lcom/dropbox/android/activity/UserSweetListFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/dropbox/android/widget/bY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/UserSweetListFragment",
        "<",
        "Lcom/dropbox/android/widget/bi;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/dropbox/android/widget/bY;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field protected b:Landroid/widget/TextView;

.field protected c:Landroid/widget/TextView;

.field protected d:Landroid/widget/ImageView;

.field protected e:Landroid/view/View;

.field protected f:Landroid/view/View;

.field protected g:Landroid/widget/Button;

.field protected h:Lcom/dropbox/android/activity/dE;

.field protected i:Landroid/os/Handler;

.field protected final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Lcom/dropbox/android/filemanager/LocalEntry;",
            ">;"
        }
    .end annotation
.end field

.field private p:Landroid/widget/ProgressBar;

.field private q:I

.field private r:Lcom/dropbox/android/activity/dD;

.field private s:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/dropbox/android/widget/bZ;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ldbxyzptlk/db231222/g/U;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;-><init>()V

    .line 93
    sget-object v0, Lcom/dropbox/android/activity/dE;->d:Lcom/dropbox/android/activity/dE;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->h:Lcom/dropbox/android/activity/dE;

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->i:Landroid/os/Handler;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    .line 117
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    .line 118
    iput-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->s:Landroid/util/Pair;

    .line 121
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->t:Ljava/util/HashSet;

    .line 179
    new-instance v0, Lcom/dropbox/android/activity/dx;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dx;-><init>(Lcom/dropbox/android/activity/PhotoGridFragmentBase;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->u:Ldbxyzptlk/db231222/g/U;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Landroid/util/Pair;)Landroid/util/Pair;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->s:Landroid/util/Pair;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoGridFragmentBase;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->p:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Lcom/dropbox/android/activity/dD;)Lcom/dropbox/android/activity/dD;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    return-object p1
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;I)V
    .locals 2

    .prologue
    .line 446
    div-int/lit16 v0, p2, 0x1388

    .line 447
    mul-int/lit16 v0, v0, 0x1388

    iput v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    .line 448
    sget-object v0, Lcom/dropbox/android/activity/dD;->c:Lcom/dropbox/android/activity/dD;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    .line 449
    new-instance v0, Landroid/util/Pair;

    iget v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    sub-int v1, p2, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->s:Landroid/util/Pair;

    .line 450
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k()V

    .line 451
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/PhotoGridFragmentBase;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    return v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 414
    invoke-static {}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a()Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PAGE_LOAD_STATUS_FRAG_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 415
    iget v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    add-int/lit16 v0, v0, 0x1388

    iput v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    .line 416
    sget-object v0, Lcom/dropbox/android/activity/dD;->a:Lcom/dropbox/android/activity/dD;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    .line 417
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k()V

    .line 418
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 421
    invoke-static {}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a()Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PAGE_LOAD_STATUS_FRAG_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 422
    const/4 v0, 0x0

    iget v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    add-int/lit16 v1, v1, -0x1388

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    .line 423
    sget-object v0, Lcom/dropbox/android/activity/dD;->b:Lcom/dropbox/android/activity/dD;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    .line 424
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k()V

    .line 425
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 431
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 432
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 433
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)Lcom/dropbox/android/util/DropboxPath;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)",
            "Lcom/dropbox/android/util/DropboxPath;"
        }
    .end annotation

    .prologue
    .line 586
    const/4 v2, 0x0

    .line 587
    new-instance v4, Lcom/dropbox/android/widget/bE;

    invoke-direct {v4}, Lcom/dropbox/android/widget/bE;-><init>()V

    .line 588
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bi;

    .line 590
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v1, v4}, Lcom/dropbox/android/widget/SweetListView;->a(Lcom/dropbox/android/widget/bE;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 591
    iget v1, v4, Lcom/dropbox/android/widget/bE;->a:I

    move-object v3, v2

    move v2, v1

    :goto_0
    iget v1, v4, Lcom/dropbox/android/widget/bE;->b:I

    if-gt v2, v1, :cond_2

    .line 592
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/util/DropboxPath;

    .line 593
    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/bi;->c(I)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v6

    .line 594
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 595
    invoke-virtual {v6}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    .line 600
    :cond_1
    if-eqz v3, :cond_3

    .line 606
    :cond_2
    :goto_1
    return-object v3

    .line 591
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_4
    move-object v3, v2

    goto :goto_1
.end method

.method protected abstract a()V
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 286
    packed-switch p1, :pswitch_data_0

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 291
    :pswitch_0
    if-eqz p3, :cond_0

    .line 292
    const-string v0, "FINAL_IMAGE_PATH"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 301
    const-string v0, "FINAL_IMAGE_INDEX"

    const/4 v1, -0x1

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 302
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    const-string v2, "FINAL_IMAGE_PATH"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    .line 304
    if-ltz v0, :cond_0

    if-eqz v1, :cond_0

    .line 307
    iget v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    if-lt v0, v2, :cond_1

    iget v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    add-int/lit16 v2, v2, 0x1388

    if-lt v0, v2, :cond_2

    .line 308
    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Lcom/dropbox/android/util/DropboxPath;I)V

    goto :goto_0

    .line 310
    :cond_2
    new-instance v2, Lcom/dropbox/android/activity/dA;

    invoke-direct {v2, p0, v1, v0}, Lcom/dropbox/android/activity/dA;-><init>(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Lcom/dropbox/android/util/DropboxPath;I)V

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 463
    invoke-static {p1}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 464
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 465
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 264
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->a(Landroid/os/Bundle;)V

    .line 266
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 267
    const-string v0, "SIS_KEY_SelectedItems"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/ParcelablePair;

    .line 269
    iget-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ParcelablePair;->a()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ParcelablePair;->b()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 271
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 520
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/bi;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 522
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    if-eqz v0, :cond_1

    .line 523
    sget-object v0, Lcom/dropbox/android/activity/dC;->c:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 534
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected scroll mode"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollToTop()V

    .line 537
    :goto_0
    iput-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->r:Lcom/dropbox/android/activity/dD;

    .line 547
    :cond_0
    :goto_1
    iput-object v3, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->m:Lcom/dropbox/android/util/aW;

    .line 548
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->requestLayout()V

    .line 550
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "PAGE_LOAD_STATUS_FRAG_TAG"

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 552
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a()V

    .line 553
    return-void

    .line 528
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollToBottom()V

    goto :goto_0

    .line 531
    :pswitch_2
    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->s:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->s:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;I)V

    goto :goto_0

    .line 538
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->m:Lcom/dropbox/android/util/aW;

    if-eqz v0, :cond_0

    .line 543
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->m:Lcom/dropbox/android/util/aW;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aW;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedRestorePositionFromTop(I)V

    goto :goto_1

    .line 523
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    .line 468
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 469
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 470
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->f()V

    .line 475
    return-void

    .line 472
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/widget/bZ;)V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->t:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t double register a listener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->t:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 569
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 581
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 481
    return-void
.end method

.method public final b(Lcom/dropbox/android/widget/bZ;)V
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->t:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t unregister a non-registered listener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->t:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 577
    return-void
.end method

.method protected abstract c()V
.end method

.method protected abstract d()Landroid/net/Uri;
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->t:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bZ;

    .line 456
    invoke-interface {v0}, Lcom/dropbox/android/widget/bZ;->a()V

    goto :goto_0

    .line 458
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->b()V

    .line 459
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 160
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 161
    if-nez v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 166
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/SweetListView;->setSweetAdapter(Lcom/dropbox/android/widget/bC;)V

    .line 167
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->n:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/SweetListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 168
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->o:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/SweetListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 169
    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->registerForContextMenu(Landroid/view/View;)V

    .line 170
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a()V

    .line 171
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->u:Ldbxyzptlk/db231222/g/U;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/U;)V

    .line 173
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->c()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 134
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    .line 135
    if-eqz v3, :cond_0

    .line 136
    new-instance v0, Lcom/dropbox/android/widget/bi;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v4

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v5

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/bi;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/filemanager/I;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    .line 143
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 502
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->d()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 503
    const-string v1, "PARAM_PAGE_SIZE"

    const/16 v2, 0x1388

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 504
    const-string v1, "PARAM_ITEM_OFFSET"

    iget v2, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 505
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 507
    new-instance v0, Landroid/support/v4/content/g;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/g;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 216
    const v0, 0x7f03001f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 218
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    .line 219
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/SweetListView;->setMinCols(I)V

    .line 220
    const v0, 0x7f070056

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->b:Landroid/widget/TextView;

    .line 221
    const v0, 0x7f070057

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->c:Landroid/widget/TextView;

    .line 222
    const v0, 0x7f07013d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->e:Landroid/view/View;

    .line 223
    const v0, 0x7f070061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->f:Landroid/view/View;

    .line 224
    const v0, 0x7f07013e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->d:Landroid/widget/ImageView;

    .line 226
    const v0, 0x7f07003e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->p:Landroid/widget/ProgressBar;

    .line 228
    const v0, 0x7f07013f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->g:Landroid/widget/Button;

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->g:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/dz;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dz;-><init>(Lcom/dropbox/android/activity/PhotoGridFragmentBase;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 252
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onDestroy()V

    .line 253
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bi;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 256
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->u:Ldbxyzptlk/db231222/g/U;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/R;->b(Ldbxyzptlk/db231222/g/U;)V

    .line 260
    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onDestroyView()V

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->unregisterForContextMenu(Landroid/view/View;)V

    .line 211
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onDetach()V

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bi;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 129
    :cond_0
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    .line 337
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bi;->g()Landroid/database/Cursor;

    move-result-object v6

    .line 338
    invoke-interface {v6, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 339
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v6, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    .line 340
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 342
    sget-object v2, Lcom/dropbox/android/activity/dC;->b:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 408
    sget-object v0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a:Ljava/lang/String;

    const-string v1, "Got unrecognized list item click."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :goto_0
    return-void

    .line 344
    :pswitch_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 345
    sget v1, Lcom/dropbox/android/provider/PhotosProvider;->e:I

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 346
    sget-object v1, Lcom/dropbox/android/provider/PhotosProvider;->b:Landroid/net/Uri;

    const-wide/16 v3, -0x1

    sget-object v5, Lcom/dropbox/android/activity/bq;->b:Lcom/dropbox/android/activity/bq;

    iget v7, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->q:I

    invoke-interface {v6}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    add-int/2addr v6, v7

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;JLcom/dropbox/android/activity/bq;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 352
    :cond_0
    invoke-virtual {p0, v6}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 364
    :pswitch_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 365
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v2

    .line 366
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/U;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 367
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->d:Lcom/dropbox/android/activity/payment/h;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v3

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->e()Z

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;Lcom/dropbox/android/util/analytics/r;Z)V

    goto :goto_0

    .line 370
    :cond_1
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/U;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/dropbox/android/util/S;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 379
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d0063

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d0064

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d0065

    new-instance v3, Lcom/dropbox/android/activity/dB;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/dB;-><init>(Lcom/dropbox/android/activity/PhotoGridFragmentBase;Ldbxyzptlk/db231222/r/d;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0066

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 397
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 398
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 402
    :pswitch_2
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j()V

    goto/16 :goto_0

    .line 405
    :pswitch_3
    invoke-direct {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->i()V

    goto/16 :goto_0

    .line 342
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 560
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bi;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 561
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->a()V

    .line 244
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onResume()V

    .line 245
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 247
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->j()V

    .line 248
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 275
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 276
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 277
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 278
    new-instance v4, Lcom/dropbox/android/util/ParcelablePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-direct {v4, v1, v0}, Lcom/dropbox/android/util/ParcelablePair;-><init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 280
    :cond_0
    const-string v0, "SIS_KEY_SelectedItems"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 281
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onStart()V

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bi;->a(Z)V

    .line 149
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onStop()V

    .line 154
    iget-object v0, p0, Lcom/dropbox/android/activity/PhotoGridFragmentBase;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/bi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/bi;->a(Z)V

    .line 155
    return-void
.end method
