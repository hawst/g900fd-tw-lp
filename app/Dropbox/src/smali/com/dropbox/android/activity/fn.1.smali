.class public abstract enum Lcom/dropbox/android/activity/fn;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/fn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/fn;

.field public static final enum b:Lcom/dropbox/android/activity/fn;

.field public static final enum c:Lcom/dropbox/android/activity/fn;

.field private static final synthetic d:[Lcom/dropbox/android/activity/fn;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    new-instance v0, Lcom/dropbox/android/activity/fo;

    const-string v1, "CAMERA_UPLOAD_THUMBS_WITH_SKIP"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/fo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/fn;->a:Lcom/dropbox/android/activity/fn;

    .line 151
    new-instance v0, Lcom/dropbox/android/activity/fq;

    const-string v1, "CAMERA_UPLOAD_THUMBS_WITH_CANCEL"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/fq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/fn;->b:Lcom/dropbox/android/activity/fn;

    .line 161
    new-instance v0, Lcom/dropbox/android/activity/fr;

    const-string v1, "CAMERA_UPLOAD_SS_OOBE"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/fr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/fn;->c:Lcom/dropbox/android/activity/fn;

    .line 139
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/activity/fn;

    sget-object v1, Lcom/dropbox/android/activity/fn;->a:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/fn;->b:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/fn;->c:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/activity/fn;->d:[Lcom/dropbox/android/activity/fn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/fm;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/fn;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static a(Lcom/dropbox/android/activity/TourPageFragment;I)Landroid/view/View;
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 262
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/ViewStub;

    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 263
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 264
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->i:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 266
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 267
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TourPageWithThumbsFragment;Ldbxyzptlk/db231222/r/d;ZLandroid/os/Bundle;)Lcom/dropbox/android/activity/fy;
    .locals 1

    .prologue
    .line 139
    invoke-static {p0, p1, p2, p3}, Lcom/dropbox/android/activity/fn;->b(Lcom/dropbox/android/activity/TourPageWithThumbsFragment;Ldbxyzptlk/db231222/r/d;ZLandroid/os/Bundle;)Lcom/dropbox/android/activity/fy;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Landroid/widget/CompoundButton;Landroid/view/View;Ldbxyzptlk/db231222/r/d;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->B()Z

    move-result v0

    .line 189
    if-eqz p3, :cond_0

    .line 190
    const-string v0, "VIDEOS_ENABLED_SWITCH_ON"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 192
    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 195
    new-instance v0, Lcom/dropbox/android/activity/fs;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fs;-><init>(Landroid/widget/CompoundButton;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/n/P;)V
    .locals 0

    .prologue
    .line 139
    invoke-static {p0, p1}, Lcom/dropbox/android/activity/fn;->b(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/n/P;)V

    return-void
.end method

.method public static a(Z)[Lcom/dropbox/android/activity/fn;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 170
    if-eqz p0, :cond_0

    .line 171
    new-array v0, v0, [Lcom/dropbox/android/activity/fn;

    sget-object v1, Lcom/dropbox/android/activity/fn;->a:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v2

    .line 173
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v0, [Lcom/dropbox/android/activity/fn;

    sget-object v1, Lcom/dropbox/android/activity/fn;->b:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method protected static final b(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/r/d;Landroid/os/Bundle;)Lcom/dropbox/android/activity/fy;
    .locals 4

    .prologue
    const/16 v2, 0x8

    .line 287
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->l:Landroid/view/View;

    move-object v1, v0

    .line 299
    :goto_0
    const v0, 0x7f070085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 301
    const v2, 0x7f070076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 302
    invoke-static {v0, v2, p1, p2}, Lcom/dropbox/android/activity/fn;->a(Landroid/widget/CompoundButton;Landroid/view/View;Ldbxyzptlk/db231222/r/d;Landroid/os/Bundle;)V

    .line 304
    const v2, 0x7f0701a4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 305
    new-instance v3, Lcom/dropbox/android/activity/fw;

    invoke-direct {v3, p0, p1}, Lcom/dropbox/android/activity/fw;-><init>(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/r/d;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    const v2, 0x7f0701a5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 315
    new-instance v2, Lcom/dropbox/android/activity/fx;

    invoke-direct {v2, v0, p1, p0}, Lcom/dropbox/android/activity/fx;-><init>(Landroid/widget/CompoundButton;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/activity/TourPageFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    new-instance v1, Lcom/dropbox/android/activity/fp;

    invoke-direct {v1, v0}, Lcom/dropbox/android/activity/fp;-><init>(Landroid/widget/CompoundButton;)V

    .line 335
    return-object v1

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 291
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/ViewStub;

    const v1, 0x7f0300a2

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 296
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method private static b(Lcom/dropbox/android/activity/TourPageWithThumbsFragment;Ldbxyzptlk/db231222/r/d;ZLandroid/os/Bundle;)Lcom/dropbox/android/activity/fy;
    .locals 3

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/actionbarsherlock/app/SherlockFragmentActivity;

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/SherlockFragmentActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 212
    const v0, 0x7f030025

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/fn;->a(Lcom/dropbox/android/activity/TourPageFragment;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    const v1, 0x7f070085

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 216
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    const v2, 0x7f070076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 217
    invoke-static {v0, v1, p1, p3}, Lcom/dropbox/android/activity/fn;->a(Landroid/widget/CompoundButton;Landroid/view/View;Ldbxyzptlk/db231222/r/d;Landroid/os/Bundle;)V

    .line 220
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    const v2, 0x7f070077

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 221
    new-instance v2, Lcom/dropbox/android/activity/ft;

    invoke-direct {v2, v0, p1, p0}, Lcom/dropbox/android/activity/ft;-><init>(Landroid/widget/CompoundButton;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/activity/TourPageWithThumbsFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    const v2, 0x7f070078

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 239
    new-instance v2, Lcom/dropbox/android/activity/fu;

    invoke-direct {v2, p0, p1}, Lcom/dropbox/android/activity/fu;-><init>(Lcom/dropbox/android/activity/TourPageWithThumbsFragment;Ldbxyzptlk/db231222/r/d;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    if-nez p2, :cond_0

    .line 248
    const v2, 0x7f0d01ec

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 249
    iget-object v1, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    const v2, 0x7f070079

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 252
    :cond_0
    new-instance v1, Lcom/dropbox/android/activity/fv;

    invoke-direct {v1, v0}, Lcom/dropbox/android/activity/fv;-><init>(Landroid/widget/CompoundButton;)V

    return-object v1
.end method

.method private static b(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/n/P;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 271
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->d()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "value"

    const-string v2, "cancel"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 273
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTRO_TOUR"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aJ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 276
    :cond_0
    invoke-virtual {p1, v3}, Ldbxyzptlk/db231222/n/P;->e(Z)V

    .line 277
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->m:Lcom/dropbox/android/activity/fz;

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->m:Lcom/dropbox/android/activity/fz;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_INDEX"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/fz;->a(I)V

    .line 280
    :cond_1
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/fn;
    .locals 1

    .prologue
    .line 139
    const-class v0, Lcom/dropbox/android/activity/fn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/fn;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/fn;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/dropbox/android/activity/fn;->d:[Lcom/dropbox/android/activity/fn;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/fn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/fn;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Lcom/dropbox/android/activity/TourPageFragment;Ldbxyzptlk/db231222/r/d;Landroid/os/Bundle;)Lcom/dropbox/android/activity/fy;
.end method
