.class final Lcom/dropbox/android/activity/dialog/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/dropbox/android/util/DropboxPath;

.field final synthetic c:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Landroid/widget/EditText;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/i;->c:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    iput-object p2, p0, Lcom/dropbox/android/activity/dialog/i;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/dropbox/android/activity/dialog/i;->b:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 174
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/i;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/i;->b:Lcom/dropbox/android/util/DropboxPath;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 178
    new-instance v1, Ldbxyzptlk/db231222/g/L;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/i;->c:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/i;->c:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-static {v3}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/i;->c:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    iget-object v4, v4, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ldbxyzptlk/db231222/g/N;

    invoke-direct {v1, v2, v3, v0, v4}, Ldbxyzptlk/db231222/g/L;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/g/N;)V

    .line 180
    invoke-virtual {v1}, Ldbxyzptlk/db231222/g/L;->f()V

    .line 181
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/g/L;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 182
    return-void
.end method
