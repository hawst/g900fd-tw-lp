.class final Lcom/dropbox/android/activity/A;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/bY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/widget/bY;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/dropbox/android/widget/bZ;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    .line 1278
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/A;->b:Ljava/util/HashSet;

    .line 1280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/A;->c:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/activity/b;)V
    .locals 0

    .prologue
    .line 1274
    invoke-direct {p0}, Lcom/dropbox/android/activity/A;-><init>()V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/bZ;

    .line 1284
    invoke-interface {v0}, Lcom/dropbox/android/widget/bZ;->a()V

    goto :goto_0

    .line 1286
    :cond_0
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/bZ;)V
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1316
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t double register a listener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1318
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1319
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1299
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303
    :goto_0
    return-void

    .line 1301
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method final a(Z)V
    .locals 0

    .prologue
    .line 1290
    iput-boolean p1, p0, Lcom/dropbox/android/activity/A;->c:Z

    .line 1291
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1331
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final b()I
    .locals 1

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/dropbox/android/widget/bZ;)V
    .locals 2

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1324
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t unregister a non-registered listener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1326
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1327
    return-void
.end method

.method final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final d()V
    .locals 1

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/dropbox/android/activity/A;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1311
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1336
    iget-boolean v0, p0, Lcom/dropbox/android/activity/A;->c:Z

    return v0
.end method
