.class public Lcom/dropbox/android/activity/CameraUploadDetailsFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# static fields
.field private static final a:Ljava/text/DateFormat;

.field private static final b:Ljava/text/DateFormat;


# instance fields
.field private c:Lcom/dropbox/android/activity/G;

.field private d:Lcom/dropbox/android/activity/G;

.field private e:Landroid/widget/ProgressBar;

.field private f:Landroid/widget/FrameLayout;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private final m:Lcom/dropbox/android/activity/D;

.field private final n:Lcom/dropbox/android/activity/E;

.field private o:Lcom/dropbox/android/activity/L;

.field private p:Landroid/database/Cursor;

.field private q:Ldbxyzptlk/db231222/j/g;

.field private r:Ljava/lang/String;

.field private final s:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a:Ljava/text/DateFormat;

    .line 68
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->b:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 84
    new-instance v0, Lcom/dropbox/android/activity/D;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/D;-><init>(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->m:Lcom/dropbox/android/activity/D;

    .line 85
    new-instance v0, Lcom/dropbox/android/activity/E;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/E;-><init>(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;Lcom/dropbox/android/activity/B;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->n:Lcom/dropbox/android/activity/E;

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->s:Landroid/os/Handler;

    .line 519
    return-void
.end method

.method static synthetic a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 63
    invoke-static {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Date;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x6

    const/4 v6, 0x1

    .line 928
    sget-object v1, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->b:Ljava/text/DateFormat;

    monitor-enter v1

    .line 929
    :try_start_0
    sget-object v0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 930
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 932
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 933
    invoke-virtual {v1, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 935
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 937
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 938
    const/4 v4, -0x1

    invoke-virtual {v3, v7, v4}, Ljava/util/Calendar;->add(II)V

    .line 940
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v4, v2, :cond_0

    .line 942
    const v1, 0x7f0d0194

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v8

    invoke-virtual {p0, v1, v2}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 951
    :goto_0
    return-object v0

    .line 930
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 943
    :cond_0
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v2, v4, :cond_1

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 945
    const v1, 0x7f0d0195

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v8

    invoke-virtual {p0, v1, v2}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 948
    :cond_1
    sget-object v1, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a:Ljava/text/DateFormat;

    monitor-enter v1

    .line 949
    :try_start_2
    sget-object v2, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a:Ljava/text/DateFormat;

    invoke-virtual {v2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 950
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 951
    const v1, 0x7f0d0193

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v8

    aput-object v0, v3, v6

    invoke-virtual {p0, v1, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 950
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->m:Lcom/dropbox/android/activity/D;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 150
    iput-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->n:Lcom/dropbox/android/activity/E;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 155
    iput-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    .line 157
    :cond_1
    return-void
.end method

.method private static b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/high16 v10, 0x44000000    # 512.0f

    const/high16 v9, 0x43c00000    # 384.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 890
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 891
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 894
    if-le v4, v3, :cond_2

    .line 895
    int-to-float v0, v3

    div-float/2addr v0, v5

    .line 896
    int-to-float v2, v4

    div-float/2addr v2, v5

    .line 897
    sub-float v7, v2, v0

    .line 899
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 900
    neg-float v8, v7

    invoke-virtual {v5, v7, v8}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 901
    const/high16 v7, 0x42b40000    # 90.0f

    invoke-virtual {v5, v7, v0, v2}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 903
    const/16 v0, 0x200

    if-ge v4, v0, :cond_0

    const/16 v0, 0x180

    if-ge v3, v0, :cond_0

    .line 904
    int-to-float v0, v4

    div-float v0, v10, v0

    int-to-float v2, v3

    div-float v2, v9, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 905
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    :cond_0
    move-object v0, p0

    move v2, v1

    .line 908
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 910
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    move-object p0, v0

    .line 923
    :cond_1
    :goto_0
    return-object p0

    .line 912
    :cond_2
    const/16 v0, 0x200

    if-ge v3, v0, :cond_1

    const/16 v0, 0x180

    if-ge v4, v0, :cond_1

    .line 913
    int-to-float v0, v3

    div-float v0, v10, v0

    int-to-float v1, v4

    div-float v1, v9, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 914
    int-to-float v1, v3

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 915
    int-to-float v2, v4

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 917
    invoke-static {p0, v1, v0, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 919
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    move-object p0, v0

    .line 920
    goto :goto_0
.end method

.method private b()V
    .locals 15

    .prologue
    const v13, 0x7f0d0074

    const/16 v10, 0x8

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->n:Lcom/dropbox/android/activity/E;

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/j/g;->b(Ldbxyzptlk/db231222/j/h;)V

    .line 162
    iput-object v4, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    .line 165
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v6

    .line 166
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a()V

    .line 167
    invoke-virtual {v6}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dropbox/android/taskqueue/U;->a(Z)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->m:Lcom/dropbox/android/activity/D;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 173
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v5, "camera_upload_status"

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    invoke-static {v0}, Lcom/dropbox/android/taskqueue/ag;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/ag;

    move-result-object v7

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v5, "camera_upload_initial_scan"

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 177
    if-ne v0, v1, :cond_3

    move v5, v1

    .line 179
    :goto_0
    if-eqz v5, :cond_4

    sget-object v0, Lcom/dropbox/android/taskqueue/ag;->g:Lcom/dropbox/android/taskqueue/ag;

    if-ne v7, v0, :cond_4

    move v0, v1

    .line 180
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v8

    .line 181
    invoke-virtual {v8}, Ldbxyzptlk/db231222/n/P;->r()Ldbxyzptlk/db231222/n/q;

    move-result-object v3

    sget-object v9, Ldbxyzptlk/db231222/n/q;->c:Ldbxyzptlk/db231222/n/q;

    if-ne v3, v9, :cond_5

    move v3, v1

    .line 183
    :goto_2
    if-nez v0, :cond_1

    if-eqz v3, :cond_9

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->j:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 185
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->i:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    .line 187
    if-eqz v0, :cond_7

    .line 188
    invoke-virtual {v8}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->l:Landroid/widget/TextView;

    const v1, 0x7f0d018e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 324
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v5, v2

    .line 177
    goto :goto_0

    :cond_4
    move v0, v2

    .line 179
    goto :goto_1

    :cond_5
    move v3, v2

    .line 181
    goto :goto_2

    .line 193
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->l:Landroid/widget/TextView;

    const v1, 0x7f0d018f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 196
    :cond_7
    sget-object v0, Lcom/dropbox/android/taskqueue/ag;->b:Lcom/dropbox/android/taskqueue/ag;

    if-ne v7, v0, :cond_8

    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->l:Landroid/widget/TextView;

    const v1, 0x7f0d0077

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 199
    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 205
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v9, "camera_upload_file_path"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v10, "camera_upload_local_mime_type"

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->r:Ljava/lang/String;

    invoke-static {v9, v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    .line 213
    :goto_4
    iput-object v9, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->r:Ljava/lang/String;

    .line 216
    invoke-static {v9}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 217
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_a

    move-object v3, v4

    .line 224
    :cond_a
    :goto_5
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d()V

    .line 227
    sget-object v11, Lcom/dropbox/android/activity/C;->a:[I

    invoke-virtual {v7}, Lcom/dropbox/android/taskqueue/ag;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    :cond_b
    move v3, v1

    .line 286
    :goto_6
    if-eqz v3, :cond_c

    .line 289
    if-eqz v5, :cond_13

    .line 290
    const v3, 0x7f0d018e

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 305
    :goto_7
    if-eqz v0, :cond_16

    sget-object v0, Lcom/dropbox/android/taskqueue/ag;->h:Lcom/dropbox/android/taskqueue/ag;

    if-ne v7, v0, :cond_16

    .line 306
    :goto_8
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c:Lcom/dropbox/android/activity/G;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080039

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v1}, Lcom/dropbox/android/activity/G;->a(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Z)V

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d:Lcom/dropbox/android/activity/G;

    invoke-virtual {v0, v3, v2}, Lcom/dropbox/android/activity/G;->a(Ljava/lang/CharSequence;Z)V

    .line 313
    :cond_c
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v2, "camera_upload_pending_paths_json"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 316
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v3, "camera_upload_local_uri"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->o:Lcom/dropbox/android/activity/L;

    invoke-virtual {v2, v0, v1, v7}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Lorg/json/JSONArray;Lcom/dropbox/android/taskqueue/ag;)V

    goto/16 :goto_3

    :cond_d
    move v0, v2

    .line 212
    goto :goto_4

    .line 229
    :pswitch_0
    const v3, 0x7f0d0077

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    .line 230
    goto :goto_6

    .line 232
    :pswitch_1
    const v3, 0x7f0d018d

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    .line 233
    goto :goto_6

    .line 235
    :pswitch_2
    const v3, 0x7f0d018c

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    .line 236
    goto :goto_6

    .line 238
    :pswitch_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/i;->a(Landroid/content/Context;)Lcom/dropbox/android/util/m;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/m;->b()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 239
    const v3, 0x7f0d0191

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    goto/16 :goto_6

    .line 241
    :cond_e
    const v3, 0x7f0d0190

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    .line 243
    goto/16 :goto_6

    .line 245
    :pswitch_4
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e()V

    move v3, v2

    .line 247
    goto/16 :goto_6

    .line 249
    :pswitch_5
    if-eqz v3, :cond_b

    .line 250
    new-instance v11, Ldbxyzptlk/db231222/j/l;

    iget-object v12, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v13, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v14, "id"

    invoke-interface {v13, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-direct {v11, v12, v13}, Ldbxyzptlk/db231222/j/l;-><init>(J)V

    .line 252
    invoke-virtual {v6}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v6

    invoke-virtual {v6, v11}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v6

    iput-object v6, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    .line 253
    iget-object v6, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    if-eqz v6, :cond_f

    .line 254
    iget-object v6, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    iget-object v11, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->n:Lcom/dropbox/android/activity/E;

    invoke-virtual {v6, v11}, Ldbxyzptlk/db231222/j/g;->a(Ldbxyzptlk/db231222/j/h;)V

    .line 257
    :cond_f
    invoke-static {v9, v10}, Lcom/dropbox/android/util/av;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    .line 258
    if-eqz v6, :cond_10

    .line 259
    invoke-direct {p0, v6}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 262
    :cond_10
    if-nez v4, :cond_17

    .line 263
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    .line 264
    const-wide/16 v11, 0x0

    cmp-long v4, v9, v11

    if-lez v4, :cond_11

    .line 265
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v9, v10}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 271
    :goto_9
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c()V

    move-object v4, v3

    move v3, v1

    .line 272
    goto/16 :goto_6

    .line 267
    :cond_11
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    goto :goto_9

    .line 275
    :pswitch_6
    invoke-virtual {p0, v13}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    .line 276
    goto/16 :goto_6

    .line 278
    :pswitch_7
    invoke-virtual {v8}, Ldbxyzptlk/db231222/n/P;->p()Z

    move-result v3

    if-eqz v3, :cond_12

    .line 279
    const v3, 0x7f0d0181

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    goto/16 :goto_6

    .line 281
    :cond_12
    const v3, 0x7f0d0183

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v1

    goto/16 :goto_6

    .line 292
    :cond_13
    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->p:Landroid/database/Cursor;

    const-string v6, "camera_upload_num_remaining"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 293
    if-lez v3, :cond_14

    .line 294
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0014

    new-array v8, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-virtual {v5, v6, v3, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_7

    .line 297
    :cond_14
    invoke-virtual {v8}, Ldbxyzptlk/db231222/n/P;->p()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 298
    const v3, 0x7f0d0182

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_7

    .line 300
    :cond_15
    const v3, 0x7f0d0184

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_7

    :cond_16
    move v1, v2

    .line 305
    goto/16 :goto_8

    .line 317
    :catch_0
    move-exception v0

    .line 319
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_17
    move-object v3, v4

    goto :goto_9

    :cond_18
    move-object v3, v4

    goto/16 :goto_5

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic b(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->b()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    instance-of v0, v0, Ldbxyzptlk/db231222/j/m;

    if-eqz v0, :cond_0

    .line 329
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->q:Ldbxyzptlk/db231222/j/g;

    check-cast v0, Ldbxyzptlk/db231222/j/m;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/m;->e()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c()V

    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->s:Landroid/os/Handler;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 338
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->f:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 344
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c:Lcom/dropbox/android/activity/G;

    const v1, 0x7f0d018b

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080092

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Lcom/dropbox/android/activity/G;->a(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Z)V

    .line 347
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d:Lcom/dropbox/android/activity/G;

    const v1, 0x7f0d018a

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/dropbox/android/activity/G;->a(Ljava/lang/CharSequence;Z)V

    .line 348
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 349
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->h:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 96
    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 98
    new-instance v2, Lcom/dropbox/android/activity/G;

    const v0, 0x7f070056

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {v2, v0}, Lcom/dropbox/android/activity/G;-><init>(Landroid/widget/TextView;)V

    iput-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c:Lcom/dropbox/android/activity/G;

    .line 99
    new-instance v2, Lcom/dropbox/android/activity/G;

    const v0, 0x7f070057

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {v2, v0}, Lcom/dropbox/android/activity/G;-><init>(Landroid/widget/TextView;)V

    iput-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d:Lcom/dropbox/android/activity/G;

    .line 100
    const v0, 0x7f070055

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e:Landroid/widget/ProgressBar;

    .line 101
    const v0, 0x7f070053

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->f:Landroid/widget/FrameLayout;

    .line 102
    const v0, 0x7f070052

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f070051

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->i:Landroid/view/View;

    .line 104
    const v0, 0x7f070054

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->h:Landroid/widget/ImageView;

    .line 105
    const v0, 0x7f07004f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->j:Landroid/view/View;

    .line 106
    const v0, 0x7f070058

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->k:Landroid/view/View;

    .line 107
    const v0, 0x7f070050

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->l:Landroid/widget/TextView;

    .line 108
    new-instance v0, Lcom/dropbox/android/activity/L;

    invoke-direct {v0, p0, p3}, Lcom/dropbox/android/activity/L;-><init>(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->o:Lcom/dropbox/android/activity/L;

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->k:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/B;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/B;-><init>(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    return-object v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->o:Lcom/dropbox/android/activity/L;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/L;->a(Landroid/os/Bundle;)V

    .line 128
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStart()V

    .line 133
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->b()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStop()V

    .line 143
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a()V

    .line 144
    return-void
.end method
