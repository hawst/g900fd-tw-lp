.class final Lcom/dropbox/android/activity/s;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Lcom/dropbox/android/activity/x;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    iput-object p6, p0, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/activity/x;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 267
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p2, Lcom/dropbox/android/activity/x;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/dropbox/android/activity/x;->b:Ljava/util/Collection;

    invoke-virtual {v0, v1, v2, p1}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    check-cast p2, Lcom/dropbox/android/activity/x;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/s;->a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/activity/x;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 289
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d0286

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->a(I)V

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->f(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 292
    return-void
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 275
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/AlbumViewActivity;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/AlbumViewActivity;->a(Lcom/dropbox/android/albums/Album;)V

    .line 276
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0, p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;Lcom/dropbox/android/albums/Album;)Lcom/dropbox/android/albums/Album;

    .line 277
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0, v3}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 278
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/AlbumViewFragment;->e(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/q;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/q;)V

    .line 279
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->f(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    .line 280
    invoke-static {}, Lcom/dropbox/android/activity/AlbumViewFragment;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Successfully created; reloading."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 283
    iget-object v0, p0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 285
    return-void
.end method
