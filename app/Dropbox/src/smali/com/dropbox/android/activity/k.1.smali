.class final Lcom/dropbox/android/activity/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/util/DropboxPath;

.field final synthetic b:I

.field final synthetic c:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;Lcom/dropbox/android/util/DropboxPath;I)V
    .locals 0

    .prologue
    .line 889
    iput-object p1, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/AlbumViewFragment;

    iput-object p2, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/util/DropboxPath;

    iput p3, p0, Lcom/dropbox/android/activity/k;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 894
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/AlbumViewFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->a()Lcom/dropbox/android/widget/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/bC;->g()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/AlbumViewFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/dropbox/android/activity/AlbumViewFragment;->m:Lcom/dropbox/android/util/aW;

    .line 897
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/AlbumViewFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/util/DropboxPath;

    iget v2, p0, Lcom/dropbox/android/activity/k;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;I)V

    .line 898
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/AlbumViewFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->requestLayout()V

    .line 902
    :goto_0
    return-void

    .line 900
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/AlbumViewFragment;

    new-instance v1, Lcom/dropbox/android/activity/z;

    iget-object v2, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/util/DropboxPath;

    iget v3, p0, Lcom/dropbox/android/activity/k;->b:I

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/dropbox/android/activity/z;-><init>(Lcom/dropbox/android/util/DropboxPath;IZ)V

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;Lcom/dropbox/android/activity/z;)Lcom/dropbox/android/activity/z;

    goto :goto_0
.end method
