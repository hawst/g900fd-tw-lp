.class final Lcom/dropbox/android/activity/dialog/B;
.super Lcom/dropbox/android/widget/bz;
.source "panda.py"


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "[",
            "Landroid/content/Intent;",
            "[",
            "Landroid/content/Intent;",
            "Lcom/dropbox/android/util/c;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/dropbox/android/widget/bB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct/range {p0 .. p5}, Lcom/dropbox/android/widget/bz;-><init>(Landroid/content/pm/PackageManager;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;Ljava/util/Comparator;)V

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/dialog/B;->b:Z

    .line 106
    return-void
.end method

.method private a(Landroid/view/View;Lcom/dropbox/android/widget/bB;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p2, Lcom/dropbox/android/widget/bB;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p2, Lcom/dropbox/android/widget/bB;->a:Landroid/content/pm/ResolveInfo;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/B;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p2, Lcom/dropbox/android/widget/bB;->c:Landroid/graphics/drawable/Drawable;

    .line 146
    :cond_0
    const v0, 0x7f0700d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p2, Lcom/dropbox/android/widget/bB;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 147
    const v0, 0x7f0700d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p2, Lcom/dropbox/android/widget/bB;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lcom/dropbox/android/widget/bz;->getCount()I

    move-result v0

    return v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/dropbox/android/activity/dialog/B;->b:Z

    if-ne v0, p1, :cond_0

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iput-boolean p1, p0, Lcom/dropbox/android/activity/dialog/B;->b:Z

    .line 128
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/B;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/dropbox/android/activity/dialog/B;->b:Z

    if-eqz v0, :cond_0

    .line 111
    invoke-super {p0}, Lcom/dropbox/android/widget/bz;->getCount()I

    move-result v0

    .line 114
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x6

    invoke-super {p0}, Lcom/dropbox/android/widget/bz;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 133
    if-nez p2, :cond_0

    .line 134
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030092

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 137
    :cond_0
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/dialog/B;->b(I)Lcom/dropbox/android/widget/bB;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/dropbox/android/activity/dialog/B;->a(Landroid/view/View;Lcom/dropbox/android/widget/bB;)V

    .line 138
    return-object p2
.end method
