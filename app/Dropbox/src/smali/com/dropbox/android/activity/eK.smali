.class public final enum Lcom/dropbox/android/activity/eK;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/eK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/eK;

.field public static final enum b:Lcom/dropbox/android/activity/eK;

.field private static final synthetic c:[Lcom/dropbox/android/activity/eK;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/dropbox/android/activity/eK;

    const-string v1, "LOCAL_ENTRY"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/eK;->a:Lcom/dropbox/android/activity/eK;

    .line 17
    new-instance v0, Lcom/dropbox/android/activity/eK;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/eK;->b:Lcom/dropbox/android/activity/eK;

    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/activity/eK;

    sget-object v1, Lcom/dropbox/android/activity/eK;->a:Lcom/dropbox/android/activity/eK;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/eK;->b:Lcom/dropbox/android/activity/eK;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/activity/eK;->c:[Lcom/dropbox/android/activity/eK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/eK;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/dropbox/android/activity/eK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/eK;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/eK;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/dropbox/android/activity/eK;->c:[Lcom/dropbox/android/activity/eK;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/eK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/eK;

    return-object v0
.end method
