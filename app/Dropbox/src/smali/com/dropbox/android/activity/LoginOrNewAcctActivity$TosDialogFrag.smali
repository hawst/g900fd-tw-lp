.class public Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/LoginOrNewAcctActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/cC;)Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;
    .locals 3

    .prologue
    .line 199
    new-instance v0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;-><init>()V

    .line 200
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 201
    const-string v2, "account_details"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 202
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 203
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/LoginOrNewAcctActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    const-class v0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 208
    new-instance v0, Lcom/dropbox/android/activity/cD;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/cD;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)V

    .line 217
    new-instance v1, Lcom/dropbox/android/activity/cE;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cE;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)V

    .line 228
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 229
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 230
    const v3, 0x7f0d0049

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 231
    const v0, 0x7f0d004a

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 233
    const v0, 0x7f0d0047

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 234
    const v0, 0x7f0d0048

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 235
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
