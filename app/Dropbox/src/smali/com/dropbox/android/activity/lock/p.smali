.class final Lcom/dropbox/android/activity/lock/p;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 688
    iput-object p2, p0, Lcom/dropbox/android/activity/lock/p;->b:Ljava/lang/String;

    .line 689
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 709
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->e(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->f(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->g(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 693
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->h(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 694
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->h(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 696
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->h(Lcom/dropbox/android/activity/lock/LockCodeActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(Lcom/dropbox/android/activity/lock/LockCodeActivity;I)Landroid/widget/EditText;

    move-result-object v0

    .line 697
    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 698
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 700
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 706
    :goto_0
    return-void

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/p;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(Lcom/dropbox/android/activity/lock/LockCodeActivity;Ljava/lang/String;Z)V

    .line 704
    iget-object v0, p0, Lcom/dropbox/android/activity/lock/p;->a:Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/LockCodeActivity;->d(Lcom/dropbox/android/activity/lock/LockCodeActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    goto :goto_0
.end method
