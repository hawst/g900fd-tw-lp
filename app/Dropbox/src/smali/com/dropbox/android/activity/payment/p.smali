.class final Lcom/dropbox/android/activity/payment/p;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/l/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/l/i",
        "<",
        "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
        "Ldbxyzptlk/db231222/z/ag;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ldbxyzptlk/db231222/z/ag;)V
    .locals 3

    .prologue
    .line 125
    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/ag;->a()Ldbxyzptlk/db231222/z/ax;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/z/ax;->a:Ldbxyzptlk/db231222/z/ax;

    if-ne v0, v1, :cond_1

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/ag;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/ag;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)V

    .line 129
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->c(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/payments/h;->a(Ljava/lang/String;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02ea

    const v2, 0x7f0d02eb

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;II)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 140
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bB()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/p;->a:Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    const v1, 0x7f0d02e8

    const v2, 0x7f0d02e9

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;II)V

    .line 142
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 121
    check-cast p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/payment/p;->a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ljava/lang/Void;)V

    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 121
    check-cast p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    check-cast p2, Ldbxyzptlk/db231222/z/ag;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/payment/p;->a(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Ldbxyzptlk/db231222/z/ag;)V

    return-void
.end method
