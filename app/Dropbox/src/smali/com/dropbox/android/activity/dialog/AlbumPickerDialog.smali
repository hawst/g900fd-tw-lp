.class public Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;
.super Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserDialogFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/dropbox/android/widget/b;

.field private b:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;-><init>()V

    .line 156
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;)Lcom/dropbox/android/widget/b;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a:Lcom/dropbox/android/widget/b;

    return-object v0
.end method

.method private c()Landroid/content/Context;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/util/bn;->e()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a:Lcom/dropbox/android/widget/b;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/b;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 150
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v5

    .line 53
    if-eqz v5, :cond_0

    .line 55
    new-instance v0, Lcom/dropbox/android/widget/b;

    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->c()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, Lcom/dropbox/android/activity/dialog/a;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/dialog/a;-><init>(Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;)V

    invoke-virtual {v5}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/b;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/dropbox/android/activity/di;Lcom/dropbox/android/taskqueue/D;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a:Lcom/dropbox/android/widget/b;

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->c()Landroid/content/Context;

    move-result-object v1

    .line 74
    const-string v0, "layout_inflater"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 75
    const v2, 0x7f030045

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 76
    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setId(I)V

    .line 78
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    const v3, 0x7f0d0283

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 80
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 81
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 82
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Lcom/dropbox/android/activity/dialog/d;

    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->c()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/d;-><init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 87
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onResume()V

    .line 88
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 90
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b:Landroid/widget/ListView;

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a:Lcom/dropbox/android/widget/b;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b:Landroid/widget/ListView;

    const v2, 0x7f020079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b:Landroid/widget/ListView;

    const v2, 0x7f0b00cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/activity/dialog/b;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dialog/b;-><init>(Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 127
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 128
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onStart()V

    .line 133
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a:Lcom/dropbox/android/widget/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/b;->a(Z)V

    .line 134
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onStop()V

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->a:Lcom/dropbox/android/widget/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/b;->a(Z)V

    .line 140
    return-void
.end method
