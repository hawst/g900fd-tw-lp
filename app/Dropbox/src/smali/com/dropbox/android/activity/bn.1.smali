.class final Lcom/dropbox/android/activity/bn;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/activity/GalleryActivity;

.field private final u:Lcom/dropbox/android/util/DropboxPath;

.field private final v:Ldbxyzptlk/db231222/n/r;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;)V
    .locals 0

    .prologue
    .line 1265
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 1266
    invoke-static {p3}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 1267
    iput-object p1, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    .line 1268
    iput-object p2, p0, Lcom/dropbox/android/activity/bn;->u:Lcom/dropbox/android/util/DropboxPath;

    .line 1269
    iput-object p3, p0, Lcom/dropbox/android/activity/bn;->v:Ldbxyzptlk/db231222/n/r;

    .line 1270
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1258
    invoke-virtual {p0}, Lcom/dropbox/android/activity/bn;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 1274
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->l(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bn;->u:Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p0, Lcom/dropbox/android/activity/bn;->v:Ldbxyzptlk/db231222/n/r;

    new-instance v3, Lcom/dropbox/android/provider/F;

    invoke-direct {v3}, Lcom/dropbox/android/provider/F;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/provider/MetadataManager;->b(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;)Lcom/dropbox/android/provider/H;

    move-result-object v1

    .line 1277
    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 1283
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    new-instance v2, Lcom/dropbox/android/activity/bu;

    iget-object v3, p0, Lcom/dropbox/android/activity/bn;->v:Ldbxyzptlk/db231222/n/r;

    iget-object v4, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/GalleryActivity;->m(Lcom/dropbox/android/activity/GalleryActivity;)I

    move-result v4

    iget-boolean v5, v1, Lcom/dropbox/android/provider/H;->b:Z

    invoke-direct {v2, v3, v4, v5}, Lcom/dropbox/android/activity/bu;-><init>(Ldbxyzptlk/db231222/n/r;IZ)V

    invoke-static {v0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/activity/bu;)Lcom/dropbox/android/activity/bu;

    .line 1284
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->v:Ldbxyzptlk/db231222/n/r;

    sget-object v2, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    if-ne v0, v2, :cond_2

    .line 1287
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->n(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1288
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_LOCAL_ENTRY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 1289
    iget-object v2, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    iget-boolean v3, v1, Lcom/dropbox/android/provider/H;->b:Z

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Ljava/lang/String;

    :goto_0
    invoke-static {v2, v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1299
    :cond_0
    :goto_1
    iget-object v0, v1, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/bn;->a(Landroid/database/Cursor;)V

    .line 1300
    iget-object v0, v1, Lcom/dropbox/android/provider/H;->a:Landroid/database/Cursor;

    return-object v0

    .line 1289
    :cond_1
    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    goto :goto_0

    .line 1293
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->o(Lcom/dropbox/android/activity/GalleryActivity;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 1294
    iget-object v0, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_LOCAL_ENTRY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 1295
    iget-object v2, p0, Lcom/dropbox/android/activity/bn;->l:Lcom/dropbox/android/activity/GalleryActivity;

    iget-wide v3, v0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    invoke-static {v2, v3, v4}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;J)J

    goto :goto_1
.end method
