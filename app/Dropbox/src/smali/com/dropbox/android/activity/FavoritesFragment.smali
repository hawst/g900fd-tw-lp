.class public Lcom/dropbox/android/activity/FavoritesFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/dropbox/android/widget/p;
.implements Lcom/dropbox/android/widget/quickactions/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/dropbox/android/widget/p;",
        "Lcom/dropbox/android/widget/quickactions/f;"
    }
.end annotation


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/view/View;

.field private c:Lcom/dropbox/android/provider/MetadataManager;

.field private d:Lcom/dropbox/android/filemanager/E;

.field private e:Landroid/os/Handler;

.field private f:Lcom/dropbox/android/service/I;

.field private g:Lcom/dropbox/android/widget/DropboxItemListView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/Button;

.field private k:I

.field private l:I

.field private m:Landroid/os/Handler;

.field private n:Lcom/dropbox/android/filemanager/G;

.field private o:Lcom/dropbox/android/widget/am;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 52
    new-instance v0, Lcom/dropbox/android/activity/aG;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aG;-><init>(Lcom/dropbox/android/activity/FavoritesFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->e:Landroid/os/Handler;

    .line 61
    new-instance v0, Lcom/dropbox/android/activity/aH;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aH;-><init>(Lcom/dropbox/android/activity/FavoritesFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->f:Lcom/dropbox/android/service/I;

    .line 81
    iput v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    .line 82
    iput v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->l:I

    .line 287
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->m:Landroid/os/Handler;

    .line 288
    new-instance v0, Lcom/dropbox/android/activity/aK;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aK;-><init>(Lcom/dropbox/android/activity/FavoritesFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->n:Lcom/dropbox/android/filemanager/G;

    .line 300
    new-instance v0, Lcom/dropbox/android/activity/aM;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aM;-><init>(Lcom/dropbox/android/activity/FavoritesFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->o:Lcom/dropbox/android/widget/am;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/FavoritesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/FavoritesFragment;)Lcom/dropbox/android/filemanager/E;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->d:Lcom/dropbox/android/filemanager/E;

    return-object v0
.end method

.method private c()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->d()V

    .line 170
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->e()V

    .line 171
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/FavoritesFragment;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->e()V

    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/FavoritesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->m:Landroid/os/Handler;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 205
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 206
    :cond_0
    const/4 v0, 0x4

    .line 211
    :goto_0
    if-eq v1, v0, :cond_1

    .line 212
    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 216
    :cond_1
    return-void

    .line 208
    :cond_2
    const/4 v0, 0x0

    .line 209
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->e()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/FavoritesFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 225
    instance-of v2, v0, Landroid/database/Cursor;

    if-eqz v2, :cond_2

    .line 226
    check-cast v0, Landroid/database/Cursor;

    .line 227
    const/4 v2, -0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v2, v1

    .line 228
    :cond_1
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 229
    invoke-static {v0}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    .line 230
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->b()Z

    move-result v4

    if-nez v4, :cond_1

    .line 231
    add-int/lit8 v2, v2, 0x1

    .line 232
    invoke-static {v3}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Ljava/lang/String;

    move-result-object v3

    .line 233
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/I;->g()Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/dropbox/android/taskqueue/SingleAttemptTaskQueue;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 234
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v2, v1

    .line 239
    :cond_3
    iput v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    .line 240
    iput v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->l:I

    .line 241
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->f()V

    goto :goto_0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 245
    iget-object v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->i:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->j:Landroid/widget/Button;

    if-eqz v2, :cond_0

    .line 248
    iget v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->l:I

    if-eqz v2, :cond_1

    .line 250
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f000f

    iget v4, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    new-array v1, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v0

    invoke-virtual {v2, v3, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 274
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->j:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 277
    :cond_0
    return-void

    .line 254
    :cond_1
    iget-object v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->d:Lcom/dropbox/android/filemanager/E;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/E;->a()Lcom/dropbox/android/filemanager/H;

    move-result-object v2

    .line 255
    sget-object v3, Lcom/dropbox/android/filemanager/H;->a:Lcom/dropbox/android/filemanager/H;

    if-ne v2, v3, :cond_2

    .line 257
    const v1, 0x7f0d00d5

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/FavoritesFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 259
    :cond_2
    iget v3, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    if-nez v3, :cond_5

    .line 260
    sget-object v0, Lcom/dropbox/android/filemanager/H;->b:Lcom/dropbox/android/filemanager/H;

    if-ne v2, v0, :cond_3

    .line 261
    const v0, 0x7f0d00d6

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/FavoritesFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_0

    .line 262
    :cond_3
    sget-object v0, Lcom/dropbox/android/filemanager/H;->c:Lcom/dropbox/android/filemanager/H;

    if-ne v2, v0, :cond_4

    .line 263
    const v0, 0x7f0d00d7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/FavoritesFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_0

    .line 265
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown status: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_5
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f000e

    iget v4, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    new-array v5, v1, [Ljava/lang/Object;

    iget v6, p0, Lcom/dropbox/android/activity/FavoritesFragment;->k:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 185
    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->b:Landroid/view/View;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 186
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->c()V

    .line 187
    return-void

    .line 185
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b_()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public final c_()V
    .locals 0

    .prologue
    .line 285
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v6

    .line 102
    if-nez v6, :cond_0

    .line 110
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x1

    sget-object v5, Lcom/dropbox/android/widget/as;->c:Lcom/dropbox/android/widget/as;

    move-object v2, p0

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/widget/DropboxItemListView;->a(ZLandroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/p;Lcom/dropbox/android/widget/quickactions/f;Lcom/dropbox/android/widget/as;Ldbxyzptlk/db231222/r/d;)V

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->o:Lcom/dropbox/android/widget/am;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setItemClickListener(Lcom/dropbox/android/widget/am;)V

    .line 107
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->d:Lcom/dropbox/android/filemanager/E;

    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->n:Lcom/dropbox/android/filemanager/G;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/E;->a(Lcom/dropbox/android/filemanager/G;)V

    .line 108
    invoke-direct {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->c()V

    .line 109
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 149
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->c:Lcom/dropbox/android/provider/MetadataManager;

    .line 152
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->z()Lcom/dropbox/android/filemanager/E;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->d:Lcom/dropbox/android/filemanager/E;

    .line 154
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lcom/dropbox/android/filemanager/D;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/FavoritesFragment;->c:Lcom/dropbox/android/provider/MetadataManager;

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/filemanager/D;-><init>(Landroid/content/Context;Lcom/dropbox/android/provider/MetadataManager;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const v3, 0x7f0700a2

    .line 115
    const v0, 0x7f030030

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 116
    const v0, 0x7f0700a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/DropboxItemListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    .line 117
    const v0, 0x7f0700a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->b:Landroid/view/View;

    .line 118
    const v0, 0x7f07009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->h:Landroid/view/View;

    .line 119
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->h:Landroid/view/View;

    const v2, 0x7f0700a1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->i:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->j:Landroid/widget/Button;

    .line 121
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->a:Landroid/widget/Button;

    .line 122
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->a:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/aI;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aI;-><init>(Lcom/dropbox/android/activity/FavoritesFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    const v0, 0x7f0700a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v2, Lcom/dropbox/android/activity/aJ;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aJ;-><init>(Lcom/dropbox/android/activity/FavoritesFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->d:Lcom/dropbox/android/filemanager/E;

    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->n:Lcom/dropbox/android/filemanager/G;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/E;->b(Lcom/dropbox/android/filemanager/G;)V

    .line 143
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDestroyView()V

    .line 144
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDetach()V

    .line 87
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 90
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/FavoritesFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->g:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 195
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onResume()V

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/activity/FavoritesFragment;->d:Lcom/dropbox/android/filemanager/E;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/E;->a(Z)V

    .line 96
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStart()V

    .line 159
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->f:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    .line 160
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/FavoritesFragment;->f:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 165
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onStop()V

    .line 166
    return-void
.end method
