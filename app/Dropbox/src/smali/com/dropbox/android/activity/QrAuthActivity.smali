.class public Lcom/dropbox/android/activity/QrAuthActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/et;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/dropbox/android/activity/QrAuthActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/QrAuthActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 109
    return-void
.end method

.method public static a(Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 220
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v0

    const-string v1, "mobile-remote-installer"

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 221
    const-string v0, "LoginRIReminder"

    invoke-static {p0, p1, v0, p2}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;Landroid/content/Context;)V

    .line 222
    return-void
.end method

.method public static a(Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 177
    const-string v0, "mobile-remote-installer"

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "ri-notf"

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 184
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->D()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/QrAuthActivity$QrAlarmReceiver;

    invoke-direct {v0, p3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 193
    const-string v1, "com.dropbox.android.activity.QrAuthFragment.ACTION_RI_REMINDER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v1, "com.dropbox.intent.extra.USER_ID"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v1, "com.dropbox.intent.extra.REMINDER_SOURCE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    const/high16 v1, 0x8000000

    invoke-static {p3, v4, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 200
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 201
    const/4 v0, 0x5

    invoke-virtual {v2, v0, v3}, Ljava/util/Calendar;->add(II)V

    .line 202
    const/16 v0, 0xb

    const/16 v3, 0x12

    invoke-virtual {v2, v0, v3}, Ljava/util/Calendar;->set(II)V

    .line 203
    const/16 v0, 0xd

    invoke-virtual {v2, v0, v4}, Ljava/util/Calendar;->set(II)V

    .line 204
    const/16 v0, 0xc

    const/16 v3, 0x1e

    invoke-virtual {v2, v0, v3}, Ljava/util/Calendar;->set(II)V

    .line 207
    const-string v0, "alarm"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 208
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static a(Ldbxyzptlk/db231222/r/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 229
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 230
    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->a(Z)V

    .line 231
    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->d(Z)V

    .line 233
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    const-string v1, "com.dropbox.android.notifications.TAG.ri_notification"

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Ljava/lang/String;)V

    .line 234
    return-void
.end method

.method public static a(Lcom/dropbox/android/util/analytics/r;)Z
    .locals 4

    .prologue
    .line 105
    const-string v0, "mobile-remote-installer"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "ri"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "ri-notf"

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/dropbox/android/activity/QrAuthActivity;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/activity/er;ZLjava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0700b2

    .line 85
    invoke-static {p1}, Lcom/dropbox/android/activity/QrAuthFragment;->a(Lcom/dropbox/android/activity/er;)Lcom/dropbox/android/activity/QrAuthFragment;

    move-result-object v1

    .line 86
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 87
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 88
    if-eqz p2, :cond_1

    .line 89
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 90
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    invoke-virtual {v3, v5, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 96
    :goto_1
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 97
    return-void

    .line 94
    :cond_1
    invoke-virtual {v3, v5, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 51
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->finish()V

    .line 72
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/QrAuthActivity;->setRequestedOrientation(I)V

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.intent.extra.QR_LAUNCH_SOURCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_2

    .line 64
    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 67
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 68
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/QrAuthActivity;->setContentView(I)V

    .line 69
    const v0, 0x7f0d0313

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/QrAuthActivity;->setTitle(I)V

    .line 71
    sget-object v0, Lcom/dropbox/android/activity/er;->a:Lcom/dropbox/android/activity/er;

    sget-object v1, Lcom/dropbox/android/activity/er;->a:Lcom/dropbox/android/activity/er;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/er;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v2, v1}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/activity/er;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 78
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    const-string v2, "RIFlowReminder"

    invoke-static {v1, v0, v2, p0}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/r/d;Ljava/lang/String;Landroid/content/Context;)V

    .line 80
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onDestroy()V

    .line 81
    return-void
.end method
