.class final Lcom/dropbox/android/activity/ee;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/PrefsActivity;

.field final synthetic b:Lcom/dropbox/android/activity/ed;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/ed;Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 554
    iput-object p1, p0, Lcom/dropbox/android/activity/ee;->b:Lcom/dropbox/android/activity/ed;

    iput-object p2, p0, Lcom/dropbox/android/activity/ee;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 558
    packed-switch p2, :pswitch_data_0

    .line 572
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid send feedback dialog option "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 560
    :pswitch_0
    sget-object v0, Lcom/dropbox/android/activity/eD;->a:Lcom/dropbox/android/activity/eD;

    .line 575
    :goto_0
    invoke-static {}, Lcom/dropbox/android/util/UIHelpers;->a()V

    .line 576
    iget-object v1, p0, Lcom/dropbox/android/activity/ee;->b:Lcom/dropbox/android/activity/ed;

    iget-object v2, p0, Lcom/dropbox/android/activity/ee;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v2, v0}, Lcom/dropbox/android/activity/SendFeedbackActivity;->a(Landroid/content/Context;Lcom/dropbox/android/activity/eD;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/ed;->startActivity(Landroid/content/Intent;)V

    .line 577
    return-void

    .line 563
    :pswitch_1
    sget-object v0, Lcom/dropbox/android/activity/eD;->b:Lcom/dropbox/android/activity/eD;

    goto :goto_0

    .line 566
    :pswitch_2
    sget-object v0, Lcom/dropbox/android/activity/eD;->c:Lcom/dropbox/android/activity/eD;

    goto :goto_0

    .line 569
    :pswitch_3
    sget-object v0, Lcom/dropbox/android/activity/eD;->d:Lcom/dropbox/android/activity/eD;

    goto :goto_0

    .line 558
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
