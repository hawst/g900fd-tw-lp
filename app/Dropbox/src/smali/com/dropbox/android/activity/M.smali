.class final Lcom/dropbox/android/activity/M;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/ai;

.field final synthetic b:I

.field final synthetic c:Lcom/dropbox/android/activity/L;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/L;Lcom/dropbox/android/filemanager/ai;I)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    iput-object p2, p0, Lcom/dropbox/android/activity/M;->a:Lcom/dropbox/android/filemanager/ai;

    iput p3, p0, Lcom/dropbox/android/activity/M;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 540
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    iget-object v0, v0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 542
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->a:Lcom/dropbox/android/filemanager/ai;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->a:Lcom/dropbox/android/filemanager/ai;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 564
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    invoke-static {v0}, Lcom/dropbox/android/activity/L;->a(Lcom/dropbox/android/activity/L;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/activity/M;->b:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 548
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    invoke-static {v0}, Lcom/dropbox/android/activity/L;->a(Lcom/dropbox/android/activity/L;)Landroid/util/SparseArray;

    move-result-object v0

    iget v2, p0, Lcom/dropbox/android/activity/M;->b:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 549
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    invoke-static {v0}, Lcom/dropbox/android/activity/L;->b(Lcom/dropbox/android/activity/L;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 551
    if-ltz v6, :cond_0

    .line 557
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->a:Lcom/dropbox/android/filemanager/ai;

    if-eqz v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    iget-object v2, p0, Lcom/dropbox/android/activity/M;->a:Lcom/dropbox/android/filemanager/ai;

    invoke-static {v0, v1, v2, v6, v7}, Lcom/dropbox/android/activity/L;->a(Lcom/dropbox/android/activity/L;Ljava/lang/String;Lcom/dropbox/android/filemanager/ai;IZ)V

    .line 563
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    invoke-static {v0}, Lcom/dropbox/android/activity/L;->c(Lcom/dropbox/android/activity/L;)V

    goto :goto_0

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    iget-object v2, p0, Lcom/dropbox/android/activity/M;->c:Lcom/dropbox/android/activity/L;

    iget-object v2, v2, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020293

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/activity/L;->a(Lcom/dropbox/android/activity/L;Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    goto :goto_1
.end method
