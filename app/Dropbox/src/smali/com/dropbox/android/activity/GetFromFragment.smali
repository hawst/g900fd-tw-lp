.class public Lcom/dropbox/android/activity/GetFromFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 314
    return-void
.end method

.method public static d()Lcom/dropbox/android/activity/GetFromFragment;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/dropbox/android/activity/GetFromFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/GetFromFragment;-><init>()V

    .line 87
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 88
    const-string v2, "ARG_HIDE_QUICKACTIONS"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 89
    const-string v2, "ARG_SHOWALL"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GetFromFragment;->setArguments(Landroid/os/Bundle;)V

    .line 91
    return-object v0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 139
    if-eqz p1, :cond_0

    const-string v0, "[a-z]+/[a-zA-Z0-9-*+]+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Lcom/dropbox/android/activity/bH;
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_RESULT_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 193
    if-nez v0, :cond_0

    .line 194
    sget-object v0, Lcom/dropbox/android/activity/bH;->a:Lcom/dropbox/android/activity/bH;

    .line 196
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/dropbox/android/activity/bH;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bH;

    move-result-object v0

    goto :goto_0
.end method

.method private r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_APP_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 202
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/activity/bH;)V
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_RESULT_TYPE"

    invoke-virtual {p1}, Lcom/dropbox/android/activity/bH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method protected final a(Lcom/dropbox/android/filemanager/LocalEntry;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 208
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 209
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/P;->c(Lcom/dropbox/android/util/DropboxPath;)V

    .line 211
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    .line 212
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 214
    invoke-direct {p0}, Lcom/dropbox/android/activity/GetFromFragment;->q()Lcom/dropbox/android/activity/bH;

    move-result-object v4

    .line 215
    sget-object v0, Lcom/dropbox/android/activity/bD;->a:[I

    invoke-virtual {v4}, Lcom/dropbox/android/activity/bH;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    return-void

    .line 217
    :pswitch_0
    new-instance v0, Ldbxyzptlk/db231222/g/h;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Ldbxyzptlk/db231222/g/h;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 218
    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/h;->f()V

    .line 219
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/db231222/g/i;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 220
    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 225
    :pswitch_1
    new-instance v0, Lcom/dropbox/android/activity/bF;

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-direct {p0}, Lcom/dropbox/android/activity/GetFromFragment;->r()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/bF;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/activity/bH;Ljava/lang/String;)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/bF;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 228
    :pswitch_2
    new-instance v0, Lcom/dropbox/android/activity/bE;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-direct {p0}, Lcom/dropbox/android/activity/GetFromFragment;->r()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/dropbox/android/activity/bE;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0}, Lcom/dropbox/android/activity/bE;->f()V

    .line 230
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/db231222/g/i;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 231
    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/bE;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/GetFromFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_MIMETYPE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 150
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 151
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_0
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ARG_EXTENSIONS"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 156
    :cond_1
    return-void
.end method

.method public final b()Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/dropbox/android/widget/as;->a:Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_APP_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 96
    const v0, 0x7f030042

    return v0
.end method

.method protected final e()Ldbxyzptlk/db231222/n/r;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    return-object v0
.end method

.method public final f()Lcom/dropbox/android/provider/E;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 177
    const-string v2, "ARG_SHOWALL"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-object v0

    .line 180
    :cond_1
    const-string v2, "ARG_EXTENSIONS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 181
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 182
    new-instance v0, Lcom/dropbox/android/provider/D;

    invoke-direct {v0, v2}, Lcom/dropbox/android/provider/D;-><init>(Ljava/util/List;)V

    goto :goto_0

    .line 184
    :cond_2
    const-string v2, "ARG_MIMETYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 185
    if-eqz v1, :cond_0

    .line 186
    new-instance v0, Lcom/dropbox/android/provider/I;

    invoke-direct {v0, v1}, Lcom/dropbox/android/provider/I;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ARG_MIMETYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    new-instance v2, Lcom/dropbox/android/activity/bC;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bC;-><init>(Lcom/dropbox/android/activity/GetFromFragment;)V

    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0700cc

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 119
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_SHOWALL"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 121
    const/4 v0, 0x1

    .line 124
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f07004b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125
    return-void

    .line 124
    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method
