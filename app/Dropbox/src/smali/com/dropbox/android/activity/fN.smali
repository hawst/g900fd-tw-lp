.class final Lcom/dropbox/android/activity/fN;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Lcom/dropbox/android/activity/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/dropbox/android/activity/fN;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/fN;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSystemUiVisibilityChange(I)V
    .locals 4

    .prologue
    .line 321
    if-eqz p1, :cond_0

    and-int/lit8 v0, p1, 0x2

    if-lez v0, :cond_1

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/fN;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->h(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fN;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 324
    :cond_1
    if-nez p1, :cond_2

    .line 326
    iget-object v0, p0, Lcom/dropbox/android/activity/fN;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/dropbox/android/activity/fN;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->l(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxMediaController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    .line 332
    :cond_2
    :goto_0
    return-void

    .line 329
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/fN;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->h(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fN;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
