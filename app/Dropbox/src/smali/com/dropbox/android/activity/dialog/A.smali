.class final Lcom/dropbox/android/activity/dialog/A;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ldbxyzptlk/db231222/n/K;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/n/K;)V
    .locals 0

    .prologue
    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/A;->a:Ldbxyzptlk/db231222/n/K;

    .line 395
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 398
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    .line 399
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->a:Ldbxyzptlk/db231222/n/K;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/K;->g()Ldbxyzptlk/db231222/o/a;

    move-result-object v0

    .line 401
    invoke-virtual {v0}, Ldbxyzptlk/db231222/o/a;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/c;

    .line 402
    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/o/c;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ldbxyzptlk/db231222/o/c;->i()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldbxyzptlk/db231222/o/c;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 404
    :cond_0
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 407
    invoke-static {}, Ldbxyzptlk/db231222/o/a;->g()Ldbxyzptlk/db231222/o/g;

    move-result-object v2

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 409
    invoke-static {}, Ldbxyzptlk/db231222/o/c;->m()Ldbxyzptlk/db231222/o/e;

    move-result-object v4

    .line 410
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ldbxyzptlk/db231222/o/e;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/o/e;

    .line 411
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ldbxyzptlk/db231222/o/e;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/o/e;

    .line 412
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Ldbxyzptlk/db231222/o/e;->a(J)Ldbxyzptlk/db231222/o/e;

    .line 413
    invoke-virtual {v4}, Ldbxyzptlk/db231222/o/e;->b()Ldbxyzptlk/db231222/o/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/o/g;->a(Ldbxyzptlk/db231222/o/c;)Ldbxyzptlk/db231222/o/g;

    goto :goto_0

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->a:Ldbxyzptlk/db231222/n/K;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/o/g;->b()Ldbxyzptlk/db231222/o/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/K;->a(Ldbxyzptlk/db231222/o/a;)V

    .line 417
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;)J
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 421
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/A;->a()V

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 425
    if-nez v0, :cond_1

    .line 426
    const-wide/16 v0, 0x0

    .line 429
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final b(Landroid/content/ComponentName;)V
    .locals 6

    .prologue
    const-wide/16 v1, 0x1

    .line 433
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 434
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/A;->a()V

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 438
    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/A;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long v0, v4, v1

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/A;->b()V

    .line 441
    return-void

    :cond_1
    move-wide v0, v1

    .line 438
    goto :goto_0
.end method
