.class final Lcom/dropbox/android/activity/do;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/actionbarsherlock/view/ActionMode$Callback;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/PhotoGridFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PhotoGridFragment;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/actionbarsherlock/view/Menu;IIIZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 397
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f08008d

    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v5

    move v1, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;IIIZZ)Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    move-result-object v0

    .line 404
    new-instance v1, Lcom/dropbox/android/activity/dp;

    invoke-direct {v1, p0, p2}, Lcom/dropbox/android/activity/dp;-><init>(Lcom/dropbox/android/activity/do;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    invoke-interface {p1, v6, p2, v6, p3}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/actionbarsherlock/view/MenuItem;->setActionView(Landroid/view/View;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 411
    return-void
.end method

.method private a(I)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 430
    packed-switch p1, :pswitch_data_0

    .line 459
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected menu id"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :pswitch_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->am()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "target"

    const-string v2, "share"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 433
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->d(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    .line 457
    :goto_0
    return v4

    .line 436
    :pswitch_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->am()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "target"

    const-string v2, "add_to_album"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 437
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->e(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    goto :goto_0

    .line 440
    :pswitch_2
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->am()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "target"

    const-string v3, "delete"

    invoke-virtual {v0, v1, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 443
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 444
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    add-int/lit8 v0, v1, 0x1

    move v1, v3

    :goto_2
    move v3, v1

    move v1, v0

    .line 449
    goto :goto_1

    .line 447
    :cond_0
    add-int/lit8 v3, v3, 0x1

    move v0, v1

    move v1, v3

    goto :goto_2

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    iget-object v5, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v5, v5, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/util/Collection;)I

    move-result v0

    if-eqz v0, :cond_2

    move v2, v4

    .line 451
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-static {v0, v3, v1, v2}, Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;->a(Lcom/dropbox/android/activity/PhotoGridFragment;IIZ)Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/PhotoGridFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/PhotoGridFragment$DeleteConfirmFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 430
    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/dropbox/android/activity/do;I)Z
    .locals 1

    .prologue
    .line 380
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/do;->a(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final onActionItemClicked(Lcom/actionbarsherlock/view/ActionMode;Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 426
    invoke-interface {p2}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/do;->a(I)Z

    move-result v0

    return v0
.end method

.method public final onCreateActionMode(Lcom/actionbarsherlock/view/ActionMode;Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method public final onDestroyActionMode(Lcom/actionbarsherlock/view/ActionMode;)V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/PhotoGridFragment;->c(Lcom/dropbox/android/activity/PhotoGridFragment;)V

    .line 416
    return-void
.end method

.method public final onPrepareActionMode(Lcom/actionbarsherlock/view/ActionMode;Lcom/actionbarsherlock/view/Menu;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 384
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragment;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/os/Handler;Landroid/app/Activity;)V

    .line 385
    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/actionbarsherlock/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 386
    invoke-interface {p2}, Lcom/actionbarsherlock/view/Menu;->clear()V

    .line 387
    const/16 v2, 0x12c

    const v3, 0x7f0d0292

    const v4, 0x7f020145

    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v5, v6

    :goto_0
    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/do;->a(Lcom/actionbarsherlock/view/Menu;IIIZ)V

    .line 389
    const/16 v2, 0x12d

    const v3, 0x7f0d0293

    const v4, 0x7f02012a

    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v5, v6

    :goto_1
    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/do;->a(Lcom/actionbarsherlock/view/Menu;IIIZ)V

    .line 391
    const/16 v2, 0x12e

    const v3, 0x7f0d0294

    const v4, 0x7f020137

    iget-object v0, p0, Lcom/dropbox/android/activity/do;->a:Lcom/dropbox/android/activity/PhotoGridFragment;

    iget-object v0, v0, Lcom/dropbox/android/activity/PhotoGridFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v5, v6

    :goto_2
    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/do;->a(Lcom/actionbarsherlock/view/Menu;IIIZ)V

    .line 393
    return v6

    :cond_0
    move v5, v7

    .line 387
    goto :goto_0

    :cond_1
    move v5, v7

    .line 389
    goto :goto_1

    :cond_2
    move v5, v7

    .line 391
    goto :goto_2
.end method
