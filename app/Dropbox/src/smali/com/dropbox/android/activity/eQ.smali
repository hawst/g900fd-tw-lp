.class final Lcom/dropbox/android/activity/eQ;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/P;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/ShareViaEmailFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/ShareViaEmailFragment;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/dropbox/android/activity/eQ;->a:Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/v/n;Lcom/dropbox/android/taskqueue/w;)V
    .locals 3

    .prologue
    .line 279
    sget-object v0, Lcom/dropbox/android/activity/ShareViaEmailFragment;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load thumbnail; status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;)V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/dropbox/android/activity/eQ;->a:Lcom/dropbox/android/activity/ShareViaEmailFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ShareViaEmailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/dropbox/android/util/bn;->h()Ldbxyzptlk/db231222/v/n;

    move-result-object v1

    invoke-virtual {p3, v1}, Ldbxyzptlk/db231222/v/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    new-instance v1, Lcom/dropbox/android/activity/eR;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/eR;-><init>(Lcom/dropbox/android/activity/eQ;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
