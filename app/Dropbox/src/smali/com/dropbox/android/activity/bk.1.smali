.class final Lcom/dropbox/android/activity/bk;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 513
    iput-object p1, p0, Lcom/dropbox/android/activity/bk;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 516
    iget-object v0, p0, Lcom/dropbox/android/activity/bk;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    .line 520
    :goto_0
    iget-object v3, p0, Lcom/dropbox/android/activity/bk;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v3

    new-array v4, v1, [Lcom/dropbox/android/util/DropboxPath;

    iget-object v5, p0, Lcom/dropbox/android/activity/bk;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v5}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/util/Collection;)I

    move-result v3

    if-ne v1, v3, :cond_1

    .line 522
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/activity/bk;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;->a(Landroid/content/res/Resources;Lcom/dropbox/android/activity/bs;Z)Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bk;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxDeleteConfirmDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 524
    return-void

    .line 516
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/bs;->b:Lcom/dropbox/android/activity/bs;

    goto :goto_0

    :cond_1
    move v1, v2

    .line 520
    goto :goto_1
.end method
