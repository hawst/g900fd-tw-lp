.class public Lcom/dropbox/android/activity/CreateShortcutFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# instance fields
.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    return-void
.end method

.method public static a()Lcom/dropbox/android/activity/CreateShortcutFragment;
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;-><init>()V

    .line 28
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 29
    const-string v2, "ARG_HIDE_QUICKACTIONS"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 30
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/CreateShortcutFragment;->setArguments(Landroid/os/Bundle;)V

    .line 32
    return-object v0
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.BROWSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 88
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 89
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 90
    const-string v0, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v0

    .line 92
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CreateShortcutFragment;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/CreateShortcutFragment;->a(Landroid/net/Uri;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final b()Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f030032

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 48
    const v0, 0x7f07004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->d:Landroid/widget/Button;

    .line 49
    iget-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->d:Landroid/widget/Button;

    const v2, 0x7f0d0013

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 50
    new-instance v0, Lcom/dropbox/android/activity/W;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/W;-><init>(Lcom/dropbox/android/activity/CreateShortcutFragment;)V

    .line 56
    iget-object v2, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->d:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const v0, 0x7f07004d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->e:Landroid/widget/Button;

    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->e:Landroid/widget/Button;

    const v2, 0x7f0d01db

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 60
    new-instance v0, Lcom/dropbox/android/activity/X;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/X;-><init>(Lcom/dropbox/android/activity/CreateShortcutFragment;)V

    .line 76
    iget-object v2, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->e:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    return-object v1
.end method
