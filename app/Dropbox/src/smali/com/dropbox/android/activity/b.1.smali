.class final Lcom/dropbox/android/activity/b;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/dropbox/android/activity/b;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    iput-object p6, p0, Lcom/dropbox/android/activity/b;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/b;->a(Lcom/dropbox/android/albums/u;Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/albums/u;Ljava/lang/Void;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/dropbox/android/activity/b;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/b;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/albums/PhotosModel;->b(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/dropbox/android/activity/b;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_0

    .line 181
    const v1, 0x7f0d02a5

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 183
    :cond_0
    return-void
.end method
