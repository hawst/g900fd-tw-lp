.class public Lcom/dropbox/android/activity/MoveActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 60
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 20
    invoke-static {p0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/app/Activity;)V

    .line 21
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveActivity;->finish()V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/MoveActivity;->setContentView(I)V

    .line 31
    const v0, 0x7f0d01d7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/MoveActivity;->setTitle(I)V

    .line 33
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 34
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 37
    if-nez p1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.dropbox.android.activity.ENTRY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 39
    invoke-static {v0}, Lcom/dropbox/android/activity/MoveToFragment;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/MoveToFragment;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->j()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 44
    new-instance v2, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v2, v1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/MoveToFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 50
    const v2, 0x7f0700b2

    const-string v3, "TAG_MOVE"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 51
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
