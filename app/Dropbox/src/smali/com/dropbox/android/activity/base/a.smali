.class public abstract Lcom/dropbox/android/activity/base/a;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/m;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/base/m;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->b:Z

    .line 13
    iput-object p1, p0, Lcom/dropbox/android/activity/base/a;->a:Lcom/dropbox/android/activity/base/m;

    .line 14
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->b:Z

    if-eqz v0, :cond_0

    .line 19
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t call super.onActivityResult(). It doesn\'t make sense."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    const v0, 0xfd91

    if-ne p1, v0, :cond_1

    .line 29
    :goto_0
    return-void

    .line 25
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->b:Z

    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->a:Lcom/dropbox/android/activity/base/m;

    invoke-interface {v0, p1, p2, p3}, Lcom/dropbox/android/activity/base/m;->a(IILandroid/content/Intent;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->b:Z

    goto :goto_0
.end method
