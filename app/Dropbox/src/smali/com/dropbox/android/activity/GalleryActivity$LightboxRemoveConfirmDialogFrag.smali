.class public Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;
.super Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities",
        "<",
        "Lcom/dropbox/android/activity/GalleryActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 792
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFragForActivities;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;Lcom/dropbox/android/activity/bs;Ljava/lang/String;)Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;
    .locals 3

    .prologue
    .line 795
    sget-object v0, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0d00a8

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 800
    new-instance v1, Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;

    invoke-direct {v1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;-><init>()V

    .line 801
    const v2, 0x7f0d00a7

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;->a(Ljava/lang/String;I)V

    .line 802
    return-object v1

    .line 795
    :cond_0
    const v0, 0x7f0d00a9

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 792
    check-cast p1, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/GalleryActivity$LightboxRemoveConfirmDialogFrag;->a(Lcom/dropbox/android/activity/GalleryActivity;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 807
    invoke-virtual {p1}, Lcom/dropbox/android/activity/GalleryActivity;->e()V

    .line 808
    return-void
.end method
