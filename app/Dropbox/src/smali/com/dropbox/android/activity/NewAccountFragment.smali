.class public Lcom/dropbox/android/activity/NewAccountFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/db;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final s:[[I


# instance fields
.field private c:Z

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;

.field private f:Lcom/dropbox/android/widget/EmailTextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/CheckBox;

.field private k:[Landroid/view/View;

.field private l:I

.field private m:I

.field private n:Landroid/view/View;

.field private o:Landroid/webkit/WebView;

.field private p:Z

.field private q:Landroid/widget/ScrollView;

.field private final r:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    .line 128
    const/4 v0, 0x5

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v3

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->s:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0d003c
        0x1
        0x7f08005b
    .end array-data

    :array_1
    .array-data 4
        0x7f0d003d
        0x1
        0x7f08005b
    .end array-data

    :array_2
    .array-data 4
        0x7f0d003e
        0x2
        0x7f08005c
    .end array-data

    :array_3
    .array-data 4
        0x7f0d003f
        0x3
        0x7f08005d
    .end array-data

    :array_4
    .array-data 4
        0x7f0d0040
        0x4
        0x7f08005e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 80
    new-instance v0, Lcom/dropbox/android/activity/cQ;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/cQ;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->r:Landroid/os/Handler;

    .line 107
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/NewAccountFragment;->setArguments(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/activity/NewAccountFragment;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/NewAccountFragment;-><init>()V

    .line 98
    invoke-virtual {v0}, Lcom/dropbox/android/activity/NewAccountFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 99
    const-string v2, "ARG_FIRST_NAME_PREFILL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v2, "ARG_LAST_NAME_PREFILL"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v2, "ARG_EMAIL_PREFILL"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/NewAccountFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 137
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 139
    if-ge v0, p1, :cond_0

    .line 140
    iget v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->l:I

    .line 145
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_0
    iget v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->m:I

    goto :goto_1

    .line 147
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/NewAccountFragment;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/NewAccountFragment;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 151
    if-ltz p1, :cond_0

    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->s:[[I

    array-length v0, v0

    if-gt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_2

    .line 152
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->d()V

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 157
    :cond_2
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-nez v0, :cond_3

    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 158
    invoke-virtual {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/NewAccountFragment;->s:[[I

    aget-object v1, v1, p1

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->l:I

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    sget-object v1, Lcom/dropbox/android/activity/NewAccountFragment;->s:[[I

    aget-object v1, v1, p1

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 163
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-nez v0, :cond_4

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    iget v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 166
    :cond_4
    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->s:[[I

    aget-object v0, v0, p1

    aget v0, v0, v4

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/NewAccountFragment;->a(I)V

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 169
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 170
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 171
    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 173
    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 174
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/NewAccountFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->d()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/NewAccountFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/NewAccountFragment;->a(I)V

    .line 122
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 123
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/NewAccountFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->f()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->c()Ljava/lang/String;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 182
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->d()V

    .line 200
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/NewAccountFragment;->b(I)V

    goto :goto_0

    .line 194
    :cond_1
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "+"

    const-string v2, "%20"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->o:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "javascript:checkPasswordStrength(\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\');"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->d()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/NewAccountFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->e()V

    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/NewAccountFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->p:Z

    if-eqz v0, :cond_1

    .line 211
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-eqz v0, :cond_2

    .line 212
    new-array v0, v1, [I

    .line 213
    new-array v1, v1, [I

    .line 214
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->j:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->getLocationInWindow([I)V

    .line 215
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->q:Landroid/widget/ScrollView;

    invoke-virtual {v2, v1}, Landroid/widget/ScrollView;->getLocationInWindow([I)V

    .line 217
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->q:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    aget v0, v0, v3

    add-int/2addr v0, v2

    aget v1, v1, v3

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->j:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->q:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    .line 223
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->q:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    .line 225
    add-int v3, v1, v2

    if-le v0, v3, :cond_1

    .line 226
    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 227
    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->q:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 230
    :cond_1
    return-void

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getBottom()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/db;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479
    const-class v0, Lcom/dropbox/android/activity/db;

    return-object v0
.end method

.method protected final b()V
    .locals 5

    .prologue
    .line 419
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/db;

    iget-object v1, p0, Lcom/dropbox/android/activity/NewAccountFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/NewAccountFragment;->f:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/EmailTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/dropbox/android/activity/db;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/db;

    invoke-interface {v0}, Lcom/dropbox/android/activity/db;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/db;

    const v1, 0x7f03005d

    const v2, 0x7f0300ac

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/dropbox/android/activity/db;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v1

    .line 243
    const v0, 0x7f070108

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->d:Landroid/widget/EditText;

    .line 245
    if-nez p3, :cond_0

    .line 246
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 249
    :cond_0
    const v0, 0x7f070109

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->e:Landroid/widget/EditText;

    .line 250
    const v0, 0x7f07010a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/EmailTextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->f:Lcom/dropbox/android/widget/EmailTextView;

    .line 251
    const v0, 0x7f0700f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->g:Landroid/widget/TextView;

    .line 252
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->f:Lcom/dropbox/android/widget/EmailTextView;

    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->g:Landroid/widget/TextView;

    const-string v3, "new"

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/widget/EmailTextView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 254
    const v0, 0x7f070112

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->o:Landroid/webkit/WebView;

    .line 255
    const v0, 0x7f070110

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->h:Landroid/widget/TextView;

    .line 256
    const v0, 0x7f07010b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    .line 257
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/widget/EditText;)V

    .line 258
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/cR;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cR;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 270
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-eqz v0, :cond_1

    .line 271
    const v0, 0x7f0701aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->j:Landroid/widget/CheckBox;

    .line 272
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 273
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->j:Landroid/widget/CheckBox;

    if-eqz v2, :cond_1

    .line 274
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->j:Landroid/widget/CheckBox;

    new-instance v3, Lcom/dropbox/android/activity/cS;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/cS;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;Landroid/graphics/Typeface;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 290
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-eqz v0, :cond_6

    const v0, 0x7f0701a6

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->n:Landroid/view/View;

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->n:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/cT;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cT;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    const v0, 0x7f070107

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->q:Landroid/widget/ScrollView;

    .line 300
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    .line 301
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    const/4 v2, 0x0

    const v3, 0x7f07010c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    .line 302
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    const v2, 0x7f07010d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v4

    .line 303
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    const/4 v2, 0x2

    const v3, 0x7f07010e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    .line 304
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->k:[Landroid/view/View;

    const/4 v2, 0x3

    const v3, 0x7f07010f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    .line 306
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-eqz v0, :cond_7

    .line 307
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 308
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0100aa

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 309
    iget v0, v0, Landroid/util/TypedValue;->data:I

    iput v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->m:I

    .line 311
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 312
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0100a9

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 313
    iget v0, v0, Landroid/util/TypedValue;->data:I

    iput v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->l:I

    .line 319
    :goto_1
    iput-boolean v4, p0, Lcom/dropbox/android/activity/NewAccountFragment;->p:Z

    .line 321
    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/dropbox/android/activity/cU;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/cU;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 341
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->o:Landroid/webkit/WebView;

    new-instance v2, Lcom/dropbox/android/activity/cW;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cW;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 357
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/cX;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cX;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 373
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/cY;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cY;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 382
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->o:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 383
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 384
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->o:Landroid/webkit/WebView;

    new-instance v2, Lcom/dropbox/android/activity/da;

    iget-object v3, p0, Lcom/dropbox/android/activity/NewAccountFragment;->r:Landroid/os/Handler;

    invoke-direct {v2, v3}, Lcom/dropbox/android/activity/da;-><init>(Landroid/os/Handler;)V

    const-string v3, "activity"

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->o:Landroid/webkit/WebView;

    const-string v2, "file:///android_asset/js/pw.html"

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 387
    iget-boolean v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->c:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 389
    const v0, 0x7f0701b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 390
    new-instance v2, Lcom/dropbox/android/activity/cZ;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cZ;-><init>(Lcom/dropbox/android/activity/NewAccountFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 402
    const-string v2, "ARG_EMAIL_PREFILL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 403
    if-eqz v2, :cond_3

    .line 404
    iget-object v3, p0, Lcom/dropbox/android/activity/NewAccountFragment;->f:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v3, v2}, Lcom/dropbox/android/widget/EmailTextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    :cond_3
    const-string v2, "ARG_FIRST_NAME_PREFILL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 407
    if-eqz v2, :cond_4

    .line 408
    iget-object v3, p0, Lcom/dropbox/android/activity/NewAccountFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 410
    :cond_4
    const-string v2, "ARG_LAST_NAME_PREFILL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 411
    if-eqz v0, :cond_5

    .line 412
    iget-object v2, p0, Lcom/dropbox/android/activity/NewAccountFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 415
    :cond_5
    return-object v1

    .line 290
    :cond_6
    const v0, 0x7f070111

    goto/16 :goto_0

    .line 315
    :cond_7
    invoke-virtual {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080058

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->m:I

    .line 316
    invoke-virtual {p0}, Lcom/dropbox/android/activity/NewAccountFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080059

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->l:I

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onResume()V

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/activity/NewAccountFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/db;

    const v1, 0x7f0d0035

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/db;->a(I)V

    .line 114
    return-void
.end method
