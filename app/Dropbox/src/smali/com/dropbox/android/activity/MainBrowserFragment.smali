.class public Lcom/dropbox/android/activity/MainBrowserFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dialog/I;
.implements Lcom/dropbox/android/widget/quickactions/e;


# instance fields
.field private d:Landroid/view/View;

.field private e:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/MainBrowserFragment;->setHasOptionsMenu(Z)V

    .line 44
    return-void
.end method

.method private static a(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 5

    .prologue
    .line 264
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 265
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/dropbox/android/activity/TextEditActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 266
    const-string v1, "EXTRA_OUTPUT_DIR"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 267
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 268
    return-void
.end method

.method private static a(Landroid/database/Cursor;Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;Lcom/dropbox/android/util/analytics/r;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 205
    invoke-virtual {p1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_SHOW_RI_ENTRY_POINT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/util/analytics/r;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/database/Cursor;Lcom/dropbox/android/util/HistoryEntry;)V
    .locals 3

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->m()V

    move-object v0, p2

    .line 244
    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Landroid/database/Cursor;Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;Lcom/dropbox/android/util/analytics/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 246
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->h()V

    .line 247
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cp()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "location"

    const-string v2, "files-tab"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 256
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 250
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->i()V

    .line 253
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->b(Z)V

    .line 254
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/database/Cursor;Lcom/dropbox/android/util/HistoryEntry;)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->n()V

    .line 311
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 312
    return-void
.end method

.method public final b()Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/dropbox/android/widget/as;->a:Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 284
    return-void
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 288
    const v0, 0x7f030034

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->l()V

    .line 261
    return-void
.end method

.method protected final n()V
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->n()V

    .line 304
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->n()V

    .line 305
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->j()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    .line 193
    const-class v1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 194
    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    .line 195
    new-instance v0, Lcom/dropbox/android/filemanager/T;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->a:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->a:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->e()Ldbxyzptlk/db231222/n/r;

    move-result-object v6

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->f()Lcom/dropbox/android/provider/E;

    move-result-object v7

    new-instance v8, Lcom/dropbox/android/activity/cK;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v9

    invoke-virtual {v9}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v9

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v10

    invoke-virtual {v10}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v10

    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v11

    invoke-direct {v8, v9, v10, v11}, Lcom/dropbox/android/activity/cK;-><init>(Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/service/a;Ldbxyzptlk/db231222/r/e;)V

    invoke-direct/range {v0 .. v8}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/j/i;Lcom/dropbox/android/taskqueue/U;Lcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;Lcom/dropbox/android/provider/E;Lcom/dropbox/android/filemanager/U;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V

    .line 87
    const/16 v0, 0xce

    const v1, 0x7f0d0094

    invoke-interface {p1, v2, v0, v2, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02013f

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 89
    const/16 v0, 0xc9

    const v1, 0x7f0d0097

    invoke-interface {p1, v2, v0, v2, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x1080055

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 91
    const/16 v0, 0xca

    const v1, 0x7f0d0095

    invoke-interface {p1, v2, v0, v2, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020153

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 93
    const/16 v0, 0xcb

    const v1, 0x7f0d0096

    invoke-interface {p1, v2, v0, v2, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020152

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 95
    const/16 v0, 0xcc

    const v1, 0x7f0d0093

    invoke-interface {p1, v2, v0, v2, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020155

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 97
    const/16 v0, 0xcd

    const v1, 0x7f0d0092

    invoke-interface {p1, v2, v0, v2, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020154

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 99
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 71
    const v0, 0x7f0700a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->d:Landroid/view/View;

    .line 72
    const v0, 0x7f0700a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->e:Landroid/widget/Button;

    .line 73
    iget-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->e:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/cJ;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cJ;-><init>(Lcom/dropbox/android/activity/MainBrowserFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    return-object v1
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 103
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 137
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 106
    :pswitch_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 107
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/dialog/UploadDialog;

    move-result-object v0

    .line 108
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/dialog/UploadDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v1

    .line 109
    goto :goto_0

    .line 113
    :pswitch_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 114
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v0

    .line 115
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 116
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :pswitch_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 121
    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/util/DropboxPath;)V

    move v0, v1

    .line 122
    goto :goto_0

    .line 124
    :pswitch_3
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bh()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 125
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->l()V

    move v0, v1

    .line 126
    goto :goto_0

    .line 128
    :pswitch_4
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    move v0, v1

    .line 129
    goto :goto_0

    .line 132
    :pswitch_5
    invoke-static {p0}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/activity/dialog/SortOrderDialog;

    move-result-object v0

    .line 133
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/dialog/SortOrderDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v1

    .line 134
    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onResume()V

    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->l()V

    .line 65
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 293
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onStop()V

    .line 294
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->n()V

    .line 298
    :cond_0
    return-void
.end method

.method protected final p()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 214
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v2

    .line 217
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->c()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Lcom/dropbox/android/util/analytics/r;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 221
    :goto_0
    if-eqz v0, :cond_2

    .line 224
    iget-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 225
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p()V

    .line 239
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 217
    goto :goto_0

    .line 226
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/a;->D()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/a;->B()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 229
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 230
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p()V

    .line 231
    const v0, 0x7f0d0196

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(I)V

    goto :goto_1

    .line 236
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/activity/MainBrowserFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 237
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->h()V

    goto :goto_1
.end method
