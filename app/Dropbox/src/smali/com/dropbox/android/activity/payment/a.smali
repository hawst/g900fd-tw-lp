.class final Lcom/dropbox/android/activity/payment/a;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/a;->a:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/a;->a:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    const-string v2, "CARD_IO_JS_CALLBACK_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->a(Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 65
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bl()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 66
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/a;->a:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->l()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lio/card/payment/CardIOActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    const-string v1, "io.card.payment.appToken"

    const-string v2, "0696c6a2bc0b4885a11aa865c1889af8"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v1, "io.card.payment.suppressManual"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    const-string v1, "io.card.payment.requireExpiry"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    const-string v1, "io.card.payment.requireCVV"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    const-string v1, "io.card.payment.requireZip"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/a;->a:Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->l()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 82
    return-void
.end method
