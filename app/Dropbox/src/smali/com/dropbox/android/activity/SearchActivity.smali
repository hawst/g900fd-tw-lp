.class public Lcom/dropbox/android/activity/SearchActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 56
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->finish()V

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 23
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->finish()V

    goto :goto_0

    .line 28
    :cond_3
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SearchActivity;->setContentView(I)V

    .line 30
    if-nez p1, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/dropbox/android/activity/SearchFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/SearchFragment;-><init>()V

    .line 36
    new-instance v2, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    invoke-direct {v2, v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/SearchFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 38
    const v2, 0x7f0700b2

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 39
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method
