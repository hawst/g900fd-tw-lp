.class public final Lcom/dropbox/android/activity/aF;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v1, p0, Lcom/dropbox/android/activity/aF;->a:Z

    .line 28
    if-eqz p1, :cond_0

    .line 29
    const-string v0, "SIS_KEY_WARN_IF_NO_EXTERNAL_STORAGE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/aF;->a:Z

    .line 31
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 37
    const-string v0, "SIS_KEY_WARN_IF_NO_EXTERNAL_STORAGE"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/aF;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    return-void
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/aF;->a:Z

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/aF;->a:Z

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/aF;->a:Z

    goto :goto_0
.end method
