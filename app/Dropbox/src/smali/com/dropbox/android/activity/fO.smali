.class final Lcom/dropbox/android/activity/fO;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/dropbox/android/activity/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/dropbox/android/activity/fO;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/fO;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 4

    .prologue
    .line 208
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->K()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "what"

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "extra"

    int-to-long v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "container"

    iget-object v2, p0, Lcom/dropbox/android/activity/fO;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fO;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fO;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->c(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/activity/fO;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const/4 v0, 0x1

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
