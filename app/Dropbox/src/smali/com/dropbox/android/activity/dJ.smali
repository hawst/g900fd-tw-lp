.class final Lcom/dropbox/android/activity/dJ;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/n/K;

.field final synthetic b:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/K;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/dropbox/android/activity/dJ;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/dJ;->a:Ldbxyzptlk/db231222/n/K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/activity/dJ;->a:Ldbxyzptlk/db231222/n/K;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/K;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/dJ;->a:Ldbxyzptlk/db231222/n/K;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/au;->a(Ldbxyzptlk/db231222/n/K;)V

    .line 296
    iget-object v0, p0, Lcom/dropbox/android/activity/dJ;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/PrefsActivity;->c(Lcom/dropbox/android/activity/PrefsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/activity/dJ;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/PrefsActivity;->e(Lcom/dropbox/android/activity/PrefsActivity;)Landroid/preference/PreferenceCategory;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/dJ;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/PrefsActivity;->d(Lcom/dropbox/android/activity/PrefsActivity;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 298
    iget-object v0, p0, Lcom/dropbox/android/activity/dJ;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/PrefsActivity;->f(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aM;->h:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 302
    :goto_0
    return v2

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dJ;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/ef;->a(Lcom/dropbox/android/activity/PrefsActivity;)V

    goto :goto_0
.end method
