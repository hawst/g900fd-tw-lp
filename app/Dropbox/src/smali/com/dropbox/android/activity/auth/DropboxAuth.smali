.class public Lcom/dropbox/android/activity/auth/DropboxAuth;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Z


# instance fields
.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/Button;

.field private n:Landroid/webkit/WebView;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->r:Z

    .line 483
    return-void
.end method

.method private static a(Landroid/content/Intent;Ldbxyzptlk/db231222/z/Z;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 405
    const-string v0, "ACCESS_TOKEN"

    iget-object v1, p1, Ldbxyzptlk/db231222/z/Z;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    const-string v0, "ACCESS_SECRET"

    iget-object v1, p1, Ldbxyzptlk/db231222/z/Z;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 407
    const-string v0, "UID"

    iget-wide v1, p1, Ldbxyzptlk/db231222/z/Z;->a:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    const-string v0, "AUTH_STATE"

    iget-object v1, p1, Ldbxyzptlk/db231222/z/Z;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 387
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 390
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 392
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/auth/DropboxAuth;Ldbxyzptlk/db231222/z/Z;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Ldbxyzptlk/db231222/z/Z;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/auth/DropboxAuth;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/z/Z;)V
    .locals 3

    .prologue
    .line 335
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 337
    invoke-static {v0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Landroid/content/Intent;Ldbxyzptlk/db231222/z/Z;)Landroid/content/Intent;

    move-result-object v0

    .line 339
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivity(Landroid/content/Intent;)V

    .line 340
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->au()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "calling.pkg"

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "calling.class"

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "consumer.key"

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 341
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_0
    return-void

    .line 342
    :catch_0
    move-exception v0

    .line 343
    sget-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v1, "Unable to find target activity at end of authentication."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    goto :goto_0

    .line 350
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->p()Landroid/content/Intent;

    move-result-object v0

    .line 351
    invoke-static {v0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Landroid/content/Intent;Ldbxyzptlk/db231222/z/Z;)Landroid/content/Intent;

    move-result-object v0

    .line 356
    :try_start_1
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 357
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 363
    :catch_1
    move-exception v0

    .line 366
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    goto :goto_0

    .line 361
    :cond_1
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivity(Landroid/content/Intent;)V

    .line 362
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->au()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "consumer.key"

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 304
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->r:Z

    .line 305
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 306
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 307
    const v1, 0x7f0d0202

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 308
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 309
    const v1, 0x7f0d0014

    new-instance v2, Lcom/dropbox/android/activity/auth/m;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/auth/m;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 316
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 317
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->av()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "msg"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 318
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->q:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->p:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->f()V

    return-void
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 256
    sget-boolean v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->b:Z

    return v0
.end method

.method private e()Ljava/lang/String;
    .locals 5

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v0

    .line 262
    const-string v1, "/app_info"

    .line 263
    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "k"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "s"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "locale"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-interface {v0}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "state"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->i:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 269
    invoke-interface {v0}, Ldbxyzptlk/db231222/y/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v3, "r6"

    invoke-static {v0, v3, v1, v2}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->g()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 274
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ax()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 275
    new-instance v0, Lcom/dropbox/android/activity/auth/p;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/dropbox/android/activity/auth/p;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;Ldbxyzptlk/db231222/z/M;)V

    .line 276
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/p;->a(I)V

    .line 277
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/p;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 278
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->k()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/webkit/WebView;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aw()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 282
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->n()V

    .line 283
    return-void
.end method

.method static synthetic h(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/activity/auth/DropboxAuth;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/activity/auth/DropboxAuth;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->i:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 286
    new-instance v0, Lcom/dropbox/android/activity/auth/o;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/dropbox/android/activity/auth/o;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    .line 287
    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/auth/o;->a(I)V

    .line 288
    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 293
    return-void
.end method

.method static synthetic k(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->q()V

    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 296
    const v0, 0x7f0d0203

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Ljava/lang/String;)V

    .line 297
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 321
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "db-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/error"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 325
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 329
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    .line 332
    return-void

    .line 330
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private o()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 370
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->p()Landroid/content/Intent;

    move-result-object v2

    .line 371
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 372
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 374
    :cond_0
    sget-object v1, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v2, "Authenticating API app has not set up proper intent filter for URI scheme."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :goto_0
    return v0

    .line 376
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v1, :cond_2

    .line 378
    sget-object v1, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v2, "Multiple entries are registered for URI scheme for authenticating API app."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "Multiple activities registered for URI scheme"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 383
    goto :goto_0
.end method

.method private p()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 396
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 397
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "db-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/connect"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 399
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 400
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 401
    return-object v0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 413
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 414
    const v1, 0x7f0d0202

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 415
    const v1, 0x7f0d0208

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 416
    const v1, 0x7f0d0014

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 417
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 418
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 92
    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivityForResult(Landroid/content/Intent;I)V

    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    .line 226
    :goto_0
    return-void

    .line 97
    :cond_0
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->setContentView(I)V

    .line 99
    const/4 v0, 0x1

    sput-boolean v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->b:Z

    .line 101
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 104
    const-string v2, "CONSUMER_KEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    .line 105
    const-string v2, "CONSUMER_SIG"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->f:Ljava/lang/String;

    .line 106
    const-string v2, "CALLING_PACKAGE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Ljava/lang/String;

    .line 108
    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->f:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 109
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v1, "App trying to authenticate without setting a consumer key or sig."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    goto :goto_0

    .line 114
    :cond_2
    const-string v2, "com.dropbox.android.AUTHENTICATE_V1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 116
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 117
    const-class v0, Lcom/dropbox/client2/android/AuthActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Ljava/lang/String;

    .line 138
    :cond_3
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->o()Z

    move-result v0

    if-nez v0, :cond_7

    .line 139
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    goto :goto_0

    .line 119
    :cond_4
    const-string v2, "com.dropbox.android.AUTHENTICATE_V2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 123
    const-string v1, "CALLING_CLASS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Ljava/lang/String;

    .line 124
    const-string v1, "AUTH_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->i:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 127
    :cond_5
    sget-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v1, "App trying to authenticate without setting calling package, class, or state."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    goto/16 :goto_0

    .line 132
    :cond_6
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    .line 133
    sget-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown authentication action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 143
    :cond_7
    const v0, 0x7f070047

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->j:Landroid/widget/Button;

    .line 144
    const v0, 0x7f070046

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->k:Landroid/widget/Button;

    .line 145
    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->l:Landroid/widget/Button;

    .line 146
    const v0, 0x7f07004a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->m:Landroid/widget/Button;

    .line 147
    const v0, 0x7f070045

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/webkit/WebView;

    .line 148
    const v0, 0x7f070043

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->o:Landroid/view/View;

    .line 149
    const v0, 0x7f070044

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->p:Landroid/view/View;

    .line 150
    const v0, 0x7f070048

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->q:Landroid/view/View;

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/webkit/WebView;

    new-instance v1, Lcom/dropbox/android/activity/auth/g;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/g;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 191
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/webkit/WebView;

    new-instance v1, Lcom/dropbox/android/activity/auth/h;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/h;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 199
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->j:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/i;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/i;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->k:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/j;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/j;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->l:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/k;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/k;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->m:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/l;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/l;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 242
    packed-switch p1, :pswitch_data_0

    .line 251
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 244
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 245
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 246
    const v1, 0x7f0d0204

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 247
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 248
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 230
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 232
    iget-boolean v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->r:Z

    if-nez v0, :cond_0

    .line 233
    new-instance v0, Lcom/dropbox/android/activity/auth/o;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/dropbox/android/activity/auth/o;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    .line 234
    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/auth/o;->a(I)V

    .line 235
    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 237
    :cond_0
    return-void
.end method
