.class public abstract Lcom/dropbox/android/activity/fB;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/at;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAM:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/util/at;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/dropbox/android/albums/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/t",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field protected c:I

.field private final d:Landroid/os/Handler;

.field private final e:Landroid/support/v4/app/FragmentActivity;

.field private final f:Lcom/dropbox/android/activity/base/l;

.field private final g:Lcom/dropbox/android/activity/base/n;

.field private final h:Landroid/support/v4/app/Fragment;

.field private final i:Ljava/lang/String;

.field private j:Landroid/os/Parcelable;

.field private k:Ljava/lang/String;

.field private l:Lcom/dropbox/android/albums/u;

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/albums/t",
            "<TRESU",
            "LT;",
            ">;",
            "Landroid/support/v4/app/Fragment;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->d:Landroid/os/Handler;

    .line 71
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    .line 89
    iput-boolean v1, p0, Lcom/dropbox/android/activity/fB;->m:Z

    .line 90
    iput-boolean v1, p0, Lcom/dropbox/android/activity/fB;->n:Z

    .line 161
    iput-object p1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "STATUS_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->i:Ljava/lang/String;

    .line 163
    iput-object p2, p0, Lcom/dropbox/android/activity/fB;->b:Lcom/dropbox/android/albums/t;

    .line 164
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->e:Landroid/support/v4/app/FragmentActivity;

    .line 165
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->f:Lcom/dropbox/android/activity/base/l;

    .line 166
    iput-object p3, p0, Lcom/dropbox/android/activity/fB;->h:Landroid/support/v4/app/Fragment;

    .line 167
    check-cast p3, Lcom/dropbox/android/activity/base/n;

    iput-object p3, p0, Lcom/dropbox/android/activity/fB;->g:Lcom/dropbox/android/activity/base/n;

    .line 168
    iput p4, p0, Lcom/dropbox/android/activity/fB;->c:I

    .line 169
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/albums/t",
            "<TRESU",
            "LT;",
            ">;",
            "Landroid/support/v4/app/FragmentActivity;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->d:Landroid/os/Handler;

    .line 71
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    .line 89
    iput-boolean v1, p0, Lcom/dropbox/android/activity/fB;->m:Z

    .line 90
    iput-boolean v1, p0, Lcom/dropbox/android/activity/fB;->n:Z

    .line 181
    iput-object p1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "STATUS_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->i:Ljava/lang/String;

    .line 183
    iput-object p2, p0, Lcom/dropbox/android/activity/fB;->b:Lcom/dropbox/android/albums/t;

    .line 184
    iput-object p3, p0, Lcom/dropbox/android/activity/fB;->e:Landroid/support/v4/app/FragmentActivity;

    .line 185
    check-cast p3, Lcom/dropbox/android/activity/base/l;

    iput-object p3, p0, Lcom/dropbox/android/activity/fB;->f:Lcom/dropbox/android/activity/base/l;

    .line 186
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->h:Landroid/support/v4/app/Fragment;

    .line 187
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->g:Lcom/dropbox/android/activity/base/n;

    .line 188
    iput p4, p0, Lcom/dropbox/android/activity/fB;->c:I

    .line 189
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/fB;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/fB;->d()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/fB;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dropbox/android/activity/fB;->c()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/fB;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 246
    new-instance v0, Lcom/dropbox/android/activity/fC;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fC;-><init>(Lcom/dropbox/android/activity/fB;)V

    .line 252
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->g:Lcom/dropbox/android/activity/base/n;

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->g:Lcom/dropbox/android/activity/base/n;

    invoke-interface {v1, v0}, Lcom/dropbox/android/activity/base/n;->a(Ljava/lang/Runnable;)Z

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->f:Lcom/dropbox/android/activity/base/l;

    invoke-interface {v1, v0}, Lcom/dropbox/android/activity/base/l;->a(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 263
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 264
    invoke-virtual {p0}, Lcom/dropbox/android/activity/fB;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 266
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->h:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_4

    .line 267
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->h:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 272
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->b:Lcom/dropbox/android/albums/t;

    iget-object v3, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/fF;

    move-result-object v3

    .line 273
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/dropbox/android/activity/fF;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v1

    .line 275
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v4

    sget-object v5, Lcom/dropbox/android/taskqueue/y;->c:Lcom/dropbox/android/taskqueue/y;

    if-eq v4, v5, :cond_7

    .line 276
    :cond_0
    if-eqz v1, :cond_1

    .line 277
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v1

    sget-object v4, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v1, v4, :cond_6

    .line 278
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    invoke-virtual {p0, v3, v1}, Lcom/dropbox/android/activity/fB;->b(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V

    .line 284
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->b:Lcom/dropbox/android/albums/t;

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    invoke-virtual {v0, v1, v3}, Lcom/dropbox/android/albums/t;->b(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 288
    :cond_2
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    .line 289
    iput-object v2, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    .line 298
    :cond_3
    :goto_3
    return-void

    .line 269
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->e:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v1, v2

    .line 273
    goto :goto_1

    .line 280
    :cond_6
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    invoke-virtual {p0, v3, v1}, Lcom/dropbox/android/activity/fB;->a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V

    goto :goto_2

    .line 292
    :cond_7
    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_3

    .line 293
    iget v1, p0, Lcom/dropbox/android/activity/fB;->c:I

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v1

    .line 294
    iget-object v2, p0, Lcom/dropbox/android/activity/fB;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private e()V
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/dropbox/android/activity/fD;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/fD;-><init>(Lcom/dropbox/android/activity/fB;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    .line 312
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/u;",
            "TPARAM;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->b:Lcom/dropbox/android/albums/t;

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/t;->b(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    .line 243
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 203
    if-eqz p1, :cond_0

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_mCurId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_mParcelableCtx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 213
    invoke-direct {p0}, Lcom/dropbox/android/activity/fB;->e()V

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->b:Lcom/dropbox/android/albums/t;

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/t;->a(Ljava/lang/String;Lcom/dropbox/android/albums/u;)V

    .line 215
    invoke-direct {p0}, Lcom/dropbox/android/activity/fB;->c()V

    .line 217
    :cond_1
    return-void
.end method

.method protected abstract a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<TRESU",
            "LT;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;ILandroid/os/Parcelable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;I",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    iput p2, p0, Lcom/dropbox/android/activity/fB;->c:I

    .line 148
    invoke-virtual {p0, p1, p3}, Lcom/dropbox/android/activity/fB;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 149
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    if-eqz v0, :cond_1

    .line 126
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "An action is already in progress"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_1
    iget v0, p0, Lcom/dropbox/android/activity/fB;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 130
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No status string is set"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_2
    iput-object p2, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    .line 141
    invoke-direct {p0}, Lcom/dropbox/android/activity/fB;->e()V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->l:Lcom/dropbox/android/albums/u;

    invoke-virtual {p0, v0, p1}, Lcom/dropbox/android/activity/fB;->a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    .line 143
    invoke-direct {p0}, Lcom/dropbox/android/activity/fB;->d()V

    .line 144
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_mCurId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_mParcelableCtx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fB;->j:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 230
    :cond_0
    return-void
.end method

.method protected b(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<TRESU",
            "LT;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/dropbox/android/activity/fB;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
