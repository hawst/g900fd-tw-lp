.class public Lcom/dropbox/android/activity/TourPageWithThumbsFragment;
.super Lcom/dropbox/android/activity/TourPageFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/at;


# instance fields
.field protected n:Landroid/view/View;

.field private final o:[[I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/activity/TourPageFragment;-><init>()V

    .line 19
    const/4 v0, 0x7

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v5, [I

    const v2, 0x7f07007c

    aput v2, v1, v4

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v5, [I

    const v3, 0x7f07007d

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [I

    const v3, 0x7f07007e

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [I

    const v3, 0x7f070080

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [I

    const v3, 0x7f070081

    aput v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->o:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f07007a
        0x7f070083
    .end array-data

    :array_1
    .array-data 4
        0x7f07007b
        0x7f070084
    .end array-data
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 41
    const/4 v0, 0x7

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    move v1, v2

    .line 50
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->o:[[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->o:[[I

    aget-object v5, v0, v1

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_0

    aget v0, v5, v3

    .line 52
    iget-object v7, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 53
    aget v7, v4, v1

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 51
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 50
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56
    :cond_1
    return-void

    .line 41
    nop

    :array_0
    .array-data 4
        0x7f020268
        0x7f020269
        0x7f02026a
        0x7f02026b
        0x7f02026c
        0x7f02026d
        0x7f02026e
    .end array-data
.end method

.method private a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;Z)V
    .locals 2

    .prologue
    .line 97
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 98
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 99
    if-eqz p3, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040004

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 102
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 104
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    return-void
.end method

.method private a([Landroid/graphics/Bitmap;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 59
    move v1, v2

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->o:[[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->o:[[I

    aget-object v4, v0, v1

    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    aget v0, v4, v3

    .line 62
    iget-object v6, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 63
    aget-object v6, p1, v1

    invoke-direct {p0, v0, v6, p2}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;Z)V

    .line 61
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 59
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_1
    return-void
.end method

.method public static b(Lcom/dropbox/android/activity/fn;I)Lcom/dropbox/android/activity/TourPageWithThumbsFragment;
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;-><init>()V

    .line 31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v2, "ARG_PAGE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/fn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v2, "ARG_INDEX"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 34
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 36
    return-object v0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v4, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->o:[[I

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v6, v4, v3

    .line 109
    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_0

    aget v0, v6, v1

    .line 110
    iget-object v8, p0, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->n:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 112
    const v8, 0x7f0201dc

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 108
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 115
    :cond_1
    return-void
.end method


# virtual methods
.method public final a([Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->a([Landroid/graphics/Bitmap;Z)V

    .line 90
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/TourPageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 73
    invoke-static {}, Lcom/dropbox/android/filemanager/aq;->a()Lcom/dropbox/android/filemanager/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aq;->c()[Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 76
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->a([Landroid/graphics/Bitmap;Z)V

    .line 85
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/aC;->a()Lcom/dropbox/android/util/aC;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aC;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-static {p0}, Lcom/dropbox/android/filemanager/aq;->a(Lcom/dropbox/android/filemanager/at;)V

    goto :goto_0

    .line 81
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->a()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 119
    invoke-super {p0}, Lcom/dropbox/android/activity/TourPageFragment;->onDestroyView()V

    .line 120
    invoke-direct {p0}, Lcom/dropbox/android/activity/TourPageWithThumbsFragment;->b()V

    .line 121
    return-void
.end method
