.class public Lcom/dropbox/android/activity/DropboxGetFrom;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/g/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 95
    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    .line 100
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v3

    .line 104
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ar()Lcom/dropbox/android/util/analytics/l;

    move-result-object v4

    const-string v5, "request.mime.type"

    invoke-virtual {v4, v5, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v4, "result.mime.type"

    invoke-virtual {v1, v4, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v4

    const-string v5, "caller"

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v4, v5, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 109
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 110
    if-eqz v0, :cond_2

    const-string v2, "image/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    const-string v2, "true"

    const-string v3, "crop"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.camera.action.CROP"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 115
    const/high16 v1, 0x2000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 117
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/DropboxGetFrom;->startActivity(Landroid/content/Intent;)V

    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_1
    return-void

    .line 104
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v1

    .line 125
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 126
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 127
    if-eqz v0, :cond_3

    .line 128
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    :goto_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/DropboxGetFrom;->setResult(ILandroid/content/Intent;)V

    .line 133
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->finish()V

    goto :goto_1

    .line 130
    :cond_3
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 44
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    .line 47
    :cond_0
    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 138
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 141
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 52
    invoke-static {p0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/app/Activity;)V

    .line 53
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxGetFrom;->setContentView(I)V

    .line 60
    const v0, 0x7f0d01d8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxGetFrom;->setTitle(I)V

    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-static {p0, v0, v2}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 70
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxGetFrom;->startActivity(Landroid/content/Intent;)V

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->finish()V

    .line 89
    :goto_0
    return-void

    .line 75
    :cond_0
    if-nez p1, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->l()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 78
    invoke-static {}, Lcom/dropbox/android/activity/GetFromFragment;->d()Lcom/dropbox/android/activity/GetFromFragment;

    move-result-object v2

    .line 79
    new-instance v3, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v3, v1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v2, v3}, Lcom/dropbox/android/activity/GetFromFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 80
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/dropbox/android/activity/GetFromFragment;->a(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 82
    const v3, 0x7f0700b2

    invoke-virtual {v1, v3, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 83
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 86
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    .line 87
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aq()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "request.mime.type"

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "caller"

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
