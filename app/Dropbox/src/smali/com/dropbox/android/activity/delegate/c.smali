.class public abstract enum Lcom/dropbox/android/activity/delegate/c;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/delegate/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/delegate/c;

.field public static final enum b:Lcom/dropbox/android/activity/delegate/c;

.field public static final enum c:Lcom/dropbox/android/activity/delegate/c;

.field public static final enum d:Lcom/dropbox/android/activity/delegate/c;

.field private static final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/dropbox/android/activity/delegate/c;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic g:[Lcom/dropbox/android/activity/delegate/c;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/dropbox/android/activity/delegate/d;

    const-string v1, "SETTINGS"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/delegate/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/c;->a:Lcom/dropbox/android/activity/delegate/c;

    .line 34
    new-instance v0, Lcom/dropbox/android/activity/delegate/e;

    const-string v1, "HELP"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/delegate/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/c;->b:Lcom/dropbox/android/activity/delegate/c;

    .line 47
    new-instance v0, Lcom/dropbox/android/activity/delegate/f;

    const-string v1, "UPGRADE"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/delegate/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/c;->c:Lcom/dropbox/android/activity/delegate/c;

    .line 57
    new-instance v0, Lcom/dropbox/android/activity/delegate/g;

    const-string v1, "DEV_SETTINGS"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/delegate/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/delegate/c;->d:Lcom/dropbox/android/activity/delegate/c;

    .line 26
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/activity/delegate/c;

    sget-object v1, Lcom/dropbox/android/activity/delegate/c;->a:Lcom/dropbox/android/activity/delegate/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/delegate/c;->b:Lcom/dropbox/android/activity/delegate/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/delegate/c;->c:Lcom/dropbox/android/activity/delegate/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/delegate/c;->d:Lcom/dropbox/android/activity/delegate/c;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/activity/delegate/c;->g:[Lcom/dropbox/android/activity/delegate/c;

    .line 68
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/delegate/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 74
    const-class v0, Lcom/dropbox/android/activity/delegate/c;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/delegate/c;

    .line 75
    sget-object v2, Lcom/dropbox/android/activity/delegate/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/delegate/c;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 77
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/c;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x64

    iput v0, p0, Lcom/dropbox/android/activity/delegate/c;->f:I

    .line 81
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/delegate/b;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/delegate/c;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/dropbox/android/activity/delegate/c;
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/delegate/c;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/delegate/c;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/dropbox/android/activity/delegate/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/delegate/c;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/delegate/c;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->g:[Lcom/dropbox/android/activity/delegate/c;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/delegate/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/delegate/c;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/dropbox/android/activity/delegate/c;->f:I

    return v0
.end method

.method abstract a(Lcom/dropbox/android/activity/base/BaseUserActivity;)V
.end method
