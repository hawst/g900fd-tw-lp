.class final Lcom/dropbox/android/activity/eV;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/albums/PhotosModel;

.field private final u:Lcom/dropbox/android/albums/Album;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;)V
    .locals 0

    .prologue
    .line 353
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 354
    iput-object p2, p0, Lcom/dropbox/android/activity/eV;->l:Lcom/dropbox/android/albums/PhotosModel;

    .line 355
    iput-object p3, p0, Lcom/dropbox/android/activity/eV;->u:Lcom/dropbox/android/albums/Album;

    .line 356
    return-void
.end method


# virtual methods
.method protected final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/dropbox/android/activity/eV;->y()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final y()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 360
    iget-object v0, p0, Lcom/dropbox/android/activity/eV;->l:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/eV;->u:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 361
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/eV;->a(Landroid/database/Cursor;)V

    .line 362
    return-object v0
.end method
