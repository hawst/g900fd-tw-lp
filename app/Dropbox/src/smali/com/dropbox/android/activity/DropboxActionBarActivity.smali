.class public Lcom/dropbox/android/activity/DropboxActionBarActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/activity/delegate/a;

.field private final e:Lcom/dropbox/android/activity/ai;

.field private final f:[Lcom/dropbox/android/activity/ak;

.field private g:Lcom/dropbox/android/activity/aF;

.field private h:Lcom/dropbox/android/activity/aj;

.field private i:Lcom/dropbox/android/activity/ar;

.field private final j:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 75
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 83
    new-instance v0, Lcom/dropbox/android/activity/delegate/a;

    invoke-direct {v0}, Lcom/dropbox/android/activity/delegate/a;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->b:Lcom/dropbox/android/activity/delegate/a;

    .line 300
    new-instance v0, Lcom/dropbox/android/activity/ai;

    const v1, 0x7f020288

    new-array v2, v6, [Lcom/dropbox/android/activity/al;

    sget-object v3, Lcom/dropbox/android/activity/al;->e:Lcom/dropbox/android/activity/al;

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/ai;-><init>(I[Lcom/dropbox/android/activity/al;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e:Lcom/dropbox/android/activity/ai;

    .line 302
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/activity/ak;

    new-instance v1, Lcom/dropbox/android/activity/ak;

    const v2, 0x7f020284

    new-array v3, v6, [Lcom/dropbox/android/activity/al;

    sget-object v4, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    aput-object v4, v3, v5

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/activity/ak;-><init>(I[Lcom/dropbox/android/activity/al;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/dropbox/android/activity/ak;

    const v2, 0x7f020289

    new-array v3, v7, [Lcom/dropbox/android/activity/al;

    sget-object v4, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    aput-object v4, v3, v5

    sget-object v4, Lcom/dropbox/android/activity/al;->d:Lcom/dropbox/android/activity/al;

    aput-object v4, v3, v6

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/activity/ak;-><init>(I[Lcom/dropbox/android/activity/al;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/dropbox/android/activity/ak;

    const v2, 0x7f02028e

    new-array v3, v6, [Lcom/dropbox/android/activity/al;

    sget-object v4, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/al;

    aput-object v4, v3, v5

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/activity/ak;-><init>(I[Lcom/dropbox/android/activity/al;)V

    aput-object v1, v0, v7

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e:Lcom/dropbox/android/activity/ai;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:[Lcom/dropbox/android/activity/ak;

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->g:Lcom/dropbox/android/activity/aF;

    .line 323
    new-instance v0, Lcom/dropbox/android/activity/ac;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ac;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->j:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 357
    new-instance v0, Lcom/dropbox/android/activity/ad;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ad;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->k:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 668
    iput-boolean v5, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l:Z

    .line 1016
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)Lcom/dropbox/android/activity/ai;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e:Lcom/dropbox/android/activity/ai;

    return-object v0
.end method

.method private a(Lcom/actionbarsherlock/app/ActionBar$TabListener;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 606
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v1

    .line 607
    invoke-virtual {v1, v0}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 608
    invoke-virtual {v1, v0}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 610
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar;->setNavigationMode(I)V

    .line 612
    iget-object v2, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:[Lcom/dropbox/android/activity/ak;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 613
    invoke-virtual {v1}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v5

    .line 614
    invoke-virtual {v4, v5}, Lcom/dropbox/android/activity/ak;->a(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 615
    invoke-virtual {v1, v5}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 612
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 617
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxActionBarActivity;Lcom/dropbox/android/activity/al;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/al;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 671
    const/4 v3, 0x0

    .line 672
    iget-object v5, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:[Lcom/dropbox/android/activity/ak;

    array-length v6, v5

    move v4, v1

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v2, v5, v4

    move v0, v1

    .line 673
    :goto_1
    iget-object v7, v2, Lcom/dropbox/android/activity/ak;->b:[Lcom/dropbox/android/activity/al;

    array-length v7, v7

    if-ge v0, v7, :cond_1

    .line 674
    iget-object v7, v2, Lcom/dropbox/android/activity/ak;->b:[Lcom/dropbox/android/activity/al;

    aget-object v7, v7, v0

    invoke-virtual {v7, p1}, Lcom/dropbox/android/activity/al;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 675
    invoke-virtual {v2, v0}, Lcom/dropbox/android/activity/ak;->b(I)V

    .line 681
    :goto_2
    if-nez v2, :cond_2

    .line 682
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Specified invalid type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 672
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 685
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v3

    .line 686
    invoke-virtual {v3}, Lcom/actionbarsherlock/app/ActionBar;->getTabCount()I

    move-result v4

    move v0, v1

    .line 687
    :goto_3
    if-ge v0, v4, :cond_3

    .line 688
    invoke-virtual {v3, v0}, Lcom/actionbarsherlock/app/ActionBar;->getTabAt(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v5

    .line 689
    invoke-virtual {v5}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-ne v6, v2, :cond_4

    .line 690
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l:Z

    .line 692
    :try_start_0
    invoke-virtual {v3, v5}, Lcom/actionbarsherlock/app/ActionBar;->selectTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    iput-boolean v1, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l:Z

    .line 699
    :cond_3
    return-void

    .line 694
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l:Z

    throw v0

    .line 687
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move-object v2, v3

    goto :goto_2
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    .line 517
    invoke-static {p1}, Lcom/dropbox/android/b;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    .line 522
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid uri passed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "invalid uri passed"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524
    const-string v0, "???"

    .line 525
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 526
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v0

    .line 528
    :cond_1
    sget-object v1, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calling activity w/ invalid uri was: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/DropboxActionBarActivity;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l:Z

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/DropboxActionBarActivity;)[Lcom/dropbox/android/activity/ak;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:[Lcom/dropbox/android/activity/ak;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 442
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 445
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 446
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 447
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v4, "photos"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 451
    :goto_0
    if-eqz v0, :cond_2

    .line 452
    sget-object v0, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    .line 513
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 514
    return-void

    :cond_1
    move v0, v2

    .line 447
    goto :goto_0

    .line 453
    :cond_2
    const-string v0, "ACTION_CAMERA_UPLOAD_DETAILS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ACTION_CAMERA_UPLOAD_GALLERY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 455
    :cond_3
    sget-object v0, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    .line 457
    const-string v0, "ACTION_CAMERA_UPLOAD_DETAILS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 464
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 466
    :cond_4
    const-string v0, "ACTION_MOST_RECENT_UPLOAD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 467
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    .line 469
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->m()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 470
    if-eqz v1, :cond_0

    .line 471
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 472
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    new-instance v3, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v3}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    goto :goto_1

    .line 475
    :cond_5
    const-string v0, "ACTION_UPLOAD_FAILURE_DLG"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 480
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 481
    if-nez v0, :cond_6

    .line 483
    :goto_2
    if-nez v1, :cond_7

    .line 484
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    .line 485
    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 486
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    new-instance v4, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v4, v3}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v4}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    .line 490
    :goto_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "EXTRA_STATUS"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/w;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    .line 491
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "EXTRA_FILENAME"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 492
    invoke-static {v1, v0, v3}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a(ZLcom/dropbox/android/taskqueue/w;Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/UploadErrorDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 481
    goto :goto_2

    .line 488
    :cond_7
    sget-object v0, Lcom/dropbox/android/activity/al;->c:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    goto :goto_3

    .line 493
    :cond_8
    const-string v0, "ACTION_NOTIFICATIONS_FEED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 494
    sget-object v0, Lcom/dropbox/android/activity/al;->e:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    goto/16 :goto_1

    .line 495
    :cond_9
    const-string v0, "ACTION_REMOTE_INSTALL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 496
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    .line 497
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/QrAuthActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 498
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "com.dropbox.intent.extra.REMINDER_SOURCE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 499
    if-eqz v1, :cond_a

    .line 500
    const-string v3, "com.dropbox.intent.extra.QR_LAUNCH_SOURCE"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    :cond_a
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 505
    :cond_b
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    .line 506
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 508
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 509
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 510
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    new-instance v3, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v3, v1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v3}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Lcom/dropbox/android/util/HistoryEntry;)V

    goto/16 :goto_1

    :cond_c
    move v0, v2

    goto/16 :goto_0
.end method

.method private f()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 556
    invoke-static {}, Lcom/dropbox/android/activity/lock/LockReceiver;->a()Lcom/dropbox/android/activity/lock/LockReceiver;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/activity/lock/LockReceiver;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 560
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v1

    .line 561
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/k;->f()Z

    move-result v2

    if-nez v2, :cond_0

    .line 563
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/IntroTourActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 564
    const-string v2, "EXTRA_IS_POST_LOGIN"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 565
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->startActivity(Landroid/content/Intent;)V

    .line 571
    :goto_0
    return v0

    .line 568
    :cond_0
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/k;->d(Z)V

    .line 571
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 578
    invoke-static {}, Lcom/dropbox/android/activity/lock/LockReceiver;->a()Lcom/dropbox/android/activity/lock/LockReceiver;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/activity/lock/LockReceiver;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 580
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 581
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->D()Z

    move-result v2

    if-nez v2, :cond_0

    .line 584
    invoke-static {}, Lcom/dropbox/android/util/S;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->K()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 585
    invoke-static {v0}, Lcom/dropbox/android/activity/fn;->a(Z)[Lcom/dropbox/android/activity/fn;

    move-result-object v2

    .line 586
    invoke-static {p0, v1, v2}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;[Lcom/dropbox/android/activity/fn;)Landroid/content/Intent;

    move-result-object v2

    .line 587
    const/16 v3, 0xf

    invoke-virtual {p0, v2, v3}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 588
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/P;->o(Z)V

    .line 593
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 645
    const/16 v0, 0xf

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 646
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->c:Lcom/dropbox/android/activity/base/d;

    new-instance v1, Lcom/dropbox/android/activity/af;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/af;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/d;->a(Ljava/lang/Runnable;)Z

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 652
    :cond_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.dropbox.android.file_added"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 989
    const v0, 0x7f0700cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 990
    const v0, 0x7f0700d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 992
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 993
    invoke-virtual {v2, p2}, Landroid/view/View;->setSelected(Z)V

    .line 995
    new-instance v0, Lcom/dropbox/android/activity/ag;

    invoke-direct {v0, p0, v1, v2}, Lcom/dropbox/android/activity/ag;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1003
    new-instance v0, Lcom/dropbox/android/activity/ah;

    invoke-direct {v0, p0, v1, v2}, Lcom/dropbox/android/activity/ah;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1011
    return-void

    .line 992
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->h:Lcom/dropbox/android/activity/aj;

    if-nez v0, :cond_0

    .line 705
    new-instance v0, Lcom/dropbox/android/activity/aj;

    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/aj;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->h:Lcom/dropbox/android/activity/aj;

    .line 707
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->h:Lcom/dropbox/android/activity/aj;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 621
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 622
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 624
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 399
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 400
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->finish()V

    .line 439
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-static {p0}, Lcom/dropbox/android/activity/al;->a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    .line 405
    new-instance v0, Lcom/dropbox/android/activity/aF;

    invoke-direct {v0, p1}, Lcom/dropbox/android/activity/aF;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->g:Lcom/dropbox/android/activity/aF;

    .line 407
    new-instance v0, Lcom/dropbox/android/activity/ar;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ar;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i:Lcom/dropbox/android/activity/ar;

    .line 416
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->setContentView(I)V

    .line 419
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i:Lcom/dropbox/android/activity/ar;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/actionbarsherlock/app/ActionBar$TabListener;)V

    .line 420
    if-nez p1, :cond_1

    .line 422
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e()V

    .line 426
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->z()Lcom/dropbox/android/filemanager/E;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dropbox/android/filemanager/E;->a(Z)V

    .line 435
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->k:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 438
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->j:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    goto :goto_0

    .line 429
    :cond_1
    const-string v0, "key_current_tab_type_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "key_current_tab_type_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/al;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/al;

    move-result-object v0

    .line 432
    :goto_2
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/al;)V

    goto :goto_1

    .line 429
    :cond_2
    sget-object v0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/activity/al;

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 4

    .prologue
    .line 628
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z

    move-result v0

    .line 630
    iget-object v1, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->b:Lcom/dropbox/android/activity/delegate/a;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/dropbox/android/activity/delegate/a;->a(Lcom/actionbarsherlock/view/Menu;Landroid/content/res/Resources;Lcom/dropbox/android/service/a;)Z

    move-result v1

    .line 631
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 598
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->setIntent(Landroid/content/Intent;)V

    .line 599
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->c:Lcom/dropbox/android/activity/base/d;

    new-instance v1, Lcom/dropbox/android/activity/ae;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ae;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/d;->a(Ljava/lang/Runnable;)Z

    .line 603
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->b:Lcom/dropbox/android/activity/delegate/a;

    invoke-virtual {v0, p0, p1}, Lcom/dropbox/android/activity/delegate/a;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    const/4 v0, 0x1

    .line 639
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 535
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 536
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f()Z

    move-result v0

    .line 537
    if-eqz v0, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->g()Z

    move-result v0

    .line 542
    if-nez v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->g:Lcom/dropbox/android/activity/aF;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/aF;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 547
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v0

    .line 548
    invoke-virtual {v0, p0}, Lcom/dropbox/android/filemanager/au;->a(Landroid/app/Activity;)V

    .line 549
    sget-object v1, Lcom/dropbox/android/filemanager/aC;->a:Lcom/dropbox/android/filemanager/aC;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/aC;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/analytics/r;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 388
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 389
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->g:Lcom/dropbox/android/activity/aF;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/aF;->a(Landroid/os/Bundle;)V

    .line 390
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->getSelectedTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v0

    .line 391
    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ak;

    .line 393
    const-string v1, "key_current_tab_type_name"

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ak;->b()Lcom/dropbox/android/activity/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/al;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_0
    return-void
.end method
