.class public final Lcom/dropbox/android/activity/eb;
.super Landroid/app/DialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 3

    .prologue
    .line 639
    new-instance v0, Lcom/dropbox/android/activity/eb;

    invoke-direct {v0}, Lcom/dropbox/android/activity/eb;-><init>()V

    .line 640
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/eb;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 641
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 645
    invoke-virtual {p0}, Lcom/dropbox/android/activity/eb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/PrefsActivity;

    .line 646
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 647
    const v2, 0x7f0d014e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 648
    const v2, 0x7f0e0006

    new-instance v3, Lcom/dropbox/android/activity/ec;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/ec;-><init>(Lcom/dropbox/android/activity/eb;Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 675
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
