.class public Lcom/dropbox/android/activity/LoginOrNewAcctActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/aE;
.implements Lcom/dropbox/android/activity/aQ;
.implements Lcom/dropbox/android/activity/ab;
.implements Lcom/dropbox/android/activity/cI;
.implements Lcom/dropbox/android/activity/cn;
.implements Lcom/dropbox/android/activity/cy;
.implements Lcom/dropbox/android/activity/db;
.implements Lcom/dropbox/android/activity/ev;
.implements Ldbxyzptlk/db231222/g/C;
.implements Ldbxyzptlk/db231222/g/Q;
.implements Ldbxyzptlk/db231222/g/af;
.implements Ldbxyzptlk/db231222/g/ao;
.implements Ldbxyzptlk/db231222/g/p;
.implements Ldbxyzptlk/db231222/g/w;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/activity/cB;

.field private e:Lcom/dropbox/android/activity/cC;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f:Z

    .line 474
    return-void
.end method

.method private a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 832
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 833
    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 834
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 835
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 819
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V

    .line 820
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 823
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 824
    invoke-virtual {v0, p2, p1, p3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 825
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 826
    invoke-virtual {v0, p4}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 827
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 828
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0701b1

    .line 796
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 800
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 801
    sget-object v1, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 802
    if-eqz v1, :cond_1

    .line 803
    :goto_0
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 807
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0

    .line 809
    :cond_0
    invoke-direct {p0, p1, v3, p2, p2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V

    .line 816
    :goto_1
    return-void

    .line 811
    :cond_1
    invoke-direct {p0, v3, p1, p2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)V

    goto :goto_1

    .line 814
    :cond_2
    const v0, 0x7f0700ef

    invoke-direct {p0, p1, v0, p2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginOrNewAcctActivity;Lcom/dropbox/android/activity/cC;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Lcom/dropbox/android/activity/cC;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/cC;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 943
    new-instance v0, Ldbxyzptlk/db231222/g/r;

    iget-object v2, p1, Lcom/dropbox/android/activity/cC;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/activity/cC;->b:Ljava/lang/String;

    iget-object v4, p1, Lcom/dropbox/android/activity/cC;->c:Ljava/lang/String;

    iget-object v5, p1, Lcom/dropbox/android/activity/cC;->d:Ljava/lang/String;

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/db231222/g/r;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 951
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->a:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/r;->a(I)V

    .line 952
    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/r;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 953
    return-void
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 956
    invoke-static {p0, p1}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    .line 957
    return-void
.end method

.method private b(Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)V
    .locals 2

    .prologue
    .line 1084
    new-instance v0, Ldbxyzptlk/db231222/g/an;

    invoke-direct {v0, p0, p1, p2, p3}, Ldbxyzptlk/db231222/g/an;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)V

    .line 1085
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->b:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/an;->a(I)V

    .line 1086
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/an;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1087
    return-void
.end method

.method private d(Ldbxyzptlk/db231222/r/d;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 383
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v0

    .line 384
    sget-object v1, Lcom/dropbox/android/filemanager/aC;->b:Lcom/dropbox/android/filemanager/aC;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v3

    invoke-virtual {v0, v1, p1, v3}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/aC;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/analytics/r;)V

    .line 386
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    .line 387
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v3

    .line 388
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 389
    if-eqz v0, :cond_5

    instance-of v1, v0, Landroid/content/Intent;

    if-eqz v1, :cond_5

    .line 390
    check-cast v0, Landroid/content/Intent;

    .line 391
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    .line 392
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    .line 394
    invoke-static {v0}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Landroid/content/Intent;)Z

    move-result v5

    .line 396
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aS()Lcom/dropbox/android/util/analytics/l;

    move-result-object v6

    const-string v7, "intent.action"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v6

    const-string v7, "caller"

    if-nez v1, :cond_3

    move-object v1, v2

    :goto_0
    invoke-virtual {v6, v7, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v6, "component"

    if-nez v4, :cond_4

    :goto_1
    invoke-virtual {v1, v6, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "allowed"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 409
    if-eqz v5, :cond_0

    .line 410
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->startActivity(Landroid/content/Intent;)V

    .line 423
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->h:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_1

    .line 424
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bX()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/cB;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 427
    invoke-virtual {v3, v9}, Ldbxyzptlk/db231222/n/P;->c(Z)V

    .line 429
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->finish()V

    .line 430
    return-void

    .line 396
    :cond_3
    invoke-virtual {v1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 412
    :cond_5
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/cB;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    invoke-virtual {v3, v9}, Ldbxyzptlk/db231222/n/P;->o(Z)V

    .line 415
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 416
    new-array v0, v9, [Lcom/dropbox/android/activity/fn;

    sget-object v1, Lcom/dropbox/android/activity/fn;->c:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v5

    .line 420
    :goto_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "SamsungDark"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {p0, v3, v0, v1, v2}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;[Lcom/dropbox/android/activity/fn;ZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 418
    :cond_6
    new-array v0, v9, [Lcom/dropbox/android/activity/fn;

    sget-object v1, Lcom/dropbox/android/activity/fn;->a:Lcom/dropbox/android/activity/fn;

    aput-object v1, v0, v5

    goto :goto_3
.end method

.method private j(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 754
    invoke-static {p1}, Lcom/dropbox/android/activity/RecoverAccountFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/RecoverAccountFragment;

    move-result-object v1

    .line 755
    sget-object v2, Lcom/dropbox/android/activity/RecoverAccountFragment;->a:Ljava/lang/String;

    .line 756
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701b1

    :goto_0
    invoke-direct {p0, v1, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    .line 760
    return-void

    .line 756
    :cond_0
    const v0, 0x7f0700ef

    goto :goto_0
.end method

.method private k(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1055
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 1056
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LoginFragment;

    .line 1057
    if-eqz v0, :cond_1

    .line 1060
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1062
    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/LoginFragment;->b(Ljava/lang/String;)V

    .line 1070
    :cond_0
    :goto_0
    return-void

    .line 1067
    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0701b1

    .line 1068
    :goto_1
    invoke-static {p1}, Lcom/dropbox/android/activity/LoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)V

    goto :goto_0

    .line 1067
    :cond_2
    const v0, 0x7f0700ef

    goto :goto_1
.end method

.method private l(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1099
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->n()Lcom/dropbox/android/filemanager/h;

    move-result-object v0

    .line 1100
    if-nez v0, :cond_0

    .line 1106
    :goto_0
    return-void

    .line 1103
    :cond_0
    new-instance v1, Ldbxyzptlk/db231222/g/o;

    invoke-direct {v1, p0, v0, p1}, Ldbxyzptlk/db231222/g/o;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/h;Ljava/lang/String;)V

    .line 1104
    sget-object v0, Lcom/dropbox/android/activity/auth/a;->b:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/g/o;->a(I)V

    .line 1105
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/g/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private m()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const v5, 0x7f0700ef

    .line 605
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 606
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 608
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 611
    sget-object v0, Lcom/dropbox/android/activity/cA;->a:[I

    iget-object v3, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/cB;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 636
    :pswitch_0
    const-string v0, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/LoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;

    move-result-object v1

    .line 637
    sget-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    .line 650
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v3

    if-nez v3, :cond_0

    .line 651
    invoke-static {}, Lcom/dropbox/android/activity/LoginBrandFragment;->b()Lcom/dropbox/android/activity/LoginBrandFragment;

    move-result-object v3

    sget-object v4, Lcom/dropbox/android/activity/LoginBrandFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 653
    :cond_0
    const v3, 0x7f0701b1

    invoke-virtual {v2, v3, v1, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 687
    :goto_1
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 690
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->g()V

    .line 693
    :cond_1
    :pswitch_1
    return-void

    .line 614
    :pswitch_2
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v0

    const-string v3, "mobile-kite-tour"

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 615
    const-string v3, "new-account"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "new-account-no-kite"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 616
    :cond_2
    const-string v0, "EXTRA_FIRST_NAME_PREFILL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "EXTRA_LAST_NAME_PREFILL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/dropbox/android/activity/NewAccountFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/activity/NewAccountFragment;

    move-result-object v1

    .line 619
    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    goto :goto_0

    .line 621
    :cond_3
    const-string v0, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/LoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;

    move-result-object v1

    .line 622
    sget-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    goto :goto_0

    .line 628
    :pswitch_3
    const-string v0, "EXTRA_FIRST_NAME_PREFILL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "EXTRA_LAST_NAME_PREFILL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/dropbox/android/activity/NewAccountFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/activity/NewAccountFragment;

    move-result-object v1

    .line 631
    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 641
    :pswitch_4
    invoke-static {}, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->b()Lcom/dropbox/android/activity/LoginOrNewAcctFragment;

    move-result-object v1

    .line 642
    sget-object v0, Lcom/dropbox/android/activity/LoginOrNewAcctFragment;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 657
    :cond_4
    sget-object v3, Lcom/dropbox/android/activity/cA;->a:[I

    iget-object v4, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    invoke-virtual {v4}, Lcom/dropbox/android/activity/cB;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    move-object v1, v0

    .line 684
    :goto_2
    invoke-virtual {v2, v5, v1, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto/16 :goto_1

    .line 661
    :pswitch_5
    const-string v0, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/LoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;

    move-result-object v1

    .line 662
    sget-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    goto :goto_2

    .line 667
    :pswitch_6
    const-string v0, "EXTRA_FIRST_NAME_PREFILL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "EXTRA_LAST_NAME_PREFILL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/dropbox/android/activity/NewAccountFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/activity/NewAccountFragment;

    move-result-object v1

    .line 670
    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    goto :goto_2

    .line 678
    :pswitch_7
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 682
    :pswitch_8
    invoke-static {}, Lcom/dropbox/android/util/C;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 611
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 657
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_1
    .end packed-switch
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 839
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 960
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 961
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 962
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 964
    :cond_0
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1026
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LoginFragment;

    .line 1027
    if-eqz v0, :cond_0

    .line 1028
    invoke-virtual {v0}, Lcom/dropbox/android/activity/LoginFragment;->b()V

    .line 1030
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 588
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    move p3, p4

    .line 587
    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    invoke-virtual {v0, p1}, Lcom/actionbarsherlock/app/ActionBar;->setTitle(I)V

    .line 171
    :cond_0
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 451
    sparse-switch p1, :sswitch_data_0

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 453
    :sswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Lcom/dropbox/android/activity/cC;)V

    goto :goto_0

    .line 461
    :sswitch_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 462
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->d()V

    goto :goto_0

    .line 463
    :cond_1
    if-nez p2, :cond_2

    .line 464
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e()V

    goto :goto_0

    .line 465
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 468
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->finish()V

    goto :goto_0

    .line 451
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x29 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/dropbox/android/filemanager/h;Ldbxyzptlk/db231222/z/K;Z)V
    .locals 1

    .prologue
    .line 1091
    iget-object v0, p1, Lcom/dropbox/android/filemanager/h;->b:Ldbxyzptlk/db231222/y/m;

    iget-object v0, v0, Ldbxyzptlk/db231222/y/m;->a:Ljava/lang/String;

    invoke-static {v0, p2, p3}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1092
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/k;->a(Lcom/dropbox/android/filemanager/h;)V

    .line 1093
    iget-object v0, p1, Lcom/dropbox/android/filemanager/h;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1094
    const v0, 0x7f0d0031

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    .line 1096
    :cond_0
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/g/D;)V
    .locals 2

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->g:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_0

    .line 1011
    sget-object v0, Lcom/dropbox/android/activity/cA;->b:[I

    invoke-virtual {p1}, Ldbxyzptlk/db231222/g/D;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1022
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->finish()V

    .line 1023
    return-void

    .line 1012
    :pswitch_0
    const v0, 0xd120c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    goto :goto_0

    .line 1013
    :pswitch_1
    const v0, 0xd120d

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    goto :goto_0

    .line 1014
    :pswitch_2
    const v0, 0xd120e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    goto :goto_0

    .line 1011
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 968
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f:Z

    .line 969
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->p()V

    .line 970
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->d(Ldbxyzptlk/db231222/r/d;)V

    .line 971
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 937
    new-instance v0, Ldbxyzptlk/db231222/g/G;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Ldbxyzptlk/db231222/g/G;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 938
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->e:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/G;->a(I)V

    .line 939
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/G;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 940
    return-void
.end method

.method public final a(Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)V
    .locals 1

    .prologue
    .line 980
    if-eqz p3, :cond_0

    .line 981
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->p()V

    .line 982
    const v0, 0x7f0d0032

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    .line 986
    :goto_0
    return-void

    .line 984
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 878
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendlog"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 880
    invoke-static {p0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Landroid/content/Context;)V

    .line 907
    :goto_0
    return-void

    .line 884
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v2, :cond_1

    .line 885
    const v0, 0x7f0d0041

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    goto :goto_0

    .line 889
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v2, :cond_2

    .line 890
    const v0, 0x7f0d0042

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    goto :goto_0

    .line 894
    :cond_2
    invoke-static {p3}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 895
    const v0, 0x7f0d0043

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    goto :goto_0

    .line 899
    :cond_3
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_4

    .line 900
    const v0, 0x7f0d0044

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    goto :goto_0

    .line 904
    :cond_4
    new-instance v0, Lcom/dropbox/android/activity/cC;

    invoke-direct {v0, p3, p4, p1, p2}, Lcom/dropbox/android/activity/cC;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    .line 905
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    invoke-static {v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->a(Lcom/dropbox/android/activity/cC;)Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;

    move-result-object v0

    .line 906
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 852
    invoke-static {}, Lcom/dropbox/android/util/K;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 855
    invoke-static {}, Lcom/dropbox/android/util/K;->b()Ljava/lang/String;

    move-result-object v2

    .line 856
    invoke-static {}, Lcom/dropbox/android/util/K;->c()Ljava/lang/String;

    move-result-object v3

    .line 870
    :goto_0
    new-instance v0, Ldbxyzptlk/db231222/g/r;

    const/4 v4, 0x1

    move-object v1, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/g/r;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 871
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->b:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/r;->a(I)V

    .line 872
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/r;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 873
    :goto_1
    return-void

    .line 858
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendlog"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 860
    invoke-static {p0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Landroid/content/Context;)V

    goto :goto_1

    .line 864
    :cond_1
    invoke-static {p1}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 865
    const v0, 0x7f0d0043

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    goto :goto_1

    :cond_2
    move-object v3, p2

    move-object v2, p1

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 998
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f:Z

    .line 999
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->d(Ldbxyzptlk/db231222/r/d;)V

    .line 1000
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 911
    invoke-static {p1}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 912
    const v0, 0x7f0d0043

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(I)V

    .line 919
    :goto_0
    return-void

    .line 916
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/g/P;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/g/P;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 917
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->c:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/P;->a(I)V

    .line 918
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/P;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 775
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->o()V

    .line 776
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aG()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 778
    invoke-static {}, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b()Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;

    move-result-object v1

    .line 779
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701b1

    :goto_0
    sget-object v2, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    .line 783
    return-void

    .line 779
    :cond_0
    const v0, 0x7f0700ef

    goto :goto_0
.end method

.method public final c(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 1110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f:Z

    .line 1111
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->d(Ldbxyzptlk/db231222/r/d;)V

    .line 1112
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 728
    invoke-static {p1}, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;

    move-result-object v0

    .line 729
    sget-object v1, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->a:Ljava/lang/String;

    .line 730
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/activity/TroubleLoggingInDialogFrag;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 731
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/LoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;

    move-result-object v0

    .line 699
    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    .line 700
    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 701
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 745
    invoke-static {p1}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/ForgotPasswordFragment;

    move-result-object v1

    .line 746
    sget-object v2, Lcom/dropbox/android/activity/ForgotPasswordFragment;->a:Ljava/lang/String;

    .line 747
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701b1

    :goto_0
    invoke-direct {p0, v1, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    .line 751
    return-void

    .line 747
    :cond_0
    const v0, 0x7f0700ef

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 707
    const-string v1, "EXTRA_FIRST_NAME_PREFILL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EXTRA_LAST_NAME_PREFILL"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "EXTRA_EMAIL_PREFILL"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/dropbox/android/activity/NewAccountFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/activity/NewAccountFragment;

    move-result-object v0

    .line 710
    sget-object v1, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    .line 711
    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 712
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 735
    invoke-static {p1}, Lcom/dropbox/android/activity/SsoLoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/SsoLoginFragment;

    move-result-object v1

    .line 736
    sget-object v2, Lcom/dropbox/android/activity/SsoLoginFragment;->a:Ljava/lang/String;

    .line 737
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701b1

    :goto_0
    invoke-direct {p0, v1, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    .line 741
    return-void

    .line 737
    :cond_0
    const v0, 0x7f0700ef

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 923
    new-instance v0, Ldbxyzptlk/db231222/g/P;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/g/P;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 924
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->c:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/P;->a(I)V

    .line 925
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/P;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 926
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 598
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 599
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->e:Lcom/dropbox/android/activity/cB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->f:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f_()V
    .locals 2

    .prologue
    .line 930
    new-instance v0, Ldbxyzptlk/db231222/g/ad;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/g/ad;-><init>(Landroid/content/Context;)V

    .line 931
    sget-object v1, Lcom/dropbox/android/activity/auth/a;->d:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/ad;->a(I)V

    .line 932
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/ad;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 933
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 435
    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 436
    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    .line 444
    const/4 v2, 0x4

    iget-boolean v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "success"

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    .line 446
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->finish()V

    .line 447
    return-void

    .line 444
    :cond_1
    const-string v1, "canceled"

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 763
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aF()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 765
    invoke-static {}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b()Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;

    move-result-object v1

    .line 766
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701b1

    :goto_0
    sget-object v2, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->a:Ljava/lang/String;

    const-string v3, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V

    .line 771
    return-void

    .line 766
    :cond_0
    const v0, 0x7f0700ef

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 975
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->j(Ljava/lang/String;)V

    .line 976
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 791
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    .line 792
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->finish()V

    .line 793
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1043
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->k(Ljava/lang/String;)V

    .line 1044
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 994
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1048
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->k(Ljava/lang/String;)V

    .line 1050
    const v0, 0x7f0d0227

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    .line 1052
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 1037
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->p()V

    .line 1038
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->g()V

    .line 1039
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 1075
    .line 1076
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1078
    const v0, 0x7f0d0239

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;I)V

    .line 1080
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    .line 353
    if-lez v1, :cond_1

    .line 354
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryAt(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v0

    .line 362
    if-eqz v0, :cond_0

    const-string v1, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    invoke-interface {v0}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->g()V

    .line 373
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onBackPressed()V

    .line 374
    return-void

    .line 367
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    .line 368
    sget-object v0, Lcom/dropbox/android/activity/cB;->h:Lcom/dropbox/android/activity/cB;

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_0

    .line 369
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bW()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/cB;->a:Lcom/dropbox/android/activity/cB;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/cB;->a(Ljava/lang/String;Lcom/dropbox/android/activity/cB;)Lcom/dropbox/android/activity/cB;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    .line 253
    invoke-static {}, Lcom/dropbox/android/filemanager/aq;->a()Lcom/dropbox/android/filemanager/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/aq;->c()[Landroid/graphics/Bitmap;

    move-result-object v0

    .line 254
    if-nez v0, :cond_0

    .line 256
    invoke-static {}, Lcom/dropbox/android/util/aC;->a()Lcom/dropbox/android/util/aC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/aC;->b()V

    .line 261
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/dropbox/android/filemanager/aq;->a(Lcom/dropbox/android/filemanager/at;)V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 266
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    .line 267
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "SamsungDark"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 268
    if-eqz v0, :cond_1

    .line 269
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Dark theme not supported on tablets."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_1
    const v1, 0x7f0c00da

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setTheme(I)V

    .line 276
    :goto_0
    if-eqz v0, :cond_3

    .line 278
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->e:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_8

    .line 279
    const v0, 0x7f0d02c9

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setTitle(I)V

    .line 284
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->hide()V

    .line 293
    :cond_3
    :goto_2
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 295
    if-eqz p1, :cond_4

    .line 296
    const-string v0, "SIS_KEY_PENDING_NEW_ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 297
    const-string v0, "SIS_KEY_PENDING_NEW_ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/cC;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    .line 303
    :cond_4
    const v0, 0x7f030057

    .line 304
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 305
    const v0, 0x7f0300af

    .line 307
    :cond_5
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setContentView(I)V

    .line 309
    if-nez p1, :cond_6

    .line 310
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->m()V

    .line 313
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/dropbox/android/util/Activities;->a(Lcom/dropbox/android/activity/cB;Landroid/app/Activity;Lcom/dropbox/android/util/analytics/r;)V

    .line 314
    return-void

    .line 273
    :cond_7
    const v1, 0x7f0c00d8

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setTheme(I)V

    goto :goto_0

    .line 280
    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->f:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_2

    .line 281
    const v0, 0x7f0d02c7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setTitle(I)V

    goto :goto_1

    .line 287
    :cond_9
    const v0, 0x7f0c00d6

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setTheme(I)V

    .line 288
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->hide()V

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 845
    invoke-static {p1}, Lcom/dropbox/android/activity/auth/a;->a(I)Lcom/dropbox/android/activity/auth/a;

    move-result-object v0

    .line 846
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/auth/a;->a(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 326
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onResume()V

    .line 329
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->d(Ldbxyzptlk/db231222/r/d;)V

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 333
    sget-object v0, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    .line 334
    const/4 v1, 0x0

    sput-object v1, Lcom/dropbox/android/activity/SsoCallbackReceiver;->a:Ljava/lang/String;

    .line 335
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->l(Ljava/lang/String;)V

    goto :goto_0

    .line 336
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->d:Lcom/dropbox/android/activity/cB;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v1, Lcom/dropbox/android/activity/cB;->g:Lcom/dropbox/android/activity/cB;

    if-ne v0, v1, :cond_0

    .line 337
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 338
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/cB;

    sget-object v2, Lcom/dropbox/android/activity/cB;->d:Lcom/dropbox/android/activity/cB;

    if-ne v0, v2, :cond_4

    sget-object v0, Ldbxyzptlk/db231222/g/F;->a:Ldbxyzptlk/db231222/g/F;

    .line 340
    :goto_1
    if-eqz v1, :cond_0

    .line 341
    new-instance v2, Ldbxyzptlk/db231222/g/B;

    invoke-direct {v2, p0, v1, v0}, Ldbxyzptlk/db231222/g/B;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/g/F;)V

    .line 342
    sget-object v0, Lcom/dropbox/android/activity/auth/a;->b:Lcom/dropbox/android/activity/auth/a;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/auth/a;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/g/B;->a(I)V

    .line 343
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/g/B;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 338
    :cond_4
    sget-object v0, Ldbxyzptlk/db231222/g/F;->b:Ldbxyzptlk/db231222/g/F;

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 318
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 319
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    if-eqz v0, :cond_0

    .line 320
    const-string v0, "SIS_KEY_PENDING_NEW_ACCOUNT"

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e:Lcom/dropbox/android/activity/cC;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 322
    :cond_0
    return-void
.end method
