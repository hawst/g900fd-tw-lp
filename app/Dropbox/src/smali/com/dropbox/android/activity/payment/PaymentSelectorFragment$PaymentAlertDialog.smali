.class public Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;
    .locals 1

    .prologue
    .line 464
    invoke-static {p0, p1, p2, p3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;
    .locals 3

    .prologue
    .line 473
    new-instance v0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;-><init>()V

    .line 474
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 475
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 476
    const-string v2, "title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const-string v2, "message"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v2, "leave_on_DISMISS"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 479
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->setArguments(Landroid/os/Bundle;)V

    .line 480
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 486
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 487
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "leave_on_DISMISS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 489
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 490
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 491
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 492
    const v0, 0x7f0d0014

    new-instance v1, Lcom/dropbox/android/activity/payment/x;

    invoke-direct {v1, p0, v2}, Lcom/dropbox/android/activity/payment/x;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorFragment$PaymentAlertDialog;Z)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 501
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 502
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
