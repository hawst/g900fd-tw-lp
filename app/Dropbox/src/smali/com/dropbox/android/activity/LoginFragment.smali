.class public Lcom/dropbox/android/activity/LoginFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/cy;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private c:Lcom/dropbox/android/widget/EmailTextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/CheckBox;

.field private g:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/LoginFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 72
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginFragment;->setArguments(Landroid/os/Bundle;)V

    .line 73
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lcom/dropbox/android/activity/LoginFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginFragment;-><init>()V

    .line 66
    invoke-virtual {v0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL_PREFILL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginFragment;Z)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    .line 116
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0701a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 121
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getBottom()I

    move-result v1

    .line 123
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    .line 124
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v3

    .line 126
    add-int v4, v2, v3

    if-le v1, v4, :cond_0

    .line 127
    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 128
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 131
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LoginFragment;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginFragment;->c()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/LoginFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 303
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/cy;

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/EmailTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/dropbox/android/activity/cy;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 304
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/LoginFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/LoginFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/LoginFragment;)Lcom/dropbox/android/widget/EmailTextView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/activity/LoginFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/activity/LoginFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/cy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    const-class v0, Lcom/dropbox/android/activity/cy;

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CLEAR_PASSWORD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/EmailTextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 136
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/cy;

    invoke-interface {v0}, Lcom/dropbox/android/activity/cy;->f()Z

    move-result v1

    .line 137
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/cy;

    const v2, 0x7f030059

    const v3, 0x7f0300aa

    invoke-interface {v0, p1, p2, v2, v3}, Lcom/dropbox/android/activity/cy;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v2

    .line 142
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 144
    const v0, 0x7f0700f5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/EmailTextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    .line 145
    const v0, 0x7f0700f6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/TextView;

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    iget-object v4, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/TextView;

    const-string v5, "login"

    invoke-virtual {v0, v4, v5}, Lcom/dropbox/android/widget/EmailTextView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 149
    if-nez p3, :cond_0

    .line 150
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/EmailTextView;->requestFocus()Z

    .line 153
    :cond_0
    if-eqz v3, :cond_1

    .line 154
    const-string v0, "ARG_EMAIL_PREFILL"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_1

    .line 156
    iget-object v3, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v3, v0}, Lcom/dropbox/android/widget/EmailTextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/dropbox/android/activity/co;

    invoke-direct {v4, p0, v0, v1}, Lcom/dropbox/android/activity/co;-><init>(Lcom/dropbox/android/activity/LoginFragment;Landroid/view/View;Z)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 180
    const v0, 0x7f0700f7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/widget/EditText;)V

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/dropbox/android/activity/cq;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/cq;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/dropbox/android/activity/cr;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/cr;-><init>(Lcom/dropbox/android/activity/LoginFragment;Z)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/dropbox/android/activity/cs;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/cs;-><init>(Lcom/dropbox/android/activity/LoginFragment;Z)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 216
    const v0, 0x7f0701aa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->f:Landroid/widget/CheckBox;

    .line 217
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->f:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 219
    iget-object v3, p0, Lcom/dropbox/android/activity/LoginFragment;->f:Landroid/widget/CheckBox;

    new-instance v4, Lcom/dropbox/android/activity/ct;

    invoke-direct {v4, p0, v0}, Lcom/dropbox/android/activity/ct;-><init>(Lcom/dropbox/android/activity/LoginFragment;Landroid/graphics/Typeface;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 246
    :cond_2
    const v0, 0x7f0700f8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->g:Landroid/view/View;

    .line 247
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->g:Landroid/view/View;

    if-nez v0, :cond_3

    .line 248
    const v0, 0x7f0701a6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->g:Landroid/view/View;

    .line 250
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->g:Landroid/view/View;

    new-instance v3, Lcom/dropbox/android/activity/cu;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/cu;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    const v0, 0x7f0700f9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 258
    if-eqz v0, :cond_4

    .line 259
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 260
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    :cond_4
    :goto_0
    if-nez v1, :cond_6

    .line 275
    const v0, 0x7f0700fa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 276
    new-instance v1, Lcom/dropbox/android/activity/cw;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cw;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    :goto_1
    return-object v2

    .line 262
    :cond_5
    new-instance v3, Lcom/dropbox/android/activity/cv;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/cv;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 287
    :cond_6
    const v0, 0x7f0701ab

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 288
    new-instance v1, Lcom/dropbox/android/activity/cx;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cx;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onResume()V

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/cy;

    const v1, 0x7f0d0025

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/cy;->a(I)V

    .line 97
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 99
    const-string v1, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const-string v1, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    const-string v2, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 102
    iget-object v2, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Lcom/dropbox/android/widget/EmailTextView;

    invoke-virtual {v2, v1}, Lcom/dropbox/android/widget/EmailTextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_0
    const-string v1, "ARG_CLEAR_PASSWORD"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 107
    const-string v1, "ARG_CLEAR_PASSWORD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 109
    :cond_1
    return-void
.end method
