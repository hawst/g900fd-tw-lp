.class public Lcom/dropbox/android/activity/AlbumViewActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/albums/Album;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/dropbox/android/activity/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 80
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/albums/Album;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/AlbumViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 40
    const-string v1, "EXTRA_ALBUM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 41
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/AlbumViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    const-string v1, "EXTRA_ALBUM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 50
    const-string v1, "EXTRA_PATHS_ADDED"

    invoke-static {p2}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 51
    const-string v1, "EXTRA_PATH_TO_SCROLL_TO"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 53
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/AlbumViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    const-string v1, "EXTRA_NEW_ALBUM"

    invoke-static {p1}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 59
    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_ALBUM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_NEW_ALBUM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->b:Ljava/util/ArrayList;

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->b:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    :goto_1
    if-ne v0, v1, :cond_2

    .line 68
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Need to have an album or a list of paths."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 67
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 71
    :cond_2
    return-void
.end method


# virtual methods
.method final a(Lcom/dropbox/android/activity/a;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->e:Lcom/dropbox/android/activity/a;

    .line 90
    return-void
.end method

.method final a(Lcom/dropbox/android/albums/Album;)V
    .locals 1

    .prologue
    .line 34
    iput-object p1, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->b:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->e()V

    .line 129
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    .line 132
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->e:Lcom/dropbox/android/activity/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->e:Lcom/dropbox/android/activity/a;

    invoke-interface {v0}, Lcom/dropbox/android/activity/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 78
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->finish()V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewActivity;->setContentView(I)V

    .line 102
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setIcon(I)V

    .line 103
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 105
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 107
    if-nez p1, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->e()V

    .line 112
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EXTRA_PATHS_ADDED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EXTRA_PATHS_ADDED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 114
    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "EXTRA_PATH_TO_SCROLL_TO"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-static {v2, v1, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/albums/Album;Ljava/util/ArrayList;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/AlbumViewFragment;

    move-result-object v0

    .line 120
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 121
    const v2, 0x7f0700b2

    const-string v3, "ALBUM_VIEW_FRAG_TAG"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 122
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->a:Lcom/dropbox/android/albums/Album;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewActivity;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/albums/Album;Ljava/util/ArrayList;)Lcom/dropbox/android/activity/AlbumViewFragment;

    move-result-object v0

    goto :goto_1
.end method
