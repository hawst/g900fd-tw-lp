.class public Lcom/dropbox/android/activity/dialog/UploadDialog;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/dialog/UploadDialog;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/dropbox/android/activity/dialog/UploadDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/UploadDialog;-><init>()V

    .line 24
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UploadDialog;->setArguments(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_FOLDER_PATH"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 26
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 31
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_FOLDER_PATH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 33
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 34
    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0d00d9

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0d00da

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 38
    new-array v3, v5, [Landroid/content/Intent;

    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-class v6, Lcom/dropbox/android/activity/GalleryPickerActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "UPLOAD_PATH"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v7

    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-class v6, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "BROWSE_MODE"

    sget-object v6, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    invoke-virtual {v6}, Lcom/dropbox/android/activity/ce;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "UPLOAD_PATH"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v3, v8

    .line 45
    new-instance v0, Lcom/dropbox/android/activity/dialog/K;

    invoke-direct {v0, p0, v3}, Lcom/dropbox/android/activity/dialog/K;-><init>(Lcom/dropbox/android/activity/dialog/UploadDialog;[Landroid/content/Intent;)V

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
