.class public Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;
.super Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag",
        "<",
        "Lcom/dropbox/android/activity/AlbumViewFragment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1173
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SimpleConfirmDialogFrag;-><init>()V

    return-void
.end method

.method static a(Lcom/dropbox/android/activity/AlbumViewFragment;Z)Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;
    .locals 3

    .prologue
    .line 1177
    new-instance v1, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;

    invoke-direct {v1}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;-><init>()V

    .line 1178
    if-eqz p1, :cond_0

    const v0, 0x7f0d02a0

    :goto_0
    const v2, 0x7f0d02a1

    invoke-virtual {v1, p0, v0, v2}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;->a(Landroid/support/v4/app/Fragment;II)V

    .line 1182
    invoke-virtual {v1}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "KEY_IsLightweight"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1183
    return-object v1

    .line 1178
    :cond_0
    const v0, 0x7f0d029f

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 1173
    check-cast p1, Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1188
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_IsLightweight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1189
    invoke-static {p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->s(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/o;

    move-result-object v0

    const v1, 0x7f0d027e

    invoke-virtual {v0, v2, v1, v2}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;ILandroid/os/Parcelable;)V

    .line 1193
    :goto_0
    return-void

    .line 1191
    :cond_0
    invoke-static {p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->t(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/o;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    goto :goto_0
.end method
