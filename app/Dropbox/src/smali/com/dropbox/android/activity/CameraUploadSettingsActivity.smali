.class public Lcom/dropbox/android/activity/CameraUploadSettingsActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/CheckBox;

.field private g:Landroid/widget/RadioButton;

.field private h:Landroid/widget/CompoundButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h:Landroid/widget/CompoundButton;

    return-void
.end method

.method private static a(Ldbxyzptlk/db231222/n/P;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    const-string v1, "ENABLED"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    const-string v1, "USE_3G"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v1, "UPLOAD_VIDEOS"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CompoundButton;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h:Landroid/widget/CompoundButton;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v10, 0x4

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 67
    const-string v1, "FULL_SCREEN"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->finish()V

    .line 214
    :goto_0
    return-void

    .line 76
    :cond_0
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->setContentView(I)V

    .line 78
    const v0, 0x7f07004e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h:Landroid/widget/CompoundButton;

    .line 81
    const v0, 0x7f07004d

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b:Landroid/widget/Button;

    .line 82
    const v0, 0x7f07004c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->e:Landroid/widget/Button;

    .line 83
    const v0, 0x7f070070

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->e:Landroid/widget/Button;

    const v1, 0x7f0d0013

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b:Landroid/widget/Button;

    const v1, 0x7f0d01f5

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v3

    .line 90
    const v0, 0x7f07006b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 91
    const v1, 0x7f07006d

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    .line 92
    const v1, 0x7f07006a

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 93
    const v1, 0x7f07006c

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 94
    const v1, 0x7f07006e

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 96
    const v1, 0x7f070069

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 101
    if-nez p1, :cond_1

    .line 102
    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a(Ldbxyzptlk/db231222/n/P;)Landroid/os/Bundle;

    move-result-object p1

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h:Landroid/widget/CompoundButton;

    const-string v8, "ENABLED"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v1, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 107
    const-string v1, "USE_3G"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    invoke-virtual {v1, v12}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 109
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 110
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 117
    :goto_1
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    const-string v8, "UPLOAD_VIDEOS"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    const/4 v1, 0x5

    new-array v8, v1, [Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    aput-object v1, v8, v2

    aput-object v0, v8, v12

    const/4 v1, 0x2

    iget-object v9, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    aput-object v9, v8, v1

    const/4 v1, 0x3

    aput-object v4, v8, v1

    aput-object v5, v8, v10

    .line 122
    const-string v1, "ENABLED"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 123
    array-length v10, v8

    move v1, v2

    :goto_2
    if-ge v1, v10, :cond_3

    aget-object v11, v8, v1

    .line 124
    invoke-virtual {v11, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 112
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 113
    invoke-virtual {v0, v12}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 114
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 126
    :cond_3
    const v1, 0x7f070068

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v9, :cond_5

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_3
    invoke-virtual {v10, v1}, Landroid/view/View;->setAlpha(F)V

    .line 129
    invoke-static {}, Lcom/dropbox/android/util/S;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 130
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    const v1, 0x7f07006f

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 133
    iput v12, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 134
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    :cond_4
    new-instance v1, Lcom/dropbox/android/activity/R;

    invoke-direct {v1, p0, v0, v6}, Lcom/dropbox/android/activity/R;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Landroid/widget/RadioButton;Landroid/view/View;)V

    .line 149
    new-instance v2, Lcom/dropbox/android/activity/S;

    invoke-direct {v2, p0, v0, v6}, Lcom/dropbox/android/activity/S;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Landroid/widget/RadioButton;Landroid/view/View;)V

    .line 158
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {v4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h:Landroid/widget/CompoundButton;

    new-instance v1, Lcom/dropbox/android/activity/T;

    invoke-direct {v1, p0, v8}, Lcom/dropbox/android/activity/T;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;[Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/U;

    invoke-direct {v1, p0, v3}, Lcom/dropbox/android/activity/U;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Ldbxyzptlk/db231222/n/P;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->e:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/V;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/V;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 126
    :cond_5
    const/high16 v1, 0x3f000000    # 0.5f

    goto :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 219
    const-string v0, "ENABLED"

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->h:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 220
    const-string v0, "USE_3G"

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    const-string v0, "UPLOAD_VIDEOS"

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 222
    return-void
.end method
