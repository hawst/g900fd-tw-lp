.class final Lcom/dropbox/android/activity/aR;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Lcom/dropbox/android/albums/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/dropbox/android/activity/aR;->b:Lcom/dropbox/android/activity/GalleryActivity;

    iput-object p6, p0, Lcom/dropbox/android/activity/aR;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/FragmentActivity;I)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/albums/h;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/dropbox/android/activity/aR;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p2, Lcom/dropbox/android/albums/h;->a:Lcom/dropbox/android/albums/Album;

    iget-object v2, p2, Lcom/dropbox/android/albums/h;->b:Ljava/util/Collection;

    invoke-virtual {v0, v1, v2, p1}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    check-cast p2, Lcom/dropbox/android/albums/h;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/aR;->a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/albums/h;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 185
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d028c

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->a(I)V

    .line 186
    return-void
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 176
    check-cast p2, Lcom/dropbox/android/util/DropboxPath;

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 178
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v1, p0, Lcom/dropbox/android/activity/aR;->b:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/aR;->b:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v2, p1, v0, p2}, Lcom/dropbox/android/activity/AlbumViewActivity;->a(Landroid/content/Context;Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    .line 181
    return-void
.end method
