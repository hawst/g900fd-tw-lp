.class final Lcom/dropbox/android/activity/eJ;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/SendToFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/SendToFragment;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/dropbox/android/activity/eJ;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 128
    iget-object v0, p0, Lcom/dropbox/android/activity/eJ;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 134
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 136
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_0

    .line 139
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/activity/eJ;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 167
    if-eqz v1, :cond_1

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/activity/eJ;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/SendToFragment;->k()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    .line 170
    check-cast v1, Lcom/dropbox/android/activity/UploadBaseActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/activity/UploadBaseActivity;->a(Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V

    .line 174
    :cond_1
    :goto_1
    return-void

    .line 141
    :cond_2
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 142
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 143
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 144
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 145
    if-eqz v0, :cond_0

    .line 146
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    :cond_3
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/activity/eJ;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a(Lcom/dropbox/android/activity/SendToFragment;)Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/eJ;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/SendToFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_1

    .line 152
    :cond_4
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 154
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 157
    instance-of v3, v0, Landroid/net/Uri;

    if-eqz v3, :cond_5

    .line 158
    check-cast v0, Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
