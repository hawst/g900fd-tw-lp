.class public final Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ldbxyzptlk/db231222/g/i;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    .line 32
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/g/i;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/db231222/g/i;

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->setRetainInstance(Z)V

    .line 24
    return-void
.end method

.method public static a(Ldbxyzptlk/db231222/g/i;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;-><init>(Ldbxyzptlk/db231222/g/i;)V

    return-object v0
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/db231222/g/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/i;->c()V

    .line 64
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/db231222/g/i;

    if-nez v0, :cond_0

    .line 42
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->setShowsDialog(Z)V

    .line 43
    const/4 v0, 0x0

    .line 47
    :goto_0
    return-object v0

    .line 45
    :cond_0
    new-instance v0, Lcom/dropbox/android/widget/ab;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/db231222/g/i;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/g/i;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/ab;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 46
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/db231222/g/i;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/g/i;->a(Lcom/dropbox/android/widget/ab;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 57
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onDestroyView()V

    .line 58
    return-void
.end method
