.class public Lcom/dropbox/android/activity/QrAuthFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/qr/k;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/activity/et;

.field private c:Lcom/dropbox/android/widget/qr/QrReaderView;

.field private d:Lcom/dropbox/android/util/analytics/w;

.field private e:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ldbxyzptlk/db231222/l/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/l",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/dropbox/android/activity/QrAuthFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/QrAuthFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    .line 364
    new-instance v0, Lcom/dropbox/android/activity/eq;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/eq;-><init>(Lcom/dropbox/android/activity/QrAuthFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->f:Ldbxyzptlk/db231222/l/l;

    .line 420
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/er;)Lcom/dropbox/android/activity/QrAuthFragment;
    .locals 4

    .prologue
    .line 140
    new-instance v0, Lcom/dropbox/android/activity/QrAuthFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/QrAuthFragment;-><init>()V

    .line 141
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 142
    const-string v2, "ARG_PAGE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/er;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/QrAuthFragment;->setArguments(Landroid/os/Bundle;)V

    .line 145
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/QrAuthFragment;)Lcom/dropbox/android/activity/et;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->b:Lcom/dropbox/android/activity/et;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/dropbox/android/activity/QrAuthFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/widget/LinearLayout;I)V
    .locals 8

    .prologue
    const v7, 0x7f08004d

    const/4 v3, 0x1

    const v6, 0x7f080081

    .line 337
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move v2, v3

    .line 338
    :goto_0
    const/4 v0, 0x3

    if-gt v2, v0, :cond_2

    .line 339
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 340
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 341
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 342
    if-ge v2, p2, :cond_0

    .line 344
    const v5, 0x7f02020d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 346
    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 347
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 338
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 348
    :cond_0
    if-ne v2, p2, :cond_1

    .line 350
    const v5, 0x7f02020c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 352
    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 353
    const v1, 0x7f080080

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 356
    :cond_1
    const v5, 0x7f02020b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 358
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 359
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 362
    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/QrAuthFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/QrAuthFragment;)Lcom/dropbox/android/widget/qr/QrReaderView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 408
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cm()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->d:Lcom/dropbox/android/util/analytics/w;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 409
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->a()V

    .line 410
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 411
    iget-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->e:Ldbxyzptlk/db231222/l/k;

    new-instance v2, Lcom/dropbox/android/activity/es;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/dropbox/android/activity/es;-><init>(Ljava/lang/String;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/a;)V

    .line 412
    invoke-static {v0}, Lcom/dropbox/android/activity/QrAuthActivity;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 413
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onAttach(Landroid/app/Activity;)V

    .line 154
    const-class v0, Lcom/dropbox/android/activity/et;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 155
    check-cast p1, Lcom/dropbox/android/activity/et;

    iput-object p1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->b:Lcom/dropbox/android/activity/et;

    .line 156
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 188
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->C()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->e:Ldbxyzptlk/db231222/l/k;

    .line 192
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x1

    .line 199
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_0

    const-string v0, "ARG_PAGE"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "QrAuthFragment expects an arg with the page\'s index in the auth flow."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_1
    const v0, 0x7f030078

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 213
    invoke-static {}, Lcom/dropbox/android/activity/er;->values()[Lcom/dropbox/android/activity/er;

    move-result-object v2

    const-string v3, "ARG_PAGE"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v1, v2, v1

    .line 215
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/G;->c()Lcom/dropbox/android/service/J;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/J;->a()Z

    move-result v2

    if-nez v2, :cond_a

    .line 216
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cl()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "page"

    invoke-virtual {v1}, Lcom/dropbox/android/activity/er;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 217
    sget-object v1, Lcom/dropbox/android/activity/er;->e:Lcom/dropbox/android/activity/er;

    move-object v3, v1

    .line 224
    :goto_0
    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->c()I

    move-result v1

    if-eq v1, v5, :cond_2

    .line 225
    const v1, 0x7f030077

    invoke-virtual {p1, v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 226
    const v1, 0x7f030074

    invoke-virtual {p1, v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 227
    const v2, 0x7f070153

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->c()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 235
    :cond_2
    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->d()I

    move-result v1

    if-eq v1, v5, :cond_3

    .line 236
    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->d()I

    move-result v1

    invoke-virtual {p1, v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 241
    :cond_3
    sget-object v1, Lcom/dropbox/android/activity/er;->a:Lcom/dropbox/android/activity/er;

    if-ne v3, v1, :cond_b

    .line 242
    const v1, 0x7f03007c

    invoke-virtual {p1, v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 261
    :cond_4
    :goto_1
    sget-object v1, Lcom/dropbox/android/activity/er;->b:Lcom/dropbox/android/activity/er;

    if-eq v3, v1, :cond_5

    sget-object v1, Lcom/dropbox/android/activity/er;->c:Lcom/dropbox/android/activity/er;

    if-eq v3, v1, :cond_5

    sget-object v1, Lcom/dropbox/android/activity/er;->d:Lcom/dropbox/android/activity/er;

    if-ne v3, v1, :cond_6

    .line 262
    :cond_5
    const v1, 0x7f030075

    invoke-virtual {p1, v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 263
    const v2, 0x7f070154

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->ordinal()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/QrAuthFragment;->a(Landroid/widget/LinearLayout;I)V

    .line 268
    :cond_6
    const v1, 0x7f070159

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 269
    if-eqz v2, :cond_7

    move-object v1, v2

    .line 270
    check-cast v1, Landroid/widget/Button;

    new-instance v4, Lcom/dropbox/android/activity/en;

    invoke-direct {v4, p0, v3}, Lcom/dropbox/android/activity/en;-><init>(Lcom/dropbox/android/activity/QrAuthFragment;Lcom/dropbox/android/activity/er;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    sget-object v1, Lcom/dropbox/android/activity/er;->b:Lcom/dropbox/android/activity/er;

    if-eq v3, v1, :cond_c

    .line 283
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 307
    :cond_7
    :goto_2
    const v1, 0x7f070158

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 308
    if-eqz v1, :cond_8

    .line 309
    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/ep;

    invoke-direct {v2, p0, v3}, Lcom/dropbox/android/activity/ep;-><init>(Lcom/dropbox/android/activity/QrAuthFragment;Lcom/dropbox/android/activity/er;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    :cond_8
    sget-object v1, Lcom/dropbox/android/activity/er;->c:Lcom/dropbox/android/activity/er;

    if-ne v3, v1, :cond_9

    .line 324
    const v1, 0x7f07015a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/qr/QrReaderView;

    iput-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    .line 325
    iget-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    invoke-virtual {v1, p0}, Lcom/dropbox/android/widget/qr/QrReaderView;->setCallback(Lcom/dropbox/android/widget/qr/k;)V

    .line 326
    iget-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    const-string v2, "DBCONNECT;"

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/qr/QrReaderView;->setQrPrefix(Ljava/lang/String;)V

    .line 329
    :cond_9
    return-object v0

    .line 219
    :cond_a
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ck()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "page"

    invoke-virtual {v1}, Lcom/dropbox/android/activity/er;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    move-object v3, v1

    .line 220
    goto/16 :goto_0

    .line 244
    :cond_b
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 245
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v3}, Lcom/dropbox/android/activity/er;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 249
    const v1, 0x7f030076

    invoke-virtual {p1, v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 251
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 253
    const v2, 0x7f070156

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    const v2, 0x7f070157

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    sget-object v2, Lcom/dropbox/android/activity/er;->b:Lcom/dropbox/android/activity/er;

    if-ne v3, v2, :cond_4

    .line 257
    const v2, 0x7f03007f

    invoke-virtual {p1, v2, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto/16 :goto_1

    .line 285
    :cond_c
    invoke-virtual {p0}, Lcom/dropbox/android/activity/QrAuthFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/high16 v4, 0x7f040000

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 286
    new-instance v4, Lcom/dropbox/android/activity/eo;

    invoke-direct {v4, p0, v2}, Lcom/dropbox/android/activity/eo;-><init>(Lcom/dropbox/android/activity/QrAuthFragment;Landroid/view/View;)V

    invoke-virtual {v1, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 302
    invoke-virtual {v2, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_2
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onDetach()V

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->b:Lcom/dropbox/android/activity/et;

    .line 162
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->a()V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->e:Ldbxyzptlk/db231222/l/k;

    iget-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->f:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->b(Ldbxyzptlk/db231222/l/l;)V

    .line 180
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onPause()V

    .line 181
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->onResume()V

    .line 167
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    if-eqz v0, :cond_0

    .line 168
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->d:Lcom/dropbox/android/util/analytics/w;

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->c:Lcom/dropbox/android/widget/qr/QrReaderView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/qr/QrReaderView;->b()V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/QrAuthFragment;->e:Ldbxyzptlk/db231222/l/k;

    iget-object v1, p0, Lcom/dropbox/android/activity/QrAuthFragment;->f:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/l;)V

    .line 172
    return-void
.end method
