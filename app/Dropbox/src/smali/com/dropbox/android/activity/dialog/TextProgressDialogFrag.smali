.class public Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;-><init>()V

    .line 19
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 20
    const-string v2, "message_res"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 21
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 22
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->setCancelable(Z)V

    .line 24
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;-><init>()V

    .line 29
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 30
    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 32
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->setCancelable(Z)V

    .line 34
    return-object v0
.end method

.method public static b(Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 51
    const-class v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 54
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 55
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 39
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 47
    :goto_0
    return-object v0

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message_res"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
