.class public Lcom/dropbox/android/activity/LoginBrandFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/cn;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/LoginBrandFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LoginBrandFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginBrandFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginBrandFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginBrandFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public static b()Lcom/dropbox/android/activity/LoginBrandFragment;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/dropbox/android/activity/LoginBrandFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginBrandFragment;-><init>()V

    .line 46
    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LoginBrandFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginBrandFragment;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/cn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    const-class v0, Lcom/dropbox/android/activity/cn;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x8

    .line 57
    invoke-super {p0, p3}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onCreate(Landroid/os/Bundle;)V

    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginBrandFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/cn;

    invoke-interface {v0}, Lcom/dropbox/android/activity/cn;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const v0, 0x7f0300a3

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 61
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 62
    const v0, 0x7f0700f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f02025c

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object v0, v1

    .line 113
    :goto_0
    return-object v0

    .line 65
    :cond_0
    const v0, 0x7f030058

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 66
    const v0, 0x7f0700f1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 67
    const v1, 0x7f0700f2

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 68
    const v1, 0x7f0700f3

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 69
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 70
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 71
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 96
    :goto_1
    const v0, 0x7f0700f4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 108
    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_2

    .line 109
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 73
    :cond_1
    new-instance v3, Lcom/dropbox/android/activity/cl;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/cl;-><init>(Lcom/dropbox/android/activity/LoginBrandFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    new-instance v0, Lcom/dropbox/android/activity/cm;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/cm;-><init>(Lcom/dropbox/android/activity/LoginBrandFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 113
    goto :goto_0
.end method
