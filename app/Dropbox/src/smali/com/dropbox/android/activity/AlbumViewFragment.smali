.class public final Lcom/dropbox/android/activity/AlbumViewFragment;
.super Lcom/dropbox/android/activity/UserSweetListFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/UserSweetListFragment",
        "<",
        "Lcom/dropbox/android/widget/f;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Lcom/dropbox/android/util/as;

.field private final B:Ldbxyzptlk/db231222/g/U;

.field private final C:Lcom/dropbox/android/activity/a;

.field private final D:Lcom/dropbox/android/albums/q;

.field private final E:Landroid/view/View$OnClickListener;

.field private F:Ldbxyzptlk/db231222/j/k;

.field private G:Ldbxyzptlk/db231222/j/l;

.field private H:Landroid/widget/TextView;

.field private final I:Lcom/actionbarsherlock/view/ActionMode$Callback;

.field private final J:Landroid/os/Handler;

.field private b:Lcom/dropbox/android/albums/Album;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private final e:Lcom/dropbox/android/activity/A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/activity/A",
            "<",
            "Lcom/dropbox/android/albums/AlbumItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/actionbarsherlock/view/ActionMode;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/ProgressBar;

.field private s:Ldbxyzptlk/db231222/g/W;

.field private t:Z

.field private u:Lcom/dropbox/android/activity/z;

.field private v:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Lcom/dropbox/android/albums/g;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcom/dropbox/android/albums/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/albums/o",
            "<",
            "Lcom/dropbox/android/activity/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/AlbumViewFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;-><init>()V

    .line 88
    iput-boolean v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    .line 91
    new-instance v0, Lcom/dropbox/android/activity/A;

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/A;-><init>(Lcom/dropbox/android/activity/b;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    .line 92
    iput-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    .line 108
    iput-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->s:Ldbxyzptlk/db231222/g/W;

    .line 114
    iput-boolean v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->t:Z

    .line 158
    iput-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    .line 315
    iput-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->A:Lcom/dropbox/android/util/as;

    .line 590
    new-instance v0, Lcom/dropbox/android/activity/c;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/c;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->B:Ldbxyzptlk/db231222/g/U;

    .line 617
    new-instance v0, Lcom/dropbox/android/activity/e;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/e;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->C:Lcom/dropbox/android/activity/a;

    .line 644
    new-instance v0, Lcom/dropbox/android/activity/f;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/f;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->D:Lcom/dropbox/android/albums/q;

    .line 750
    new-instance v0, Lcom/dropbox/android/activity/h;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/h;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->E:Landroid/view/View$OnClickListener;

    .line 798
    new-instance v0, Lcom/dropbox/android/activity/i;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/i;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->F:Ldbxyzptlk/db231222/j/k;

    .line 1001
    new-instance v0, Lcom/dropbox/android/activity/l;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/l;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->I:Lcom/actionbarsherlock/view/ActionMode$Callback;

    .line 1340
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->J:Landroid/os/Handler;

    .line 1344
    return-void
.end method

.method public static a(Lcom/dropbox/android/albums/Album;Ljava/util/ArrayList;)Lcom/dropbox/android/activity/AlbumViewFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/dropbox/android/activity/AlbumViewFragment;"
        }
    .end annotation

    .prologue
    .line 319
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 320
    if-eqz p0, :cond_1

    .line 321
    const-string v1, "ARG_ALBUM"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 325
    :cond_0
    :goto_0
    new-instance v1, Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;-><init>()V

    .line 326
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 328
    return-object v1

    .line 322
    :cond_1
    if-eqz p1, :cond_0

    .line 323
    const-string v1, "ARG_NEW_ALBUM"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/albums/Album;Ljava/util/ArrayList;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/AlbumViewFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")",
            "Lcom/dropbox/android/activity/AlbumViewFragment;"
        }
    .end annotation

    .prologue
    .line 333
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 334
    const-string v1, "ARG_ALBUM"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 335
    const-string v1, "ARG_PATHS_ADDED"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 336
    const-string v1, "ARG_PATH_TO_SCROLL_TO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 338
    new-instance v1, Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;-><init>()V

    .line 339
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 341
    return-object v1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/AlbumViewFragment;Lcom/dropbox/android/activity/z;)Lcom/dropbox/android/activity/z;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/AlbumViewFragment;Lcom/dropbox/android/albums/Album;)Lcom/dropbox/android/albums/Album;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    return-object p1
.end method

.method private a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;
    .locals 7

    .prologue
    .line 166
    new-instance v0, Lcom/dropbox/android/activity/b;

    const-string v2, "SIS_KEY_WaitingForUnshare"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->f:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d027e

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/b;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->v:Lcom/dropbox/android/albums/o;

    .line 186
    new-instance v0, Lcom/dropbox/android/activity/p;

    const-string v2, "SIS_KEY_WaitingForDelete"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->g:Lcom/dropbox/android/albums/t;

    const/4 v5, -0x1

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/p;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->w:Lcom/dropbox/android/albums/o;

    .line 209
    new-instance v0, Lcom/dropbox/android/activity/q;

    const-string v2, "SIS_KEY_WaitingForRename"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->h:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d0288

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/q;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->x:Lcom/dropbox/android/albums/o;

    .line 237
    new-instance v0, Lcom/dropbox/android/activity/r;

    const-string v2, "SIS_KEY_WaitingForRemove"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->d:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d027c

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/r;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->y:Lcom/dropbox/android/albums/o;

    .line 259
    new-instance v0, Lcom/dropbox/android/activity/s;

    const-string v2, "SIS_KEY_WaitingForCreate"

    iget-object v3, p1, Lcom/dropbox/android/albums/PhotosModel;->c:Lcom/dropbox/android/albums/t;

    const v5, 0x7f0d0287

    move-object v1, p0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/s;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->z:Lcom/dropbox/android/albums/o;

    .line 295
    new-instance v0, Lcom/dropbox/android/util/as;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/dropbox/android/util/at;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->v:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->w:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->x:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->y:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->z:Lcom/dropbox/android/albums/o;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/as;-><init>([Lcom/dropbox/android/util/at;)V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/AlbumViewFragment;Ldbxyzptlk/db231222/g/W;)Ldbxyzptlk/db231222/g/W;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->s:Ldbxyzptlk/db231222/g/W;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(Lcom/dropbox/android/albums/AlbumItemEntry;)V
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {p1}, Lcom/dropbox/android/albums/AlbumItemEntry;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/activity/A;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 955
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->p()V

    .line 956
    return-void
.end method

.method private a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1130
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 1131
    if-nez v2, :cond_0

    .line 1153
    :goto_0
    return v0

    .line 1134
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1144
    :pswitch_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;->a(Lcom/dropbox/android/activity/AlbumViewFragment;Z)Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/AlbumViewFragment$UnshareAlbumConfirm;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v1

    .line 1145
    goto :goto_0

    .line 1136
    :pswitch_1
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/dropbox/android/activity/PhotoGridPickerActivity;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1137
    const-string v2, "ALBUM_TO_ADD_TO_EXTRA"

    iget-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1138
    invoke-virtual {p0, v3, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    .line 1139
    goto :goto_0

    .line 1141
    :pswitch_2
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->q()V

    move v0, v1

    .line 1142
    goto :goto_0

    .line 1147
    :pswitch_3
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment$DeleteAlbumConfirmFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;Z)Lcom/dropbox/android/activity/AlbumViewFragment$DeleteAlbumConfirmFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/activity/AlbumViewFragment$DeleteAlbumConfirmFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v1

    .line 1148
    goto :goto_0

    .line 1150
    :pswitch_4
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->e()V

    move v0, v1

    .line 1151
    goto :goto_0

    .line 1134
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lcom/dropbox/android/activity/AlbumViewFragment;I)Z
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/AlbumViewFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/dropbox/android/activity/AlbumViewFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()Lcom/dropbox/android/albums/PhotosModel;
    .locals 1

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->j()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 605
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->r:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->s:Ldbxyzptlk/db231222/g/W;

    sget-object v2, Ldbxyzptlk/db231222/g/W;->a:Ldbxyzptlk/db231222/g/W;

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->t:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 606
    return-void

    .line 605
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->r()V

    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/q;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->D:Lcom/dropbox/android/albums/q;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 610
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 611
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_0

    .line 612
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->o()V

    .line 613
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-static {v0}, Lcom/dropbox/android/util/Activities;->a(Lcom/dropbox/android/albums/Album;)Lcom/dropbox/android/activity/base/BaseUserDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 615
    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 631
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 632
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    .line 634
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->c()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    .line 635
    if-eqz v0, :cond_0

    .line 636
    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    .line 637
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->o()V

    .line 642
    :goto_0
    return-void

    .line 639
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/AlbumViewFragment;->a:Ljava/lang/String;

    const-string v1, "Got a change notification for an album that no longer exists."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->o()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->k()V

    return-void
.end method

.method static synthetic h(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->e()V

    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 659
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 665
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    :goto_0
    return-void

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 670
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 671
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 675
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/AlbumViewFragment;->setMenuVisibility(Z)V

    .line 678
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 679
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 681
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 682
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ak()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 683
    iput-boolean v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    goto :goto_0
.end method

.method static synthetic i(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->d()V

    return-void
.end method

.method static synthetic j(Lcom/dropbox/android/activity/AlbumViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->J:Landroid/os/Handler;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 688
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 689
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 690
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 692
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 698
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->setMenuVisibility(Z)V

    .line 699
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->al()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 700
    iput-boolean v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    .line 701
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 704
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->z:Lcom/dropbox/android/albums/o;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/o;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->x:Lcom/dropbox/android/albums/o;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/o;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 705
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/AlbumViewFragment;->a:Ljava/lang/String;

    const-string v1, "Create or rename attempted twice!"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_1
    :goto_0
    return-void

    .line 709
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 710
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 714
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->l()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 715
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->z:Lcom/dropbox/android/albums/o;

    new-instance v2, Lcom/dropbox/android/activity/x;

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/dropbox/android/util/DropboxPath;->b(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/dropbox/android/activity/x;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v4}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 729
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 731
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 718
    :cond_3
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 719
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 720
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->x:Lcom/dropbox/android/albums/o;

    invoke-virtual {v1, v0, v4}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 722
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 723
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->j()V

    goto :goto_1

    .line 726
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should be in create or rename mode."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic k(Lcom/dropbox/android/activity/AlbumViewFragment;)Z
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m()Z

    move-result v0

    return v0
.end method

.method static synthetic l(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->f()V

    return-void
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic m(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/activity/A;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    return-object v0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 744
    iget-boolean v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    if-eqz v0, :cond_0

    .line 745
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->l()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 747
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    return v0
.end method

.method static synthetic n(Lcom/dropbox/android/activity/AlbumViewFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    return-object v0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 765
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    if-eqz v0, :cond_0

    .line 766
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 768
    :cond_0
    return-void
.end method

.method private o()V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 814
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 817
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->l()Z

    move-result v5

    .line 818
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m()Z

    move-result v6

    .line 821
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    if-nez v0, :cond_0

    .line 822
    new-instance v0, Ldbxyzptlk/db231222/j/l;

    iget-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-direct {v0, v4}, Ldbxyzptlk/db231222/j/l;-><init>(Lcom/dropbox/android/albums/Album;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    .line 823
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v0

    iget-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    iget-object v7, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->F:Ldbxyzptlk/db231222/j/k;

    invoke-virtual {v0, v4, v7}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/k;)V

    .line 827
    :cond_0
    if-eqz v5, :cond_9

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    move-object v4, v0

    .line 828
    :goto_0
    if-nez v5, :cond_a

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    .line 830
    :goto_1
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 831
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 832
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v7}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 833
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 839
    :goto_2
    iget-object v7, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->i:Landroid/view/View;

    if-nez v5, :cond_1

    if-eqz v6, :cond_c

    :cond_1
    move v0, v2

    :goto_3
    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 841
    if-nez v5, :cond_2

    if-nez v6, :cond_2

    .line 842
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 845
    :cond_2
    if-nez v5, :cond_3

    if-eqz v6, :cond_4

    .line 846
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 849
    :cond_4
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->H:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x7f0d0285

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 851
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->j:Landroid/view/View;

    if-nez v5, :cond_5

    if-eqz v6, :cond_e

    :cond_5
    move v0, v2

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 852
    if-nez v5, :cond_f

    if-nez v6, :cond_f

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->setMenuVisibility(Z)V

    .line 853
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 854
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    if-nez v5, :cond_6

    if-eqz v6, :cond_7

    :cond_6
    move v2, v3

    :cond_7
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 857
    :cond_8
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->n()V

    .line 859
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/actionbarsherlock/app/SherlockFragmentActivity;

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/SherlockFragmentActivity;->supportInvalidateOptionsMenu()V

    .line 860
    return-void

    .line 827
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    move-object v4, v0

    goto/16 :goto_0

    .line 828
    :cond_a
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    goto/16 :goto_1

    .line 835
    :cond_b
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 836
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_c
    move v0, v1

    .line 839
    goto :goto_3

    .line 849
    :cond_d
    const v0, 0x7f0d02a3

    goto :goto_4

    :cond_e
    move v0, v3

    .line 851
    goto :goto_5

    :cond_f
    move v0, v2

    .line 852
    goto :goto_6
.end method

.method static synthetic o(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->i()V

    return-void
.end method

.method static synthetic p(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/o;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->y:Lcom/dropbox/android/albums/o;

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 959
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/A;->b()I

    move-result v1

    invoke-static {v1}, Lcom/dropbox/android/util/UIHelpers;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 961
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    invoke-virtual {v0}, Lcom/actionbarsherlock/view/ActionMode;->invalidate()V

    .line 963
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->a()V

    .line 964
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 967
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 980
    :cond_0
    :goto_0
    return-void

    .line 970
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/A;->a(Z)V

    .line 974
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ai()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 975
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->p()V

    .line 976
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/actionbarsherlock/app/SherlockFragmentActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->I:Lcom/actionbarsherlock/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/SherlockFragmentActivity;->startActionMode(Lcom/actionbarsherlock/view/ActionMode$Callback;)Lcom/actionbarsherlock/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    .line 977
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic q(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->n()V

    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    invoke-virtual {v0}, Lcom/actionbarsherlock/view/ActionMode;->finish()V

    .line 986
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->s()V

    .line 987
    return-void
.end method

.method static synthetic r(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->s()V

    return-void
.end method

.method static synthetic s(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/o;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->w:Lcom/dropbox/android/albums/o;

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 990
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->f:Lcom/actionbarsherlock/view/ActionMode;

    .line 991
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 992
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 994
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/A;->a(Z)V

    .line 996
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aj()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 997
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->d()V

    .line 998
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->p()V

    .line 999
    return-void
.end method

.method static synthetic t(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/o;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->v:Lcom/dropbox/android/albums/o;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 771
    .line 773
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/A;->c()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 774
    invoke-virtual {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    :goto_1
    move v2, v1

    move v1, v0

    .line 779
    goto :goto_0

    .line 777
    :cond_0
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    goto :goto_1

    .line 780
    :cond_1
    invoke-static {p0, v2, v1}, Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;II)Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 781
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 864
    packed-switch p1, :pswitch_data_0

    .line 909
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 866
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 867
    const-string v0, "PATH_TO_SCROLL_TO_EXTRA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 868
    const-string v1, "ADDED_PATHS_EXTRA"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 869
    invoke-static {v1}, Lcom/dropbox/android/util/DropboxPath;->b(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    .line 871
    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v2, v0, v3, v1}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V

    .line 912
    :cond_0
    :goto_0
    return-void

    .line 879
    :pswitch_1
    if-eqz p3, :cond_0

    .line 880
    const-string v0, "FINAL_IMAGE_PATH"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 886
    const-string v0, "FINAL_IMAGE_INDEX"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 887
    if-ltz v0, :cond_0

    .line 888
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    const-string v2, "FINAL_IMAGE_PATH"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    .line 889
    new-instance v2, Lcom/dropbox/android/activity/k;

    invoke-direct {v2, p0, v1, v0}, Lcom/dropbox/android/activity/k;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;Lcom/dropbox/android/util/DropboxPath;I)V

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 864
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 392
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->a(Landroid/os/Bundle;)V

    .line 393
    if-eqz p1, :cond_0

    .line 394
    const-string v0, "SIS_KEY_TMP_ALBUM"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    .line 395
    const-string v0, "SIS_KEY_ALBUM"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    .line 396
    const-string v0, "SIS_IN_RENAME_MODE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    .line 398
    const-string v0, "SIS_KEY_IsInMultiSelect"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 399
    if-eqz v0, :cond_0

    .line 400
    const-string v0, "SIS_KEY_SelectedItems"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 401
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/A;->d()V

    .line 402
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/A;->a(Z)V

    .line 403
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 404
    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/android/activity/A;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 408
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1232
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/f;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1233
    check-cast p1, Lcom/dropbox/android/activity/y;

    invoke-virtual {p1}, Lcom/dropbox/android/activity/y;->y()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->t:Z

    .line 1235
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->d()V

    .line 1237
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1238
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1246
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    if-eqz v0, :cond_3

    .line 1247
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    iget-object v1, v1, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    iget v2, v2, Lcom/dropbox/android/activity/z;->b:I

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    iget-object v3, v3, Lcom/dropbox/android/activity/z;->c:Ljava/util/Collection;

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/widget/SweetListView;->setDelayedScrollAndHighlight(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V

    .line 1249
    iput-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    .line 1258
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/SweetListView;->requestLayout()V

    .line 1261
    iput-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->m:Lcom/dropbox/android/util/aW;

    .line 1262
    return-void

    .line 1240
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1251
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->m:Lcom/dropbox/android/util/aW;

    if-eqz v0, :cond_1

    .line 1255
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->m:Lcom/dropbox/android/util/aW;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aW;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedRestorePositionFromTop(I)V

    goto :goto_1
.end method

.method protected final a(Lcom/dropbox/android/widget/SweetListView;Landroid/view/View;IJ)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1198
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/f;->g()Landroid/database/Cursor;

    move-result-object v0

    .line 1199
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1200
    sget-object v2, Lcom/dropbox/android/activity/AlbumViewFragment;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t move cursor to position:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 1215
    :goto_0
    return v0

    .line 1206
    :cond_1
    sget-object v2, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v0, v2}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v2

    .line 1207
    sget-object v3, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/A;->e()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1208
    invoke-static {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/AlbumItemEntry;

    move-result-object v0

    .line 1209
    invoke-virtual {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1210
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/albums/AlbumItemEntry;)V

    .line 1211
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->q()V

    .line 1213
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1215
    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 477
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->setHasOptionsMenu(Z)V

    .line 479
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 481
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 482
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setSweetAdapter(Lcom/dropbox/android/widget/bC;)V

    .line 483
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->n:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 484
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->o:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 485
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 488
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->q()V

    .line 491
    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 346
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onAttach(Landroid/app/Activity;)V

    .line 347
    check-cast p1, Lcom/dropbox/android/activity/AlbumViewActivity;

    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->C:Lcom/dropbox/android/activity/a;

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/AlbumViewActivity;->a(Lcom/dropbox/android/activity/a;)V

    .line 348
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 366
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 368
    if-nez p1, :cond_0

    .line 369
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "ARG_ALBUM"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    .line 370
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "ARG_NEW_ALBUM"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    if-nez v3, :cond_2

    :goto_1
    if-ne v0, v1, :cond_3

    .line 374
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Either an album or a list of paths must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 373
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 377
    :cond_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 378
    if-eqz v0, :cond_4

    .line 379
    new-instance v1, Lcom/dropbox/android/widget/f;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/I;->f()Lcom/dropbox/android/taskqueue/D;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/dropbox/android/widget/f;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/dropbox/android/widget/bY;Lcom/dropbox/android/taskqueue/D;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    .line 385
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/albums/PhotosModel;)Lcom/dropbox/android/util/as;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->A:Lcom/dropbox/android/util/as;

    .line 386
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->A:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->a(Landroid/os/Bundle;)V

    .line 388
    :cond_4
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1225
    new-instance v0, Lcom/dropbox/android/activity/y;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->c()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    iget-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/android/activity/y;-><init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public final onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .locals 10

    .prologue
    const v6, 0x7f0d0285

    const/4 v9, 0x6

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 1078
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_3

    .line 1079
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1080
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v6

    :goto_0
    const v2, 0x7f08008c

    const v3, 0x7f020143

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;IIIZZ)Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    move-result-object v0

    .line 1087
    new-instance v1, Lcom/dropbox/android/activity/n;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/n;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1093
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    invoke-interface {p1, v7, v1, v7, v6}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/actionbarsherlock/view/MenuItem;->setActionView(Landroid/view/View;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v9}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1099
    const/4 v0, 0x3

    const v1, 0x7f0d0099

    invoke-interface {p1, v7, v0, v7, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020123

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v8}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 1102
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1103
    const v0, 0x7f0d0098

    invoke-interface {p1, v7, v8, v7, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020140

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v8}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 1107
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1108
    const v0, 0x7f0d0295

    invoke-interface {p1, v7, v7, v7, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    .line 1110
    :cond_2
    const v0, 0x7f0d0297

    invoke-interface {p1, v7, v4, v7, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    .line 1117
    :cond_3
    :goto_2
    return-void

    .line 1080
    :cond_4
    const v1, 0x7f0d02a3

    goto :goto_0

    .line 1093
    :cond_5
    const v6, 0x7f0d02a3

    goto :goto_1

    .line 1112
    :cond_6
    const v0, 0x7f0d0296

    invoke-interface {p1, v7, v7, v7, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020001

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v9}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 502
    const v0, 0x7f030017

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 504
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    .line 510
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/SweetListView;->setOverScrollMode(I)V

    .line 511
    const v0, 0x7f07003c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    .line 512
    const v0, 0x7f07003b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    .line 513
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFreezesText(Z)V

    .line 514
    if-eqz p3, :cond_0

    .line 515
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    const-string v2, "SIS_NAME_EDITTEXT"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 525
    const v0, 0x7f07003d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->i:Landroid/view/View;

    .line 527
    const v0, 0x7f070040

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->j:Landroid/view/View;

    .line 528
    const v0, 0x7f07003f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->p:Landroid/view/View;

    .line 530
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->i:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/t;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/t;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/u;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/u;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 548
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/v;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/v;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 561
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 563
    const v0, 0x7f070042

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->H:Landroid/widget/TextView;

    .line 564
    const v0, 0x7f070041

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    .line 565
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    new-instance v2, Lcom/dropbox/android/activity/w;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/w;-><init>(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 571
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->q:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 575
    :cond_1
    const v0, 0x7f07003e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->r:Landroid/widget/ProgressBar;

    .line 577
    if-nez p3, :cond_2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ARG_PATHS_ADDED"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 579
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ARG_PATH_TO_SCROLL_TO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 580
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_PATHS_ADDED"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 582
    new-instance v3, Lcom/dropbox/android/activity/z;

    invoke-static {v2}, Lcom/dropbox/android/util/DropboxPath;->b(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v3, v0, v4, v2}, Lcom/dropbox/android/activity/z;-><init>(Lcom/dropbox/android/util/DropboxPath;ILjava/util/Collection;)V

    iput-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->u:Lcom/dropbox/android/activity/z;

    .line 585
    :cond_2
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->o()V

    .line 587
    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 456
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onDestroy()V

    .line 458
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->A:Lcom/dropbox/android/util/as;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->A:Lcom/dropbox/android/util/as;

    invoke-virtual {v0}, Lcom/dropbox/android/util/as;->a()V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    if-eqz v0, :cond_2

    .line 463
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->h()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 464
    if-eqz v0, :cond_1

    .line 465
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->F:Ldbxyzptlk/db231222/j/k;

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/k;)V

    .line 467
    :cond_1
    iput-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->G:Ldbxyzptlk/db231222/j/l;

    .line 470
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    if-eqz v0, :cond_3

    .line 471
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/widget/f;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 473
    :cond_3
    return-void
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 495
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onDestroyView()V

    .line 496
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->k:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->unregisterForContextMenu(Landroid/view/View;)V

    .line 497
    return-void
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 352
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onDetach()V

    .line 353
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/f;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 356
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/AlbumViewActivity;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/AlbumViewActivity;->a(Lcom/dropbox/android/activity/a;)V

    .line 357
    return-void
.end method

.method public final onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    .line 916
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 951
    :cond_0
    :goto_0
    return-void

    .line 920
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/f;->g()Landroid/database/Cursor;

    move-result-object v6

    .line 921
    invoke-interface {v6, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 923
    sget-object v0, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v6, v0}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v0

    .line 924
    sget-object v1, Lcom/dropbox/android/activity/o;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 926
    :pswitch_0
    invoke-static {v6}, Lcom/dropbox/android/albums/AlbumItemEntry;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/AlbumItemEntry;

    move-result-object v2

    .line 927
    invoke-virtual {v2}, Lcom/dropbox/android/albums/AlbumItemEntry;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 931
    invoke-static {v6}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 932
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 934
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 936
    sget-object v1, Lcom/dropbox/android/provider/PhotosProvider;->b:Landroid/net/Uri;

    .line 939
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "album_gid"

    iget-object v4, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v4}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 941
    iget-object v3, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->i()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v5, Lcom/dropbox/android/activity/bq;->d:Lcom/dropbox/android/activity/bq;

    .line 942
    :goto_1
    invoke-virtual {v2}, Lcom/dropbox/android/albums/AlbumItemEntry;->d()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/activity/GalleryActivity;->a(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;JLcom/dropbox/android/activity/bq;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 941
    :cond_2
    sget-object v5, Lcom/dropbox/android/activity/bq;->c:Lcom/dropbox/android/activity/bq;

    goto :goto_1

    .line 945
    :cond_3
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/albums/AlbumItemEntry;)V

    goto :goto_0

    .line 924
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 80
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1269
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/f;->d(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1270
    return-void
.end method

.method public final onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1121
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->l()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Z)V

    .line 1122
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1123
    const/4 v0, 0x1

    .line 1125
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 412
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UserSweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 413
    const-string v0, "SIS_KEY_TMP_ALBUM"

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 414
    const-string v0, "SIS_KEY_ALBUM"

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 415
    const-string v0, "SIS_NAME_EDITTEXT"

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 416
    const-string v0, "SIS_IN_RENAME_MODE"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 418
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->A:Lcom/dropbox/android/util/as;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/as;->b(Landroid/os/Bundle;)V

    .line 420
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/A;->e()Z

    move-result v0

    .line 421
    const-string v1, "SIS_KEY_IsInMultiSelect"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 423
    if-eqz v0, :cond_0

    .line 424
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->e:Lcom/dropbox/android/activity/A;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/A;->c()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 425
    const-string v1, "SIS_KEY_SelectedItems"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 427
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 431
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onStart()V

    .line 432
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/f;->a(Z)V

    .line 433
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->c()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    .line 434
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->D:Lcom/dropbox/android/albums/q;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/q;)V

    .line 437
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 438
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->i()V

    .line 440
    :cond_1
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->B:Ldbxyzptlk/db231222/g/U;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/U;)V

    .line 441
    return-void
.end method

.method public final onStop()V
    .locals 3

    .prologue
    .line 445
    invoke-super {p0}, Lcom/dropbox/android/activity/UserSweetListFragment;->onStop()V

    .line 446
    iget-object v0, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->l:Lcom/dropbox/android/widget/bC;

    check-cast v0, Lcom/dropbox/android/widget/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/f;->a(Z)V

    .line 447
    invoke-direct {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->c()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v0

    .line 448
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->b:Lcom/dropbox/android/albums/Album;

    iget-object v2, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->D:Lcom/dropbox/android/albums/q;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->b(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/q;)V

    .line 451
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/albums/PhotosModel;->b()Ldbxyzptlk/db231222/g/R;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/AlbumViewFragment;->B:Ldbxyzptlk/db231222/g/U;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/R;->b(Ldbxyzptlk/db231222/g/U;)V

    .line 452
    return-void
.end method
