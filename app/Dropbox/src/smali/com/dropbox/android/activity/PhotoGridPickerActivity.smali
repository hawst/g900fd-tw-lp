.class public Lcom/dropbox/android/activity/PhotoGridPickerActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ALBUM_TO_ADD_TO_EXTRA"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected album to add to"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    invoke-static {p0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/app/Activity;)V

    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->setContentView(I)V

    .line 35
    const v0, 0x7f0d0283

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->setTitle(I)V

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 38
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 40
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    const v0, 0x7f0d0061

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 43
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->finish()V

    .line 55
    :cond_1
    :goto_0
    return-void

    .line 47
    :cond_2
    if-nez p1, :cond_1

    .line 49
    new-instance v0, Lcom/dropbox/android/activity/PhotoGridPickerFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/PhotoGridPickerFragment;-><init>()V

    .line 50
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PhotoGridPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 51
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 52
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
