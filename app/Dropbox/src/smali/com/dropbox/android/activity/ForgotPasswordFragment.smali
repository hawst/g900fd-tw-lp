.class public Lcom/dropbox/android/activity/ForgotPasswordFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/aQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private c:Landroid/widget/AutoCompleteTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/ForgotPasswordFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 44
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->setArguments(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/ForgotPasswordFragment;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/dropbox/android/activity/ForgotPasswordFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;-><init>()V

    .line 39
    invoke-virtual {v0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/ForgotPasswordFragment;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 100
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aQ;

    .line 102
    if-eqz v0, :cond_0

    .line 103
    iget-object v1, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/aQ;->b(Ljava/lang/String;)V

    .line 105
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/aQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    const-class v0, Lcom/dropbox/android/activity/aQ;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onActivityCreated(Landroid/os/Bundle;)V

    .line 110
    if-nez p1, :cond_0

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aQ;

    const v1, 0x7f030036

    const v2, 0x7f0300a8

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/dropbox/android/activity/aQ;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v1

    .line 62
    const v0, 0x7f0700b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->c:Landroid/widget/AutoCompleteTextView;

    .line 63
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x109000a

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/dropbox/android/util/Z;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 66
    iget-object v2, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->c:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/dropbox/android/activity/aO;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aO;-><init>(Lcom/dropbox/android/activity/ForgotPasswordFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 79
    if-nez p3, :cond_0

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aQ;

    invoke-interface {v0}, Lcom/dropbox/android/activity/aQ;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0701a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 85
    :goto_0
    new-instance v2, Lcom/dropbox/android/activity/aP;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aP;-><init>(Lcom/dropbox/android/activity/ForgotPasswordFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-object v1

    .line 83
    :cond_1
    const v0, 0x7f0700b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onResume()V

    .line 50
    iget-object v0, p0, Lcom/dropbox/android/activity/ForgotPasswordFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/aQ;

    const v1, 0x7f0d0221

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/aQ;->a(I)V

    .line 51
    return-void
.end method
