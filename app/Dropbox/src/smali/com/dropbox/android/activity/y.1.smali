.class final Lcom/dropbox/android/activity/y;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/albums/PhotosModel;

.field private final u:Lcom/dropbox/android/albums/Album;

.field private final v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dropbox/android/albums/PhotosModel;",
            "Lcom/dropbox/android/albums/Album;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1352
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 1349
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/y;->w:Z

    .line 1353
    iput-object p2, p0, Lcom/dropbox/android/activity/y;->l:Lcom/dropbox/android/albums/PhotosModel;

    .line 1354
    iput-object p3, p0, Lcom/dropbox/android/activity/y;->u:Lcom/dropbox/android/albums/Album;

    .line 1355
    iput-object p4, p0, Lcom/dropbox/android/activity/y;->v:Ljava/util/ArrayList;

    .line 1356
    return-void
.end method


# virtual methods
.method protected final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1344
    invoke-virtual {p0}, Lcom/dropbox/android/activity/y;->z()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 1363
    iget-boolean v0, p0, Lcom/dropbox/android/activity/y;->w:Z

    return v0
.end method

.method protected final z()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 1368
    .line 1369
    iget-object v0, p0, Lcom/dropbox/android/activity/y;->u:Lcom/dropbox/android/albums/Album;

    if-eqz v0, :cond_0

    .line 1370
    iget-object v0, p0, Lcom/dropbox/android/activity/y;->l:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/y;->u:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 1373
    iget-object v1, p0, Lcom/dropbox/android/activity/y;->l:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v2, p0, Lcom/dropbox/android/activity/y;->u:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/albums/PhotosModel;->b(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/activity/y;->w:Z

    .line 1378
    :goto_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/y;->a(Landroid/database/Cursor;)V

    .line 1379
    return-object v0

    .line 1375
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/y;->l:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/y;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/PhotosModel;->a(Ljava/util/List;)Landroid/database/Cursor;

    move-result-object v0

    .line 1376
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dropbox/android/activity/y;->w:Z

    goto :goto_0
.end method
