.class public Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/payment/y;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private e:Z

.field private f:I

.field private final g:Lcom/dropbox/android/service/I;

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 45
    new-instance v0, Lcom/dropbox/android/activity/payment/m;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/payment/m;-><init>(Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g:Lcom/dropbox/android/service/I;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;)Lcom/dropbox/android/service/I;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g:Lcom/dropbox/android/service/I;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;Lcom/dropbox/android/util/analytics/r;Z)V
    .locals 4

    .prologue
    .line 84
    const-string v0, "mobile-payment-selector-v1"

    invoke-virtual {p2, v0}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    const-string v1, "mobile-monsoon-upgrade-plan"

    invoke-virtual {p2, v1}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    const-string v2, "mobile-payment-selector-enable"

    invoke-virtual {p2, v2}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    const-string v3, "CONTROL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CONTROL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CONTROL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    .line 93
    :goto_0
    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    const-string v1, "payment_selector_fragment_upgrade_source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 96
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 101
    :goto_1
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_1
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/dropbox/android/activity/payment/PaymentCCWebviewActivity;->a(Landroid/content/Context;Lcom/dropbox/android/activity/payment/h;Z)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f()V

    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 133
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->br()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 134
    const v0, 0x7f030069

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(I)V

    .line 137
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_selector_fragment_upgrade_source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/h;

    .line 139
    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->h:Z

    iget-boolean v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->i:Z

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(Lcom/dropbox/android/activity/payment/h;ZZ)Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    move-result-object v0

    .line 141
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a(Z)Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    move-result-object v1

    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 144
    const v3, 0x7f070131

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 145
    const v1, 0x7f070132

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 146
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    .line 149
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 152
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(I)V

    .line 154
    new-instance v0, Lcom/dropbox/android/activity/payment/PaymentOfflineFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/payment/PaymentOfflineFragment;-><init>()V

    .line 155
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 156
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 157
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    .line 160
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 107
    iget v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f:I

    if-eq v0, p1, :cond_1

    .line 108
    sget-object v0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Swapping to layout 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, v3}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f:I

    invoke-static {v2, v3}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f:I

    const v1, 0x7f030069

    if-ne v0, v1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 116
    const v1, 0x7f070131

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 117
    const v2, 0x7f070132

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 119
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 121
    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 122
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 125
    :cond_0
    iput p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f:I

    .line 126
    iget v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f:I

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->setContentView(I)V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_1
    sget-object v0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already at layout 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, v3}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 261
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    .line 263
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070132

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;

    .line 265
    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/activity/payment/PaymentSelectorFragment;->a(IILandroid/content/Intent;)V

    .line 267
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 271
    if-eqz p1, :cond_0

    .line 272
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(I)V

    .line 273
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 274
    const v1, 0x7f0700b2

    new-instance v2, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;

    invoke-direct {v2}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 275
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->e:Z

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->finish()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->h:Z

    .line 285
    iput-boolean p1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->i:Z

    .line 286
    return-void
.end method

.method final e()Z
    .locals 2

    .prologue
    .line 168
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    const/4 v0, 0x1

    .line 175
    :goto_0
    return v0

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g()V

    .line 174
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    .line 175
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 185
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->requestWindowFeature(I)Z

    .line 187
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 188
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->finish()V

    .line 224
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 194
    const v0, 0x7f0d02d9

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->setTitle(I)V

    .line 196
    if-eqz p1, :cond_4

    .line 199
    const-string v0, "SIS_KEY_SHOWINGOFFLINE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    .line 200
    const-string v0, "SIS_KEY_SHOWINGUPGRADESUCCESS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->e:Z

    .line 202
    const-string v0, "EXTRA_CHECK_SUBSCRIPTIONS"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->h:Z

    .line 204
    const-string v0, "EXTRA_SUBSCRIPTION_USED"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->i:Z

    .line 205
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->e:Z

    if-eqz v0, :cond_1

    .line 206
    iput-boolean v2, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    .line 209
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->e:Z

    if-eqz v0, :cond_3

    .line 210
    :cond_2
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(I)V

    goto :goto_0

    .line 212
    :cond_3
    const v0, 0x7f030069

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->a(I)V

    goto :goto_0

    .line 217
    :cond_4
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 218
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f()V

    goto :goto_0

    .line 220
    :cond_5
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onPause()V

    .line 244
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 245
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 228
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 229
    iget-boolean v0, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    if-eqz v0, :cond_0

    .line 230
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    .line 233
    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    invoke-direct {p0}, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->f()V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->g:Lcom/dropbox/android/service/I;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 250
    const-string v0, "SIS_KEY_SHOWINGOFFLINE"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    const-string v0, "SIS_KEY_SHOWINGUPGRADESUCCESS"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 252
    const-string v0, "EXTRA_CHECK_SUBSCRIPTIONS"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 254
    const-string v0, "EXTRA_SUBSCRIPTION_USED"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/payment/PaymentSelectorActivity;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 255
    return-void
.end method
