.class public Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 18
    const-wide/16 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->requestWindowFeature(J)V

    .line 20
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->finish()V

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 27
    const v0, 0x7f0d02d9

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->setTitle(I)V

    .line 28
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->setContentView(I)V

    .line 30
    if-nez p1, :cond_0

    .line 31
    new-instance v0, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessTestActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 33
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 34
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
