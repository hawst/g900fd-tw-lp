.class public Lcom/dropbox/android/activity/AccountsAndSyncSetupActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 33
    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-string v0, "accountAuthenticatorResponse"

    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 37
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/dropbox/android/activity/fn;->a(Z)[Lcom/dropbox/android/activity/fn;

    move-result-object v0

    .line 21
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;[Lcom/dropbox/android/activity/fn;)Landroid/content/Intent;

    move-result-object v0

    .line 22
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AccountsAndSyncSetupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 24
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 25
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AccountsAndSyncSetupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/dropbox/android/activity/AccountsAndSyncSetupActivity;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 26
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/AccountsAndSyncSetupActivity;->startActivity(Landroid/content/Intent;)V

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AccountsAndSyncSetupActivity;->finish()V

    .line 30
    return-void
.end method
