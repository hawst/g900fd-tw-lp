.class final Lcom/dropbox/android/activity/cJ;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/MainBrowserFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/MainBrowserFragment;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/dropbox/android/activity/cJ;->a:Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/activity/cJ;->a:Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/activity/QrAuthActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    const-string v1, "com.dropbox.intent.extra.QR_LAUNCH_SOURCE"

    const-string v2, "MainBrowserFragment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    iget-object v1, p0, Lcom/dropbox/android/activity/cJ;->a:Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/MainBrowserFragment;->startActivity(Landroid/content/Intent;)V

    .line 79
    return-void
.end method
