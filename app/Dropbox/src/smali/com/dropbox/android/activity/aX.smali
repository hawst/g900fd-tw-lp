.class final Lcom/dropbox/android/activity/aX;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lcom/dropbox/android/activity/aX;->b:Lcom/dropbox/android/activity/GalleryActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/aX;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 657
    iget-object v0, p0, Lcom/dropbox/android/activity/aX;->b:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    if-nez v0, :cond_0

    .line 685
    :goto_0
    return-void

    .line 661
    :cond_0
    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/dropbox/android/activity/aX;->b:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/aX;->a:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 664
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0d01e3

    invoke-interface {v1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 665
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0d01e0

    invoke-interface {v2, v3}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 667
    new-instance v3, Lcom/dropbox/android/activity/aY;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/aY;-><init>(Lcom/dropbox/android/activity/aX;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 675
    new-instance v1, Lcom/dropbox/android/activity/aZ;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/aZ;-><init>(Lcom/dropbox/android/activity/aX;)V

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 684
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method
