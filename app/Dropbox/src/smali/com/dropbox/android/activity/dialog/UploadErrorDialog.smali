.class public Lcom/dropbox/android/activity/dialog/UploadErrorDialog;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    .line 45
    return-void
.end method

.method private static a(ZLcom/dropbox/android/taskqueue/w;)I
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/dropbox/android/activity/dialog/M;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 45
    if-eqz p0, :cond_3

    const v0, 0x7f0d00e1

    :goto_0
    return v0

    .line 34
    :pswitch_0
    if-eqz p0, :cond_0

    .line 35
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "We swallow camera upload storage exceptions and should not hit this."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    const v0, 0x7f0d00df

    goto :goto_0

    .line 39
    :pswitch_1
    if-eqz p0, :cond_1

    const v0, 0x7f0d00e3

    goto :goto_0

    :cond_1
    const v0, 0x7f0d00de

    goto :goto_0

    .line 43
    :pswitch_2
    if-eqz p0, :cond_2

    const v0, 0x7f0d00e2

    goto :goto_0

    :cond_2
    const v0, 0x7f0d00dd

    goto :goto_0

    .line 45
    :cond_3
    const v0, 0x7f0d00dc

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a(ZLcom/dropbox/android/taskqueue/w;Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/UploadErrorDialog;
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;-><init>()V

    .line 23
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 24
    const-string v2, "ARG_CAMERA_UPLOAD"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 25
    const-string v2, "ARG_STATUS"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v2, "ARG_FILENAME"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->setArguments(Landroid/os/Bundle;)V

    .line 28
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CAMERA_UPLOAD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 52
    const-class v0, Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_STATUS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dropbox/android/taskqueue/w;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/w;

    .line 53
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_FILENAME"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-static {v1, v0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a(ZLcom/dropbox/android/taskqueue/w;)I

    move-result v0

    .line 56
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {v3, v0, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 58
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 59
    if-eqz v1, :cond_0

    const v0, 0x7f0d00e0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 60
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 61
    const v0, 0x7f0d0014

    new-instance v1, Lcom/dropbox/android/activity/dialog/L;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dialog/L;-><init>(Lcom/dropbox/android/activity/dialog/UploadErrorDialog;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 68
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 59
    :cond_0
    const v0, 0x7f0d00db

    goto :goto_0
.end method
