.class final Lcom/dropbox/android/activity/dX;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/n/P;

.field final synthetic b:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/dropbox/android/activity/dX;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/dX;->a:Ldbxyzptlk/db231222/n/P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 250
    const-string v0, "limit"

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 251
    iget-object v1, p0, Lcom/dropbox/android/activity/dX;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/PrefsActivity;->b(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/service/i;->c(Z)V

    .line 252
    iget-object v1, p0, Lcom/dropbox/android/activity/dX;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/dX;->a:Ldbxyzptlk/db231222/n/P;

    invoke-static {v1, v2}, Lcom/dropbox/android/activity/PrefsActivity;->a(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V

    .line 253
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->l()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "cameraupload.limitdataplanfilesize"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 254
    const/4 v0, 0x1

    return v0
.end method
