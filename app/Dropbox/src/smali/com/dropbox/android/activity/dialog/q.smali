.class final Lcom/dropbox/android/activity/dialog/q;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/app/AlertDialog;

.field final synthetic b:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/q;->b:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    iput-object p2, p0, Lcom/dropbox/android/activity/dialog/q;->a:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 157
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-static {v1}, Lcom/dropbox/android/util/ab;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {p1, v0, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/q;->a:Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/q;->b:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method
