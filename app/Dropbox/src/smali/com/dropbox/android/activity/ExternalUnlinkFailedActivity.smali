.class public Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public final a_()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity;->finish()V

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    if-nez p1, :cond_0

    .line 32
    invoke-static {}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;->a()Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method
