.class final Lcom/dropbox/android/activity/fP;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/dropbox/android/activity/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/dropbox/android/activity/fP;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/fP;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4

    .prologue
    .line 230
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->I()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "buffer"

    iget-object v2, p0, Lcom/dropbox/android/activity/fP;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/widget/DbxVideoView;->f()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "container"

    iget-object v2, p0, Lcom/dropbox/android/activity/fP;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fP;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/util/analytics/ChainInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/fP;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->c(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/service/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 234
    return-void
.end method
