.class public final Lcom/dropbox/android/activity/ed;
.super Landroid/app/DialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 541
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 3

    .prologue
    .line 544
    new-instance v0, Lcom/dropbox/android/activity/ed;

    invoke-direct {v0}, Lcom/dropbox/android/activity/ed;-><init>()V

    .line 545
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/ed;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 546
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 550
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ed;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/PrefsActivity;

    .line 551
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 552
    const v2, 0x7f0d01a9

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 553
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 554
    const v2, 0x7f0e0007

    new-instance v3, Lcom/dropbox/android/activity/ee;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/ee;-><init>(Lcom/dropbox/android/activity/ed;Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 579
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
