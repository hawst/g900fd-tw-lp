.class public Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/ab;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public static b()Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;-><init>()V

    .line 30
    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/ab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    const-class v0, Lcom/dropbox/android/activity/ab;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ab;

    invoke-interface {v0}, Lcom/dropbox/android/activity/ab;->f()Z

    move-result v1

    .line 49
    iget-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ab;

    const v2, 0x7f03002a

    const v3, 0x7f0300a4

    invoke-interface {v0, p1, p2, v2, v3}, Lcom/dropbox/android/activity/ab;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v2

    .line 55
    const v0, 0x7f070094

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 56
    new-instance v3, Lcom/dropbox/android/activity/Y;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/Y;-><init>(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v0, 0x7f070095

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 66
    new-instance v3, Lcom/dropbox/android/activity/Z;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/Z;-><init>(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    if-eqz v1, :cond_0

    .line 74
    const v0, 0x7f0701a3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/dropbox/android/activity/aa;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/aa;-><init>(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_0
    return-object v2
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;->onResume()V

    .line 41
    iget-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/activity/ab;

    const v1, 0x7f0d023b

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/ab;->a(I)V

    .line 42
    return-void
.end method
