.class final Lcom/dropbox/android/activity/ad;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/DropboxActionBarActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/dropbox/android/activity/ad;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Ljava/lang/Integer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/dropbox/android/activity/ad;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)Lcom/dropbox/android/activity/ai;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/ai;->a(I)V

    .line 367
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 371
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 372
    iget-object v0, p0, Lcom/dropbox/android/activity/ad;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 373
    if-nez v0, :cond_1

    move-object v0, v1

    .line 382
    :goto_1
    return-object v0

    .line 371
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 378
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aJ;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aJ;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 382
    new-instance v0, Lcom/dropbox/android/notifications/I;

    iget-object v3, p0, Lcom/dropbox/android/activity/ad;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Lcom/dropbox/android/activity/as;

    invoke-direct {v4, v1}, Lcom/dropbox/android/activity/as;-><init>(Lcom/dropbox/android/activity/ac;)V

    invoke-direct {v0, v3, v2, v4}, Lcom/dropbox/android/notifications/I;-><init>(Landroid/content/Context;Lcom/dropbox/sync/android/aJ;Lcom/dropbox/android/notifications/K;)V

    goto :goto_1

    .line 379
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 380
    goto :goto_1
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 358
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/ad;->a(Landroid/support/v4/content/p;Ljava/lang/Integer;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 362
    return-void
.end method
