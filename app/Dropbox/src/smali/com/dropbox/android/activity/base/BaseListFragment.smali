.class public Lcom/dropbox/android/activity/base/BaseListFragment;
.super Lcom/actionbarsherlock/app/SherlockListFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/base/m;
.implements Lcom/dropbox/android/activity/base/n;


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;-><init>()V

    .line 15
    new-instance v0, Lcom/dropbox/android/activity/base/c;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/base/c;-><init>(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/activity/base/m;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    return-void
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/c;->a(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 45
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->e()V

    .line 46
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/android/activity/base/c;->a(IILandroid/content/Intent;)V

    .line 87
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListFragment;->onAttach(Landroid/app/Activity;)V

    .line 20
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->a()V

    .line 21
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->c()V

    .line 33
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/actionbarsherlock/app/SherlockListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/base/c;->d()V

    .line 39
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onDestroy()V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->k()V

    .line 82
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onDestroyView()V

    .line 75
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->j()V

    .line 76
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onDetach()V

    .line 26
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->b()V

    .line 27
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onPause()V

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->h()V

    .line 64
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onResume()V

    .line 57
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->g()V

    .line 58
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onStart()V

    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->f()V

    .line 52
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockListFragment;->onStop()V

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseListFragment;->a:Lcom/dropbox/android/activity/base/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/c;->i()V

    .line 70
    return-void
.end method
