.class final Lcom/dropbox/android/activity/r;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Lcom/dropbox/android/albums/g;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/dropbox/android/activity/r;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    iput-object p6, p0, Lcom/dropbox/android/activity/r;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/albums/g;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, Lcom/dropbox/android/activity/r;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p2, Lcom/dropbox/android/albums/g;->a:Lcom/dropbox/android/albums/Album;

    iget-object v2, p2, Lcom/dropbox/android/albums/g;->b:Ljava/util/Collection;

    invoke-virtual {v0, v1, v2, p1}, Lcom/dropbox/android/albums/PhotosModel;->b(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    check-cast p2, Lcom/dropbox/android/albums/g;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/r;->a(Lcom/dropbox/android/albums/u;Lcom/dropbox/android/albums/g;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d028e

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->a(I)V

    .line 246
    return-void
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/dropbox/android/activity/r;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->d(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    .line 256
    return-void
.end method
