.class public Lcom/dropbox/android/activity/LockCodePrefsActivity;
.super Lcom/dropbox/android/activity/base/BasePreferenceActivity;
.source "panda.py"


# instance fields
.field private b:Landroid/preference/Preference;

.field private c:Landroid/preference/Preference;

.field private d:Landroid/preference/CheckBoxPreference;

.field private e:Ldbxyzptlk/db231222/r/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e:Ldbxyzptlk/db231222/r/o;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LockCodePrefsActivity;)Ldbxyzptlk/db231222/r/o;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e:Ldbxyzptlk/db231222/r/o;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LockCodePrefsActivity;Lcom/dropbox/android/activity/lock/f;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->a(Lcom/dropbox/android/activity/lock/f;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/lock/f;)V
    .locals 3

    .prologue
    .line 101
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/lock/LockCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 102
    const-string v1, "PURPOSE"

    invoke-virtual {p1}, Lcom/dropbox/android/activity/lock/f;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 103
    invoke-virtual {p1}, Lcom/dropbox/android/activity/lock/f;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 104
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LockCodePrefsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->d:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->a()Z

    move-result v0

    .line 108
    iget-object v1, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e:Ldbxyzptlk/db231222/r/o;

    invoke-interface {v1}, Ldbxyzptlk/db231222/r/o;->c()Z

    move-result v1

    .line 109
    iget-object v2, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 110
    iget-object v2, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 111
    iget-object v2, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 112
    iget-object v1, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 113
    iget-object v1, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 114
    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->b:Landroid/preference/Preference;

    const v1, 0x7f0d01c3

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->b:Landroid/preference/Preference;

    const v1, 0x7f0d01c2

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 123
    invoke-static {p1}, Lcom/dropbox/android/activity/lock/f;->a(I)Lcom/dropbox/android/activity/lock/f;

    move-result-object v0

    .line 124
    sget-object v1, Lcom/dropbox/android/activity/ck;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/activity/lock/f;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 141
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 126
    :pswitch_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e()V

    .line 143
    :goto_0
    :pswitch_1
    return-void

    .line 130
    :pswitch_2
    invoke-direct {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e()V

    goto :goto_0

    .line 137
    :pswitch_3
    invoke-direct {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e()V

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 31
    if-nez v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->finish()V

    .line 87
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e:Ldbxyzptlk/db231222/r/o;

    .line 39
    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->addPreferencesFromResource(I)V

    .line 41
    const-string v0, "lock_code_settings_toggle"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->b:Landroid/preference/Preference;

    .line 42
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->b:Landroid/preference/Preference;

    new-instance v1, Lcom/dropbox/android/activity/ch;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ch;-><init>(Lcom/dropbox/android/activity/LockCodePrefsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 57
    const-string v0, "lock_code_settings_change"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->c:Landroid/preference/Preference;

    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->c:Landroid/preference/Preference;

    new-instance v1, Lcom/dropbox/android/activity/ci;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ci;-><init>(Lcom/dropbox/android/activity/LockCodePrefsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 66
    const-string v0, "lock_code_settings_erase_data"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->d:Landroid/preference/CheckBoxPreference;

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/LockCodePrefsActivity;->d:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/dropbox/android/activity/cj;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cj;-><init>(Lcom/dropbox/android/activity/LockCodePrefsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 82
    invoke-direct {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->e()V

    .line 84
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 86
    const v0, 0x7f0d01c1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->setTitle(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LockCodePrefsActivity;->finish()V

    .line 95
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->onResume()V

    .line 96
    return-void
.end method
