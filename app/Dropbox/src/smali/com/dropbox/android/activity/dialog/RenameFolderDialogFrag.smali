.class public Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dialog/p;


# instance fields
.field a:Landroid/view/View$OnClickListener;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/ProgressBar;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/text/TextWatcher;

.field private f:Lcom/dropbox/android/activity/dialog/v;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;-><init>()V

    .line 263
    new-instance v0, Lcom/dropbox/android/activity/dialog/r;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/r;-><init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a:Landroid/view/View$OnClickListener;

    .line 392
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 64
    new-instance v1, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-direct {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;-><init>()V

    .line 65
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v1
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 217
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Z)V

    .line 218
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c()V

    .line 219
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 220
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/dropbox/android/util/bn;->a(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    const v1, 0x7f0d0072

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 204
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 205
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 208
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 209
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 210
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 230
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Z)V

    .line 231
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d()V

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 233
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/dropbox/android/util/bn;->a(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 198
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 244
    iput-boolean p1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->g:Z

    .line 245
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 71
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 73
    iget-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0d00ba

    .line 75
    :goto_0
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 77
    iput-boolean v5, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->g:Z

    .line 78
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f03008f

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 80
    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0d00bb

    .line 81
    :goto_1
    invoke-virtual {v2, v0, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 82
    const v0, 0x7f0d0013

    invoke-virtual {v2, v0, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 86
    if-nez p1, :cond_2

    .line 87
    iput-boolean v5, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->h:Z

    .line 88
    iput-object v4, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    .line 102
    :goto_2
    return-object v1

    .line 73
    :cond_0
    const v1, 0x7f0d00b7

    goto :goto_0

    .line 80
    :cond_1
    const v0, 0x7f0d00b8

    goto :goto_1

    .line 90
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->h:Z

    .line 92
    new-instance v0, Lcom/dropbox/android/activity/dialog/v;

    invoke-direct {v0, v4}, Lcom/dropbox/android/activity/dialog/v;-><init>(Lcom/dropbox/android/activity/dialog/q;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v2, "CONFIRM_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/dropbox/android/activity/dialog/v;->a:Z

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v2, "CANCEL_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/dropbox/android/activity/dialog/v;->b:Z

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v2, "STATUS_TEXT"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/dropbox/android/activity/dialog/v;->c:Ljava/lang/String;

    .line 96
    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v0, "STATUS_COLORS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/res/ColorStateList;

    iput-object v0, v2, Lcom/dropbox/android/activity/dialog/v;->d:Landroid/content/res/ColorStateList;

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v2, "CANCELABLE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/dropbox/android/activity/dialog/v;->e:Z

    .line 98
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v2, "EDITABLE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/dropbox/android/activity/dialog/v;->f:Z

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    const-string v2, "PROGRESS_VISIBILITY"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lcom/dropbox/android/activity/dialog/v;->g:I

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 254
    const-string v1, "CONFIRM_STATE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 255
    const-string v1, "CANCEL_STATE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v2, -0x2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 256
    const-string v0, "STATUS_TEXT"

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v0, "STATUS_COLORS"

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 258
    const-string v0, "CANCELABLE"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 259
    const-string v0, "EDITABLE"

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    const-string v0, "PROGRESS_VISIBILITY"

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 261
    return-void
.end method

.method public onStart()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 107
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserDialogFragment;->onStart()V

    .line 109
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 111
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    .line 112
    const v2, 0x7f070179

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    .line 119
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-boolean v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->h:Z

    if-eqz v2, :cond_0

    .line 122
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v4

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 124
    iget-object v2, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-static {v2}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 125
    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v3, v0}, Landroid/widget/EditText;->setSelection(II)V

    .line 135
    :cond_0
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e:Landroid/text/TextWatcher;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e:Landroid/text/TextWatcher;

    .line 144
    :cond_1
    new-instance v0, Lcom/dropbox/android/activity/dialog/q;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/dialog/q;-><init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;Landroid/app/AlertDialog;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e:Landroid/text/TextWatcher;

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 173
    const v0, 0x7f07017b

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    .line 175
    iget-boolean v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->h:Z

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    :cond_2
    const v0, 0x7f07017a

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/widget/ProgressBar;

    .line 180
    iget-boolean v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->h:Z

    if-eqz v0, :cond_3

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    if-eqz v0, :cond_4

    .line 186
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget-boolean v2, v2, Lcom/dropbox/android/activity/dialog/v;->a:Z

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 187
    const/4 v0, -0x2

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget-boolean v2, v2, Lcom/dropbox/android/activity/dialog/v;->b:Z

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget-object v2, v2, Lcom/dropbox/android/activity/dialog/v;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget-object v2, v2, Lcom/dropbox/android/activity/dialog/v;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget-boolean v0, v0, Lcom/dropbox/android/activity/dialog/v;->e:Z

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 191
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget-boolean v1, v1, Lcom/dropbox/android/activity/dialog/v;->f:Z

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->f:Lcom/dropbox/android/activity/dialog/v;

    iget v1, v1, Lcom/dropbox/android/activity/dialog/v;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 194
    :cond_4
    return-void

    :cond_5
    move v2, v3

    .line 122
    goto/16 :goto_0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 405
    return-void
.end method
