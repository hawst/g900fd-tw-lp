.class final Lcom/dropbox/android/activity/bj;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/ax;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->b(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/GalleryActivity;->c(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 490
    iget-object v0, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d()V

    .line 491
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/analytics/ChainInfo;)V
    .locals 3

    .prologue
    .line 502
    iget-object v0, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/bs;->a:Lcom/dropbox/android/util/bs;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;Lcom/dropbox/android/util/analytics/ChainInfo;)V

    .line 505
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->b(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/GalleryActivity;->c(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 497
    iget-object v0, p0, Lcom/dropbox/android/activity/bj;->a:Lcom/dropbox/android/activity/GalleryActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Z)V

    .line 498
    return-void
.end method
