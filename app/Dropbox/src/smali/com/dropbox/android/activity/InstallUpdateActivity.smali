.class public Lcom/dropbox/android/activity/InstallUpdateActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Lcom/dropbox/android/activity/base/d;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 17
    const v0, 0x103006e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->setTheme(I)V

    .line 18
    const/4 v0, 0x1

    .line 21
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->e()Z

    move-result v0

    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->finish()V

    .line 41
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_FORCE_UPDATE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_USE_PLAY_STORE"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EXTRA_RELEASE_NOTES"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 38
    invoke-static {v1, v2, v0, v3}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a(ZZZLjava/lang/String;)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/dropbox/android/activity/InstallUpdateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method
