.class public Lcom/dropbox/android/activity/IntroTourActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/bX;


# instance fields
.field private a:Lcom/dropbox/android/activity/bR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 105
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bR;->a()Lcom/dropbox/android/activity/bW;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bW;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sign_up"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 74
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->d(Z)V

    .line 75
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/IntroTourActivity;->setResult(I)V

    .line 76
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourActivity;->finish()V

    .line 77
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bR;->a()Lcom/dropbox/android/activity/bW;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bW;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "log_in"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 82
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/n/k;->d(Z)V

    .line 83
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/IntroTourActivity;->setResult(I)V

    .line 84
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourActivity;->finish()V

    .line 85
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bR;->a()Lcom/dropbox/android/activity/bW;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bW;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "get_started"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 91
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->d(Z)V

    .line 92
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourActivity;->finish()V

    .line 93
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bR;->a()Lcom/dropbox/android/activity/bW;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bW;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "back"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 98
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/IntroTourActivity;->setResult(I)V

    .line 99
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourActivity;->finish()V

    .line 100
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f030068

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/IntroTourActivity;->setContentView(I)V

    .line 46
    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/IntroTourActivity;->setRequestedOrientation(I)V

    .line 51
    :cond_0
    new-instance v0, Lcom/dropbox/android/activity/bR;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_IS_POST_LOGIN"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/bR;-><init>(Landroid/support/v4/app/FragmentManager;ZLcom/dropbox/android/activity/bQ;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    .line 54
    const v0, 0x7f07012f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/IntroTourActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 55
    iget-object v1, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/E;)V

    .line 59
    iget-object v1, p0, Lcom/dropbox/android/activity/IntroTourActivity;->a:Lcom/dropbox/android/activity/bR;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bR;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 61
    const v1, 0x7f070130

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/IntroTourActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/viewpagerindicator/CirclePageIndicator;

    .line 62
    invoke-virtual {v1, v0}, Lcom/viewpagerindicator/CirclePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/viewpagerindicator/CirclePageIndicator;->setSnap(Z)V

    .line 64
    return-void
.end method
