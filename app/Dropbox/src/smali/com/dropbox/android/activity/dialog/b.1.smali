.class final Lcom/dropbox/android/activity/dialog/b;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->b(Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;)Lcom/dropbox/android/widget/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/b;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 100
    invoke-interface {v1, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 101
    const/4 v0, 0x0

    .line 102
    sget-object v2, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {v1, v2}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v2

    .line 103
    sget-object v3, Lcom/dropbox/android/activity/dialog/c;->a:[I

    invoke-virtual {v2}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 110
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :pswitch_0
    invoke-static {v1}, Lcom/dropbox/android/albums/Album;->a(Landroid/database/Cursor;)Lcom/dropbox/android/albums/Album;

    move-result-object v0

    move-object v1, v0

    .line 115
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/dropbox/android/activity/dialog/e;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/e;

    .line 121
    :goto_1
    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/dialog/e;->a(Lcom/dropbox/android/albums/Album;)V

    .line 122
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->dismiss()V

    .line 123
    return-void

    :pswitch_1
    move-object v1, v0

    .line 108
    goto :goto_0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/b;->a:Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/AlbumPickerDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/e;

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
