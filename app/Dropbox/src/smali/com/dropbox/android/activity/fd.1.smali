.class public final Lcom/dropbox/android/activity/fd;
.super Landroid/app/DialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/TextEditActivity;)V
    .locals 3

    .prologue
    .line 97
    new-instance v0, Lcom/dropbox/android/activity/fd;

    invoke-direct {v0}, Lcom/dropbox/android/activity/fd;-><init>()V

    .line 98
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/fd;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 99
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/dropbox/android/activity/fd;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/TextEditActivity;

    .line 105
    new-instance v1, Lcom/dropbox/android/activity/fe;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/fe;-><init>(Lcom/dropbox/android/activity/fd;Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 111
    new-instance v2, Lcom/dropbox/android/activity/ff;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/ff;-><init>(Lcom/dropbox/android/activity/fd;Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 124
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/fd;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 126
    const v3, 0x7f0d01f5

    invoke-virtual {v0, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 127
    const v2, 0x7f0d01f6

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    const v1, 0x7f0d01f3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 129
    const v1, 0x7f0d01f4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 130
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
