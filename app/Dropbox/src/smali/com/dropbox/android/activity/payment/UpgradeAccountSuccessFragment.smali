.class public Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;
.super Lcom/dropbox/android/activity/base/BaseUserFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 31
    const v0, 0x7f0300d4

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 35
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 38
    if-nez v0, :cond_0

    move-object v0, v1

    .line 72
    :goto_0
    return-object v0

    .line 43
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->j()Z

    move-result v2

    invoke-static {v2}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 48
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 53
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v3

    .line 54
    invoke-virtual {v3}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v3

    .line 55
    invoke-static {v2, v3, v4, v7}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v3

    .line 57
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->i()Ljava/lang/String;

    move-result-object v0

    .line 59
    new-instance v4, Landroid/text/SpannableString;

    const v5, 0x7f0d02f5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v0, v6, v3

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 60
    const v0, 0x7f0701ca

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    :cond_1
    const v0, 0x7f0701cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 65
    new-instance v2, Lcom/dropbox/android/activity/payment/z;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/payment/z;-><init>(Lcom/dropbox/android/activity/payment/UpgradeAccountSuccessFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 72
    goto :goto_0
.end method
