.class final Lcom/dropbox/android/activity/bJ;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/am;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/Z;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 392
    sget-object v0, Lcom/dropbox/android/activity/bM;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/provider/Z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 418
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected item type in this fragment"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 394
    :pswitch_0
    invoke-static {p2}, Lcom/dropbox/android/provider/V;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 395
    iget-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/filemanager/LocalEntry;I)V

    goto :goto_0

    .line 402
    :pswitch_1
    iget-object v0, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->n()V

    goto :goto_0

    .line 406
    :pswitch_2
    new-instance v0, Ldbxyzptlk/db231222/j/l;

    const-string v1, "id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/j/l;-><init>(J)V

    .line 407
    iget-object v1, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;)Ldbxyzptlk/db231222/j/g;

    move-result-object v0

    .line 408
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/j/g;->c()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v1, :cond_0

    .line 412
    sget-object v0, Lcom/dropbox/android/activity/dialog/o;->a:Lcom/dropbox/android/activity/dialog/o;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/a;->e()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Lcom/dropbox/android/activity/dialog/o;Lcom/dropbox/android/activity/dialog/p;Z)Lcom/dropbox/android/activity/dialog/OverQuotaDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bJ;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 386
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    .line 387
    const/4 v0, 0x1

    return v0
.end method
