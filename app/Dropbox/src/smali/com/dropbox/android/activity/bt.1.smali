.class final Lcom/dropbox/android/activity/bt;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/activity/GalleryActivity;

.field private final u:Lcom/dropbox/android/provider/MetadataManager;

.field private final v:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/provider/MetadataManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1311
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 1312
    iput-object p1, p0, Lcom/dropbox/android/activity/bt;->l:Lcom/dropbox/android/activity/GalleryActivity;

    .line 1313
    iput-object p2, p0, Lcom/dropbox/android/activity/bt;->u:Lcom/dropbox/android/provider/MetadataManager;

    .line 1314
    iput-object p3, p0, Lcom/dropbox/android/activity/bt;->v:Ljava/lang/String;

    .line 1315
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1304
    invoke-virtual {p0}, Lcom/dropbox/android/activity/bt;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 1321
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/activity/bt;->u:Lcom/dropbox/android/provider/MetadataManager;

    iget-object v1, p0, Lcom/dropbox/android/activity/bt;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/MetadataManager;->a(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Lcom/dropbox/android/provider/J; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1327
    iget-object v0, p0, Lcom/dropbox/android/activity/bt;->l:Lcom/dropbox/android/activity/GalleryActivity;

    new-instance v2, Lcom/dropbox/android/activity/bu;

    sget-object v3, Ldbxyzptlk/db231222/n/r;->b:Ldbxyzptlk/db231222/n/r;

    iget-object v4, p0, Lcom/dropbox/android/activity/bt;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/GalleryActivity;->m(Lcom/dropbox/android/activity/GalleryActivity;)I

    move-result v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/dropbox/android/activity/bu;-><init>(Ldbxyzptlk/db231222/n/r;IZ)V

    invoke-static {v0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/activity/bu;)Lcom/dropbox/android/activity/bu;

    .line 1328
    iget-object v0, p0, Lcom/dropbox/android/activity/bt;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->o(Lcom/dropbox/android/activity/GalleryActivity;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 1329
    iget-object v0, p0, Lcom/dropbox/android/activity/bt;->l:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_LOCAL_ENTRY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 1330
    iget-object v2, p0, Lcom/dropbox/android/activity/bt;->l:Lcom/dropbox/android/activity/GalleryActivity;

    iget-wide v3, v0, Lcom/dropbox/android/filemanager/LocalEntry;->o:J

    invoke-static {v2, v3, v4}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;J)J

    .line 1333
    :cond_0
    new-instance v0, Lcom/dropbox/android/provider/r;

    invoke-static {}, Lcom/dropbox/android/util/ab;->b()Lcom/dropbox/android/util/aY;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/provider/r;-><init>(Landroid/database/Cursor;Lcom/dropbox/android/util/aY;)V

    .line 1334
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/bt;->a(Landroid/database/Cursor;)V

    .line 1335
    return-object v0

    .line 1322
    :catch_0
    move-exception v0

    .line 1323
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
