.class public Lcom/dropbox/android/activity/IntroTourFragment;
.super Lcom/dropbox/android/activity/base/BaseFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/bX;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/dropbox/android/activity/bW;

.field private c:Lcom/dropbox/android/widget/TextureVideoView;

.field private d:Landroid/widget/ImageView;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragmentWCallback;-><init>()V

    .line 53
    return-void
.end method

.method public static a(IZ)Lcom/dropbox/android/activity/IntroTourFragment;
    .locals 3

    .prologue
    .line 75
    new-instance v0, Lcom/dropbox/android/activity/IntroTourFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/IntroTourFragment;-><init>()V

    .line 77
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 78
    const-string v2, "ARG_PAGE"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 79
    const-string v2, "ARG_IS_POST_LOGIN"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 80
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/IntroTourFragment;->setArguments(Landroid/os/Bundle;)V

    .line 82
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/IntroTourFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/IntroTourFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/IntroTourFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/bX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const-class v0, Lcom/dropbox/android/activity/bX;

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->f:Z

    .line 221
    iget-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/TextureVideoView;->a()V

    .line 224
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->f:Z

    .line 231
    iget-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/TextureVideoView;->b()V

    .line 234
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f0701c2

    const v6, 0x7f0701c1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    const-string v1, "ARG_PAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IntroTourFragment expects an arg with the page\'s index in the auth flow."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_1
    const-string v1, "ARG_PAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 97
    const-string v2, "ARG_IS_POST_LOGIN"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 98
    if-ltz v1, :cond_2

    invoke-static {}, Lcom/dropbox/android/activity/bW;->values()[Lcom/dropbox/android/activity/bW;

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 99
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown IntroTourFragment page index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_3
    invoke-static {}, Lcom/dropbox/android/activity/bW;->values()[Lcom/dropbox/android/activity/bW;

    move-result-object v2

    aget-object v2, v2, v1

    iput-object v2, p0, Lcom/dropbox/android/activity/IntroTourFragment;->a:Lcom/dropbox/android/activity/bW;

    .line 104
    sget-object v2, Lcom/dropbox/android/activity/bV;->a:[I

    iget-object v3, p0, Lcom/dropbox/android/activity/IntroTourFragment;->a:Lcom/dropbox/android/activity/bW;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/bW;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown IntroTourFragment page index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :pswitch_0
    if-eqz v0, :cond_a

    .line 107
    const v0, 0x7f0300d0

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    .line 113
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f040005

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 114
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 117
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f040006

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 118
    const v0, 0x7f0701c4

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 159
    :goto_1
    const v0, 0x7f0701c5

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_4

    .line 161
    new-instance v2, Lcom/dropbox/android/activity/bS;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bS;-><init>(Lcom/dropbox/android/activity/IntroTourFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    :cond_4
    const v0, 0x7f0701c7

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_5

    .line 172
    new-instance v2, Lcom/dropbox/android/activity/bT;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bT;-><init>(Lcom/dropbox/android/activity/IntroTourFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_5
    const v0, 0x7f0701c8

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_6

    .line 183
    new-instance v2, Lcom/dropbox/android/activity/bU;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bU;-><init>(Lcom/dropbox/android/activity/IntroTourFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    :cond_6
    const/4 v0, 0x5

    invoke-static {v0}, Landroid/media/CamcorderProfile;->hasProfile(I)Z

    move-result v0

    .line 196
    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 197
    iget-object v3, p0, Lcom/dropbox/android/activity/IntroTourFragment;->d:Landroid/widget/ImageView;

    if-eqz v3, :cond_8

    if-eqz v0, :cond_7

    const/16 v0, 0x140

    if-ge v2, v0, :cond_8

    .line 200
    :cond_7
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/TextureVideoView;->setVisibility(I)V

    .line 202
    iput-boolean v4, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    .line 204
    :cond_8
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->a:Lcom/dropbox/android/activity/bW;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bW;->toString()Ljava/lang/String;

    move-result-object v2

    iget-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    if-eqz v0, :cond_c

    const-string v0, "video"

    :goto_2
    invoke-static {v2, v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 209
    iget-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->f:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    if-eqz v0, :cond_9

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/TextureVideoView;->a()V

    .line 213
    :cond_9
    return-object v1

    .line 109
    :cond_a
    const v0, 0x7f0300cf

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    goto/16 :goto_0

    .line 122
    :pswitch_1
    const v0, 0x7f0300ce

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 123
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/TextureVideoView;

    iput-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    .line 124
    iget-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v1, v5}, Lcom/dropbox/android/widget/TextureVideoView;->setLooping(Z)V

    .line 125
    iget-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/TextureVideoView;->setVideoUri(Landroid/net/Uri;)V

    .line 127
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->d:Landroid/widget/ImageView;

    .line 128
    iput-boolean v5, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    move-object v1, v0

    .line 129
    goto/16 :goto_1

    .line 131
    :pswitch_2
    const v0, 0x7f0300cd

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 132
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/TextureVideoView;

    iput-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    .line 133
    iget-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    invoke-virtual {v1, v5}, Lcom/dropbox/android/widget/TextureVideoView;->setLooping(Z)V

    .line 134
    iget-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/high16 v3, 0x7f060000

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/TextureVideoView;->setVideoUri(Landroid/net/Uri;)V

    .line 136
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/dropbox/android/activity/IntroTourFragment;->d:Landroid/widget/ImageView;

    .line 137
    iput-boolean v5, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    move-object v1, v0

    .line 138
    goto/16 :goto_1

    .line 140
    :pswitch_3
    if-eqz v0, :cond_b

    .line 141
    const v0, 0x7f0300d2

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    .line 146
    :goto_3
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/TextureVideoView;

    iput-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->c:Lcom/dropbox/android/widget/TextureVideoView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/activity/IntroTourFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/TextureVideoView;->setVideoUri(Landroid/net/Uri;)V

    .line 149
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/IntroTourFragment;->d:Landroid/widget/ImageView;

    .line 150
    iput-boolean v5, p0, Lcom/dropbox/android/activity/IntroTourFragment;->e:Z

    goto/16 :goto_1

    .line 143
    :cond_b
    const v0, 0x7f0300d1

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    goto :goto_3

    .line 204
    :cond_c
    const-string v0, "image"

    goto/16 :goto_2

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
