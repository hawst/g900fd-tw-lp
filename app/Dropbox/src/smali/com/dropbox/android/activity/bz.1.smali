.class final Lcom/dropbox/android/activity/bz;
.super Landroid/widget/ArrayAdapter;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/dropbox/android/activity/bB;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field b:[Lcom/dropbox/android/activity/bB;

.field final synthetic c:Lcom/dropbox/android/activity/GandalfDebugActivity;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/GandalfDebugActivity;Landroid/content/Context;I[Lcom/dropbox/android/activity/bB;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/dropbox/android/activity/bz;->c:Lcom/dropbox/android/activity/GandalfDebugActivity;

    .line 45
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 46
    iput p3, p0, Lcom/dropbox/android/activity/bz;->a:I

    .line 47
    iput-object p4, p0, Lcom/dropbox/android/activity/bz;->b:[Lcom/dropbox/android/activity/bB;

    .line 48
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 52
    iget-object v0, p0, Lcom/dropbox/android/activity/bz;->c:Lcom/dropbox/android/activity/GandalfDebugActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GandalfDebugActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 53
    iget v2, p0, Lcom/dropbox/android/activity/bz;->a:I

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 54
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 56
    iget-object v0, p0, Lcom/dropbox/android/activity/bz;->b:[Lcom/dropbox/android/activity/bB;

    aget-object v3, v0, p1

    .line 58
    const v0, 0x7f0700c1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 59
    iget-object v4, v3, Lcom/dropbox/android/activity/bB;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    const v0, 0x7f0700c2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 63
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 64
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 65
    iget-object v4, p0, Lcom/dropbox/android/activity/bz;->c:Lcom/dropbox/android/activity/GandalfDebugActivity;

    iget-object v4, v4, Lcom/dropbox/android/activity/GandalfDebugActivity;->a:Lcom/dropbox/android/util/analytics/r;

    iget-object v5, v3, Lcom/dropbox/android/activity/bB;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 66
    :goto_0
    iget-object v5, v3, Lcom/dropbox/android/activity/bB;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_1

    .line 67
    new-instance v5, Landroid/widget/RadioButton;

    iget-object v6, p0, Lcom/dropbox/android/activity/bz;->c:Lcom/dropbox/android/activity/GandalfDebugActivity;

    invoke-virtual {v6}, Lcom/dropbox/android/activity/GandalfDebugActivity;->l()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 68
    invoke-virtual {v5, v1}, Landroid/widget/RadioButton;->setId(I)V

    .line 69
    iget-object v6, v3, Lcom/dropbox/android/activity/bB;->b:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v6, p0, Lcom/dropbox/android/activity/bz;->c:Lcom/dropbox/android/activity/GandalfDebugActivity;

    invoke-virtual {v6}, Lcom/dropbox/android/activity/GandalfDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08002f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/RadioButton;->setTextColor(I)V

    .line 71
    iget-object v6, v3, Lcom/dropbox/android/activity/bB;->b:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 72
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 74
    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    :cond_1
    new-instance v1, Lcom/dropbox/android/activity/bA;

    invoke-direct {v1, p0, v3}, Lcom/dropbox/android/activity/bA;-><init>(Lcom/dropbox/android/activity/bz;Lcom/dropbox/android/activity/bB;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 84
    return-object v2
.end method
