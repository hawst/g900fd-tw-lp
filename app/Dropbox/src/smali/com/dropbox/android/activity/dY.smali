.class final Lcom/dropbox/android/activity/dY;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/dropbox/android/activity/dY;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 262
    const-string v0, "photos_and_videos"

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 263
    iget-object v1, p0, Lcom/dropbox/android/activity/dY;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/PrefsActivity;->b(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->s()Lcom/dropbox/android/service/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/service/i;->e(Z)V

    .line 264
    iget-object v1, p0, Lcom/dropbox/android/activity/dY;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/PrefsActivity;->b(Lcom/dropbox/android/activity/PrefsActivity;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 265
    iget-object v2, p0, Lcom/dropbox/android/activity/dY;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v2, v1}, Lcom/dropbox/android/activity/PrefsActivity;->a(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/db231222/n/P;)V

    .line 266
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->l()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "cameraupload.enablevideouploads"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 267
    const/4 v0, 0x1

    return v0
.end method
