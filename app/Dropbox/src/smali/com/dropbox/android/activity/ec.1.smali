.class final Lcom/dropbox/android/activity/ec;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/PrefsActivity;

.field final synthetic b:Lcom/dropbox/android/activity/eb;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/eb;Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/dropbox/android/activity/ec;->b:Lcom/dropbox/android/activity/eb;

    iput-object p2, p0, Lcom/dropbox/android/activity/ec;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 652
    packed-switch p2, :pswitch_data_0

    .line 663
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 654
    :pswitch_0
    sget-object v0, Lcom/dropbox/android/util/ar;->a:Lcom/dropbox/android/util/ar;

    .line 665
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/ec;->b:Lcom/dropbox/android/activity/eb;

    iget-object v2, p0, Lcom/dropbox/android/activity/ec;->a:Lcom/dropbox/android/activity/PrefsActivity;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Lcom/dropbox/android/util/ar;->a(Landroid/app/Activity;ZZ)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    .line 667
    sget-object v1, Lcom/dropbox/android/util/ar;->a:Lcom/dropbox/android/util/ar;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ar;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bc()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 673
    :goto_1
    return-void

    .line 657
    :pswitch_1
    sget-object v0, Lcom/dropbox/android/util/ar;->b:Lcom/dropbox/android/util/ar;

    goto :goto_0

    .line 660
    :pswitch_2
    sget-object v0, Lcom/dropbox/android/util/ar;->c:Lcom/dropbox/android/util/ar;

    goto :goto_0

    .line 671
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bd()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_1

    .line 652
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
