.class final Lcom/dropbox/android/activity/payment/j;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ldbxyzptlk/db231222/r/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/dropbox/android/activity/payment/j;->a:Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/r/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;",
            "Ldbxyzptlk/db231222/r/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/c;->a()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    iget-object v1, p0, Lcom/dropbox/android/activity/payment/j;->a:Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    invoke-static {v1, v0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a(Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;Ldbxyzptlk/db231222/s/a;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/j;->a:Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/BaseActivity;->setSupportProgressBarIndeterminateVisibility(Z)V

    .line 59
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v1, Lcom/dropbox/android/activity/payment/k;

    iget-object v0, p0, Lcom/dropbox/android/activity/payment/j;->a:Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/payment/j;->a:Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;->a(Lcom/dropbox/android/activity/payment/PaymentPlanDetailsFragment;)Lcom/dropbox/android/service/a;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    sget-object v4, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/dropbox/android/activity/payment/k;-><init>(Lcom/dropbox/android/activity/base/BaseActivity;Lcom/dropbox/android/service/a;Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)V

    return-object v1

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown loader: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p2, Ldbxyzptlk/db231222/r/c;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/payment/j;->a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/r/c;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    return-void
.end method
