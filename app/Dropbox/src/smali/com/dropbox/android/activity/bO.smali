.class final Lcom/dropbox/android/activity/bO;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

.field private b:Z

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method private constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 745
    iput-object p1, p0, Lcom/dropbox/android/activity/bO;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 747
    iput-boolean v0, p0, Lcom/dropbox/android/activity/bO;->b:Z

    .line 748
    iput v0, p0, Lcom/dropbox/android/activity/bO;->c:I

    .line 749
    iput v0, p0, Lcom/dropbox/android/activity/bO;->d:I

    .line 750
    iput v0, p0, Lcom/dropbox/android/activity/bO;->e:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Lcom/dropbox/android/activity/bI;)V
    .locals 0

    .prologue
    .line 745
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/bO;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 767
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    iget v1, p0, Lcom/dropbox/android/activity/bO;->c:I

    iget v2, p0, Lcom/dropbox/android/activity/bO;->d:I

    iget v3, p0, Lcom/dropbox/android/activity/bO;->e:I

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;III)V

    .line 768
    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 755
    iget v0, p0, Lcom/dropbox/android/activity/bO;->c:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/activity/bO;->d:I

    if-ne p3, v0, :cond_0

    iget v0, p0, Lcom/dropbox/android/activity/bO;->e:I

    if-eq p4, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 757
    :goto_0
    iput p2, p0, Lcom/dropbox/android/activity/bO;->c:I

    .line 758
    iput p3, p0, Lcom/dropbox/android/activity/bO;->d:I

    .line 759
    iput p4, p0, Lcom/dropbox/android/activity/bO;->e:I

    .line 761
    iget-boolean v1, p0, Lcom/dropbox/android/activity/bO;->b:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 762
    invoke-direct {p0}, Lcom/dropbox/android/activity/bO;->a()V

    .line 764
    :cond_1
    return-void

    .line 755
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 772
    iget-boolean v1, p0, Lcom/dropbox/android/activity/bO;->b:Z

    .line 773
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/activity/bO;->b:Z

    .line 774
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/dropbox/android/activity/bO;->b:Z

    if-nez v0, :cond_0

    .line 776
    invoke-direct {p0}, Lcom/dropbox/android/activity/bO;->a()V

    .line 778
    :cond_0
    return-void

    .line 773
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
