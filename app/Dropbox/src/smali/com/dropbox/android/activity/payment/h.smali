.class public final enum Lcom/dropbox/android/activity/payment/h;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/payment/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/payment/h;

.field public static final enum b:Lcom/dropbox/android/activity/payment/h;

.field public static final enum c:Lcom/dropbox/android/activity/payment/h;

.field public static final enum d:Lcom/dropbox/android/activity/payment/h;

.field public static final enum e:Lcom/dropbox/android/activity/payment/h;

.field public static final enum f:Lcom/dropbox/android/activity/payment/h;

.field public static final enum g:Lcom/dropbox/android/activity/payment/h;

.field public static final enum h:Lcom/dropbox/android/activity/payment/h;

.field public static final enum i:Lcom/dropbox/android/activity/payment/h;

.field public static final enum j:Lcom/dropbox/android/activity/payment/h;

.field public static final enum k:Lcom/dropbox/android/activity/payment/h;

.field public static final enum l:Lcom/dropbox/android/activity/payment/h;

.field public static final enum m:Lcom/dropbox/android/activity/payment/h;

.field public static final enum n:Lcom/dropbox/android/activity/payment/h;

.field private static final synthetic p:[Lcom/dropbox/android/activity/payment/h;


# instance fields
.field private o:Lcom/dropbox/android/util/bz;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 311
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "SETTINGS_UPGRADE_BUTTON"

    sget-object v2, Lcom/dropbox/android/util/bz;->h:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->a:Lcom/dropbox/android/activity/payment/h;

    .line 312
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "SETTINGS_SPACE_BUTTON"

    sget-object v2, Lcom/dropbox/android/util/bz;->i:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v5, v2}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->b:Lcom/dropbox/android/activity/payment/h;

    .line 313
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "CONTEXT_MENU"

    sget-object v2, Lcom/dropbox/android/util/bz;->j:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v6, v2}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->c:Lcom/dropbox/android/activity/payment/h;

    .line 314
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_CAMERA_UPLOAD"

    sget-object v2, Lcom/dropbox/android/util/bz;->k:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v7, v2}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->d:Lcom/dropbox/android/activity/payment/h;

    .line 315
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_MANUAL_UPLOAD"

    sget-object v2, Lcom/dropbox/android/util/bz;->l:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v8, v2}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->e:Lcom/dropbox/android/activity/payment/h;

    .line 316
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_MOVE_FILE"

    const/4 v2, 0x5

    sget-object v3, Lcom/dropbox/android/util/bz;->m:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->f:Lcom/dropbox/android/activity/payment/h;

    .line 317
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_MOVE_FOLDER"

    const/4 v2, 0x6

    sget-object v3, Lcom/dropbox/android/util/bz;->n:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->g:Lcom/dropbox/android/activity/payment/h;

    .line 318
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_RENAME_FILE"

    const/4 v2, 0x7

    sget-object v3, Lcom/dropbox/android/util/bz;->o:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->h:Lcom/dropbox/android/activity/payment/h;

    .line 319
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_RENAME_FOLDER"

    const/16 v2, 0x8

    sget-object v3, Lcom/dropbox/android/util/bz;->p:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->i:Lcom/dropbox/android/activity/payment/h;

    .line 320
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_NEW_FOLDER"

    const/16 v2, 0x9

    sget-object v3, Lcom/dropbox/android/util/bz;->q:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->j:Lcom/dropbox/android/activity/payment/h;

    .line 321
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "OVER_QUOTA_SHARED_FOLDER"

    const/16 v2, 0xa

    sget-object v3, Lcom/dropbox/android/util/bz;->r:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->k:Lcom/dropbox/android/activity/payment/h;

    .line 322
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "NOTIFICATION_UPGRADE_BUTTON"

    const/16 v2, 0xb

    sget-object v3, Lcom/dropbox/android/util/bz;->s:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->l:Lcom/dropbox/android/activity/payment/h;

    .line 323
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "NOTIFICATION_DEAL_EXPIRATION_UPGRADE_BUTTON"

    const/16 v2, 0xc

    sget-object v3, Lcom/dropbox/android/util/bz;->t:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->m:Lcom/dropbox/android/activity/payment/h;

    .line 324
    new-instance v0, Lcom/dropbox/android/activity/payment/h;

    const-string v1, "NOTIFICATION_BACKGROUND"

    const/16 v2, 0xd

    sget-object v3, Lcom/dropbox/android/util/bz;->u:Lcom/dropbox/android/util/bz;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/payment/h;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->n:Lcom/dropbox/android/activity/payment/h;

    .line 310
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/dropbox/android/activity/payment/h;

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->a:Lcom/dropbox/android/activity/payment/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->b:Lcom/dropbox/android/activity/payment/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->c:Lcom/dropbox/android/activity/payment/h;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->d:Lcom/dropbox/android/activity/payment/h;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/android/activity/payment/h;->e:Lcom/dropbox/android/activity/payment/h;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->f:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->g:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->h:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->i:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->j:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->k:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->l:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->m:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dropbox/android/activity/payment/h;->n:Lcom/dropbox/android/activity/payment/h;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/activity/payment/h;->p:[Lcom/dropbox/android/activity/payment/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/bz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 328
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 329
    iput-object p3, p0, Lcom/dropbox/android/activity/payment/h;->o:Lcom/dropbox/android/util/bz;

    .line 330
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/payment/h;
    .locals 1

    .prologue
    .line 310
    const-class v0, Lcom/dropbox/android/activity/payment/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/payment/h;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/payment/h;
    .locals 1

    .prologue
    .line 310
    sget-object v0, Lcom/dropbox/android/activity/payment/h;->p:[Lcom/dropbox/android/activity/payment/h;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/payment/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/payment/h;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/util/bz;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/dropbox/android/activity/payment/h;->o:Lcom/dropbox/android/util/bz;

    return-object v0
.end method
