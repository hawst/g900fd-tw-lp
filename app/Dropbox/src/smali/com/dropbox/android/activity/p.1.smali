.class final Lcom/dropbox/android/activity/p;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/dropbox/android/activity/p;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    iput-object p6, p0, Lcom/dropbox/android/activity/p;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/p;->a(Lcom/dropbox/android/albums/u;Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/albums/u;Ljava/lang/Void;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/dropbox/android/activity/p;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/p;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d02a4

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->a(I)V

    .line 205
    return-void
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/p;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 195
    return-void
.end method
