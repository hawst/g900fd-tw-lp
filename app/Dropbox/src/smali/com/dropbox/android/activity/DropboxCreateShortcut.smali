.class public Lcom/dropbox/android/activity/DropboxCreateShortcut;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 47
    const v0, 0x7f0700b2

    invoke-static {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/support/v4/app/FragmentActivity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onBackPressed()V

    .line 50
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 16
    invoke-static {p0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/app/Activity;)V

    .line 17
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->setContentView(I)V

    .line 24
    const v0, 0x7f0d01da

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->setTitle(I)V

    .line 26
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 27
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    const v0, 0x7f0d0061

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 32
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->finish()V

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    if-nez p1, :cond_0

    .line 38
    invoke-static {}, Lcom/dropbox/android/activity/CreateShortcutFragment;->a()Lcom/dropbox/android/activity/CreateShortcutFragment;

    move-result-object v0

    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 40
    const v2, 0x7f0700b2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 41
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
