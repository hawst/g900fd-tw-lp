.class public Lcom/dropbox/android/activity/DropboxBrowser;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 75
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public final a_()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxBrowser;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 47
    const-class v1, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 51
    const-string v1, "EXTRA_DONT_CLEAR_FLAGS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 55
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/e;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    invoke-static {p0, v0, v2}, Lcom/dropbox/android/activity/LoginChoiceActivity;->a(Lcom/dropbox/android/activity/base/BaseActivity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 57
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxBrowser;->startActivity(Landroid/content/Intent;)V

    .line 61
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxBrowser;->finish()V

    .line 62
    return-void

    .line 59
    :cond_1
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxBrowser;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
