.class public final enum Lcom/dropbox/android/activity/ce;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/ce;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/ce;

.field public static final enum b:Lcom/dropbox/android/activity/ce;

.field private static final synthetic c:[Lcom/dropbox/android/activity/ce;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/dropbox/android/activity/ce;

    const-string v1, "IMPORT_FILES"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/ce;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    .line 49
    new-instance v0, Lcom/dropbox/android/activity/ce;

    const-string v1, "EXPORT_TO_FOLDER"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/ce;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/ce;->b:Lcom/dropbox/android/activity/ce;

    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/activity/ce;

    sget-object v1, Lcom/dropbox/android/activity/ce;->a:Lcom/dropbox/android/activity/ce;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/ce;->b:Lcom/dropbox/android/activity/ce;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/activity/ce;->c:[Lcom/dropbox/android/activity/ce;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/ce;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/dropbox/android/activity/ce;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ce;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/ce;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/dropbox/android/activity/ce;->c:[Lcom/dropbox/android/activity/ce;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/ce;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/ce;

    return-object v0
.end method
