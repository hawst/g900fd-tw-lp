.class final enum Lcom/dropbox/android/activity/bs;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/bs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/bs;

.field public static final enum b:Lcom/dropbox/android/activity/bs;

.field private static final synthetic c:[Lcom/dropbox/android/activity/bs;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 691
    new-instance v0, Lcom/dropbox/android/activity/bs;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/bs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    .line 692
    new-instance v0, Lcom/dropbox/android/activity/bs;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/bs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/bs;->b:Lcom/dropbox/android/activity/bs;

    .line 690
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/activity/bs;

    sget-object v1, Lcom/dropbox/android/activity/bs;->a:Lcom/dropbox/android/activity/bs;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/bs;->b:Lcom/dropbox/android/activity/bs;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/activity/bs;->c:[Lcom/dropbox/android/activity/bs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 690
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bs;
    .locals 1

    .prologue
    .line 690
    const-class v0, Lcom/dropbox/android/activity/bs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/bs;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/bs;
    .locals 1

    .prologue
    .line 690
    sget-object v0, Lcom/dropbox/android/activity/bs;->c:[Lcom/dropbox/android/activity/bs;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/bs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/bs;

    return-object v0
.end method
