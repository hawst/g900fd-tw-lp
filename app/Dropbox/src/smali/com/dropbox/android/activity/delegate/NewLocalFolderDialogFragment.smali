.class public Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;
.super Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback",
        "<",
        "Lcom/dropbox/android/activity/delegate/l;",
        ">;"
    }
.end annotation


# static fields
.field private static c:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "root"

    sput-object v0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback;-><init>()V

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a(Landroid/net/Uri;)Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;-><init>()V

    .line 40
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 41
    sget-object v2, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 42
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;Ljava/lang/String;)Lcom/dropbox/android/activity/delegate/j;
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/delegate/j;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/dropbox/android/activity/delegate/j;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 124
    invoke-static {p1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    new-instance v0, Lcom/dropbox/android/activity/delegate/j;

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->c:Lcom/dropbox/android/activity/delegate/k;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/delegate/j;-><init>(Lcom/dropbox/android/activity/delegate/k;Landroid/net/Uri;)V

    .line 135
    :goto_0
    return-object v0

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 128
    new-instance v1, Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 129
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130
    new-instance v0, Lcom/dropbox/android/activity/delegate/j;

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->b:Lcom/dropbox/android/activity/delegate/k;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/delegate/j;-><init>(Lcom/dropbox/android/activity/delegate/k;Landroid/net/Uri;)V

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 132
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 133
    new-instance v0, Lcom/dropbox/android/activity/delegate/j;

    sget-object v2, Lcom/dropbox/android/activity/delegate/k;->a:Lcom/dropbox/android/activity/delegate/k;

    invoke-direct {v0, v2, v1}, Lcom/dropbox/android/activity/delegate/j;-><init>(Lcom/dropbox/android/activity/delegate/k;Landroid/net/Uri;)V

    goto :goto_0

    .line 135
    :cond_2
    new-instance v0, Lcom/dropbox/android/activity/delegate/j;

    sget-object v1, Lcom/dropbox/android/activity/delegate/k;->c:Lcom/dropbox/android/activity/delegate/k;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/delegate/j;-><init>(Lcom/dropbox/android/activity/delegate/k;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/dropbox/android/activity/delegate/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    const-class v0, Lcom/dropbox/android/activity/delegate/l;

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 48
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    const v0, 0x7f0d00b0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 50
    const v0, 0x7f0d00b1

    new-instance v2, Lcom/dropbox/android/activity/delegate/h;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/delegate/h;-><init>(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    const v0, 0x7f0d0013

    invoke-virtual {v1, v0, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03005e

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 67
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 69
    const v0, 0x7f070114

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->b:Landroid/widget/EditText;

    .line 70
    const v0, 0x7f070115

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 71
    const v0, 0x7f070116

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 73
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->b:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/delegate/i;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/activity/delegate/i;-><init>(Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 90
    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragmentWCallback;->onStart()V

    .line 99
    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v0, p0, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 102
    :cond_0
    return-void

    .line 100
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
