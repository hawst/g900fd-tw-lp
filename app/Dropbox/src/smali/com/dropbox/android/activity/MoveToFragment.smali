.class public Lcom/dropbox/android/activity/MoveToFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dialog/p;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/ImageButton;

.field private g:Z

.field private final i:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/dropbox/android/activity/MoveToFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/MoveToFragment;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 140
    new-instance v0, Lcom/dropbox/android/activity/cN;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/cN;-><init>(Lcom/dropbox/android/activity/MoveToFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->i:Landroid/view/View$OnClickListener;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->g:Z

    .line 46
    return-void
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/MoveToFragment;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lcom/dropbox/android/activity/MoveToFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/MoveToFragment;-><init>()V

    .line 50
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    const-string v2, "ARG_HIDE_QUICKACTIONS"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 52
    const-string v2, "ARG_LOCAL_ENTRY"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 53
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/MoveToFragment;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/MoveToFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveToFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/MoveToFragment;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/MoveToFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveToFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/MoveToFragment;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveToFragment;->t()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/dropbox/android/activity/MoveToFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method private u()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 102
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 107
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 108
    return-void
.end method


# virtual methods
.method public final b()Lcom/dropbox/android/widget/as;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/dropbox/android/widget/as;->b:Lcom/dropbox/android/widget/as;

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 127
    const v0, 0x7f030032

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->g:Z

    .line 112
    invoke-direct {p0}, Lcom/dropbox/android/activity/MoveToFragment;->v()V

    .line 113
    return-void
.end method

.method protected final o()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->g:Z

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o()Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 72
    new-instance v0, Lcom/dropbox/android/activity/cL;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/cL;-><init>(Lcom/dropbox/android/activity/MoveToFragment;)V

    .line 78
    iget-object v1, p0, Lcom/dropbox/android/activity/MoveToFragment;->e:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->e:Landroid/widget/Button;

    const v1, 0x7f0d0013

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->d:Landroid/widget/Button;

    const v1, 0x7f0d01e8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/dropbox/android/activity/MoveToFragment;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 85
    new-instance v0, Lcom/dropbox/android/activity/cM;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/cM;-><init>(Lcom/dropbox/android/activity/MoveToFragment;)V

    .line 94
    iget-object v1, p0, Lcom/dropbox/android/activity/MoveToFragment;->f:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 61
    const v0, 0x7f07004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->e:Landroid/widget/Button;

    .line 62
    const v0, 0x7f07004d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->d:Landroid/widget/Button;

    .line 63
    const v0, 0x7f0700ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->f:Landroid/widget/ImageButton;

    .line 65
    return-object v1
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/MoveToFragment;->g:Z

    .line 117
    invoke-direct {p0}, Lcom/dropbox/android/activity/MoveToFragment;->u()V

    .line 118
    return-void
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MoveToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 304
    return-void
.end method
