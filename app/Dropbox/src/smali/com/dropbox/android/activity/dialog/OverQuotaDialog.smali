.class public Lcom/dropbox/android/activity/dialog/OverQuotaDialog;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    .line 89
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/dialog/o;Lcom/dropbox/android/activity/dialog/p;Z)Lcom/dropbox/android/activity/dialog/OverQuotaDialog;
    .locals 3

    .prologue
    .line 107
    new-instance v0, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;-><init>()V

    .line 108
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 109
    const-string v2, "ARG_OPERATION"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 110
    const-string v2, "ARG_IS_FREE_USER"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 111
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->setArguments(Landroid/os/Bundle;)V

    .line 112
    check-cast p1, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 113
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_OPERATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/o;

    .line 119
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_IS_FREE_USER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 120
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/OverQuotaDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 122
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/o;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 123
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/o;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 125
    const v3, 0x7f0d0013

    new-instance v4, Lcom/dropbox/android/activity/dialog/m;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/dialog/m;-><init>(Lcom/dropbox/android/activity/dialog/OverQuotaDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    const v3, 0x7f0d0069

    new-instance v4, Lcom/dropbox/android/activity/dialog/n;

    invoke-direct {v4, p0, v0, v1}, Lcom/dropbox/android/activity/dialog/n;-><init>(Lcom/dropbox/android/activity/dialog/OverQuotaDialog;Lcom/dropbox/android/activity/dialog/o;Z)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 141
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
