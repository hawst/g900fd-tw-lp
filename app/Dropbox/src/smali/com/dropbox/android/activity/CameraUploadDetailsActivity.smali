.class public Lcom/dropbox/android/activity/CameraUploadDetailsActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# instance fields
.field private a:Z

.field private b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;-><init>()V

    .line 79
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 81
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    .line 85
    :cond_0
    const v2, 0x7f0700b2

    const-string v3, "details"

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 86
    if-eqz p1, :cond_1

    .line 87
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 56
    iget-boolean v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->c:Lcom/dropbox/android/activity/base/d;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iput-boolean v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 59
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 67
    iput-boolean v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 68
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "details"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    .line 44
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 46
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->e()V

    .line 47
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->finish()V

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 30
    const v0, 0x7f0d0180

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->setTitle(I)V

    .line 32
    if-nez p1, :cond_0

    .line 33
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a(Z)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onResume()V

    .line 52
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->e()V

    .line 53
    return-void
.end method
