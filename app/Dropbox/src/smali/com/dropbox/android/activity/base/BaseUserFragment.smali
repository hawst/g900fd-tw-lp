.class public Lcom/dropbox/android/activity/base/BaseUserFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 10
    new-instance v0, Lcom/dropbox/android/activity/base/k;

    invoke-direct {v0}, Lcom/dropbox/android/activity/base/k;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserFragment;->a:Lcom/dropbox/android/activity/base/k;

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 14
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 15
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserFragment;->a:Lcom/dropbox/android/activity/base/k;

    check-cast p1, Lcom/dropbox/android/activity/base/BaseUserActivity;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;)V

    .line 16
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/BaseUserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BaseUserActivity;

    .line 22
    iget-object v1, p0, Lcom/dropbox/android/activity/base/BaseUserFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v1, v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Lcom/dropbox/android/activity/base/BaseUserActivity;Landroid/os/Bundle;)V

    .line 23
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 28
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/base/k;->a(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method protected final t()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/dropbox/android/activity/base/BaseUserFragment;->a:Lcom/dropbox/android/activity/base/k;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/k;->a()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method
