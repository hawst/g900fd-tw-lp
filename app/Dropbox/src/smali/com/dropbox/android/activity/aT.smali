.class final Lcom/dropbox/android/activity/aT;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 575
    iput-object p1, p0, Lcom/dropbox/android/activity/aT;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/dropbox/android/activity/aT;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    if-nez v0, :cond_0

    .line 588
    :goto_0
    return-void

    .line 583
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 584
    iget-object v1, p0, Lcom/dropbox/android/activity/aT;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/GalleryActivity;->d(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/widget/GalleryView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->e()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    new-instance v1, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;

    invoke-direct {v1, v0}, Lcom/dropbox/android/activity/GalleryActivity$LightweightSharePickerSpec;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    move-result-object v0

    .line 587
    iget-object v1, p0, Lcom/dropbox/android/activity/aT;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method
