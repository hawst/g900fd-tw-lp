.class final Lcom/dropbox/android/activity/dM;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/dropbox/android/activity/dM;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 345
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/activity/dM;->a:Lcom/dropbox/android/activity/PrefsActivity;

    const-class v2, Lcom/dropbox/android/activity/QrAuthActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 346
    const-string v1, "com.dropbox.intent.extra.QR_LAUNCH_SOURCE"

    const-string v2, "Prefs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    iget-object v1, p0, Lcom/dropbox/android/activity/dM;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/PrefsActivity;->startActivity(Landroid/content/Intent;)V

    .line 348
    const/4 v0, 0x1

    return v0
.end method
