.class final Lcom/dropbox/android/activity/fW;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/z/aF;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ldbxyzptlk/db231222/z/M;

.field private c:Lcom/dropbox/android/activity/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ldbxyzptlk/db231222/z/M;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 466
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 467
    iput-object p3, p0, Lcom/dropbox/android/activity/fW;->a:Ljava/lang/String;

    .line 468
    iput-object p2, p0, Lcom/dropbox/android/activity/fW;->b:Ldbxyzptlk/db231222/z/M;

    .line 469
    iput-object p1, p0, Lcom/dropbox/android/activity/fW;->c:Lcom/dropbox/android/activity/VideoPlayerActivity;

    .line 470
    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ldbxyzptlk/db231222/z/aF;
    .locals 3

    .prologue
    .line 483
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetching "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/fW;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/dropbox/android/activity/fW;->b:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/activity/fW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/z/aF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 498
    :goto_0
    return-object v0

    .line 486
    :catch_0
    move-exception v0

    .line 487
    invoke-static {}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdjustMetricsAsyncTask"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 488
    instance-of v1, v0, Ldbxyzptlk/db231222/w/a;

    if-nez v1, :cond_0

    .line 495
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    const-string v2, "AdjustMetricsAsyncTask"

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 498
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 477
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/fW;->c:Lcom/dropbox/android/activity/VideoPlayerActivity;

    .line 478
    return-void
.end method

.method protected final a(Ldbxyzptlk/db231222/z/aF;)V
    .locals 5

    .prologue
    .line 503
    if-nez p1, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 506
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/fW;->c:Lcom/dropbox/android/activity/VideoPlayerActivity;

    .line 507
    if-eqz v0, :cond_0

    .line 508
    const-wide v1, 0x408f400000000000L    # 1000.0

    iget-wide v3, p1, Ldbxyzptlk/db231222/z/aF;->f:D

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;I)V

    .line 509
    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;

    move-result-object v1

    iget-wide v2, p1, Ldbxyzptlk/db231222/z/aF;->d:J

    long-to-int v2, v2

    iget-wide v3, p1, Ldbxyzptlk/db231222/z/aF;->e:J

    long-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/widget/DbxVideoView;->a(II)V

    .line 514
    iget-wide v1, p1, Ldbxyzptlk/db231222/z/aF;->a:D

    iget-wide v3, p1, Ldbxyzptlk/db231222/z/aF;->f:D

    sub-double/2addr v1, v3

    const-wide/high16 v3, 0x4014000000000000L    # 5.0

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_0

    .line 515
    const v1, 0x7f0d0278

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 458
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/fW;->a([Ljava/lang/Void;)Ldbxyzptlk/db231222/z/aF;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 458
    check-cast p1, Ldbxyzptlk/db231222/z/aF;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/fW;->a(Ldbxyzptlk/db231222/z/aF;)V

    return-void
.end method
