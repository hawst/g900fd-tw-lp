.class public final enum Lcom/dropbox/android/activity/er;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/activity/er;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/er;

.field public static final enum b:Lcom/dropbox/android/activity/er;

.field public static final enum c:Lcom/dropbox/android/activity/er;

.field public static final enum d:Lcom/dropbox/android/activity/er;

.field public static final enum e:Lcom/dropbox/android/activity/er;

.field private static final synthetic j:[Lcom/dropbox/android/activity/er;


# instance fields
.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 59
    new-instance v0, Lcom/dropbox/android/activity/er;

    const-string v1, "NEAR_COMPUTER"

    const v4, 0x7f0d0315

    const v5, 0x7f020211

    const v6, 0x7f03007b

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/er;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/dropbox/android/activity/er;->a:Lcom/dropbox/android/activity/er;

    .line 64
    new-instance v4, Lcom/dropbox/android/activity/er;

    const-string v5, "START"

    const v7, 0x7f0d0323

    const v8, 0x7f0d0316

    const v9, 0x7f020212

    const v10, 0x7f03007e

    move v6, v11

    invoke-direct/range {v4 .. v10}, Lcom/dropbox/android/activity/er;-><init>(Ljava/lang/String;IIIII)V

    sput-object v4, Lcom/dropbox/android/activity/er;->b:Lcom/dropbox/android/activity/er;

    .line 69
    new-instance v4, Lcom/dropbox/android/activity/er;

    const-string v5, "SIGN_IN"

    const v7, 0x7f0d0324

    const v8, 0x7f0d0319

    const v10, 0x7f03007d

    move v6, v12

    move v9, v3

    invoke-direct/range {v4 .. v10}, Lcom/dropbox/android/activity/er;-><init>(Ljava/lang/String;IIIII)V

    sput-object v4, Lcom/dropbox/android/activity/er;->c:Lcom/dropbox/android/activity/er;

    .line 74
    new-instance v3, Lcom/dropbox/android/activity/er;

    const-string v4, "INSTALL"

    const v6, 0x7f0d0325

    const v7, 0x7f0d031b

    const v8, 0x7f020210

    const v9, 0x7f03007a

    move v5, v13

    invoke-direct/range {v3 .. v9}, Lcom/dropbox/android/activity/er;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, Lcom/dropbox/android/activity/er;->d:Lcom/dropbox/android/activity/er;

    .line 79
    new-instance v3, Lcom/dropbox/android/activity/er;

    const-string v4, "CONNECTIVITY_ERROR"

    const/4 v5, 0x4

    const v6, 0x7f0d0326

    const v7, 0x7f0d031d

    invoke-static {}, Lcom/dropbox/android/util/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v8, 0x7f02020f

    :goto_0
    const v9, 0x7f030079

    invoke-direct/range {v3 .. v9}, Lcom/dropbox/android/activity/er;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, Lcom/dropbox/android/activity/er;->e:Lcom/dropbox/android/activity/er;

    .line 57
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/activity/er;

    sget-object v1, Lcom/dropbox/android/activity/er;->a:Lcom/dropbox/android/activity/er;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/er;->b:Lcom/dropbox/android/activity/er;

    aput-object v1, v0, v11

    sget-object v1, Lcom/dropbox/android/activity/er;->c:Lcom/dropbox/android/activity/er;

    aput-object v1, v0, v12

    sget-object v1, Lcom/dropbox/android/activity/er;->d:Lcom/dropbox/android/activity/er;

    aput-object v1, v0, v13

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/activity/er;->e:Lcom/dropbox/android/activity/er;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/activity/er;->j:[Lcom/dropbox/android/activity/er;

    return-void

    .line 79
    :cond_0
    const v8, 0x7f02020e

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 92
    iput p3, p0, Lcom/dropbox/android/activity/er;->f:I

    .line 93
    iput p4, p0, Lcom/dropbox/android/activity/er;->g:I

    .line 94
    iput p5, p0, Lcom/dropbox/android/activity/er;->h:I

    .line 95
    iput p6, p0, Lcom/dropbox/android/activity/er;->i:I

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/er;
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/dropbox/android/activity/er;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/er;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/er;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/dropbox/android/activity/er;->j:[Lcom/dropbox/android/activity/er;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/er;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/er;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/dropbox/android/activity/er;->f:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/dropbox/android/activity/er;->g:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/dropbox/android/activity/er;->h:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/dropbox/android/activity/er;->i:I

    return v0
.end method

.method public final e()Lcom/dropbox/android/activity/er;
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/dropbox/android/activity/er;->ordinal()I

    move-result v0

    sget-object v1, Lcom/dropbox/android/activity/er;->d:Lcom/dropbox/android/activity/er;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/er;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dropbox/android/activity/er;->values()[Lcom/dropbox/android/activity/er;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/er;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method
