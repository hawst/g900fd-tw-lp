.class final Lcom/dropbox/android/activity/q;
.super Lcom/dropbox/android/albums/o;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/albums/o",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/albums/PhotosModel;

.field final synthetic b:Lcom/dropbox/android/activity/AlbumViewFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/AlbumViewFragment;Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;ILcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/dropbox/android/activity/q;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    iput-object p6, p0, Lcom/dropbox/android/activity/q;->a:Lcom/dropbox/android/albums/PhotosModel;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/dropbox/android/albums/o;-><init>(Ljava/lang/String;Lcom/dropbox/android/albums/t;Landroid/support/v4/app/Fragment;I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/dropbox/android/albums/u;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/q;->a(Lcom/dropbox/android/albums/u;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/albums/u;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/dropbox/android/activity/q;->a:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v1, p0, Lcom/dropbox/android/activity/q;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p1}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Ljava/lang/String;Lcom/dropbox/android/albums/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/dropbox/android/activity/fF;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/fF",
            "<",
            "Lcom/dropbox/android/albums/Album;",
            ">;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p1}, Lcom/dropbox/android/activity/fF;->a()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->p:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v1, :cond_0

    .line 218
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d02a7

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->b(I)V

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d02a6

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/bl;->b(I)V

    goto :goto_0
.end method

.method protected final a(Lcom/dropbox/android/albums/Album;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/dropbox/android/activity/q;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->b(Lcom/dropbox/android/activity/AlbumViewFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/q;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Lcom/dropbox/android/activity/q;->b:Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/AlbumViewFragment;->c(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    .line 228
    return-void
.end method
