.class public Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;
.super Lcom/dropbox/android/activity/dialog/PhotoBatchRemoveConfirmDialogFrag;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/dialog/PhotoBatchRemoveConfirmDialogFrag",
        "<",
        "Lcom/dropbox/android/activity/AlbumViewFragment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 783
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/PhotoBatchRemoveConfirmDialogFrag;-><init>()V

    return-void
.end method

.method static a(Lcom/dropbox/android/activity/AlbumViewFragment;II)Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;
    .locals 2

    .prologue
    .line 786
    new-instance v0, Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;-><init>()V

    .line 787
    invoke-virtual {p0}, Lcom/dropbox/android/activity/AlbumViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1, p0, p1, p2}, Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;->a(Landroid/content/res/Resources;Landroid/support/v4/app/Fragment;II)V

    .line 788
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 783
    check-cast p1, Lcom/dropbox/android/activity/AlbumViewFragment;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/AlbumViewFragment$RemoveConfirmFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/AlbumViewFragment;)V
    .locals 4

    .prologue
    .line 793
    invoke-static {p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->p(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/o;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/albums/g;

    invoke-static {p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->a(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/albums/Album;

    move-result-object v2

    invoke-static {p1}, Lcom/dropbox/android/activity/AlbumViewFragment;->m(Lcom/dropbox/android/activity/AlbumViewFragment;)Lcom/dropbox/android/activity/A;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/activity/A;->c()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/albums/g;-><init>(Lcom/dropbox/android/albums/Album;Ljava/util/Collection;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/albums/o;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 795
    return-void
.end method
