.class public Lcom/dropbox/android/activity/GalleryPickerActivity;
.super Lcom/dropbox/android/activity/UploadBaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/dG;


# static fields
.field static final synthetic a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/dropbox/android/activity/GalleryPickerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/android/activity/GalleryPickerActivity;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    sget-boolean v0, Lcom/dropbox/android/activity/GalleryPickerActivity;->a:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 59
    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->a(Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V

    .line 60
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->setResult(I)V

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->finish()V

    .line 53
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UploadBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Launched without an upload path extra"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->finish()V

    .line 46
    :goto_0
    return-void

    .line 31
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->setContentView(I)V

    .line 33
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "GALLERY_PICKER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/GalleryPickerFragment;

    .line 35
    if-nez v0, :cond_2

    .line 36
    new-instance v0, Lcom/dropbox/android/activity/GalleryPickerFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/GalleryPickerFragment;-><init>()V

    .line 37
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryPickerFragment;->setRetainInstance(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 40
    const v2, 0x7f0700b2

    const-string v3, "GALLERY_PICKER"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 41
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 45
    :cond_2
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Lcom/dropbox/android/activity/dG;)V

    goto :goto_0
.end method
