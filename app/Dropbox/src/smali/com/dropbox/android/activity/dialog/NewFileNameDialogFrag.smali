.class public Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# instance fields
.field a:Landroid/content/DialogInterface$OnClickListener;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    .line 52
    new-instance v0, Lcom/dropbox/android/activity/dialog/f;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/f;-><init>(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/text/TextWatcher;

    .line 74
    new-instance v0, Lcom/dropbox/android/activity/dialog/g;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/g;-><init>(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/activity/SendToFragment;)Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;-><init>()V

    .line 23
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 24
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 31
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 32
    const v1, 0x7f0d01f8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 33
    const v1, 0x7f0d01f9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 34
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 36
    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    .line 37
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    const v2, 0x1020003

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setId(I)V

    .line 38
    if-nez p1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    const v2, 0x7f0d01fa

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 40
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setLines(I)V

    .line 41
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 43
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 44
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 46
    const v1, 0x7f0d0014

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 47
    const v1, 0x7f0d0013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 49
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
