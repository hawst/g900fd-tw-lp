.class public Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;
.super Lcom/dropbox/android/activity/base/BaseDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a()Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/dropbox/android/util/bn;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 49
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 50
    const v0, 0x7f0d0062

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 51
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 52
    const v0, 0x7f0d0014

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 59
    invoke-virtual {p0}, Lcom/dropbox/android/activity/ExternalUnlinkFailedActivity$ExternalUnlinkFailedDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 63
    :cond_0
    return-void
.end method
