.class final Lcom/dropbox/android/activity/L;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/ag;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

.field private final b:Ljava/util/Random;

.field private final c:Lcom/dropbox/android/filemanager/X;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Lcom/dropbox/android/taskqueue/ag;

.field private i:[F

.field private j:[I

.field private final k:Landroid/os/Handler;

.field private final l:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 568
    iput-object p1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->b:Ljava/util/Random;

    .line 522
    invoke-static {}, Lcom/dropbox/android/filemanager/X;->a()Lcom/dropbox/android/filemanager/X;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->c:Lcom/dropbox/android/filemanager/X;

    .line 523
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    .line 524
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->e:Landroid/util/SparseArray;

    .line 525
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/activity/L;->f:I

    .line 526
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/L;->g:Z

    .line 530
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->k:Landroid/os/Handler;

    .line 533
    const/high16 v0, 0x41000000    # 8.0f

    iget-object v1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/dropbox/android/activity/L;->l:I

    .line 569
    if-eqz p2, :cond_2

    const-string v0, "saved_rotations"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 570
    const-string v0, "saved_rotations"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->i:[F

    .line 578
    :cond_0
    :goto_0
    if-eqz p2, :cond_3

    const-string v0, "saved_padding_offsets"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 579
    const-string v0, "saved_padding_offsets"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->j:[I

    .line 586
    :cond_1
    :goto_1
    return-void

    .line 572
    :cond_2
    invoke-virtual {p1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 573
    if-eqz v0, :cond_0

    const-string v1, "saved_rotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574
    const-string v1, "saved_rotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->i:[F

    goto :goto_0

    .line 581
    :cond_3
    invoke-virtual {p1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 582
    if-eqz v0, :cond_1

    const-string v1, "saved_padding_offsets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 583
    const-string v1, "saved_padding_offsets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/L;->j:[I

    goto :goto_1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/L;I)I
    .locals 1

    .prologue
    .line 519
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/L;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/L;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->e:Landroid/util/SparseArray;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 697
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 701
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 702
    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 703
    new-instance v1, Lcom/dropbox/android/activity/N;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/N;-><init>(Lcom/dropbox/android/activity/L;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 723
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 725
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 875
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/activity/L;->a(ZI)V

    .line 876
    return-void
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 819
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 820
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 822
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/L;->a(Z)V

    .line 826
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/activity/L;->b()V

    .line 827
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/L;II)V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/L;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/L;Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V
    .locals 0

    .prologue
    .line 519
    invoke-direct/range {p0 .. p7}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/L;Ljava/lang/String;Lcom/dropbox/android/filemanager/ai;IZ)V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/ai;IZ)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 784
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 785
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/L;->b(I)I

    move-result v2

    .line 787
    iget-object v3, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 788
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 789
    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/L;->a(II)V

    .line 815
    :goto_0
    return-void

    .line 791
    :cond_0
    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v0, :cond_1

    :goto_1
    new-instance v1, Lcom/dropbox/android/activity/P;

    invoke-direct {v1, p0, v3, p1}, Lcom/dropbox/android/activity/P;-><init>(Lcom/dropbox/android/activity/L;Landroid/view/View;Ljava/lang/String;)V

    invoke-static {v3, v0, v1}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;ZLandroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 746
    invoke-direct {p0}, Lcom/dropbox/android/activity/L;->a()V

    .line 748
    iget-boolean v0, p0, Lcom/dropbox/android/activity/L;->g:Z

    if-nez v0, :cond_0

    const/4 v5, 0x1

    .line 749
    :goto_0
    iput-boolean v6, p0, Lcom/dropbox/android/activity/L;->g:Z

    .line 751
    if-eqz p7, :cond_1

    .line 752
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, p6}, Lcom/dropbox/android/activity/L;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v1, p2

    move v2, p3

    move-wide v3, p4

    .line 753
    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;ZJZ)V

    .line 780
    :goto_1
    iput-boolean v6, p0, Lcom/dropbox/android/activity/L;->g:Z

    .line 781
    return-void

    :cond_0
    move v5, v6

    .line 748
    goto :goto_0

    .line 755
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x7

    iget v3, p0, Lcom/dropbox/android/activity/L;->l:I

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/widget/r;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 760
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->i:[F

    if-eqz v1, :cond_2

    .line 761
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->i:[F

    aget v1, v1, p6

    .line 766
    :goto_2
    iget-object v2, p0, Lcom/dropbox/android/activity/L;->j:[I

    if-eqz v2, :cond_4

    .line 767
    iget-object v2, p0, Lcom/dropbox/android/activity/L;->j:[I

    mul-int/lit8 v3, p6, 0x2

    aget v2, v2, v3

    .line 768
    iget-object v3, p0, Lcom/dropbox/android/activity/L;->j:[I

    mul-int/lit8 v4, p6, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    .line 773
    :goto_3
    invoke-static {v0, v1}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;F)V

    .line 774
    invoke-static {v0, v2, v3}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;II)V

    move-object v1, p2

    move v2, p3

    move-wide v3, p4

    .line 775
    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;ZJZ)V

    .line 777
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p6, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 778
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-direct {p0, p6}, Lcom/dropbox/android/activity/L;->b(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    goto :goto_1

    .line 763
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->b:Ljava/util/Random;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x7

    int-to-float v1, v1

    goto :goto_2

    .line 770
    :cond_4
    iget-object v2, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/dropbox/android/activity/L;->l:I

    div-int/lit8 v2, v2, 0x2

    .line 771
    :goto_4
    iget-object v3, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/dropbox/android/activity/L;->l:I

    div-int/lit8 v3, v3, 0x2

    goto :goto_3

    .line 770
    :cond_5
    iget v2, p0, Lcom/dropbox/android/activity/L;->l:I

    iget-object v3, p0, Lcom/dropbox/android/activity/L;->b:Ljava/util/Random;

    iget v4, p0, Lcom/dropbox/android/activity/L;->l:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_4

    .line 771
    :cond_6
    iget v3, p0, Lcom/dropbox/android/activity/L;->l:I

    iget-object v4, p0, Lcom/dropbox/android/activity/L;->b:Ljava/util/Random;

    iget v7, p0, Lcom/dropbox/android/activity/L;->l:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v4, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    sub-int/2addr v3, v4

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Lcom/dropbox/android/filemanager/ai;IZ)V
    .locals 8

    .prologue
    .line 728
    if-eqz p2, :cond_0

    .line 729
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p2, Lcom/dropbox/android/filemanager/ai;->a:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-boolean v3, p2, Lcom/dropbox/android/filemanager/ai;->b:Z

    iget-wide v4, p2, Lcom/dropbox/android/filemanager/ai;->c:J

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    .line 743
    :goto_0
    return-void

    .line 736
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 680
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->f(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    const v0, 0x7f02007a

    .line 685
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 686
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 687
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 689
    if-eqz p1, :cond_0

    .line 690
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 691
    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 692
    iget-object v1, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->g(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 694
    :cond_0
    return-void

    .line 683
    :cond_1
    const v0, 0x7f02017f

    goto :goto_0
.end method

.method private a(ZI)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 858
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->h(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 859
    if-eqz p1, :cond_0

    .line 860
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->h(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 862
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 863
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 864
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;Z)V

    .line 863
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 858
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 868
    :cond_2
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/L;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const v2, 0x7f02016a

    .line 830
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    invoke-direct {p0}, Lcom/dropbox/android/activity/L;->c()V

    .line 855
    :goto_0
    return-void

    .line 835
    :cond_0
    sget-object v0, Lcom/dropbox/android/activity/C;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/L;->h:Lcom/dropbox/android/taskqueue/ag;

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/ag;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 852
    invoke-direct {p0}, Lcom/dropbox/android/activity/L;->c()V

    goto :goto_0

    .line 837
    :pswitch_0
    const v0, 0x7f020230

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/L;->a(I)V

    goto :goto_0

    .line 840
    :pswitch_1
    const v0, 0x7f0202ad

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/L;->a(I)V

    goto :goto_0

    .line 843
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/L;->a(I)V

    goto :goto_0

    .line 846
    :pswitch_3
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/L;->a(I)V

    goto :goto_0

    .line 849
    :pswitch_4
    const v0, 0x7f02007b

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/L;->a(I)V

    goto :goto_0

    .line 835
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 871
    invoke-direct {p0, v0, v0}, Lcom/dropbox/android/activity/L;->a(ZI)V

    .line 872
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/L;)V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0}, Lcom/dropbox/android/activity/L;->b()V

    return-void
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/filemanager/ai;)V
    .locals 2

    .prologue
    .line 537
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->k:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/activity/M;

    invoke-direct {v1, p0, p2, p1}, Lcom/dropbox/android/activity/M;-><init>(Lcom/dropbox/android/activity/L;Lcom/dropbox/android/filemanager/ai;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 566
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v2, 0x0

    .line 589
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 594
    new-array v3, v7, [F

    .line 595
    const/16 v0, 0xa

    new-array v4, v0, [I

    move v1, v2

    .line 596
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    if-ge v1, v7, :cond_0

    .line 597
    iget-object v0, p0, Lcom/dropbox/android/activity/L;->a:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/L;->b(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/RotatableFrameLayout;

    .line 598
    invoke-virtual {v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a()F

    move-result v5

    aput v5, v3, v1

    .line 599
    invoke-static {v0}, Lcom/dropbox/android/widget/r;->a(Landroid/view/View;)[I

    move-result-object v0

    .line 600
    mul-int/lit8 v5, v1, 0x2

    aget v6, v0, v2

    aput v6, v4, v5

    .line 601
    mul-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x1

    aget v0, v0, v6

    aput v0, v4, v5

    .line 596
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 603
    :cond_0
    const-string v0, "saved_rotations"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 604
    const-string v0, "saved_padding_offsets"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 606
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONArray;Lcom/dropbox/android/taskqueue/ag;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 609
    if-nez p1, :cond_0

    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 610
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not expecting upcoming camera uploads when there is no current one."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 613
    :cond_0
    iput-object p3, p0, Lcom/dropbox/android/activity/L;->h:Lcom/dropbox/android/taskqueue/ag;

    .line 616
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 617
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 618
    if-eqz p1, :cond_1

    .line 619
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v2

    .line 622
    :goto_0
    :try_start_0
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 623
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->size()I

    move-result v1

    const/4 v5, 0x5

    if-ge v1, v5, :cond_2

    .line 624
    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 622
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626
    :cond_2
    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 629
    :catch_0
    move-exception v0

    .line 630
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 635
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 636
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 637
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 643
    :cond_5
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 646
    iget-object v6, p0, Lcom/dropbox/android/activity/L;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 647
    if-ltz v6, :cond_6

    .line 648
    add-int/lit8 v0, v6, 0x1

    move v1, v0

    .line 649
    goto :goto_3

    .line 652
    :cond_6
    iget v6, p0, Lcom/dropbox/android/activity/L;->f:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/dropbox/android/activity/L;->f:I

    .line 653
    iget-object v6, p0, Lcom/dropbox/android/activity/L;->e:Landroid/util/SparseArray;

    iget v7, p0, Lcom/dropbox/android/activity/L;->f:I

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 654
    iget-object v6, p0, Lcom/dropbox/android/activity/L;->c:Lcom/dropbox/android/filemanager/X;

    iget v7, p0, Lcom/dropbox/android/activity/L;->f:I

    invoke-virtual {v6, v0, v7, v9, p0}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/ag;)Lcom/dropbox/android/filemanager/ai;

    move-result-object v6

    .line 656
    if-eqz v6, :cond_7

    .line 657
    invoke-direct {p0, v0, v6, v1, v2}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/ai;IZ)V

    .line 661
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 662
    goto :goto_3

    .line 659
    :cond_7
    invoke-direct {p0, v0, v8, v1, v2}, Lcom/dropbox/android/activity/L;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/ai;IZ)V

    goto :goto_4

    .line 664
    :cond_8
    iget-boolean v0, p0, Lcom/dropbox/android/activity/L;->g:Z

    if-eqz v0, :cond_9

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 665
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/L;->a(Z)V

    .line 668
    :cond_9
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 669
    iget-object v3, p0, Lcom/dropbox/android/activity/L;->c:Lcom/dropbox/android/filemanager/X;

    invoke-virtual {v3, v0, v9}, Lcom/dropbox/android/filemanager/X;->a(Ljava/lang/String;I)V

    goto :goto_5

    .line 672
    :cond_a
    invoke-direct {p0}, Lcom/dropbox/android/activity/L;->b()V

    .line 673
    iput-boolean v2, p0, Lcom/dropbox/android/activity/L;->g:Z

    .line 674
    iput-object v8, p0, Lcom/dropbox/android/activity/L;->i:[F

    .line 675
    iput-object v8, p0, Lcom/dropbox/android/activity/L;->j:[I

    .line 676
    return-void
.end method
