.class final Lcom/dropbox/android/activity/bc;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .locals 0

    .prologue
    .line 1371
    iput-object p1, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1408
    invoke-static {}, Lcom/dropbox/android/activity/GalleryActivity;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onLoadFinished loader callback called"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    iget-object v0, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0, p2}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Landroid/database/Cursor;)V

    .line 1410
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1375
    sget-object v0, Lcom/dropbox/android/activity/bd;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/GalleryActivity;->i(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/activity/bq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1402
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected gallery type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/GalleryActivity;->i(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/activity/bq;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1379
    :pswitch_0
    new-instance v0, Lcom/dropbox/android/activity/bm;

    iget-object v1, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/GalleryActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/GalleryActivity;->f()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "album_gid"

    invoke-virtual {v3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/activity/bm;-><init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;Ljava/lang/String;)V

    .line 1396
    :goto_0
    return-object v0

    .line 1382
    :pswitch_1
    new-instance v0, Landroid/support/v4/content/F;

    iget-object v1, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/GalleryActivity;->f()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/provider/PhotosProvider;->f:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1386
    :pswitch_2
    iget-object v0, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_HISTORY_ENTRY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry;

    .line 1387
    sget-object v1, Lcom/dropbox/android/activity/bd;->c:[I

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->a()Lcom/dropbox/android/util/al;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/al;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1398
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported history type in gallery: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry;->a()Lcom/dropbox/android/util/al;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1389
    :pswitch_3
    new-instance v1, Lcom/dropbox/android/activity/bn;

    iget-object v2, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->f()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    iget-object v3, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EXTRA_SORT_ORDER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ldbxyzptlk/db231222/n/r;->valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/n/r;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/dropbox/android/activity/bn;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/n/r;)V

    move-object v0, v1

    goto :goto_0

    .line 1393
    :pswitch_4
    new-instance v1, Lcom/dropbox/android/activity/bt;

    iget-object v2, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v3, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v3}, Lcom/dropbox/android/activity/GalleryActivity;->l(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v3

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/dropbox/android/activity/bt;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/provider/MetadataManager;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 1396
    :pswitch_5
    new-instance v0, Lcom/dropbox/android/activity/bp;

    iget-object v1, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/GalleryActivity;->l(Lcom/dropbox/android/activity/GalleryActivity;)Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/bp;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Lcom/dropbox/android/provider/MetadataManager;)V

    goto/16 :goto_0

    .line 1375
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    .line 1387
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1371
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bc;->a(Landroid/support/v4/content/p;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1416
    invoke-static {}, Lcom/dropbox/android/activity/GalleryActivity;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onLoaderReset loader callback called"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    return-void
.end method
