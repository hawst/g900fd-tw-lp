.class final Lcom/dropbox/android/activity/ac;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ldbxyzptlk/db231222/r/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/DropboxActionBarActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/r/c;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;",
            "Ldbxyzptlk/db231222/r/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 343
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/c;->a()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 346
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 348
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)Lcom/dropbox/android/activity/ai;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/ai;->a(Z)V

    .line 350
    :cond_0
    return-void

    .line 347
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 329
    if-ne p1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 330
    iget-object v0, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->c()Z

    move-result v0

    .line 331
    if-eqz v0, :cond_1

    .line 333
    new-instance v0, Ldbxyzptlk/db231222/r/a;

    iget-object v1, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/service/e;->b:Lcom/dropbox/android/service/e;

    sget-object v4, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/r/a;-><init>(Landroid/content/Context;Lcom/dropbox/android/service/a;Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)V

    .line 336
    :goto_1
    return-object v0

    .line 329
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 336
    :cond_1
    new-instance v0, Ldbxyzptlk/db231222/r/a;

    iget-object v1, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/ac;->a:Lcom/dropbox/android/activity/DropboxActionBarActivity;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/service/e;->e:Lcom/dropbox/android/service/e;

    sget-object v4, Lcom/dropbox/android/service/e;->d:Lcom/dropbox/android/service/e;

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/r/a;-><init>(Landroid/content/Context;Lcom/dropbox/android/service/a;Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)V

    goto :goto_1
.end method

.method public final synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 324
    check-cast p2, Ldbxyzptlk/db231222/r/c;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/ac;->a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/r/c;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/r/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 354
    return-void
.end method
