.class final Lcom/dropbox/android/activity/dialog/d;
.super Landroid/support/v4/content/F;
.source "panda.py"


# instance fields
.field private final l:Lcom/dropbox/android/albums/PhotosModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/albums/PhotosModel;)V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0, p1}, Landroid/support/v4/content/F;-><init>(Landroid/content/Context;)V

    .line 162
    iput-object p2, p0, Lcom/dropbox/android/activity/dialog/d;->l:Lcom/dropbox/android/albums/PhotosModel;

    .line 163
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/d;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 167
    const-string v0, "_create_new_album_ui"

    invoke-static {v0}, Lcom/dropbox/android/provider/Y;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 168
    new-instance v1, Lcom/dropbox/android/provider/W;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/d;->l:Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/PhotosModel;->c()Landroid/database/Cursor;

    move-result-object v2

    const-string v3, "_album"

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/provider/W;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 171
    new-instance v2, Landroid/database/MergeCursor;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/database/Cursor;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-direct {v2, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 172
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/dialog/d;->a(Landroid/database/Cursor;)V

    .line 173
    return-object v2
.end method
