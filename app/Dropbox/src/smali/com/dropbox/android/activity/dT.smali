.class final Lcom/dropbox/android/activity/dT;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/dropbox/android/activity/dT;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/activity/dT;->a:Lcom/dropbox/android/activity/PrefsActivity;

    const-class v2, Lcom/dropbox/android/activity/UserPrefsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 180
    const-string v1, "com.dropbox.activity.extra.ROLE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 181
    iget-object v1, p0, Lcom/dropbox/android/activity/dT;->a:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/PrefsActivity;->startActivity(Landroid/content/Intent;)V

    .line 182
    const/4 v0, 0x0

    return v0
.end method
