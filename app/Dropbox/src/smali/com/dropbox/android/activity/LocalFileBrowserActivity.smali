.class public Lcom/dropbox/android/activity/LocalFileBrowserActivity;
.super Lcom/dropbox/android/activity/UploadBaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/cf;
.implements Lcom/dropbox/android/activity/delegate/l;


# static fields
.field static final synthetic a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->c(Landroid/net/Uri;)V

    .line 105
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 100
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 102
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 103
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    goto :goto_0
.end method

.method protected final a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;Landroid/net/Uri;Z)V
    .locals 5

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0, p3}, Ldbxyzptlk/db231222/n/P;->a(Landroid/net/Uri;)V

    .line 145
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(I)V

    .line 146
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    .line 148
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/I;->b(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 149
    if-nez v0, :cond_0

    .line 150
    invoke-static {}, Lcom/dropbox/android/util/bl;->a()Lcom/dropbox/android/util/bl;

    move-result-object v0

    const v1, 0x7f0d00ca

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/bl;->a(I[Ljava/lang/Object;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    invoke-virtual {v1, v0, p2, p4}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    sget-boolean v0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 89
    :cond_1
    invoke-virtual {p0, p2, p1}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a(Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V

    .line 90
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 158
    invoke-static {p1}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a(Landroid/net/Uri;)Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    move-result-object v0

    .line 159
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 160
    return-void
.end method

.method protected final c(Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 108
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 109
    const-string v1, "EXPORT_DB_PROVIDER_URI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 110
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 112
    invoke-static {p1}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 113
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-virtual {p0, v1, v2, p1, v6}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;Landroid/net/Uri;Z)V

    .line 140
    :goto_0
    return-void

    .line 119
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 120
    const v3, 0x7f0d00cc

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 122
    const v3, 0x7f0d00cd

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 123
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 125
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 127
    new-instance v3, Lcom/dropbox/android/activity/cg;

    invoke-direct {v3, p0, v1, v2, p1}, Lcom/dropbox/android/activity/cg;-><init>(Lcom/dropbox/android/activity/LocalFileBrowserActivity;Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;Landroid/net/Uri;)V

    .line 134
    const v1, 0x7f0d00ce

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 135
    const v1, 0x7f0d0013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 138
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public final d(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "FILE_BROWSER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    .line 165
    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/net/Uri;)V

    .line 166
    return-void
.end method

.method public final e_()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(I)V

    .line 83
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    .line 84
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "FILE_BROWSER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    .line 71
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-super {p0}, Lcom/dropbox/android/activity/UploadBaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/UploadBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "FILE_BROWSER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    .line 44
    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 48
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    sget-object v0, Lcom/dropbox/android/activity/ce;->b:Lcom/dropbox/android/activity/ce;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/ce;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 56
    const-string v2, "key_Mode"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;-><init>()V

    .line 59
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->setRetainInstance(Z)V

    .line 62
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 63
    const v2, 0x7f0700b2

    const-string v3, "FILE_BROWSER"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 64
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 51
    :cond_2
    const-string v1, "BROWSE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
