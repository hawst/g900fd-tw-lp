.class public Lcom/dropbox/android/DropboxModule;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation runtime Ldagger/Module;
    injects = {
        Lcom/dropbox/android/service/CameraUploadService;,
        Lcom/dropbox/android/service/v;
    }
    library = true
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideApiManager()Lcom/dropbox/android/filemanager/a;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .prologue
    .line 58
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    return-object v0
.end method

.method provideGlobalProperties()Ldbxyzptlk/db231222/n/k;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldbxyzptlk/db231222/W/d;
    .end annotation

    .prologue
    .line 34
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    return-object v0
.end method

.method provideMediaProviderPagerFactory()Lcom/dropbox/android/util/az;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldbxyzptlk/db231222/W/d;
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/dropbox/android/util/aA;

    invoke-direct {v0}, Lcom/dropbox/android/util/aA;-><init>()V

    return-object v0
.end method

.method provideMediaProviderWrapper()Ldbxyzptlk/db231222/u/b;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ldbxyzptlk/db231222/u/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/u/b;-><init>()V

    return-object v0
.end method

.method provideScanUrisTaskFactory()Lcom/dropbox/android/service/x;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldbxyzptlk/db231222/W/d;
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcom/dropbox/android/service/x;

    invoke-direct {v0}, Lcom/dropbox/android/service/x;-><init>()V

    return-object v0
.end method

.method provideSleeper()Ldbxyzptlk/db231222/u/d;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldbxyzptlk/db231222/W/d;
    .end annotation

    .prologue
    .line 54
    new-instance v0, Ldbxyzptlk/db231222/u/d;

    invoke-direct {v0}, Ldbxyzptlk/db231222/u/d;-><init>()V

    return-object v0
.end method

.method provideTimeReader()Ldbxyzptlk/db231222/u/c;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldbxyzptlk/db231222/W/d;
    .end annotation

    .prologue
    .line 50
    new-instance v0, Ldbxyzptlk/db231222/u/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/u/c;-><init>()V

    return-object v0
.end method

.method provideTimer()Ljava/util/Timer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .prologue
    .line 42
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    return-object v0
.end method
