.class public final Lcom/dropbox/android/service/J;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/analytics/m;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(ZZZZ)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean p1, p0, Lcom/dropbox/android/service/J;->a:Z

    .line 30
    iput-boolean p2, p0, Lcom/dropbox/android/service/J;->b:Z

    .line 31
    iput-boolean p3, p0, Lcom/dropbox/android/service/J;->c:Z

    .line 32
    iput-boolean p4, p0, Lcom/dropbox/android/service/J;->d:Z

    .line 33
    return-void
.end method

.method synthetic constructor <init>(ZZZZLcom/dropbox/android/service/H;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/service/J;-><init>(ZZZZ)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/J;Z)Z
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/dropbox/android/service/J;->a:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/service/J;Z)Z
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/dropbox/android/service/J;->b:Z

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/service/J;Z)Z
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/dropbox/android/service/J;->c:Z

    return p1
.end method

.method static synthetic d(Lcom/dropbox/android/service/J;Z)Z
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/dropbox/android/service/J;->d:Z

    return p1
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/analytics/l;)V
    .locals 2

    .prologue
    .line 70
    const-string v0, "network.state.connected"

    invoke-virtual {p0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 71
    const-string v0, "network.state.isWifi"

    invoke-virtual {p0}, Lcom/dropbox/android/service/J;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 72
    const-string v0, "network.state.isRoaming"

    invoke-virtual {p0}, Lcom/dropbox/android/service/J;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 73
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/dropbox/android/service/J;->a:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/dropbox/android/service/J;->b:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/dropbox/android/service/J;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/dropbox/android/service/J;->d:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/dropbox/android/service/J;

    if-eqz v1, :cond_0

    .line 59
    check-cast p1, Lcom/dropbox/android/service/J;

    .line 60
    iget-boolean v1, p0, Lcom/dropbox/android/service/J;->a:Z

    iget-boolean v2, p1, Lcom/dropbox/android/service/J;->a:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/dropbox/android/service/J;->b:Z

    iget-boolean v2, p1, Lcom/dropbox/android/service/J;->b:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/dropbox/android/service/J;->c:Z

    iget-boolean v2, p1, Lcom/dropbox/android/service/J;->c:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/dropbox/android/service/J;->d:Z

    iget-boolean v2, p1, Lcom/dropbox/android/service/J;->d:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 65
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-boolean v0, p0, Lcom/dropbox/android/service/J;->a:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    iget-boolean v2, p0, Lcom/dropbox/android/service/J;->b:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    :goto_1
    or-int/2addr v2, v0

    iget-boolean v0, p0, Lcom/dropbox/android/service/J;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    :goto_2
    or-int/2addr v0, v2

    iget-boolean v2, p0, Lcom/dropbox/android/service/J;->d:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
