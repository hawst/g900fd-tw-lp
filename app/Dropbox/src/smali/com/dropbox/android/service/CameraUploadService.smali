.class public Lcom/dropbox/android/service/CameraUploadService;
.super Landroid/app/Service;
.source "panda.py"


# static fields
.field private static final d:Ljava/lang/String;

.field private static final e:J


# instance fields
.field a:Ldbxyzptlk/db231222/W/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/W/b",
            "<",
            "Ljava/util/Timer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation
.end field

.field b:Lcom/dropbox/android/service/x;
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation
.end field

.field c:Ldbxyzptlk/db231222/u/b;
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation
.end field

.field private f:Lcom/dropbox/android/filemanager/I;

.field private g:Ldbxyzptlk/db231222/n/P;

.field private h:Ldbxyzptlk/db231222/z/M;

.field private final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/dropbox/android/service/p;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/Object;

.field private l:Ljava/util/Timer;

.field private m:I

.field private volatile n:Z

.field private o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private p:Z

.field private final q:Landroid/content/BroadcastReceiver;

.field private final r:Ljava/lang/Object;

.field private s:Lcom/dropbox/android/service/v;

.field private t:Lcom/dropbox/android/service/I;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 87
    const-class v0, Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    .line 111
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "htc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const-wide/16 v0, 0x1f40

    sput-wide v0, Lcom/dropbox/android/service/CameraUploadService;->e:J

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    const-wide/16 v0, 0xfa

    sput-wide v0, Lcom/dropbox/android/service/CameraUploadService;->e:J

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x6

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 155
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->i:Ljava/util/HashMap;

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->j:Ljava/util/ArrayList;

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->k:Ljava/lang/Object;

    .line 166
    iput v2, p0, Lcom/dropbox/android/service/CameraUploadService;->m:I

    .line 168
    iput-boolean v1, p0, Lcom/dropbox/android/service/CameraUploadService;->n:Z

    .line 169
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 170
    iput-boolean v1, p0, Lcom/dropbox/android/service/CameraUploadService;->p:Z

    .line 172
    new-instance v0, Lcom/dropbox/android/service/f;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/f;-><init>(Lcom/dropbox/android/service/CameraUploadService;)V

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->q:Landroid/content/BroadcastReceiver;

    .line 186
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->r:Ljava/lang/Object;

    .line 191
    iput-object v4, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    .line 193
    iput-object v4, p0, Lcom/dropbox/android/service/CameraUploadService;->t:Lcom/dropbox/android/service/I;

    .line 1590
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/I;)Lcom/dropbox/android/service/I;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/dropbox/android/service/CameraUploadService;->t:Lcom/dropbox/android/service/I;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/v;)Lcom/dropbox/android/service/v;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/io/File;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 628
    const/16 v0, 0x2004

    new-array v3, v0, [B

    .line 631
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    .line 632
    ushr-int/lit8 v1, v0, 0x18

    int-to-byte v1, v1

    aput-byte v1, v3, v2

    .line 633
    const/4 v1, 0x1

    ushr-int/lit8 v4, v0, 0x10

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 634
    const/4 v1, 0x2

    ushr-int/lit8 v4, v0, 0x8

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 635
    const/4 v1, 0x3

    int-to-byte v0, v0

    aput-byte v0, v3, v1

    .line 637
    const/4 v1, 0x0

    .line 639
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/DigestException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v1, v2

    .line 643
    :cond_0
    add-int/lit8 v2, v1, 0x4

    rsub-int v4, v1, 0x2000

    :try_start_1
    invoke-virtual {v0, v3, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 644
    if-gez v2, :cond_1

    .line 653
    :goto_0
    const/16 v2, 0x10

    new-array v2, v2, [B

    .line 654
    const-string v4, "MD5"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 655
    const/4 v5, 0x0

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v4, v3, v5, v1}, Ljava/security/MessageDigest;->update([BII)V

    .line 656
    const/4 v1, 0x0

    array-length v3, v2

    invoke-virtual {v4, v2, v1, v3}, Ljava/security/MessageDigest;->digest([BII)I

    .line 658
    invoke-static {v2}, Ldbxyzptlk/db231222/z/a;->a([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/DigestException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 665
    invoke-static {v0}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    return-object v1

    .line 647
    :cond_1
    add-int/2addr v1, v2

    .line 648
    const/16 v2, 0x2000

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 659
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 660
    :goto_1
    :try_start_2
    new-instance v1, Ljava/io/IOException;

    const-string v2, "no such algorithm exception"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 665
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v0

    .line 661
    :catch_1
    move-exception v0

    .line 662
    :goto_3
    :try_start_3
    sget-object v2, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v3, "failed to server hash camera upload"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 663
    new-instance v0, Ljava/io/IOException;

    const-string v2, "digest exception"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 665
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 661
    :catch_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    .line 659
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static synthetic a(Ljava/io/File;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0, p1, p2}, Lcom/dropbox/android/service/CameraUploadService;->b(Ljava/io/File;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private a(J)V
    .locals 4

    .prologue
    .line 537
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->c:Ldbxyzptlk/db231222/u/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/u/b;->a()[Lcom/dropbox/android/filemanager/ad;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 538
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/ad;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3, p1, p2}, Lcom/dropbox/android/service/CameraUploadService;->a(Landroid/net/Uri;J)V

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 85
    invoke-static {p0, p1}, Lcom/dropbox/android/service/CameraUploadService;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V

    return-void
.end method

.method private a(Landroid/net/Uri;J)V
    .locals 5

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/dropbox/android/service/CameraUploadService;->n:Z

    if-eqz v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 517
    if-eqz v0, :cond_0

    .line 520
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 521
    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Lcom/dropbox/android/service/CameraUploadService;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 526
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    if-nez v2, :cond_2

    .line 527
    iget-object v2, p0, Lcom/dropbox/android/service/CameraUploadService;->k:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528
    :try_start_1
    iget-object v3, p0, Lcom/dropbox/android/service/CameraUploadService;->b:Lcom/dropbox/android/service/x;

    iget v4, p0, Lcom/dropbox/android/service/CameraUploadService;->m:I

    invoke-virtual {v3, p0, v0, v4}, Lcom/dropbox/android/service/x;->a(Lcom/dropbox/android/service/CameraUploadService;Ldbxyzptlk/db231222/r/d;I)Lcom/dropbox/android/service/v;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    .line 529
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->l:Ljava/util/Timer;

    iget-object v3, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    invoke-virtual {v0, v3, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 530
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 532
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/service/v;->a(Landroid/net/Uri;)V

    .line 533
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 530
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;J)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/service/CameraUploadService;->a(J)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;Landroid/net/Uri;J)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/service/CameraUploadService;->a(Landroid/net/Uri;J)V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 543
    sget-object v2, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v3, "Getting hashes..."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/service/CameraUploadService;->h:Ldbxyzptlk/db231222/z/M;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/M;->g()Ljava/util/List;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 563
    sget-object v3, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v6, "Inserting hashes..."

    invoke-static {v3, v6}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 565
    const-string v3, "INSERT INTO camera_upload (server_hash, uploaded) VALUES (?, ?)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v7

    .line 568
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 570
    :try_start_1
    const-string v3, "SELECT COUNT(*) FROM camera_upload"

    const/4 v6, 0x0

    invoke-static {p1, v3, v6}, Lcom/dropbox/android/util/B;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v8

    .line 572
    cmp-long v3, v8, v4

    if-lez v3, :cond_2

    move v6, v1

    .line 574
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 577
    if-eqz v6, :cond_5

    .line 578
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 579
    const-string v3, "uploaded"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 580
    const-string v3, "camera_upload"

    const-string v9, "server_hash = ?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-virtual {p1, v3, v2, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    .line 584
    :goto_2
    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 585
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 586
    const/4 v2, 0x1

    invoke-virtual {v7, v2, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 587
    const/4 v0, 0x2

    const-wide/16 v2, 0x1

    invoke-virtual {v7, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 588
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 593
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 594
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0

    .line 550
    :catch_0
    move-exception v1

    .line 551
    sget-object v2, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v3, "Error from server getting hashes."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget v2, v1, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v3, 0x1f4

    if-eq v2, v3, :cond_1

    iget v2, v1, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v3, 0x1f6

    if-eq v2, v3, :cond_1

    iget v2, v1, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v3, 0x1f7

    if-eq v2, v3, :cond_1

    .line 553
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 599
    :cond_1
    :goto_3
    return v0

    .line 558
    :catch_1
    move-exception v1

    .line 559
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_2
    move v6, v0

    .line 572
    goto :goto_0

    .line 591
    :cond_3
    :try_start_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 593
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 594
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 598
    :cond_4
    sget-object v0, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v2, "Done with hashes."

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 599
    goto :goto_3

    .line 556
    :catch_2
    move-exception v1

    goto :goto_3

    .line 548
    :catch_3
    move-exception v1

    goto :goto_3

    :cond_5
    move-wide v2, v4

    goto :goto_2
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/dropbox/android/service/CameraUploadService;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/service/CameraUploadService;Z)Z
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/dropbox/android/service/CameraUploadService;->p:Z

    return p1
.end method

.method static synthetic b()J
    .locals 2

    .prologue
    .line 85
    sget-wide v0, Lcom/dropbox/android/service/CameraUploadService;->e:J

    return-wide v0
.end method

.method private static b(Ljava/io/File;J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 603
    const-string v0, "INSERT INTO camera_upload (local_hash, ignored) VALUES (?, ?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 606
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 609
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 610
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 611
    const/4 v3, 0x1

    invoke-virtual {v1, v3, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 612
    const/4 v0, 0x2

    const-wide/16 v3, 0x1

    invoke-virtual {v1, v0, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 613
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 619
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0

    .line 616
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 619
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 621
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/service/CameraUploadService;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/dropbox/android/service/CameraUploadService;->c()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/filemanager/I;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->f:Lcom/dropbox/android/filemanager/I;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 679
    iget-object v1, p0, Lcom/dropbox/android/service/CameraUploadService;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 680
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/service/CameraUploadService;->k:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 681
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->l:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 682
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->a:Ldbxyzptlk/db231222/W/b;

    invoke-interface {v0}, Ldbxyzptlk/db231222/W/b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Timer;

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->l:Ljava/util/Timer;

    .line 688
    iget v0, p0, Lcom/dropbox/android/service/CameraUploadService;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/service/CameraUploadService;->m:I

    .line 689
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 694
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->s:Lcom/dropbox/android/service/v;

    .line 696
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->r:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 697
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 699
    sget-object v0, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v1, "Cancelled tasks"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    return-void

    .line 689
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 697
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method static synthetic d(Lcom/dropbox/android/service/CameraUploadService;)Ldbxyzptlk/db231222/n/P;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->g:Ldbxyzptlk/db231222/n/P;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->r:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/service/CameraUploadService;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/dropbox/android/service/CameraUploadService;->p:Z

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/I;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->t:Lcom/dropbox/android/service/I;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/service/CameraUploadService;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/dropbox/android/service/CameraUploadService;->n:Z

    return v0
.end method

.method static synthetic i(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->l:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/service/CameraUploadService;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/dropbox/android/service/CameraUploadService;->m:I

    return v0
.end method

.method static synthetic k(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic l(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->k:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 508
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 396
    const-string v0, "create"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 397
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 399
    invoke-static {p0}, Lcom/dropbox/android/g;->a(Ljava/lang/Object;)V

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->a:Ldbxyzptlk/db231222/W/b;

    invoke-interface {v0}, Ldbxyzptlk/db231222/W/b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Timer;

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->l:Ljava/util/Timer;

    .line 401
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 402
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 403
    :goto_0
    if-nez v0, :cond_1

    .line 404
    invoke-virtual {p0}, Lcom/dropbox/android/service/CameraUploadService;->stopSelf()V

    .line 435
    :goto_1
    return-void

    .line 402
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 407
    :cond_1
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/service/CameraUploadService;->f:Lcom/dropbox/android/filemanager/I;

    .line 408
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/service/CameraUploadService;->g:Ldbxyzptlk/db231222/n/P;

    .line 409
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->h:Ldbxyzptlk/db231222/z/M;

    .line 412
    invoke-virtual {p0}, Lcom/dropbox/android/service/CameraUploadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 413
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->c:Ldbxyzptlk/db231222/u/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/u/b;->a()[Lcom/dropbox/android/filemanager/ad;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 414
    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/ad;->b()Landroid/net/Uri;

    move-result-object v4

    .line 415
    new-instance v5, Lcom/dropbox/android/service/g;

    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    invoke-direct {v5, p0, v6, v4}, Lcom/dropbox/android/service/g;-><init>(Lcom/dropbox/android/service/CameraUploadService;Landroid/os/Handler;Landroid/net/Uri;)V

    .line 421
    iget-object v6, p0, Lcom/dropbox/android/service/CameraUploadService;->j:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    const/4 v6, 0x1

    invoke-virtual {v1, v4, v6, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 413
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 425
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 427
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Lcom/dropbox/android/service/CameraUploadService;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/service/CameraUploadService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 430
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 431
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 432
    iget-object v1, p0, Lcom/dropbox/android/service/CameraUploadService;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/service/CameraUploadService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 434
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->q:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.dropbox.intent.action.FULL_SCAN"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/service/CameraUploadService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 477
    sget-object v0, Lcom/dropbox/android/service/CameraUploadService;->d:Ljava/lang/String;

    const-string v1, "Destroying camera upload service."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v0, "destroy"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 488
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/service/CameraUploadService;->n:Z

    .line 489
    invoke-direct {p0}, Lcom/dropbox/android/service/CameraUploadService;->c()V

    .line 494
    invoke-virtual {p0}, Lcom/dropbox/android/service/CameraUploadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 495
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/ContentObserver;

    .line 496
    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/service/CameraUploadService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 501
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cr()V

    .line 502
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 503
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 439
    const/4 v0, 0x0

    .line 440
    if-eqz p1, :cond_0

    .line 441
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 444
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->F()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 446
    const-string v1, "ACTION_REQUIRE_HASH_UPDATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/service/h;

    invoke-direct {v1, p0}, Lcom/dropbox/android/service/h;-><init>(Lcom/dropbox/android/service/CameraUploadService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 463
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/service/CameraUploadService;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 464
    const/4 v0, 0x1

    .line 471
    :goto_1
    return v0

    .line 460
    :cond_1
    const-wide/16 v0, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/service/CameraUploadService;->a(J)V

    goto :goto_0

    .line 470
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/android/service/CameraUploadService;->stopSelf()V

    .line 471
    const/4 v0, 0x2

    goto :goto_1
.end method
