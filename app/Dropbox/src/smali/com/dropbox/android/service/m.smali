.class final Lcom/dropbox/android/service/m;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/dropbox/android/service/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Ljava/lang/String;

.field private final f:Ljava/util/Date;

.field private final g:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1539
    iput-object p1, p0, Lcom/dropbox/android/service/m;->a:Ljava/io/File;

    .line 1540
    iput-object p2, p0, Lcom/dropbox/android/service/m;->b:Ljava/lang/String;

    .line 1541
    iput-object p3, p0, Lcom/dropbox/android/service/m;->c:Ljava/lang/String;

    .line 1542
    iput-wide p4, p0, Lcom/dropbox/android/service/m;->d:J

    .line 1543
    iput-object p6, p0, Lcom/dropbox/android/service/m;->e:Ljava/lang/String;

    .line 1544
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/dropbox/android/util/av;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    .line 1545
    iput-object p7, p0, Lcom/dropbox/android/service/m;->g:Landroid/net/Uri;

    .line 1546
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/service/m;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/dropbox/android/service/m;->g:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/service/m;)Ljava/io/File;
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/dropbox/android/service/m;->a:Ljava/io/File;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/service/m;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/dropbox/android/service/m;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/service/m;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/dropbox/android/service/m;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/service/m;)J
    .locals 2

    .prologue
    .line 1527
    iget-wide v0, p0, Lcom/dropbox/android/service/m;->d:J

    return-wide v0
.end method

.method static synthetic h(Lcom/dropbox/android/service/m;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/dropbox/android/service/m;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/service/m;)I
    .locals 3

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    iget-object v1, p1, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    .line 1551
    if-eqz v0, :cond_1

    .line 1567
    :cond_0
    :goto_0
    return v0

    .line 1557
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/service/m;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p1, Lcom/dropbox/android/service/m;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    .line 1558
    if-nez v0, :cond_0

    .line 1565
    iget-object v0, p0, Lcom/dropbox/android/service/m;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1566
    iget-object v1, p1, Lcom/dropbox/android/service/m;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 1567
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Lcom/dropbox/android/service/m;)Z
    .locals 2

    .prologue
    .line 1571
    iget-object v0, p0, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    iget-object v1, p1, Lcom/dropbox/android/service/m;->f:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1527
    check-cast p1, Lcom/dropbox/android/service/m;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/service/m;->a(Lcom/dropbox/android/service/m;)I

    move-result v0

    return v0
.end method
