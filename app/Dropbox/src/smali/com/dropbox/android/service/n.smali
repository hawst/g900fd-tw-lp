.class public abstract Lcom/dropbox/android/service/n;
.super Ljava/util/TimerTask;
.source "panda.py"


# instance fields
.field final a:Lcom/dropbox/android/service/CameraUploadService;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/service/CameraUploadService;I)V
    .locals 0

    .prologue
    .line 1188
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1189
    iput-object p1, p0, Lcom/dropbox/android/service/n;->a:Lcom/dropbox/android/service/CameraUploadService;

    .line 1190
    iput p2, p0, Lcom/dropbox/android/service/n;->b:I

    .line 1191
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1209
    iget-object v0, p0, Lcom/dropbox/android/service/n;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->l(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1210
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/service/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1211
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 1213
    :cond_0
    monitor-exit v1

    .line 1214
    return-void

    .line 1213
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final b()Z
    .locals 3

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/dropbox/android/service/n;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->l(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1218
    :try_start_0
    iget v0, p0, Lcom/dropbox/android/service/n;->b:I

    iget-object v2, p0, Lcom/dropbox/android/service/n;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->j(Lcom/dropbox/android/service/CameraUploadService;)I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/service/n;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->h(Lcom/dropbox/android/service/CameraUploadService;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1219
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 1197
    invoke-virtual {p0}, Lcom/dropbox/android/service/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1198
    invoke-virtual {p0}, Lcom/dropbox/android/service/n;->a()V

    .line 1200
    :cond_0
    return-void
.end method
