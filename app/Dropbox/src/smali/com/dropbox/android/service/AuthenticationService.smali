.class public Lcom/dropbox/android/service/AuthenticationService;
.super Landroid/app/Service;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/authenticator/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/service/AuthenticationService;->a:Lcom/dropbox/android/authenticator/a;

    invoke-virtual {v0}, Lcom/dropbox/android/authenticator/a;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "create"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 17
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 19
    new-instance v0, Lcom/dropbox/android/authenticator/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/authenticator/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/service/AuthenticationService;->a:Lcom/dropbox/android/authenticator/a;

    .line 20
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "destroy"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 25
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 26
    return-void
.end method
