.class public Lcom/dropbox/android/service/NotificationService;
.super Landroid/app/Service;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "create"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 21
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 22
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "destroy"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 27
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 28
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/dropbox/android/notifications/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/notifications/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    .line 33
    invoke-virtual {v0, p1}, Lcom/dropbox/android/notifications/d;->a(Landroid/content/Intent;)V

    .line 35
    invoke-virtual {v0}, Lcom/dropbox/android/notifications/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/service/NotificationService;->stopSelf()V

    .line 39
    const/4 v0, 0x2

    goto :goto_0
.end method
