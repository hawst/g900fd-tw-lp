.class public final Lcom/dropbox/android/service/i;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Ldbxyzptlk/db231222/n/P;

.field private final d:Lcom/dropbox/android/filemanager/I;

.field private final e:Lcom/dropbox/android/notifications/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/notifications/d;)V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    iput-object p1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    .line 217
    iput-object p2, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    .line 218
    iput-object p3, p0, Lcom/dropbox/android/service/i;->b:Ldbxyzptlk/db231222/z/M;

    .line 219
    iput-object p4, p0, Lcom/dropbox/android/service/i;->d:Lcom/dropbox/android/filemanager/I;

    .line 220
    iput-object p5, p0, Lcom/dropbox/android/service/i;->e:Lcom/dropbox/android/notifications/d;

    .line 221
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/i;)Lcom/dropbox/android/filemanager/I;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/dropbox/android/service/i;->d:Lcom/dropbox/android/filemanager/I;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/service/i;)Ldbxyzptlk/db231222/z/M;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/dropbox/android/service/i;->b:Ldbxyzptlk/db231222/z/M;

    return-object v0
.end method

.method private f(Z)V
    .locals 3

    .prologue
    .line 371
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    const-class v1, Lcom/dropbox/android/service/NewPictureReceiver;

    invoke-static {v0, v1, p1}, Lcom/dropbox/android/util/ba;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 375
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    const-class v2, Lcom/dropbox/android/service/CameraUploadService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    if-eqz p1, :cond_1

    .line 377
    iget-object v1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 227
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    const-class v2, Lcom/dropbox/android/service/CameraUploadService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    const-string v1, "ACTION_REQUIRE_HASH_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    iget-object v1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 230
    return-void
.end method

.method public final a(Lcom/dropbox/android/provider/j;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 388
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/service/i;->a(Z)Z

    .line 389
    invoke-virtual {p1}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 390
    const-string v1, "camera_upload"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 391
    return-void
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/P;->e(Z)V

    .line 238
    invoke-direct {p0, p1}, Lcom/dropbox/android/service/i;->f(Z)V

    .line 239
    if-eqz p1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/dropbox/android/service/i;->d:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->i()V

    .line 258
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/provider/SDKProvider;->b(Landroid/content/Context;)V

    .line 259
    const/4 v0, 0x1

    .line 261
    :goto_1
    return v0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/service/i;->e:Lcom/dropbox/android/notifications/d;

    sget-object v1, Lcom/dropbox/android/util/aM;->a:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 245
    iget-object v0, p0, Lcom/dropbox/android/service/i;->e:Lcom/dropbox/android/notifications/d;

    sget-object v1, Lcom/dropbox/android/util/aM;->c:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;)V

    .line 249
    new-instance v0, Lcom/dropbox/android/service/j;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/j;-><init>(Lcom/dropbox/android/service/i;)V

    .line 255
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 256
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 261
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/service/i;->f(Z)V

    .line 354
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/P;->j(Z)V

    .line 268
    if-eqz p1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/dropbox/android/service/i;->d:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->i()V

    .line 272
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->z()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/P;->k(Z)V

    .line 282
    if-nez p1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/dropbox/android/service/i;->d:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->i()V

    .line 286
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 3

    .prologue
    .line 294
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aK()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "enabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 296
    new-instance v0, Lcom/dropbox/android/service/k;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/service/k;-><init>(Lcom/dropbox/android/service/i;Z)V

    invoke-virtual {v0}, Lcom/dropbox/android/service/k;->start()V

    .line 308
    return-void
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 315
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 316
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/P;->l(Z)V

    .line 319
    invoke-virtual {p0, p1}, Lcom/dropbox/android/service/i;->d(Z)V

    .line 321
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    if-eqz p1, :cond_1

    .line 324
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->i(Z)V

    .line 325
    iget-object v0, p0, Lcom/dropbox/android/service/i;->c:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/P;->g(Z)V

    .line 327
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.intent.action.FULL_SCAN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/dropbox/android/service/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 331
    iget-object v0, p0, Lcom/dropbox/android/service/i;->d:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->e()V

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    new-instance v0, Lcom/dropbox/android/service/l;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/l;-><init>(Lcom/dropbox/android/service/i;)V

    invoke-virtual {v0}, Lcom/dropbox/android/service/l;->start()V

    goto :goto_0
.end method
