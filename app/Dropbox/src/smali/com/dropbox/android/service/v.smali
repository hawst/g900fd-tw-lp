.class public Lcom/dropbox/android/service/v;
.super Lcom/dropbox/android/service/n;
.source "panda.py"


# instance fields
.field b:Ldbxyzptlk/db231222/u/d;
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation
.end field

.field c:Ldbxyzptlk/db231222/u/c;
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation
.end field

.field d:Lcom/dropbox/android/util/az;
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation
.end field

.field private final e:Ldbxyzptlk/db231222/r/d;

.field private final f:Lcom/dropbox/android/provider/j;

.field private final g:Ldbxyzptlk/db231222/n/P;

.field private final h:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private i:J


# direct methods
.method public constructor <init>(Lcom/dropbox/android/service/CameraUploadService;Ldbxyzptlk/db231222/r/d;I)V
    .locals 2

    .prologue
    .line 722
    invoke-direct {p0, p1, p3}, Lcom/dropbox/android/service/n;-><init>(Lcom/dropbox/android/service/CameraUploadService;I)V

    .line 714
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/v;->h:Ljava/util/HashSet;

    .line 715
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/service/v;->i:J

    .line 723
    iput-object p2, p0, Lcom/dropbox/android/service/v;->e:Ldbxyzptlk/db231222/r/d;

    .line 724
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    .line 725
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->q()Lcom/dropbox/android/provider/j;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/v;->f:Lcom/dropbox/android/provider/j;

    .line 726
    invoke-static {p0}, Lcom/dropbox/android/g;->a(Ljava/lang/Object;)V

    .line 727
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/service/q;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1082
    const-string v1, "camera_upload"

    sget-object v2, Lcom/dropbox/android/service/q;->a:[Ljava/lang/String;

    const-string v3, "local_hash IS NOT NULL"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1085
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 1086
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1087
    new-instance v2, Lcom/dropbox/android/service/q;

    invoke-direct {v2, v1}, Lcom/dropbox/android/service/q;-><init>(Landroid/database/Cursor;)V

    .line 1088
    invoke-static {v2}, Lcom/dropbox/android/service/q;->e(Lcom/dropbox/android/service/q;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1091
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1093
    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/dropbox/android/service/v;->e:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 40

    .prologue
    .line 737
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setPriority(I)V

    .line 738
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Starting camera upload scan..."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-class v17, Lcom/dropbox/android/service/v;

    monitor-enter v17

    .line 741
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->e(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742
    const-wide/16 v4, 0x0

    .line 743
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/dropbox/android/service/v;->i:J

    const-wide/16 v7, 0xbb8

    add-long/2addr v2, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dropbox/android/service/v;->c:Ldbxyzptlk/db231222/u/c;

    invoke-virtual {v7}, Ldbxyzptlk/db231222/u/c;->a()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v7

    sub-long/2addr v2, v7

    .line 744
    :goto_0
    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-lez v7, :cond_1

    const-wide/32 v7, 0xea60

    cmp-long v7, v4, v7

    if-gez v7, :cond_1

    .line 750
    :try_start_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dropbox/android/service/v;->b:Ldbxyzptlk/db231222/u/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v8}, Lcom/dropbox/android/service/CameraUploadService;->e(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8, v2, v3}, Ldbxyzptlk/db231222/u/d;->a(Ljava/lang/Object;J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 756
    :goto_1
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/service/v;->b()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 757
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    monitor-exit v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1078
    :goto_2
    return-void

    .line 760
    :cond_0
    add-long/2addr v4, v2

    .line 761
    :try_start_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/dropbox/android/service/v;->i:J

    const-wide/16 v7, 0xbb8

    add-long/2addr v2, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dropbox/android/service/v;->c:Ldbxyzptlk/db231222/u/c;

    invoke-virtual {v7}, Ldbxyzptlk/db231222/u/c;->a()J

    move-result-wide v7

    sub-long/2addr v2, v7

    goto :goto_0

    .line 766
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/service/v;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 767
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    monitor-exit v17

    goto :goto_2

    .line 1077
    :catchall_0
    move-exception v2

    monitor-exit v17
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v2

    .line 770
    :cond_2
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/v;)Lcom/dropbox/android/service/v;

    .line 771
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 773
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->c:Ldbxyzptlk/db231222/u/c;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/u/c;->a()J

    move-result-wide v18

    .line 776
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v2

    if-nez v2, :cond_3

    .line 777
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v2}, Lcom/dropbox/android/service/CameraUploadService;->stopSelf()V

    .line 778
    monitor-exit v17
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 771
    :catchall_1
    move-exception v2

    :try_start_9
    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v2

    .line 782
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->C()Z

    move-result v20

    .line 783
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 787
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    .line 788
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->r()Ldbxyzptlk/db231222/n/q;

    move-result-object v3

    sget-object v4, Ldbxyzptlk/db231222/n/q;->c:Ldbxyzptlk/db231222/n/q;

    if-eq v3, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->x()Z

    move-result v3

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->q()Z

    move-result v3

    if-eqz v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->f(Lcom/dropbox/android/service/CameraUploadService;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 793
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/filemanager/I;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/I;->k()V

    .line 794
    const/4 v2, 0x1

    move/from16 v16, v2

    .line 798
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->f:Lcom/dropbox/android/provider/j;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 807
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v3

    .line 808
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/n/P;->r()Ldbxyzptlk/db231222/n/q;

    move-result-object v4

    sget-object v5, Ldbxyzptlk/db231222/n/q;->a:Ldbxyzptlk/db231222/n/q;

    if-eq v4, v5, :cond_5

    .line 809
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/U;->e()V

    .line 811
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v4, v2}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 812
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    sget-object v5, Ldbxyzptlk/db231222/n/q;->a:Ldbxyzptlk/db231222/n/q;

    invoke-virtual {v4, v5}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/q;)V

    .line 813
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/U;->e()V

    .line 815
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->g(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/I;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 816
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v4}, Lcom/dropbox/android/service/CameraUploadService;->g(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/I;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dropbox/android/service/G;->b(Lcom/dropbox/android/service/I;)V

    .line 817
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/I;)Lcom/dropbox/android/service/I;

    .line 839
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v3}, Lcom/dropbox/android/service/CameraUploadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    .line 844
    const/4 v3, 0x0

    .line 845
    if-eqz v16, :cond_1a

    .line 846
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/dropbox/android/service/v;->a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashMap;

    move-result-object v3

    move-object v15, v3

    .line 849
    :goto_5
    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    .line 850
    invoke-static {}, Lcom/dropbox/android/service/y;->a()Lcom/dropbox/android/service/y;

    move-result-object v24

    .line 851
    invoke-static {}, Lcom/dropbox/android/service/y;->b()Lcom/dropbox/android/service/y;

    move-result-object v25

    .line 854
    new-instance v26, Ljava/util/LinkedList;

    invoke-direct/range {v26 .. v26}, Ljava/util/LinkedList;-><init>()V

    .line 855
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->h:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_6
    :goto_6
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_17

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/net/Uri;

    .line 856
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->d:Lcom/dropbox/android/util/az;

    move-object/from16 v0, v22

    invoke-interface {v3, v0, v12}, Lcom/dropbox/android/util/az;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/util/ax;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v28

    .line 857
    if-eqz v28, :cond_6

    .line 862
    :try_start_b
    const-string v3, "_id"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v29

    .line 863
    const-string v3, "_data"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v30

    .line 864
    const-string v3, "_size"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v31

    .line 865
    const-string v3, "mime_type"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v32

    .line 866
    const-string v3, "date_modified"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v33

    .line 868
    :cond_7
    :goto_7
    invoke-virtual/range {v28 .. v28}, Lcom/dropbox/android/util/ax;->a()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 869
    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ax;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 870
    if-eqz v3, :cond_7

    .line 875
    invoke-static {v3}, Lcom/dropbox/android/util/av;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-static {v3}, Lcom/dropbox/android/util/av;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 879
    :cond_8
    new-instance v34, Ljava/io/File;

    move-object/from16 v0, v34

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 881
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 885
    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ax;->a(I)J

    move-result-wide v3

    .line 886
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-nez v5, :cond_19

    .line 890
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->length()J

    move-result-wide v3

    move-wide v13, v3

    .line 893
    :goto_8
    const-wide/16 v3, 0x0

    cmp-long v3, v13, v3

    if-lez v3, :cond_7

    .line 897
    move-object/from16 v0, v28

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ax;->b(I)Ljava/lang/String;

    move-result-object v35

    .line 903
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static/range {v35 .. v35}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 908
    :cond_9
    move-object/from16 v0, v34

    invoke-static {v0, v13, v14}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;J)Ljava/lang/String;

    move-result-object v36

    .line 910
    invoke-virtual/range {v28 .. v29}, Lcom/dropbox/android/util/ax;->a(I)J

    move-result-wide v3

    invoke-static {v12, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v37

    .line 911
    move-object/from16 v0, v28

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ax;->a(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v38, v3, v5

    .line 914
    const/4 v10, 0x0

    .line 915
    if-eqz v15, :cond_e

    .line 916
    move-object/from16 v0, v36

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dropbox/android/service/q;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-object v4, v3

    .line 932
    :goto_9
    if-eqz v4, :cond_12

    .line 936
    if-nez v16, :cond_f

    .line 1001
    :cond_a
    :try_start_c
    invoke-virtual/range {v28 .. v28}, Lcom/dropbox/android/util/ax;->b()V

    goto/16 :goto_6

    .line 820
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v4}, Ldbxyzptlk/db231222/n/P;->r()Ldbxyzptlk/db231222/n/q;

    move-result-object v4

    sget-object v5, Ldbxyzptlk/db231222/n/q;->c:Ldbxyzptlk/db231222/n/q;

    if-ne v4, v5, :cond_d

    .line 825
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->g(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/I;

    move-result-object v2

    if-nez v2, :cond_c

    .line 826
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    new-instance v3, Lcom/dropbox/android/service/r;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/dropbox/android/service/r;-><init>(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/f;)V

    invoke-static {v2, v3}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/I;)Lcom/dropbox/android/service/I;

    .line 827
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->g(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/I;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/dropbox/android/service/G;->a(Lcom/dropbox/android/service/I;)V

    .line 829
    :cond_c
    monitor-exit v17

    goto/16 :goto_2

    .line 833
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    sget-object v5, Ldbxyzptlk/db231222/n/q;->a:Ldbxyzptlk/db231222/n/q;

    invoke-virtual {v4, v5}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/q;)V

    .line 834
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/U;->e()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_4

    .line 918
    :cond_e
    :try_start_d
    const-string v3, "camera_upload"

    sget-object v4, Lcom/dropbox/android/service/q;->a:[Ljava/lang/String;

    const-string v5, "local_hash = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v36, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    move-result-object v4

    .line 924
    :try_start_e
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 925
    new-instance v3, Lcom/dropbox/android/service/q;

    invoke-direct {v3, v4}, Lcom/dropbox/android/service/q;-><init>(Landroid/database/Cursor;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 928
    :goto_a
    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move-object v4, v3

    .line 929
    goto :goto_9

    .line 928
    :catchall_2
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 1001
    :catchall_3
    move-exception v2

    :try_start_10
    invoke-virtual/range {v28 .. v28}, Lcom/dropbox/android/util/ax;->b()V

    throw v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 940
    :cond_f
    :try_start_11
    invoke-static {v4}, Lcom/dropbox/android/service/q;->a(Lcom/dropbox/android/service/q;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 947
    if-eqz v20, :cond_10

    .line 948
    invoke-static {v4}, Lcom/dropbox/android/service/q;->b(Lcom/dropbox/android/service/q;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 950
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 951
    const-string v5, "ignored"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 952
    const-string v5, "camera_upload"

    const-string v6, "_id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4}, Lcom/dropbox/android/service/q;->c(Lcom/dropbox/android/service/q;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-virtual {v2, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_7

    .line 970
    :cond_10
    invoke-static {v4}, Lcom/dropbox/android/service/q;->b(Lcom/dropbox/android/service/q;)Z

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->x()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 971
    :cond_11
    new-instance v3, Lcom/dropbox/android/service/o;

    invoke-static {v4}, Lcom/dropbox/android/service/q;->c(Lcom/dropbox/android/service/q;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-static {v4}, Lcom/dropbox/android/service/q;->d(Lcom/dropbox/android/service/q;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v4, v34

    move-object/from16 v5, v37

    move-object/from16 v6, v36

    move-object/from16 v7, v35

    move-wide/from16 v8, v38

    invoke-direct/range {v3 .. v12}, Lcom/dropbox/android/service/o;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 979
    :cond_12
    move-object/from16 v0, v23

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 980
    if-nez v3, :cond_13

    .line 981
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 983
    :cond_13
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v23

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 985
    invoke-static/range {v35 .. v35}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 986
    move-object/from16 v0, v24

    invoke-virtual {v0, v13, v14}, Lcom/dropbox/android/service/y;->a(J)V

    .line 992
    :cond_14
    :goto_b
    if-eqz v20, :cond_16

    .line 993
    move-object/from16 v0, v21

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 987
    :cond_15
    invoke-static/range {v35 .. v35}, Lcom/dropbox/android/util/ab;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 988
    move-object/from16 v0, v25

    invoke-virtual {v0, v13, v14}, Lcom/dropbox/android/service/y;->a(J)V

    goto :goto_b

    .line 995
    :cond_16
    new-instance v5, Lcom/dropbox/android/service/o;

    move-object/from16 v6, v34

    move-object/from16 v7, v37

    move-object/from16 v8, v36

    move-object/from16 v9, v35

    move-wide/from16 v10, v38

    invoke-direct/range {v5 .. v12}, Lcom/dropbox/android/service/o;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    goto/16 :goto_7

    .line 1005
    :cond_17
    :try_start_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/v;->c:Ldbxyzptlk/db231222/u/c;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/u/c;->a()J

    move-result-wide v3

    sub-long v3, v3, v18

    .line 1007
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Finished querying providers."

    invoke-static {v5, v6}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->j()Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v6, "mimes"

    move-object/from16 v0, v23

    invoke-virtual {v5, v6, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v6, "vidsizes"

    invoke-virtual/range {v24 .. v24}, Lcom/dropbox/android/service/y;->c()Ljava/util/LinkedHashMap;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v6, "imgsizes"

    invoke-virtual/range {v25 .. v25}, Lcom/dropbox/android/service/y;->c()Ljava/util/LinkedHashMap;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v6, "duration"

    invoke-virtual {v5, v6, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "ignore.existing"

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v4, "initial"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/service/v;->g:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v5}, Ldbxyzptlk/db231222/n/P;->x()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1014
    new-instance v3, Lcom/dropbox/android/service/w;

    move-object/from16 v4, p0

    move-object/from16 v5, v26

    move/from16 v6, v20

    move-object v7, v2

    move-object/from16 v8, v21

    invoke-direct/range {v3 .. v8}, Lcom/dropbox/android/service/w;-><init>(Lcom/dropbox/android/service/v;Ljava/util/LinkedList;ZLandroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/v;->a(Ljava/lang/Runnable;)V

    .line 1076
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Finished camera upload scan."

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1077
    monitor-exit v17
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_2

    .line 751
    :catch_0
    move-exception v7

    goto/16 :goto_1

    :cond_18
    move-object v3, v10

    goto/16 :goto_a

    :cond_19
    move-wide v13, v3

    goto/16 :goto_8

    :cond_1a
    move-object v15, v3

    goto/16 :goto_5

    :cond_1b
    move/from16 v16, v2

    goto/16 :goto_3
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 731
    iget-object v0, p0, Lcom/dropbox/android/service/v;->h:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 732
    iget-object v0, p0, Lcom/dropbox/android/service/v;->c:Ldbxyzptlk/db231222/u/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/u/c;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/service/v;->i:J

    .line 733
    return-void
.end method
