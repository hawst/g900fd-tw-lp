.class public final Lcom/dropbox/android/service/F;
.super Landroid/support/v4/content/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/a",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/dropbox/android/service/E;",
        ">;>;"
    }
.end annotation


# instance fields
.field private f:Landroid/content/Context;

.field private g:Lcom/dropbox/android/service/D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/service/D",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/service/D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dropbox/android/service/D",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 600
    invoke-direct {p0, p1}, Landroid/support/v4/content/a;-><init>(Landroid/content/Context;)V

    .line 601
    iput-object p1, p0, Lcom/dropbox/android/service/F;->f:Landroid/content/Context;

    .line 602
    iput-object p2, p0, Lcom/dropbox/android/service/F;->g:Lcom/dropbox/android/service/D;

    .line 603
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 594
    invoke-virtual {p0}, Lcom/dropbox/android/service/F;->j()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 607
    invoke-super {p0}, Landroid/support/v4/content/a;->f()V

    .line 608
    invoke-virtual {p0}, Lcom/dropbox/android/service/F;->p()V

    .line 609
    return-void
.end method

.method public final j()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613
    iget-object v0, p0, Lcom/dropbox/android/service/F;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/service/F;->g:Lcom/dropbox/android/service/D;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/A;->a(Landroid/content/ContentResolver;Lcom/dropbox/android/service/D;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
