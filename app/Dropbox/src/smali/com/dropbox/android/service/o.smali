.class final Lcom/dropbox/android/service/o;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Landroid/net/Uri;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464
    iput-object v0, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    .line 1465
    iput-object v0, p0, Lcom/dropbox/android/service/o;->h:Ljava/lang/Long;

    .line 1476
    iput-object p1, p0, Lcom/dropbox/android/service/o;->a:Ljava/io/File;

    .line 1477
    iput-object p2, p0, Lcom/dropbox/android/service/o;->b:Ljava/lang/String;

    .line 1478
    iput-object p3, p0, Lcom/dropbox/android/service/o;->c:Ljava/lang/String;

    .line 1479
    iput-object p4, p0, Lcom/dropbox/android/service/o;->d:Ljava/lang/String;

    .line 1480
    iput-wide p5, p0, Lcom/dropbox/android/service/o;->e:J

    .line 1481
    iput-object p7, p0, Lcom/dropbox/android/service/o;->f:Landroid/net/Uri;

    .line 1482
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 9

    .prologue
    .line 1469
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/dropbox/android/service/o;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V

    .line 1470
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/dropbox/android/service/o;->h:Ljava/lang/Long;

    .line 1471
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    .line 1472
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/o;)J
    .locals 2

    .prologue
    .line 1456
    iget-wide v0, p0, Lcom/dropbox/android/service/o;->e:J

    return-wide v0
.end method

.method static synthetic b(Lcom/dropbox/android/service/o;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1487
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/service/o;->a:Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1492
    :cond_0
    :goto_0
    return-void

    .line 1488
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1495
    const-string v2, "SELECT COUNT(*) FROM camera_upload WHERE server_hash = ? AND uploaded = ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    aput-object v4, v3, v1

    const-string v4, "1"

    aput-object v4, v3, v0

    invoke-static {p1, v2, v3}, Lcom/dropbox/android/util/B;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 1500
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b()Lcom/dropbox/android/service/m;
    .locals 8

    .prologue
    .line 1523
    new-instance v0, Lcom/dropbox/android/service/m;

    iget-object v1, p0, Lcom/dropbox/android/service/o;->a:Ljava/io/File;

    iget-object v2, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/service/o;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/service/o;->h:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p0, Lcom/dropbox/android/service/o;->b:Ljava/lang/String;

    iget-object v7, p0, Lcom/dropbox/android/service/o;->f:Landroid/net/Uri;

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/service/m;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    .line 1504
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1505
    const-string v1, "local_hash"

    iget-object v2, p0, Lcom/dropbox/android/service/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    const-string v1, "camera_upload"

    const-string v2, "server_hash = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1508
    return-void
.end method

.method public final c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    .line 1511
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1512
    const-string v1, "server_hash"

    iget-object v2, p0, Lcom/dropbox/android/service/o;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1513
    iget-object v1, p0, Lcom/dropbox/android/service/o;->h:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 1514
    const-string v1, "camera_upload"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/dropbox/android/service/o;->h:Ljava/lang/Long;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1520
    :goto_0
    return-void

    .line 1517
    :cond_0
    const-string v1, "local_hash"

    iget-object v2, p0, Lcom/dropbox/android/service/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1518
    const-string v1, "camera_upload"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/o;->h:Ljava/lang/Long;

    goto :goto_0
.end method
