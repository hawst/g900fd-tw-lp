.class public Lcom/dropbox/android/service/ReportReceiver;
.super Landroid/content/BroadcastReceiver;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.android.action.REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lcom/dropbox/android/service/ReportReceiver;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 41
    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 47
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 49
    invoke-static {v1, v0}, Lcom/dropbox/android/service/ReportReceiver;->a(Landroid/app/PendingIntent;Landroid/app/AlarmManager;)V

    .line 50
    return-void
.end method

.method private static a(Landroid/app/PendingIntent;Landroid/app/AlarmManager;)V
    .locals 4

    .prologue
    .line 60
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    .line 61
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0, v1, p0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 62
    return-void
.end method

.method public static b()V
    .locals 3

    .prologue
    .line 53
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lcom/dropbox/android/service/ReportReceiver;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 55
    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 56
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 24
    invoke-static {p0, p2}, Lcom/dropbox/android/util/analytics/a;->a(Landroid/content/BroadcastReceiver;Landroid/content/Intent;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 26
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    invoke-static {}, Lcom/dropbox/android/filemanager/au;->a()Lcom/dropbox/android/filemanager/au;

    move-result-object v1

    .line 29
    sget-object v2, Lcom/dropbox/android/filemanager/aC;->a:Lcom/dropbox/android/filemanager/aC;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/aC;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/analytics/r;)V

    .line 30
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->g()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/au;->a(Ldbxyzptlk/db231222/n/K;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    invoke-static {p1}, Lcom/dropbox/android/service/ReportReceiver;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 32
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 33
    invoke-static {v1, v0}, Lcom/dropbox/android/service/ReportReceiver;->a(Landroid/app/PendingIntent;Landroid/app/AlarmManager;)V

    .line 36
    :cond_0
    return-void
.end method
