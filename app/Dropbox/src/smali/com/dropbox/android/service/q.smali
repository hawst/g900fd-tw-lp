.class final Lcom/dropbox/android/service/q;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final a:[Ljava/lang/String;


# instance fields
.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1441
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "server_hash"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "uploaded"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ignored"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_hash"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/service/q;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1434
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/dropbox/android/service/q;->b:J

    .line 1435
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/dropbox/android/service/q;->d:Ljava/lang/String;

    .line 1436
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/dropbox/android/service/q;->e:Z

    .line 1437
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    :goto_2
    iput-boolean v2, p0, Lcom/dropbox/android/service/q;->f:Z

    .line 1438
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-object v1, p0, Lcom/dropbox/android/service/q;->c:Ljava/lang/String;

    .line 1439
    return-void

    .line 1435
    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 1436
    goto :goto_1

    :cond_2
    move v2, v3

    .line 1437
    goto :goto_2

    .line 1438
    :cond_3
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method static synthetic a(Lcom/dropbox/android/service/q;)Z
    .locals 1

    .prologue
    .line 1422
    iget-boolean v0, p0, Lcom/dropbox/android/service/q;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/service/q;)Z
    .locals 1

    .prologue
    .line 1422
    iget-boolean v0, p0, Lcom/dropbox/android/service/q;->f:Z

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/service/q;)J
    .locals 2

    .prologue
    .line 1422
    iget-wide v0, p0, Lcom/dropbox/android/service/q;->b:J

    return-wide v0
.end method

.method static synthetic d(Lcom/dropbox/android/service/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1422
    iget-object v0, p0, Lcom/dropbox/android/service/q;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/service/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1422
    iget-object v0, p0, Lcom/dropbox/android/service/q;->c:Ljava/lang/String;

    return-object v0
.end method
