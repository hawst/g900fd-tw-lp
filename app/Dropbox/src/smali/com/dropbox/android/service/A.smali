.class public final Lcom/dropbox/android/service/A;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/ContentResolver;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/C;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/dropbox/android/service/A;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/service/A;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 3
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;

    .line 112
    iput-object p1, p0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    .line 114
    new-instance v0, Lcom/dropbox/android/service/B;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/service/B;-><init>(Lcom/dropbox/android/service/A;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dropbox/android/service/A;->d:Landroid/database/ContentObserver;

    .line 128
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dropbox/android/service/A;->d:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 129
    return-void
.end method

.method static synthetic a(Landroid/net/Uri;Landroid/content/ContentResolver;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 40
    invoke-static {p0, p1}, Lcom/dropbox/android/service/A;->b(Landroid/net/Uri;Landroid/content/ContentResolver;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/ContentResolver;)Ljava/util/LinkedHashMap;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/dropbox/android/service/E;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 426
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v7

    const-string v0, "display_name"

    aput-object v0, v2, v6

    const/4 v0, 0x2

    const-string v1, "times_contacted"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "photo_thumb_uri"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "in_visible_group"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "starred"

    aput-object v1, v2, v0

    .line 433
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 439
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 440
    const-string v0, "display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 441
    const-string v0, "times_contacted"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 442
    const-string v0, "in_visible_group"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 443
    const-string v0, "starred"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 444
    const-string v0, "photo_thumb_uri"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 446
    new-instance v11, Ljava/util/LinkedHashMap;

    invoke-direct {v11}, Ljava/util/LinkedHashMap;-><init>()V

    .line 448
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 449
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 450
    new-instance v12, Lcom/dropbox/android/service/E;

    invoke-direct {v12, p0}, Lcom/dropbox/android/service/E;-><init>(Landroid/content/ContentResolver;)V

    .line 451
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v12, v0}, Lcom/dropbox/android/service/E;->a(I)V

    .line 452
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Lcom/dropbox/android/service/E;->a(Ljava/lang/String;)V

    .line 454
    invoke-virtual {v12}, Lcom/dropbox/android/service/E;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v12}, Lcom/dropbox/android/service/E;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-virtual {v12, v3}, Lcom/dropbox/android/service/E;->a(Ljava/lang/String;)V

    .line 457
    :cond_0
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v12, v0}, Lcom/dropbox/android/service/E;->b(I)V

    .line 458
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Lcom/dropbox/android/service/E;->b(Ljava/lang/String;)V

    .line 459
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v6

    :goto_1
    invoke-virtual {v12, v0}, Lcom/dropbox/android/service/E;->b(Z)V

    .line 460
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v6

    :goto_2
    invoke-virtual {v12, v0}, Lcom/dropbox/android/service/E;->a(Z)V

    .line 462
    invoke-virtual {v12}, Lcom/dropbox/android/service/E;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v11, v0, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_1
    move v0, v7

    .line 459
    goto :goto_1

    :cond_2
    move v0, v7

    .line 460
    goto :goto_2

    .line 465
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 467
    return-object v11
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/dropbox/android/service/D;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/dropbox/android/service/D",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 560
    invoke-static {p0}, Lcom/dropbox/android/service/A;->a(Landroid/content/ContentResolver;)Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 561
    invoke-static {p0, v1}, Lcom/dropbox/android/service/A;->a(Landroid/content/ContentResolver;Ljava/util/LinkedHashMap;)V

    .line 562
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 563
    invoke-static {p0, v0}, Lcom/dropbox/android/service/A;->a(Landroid/content/ContentResolver;Ljava/util/List;)V

    .line 566
    if-eqz p1, :cond_1

    .line 567
    invoke-interface {p1, v0}, Lcom/dropbox/android/service/D;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 571
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 572
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->i()V

    goto :goto_1

    .line 575
    :cond_0
    return-object v1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/service/A;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;

    return-object p1
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/util/LinkedHashMap;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/dropbox/android/service/E;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 478
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v7

    const-string v0, "data1"

    aput-object v0, v2, v6

    const/4 v0, 0x2

    const-string v1, "is_super_primary"

    aput-object v1, v2, v0

    .line 481
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 487
    const-string v0, "contact_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 488
    const-string v0, "data1"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 489
    const-string v0, "is_super_primary"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 490
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 492
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 493
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_5

    .line 494
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 495
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 496
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 498
    invoke-static {v9}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 499
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v10

    .line 500
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3

    move v1, v6

    .line 502
    :goto_1
    if-eqz v1, :cond_4

    .line 503
    invoke-interface {v10, v9}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 504
    const/4 v10, -0x1

    if-eq v1, v10, :cond_0

    .line 505
    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/E;->c(I)V

    .line 507
    :cond_0
    invoke-virtual {v0, v9}, Lcom/dropbox/android/service/E;->c(Ljava/lang/String;)V

    .line 511
    :cond_1
    :goto_2
    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 513
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_3
    move v1, v7

    .line 500
    goto :goto_1

    .line 508
    :cond_4
    invoke-interface {v10, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 509
    invoke-virtual {v0, v9}, Lcom/dropbox/android/service/E;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 515
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 516
    return-void
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 527
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v6

    const-string v3, "data15 IS NOT NULL AND mimetype=?"

    new-array v4, v4, [Ljava/lang/String;

    const-string v0, "vnd.android.cursor.item/photo"

    aput-object v0, v4, v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 533
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 534
    const-string v2, "contact_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 536
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 537
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 538
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 539
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 540
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 542
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 544
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 545
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 546
    invoke-virtual {v0, v5}, Lcom/dropbox/android/service/E;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 549
    :cond_2
    return-void
.end method

.method private static b(Landroid/net/Uri;Landroid/content/ContentResolver;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 80
    if-nez p0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-object v3

    .line 84
    :cond_1
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "data15"

    aput-object v0, v2, v6

    move-object v0, p1

    move-object v1, p0

    move-object v4, v3

    move-object v5, v3

    .line 85
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 86
    if-eqz v1, :cond_0

    .line 92
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 96
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 99
    if-eqz v0, :cond_0

    .line 100
    array-length v1, v0

    invoke-static {v0, v6, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    if-nez v1, :cond_0

    .line 160
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "This object was already closed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 163
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 164
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    :goto_0
    monitor-exit p0

    return-object v1

    .line 167
    :cond_1
    const/16 v1, 0x8

    :try_start_2
    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "display_name"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "data2"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "data3"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "photo_thumb_uri"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string v2, "display_name_source"

    aput-object v2, v3, v1

    .line 178
    const-string v4, "data1 != \'\' AND data1 IS NOT NULL"

    .line 179
    const-string v6, "display_name COLLATE NOCASE, contact_id, data1 COLLATE NOCASE"

    .line 182
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 184
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 185
    if-nez v14, :cond_2

    .line 186
    invoke-static {v15}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;

    .line 187
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 191
    :cond_2
    :try_start_3
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 193
    const-string v1, "display_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 194
    const-string v1, "data1"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 195
    const-string v1, "_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 196
    const-string v1, "contact_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 197
    const-string v1, "data2"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 198
    const-string v1, "data3"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 200
    const-string v1, "photo_thumb_uri"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 201
    const-string v1, "photo_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 202
    const-string v1, "display_name_source"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 204
    const/4 v1, -0x1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 205
    :cond_3
    :goto_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 206
    move/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v2, ""

    .line 207
    :goto_2
    move/from16 v0, v18

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 208
    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 209
    move/from16 v0, v20

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 210
    move/from16 v0, v21

    invoke-interface {v14, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v4, 0x3

    .line 211
    :goto_3
    move/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v5, ""

    .line 213
    :goto_4
    const/4 v10, 0x0

    .line 216
    move/from16 v0, v23

    invoke-interface {v14, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 217
    move/from16 v0, v23

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 221
    :cond_4
    const/16 v11, 0x23

    .line 222
    const/4 v1, -0x1

    move/from16 v0, v24

    if-eq v0, v1, :cond_5

    move/from16 v0, v24

    invoke-interface {v14, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 223
    move/from16 v0, v24

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 226
    :cond_5
    invoke-static {v3}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 230
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v12}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 231
    new-instance v1, Lcom/dropbox/android/service/C;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    const/4 v13, 0x0

    invoke-direct/range {v1 .. v13}, Lcom/dropbox/android/service/C;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ILandroid/content/ContentResolver;Lcom/dropbox/android/service/B;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_1

    .line 236
    :catchall_1
    move-exception v1

    :try_start_4
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 206
    :cond_6
    :try_start_5
    move/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 210
    :cond_7
    move/from16 v0, v21

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    goto :goto_3

    .line 211
    :cond_8
    move/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v5

    goto :goto_4

    .line 236
    :cond_9
    :try_start_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 239
    invoke-static {v15}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;

    .line 240
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized close()V
    .locals 2

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This object has already been closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/dropbox/android/service/A;->d:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/service/A;->c:Ljava/util/List;

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/service/A;->d:Landroid/database/ContentObserver;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    monitor-exit p0

    return-void
.end method

.method protected final finalize()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/dropbox/android/service/A;->b:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lcom/dropbox/android/service/A;->a:Ljava/lang/String;

    const-string v1, "Contact store object was never closed"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 137
    return-void
.end method
