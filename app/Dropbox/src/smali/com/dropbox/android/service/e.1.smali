.class public final Lcom/dropbox/android/service/e;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/dropbox/android/service/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/dropbox/android/service/e;

.field public static final b:Lcom/dropbox/android/service/e;

.field public static final c:Lcom/dropbox/android/service/e;

.field public static final d:Lcom/dropbox/android/service/e;

.field public static final e:Lcom/dropbox/android/service/e;


# instance fields
.field private final f:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    new-instance v0, Lcom/dropbox/android/service/e;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/service/e;-><init>(J)V

    sput-object v0, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    .line 72
    new-instance v0, Lcom/dropbox/android/service/e;

    const-wide/16 v1, 0x1388

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/service/e;-><init>(J)V

    sput-object v0, Lcom/dropbox/android/service/e;->b:Lcom/dropbox/android/service/e;

    .line 74
    new-instance v0, Lcom/dropbox/android/service/e;

    const-wide/32 v1, 0xdbba0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/service/e;-><init>(J)V

    sput-object v0, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    .line 76
    new-instance v0, Lcom/dropbox/android/service/e;

    const-wide/32 v1, 0x5265c00

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/service/e;-><init>(J)V

    sput-object v0, Lcom/dropbox/android/service/e;->d:Lcom/dropbox/android/service/e;

    .line 78
    new-instance v0, Lcom/dropbox/android/service/e;

    const-wide v1, 0x7fffffffffffffffL

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/service/e;-><init>(J)V

    sput-object v0, Lcom/dropbox/android/service/e;->e:Lcom/dropbox/android/service/e;

    return-void
.end method

.method private constructor <init>(J)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-wide p1, p0, Lcom/dropbox/android/service/e;->f:J

    .line 84
    return-void
.end method

.method public static a(Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)Lcom/dropbox/android/service/e;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/dropbox/android/service/e;->a(Lcom/dropbox/android/service/e;)I

    move-result v0

    if-gez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    move-object p0, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/dropbox/android/service/e;->f:J

    sub-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/dropbox/android/service/e;)I
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/dropbox/android/service/e;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/service/e;->a(J)I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/dropbox/android/service/e;->f:J

    return-wide v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 68
    check-cast p1, Lcom/dropbox/android/service/e;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/service/e;->a(Lcom/dropbox/android/service/e;)I

    move-result v0

    return v0
.end method
