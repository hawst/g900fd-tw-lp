.class final Lcom/dropbox/android/service/w;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/LinkedList;

.field final synthetic b:Z

.field final synthetic c:Landroid/database/sqlite/SQLiteDatabase;

.field final synthetic d:Ljava/util/ArrayList;

.field final synthetic e:Lcom/dropbox/android/service/v;


# direct methods
.method constructor <init>(Lcom/dropbox/android/service/v;Ljava/util/LinkedList;ZLandroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1014
    iput-object p1, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iput-object p2, p0, Lcom/dropbox/android/service/w;->a:Ljava/util/LinkedList;

    iput-boolean p3, p0, Lcom/dropbox/android/service/w;->b:Z

    iput-object p4, p0, Lcom/dropbox/android/service/w;->c:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p5, p0, Lcom/dropbox/android/service/w;->d:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const-wide/16 v1, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 1017
    iget-object v0, p0, Lcom/dropbox/android/service/w;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1018
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v0, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->f(Lcom/dropbox/android/service/CameraUploadService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0, v8}, Ldbxyzptlk/db231222/n/P;->g(Z)V

    .line 1021
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v0, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->e()V

    .line 1061
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/service/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1062
    iget-object v0, p0, Lcom/dropbox/android/service/w;->c:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lcom/dropbox/android/service/w;->d:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/CameraUploadService;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V

    .line 1065
    :cond_1
    iget-boolean v0, p0, Lcom/dropbox/android/service/w;->b:Z

    if-eqz v0, :cond_2

    .line 1067
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0, v8}, Ldbxyzptlk/db231222/n/P;->n(Z)V

    .line 1070
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1071
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0, v8}, Ldbxyzptlk/db231222/n/P;->i(Z)V

    .line 1073
    :cond_3
    return-void

    .line 1025
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->q()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v0, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->f(Lcom/dropbox/android/service/CameraUploadService;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1026
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v0, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0, v4}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Z)Z

    .line 1035
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v0, v0, Lcom/dropbox/android/service/v;->c:Ldbxyzptlk/db231222/u/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/u/c;->a()J

    move-result-wide v5

    .line 1036
    iget-object v0, p0, Lcom/dropbox/android/service/w;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/o;

    .line 1037
    invoke-static {v0}, Lcom/dropbox/android/service/o;->a(Lcom/dropbox/android/service/o;)J

    move-result-wide v9

    cmp-long v7, v9, v1

    if-lez v7, :cond_7

    .line 1038
    invoke-static {v0}, Lcom/dropbox/android/service/o;->a(Lcom/dropbox/android/service/o;)J

    move-result-wide v9

    sub-long v9, v5, v9

    .line 1039
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->b()J

    move-result-wide v11

    cmp-long v0, v9, v11

    if-gez v0, :cond_6

    .line 1040
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->b()J

    move-result-wide v0

    move-wide v6, v0

    .line 1054
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v0, v0, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->h(Lcom/dropbox/android/service/CameraUploadService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1055
    iget-boolean v0, p0, Lcom/dropbox/android/service/w;->b:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v0}, Lcom/dropbox/android/service/v;->a(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->x()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1056
    :goto_2
    new-instance v0, Lcom/dropbox/android/service/s;

    iget-object v1, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v1, v1, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    iget-object v2, p0, Lcom/dropbox/android/service/w;->a:Ljava/util/LinkedList;

    iget-object v3, p0, Lcom/dropbox/android/service/w;->c:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v5, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    invoke-static {v5}, Lcom/dropbox/android/service/v;->b(Lcom/dropbox/android/service/v;)Ldbxyzptlk/db231222/r/d;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/service/s;-><init>(Lcom/dropbox/android/service/CameraUploadService;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;ZLdbxyzptlk/db231222/r/d;)V

    .line 1057
    iget-object v1, p0, Lcom/dropbox/android/service/w;->e:Lcom/dropbox/android/service/v;

    iget-object v1, v1, Lcom/dropbox/android/service/v;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v1}, Lcom/dropbox/android/service/CameraUploadService;->i(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1, v0, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 1044
    :cond_7
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->b()J

    move-result-wide v0

    move-wide v6, v0

    .line 1045
    goto :goto_1

    :cond_8
    move v4, v8

    .line 1055
    goto :goto_2

    :cond_9
    move-wide v6, v1

    goto :goto_1
.end method
