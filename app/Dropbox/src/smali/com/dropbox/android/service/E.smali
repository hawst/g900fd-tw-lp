.class public final Lcom/dropbox/android/service/E;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Landroid/graphics/Bitmap;

.field private i:Z

.field private j:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput v1, p0, Lcom/dropbox/android/service/E;->a:I

    .line 258
    iput-object v2, p0, Lcom/dropbox/android/service/E;->b:Ljava/lang/String;

    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/E;->c:Ljava/util/List;

    .line 262
    iput v1, p0, Lcom/dropbox/android/service/E;->d:I

    .line 264
    iput-boolean v1, p0, Lcom/dropbox/android/service/E;->e:Z

    .line 266
    iput-boolean v1, p0, Lcom/dropbox/android/service/E;->f:Z

    .line 268
    iput-object v2, p0, Lcom/dropbox/android/service/E;->g:Ljava/lang/String;

    .line 270
    iput-object v2, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;

    .line 275
    iput-boolean v1, p0, Lcom/dropbox/android/service/E;->i:Z

    .line 280
    iput-object p1, p0, Lcom/dropbox/android/service/E;->j:Landroid/content/ContentResolver;

    .line 281
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/dropbox/android/service/E;->i:Z

    if-eqz v0, :cond_0

    .line 414
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 416
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 304
    iget v0, p0, Lcom/dropbox/android/service/E;->a:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 341
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 342
    iput p1, p0, Lcom/dropbox/android/service/E;->a:I

    .line 343
    return-void
.end method

.method public final a(Lcom/dropbox/android/service/D;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/service/D",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 288
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/service/E;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/service/E;->j:Landroid/content/ContentResolver;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/A;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;

    .line 291
    if-eqz p1, :cond_0

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;

    invoke-interface {p1, v0}, Lcom/dropbox/android/service/D;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 298
    iput-object v2, p0, Lcom/dropbox/android/service/E;->g:Ljava/lang/String;

    .line 300
    :cond_1
    return-void

    .line 294
    :catch_0
    move-exception v0

    .line 295
    iput-object v2, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 347
    iput-object p1, p0, Lcom/dropbox/android/service/E;->b:Ljava/lang/String;

    .line 348
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 362
    iput-boolean p1, p0, Lcom/dropbox/android/service/E;->e:Z

    .line 363
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/dropbox/android/service/E;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 357
    iput p1, p0, Lcom/dropbox/android/service/E;->d:I

    .line 358
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 366
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 367
    iput-object p1, p0, Lcom/dropbox/android/service/E;->g:Ljava/lang/String;

    .line 368
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 377
    iput-boolean p1, p0, Lcom/dropbox/android/service/E;->f:Z

    .line 378
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/service/E;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 406
    iget-object v0, p0, Lcom/dropbox/android/service/E;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 407
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 390
    iget-object v0, p0, Lcom/dropbox/android/service/E;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 391
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/dropbox/android/service/E;->d:I

    return v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/dropbox/android/service/E;->j()V

    .line 398
    iget-object v0, p0, Lcom/dropbox/android/service/E;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/dropbox/android/service/E;->e:Z

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/dropbox/android/service/E;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/dropbox/android/service/E;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/dropbox/android/service/E;->f:Z

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/service/E;->i:Z

    .line 382
    return-void
.end method
