.class public final Lcom/dropbox/android/service/s;
.super Lcom/dropbox/android/service/n;
.source "panda.py"


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/o;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/database/sqlite/SQLiteDatabase;

.field private final d:Z

.field private final e:Ldbxyzptlk/db231222/r/d;

.field private final f:Ldbxyzptlk/db231222/n/P;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/service/CameraUploadService;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;ZLdbxyzptlk/db231222/r/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/service/CameraUploadService;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/o;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Z",
            "Ldbxyzptlk/db231222/r/d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1110
    invoke-static {p1}, Lcom/dropbox/android/service/CameraUploadService;->j(Lcom/dropbox/android/service/CameraUploadService;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/service/n;-><init>(Lcom/dropbox/android/service/CameraUploadService;I)V

    .line 1111
    iput-object p2, p0, Lcom/dropbox/android/service/s;->b:Ljava/util/List;

    .line 1112
    iput-object p3, p0, Lcom/dropbox/android/service/s;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 1113
    iput-boolean p4, p0, Lcom/dropbox/android/service/s;->d:Z

    .line 1114
    iput-object p5, p0, Lcom/dropbox/android/service/s;->e:Ldbxyzptlk/db231222/r/d;

    .line 1115
    invoke-virtual {p5}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/service/s;->f:Ldbxyzptlk/db231222/n/P;

    .line 1116
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/service/s;)Ldbxyzptlk/db231222/n/P;
    .locals 1

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/dropbox/android/service/s;->f:Ldbxyzptlk/db231222/n/P;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/service/s;)Z
    .locals 1

    .prologue
    .line 1097
    iget-boolean v0, p0, Lcom/dropbox/android/service/s;->d:Z

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1120
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 1121
    new-instance v1, Lcom/dropbox/android/service/z;

    iget-object v0, p0, Lcom/dropbox/android/service/s;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v0}, Lcom/dropbox/android/service/CameraUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/service/s;->e:Ldbxyzptlk/db231222/r/d;

    iget-object v3, p0, Lcom/dropbox/android/service/s;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/service/s;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v4}, Lcom/dropbox/android/service/CameraUploadService;->k(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/HashMap;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/dropbox/android/service/z;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/taskqueue/U;Ljava/util/Map;)V

    .line 1124
    iget-object v0, p0, Lcom/dropbox/android/service/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/o;

    .line 1125
    invoke-virtual {v0}, Lcom/dropbox/android/service/o;->a()V

    .line 1126
    invoke-static {v0}, Lcom/dropbox/android/service/o;->b(Lcom/dropbox/android/service/o;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1131
    iget-object v3, p0, Lcom/dropbox/android/service/s;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/o;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1133
    iget-object v3, p0, Lcom/dropbox/android/service/s;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/o;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1136
    :cond_1
    iget-object v3, p0, Lcom/dropbox/android/service/s;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/o;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1139
    new-instance v3, Lcom/dropbox/android/service/t;

    invoke-direct {v3, p0, v1, v0}, Lcom/dropbox/android/service/t;-><init>(Lcom/dropbox/android/service/s;Lcom/dropbox/android/service/z;Lcom/dropbox/android/service/o;)V

    invoke-virtual {p0, v3}, Lcom/dropbox/android/service/s;->a(Ljava/lang/Runnable;)V

    .line 1155
    iget-object v0, p0, Lcom/dropbox/android/service/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x5

    if-le v0, v3, :cond_0

    .line 1158
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    .line 1162
    :cond_2
    new-instance v0, Lcom/dropbox/android/service/u;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/service/u;-><init>(Lcom/dropbox/android/service/s;Lcom/dropbox/android/service/z;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/service/s;->a(Ljava/lang/Runnable;)V

    .line 1169
    iget-object v0, p0, Lcom/dropbox/android/service/s;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0, v5}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Z)Z

    .line 1170
    iget-object v0, p0, Lcom/dropbox/android/service/s;->f:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/n/P;->g(Z)V

    .line 1171
    iget-object v0, p0, Lcom/dropbox/android/service/s;->a:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->h()Lcom/dropbox/android/taskqueue/U;

    move-result-object v0

    .line 1172
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/U;->e()V

    .line 1173
    iget-object v1, p0, Lcom/dropbox/android/service/s;->e:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->t()Lcom/dropbox/android/notifications/d;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/service/s;->f:Ldbxyzptlk/db231222/n/P;

    invoke-static {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Lcom/dropbox/android/notifications/d;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/taskqueue/U;)V

    .line 1175
    return-void
.end method
