.class public final Lcom/dropbox/android/service/a;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/r/d;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Ljava/lang/Object;

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/dropbox/android/service/d;",
            "Lcom/dropbox/android/service/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/atomic/AtomicLong;

.field private g:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/r/d;Landroid/content/Context;)V
    .locals 5

    .prologue
    const-wide/16 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/a;->d:Ljava/lang/Object;

    .line 50
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/a;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 57
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/dropbox/android/service/a;->f:Ljava/util/concurrent/atomic/AtomicLong;

    .line 63
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/dropbox/android/service/a;->g:Ljava/util/concurrent/atomic/AtomicLong;

    .line 130
    iput-object p1, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    .line 131
    iput-object p2, p0, Lcom/dropbox/android/service/a;->b:Landroid/content/Context;

    .line 134
    new-instance v0, Lcom/dropbox/android/service/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/b;-><init>(Lcom/dropbox/android/service/a;)V

    iput-object v0, p0, Lcom/dropbox/android/service/a;->c:Landroid/content/BroadcastReceiver;

    .line 152
    :try_start_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.dropbox.android.action.ACCOUNT_INFO"

    const-string v2, "text/plain"

    invoke-direct {v0, v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v1, "dropbox"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 154
    const-string v1, "accountinfo"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    .line 155
    iget-object v1, p0, Lcom/dropbox/android/service/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/service/a;->c:Landroid/content/BroadcastReceiver;

    const-string v3, "com.dropbox.android.service.ACCOUNT_INFO_ALARM_TRIGGER"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 158
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/service/a;)Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method private a(Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    .locals 5

    .prologue
    .line 238
    iget-object v0, p0, Lcom/dropbox/android/service/a;->d:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 239
    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 241
    iget-object v1, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->k()Ldbxyzptlk/db231222/y/k;

    move-result-object v1

    .line 245
    if-nez v1, :cond_0

    .line 246
    :try_start_0
    new-instance v0, Ldbxyzptlk/db231222/w/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/j;-><init>(Lorg/apache/http/HttpResponse;)V

    throw v0
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :catch_0
    move-exception v0

    .line 256
    iget-object v1, p0, Lcom/dropbox/android/service/a;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 257
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->g()V

    .line 258
    throw v0

    .line 249
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/M;->d()Ldbxyzptlk/db231222/z/R;

    move-result-object v2

    .line 250
    iget-object v3, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->h()Z

    move-result v3

    if-nez v3, :cond_1

    .line 253
    new-instance v0, Ldbxyzptlk/db231222/w/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/j;-><init>(Lorg/apache/http/HttpResponse;)V

    throw v0
    :try_end_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1 .. :try_end_1} :catch_0

    .line 264
    :cond_1
    invoke-virtual {p1, v2, v1}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/z/R;Ldbxyzptlk/db231222/y/k;)Ldbxyzptlk/db231222/r/d;

    .line 266
    iget-object v1, v2, Ldbxyzptlk/db231222/z/R;->a:Ldbxyzptlk/db231222/s/a;

    .line 267
    iget-object v2, p0, Lcom/dropbox/android/service/a;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->a()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 268
    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/service/a;->a(Ldbxyzptlk/db231222/s/a;Ldbxyzptlk/db231222/s/a;)V

    .line 271
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->g()V

    .line 273
    return-object v1
.end method

.method private a(Ldbxyzptlk/db231222/s/a;Ldbxyzptlk/db231222/s/a;)V
    .locals 2

    .prologue
    .line 324
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 333
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/a;->J()[B

    move-result-object v0

    invoke-virtual {p2}, Ldbxyzptlk/db231222/s/a;->J()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/dropbox/android/service/a;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/d;

    .line 335
    invoke-interface {v0, p2}, Lcom/dropbox/android/service/d;->a(Ldbxyzptlk/db231222/s/a;)V

    goto :goto_0

    .line 338
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/service/a;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->h()V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/service/a;)Lcom/dropbox/android/service/e;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->f()Lcom/dropbox/android/service/e;

    move-result-object v0

    return-object v0
.end method

.method private f()Lcom/dropbox/android/service/e;
    .locals 3

    .prologue
    .line 344
    sget-object v0, Lcom/dropbox/android/service/e;->e:Lcom/dropbox/android/service/e;

    .line 345
    iget-object v1, p0, Lcom/dropbox/android/service/a;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/e;

    .line 346
    invoke-static {v1, v0}, Lcom/dropbox/android/service/e;->a(Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)Lcom/dropbox/android/service/e;

    move-result-object v0

    move-object v1, v0

    .line 347
    goto :goto_0

    .line 348
    :cond_0
    return-object v1
.end method

.method private g()V
    .locals 7

    .prologue
    .line 355
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.android.action.ACCOUNT_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 363
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 364
    const-string v2, "dropbox"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 365
    const-string v2, "accountinfo"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 366
    const-string v2, "userID"

    iget-object v3, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 367
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "text/plain"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    iget-object v1, p0, Lcom/dropbox/android/service/a;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 374
    iget-object v0, p0, Lcom/dropbox/android/service/a;->b:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 376
    monitor-enter p0

    .line 377
    :try_start_0
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 379
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->f()Lcom/dropbox/android/service/e;

    move-result-object v1

    .line 381
    sget-object v2, Lcom/dropbox/android/service/e;->e:Lcom/dropbox/android/service/e;

    if-eq v1, v2, :cond_0

    .line 384
    iget-object v2, p0, Lcom/dropbox/android/service/a;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-object v4, p0, Lcom/dropbox/android/service/a;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 387
    invoke-virtual {v1}, Lcom/dropbox/android/service/e;->a()J

    move-result-wide v4

    .line 389
    const/4 v1, 0x3

    add-long/2addr v2, v4

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 392
    :cond_0
    monitor-exit p0

    .line 393
    return-void

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 399
    new-instance v0, Lcom/dropbox/android/service/c;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/c;-><init>(Lcom/dropbox/android/service/a;)V

    invoke-virtual {v0}, Lcom/dropbox/android/service/c;->start()V

    .line 410
    return-void
.end method


# virtual methods
.method protected final a()J
    .locals 2

    .prologue
    .line 167
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    .locals 4

    .prologue
    .line 184
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 186
    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/dropbox/android/service/a;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 187
    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/service/e;->a(J)I

    move-result v2

    if-gtz v2, :cond_1

    .line 189
    iget-object v2, p0, Lcom/dropbox/android/service/a;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 190
    :try_start_0
    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/service/e;->a(J)I

    move-result v0

    if-gtz v0, :cond_0

    .line 193
    invoke-direct {p0, p2}, Lcom/dropbox/android/service/a;->a(Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    monitor-exit v2

    .line 199
    :goto_0
    return-object v0

    .line 195
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/service/d;)V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/dropbox/android/service/a;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 310
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->g()V

    .line 311
    return-void

    .line 309
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/d;)V
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/dropbox/android/service/e;->c:Lcom/dropbox/android/service/e;

    invoke-virtual {p1, v0}, Lcom/dropbox/android/service/e;->a(Lcom/dropbox/android/service/e;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/service/a;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    invoke-direct {p0}, Lcom/dropbox/android/service/a;->g()V

    .line 299
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ldbxyzptlk/db231222/s/a;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/dropbox/android/service/a;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->g()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 6

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 220
    const/4 v0, 0x0

    .line 221
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v1

    .line 223
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v2

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 224
    const/4 v0, 0x1

    .line 227
    :cond_0
    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/dropbox/android/service/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/dropbox/android/service/a;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 418
    return-void
.end method

.method public final e()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 430
    invoke-virtual {p0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 431
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v2

    if-nez v2, :cond_1

    .line 438
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v1

    const-wide v3, 0x1680000000L

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
