.class public final Lcom/dropbox/android/service/C;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:J

.field public final g:Landroid/net/Uri;

.field public final h:I

.field private final i:Landroid/content/ContentResolver;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ILandroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/dropbox/android/service/C;->a:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/dropbox/android/service/C;->b:Ljava/lang/String;

    .line 63
    iput p3, p0, Lcom/dropbox/android/service/C;->c:I

    .line 64
    iput-object p4, p0, Lcom/dropbox/android/service/C;->d:Ljava/lang/String;

    .line 65
    iput-wide p5, p0, Lcom/dropbox/android/service/C;->e:J

    .line 66
    iput-wide p7, p0, Lcom/dropbox/android/service/C;->f:J

    .line 67
    iput-object p9, p0, Lcom/dropbox/android/service/C;->g:Landroid/net/Uri;

    .line 68
    iput p10, p0, Lcom/dropbox/android/service/C;->h:I

    .line 69
    iput-object p11, p0, Lcom/dropbox/android/service/C;->i:Landroid/content/ContentResolver;

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ILandroid/content/ContentResolver;Lcom/dropbox/android/service/B;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct/range {p0 .. p11}, Lcom/dropbox/android/service/C;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ILandroid/content/ContentResolver;)V

    return-void
.end method
