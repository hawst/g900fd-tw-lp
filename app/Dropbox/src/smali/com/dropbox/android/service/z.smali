.class final Lcom/dropbox/android/service/z;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:I

.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/service/m;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Ldbxyzptlk/db231222/r/d;

.field private final e:Lcom/dropbox/android/taskqueue/U;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/dropbox/android/service/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/taskqueue/U;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ldbxyzptlk/db231222/r/d;",
            "Lcom/dropbox/android/taskqueue/U;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/dropbox/android/service/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1242
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/service/z;->a:I

    .line 1243
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    .line 1254
    iput-object p1, p0, Lcom/dropbox/android/service/z;->c:Landroid/content/Context;

    .line 1255
    iput-object p2, p0, Lcom/dropbox/android/service/z;->d:Ldbxyzptlk/db231222/r/d;

    .line 1256
    iput-object p3, p0, Lcom/dropbox/android/service/z;->e:Lcom/dropbox/android/taskqueue/U;

    .line 1257
    iput-object p4, p0, Lcom/dropbox/android/service/z;->f:Ljava/util/Map;

    .line 1258
    return-void
.end method

.method private a(Lcom/dropbox/android/service/m;I)Lcom/dropbox/android/taskqueue/CameraUploadTask;
    .locals 10

    .prologue
    .line 1410
    new-instance v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    iget-object v1, p0, Lcom/dropbox/android/service/z;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/service/z;->d:Ldbxyzptlk/db231222/r/d;

    invoke-static {p1}, Lcom/dropbox/android/service/m;->d(Lcom/dropbox/android/service/m;)Ljava/io/File;

    move-result-object v3

    invoke-static {p1}, Lcom/dropbox/android/service/m;->e(Lcom/dropbox/android/service/m;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/dropbox/android/service/m;->f(Lcom/dropbox/android/service/m;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lcom/dropbox/android/service/m;->g(Lcom/dropbox/android/service/m;)J

    move-result-wide v6

    invoke-static {p1}, Lcom/dropbox/android/service/m;->h(Lcom/dropbox/android/service/m;)Ljava/lang/String;

    move-result-object v8

    move v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/dropbox/android/taskqueue/CameraUploadTask;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V

    return-object v0
.end method

.method private a(Ljava/util/LinkedList;)Ljava/util/LinkedList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/service/m;",
            ">;)",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/dropbox/android/service/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1392
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 1394
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/m;

    .line 1395
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/service/m;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/m;->b(Lcom/dropbox/android/service/m;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1397
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1398
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1399
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1402
    :cond_1
    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1405
    :cond_2
    return-object v2
.end method

.method private a(Z)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1287
    iget-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1288
    iget-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-direct {p0, v0}, Lcom/dropbox/android/service/z;->a(Ljava/util/LinkedList;)Ljava/util/LinkedList;

    move-result-object v9

    .line 1290
    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gt v0, v8, :cond_0

    if-eqz p1, :cond_c

    .line 1291
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1292
    iget-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1294
    if-nez p1, :cond_1

    .line 1297
    iget-object v1, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    move v3, v4

    .line 1300
    :goto_0
    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    .line 1301
    invoke-virtual {v9, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 1302
    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_2

    move v7, v8

    .line 1303
    :goto_1
    const/4 v5, 0x0

    .line 1304
    if-nez v3, :cond_e

    .line 1309
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/service/m;

    invoke-static {v1}, Lcom/dropbox/android/service/m;->c(Lcom/dropbox/android/service/m;)Landroid/net/Uri;

    move-result-object v1

    .line 1310
    iget-object v2, p0, Lcom/dropbox/android/service/z;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/service/p;

    .line 1311
    if-eqz v1, :cond_e

    invoke-static {v1}, Lcom/dropbox/android/service/p;->a(Lcom/dropbox/android/service/p;)Lcom/dropbox/android/service/m;

    move-result-object v10

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dropbox/android/service/m;

    invoke-virtual {v10, v2}, Lcom/dropbox/android/service/m;->b(Lcom/dropbox/android/service/m;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1316
    :goto_2
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-le v2, v8, :cond_6

    .line 1319
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1323
    if-nez v1, :cond_3

    move v1, v4

    .line 1328
    :goto_3
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/service/m;

    .line 1329
    add-int/lit8 v2, v2, 0x1

    .line 1330
    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/service/z;->a(Lcom/dropbox/android/service/m;I)Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_2
    move v7, v4

    .line 1302
    goto :goto_1

    .line 1326
    :cond_3
    invoke-static {v1}, Lcom/dropbox/android/service/p;->b(Lcom/dropbox/android/service/p;)I

    move-result v1

    goto :goto_3

    .line 1341
    :cond_4
    if-eqz v7, :cond_5

    .line 1342
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/m;

    .line 1343
    iget-object v1, p0, Lcom/dropbox/android/service/z;->f:Ljava/util/Map;

    invoke-static {v0}, Lcom/dropbox/android/service/m;->c(Lcom/dropbox/android/service/m;)Landroid/net/Uri;

    move-result-object v5

    new-instance v7, Lcom/dropbox/android/service/p;

    invoke-direct {v7, v0, v2}, Lcom/dropbox/android/service/p;-><init>(Lcom/dropbox/android/service/m;I)V

    invoke-interface {v1, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300
    :cond_5
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 1349
    :cond_6
    if-nez v1, :cond_7

    .line 1350
    const/4 v1, -0x1

    .line 1361
    :goto_6
    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/m;

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/service/z;->a(Lcom/dropbox/android/service/m;I)Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1352
    :cond_7
    invoke-static {v1}, Lcom/dropbox/android/service/p;->b(Lcom/dropbox/android/service/p;)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    .line 1354
    if-eqz v7, :cond_8

    .line 1356
    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/service/m;

    .line 1357
    iget-object v5, p0, Lcom/dropbox/android/service/z;->f:Ljava/util/Map;

    invoke-static {v1}, Lcom/dropbox/android/service/m;->c(Lcom/dropbox/android/service/m;)Landroid/net/Uri;

    move-result-object v7

    new-instance v10, Lcom/dropbox/android/service/p;

    invoke-direct {v10, v1, v2}, Lcom/dropbox/android/service/p;-><init>(Lcom/dropbox/android/service/m;I)V

    invoke-interface {v5, v7, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    move v1, v2

    goto :goto_6

    .line 1365
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/service/z;->d:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1370
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1371
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    .line 1372
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g()Z

    move-result v3

    if-nez v3, :cond_a

    .line 1373
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_b
    move-object v0, v1

    .line 1379
    :goto_8
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1380
    iget-object v1, p0, Lcom/dropbox/android/service/z;->e:Lcom/dropbox/android/taskqueue/U;

    invoke-virtual {v1, v0, v8}, Lcom/dropbox/android/taskqueue/U;->a(Ljava/util/List;Z)V

    .line 1384
    :cond_c
    return-void

    :cond_d
    move-object v0, v6

    goto :goto_8

    :cond_e
    move-object v1, v5

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1276
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/service/z;->a(Z)V

    .line 1277
    return-void
.end method

.method public final a(Lcom/dropbox/android/service/m;)V
    .locals 2

    .prologue
    .line 1261
    iget v0, p0, Lcom/dropbox/android/service/z;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/service/z;->a:I

    .line 1262
    iget-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1264
    iget v0, p0, Lcom/dropbox/android/service/z;->a:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/service/z;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_1

    .line 1265
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/service/z;->a(Z)V

    .line 1267
    :cond_1
    return-void
.end method
