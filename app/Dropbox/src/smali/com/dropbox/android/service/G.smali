.class public final Lcom/dropbox/android/service/G;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static b:Lcom/dropbox/android/service/G;


# instance fields
.field final a:Landroid/content/Context;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/dropbox/android/service/I;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/dropbox/android/service/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/service/G;->b:Lcom/dropbox/android/service/G;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/G;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/service/G;->e:Lcom/dropbox/android/service/J;

    .line 98
    iput-object p1, p0, Lcom/dropbox/android/service/G;->a:Landroid/content/Context;

    .line 99
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/dropbox/android/service/G;->c:Landroid/net/ConnectivityManager;

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/service/G;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/service/DropboxNetworkReceiver;->a(Landroid/content/Context;Z)V

    .line 103
    return-void
.end method

.method public static a()Lcom/dropbox/android/service/G;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/dropbox/android/service/G;->b:Lcom/dropbox/android/service/G;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 120
    :cond_0
    sget-object v0, Lcom/dropbox/android/service/G;->b:Lcom/dropbox/android/service/G;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/dropbox/android/service/G;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/dropbox/android/service/G;->b:Lcom/dropbox/android/service/G;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/dropbox/android/service/G;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/G;-><init>(Landroid/content/Context;)V

    .line 109
    sput-object v0, Lcom/dropbox/android/service/G;->b:Lcom/dropbox/android/service/G;

    .line 110
    return-object v0

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 236
    packed-switch p0, :pswitch_data_0

    .line 243
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 241
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e()V
    .locals 4

    .prologue
    .line 210
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/service/G;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/I;

    .line 213
    invoke-interface {v0}, Lcom/dropbox/android/service/I;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    :cond_0
    iget-object v3, p0, Lcom/dropbox/android/service/G;->e:Lcom/dropbox/android/service/J;

    invoke-interface {v0, v3}, Lcom/dropbox/android/service/I;->a(Lcom/dropbox/android/service/J;)V

    goto :goto_0

    .line 220
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/service/G;->e:Lcom/dropbox/android/service/J;

    .line 222
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    .line 223
    new-instance v3, Lcom/dropbox/android/service/H;

    invoke-direct {v3, p0, v1, v0, v2}, Lcom/dropbox/android/service/H;-><init>(Lcom/dropbox/android/service/G;Ljava/util/ArrayList;Lcom/dropbox/android/service/J;Ljava/util/Timer;)V

    const-wide/16 v0, 0xbb8

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 233
    :cond_2
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/dropbox/android/service/I;)V
    .locals 2

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/service/G;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/service/G;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/service/DropboxNetworkReceiver;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-void

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/dropbox/android/service/J;
    .locals 1

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/service/G;->d()V

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/service/G;->e:Lcom/dropbox/android/service/J;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/dropbox/android/service/I;)V
    .locals 2

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/service/G;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/dropbox/android/service/G;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/service/G;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/service/DropboxNetworkReceiver;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    monitor-exit p0

    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/dropbox/android/service/J;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 169
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/service/G;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v7

    .line 171
    new-instance v0, Lcom/dropbox/android/service/J;

    const/4 v5, 0x0

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/service/J;-><init>(ZZZZLcom/dropbox/android/service/H;)V

    .line 172
    if-eqz v7, :cond_0

    .line 173
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    invoke-static {v0, v6}, Lcom/dropbox/android/service/J;->a(Lcom/dropbox/android/service/J;Z)Z

    .line 176
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 177
    invoke-static {v0, v6}, Lcom/dropbox/android/service/J;->b(Lcom/dropbox/android/service/J;Z)Z

    .line 184
    :cond_0
    :goto_0
    return-object v0

    .line 179
    :cond_1
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    invoke-static {v2}, Lcom/dropbox/android/service/G;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    move v1, v6

    :cond_2
    invoke-static {v0, v1}, Lcom/dropbox/android/service/J;->c(Lcom/dropbox/android/service/J;Z)Z

    .line 180
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/service/J;->d(Lcom/dropbox/android/service/J;Z)Z

    goto :goto_0
.end method

.method final declared-synchronized d()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/service/G;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v8

    .line 189
    new-instance v0, Lcom/dropbox/android/service/J;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/service/J;-><init>(ZZZZLcom/dropbox/android/service/H;)V

    .line 190
    if-eqz v8, :cond_0

    .line 191
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/service/J;->a(Lcom/dropbox/android/service/J;Z)Z

    .line 194
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 195
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/dropbox/android/service/J;->b(Lcom/dropbox/android/service/J;Z)Z

    .line 203
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/service/G;->e:Lcom/dropbox/android/service/J;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/J;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 204
    iput-object v0, p0, Lcom/dropbox/android/service/G;->e:Lcom/dropbox/android/service/J;

    .line 205
    invoke-direct {p0}, Lcom/dropbox/android/service/G;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :cond_1
    monitor-exit p0

    return-void

    .line 197
    :cond_2
    :try_start_1
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    invoke-static {v1}, Lcom/dropbox/android/service/G;->a(I)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v6

    :goto_1
    invoke-static {v0, v1}, Lcom/dropbox/android/service/J;->c(Lcom/dropbox/android/service/J;Z)Z

    .line 198
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/service/J;->d(Lcom/dropbox/android/service/J;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v1, v7

    .line 197
    goto :goto_1
.end method
