.class public final Lcom/dropbox/android/g;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Ldagger/ObjectGraph;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/g;->a:Ldagger/ObjectGraph;

    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 14
    const-class v1, Lcom/dropbox/android/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/g;->a:Ldagger/ObjectGraph;

    invoke-virtual {v0, p0}, Ldagger/ObjectGraph;->inject(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    monitor-exit v1

    return-void

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a()Z
    .locals 2

    .prologue
    .line 37
    const-class v1, Lcom/dropbox/android/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/g;->a:Ldagger/ObjectGraph;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 22
    const-class v1, Lcom/dropbox/android/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/g;->a:Ldagger/ObjectGraph;

    if-eqz v0, :cond_0

    .line 23
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Setting the module twice!"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 25
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v0, v2

    invoke-static {v0}, Ldagger/ObjectGraph;->create([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v0

    .line 33
    sput-object v0, Lcom/dropbox/android/g;->a:Ldagger/ObjectGraph;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 34
    monitor-exit v1

    return-void
.end method
