.class public Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030099

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 23
    const v0, 0x7f07019b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 24
    iget-object v1, p2, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    iget-boolean v0, p2, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->c:Z

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Lcom/dropbox/android/sharedfolder/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/dropbox/android/sharedfolder/c;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    :cond_0
    return-void
.end method
