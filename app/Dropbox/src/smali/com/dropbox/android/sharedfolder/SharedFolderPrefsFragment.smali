.class public Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;
.super Landroid/support/v4/app/Fragment;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 131
    return-void
.end method

.method static a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;Z)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30
    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 31
    const-string v1, "EXTRA_IS_BUSINESS"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 33
    new-instance v1, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-direct {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;-><init>()V

    .line 34
    invoke-virtual {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 35
    return-object v1
.end method

.method static synthetic a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/widget/CustomTwoLineView;Lcom/dropbox/android/sharedfolder/q;)V
    .locals 1

    .prologue
    .line 143
    invoke-interface {p2}, Lcom/dropbox/android/sharedfolder/q;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/dropbox/android/sharedfolder/n;

    invoke-direct {v0, p0, p2, p1}, Lcom/dropbox/android/sharedfolder/n;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;Lcom/dropbox/android/sharedfolder/q;Lcom/dropbox/android/widget/CustomTwoLineView;)V

    invoke-virtual {p1, v0}, Lcom/dropbox/android/widget/CustomTwoLineView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/dropbox/android/sharedfolder/p;

    const-string v1, "Instantiating activity must be an OnPreferencesChangedListener!"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 44
    if-nez p1, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 53
    const v0, 0x7f03009a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 54
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "EXTRA_IS_BUSINESS"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 57
    const v0, 0x7f07019c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/CustomTwoLineView;

    .line 58
    new-instance v3, Lcom/dropbox/android/sharedfolder/l;

    invoke-direct {v3, p0, v2}, Lcom/dropbox/android/sharedfolder/l;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;Z)V

    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/widget/CustomTwoLineView;Lcom/dropbox/android/sharedfolder/q;)V

    .line 93
    const v0, 0x7f07019d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/CustomTwoLineView;

    .line 94
    if-eqz v2, :cond_0

    .line 95
    new-instance v2, Lcom/dropbox/android/sharedfolder/m;

    invoke-direct {v2, p0}, Lcom/dropbox/android/sharedfolder/m;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)V

    invoke-direct {p0, v0, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/widget/CustomTwoLineView;Lcom/dropbox/android/sharedfolder/q;)V

    .line 125
    :goto_0
    return-object v1

    .line 122
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/CustomTwoLineView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 179
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 180
    return-void
.end method
