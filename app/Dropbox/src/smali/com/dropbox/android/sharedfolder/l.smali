.class final Lcom/dropbox/android/sharedfolder/l;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/sharedfolder/q;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;Z)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    iput-boolean p2, p0, Lcom/dropbox/android/sharedfolder/l;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 61
    if-ne p1, v0, :cond_0

    .line 62
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-static {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b(Z)V

    .line 63
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()[Ljava/lang/String;
    .locals 6

    .prologue
    const v5, 0x7f0d010e

    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/l;->a:Z

    if-eqz v0, :cond_0

    .line 71
    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v2, 0x7f0d010f

    invoke-virtual {v1, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-virtual {v1, v5}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v2, 0x7f0d010d

    invoke-virtual {v1, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-virtual {v1, v5}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v1, 0x7f0d010e

    invoke-virtual {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/l;->a:Z

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v1, 0x7f0d010f

    invoke-virtual {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/l;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
