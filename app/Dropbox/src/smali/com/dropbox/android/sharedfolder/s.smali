.class public final Lcom/dropbox/android/sharedfolder/s;
.super Lcom/android/ex/chips/a;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/sync/android/ai;

.field private final b:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/sync/android/ai;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/android/ex/chips/a;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p2, p0, Lcom/dropbox/android/sharedfolder/s;->a:Lcom/dropbox/sync/android/ai;

    .line 33
    iput-object p3, p0, Lcom/dropbox/android/sharedfolder/s;->b:[Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 38
    new-instance v2, Landroid/database/MatrixCursor;

    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/s;->b:[Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/s;->a:Lcom/dropbox/sync/android/ai;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/ai;->a(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 48
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/DbxContact;

    .line 50
    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxContact;->b()Lcom/dropbox/sync/android/ah;

    move-result-object v1

    sget-object v4, Lcom/dropbox/sync/android/ah;->b:Lcom/dropbox/sync/android/ah;

    if-eq v1, v4, :cond_1

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxContact;->b()Lcom/dropbox/sync/android/ah;

    move-result-object v1

    sget-object v4, Lcom/dropbox/sync/android/ah;->c:Lcom/dropbox/sync/android/ah;

    if-ne v1, v4, :cond_0

    .line 52
    :cond_1
    const-string v1, ""

    .line 53
    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxContact;->c()Z

    move-result v4

    if-nez v4, :cond_2

    .line 54
    const-string v1, "NON TEAM"

    .line 57
    :cond_2
    const/16 v4, 0x23

    .line 59
    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxContact;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxContact;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x3

    aput-object v1, v5, v0

    const/4 v0, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x6

    const/4 v1, 0x0

    aput-object v1, v5, v0

    const/4 v0, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 71
    :goto_1
    return-object v0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method
