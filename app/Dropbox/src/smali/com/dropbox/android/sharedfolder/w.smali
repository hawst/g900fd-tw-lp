.class final Lcom/dropbox/android/sharedfolder/w;
.super Landroid/support/v4/content/c;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/c",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Lcom/dropbox/sync/android/ai;

.field private final g:Lcom/dropbox/android/service/A;

.field private final h:Lcom/dropbox/android/util/analytics/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/sync/android/ai;Lcom/dropbox/android/service/A;Lcom/dropbox/android/util/analytics/r;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Landroid/support/v4/content/c;-><init>(Landroid/content/Context;)V

    .line 112
    iput-object p2, p0, Lcom/dropbox/android/sharedfolder/w;->f:Lcom/dropbox/sync/android/ai;

    .line 113
    iput-object p3, p0, Lcom/dropbox/android/sharedfolder/w;->g:Lcom/dropbox/android/service/A;

    .line 114
    iput-object p4, p0, Lcom/dropbox/android/sharedfolder/w;->h:Lcom/dropbox/android/util/analytics/r;

    .line 115
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/w;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 6

    .prologue
    .line 120
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 121
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/w;->h:Lcom/dropbox/android/util/analytics/r;

    const-string v2, "mobile-contacts-local-upload"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "upload-contacts"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/w;->g:Lcom/dropbox/android/service/A;

    invoke-virtual {v0}, Lcom/dropbox/android/service/A;->a()Ljava/util/List;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/C;

    .line 124
    new-instance v3, Lcom/dropbox/sync/android/DbxContact;

    iget-object v4, v0, Lcom/dropbox/android/service/C;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/dropbox/android/service/C;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/dropbox/sync/android/DbxContact;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    invoke-static {v0}, Lcom/dropbox/sync/android/bx;->a(Lcom/dropbox/sync/android/DbxException;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 128
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/w;->f:Lcom/dropbox/sync/android/ai;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/ai;->a(Ljava/util/List;)V

    .line 129
    const-string v0, "com.dropbox.android.sharedfolder.SharedFolderSendInviteActivity.CONTACT_SUCCESS"
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
