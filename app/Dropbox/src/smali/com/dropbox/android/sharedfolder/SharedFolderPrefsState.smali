.class public Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/dropbox/android/sharedfolder/r;

    invoke-direct {v0}, Lcom/dropbox/android/sharedfolder/r;-><init>()V

    sput-object v0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    .line 22
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    .line 36
    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0

    :cond_1
    move v1, v2

    .line 35
    goto :goto_1
.end method

.method public constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    .line 30
    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    .line 44
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    .line 52
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 89
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 92
    :cond_1
    check-cast p1, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 93
    iget-boolean v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 95
    iget-boolean v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 98
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 103
    .line 105
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 107
    return v0

    :cond_0
    move v0, v2

    .line 105
    goto :goto_0

    :cond_1
    move v1, v2

    .line 106
    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    return-void

    :cond_0
    move v0, v2

    .line 122
    goto :goto_0

    :cond_1
    move v1, v2

    .line 123
    goto :goto_1
.end method
