.class final Lcom/dropbox/android/sharedfolder/x;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/android/ex/chips/M;


# instance fields
.field private final a:Lcom/dropbox/android/sharedfolder/y;

.field private final b:Landroid/widget/AutoCompleteTextView$Validator;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/sharedfolder/y;Landroid/widget/AutoCompleteTextView$Validator;I)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/x;->a:Lcom/dropbox/android/sharedfolder/y;

    .line 80
    iput-object p2, p0, Lcom/dropbox/android/sharedfolder/x;->b:Landroid/widget/AutoCompleteTextView$Validator;

    .line 81
    iput p3, p0, Lcom/dropbox/android/sharedfolder/x;->c:I

    .line 82
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/x;->b:Landroid/widget/AutoCompleteTextView$Validator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/x;->b:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    :cond_0
    sget-object v0, Lcom/android/ex/chips/K;->c:Lcom/android/ex/chips/K;

    .line 99
    :goto_0
    return-object v0

    .line 91
    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v0

    iget v2, p0, Lcom/dropbox/android/sharedfolder/x;->c:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const-string v0, "NON TEAM"

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/x;->a:Lcom/dropbox/android/sharedfolder/y;

    sget-object v1, Lcom/dropbox/android/sharedfolder/y;->c:Lcom/dropbox/android/sharedfolder/y;

    if-ne v0, v1, :cond_3

    .line 94
    sget-object v0, Lcom/android/ex/chips/K;->c:Lcom/android/ex/chips/K;

    goto :goto_0

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/x;->a:Lcom/dropbox/android/sharedfolder/y;

    sget-object v1, Lcom/dropbox/android/sharedfolder/y;->b:Lcom/dropbox/android/sharedfolder/y;

    if-ne v0, v1, :cond_4

    .line 96
    sget-object v0, Lcom/android/ex/chips/K;->b:Lcom/android/ex/chips/K;

    goto :goto_0

    .line 99
    :cond_4
    sget-object v0, Lcom/android/ex/chips/K;->a:Lcom/android/ex/chips/K;

    goto :goto_0
.end method
