.class final Lcom/dropbox/android/sharedfolder/b;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    invoke-direct {v0, p1}, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final a(I)[Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;
    .locals 1

    .prologue
    .line 47
    new-array v0, p1, [Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/dropbox/android/sharedfolder/b;->a(Landroid/os/Parcel;)Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/dropbox/android/sharedfolder/b;->a(I)[Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    move-result-object v0

    return-object v0
.end method
