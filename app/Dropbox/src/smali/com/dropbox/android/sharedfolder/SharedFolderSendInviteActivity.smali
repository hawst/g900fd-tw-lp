.class public Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/dropbox/android/sharedfolder/p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/dropbox/android/sharedfolder/p;"
    }
.end annotation


# instance fields
.field private a:Lcom/dropbox/android/filemanager/LocalEntry;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

.field private f:Lcom/actionbarsherlock/view/MenuItem;

.field private g:Z

.field private h:Lcom/dropbox/android/widget/ContactEditTextView;

.field private i:Landroid/widget/EditText;

.field private j:Lcom/dropbox/sync/android/aV;

.field private k:Lcom/dropbox/android/service/A;

.field private l:Lcom/dropbox/sync/android/ai;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/TextView;

.field private o:Lcom/dropbox/android/util/analytics/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g:Z

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/widget/ContactEditTextView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    return-object v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f:Lcom/actionbarsherlock/view/MenuItem;

    invoke-interface {v0, p1}, Lcom/actionbarsherlock/view/MenuItem;->setEnabled(Z)Lcom/actionbarsherlock/view/MenuItem;

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f:Lcom/actionbarsherlock/view/MenuItem;

    invoke-interface {v0}, Lcom/actionbarsherlock/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p1, :cond_0

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 409
    return-void

    .line 408
    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/sync/android/aV;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->j:Lcom/dropbox/sync/android/aV;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)I
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f()I

    move-result v0

    return v0
.end method

.method private e()Lcom/dropbox/android/sharedfolder/y;
    .locals 3

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v2

    .line 287
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/a;->z()Ldbxyzptlk/db231222/s/h;

    move-result-object v0

    move-object v1, v0

    .line 288
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v0

    .line 289
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v2

    .line 291
    if-nez v0, :cond_2

    .line 292
    sget-object v0, Lcom/dropbox/android/sharedfolder/y;->a:Lcom/dropbox/android/sharedfolder/y;

    .line 297
    :goto_2
    return-object v0

    .line 287
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/s/h;->a:Ldbxyzptlk/db231222/s/h;

    move-object v1, v0

    goto :goto_0

    .line 288
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 294
    :cond_2
    if-nez v2, :cond_3

    sget-object v0, Ldbxyzptlk/db231222/s/h;->d:Ldbxyzptlk/db231222/s/h;

    if-ne v1, v0, :cond_4

    .line 295
    :cond_3
    sget-object v0, Lcom/dropbox/android/sharedfolder/y;->c:Lcom/dropbox/android/sharedfolder/y;

    goto :goto_2

    .line 297
    :cond_4
    sget-object v0, Lcom/dropbox/android/sharedfolder/y;->b:Lcom/dropbox/android/sharedfolder/y;

    goto :goto_2
.end method

.method private f()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 415
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 416
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v1

    .line 417
    :goto_0
    if-nez v1, :cond_2

    .line 418
    const/4 v1, -0x1

    .line 426
    :cond_0
    return v1

    :cond_1
    move v1, v0

    .line 416
    goto :goto_0

    .line 421
    :cond_2
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/ContactEditTextView;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 422
    const-string v3, "NON TEAM"

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 423
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 425
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method static synthetic f(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 446
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->finish()V

    .line 451
    :goto_0
    return-void

    .line 449
    :cond_0
    invoke-static {}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity$ConfirmLeaveDialog;->a()Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity$ConfirmLeaveDialog;

    move-result-object v0

    .line 450
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity$ConfirmLeaveDialog;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/content/p;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 474
    return-void
.end method

.method public final a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 483
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cb()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is_new"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "changed"

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {p1, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "is_only_owner_can_invite_perm"

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 487
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 488
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    const-string v1, "dfb_is_team_only_perm"

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 491
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 493
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 494
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    new-instance v1, Lcom/dropbox/android/sharedfolder/x;

    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e()Lcom/dropbox/android/sharedfolder/y;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ContactEditTextView;->getValidator()Landroid/widget/AutoCompleteTextView$Validator;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v4}, Lcom/dropbox/android/widget/ContactEditTextView;->r()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/dropbox/android/sharedfolder/x;-><init>(Lcom/dropbox/android/sharedfolder/y;Landroid/widget/AutoCompleteTextView$Validator;I)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ContactEditTextView;->setContactStatusResolver(Lcom/android/ex/chips/M;)V

    .line 496
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->c()V

    .line 497
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->supportInvalidateOptionsMenu()V

    .line 498
    return-void

    .line 483
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g()V

    .line 442
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v6, 0x7f070195

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 172
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 173
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->finish()V

    .line 283
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 180
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 182
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03009b

    invoke-virtual {v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 183
    const v0, 0x7f07019f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i:Landroid/widget/EditText;

    .line 185
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aV;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->j:Lcom/dropbox/sync/android/aV;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    if-eqz p1, :cond_3

    .line 195
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 196
    const-string v0, "SIS_KEY_RECIPIENTS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->b:Ljava/util/ArrayList;

    .line 197
    const-string v0, "SIS_KEY_PREFS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 198
    const-string v0, "SIS_KEY_HIDE_PREFS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g:Z

    .line 222
    :goto_1
    const v0, 0x7f0d00fc

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v5, v5, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    aput-object v5, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 225
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g:Z

    if-nez v0, :cond_1

    .line 226
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 230
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-static {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;Z)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    move-result-object v0

    .line 232
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 233
    invoke-virtual {v1, v6, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 234
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 239
    :cond_1
    const v0, 0x7f07019e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ContactEditTextView;

    .line 241
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->v()Lcom/dropbox/sync/android/ai;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->l:Lcom/dropbox/sync/android/ai;

    .line 242
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->l:Lcom/dropbox/sync/android/ai;

    if-eqz v1, :cond_2

    .line 243
    new-instance v1, Lcom/dropbox/android/service/A;

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/dropbox/android/service/A;-><init>(Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->k:Lcom/dropbox/android/service/A;

    .line 244
    new-instance v1, Lcom/dropbox/android/sharedfolder/s;

    iget-object v5, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->l:Lcom/dropbox/sync/android/ai;

    invoke-static {}, Lcom/dropbox/android/widget/ContactEditTextView;->u()[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, p0, v5, v6}, Lcom/dropbox/android/sharedfolder/s;-><init>(Landroid/content/Context;Lcom/dropbox/sync/android/ai;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ContactEditTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 248
    :cond_2
    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    .line 249
    const v1, 0x7f0701a0

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->m:Landroid/view/View;

    .line 250
    const v1, 0x7f0701a2

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->n:Landroid/widget/TextView;

    .line 252
    new-instance v1, Lcom/dropbox/android/sharedfolder/t;

    invoke-direct {v1, p0}, Lcom/dropbox/android/sharedfolder/t;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ContactEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 275
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    new-instance v5, Lcom/dropbox/android/sharedfolder/x;

    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e()Lcom/dropbox/android/sharedfolder/y;

    move-result-object v6

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->getValidator()Landroid/widget/AutoCompleteTextView$Validator;

    move-result-object v7

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->r()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/dropbox/android/sharedfolder/x;-><init>(Lcom/dropbox/android/sharedfolder/y;Landroid/widget/AutoCompleteTextView$Validator;I)V

    invoke-virtual {v1, v5}, Lcom/dropbox/android/widget/ContactEditTextView;->setContactStatusResolver(Lcom/android/ex/chips/M;)V

    .line 278
    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/ContactEditTextView;->setFocusableInTouchMode(Z)V

    .line 279
    invoke-virtual {p0, v4}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->setContentView(Landroid/view/View;)V

    .line 281
    invoke-static {}, Lcom/dropbox/android/util/analytics/r;->a()Lcom/dropbox/android/util/analytics/r;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->o:Lcom/dropbox/android/util/analytics/r;

    .line 282
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v9, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    goto/16 :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    .line 188
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->finish()V

    goto/16 :goto_0

    .line 201
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->b:Ljava/util/ArrayList;

    .line 203
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 204
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    if-nez v0, :cond_4

    .line 205
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t invite to shared folder without LocalEntry."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_4
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 211
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 212
    iput-boolean v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g:Z

    .line 213
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bZ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "is_new"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_1

    .line 216
    :cond_5
    new-instance v0, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-direct {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 217
    iput-boolean v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g:Z

    .line 218
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bZ()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "is_new"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_1

    :cond_6
    move v0, v3

    .line 227
    goto/16 :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 464
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->l:Lcom/dropbox/sync/android/ai;

    if-nez v0, :cond_0

    .line 465
    const/4 v0, 0x0

    .line 467
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/dropbox/android/sharedfolder/w;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->l:Lcom/dropbox/sync/android/ai;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->k:Lcom/dropbox/android/service/A;

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->o:Lcom/dropbox/android/util/analytics/r;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/dropbox/android/sharedfolder/w;-><init>(Landroid/content/Context;Lcom/dropbox/sync/android/ai;Lcom/dropbox/android/service/A;Lcom/dropbox/android/util/analytics/r;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 304
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z

    .line 305
    const v0, 0x7f0d00fb

    invoke-interface {p1, v3, v3, v3, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIII)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02026f

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/sharedfolder/v;

    invoke-direct {v1, p0}, Lcom/dropbox/android/sharedfolder/v;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)V

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setOnMenuItemClickListener(Lcom/actionbarsherlock/view/MenuItem$OnMenuItemClickListener;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f:Lcom/actionbarsherlock/view/MenuItem;

    .line 345
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f:Lcom/actionbarsherlock/view/MenuItem;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 349
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    if-eqz v0, :cond_b

    .line 350
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->a()Ljava/util/List;

    move-result-object v0

    .line 351
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/ContactEditTextView;->d()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 352
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 354
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 355
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->h:Lcom/dropbox/android/widget/ContactEditTextView;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ContactEditTextView;->getValidator()Landroid/widget/AutoCompleteTextView$Validator;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 398
    :goto_1
    invoke-direct {p0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a(Z)V

    .line 403
    :goto_2
    return v2

    :cond_1
    move v1, v3

    .line 351
    goto :goto_0

    .line 360
    :cond_2
    if-nez v1, :cond_5

    .line 362
    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f()I

    move-result v4

    .line 363
    if-lez v4, :cond_4

    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e()Lcom/dropbox/android/sharedfolder/y;

    move-result-object v4

    sget-object v5, Lcom/dropbox/android/sharedfolder/y;->c:Lcom/dropbox/android/sharedfolder/y;

    if-ne v4, v5, :cond_4

    .line 364
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->n:Landroid/widget/TextView;

    const v4, 0x7f0d0143

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 369
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 365
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 367
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->n:Landroid/widget/TextView;

    const v4, 0x7f0d0142

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 373
    :cond_5
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 374
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 375
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/ex/chips/RecipientEntry;->f()Ljava/lang/String;

    move-result-object v7

    .line 376
    if-eqz v7, :cond_6

    const-string v8, "NON TEAM"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 378
    :cond_6
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 379
    add-int/lit8 v4, v4, 0x1

    .line 381
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7
    move v0, v4

    move v4, v0

    .line 384
    goto :goto_4

    .line 386
    :cond_8
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 387
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v0

    .line 389
    :goto_5
    if-lez v4, :cond_a

    if-eqz v0, :cond_a

    .line 390
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->n:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0013

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_9
    move v0, v3

    .line 387
    goto :goto_5

    .line 394
    :cond_a
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 400
    :cond_b
    invoke-direct {p0, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a(Z)V

    goto/16 :goto_2
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a(Landroid/support/v4/content/p;Ljava/lang/String;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 479
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 455
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 456
    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g()V

    .line 457
    const/4 v0, 0x1

    .line 459
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 432
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 433
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 434
    const-string v0, "SIS_KEY_PREFS"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 435
    const-string v0, "SIS_KEY_HIDE_PREFS"

    iget-boolean v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 436
    const-string v0, "SIS_KEY_RECIPIENTS"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 437
    return-void
.end method
