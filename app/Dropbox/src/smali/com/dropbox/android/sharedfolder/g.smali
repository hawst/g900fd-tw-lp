.class final Lcom/dropbox/android/sharedfolder/g;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/g;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/g;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->l()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/sharedfolder/SharedFolderManageUnshareActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_ID"

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/g;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-static {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_PATH"

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/g;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-static {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 153
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/g;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 154
    return-void
.end method
