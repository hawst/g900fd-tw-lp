.class final Lcom/dropbox/android/sharedfolder/c;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

.field final synthetic b:Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

.field final synthetic c:Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/c;->c:Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;

    iput-object p2, p0, Lcom/dropbox/android/sharedfolder/c;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    iput-object p3, p0, Lcom/dropbox/android/sharedfolder/c;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 30
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/c;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/c;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    const v4, 0x7f0d011a

    invoke-virtual {v3, v4}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/c;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    const v4, 0x7f0d011e

    invoke-virtual {v3, v4}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 35
    new-instance v2, Lcom/dropbox/android/sharedfolder/d;

    invoke-direct {v2, p0}, Lcom/dropbox/android/sharedfolder/d;-><init>(Lcom/dropbox/android/sharedfolder/c;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/c;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 54
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 55
    return-void
.end method
