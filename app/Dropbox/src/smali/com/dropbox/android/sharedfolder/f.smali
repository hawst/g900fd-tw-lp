.class final Lcom/dropbox/android/sharedfolder/f;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/f;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/f;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->l()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_PREFS"

    new-instance v2, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/f;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-static {v3}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 127
    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/f;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-static {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 128
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/f;->a:Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->l()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 129
    return-void
.end method
