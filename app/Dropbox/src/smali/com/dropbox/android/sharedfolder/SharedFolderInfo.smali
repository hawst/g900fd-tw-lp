.class public Lcom/dropbox/android/sharedfolder/SharedFolderInfo;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/sharedfolder/SharedFolderInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;

.field private g:Z

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/dropbox/android/sharedfolder/a;

    invoke-direct {v0}, Lcom/dropbox/android/sharedfolder/a;-><init>()V

    sput-object v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 57
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 58
    sget-object v4, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v4}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 59
    sget-object v4, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 61
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    .line 62
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->c:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->d:Ljava/lang/String;

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->f:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->g:Z

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->h:Z

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->e:Z

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 66
    goto :goto_0

    :cond_1
    move v0, v2

    .line 67
    goto :goto_1

    :cond_2
    move v1, v2

    .line 68
    goto :goto_2
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;)V
    .locals 7

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->c()Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->c()Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->f:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;

    .line 40
    new-instance v4, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-direct {v4, v0, v5}, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;Z)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :cond_1
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;

    .line 43
    new-instance v4, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    invoke-direct {v4, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 46
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    .line 47
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    .line 48
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->c:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->f()Lcom/dropbox/sync/android/DbxFileInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/sync/android/DbxFileInfo;->a:Lcom/dropbox/sync/android/aT;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aT;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->d:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->g:Z

    .line 51
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->h:Z

    .line 52
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->e:Z

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;)V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->h:Z

    .line 103
    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->g:Z

    .line 104
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->h:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->g:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->g:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->h:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->e:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    return-void

    :cond_0
    move v0, v2

    .line 84
    goto :goto_0

    :cond_1
    move v0, v2

    .line 85
    goto :goto_1

    :cond_2
    move v1, v2

    .line 86
    goto :goto_2
.end method
