.class public Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030099

    invoke-virtual {v0, v1, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 27
    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v0

    move v1, v0

    .line 30
    :goto_0
    const v0, 0x7f07019b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31
    iget-object v3, p2, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    const v0, 0x7f070088

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 34
    iget-boolean v3, p2, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->d:Z

    if-eqz v3, :cond_3

    .line 35
    const v1, 0x7f0d0118

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 41
    :cond_0
    :goto_1
    const-string v0, "#%06X"

    new-array v1, v6, [Ljava/lang/Object;

    const v3, 0xffffff

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080085

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    and-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d0139

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v2

    iget-object v0, p2, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 49
    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0133

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d013a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    .line 61
    :goto_2
    iget-object v1, p2, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 62
    new-instance v1, Lcom/dropbox/android/sharedfolder/A;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/dropbox/android/sharedfolder/A;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;[Ljava/lang/CharSequence;Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 28
    goto/16 :goto_0

    .line 36
    :cond_3
    if-eqz v1, :cond_0

    iget-boolean v1, p2, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->e:Z

    if-nez v1, :cond_0

    .line 37
    const v1, 0x7f0d0119

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 56
    :cond_4
    new-array v0, v6, [Ljava/lang/CharSequence;

    aput-object v1, v0, v2

    goto :goto_2
.end method
