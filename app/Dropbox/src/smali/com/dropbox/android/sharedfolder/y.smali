.class public final enum Lcom/dropbox/android/sharedfolder/y;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/sharedfolder/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/sharedfolder/y;

.field public static final enum b:Lcom/dropbox/android/sharedfolder/y;

.field public static final enum c:Lcom/dropbox/android/sharedfolder/y;

.field private static final synthetic d:[Lcom/dropbox/android/sharedfolder/y;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/dropbox/android/sharedfolder/y;

    const-string v1, "NON_TEAM"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/sharedfolder/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/sharedfolder/y;->a:Lcom/dropbox/android/sharedfolder/y;

    .line 68
    new-instance v0, Lcom/dropbox/android/sharedfolder/y;

    const-string v1, "TEAM_INVITES_OK"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/sharedfolder/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/sharedfolder/y;->b:Lcom/dropbox/android/sharedfolder/y;

    .line 69
    new-instance v0, Lcom/dropbox/android/sharedfolder/y;

    const-string v1, "TEAM_INVITES_DISALLOWED"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/sharedfolder/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/sharedfolder/y;->c:Lcom/dropbox/android/sharedfolder/y;

    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/sharedfolder/y;

    sget-object v1, Lcom/dropbox/android/sharedfolder/y;->a:Lcom/dropbox/android/sharedfolder/y;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/sharedfolder/y;->b:Lcom/dropbox/android/sharedfolder/y;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/sharedfolder/y;->c:Lcom/dropbox/android/sharedfolder/y;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/sharedfolder/y;->d:[Lcom/dropbox/android/sharedfolder/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/sharedfolder/y;
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/dropbox/android/sharedfolder/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/y;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/sharedfolder/y;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/dropbox/android/sharedfolder/y;->d:[Lcom/dropbox/android/sharedfolder/y;

    invoke-virtual {v0}, [Lcom/dropbox/android/sharedfolder/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/sharedfolder/y;

    return-object v0
.end method
