.class public Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/dropbox/android/sharedfolder/b;

    invoke-direct {v0}, Lcom/dropbox/android/sharedfolder/b;-><init>()V

    sput-object v0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->a:Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->b:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->c:Z

    .line 24
    return-void

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->a:Ljava/lang/String;

    .line 16
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->b:Ljava/lang/String;

    .line 17
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderInviteeInfo;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->c:Z

    .line 18
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 36
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
