.class public abstract Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/sync/android/aV;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Z)V
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method protected final m()Lcom/dropbox/sync/android/aV;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->a:Lcom/dropbox/sync/android/aV;

    return-object v0
.end method

.method protected final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->finish()V

    .line 80
    :goto_0
    return-void

    .line 43
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aV;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->a:Lcom/dropbox/sync/android/aV;

    .line 44
    if-eqz p1, :cond_1

    const-string v0, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    const-string v0, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->b:Ljava/lang/String;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 58
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 59
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 61
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030098

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->setContentView(Landroid/view/View;)V

    .line 63
    const v0, 0x7f070198

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const v0, 0x7f070199

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/CustomTwoLineView;

    .line 67
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setTitle(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->setDescription(Ljava/lang/String;)V

    .line 70
    const v0, 0x7f07019a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 72
    new-instance v1, Lcom/dropbox/android/sharedfolder/e;

    invoke-direct {v1, p0}, Lcom/dropbox/android/sharedfolder/e;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 47
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->b:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->b:Ljava/lang/String;

    const-string v1, "Must pass SharedFolderManageActionBase the shared folder ID."

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/dropbox/sync/android/aH; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->finish()V

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 86
    const-string v0, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_ID"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method
