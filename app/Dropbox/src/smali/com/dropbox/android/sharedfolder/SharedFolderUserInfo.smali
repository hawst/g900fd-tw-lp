.class public Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/dropbox/android/sharedfolder/z;

    invoke-direct {v0}, Lcom/dropbox/android/sharedfolder/z;-><init>()V

    sput-object v0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->a:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->b:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->c:Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->e:Z

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->d:Z

    .line 29
    return-void

    :cond_0
    move v0, v2

    .line 27
    goto :goto_0

    :cond_1
    move v1, v2

    .line 28
    goto :goto_1
.end method

.method public constructor <init>(Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;Z)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->a:Ljava/lang/String;

    .line 17
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->b:Ljava/lang/String;

    .line 18
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->c:Ljava/lang/String;

    .line 19
    invoke-virtual {p1}, Lcom/dropbox/sync/android/DbxSharedFolderUserInfo;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->e:Z

    .line 20
    iput-boolean p2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->d:Z

    .line 21
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    iget-boolean v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->d:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    return-void

    :cond_0
    move v0, v2

    .line 41
    goto :goto_0

    :cond_1
    move v1, v2

    .line 42
    goto :goto_1
.end method
