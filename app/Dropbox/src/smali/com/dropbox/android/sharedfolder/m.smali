.class final Lcom/dropbox/android/sharedfolder/m;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/sharedfolder/q;


# instance fields
.field final synthetic a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 98
    if-ne p1, v0, :cond_0

    .line 99
    :goto_0
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-static {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a(Z)V

    .line 100
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v3, 0x7f0d010d

    invoke-virtual {v2, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v3, 0x7f0d010f

    invoke-virtual {v2, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v1, 0x7f0d010f

    invoke-virtual {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/m;->a:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
