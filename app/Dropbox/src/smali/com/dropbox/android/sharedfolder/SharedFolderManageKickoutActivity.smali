.class public Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;
.super Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;
.source "panda.py"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 53
    const v0, 0x7f0d0136

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 57
    new-instance v0, Ldbxyzptlk/db231222/p/a;

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->m()Lcom/dropbox/sync/android/aV;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->n()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->b:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/p/a;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 58
    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/p/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 59
    return-void

    :cond_0
    move v5, v6

    .line 57
    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f0d0133

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 38
    const v0, 0x7f0d0134

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f0d0126

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 48
    const v0, 0x7f0d0135

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 22
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.dropbox.android.sharedfolder.SharedFolderManageKickoutActivity.extra.EXTRA_NAME_TO_KICK"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->a:Ljava/lang/String;

    .line 23
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "SharedFolderManageKickoutActivity must be passed the name of the user to kick."

    invoke-static {v0, v3}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 25
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.dropbox.android.sharedfolder.SharedFolderManageKickoutActivity.extra.EXTRA_USERID_TO_KICK"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->b:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    :goto_1
    const-string v0, "SharedFolderManageKickoutActivity must be passed the ID of the user to kick."

    invoke-static {v1, v0}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    return-void

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0

    :cond_1
    move v1, v2

    .line 26
    goto :goto_1
.end method
