.class final Lcom/dropbox/android/sharedfolder/n;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/sharedfolder/q;

.field final synthetic b:Lcom/dropbox/android/widget/CustomTwoLineView;

.field final synthetic c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;Lcom/dropbox/android/sharedfolder/q;Lcom/dropbox/android/widget/CustomTwoLineView;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/n;->c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    iput-object p2, p0, Lcom/dropbox/android/sharedfolder/n;->a:Lcom/dropbox/android/sharedfolder/q;

    iput-object p3, p0, Lcom/dropbox/android/sharedfolder/n;->b:Lcom/dropbox/android/widget/CustomTwoLineView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 149
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/n;->c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 152
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/n;->a:Lcom/dropbox/android/sharedfolder/q;

    invoke-interface {v1}, Lcom/dropbox/android/sharedfolder/q;->b()[Ljava/lang/String;

    move-result-object v1

    .line 154
    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/n;->a:Lcom/dropbox/android/sharedfolder/q;

    invoke-interface {v2}, Lcom/dropbox/android/sharedfolder/q;->a()I

    move-result v2

    .line 155
    new-instance v3, Lcom/dropbox/android/sharedfolder/o;

    invoke-direct {v3, p0}, Lcom/dropbox/android/sharedfolder/o;-><init>(Lcom/dropbox/android/sharedfolder/n;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 169
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/n;->b:Lcom/dropbox/android/widget/CustomTwoLineView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/CustomTwoLineView;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 170
    const v1, 0x7f0d0013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 171
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 172
    return-void
.end method
