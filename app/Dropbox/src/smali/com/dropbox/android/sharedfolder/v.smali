.class final Lcom/dropbox/android/sharedfolder/v;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/actionbarsherlock/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 311
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->a(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/widget/ContactEditTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ContactEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    .line 312
    array-length v0, v1

    if-nez v0, :cond_0

    move v0, v8

    .line 342
    :goto_0
    return v0

    .line 317
    :cond_0
    array-length v2, v1

    move v0, v9

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 318
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/bh;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->l()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d0142

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    move v0, v9

    .line 320
    goto :goto_0

    .line 317
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 324
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 325
    array-length v2, v1

    move v0, v9

    :goto_2
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 326
    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 329
    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->b(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->b(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 331
    :goto_3
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v0}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->c(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 332
    new-instance v0, Ldbxyzptlk/db231222/p/g;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->d(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/sync/android/aV;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v3}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->c(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    iget-object v6, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v6}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/p/g;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;I)V

    .line 335
    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/p/g;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_4
    move v0, v8

    .line 342
    goto :goto_0

    .line 329
    :cond_5
    const-string v5, ""

    goto :goto_3

    .line 337
    :cond_6
    new-instance v0, Ldbxyzptlk/db231222/p/c;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->d(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/sync/android/aV;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v3}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->c(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    iget-object v6, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v6}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->f(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/sharedfolder/v;->a:Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;

    invoke-static {v7}, Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;->e(Lcom/dropbox/android/sharedfolder/SharedFolderSendInviteActivity;)I

    move-result v7

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/db231222/p/c;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Lcom/dropbox/android/util/DropboxPath;Ljava/util/ArrayList;Ljava/lang/String;Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;I)V

    .line 340
    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/p/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_4
.end method
