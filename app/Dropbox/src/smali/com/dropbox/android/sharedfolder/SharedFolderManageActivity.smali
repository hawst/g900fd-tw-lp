.class public Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;
.super Lcom/dropbox/android/activity/base/BaseUserActivity;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/dropbox/android/sharedfolder/p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/android/activity/base/BaseUserActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ldbxyzptlk/db231222/p/i;",
        ">;",
        "Lcom/dropbox/android/sharedfolder/p;"
    }
.end annotation


# instance fields
.field private a:Lcom/dropbox/sync/android/aV;

.field private b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

.field private e:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseUserActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)Lcom/dropbox/sync/android/aV;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a:Lcom/dropbox/sync/android/aV;

    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 208
    packed-switch p1, :pswitch_data_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 210
    :pswitch_0
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    goto :goto_0

    .line 217
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->finish()V

    goto :goto_0

    .line 222
    :pswitch_2
    if-eqz p3, :cond_0

    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    .line 224
    const v0, 0x7f0d0140

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/p/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/p/i;",
            ">;",
            "Ldbxyzptlk/db231222/p/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 338
    iget-object v0, p2, Ldbxyzptlk/db231222/p/i;->a:Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    if-nez v0, :cond_2

    .line 342
    iget-object v0, p2, Ldbxyzptlk/db231222/p/i;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Ldbxyzptlk/db231222/p/i;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p2, Ldbxyzptlk/db231222/p/i;->b:Ljava/lang/String;

    .line 350
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0d023f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d0014

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 355
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 356
    new-instance v1, Lcom/dropbox/android/sharedfolder/k;

    invoke-direct {v1, p0}, Lcom/dropbox/android/sharedfolder/k;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 363
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 367
    :goto_1
    return-void

    .line 344
    :cond_0
    iget v0, p2, Ldbxyzptlk/db231222/p/i;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 345
    iget v0, p2, Ldbxyzptlk/db231222/p/i;->c:I

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 347
    :cond_1
    const v0, 0x7f0d0105

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 365
    :cond_2
    new-instance v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, p2, Ldbxyzptlk/db231222/p/i;->a:Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    invoke-direct {v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    goto :goto_1
.end method

.method public final a(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V
    .locals 10

    .prologue
    const v9, 0x7f07004b

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v8, 0x8

    .line 96
    if-nez p1, :cond_1

    .line 98
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->finish()V

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iput-object p1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    .line 105
    const v0, 0x7f030097

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->setContentView(I)V

    .line 108
    const v0, 0x7f070190

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 109
    iget-object v1, p1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    const v0, 0x7f070191

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 112
    iget-object v1, p1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0f0012

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v2, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 116
    const v1, 0x7f0700ae

    invoke-virtual {p0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 117
    iget-object v5, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-boolean v5, v5, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->e:Z

    if-nez v5, :cond_3

    .line 118
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v5, 0x7f0d0115

    invoke-virtual {p0, v5}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object v1, v2

    .line 132
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    const v0, 0x7f070196

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v3

    .line 140
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 141
    new-instance v2, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-direct {v2, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    invoke-static {v2, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;Z)Lcom/dropbox/android/sharedfolder/SharedFolderPrefsFragment;

    move-result-object v1

    .line 143
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 144
    const v3, 0x7f070195

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 145
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 147
    new-instance v1, Lcom/dropbox/android/sharedfolder/g;

    invoke-direct {v1, p0}, Lcom/dropbox/android/sharedfolder/g;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    :goto_3
    const v0, 0x7f070197

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 161
    new-instance v1, Lcom/dropbox/android/sharedfolder/h;

    invoke-direct {v1, p0}, Lcom/dropbox/android/sharedfolder/h;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    const v0, 0x7f070192

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move v2, v4

    .line 173
    :goto_4
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_6

    .line 174
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;

    .line 175
    new-instance v3, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderUserRowView;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;)V

    .line 176
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 177
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v2, v1, :cond_2

    .line 178
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 122
    :cond_3
    new-instance v5, Lcom/dropbox/android/sharedfolder/f;

    invoke-direct {v5, p0}, Lcom/dropbox/android/sharedfolder/f;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;)V

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, v2

    goto/16 :goto_1

    :cond_4
    move v1, v4

    .line 137
    goto/16 :goto_2

    .line 157
    :cond_5
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 183
    :cond_6
    const v0, 0x7f070194

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 184
    :goto_5
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_8

    .line 185
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;

    .line 186
    new-instance v2, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;

    invoke-direct {v2, p0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeRowView;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V

    .line 187
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 188
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v4, v1, :cond_7

    .line 189
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 184
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 193
    :cond_8
    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v1, v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    const v1, 0x7f070193

    invoke-virtual {p0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 195
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected final a(Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V
    .locals 4

    .prologue
    .line 233
    new-instance v0, Ldbxyzptlk/db231222/p/f;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a:Lcom/dropbox/sync/android/aV;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Ldbxyzptlk/db231222/p/f;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/p/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 235
    return-void
.end method

.method public final a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 375
    new-instance v1, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-direct {v1, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    .line 376
    if-eqz p1, :cond_3

    .line 378
    invoke-virtual {v1, p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 380
    new-instance v2, Ldbxyzptlk/db231222/p/m;

    iget-object v3, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a:Lcom/dropbox/sync/android/aV;

    iget-object v4, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v4, v4, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-direct {v2, p0, v3, v4, p1}, Ldbxyzptlk/db231222/p/m;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;)V

    .line 382
    new-array v3, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/p/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 383
    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {v2, p1}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a(Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;)V

    .line 386
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cb()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "is_new"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "shared_folder_id"

    iget-object v4, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v4, v4, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "changed"

    invoke-virtual {v1, p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "is_only_owner_can_invite_perm"

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 391
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 392
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 393
    const-string v1, "dfb_is_team_only_perm"

    invoke-virtual {p1}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 395
    :cond_2
    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 397
    :cond_3
    return-void
.end method

.method protected final a(Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;)V
    .locals 3

    .prologue
    .line 280
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/sharedfolder/SharedFolderManageKickoutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 281
    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_ID"

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageKickoutActivity.extra.EXTRA_NAME_TO_KICK"

    iget-object v2, p1, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageKickoutActivity.extra.EXTRA_USERID_TO_KICK"

    iget-object v2, p1, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 288
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 265
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 266
    const-string v1, "message/rfc822"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    const-string v1, "android.intent.extra.EMAIL"

    new-array v2, v2, [Ljava/lang/String;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->startActivity(Landroid/content/Intent;)V

    .line 270
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ch()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "had_mail_apps"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 272
    :catch_0
    move-exception v0

    .line 273
    const v0, 0x7f0d0138

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 274
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ch()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "had_mail_apps"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method

.method protected final b(Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V
    .locals 5

    .prologue
    .line 238
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 239
    new-instance v0, Lcom/dropbox/android/sharedfolder/i;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/sharedfolder/i;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;)V

    .line 249
    const/4 v2, -0x1

    const v3, 0x7f0d011e

    invoke-virtual {p0, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 253
    const/4 v2, -0x2

    const v0, 0x7f0d0013

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 257
    const v0, 0x7f0d011f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 259
    const v0, 0x7f0d0120

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/dropbox/android/sharedfolder/SharedFolderInviteeInfo;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 261
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 262
    return-void
.end method

.method protected final b(Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;)V
    .locals 5

    .prologue
    .line 291
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 292
    new-instance v0, Lcom/dropbox/android/sharedfolder/j;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/sharedfolder/j;-><init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;)V

    .line 303
    const/4 v2, -0x1

    const v3, 0x7f0d013a

    invoke-virtual {p0, v3}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 307
    const/4 v2, -0x2

    const v0, 0x7f0d0013

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 311
    const v0, 0x7f0d013b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 313
    const v0, 0x7f0d013c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/dropbox/android/sharedfolder/SharedFolderUserInfo;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 314
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 316
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 317
    return-void
.end method

.method protected final e()Z
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v0, v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iget-object v0, v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 58
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->finish()V

    .line 93
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 67
    const v0, 0x7f0d00fe

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/sync/android/aV;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a:Lcom/dropbox/sync/android/aV;
    :try_end_0
    .catch Lcom/dropbox/sync/android/aH; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    if-eqz p1, :cond_1

    .line 79
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 80
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a(Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    .line 74
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->finish()V

    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    if-nez v0, :cond_2

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t manage shared folder without LocalEntry."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_2
    const v0, 0x7f0d0104

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 89
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/p;

    .line 91
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cc()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "shared_folder_id"

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/p;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/p/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a:Lcom/dropbox/sync/android/aV;

    if-eqz v0, :cond_0

    .line 329
    new-instance v0, Ldbxyzptlk/db231222/p/h;

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a:Lcom/dropbox/sync/android/aV;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Ldbxyzptlk/db231222/p/h;-><init>(Landroid/content/Context;Lcom/dropbox/sync/android/aV;Ljava/lang/String;)V

    .line 331
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/p;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p2, Ldbxyzptlk/db231222/p/i;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->a(Landroid/support/v4/content/p;Ldbxyzptlk/db231222/p/i;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/p",
            "<",
            "Ldbxyzptlk/db231222/p/i;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 201
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 202
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 203
    const-string v0, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    iget-object v1, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->b:Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 204
    return-void
.end method
