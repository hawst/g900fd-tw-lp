.class public Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;
.super Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/util/DropboxPath;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 45
    new-instance v0, Ldbxyzptlk/db231222/p/b;

    iget-object v2, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->m()Lcom/dropbox/sync/android/aV;

    move-result-object v3

    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->n()Ljava/lang/String;

    move-result-object v4

    if-nez p1, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/p/b;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Z)V

    .line 47
    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/p/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 48
    return-void

    :cond_0
    move v5, v6

    .line 45
    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f0d012c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f0d012d

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f0d012e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f0d012f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.android.sharedfolder.SharedFolderManageActionBaseActivity.extra.SHARED_FOLDER_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    iput-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 17
    iget-object v0, p0, Lcom/dropbox/android/sharedfolder/SharedFolderManageLeaveActivity;->a:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "SharedFolderManageUnshareActivity must be passed the folder\'s path."

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 19
    invoke-super {p0, p1}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActionBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
