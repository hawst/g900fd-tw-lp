.class public final enum Lcom/dropbox/android/payments/g;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/payments/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/payments/g;

.field public static final enum b:Lcom/dropbox/android/payments/g;

.field private static final synthetic c:[Lcom/dropbox/android/payments/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 189
    new-instance v0, Lcom/dropbox/android/payments/g;

    const-string v1, "GET_DEV_PAYLOAD"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/payments/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/payments/g;->a:Lcom/dropbox/android/payments/g;

    .line 190
    new-instance v0, Lcom/dropbox/android/payments/g;

    const-string v1, "SEND_UPGRADE"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/payments/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/payments/g;->b:Lcom/dropbox/android/payments/g;

    .line 188
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/payments/g;

    sget-object v1, Lcom/dropbox/android/payments/g;->a:Lcom/dropbox/android/payments/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/payments/g;->b:Lcom/dropbox/android/payments/g;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/payments/g;->c:[Lcom/dropbox/android/payments/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/payments/g;
    .locals 1

    .prologue
    .line 188
    const-class v0, Lcom/dropbox/android/payments/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/payments/g;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/payments/g;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/dropbox/android/payments/g;->c:[Lcom/dropbox/android/payments/g;

    invoke-virtual {v0}, [Lcom/dropbox/android/payments/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/payments/g;

    return-object v0
.end method
