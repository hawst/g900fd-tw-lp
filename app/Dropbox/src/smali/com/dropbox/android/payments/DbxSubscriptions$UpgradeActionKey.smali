.class public Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static a:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/dropbox/android/payments/g;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/dropbox/android/payments/f;

    invoke-direct {v0}, Lcom/dropbox/android/payments/f;-><init>()V

    sput-object v0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->a:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/payments/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-object p1, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->b:Lcom/dropbox/android/payments/g;

    .line 198
    iput-object p2, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->c:Ljava/lang/String;

    .line 199
    return-void
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/payments/g;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->b:Lcom/dropbox/android/payments/g;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 223
    if-nez p1, :cond_1

    .line 232
    :cond_0
    :goto_0
    return v0

    .line 227
    :cond_1
    instance-of v1, p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    if-eqz v1, :cond_0

    .line 231
    check-cast p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    .line 232
    iget-object v1, p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->b:Lcom/dropbox/android/payments/g;

    iget-object v2, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->b:Lcom/dropbox/android/payments/g;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->b:Lcom/dropbox/android/payments/g;

    invoke-virtual {v1}, Lcom/dropbox/android/payments/g;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->b:Lcom/dropbox/android/payments/g;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/g;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    return-void
.end method
