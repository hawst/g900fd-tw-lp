.class public final Lcom/dropbox/android/payments/h;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field protected a:I

.field protected b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/dropbox/android/payments/v;

.field private f:Ldbxyzptlk/db231222/e/a;

.field private final g:Landroid/content/ServiceConnection;

.field private h:Z

.field private final i:Ljava/lang/String;

.field private j:Landroid/os/Handler;

.field private k:Z

.field private l:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/dropbox/android/payments/v;)V
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/payments/h;->h:Z

    .line 234
    iput-object p1, p0, Lcom/dropbox/android/payments/h;->c:Landroid/content/Context;

    .line 235
    iput-object p2, p0, Lcom/dropbox/android/payments/h;->d:Ljava/lang/String;

    .line 236
    iput-object p3, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    .line 238
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/payments/h;->i:Ljava/lang/String;

    .line 240
    new-instance v0, Lcom/dropbox/android/payments/i;

    invoke-direct {v0, p0}, Lcom/dropbox/android/payments/i;-><init>(Lcom/dropbox/android/payments/h;)V

    iput-object v0, p0, Lcom/dropbox/android/payments/h;->g:Landroid/content/ServiceConnection;

    .line 254
    return-void
.end method

.method private a(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 567
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 568
    invoke-direct {p0, v0}, Lcom/dropbox/android/payments/h;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 577
    if-nez p1, :cond_0

    .line 578
    const/4 v0, 0x0

    .line 582
    :goto_0
    return v0

    .line 579
    :cond_0
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 580
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 581
    :cond_1
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 582
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    .line 584
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected type for intent response code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/payments/h;Ldbxyzptlk/db231222/e/a;)Ldbxyzptlk/db231222/e/a;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/payments/h;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/dropbox/android/payments/h;->f()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/payments/h;Ljava/lang/String;Ldbxyzptlk/db231222/e/a;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/payments/h;->a(Ljava/lang/String;Ldbxyzptlk/db231222/e/a;)V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/e/a;)V
    .locals 10

    .prologue
    const/16 v9, -0x3f3

    .line 508
    const/4 v0, 0x0

    .line 509
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 513
    :cond_0
    const/4 v1, 0x3

    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/payments/h;->i:Ljava/lang/String;

    const-string v4, "subs"

    invoke-interface {p1, v1, v2, v4, v0}, Ldbxyzptlk/db231222/e/a;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 516
    invoke-virtual {p0, v4}, Lcom/dropbox/android/payments/h;->a(Landroid/os/Bundle;)I

    move-result v0

    .line 517
    if-nez v0, :cond_5

    .line 518
    const-string v0, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 521
    :cond_1
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3ea

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V

    .line 559
    :goto_0
    return-void

    .line 524
    :cond_2
    const-string v0, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 525
    const-string v0, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 526
    const-string v0, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 527
    new-instance v7, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v7}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    .line 528
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 529
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 530
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 531
    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/payments/h;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 532
    new-instance v1, Ldbxyzptlk/db231222/x/k;

    invoke-virtual {v7, v0}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    invoke-direct {v1, v8}, Ldbxyzptlk/db231222/x/k;-><init>(Ljava/lang/Object;)V

    .line 533
    new-instance v8, Lcom/dropbox/android/payments/x;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v1

    invoke-direct {v8, v1, v0}, Lcom/dropbox/android/payments/x;-><init>(Ldbxyzptlk/db231222/x/g;Ljava/lang/String;)V

    .line 534
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 536
    :cond_3
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3eb

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 546
    :catch_0
    move-exception v0

    .line 547
    new-instance v1, Lcom/dropbox/android/payments/u;

    const/16 v2, -0x3e9

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V

    goto :goto_0

    .line 540
    :cond_4
    :try_start_1
    const-string v0, "INAPP_CONTINUATION_TOKEN"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 556
    if-nez v0, :cond_0

    .line 558
    invoke-virtual {p0, v3}, Lcom/dropbox/android/payments/h;->b(Ljava/util/List;)V

    goto :goto_0

    .line 543
    :cond_5
    :try_start_2
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v0}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 549
    :catch_1
    move-exception v0

    .line 550
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v9, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V

    goto :goto_0

    .line 552
    :catch_2
    move-exception v0

    .line 553
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v9, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;Ldbxyzptlk/db231222/e/a;)V
    .locals 4

    .prologue
    .line 464
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 465
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 466
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 468
    const-string v2, "ITEM_ID_LIST"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 471
    const/4 v0, 0x3

    :try_start_0
    iget-object v2, p0, Lcom/dropbox/android/payments/h;->i:Ljava/lang/String;

    const-string v3, "subs"

    invoke-interface {p2, v0, v2, v3, v1}, Ldbxyzptlk/db231222/e/a;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 473
    const-string v1, "DETAILS_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    const-string v1, "DETAILS_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 475
    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->a(Ljava/util/List;)V

    .line 488
    :goto_0
    return-void

    .line 477
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->a(Landroid/os/Bundle;)I

    move-result v0

    .line 478
    if-eqz v0, :cond_1

    .line 479
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v0}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->c(Lcom/dropbox/android/payments/u;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 485
    :catch_0
    move-exception v0

    .line 486
    new-instance v1, Lcom/dropbox/android/payments/u;

    const/16 v2, -0x3e9

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->c(Lcom/dropbox/android/payments/u;)V

    goto :goto_0

    .line 481
    :cond_1
    :try_start_1
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3ea

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->c(Lcom/dropbox/android/payments/u;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/android/payments/h;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/dropbox/android/payments/h;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/payments/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/payments/h;Ldbxyzptlk/db231222/e/a;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/dropbox/android/payments/h;->a(Ldbxyzptlk/db231222/e/a;)V

    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/payments/h;)Ldbxyzptlk/db231222/e/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/payments/h;)Lcom/dropbox/android/payments/v;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/dropbox/android/payments/m;

    invoke-direct {v1, p0}, Lcom/dropbox/android/payments/m;-><init>(Lcom/dropbox/android/payments/h;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 334
    return-void
.end method


# virtual methods
.method protected final a(Landroid/os/Bundle;)I
    .locals 1

    .prologue
    .line 562
    const-string v0, "RESPONSE_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 563
    invoke-direct {p0, v0}, Lcom/dropbox/android/payments/h;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 265
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 266
    iput-boolean v2, p0, Lcom/dropbox/android/payments/h;->h:Z

    .line 267
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->l:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 268
    iget-boolean v0, p0, Lcom/dropbox/android/payments/h;->k:Z

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/dropbox/android/payments/h;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 272
    iput-boolean v2, p0, Lcom/dropbox/android/payments/h;->k:Z

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    .line 275
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/dropbox/android/payments/h;->i:Ljava/lang/String;

    const-string v4, "subs"

    move-object v3, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Ldbxyzptlk/db231222/e/a;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 350
    iput-object p3, p0, Lcom/dropbox/android/payments/h;->b:Ljava/lang/String;

    .line 351
    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->a(Landroid/os/Bundle;)I

    move-result v1

    .line 352
    if-nez v1, :cond_0

    .line 353
    const-string v1, "BUY_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 354
    iput p4, p0, Lcom/dropbox/android/payments/h;->a:I

    .line 355
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    move v2, p4

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    .line 365
    :goto_0
    return-void

    .line 358
    :cond_0
    new-instance v0, Lcom/dropbox/android/payments/u;

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 360
    :catch_0
    move-exception v0

    .line 361
    new-instance v1, Lcom/dropbox/android/payments/u;

    const/16 v2, -0x3e9

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_0

    .line 362
    :catch_1
    move-exception v0

    .line 363
    new-instance v1, Lcom/dropbox/android/payments/u;

    const/16 v2, -0x3ec

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_0
.end method

.method protected final a(Lcom/dropbox/android/payments/u;)V
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/q;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/q;-><init>(Lcom/dropbox/android/payments/h;Lcom/dropbox/android/payments/u;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 610
    :cond_0
    return-void
.end method

.method protected final a(Lcom/dropbox/android/payments/x;)V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/r;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/r;-><init>(Lcom/dropbox/android/payments/h;Lcom/dropbox/android/payments/x;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 622
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    .line 451
    if-nez v0, :cond_0

    .line 452
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3f5

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->c(Lcom/dropbox/android/payments/u;)V

    .line 461
    :goto_0
    return-void

    .line 454
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/payments/h;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/dropbox/android/payments/n;

    invoke-direct {v2, p0, p1, v0}, Lcom/dropbox/android/payments/n;-><init>(Lcom/dropbox/android/payments/h;Ljava/lang/String;Ldbxyzptlk/db231222/e/a;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 637
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/t;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/t;-><init>(Lcom/dropbox/android/payments/h;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 645
    :cond_0
    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/16 v1, -0x3ea

    const/16 v0, -0x3ed

    const/16 v5, -0x3f3

    .line 379
    iget v2, p0, Lcom/dropbox/android/payments/h;->a:I

    if-eq p1, v2, :cond_0

    .line 380
    const/4 v0, 0x0

    .line 436
    :goto_0
    return v0

    .line 383
    :cond_0
    if-eqz p3, :cond_6

    .line 384
    invoke-direct {p0, p3}, Lcom/dropbox/android/payments/h;->a(Landroid/content/Intent;)I

    move-result v2

    .line 385
    const-string v3, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 386
    const-string v4, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 388
    if-ne p2, v6, :cond_4

    if-nez v2, :cond_4

    .line 390
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 396
    invoke-virtual {p0, v3, v4}, Lcom/dropbox/android/payments/h;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    :try_start_0
    new-instance v0, Ldbxyzptlk/db231222/x/k;

    new-instance v1, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v1}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    invoke-virtual {v1, v3}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/x/k;-><init>(Ljava/lang/Object;)V

    .line 400
    new-instance v1, Lcom/dropbox/android/payments/x;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    invoke-direct {v1, v0, v3}, Lcom/dropbox/android/payments/x;-><init>(Ldbxyzptlk/db231222/x/g;Ljava/lang/String;)V

    .line 402
    invoke-virtual {v1}, Lcom/dropbox/android/payments/x;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/payments/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->a(Lcom/dropbox/android/payments/x;)V

    .line 436
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 405
    :cond_1
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3f4

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 407
    :catch_0
    move-exception v0

    .line 408
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v5, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 409
    :catch_1
    move-exception v0

    .line 410
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v5, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 413
    :cond_2
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3eb

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 416
    :cond_3
    new-instance v0, Lcom/dropbox/android/payments/u;

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 418
    :cond_4
    if-nez p2, :cond_5

    .line 419
    new-instance v1, Lcom/dropbox/android/payments/u;

    invoke-direct {v1, v0}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 421
    :cond_5
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3f2

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 426
    :cond_6
    if-nez p2, :cond_8

    move p2, v0

    .line 433
    :cond_7
    :goto_2
    new-instance v0, Lcom/dropbox/android/payments/u;

    invoke-direct {v0, p2}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->b(Lcom/dropbox/android/payments/u;)V

    goto :goto_1

    .line 428
    :cond_8
    if-ne p2, v6, :cond_7

    move p2, v1

    .line 429
    goto :goto_2
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->d:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/dropbox/android/payments/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/dropbox/android/payments/u;)V
    .locals 2

    .prologue
    .line 625
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/s;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/s;-><init>(Lcom/dropbox/android/payments/h;Lcom/dropbox/android/payments/u;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 633
    :cond_0
    return-void
.end method

.method protected final b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/payments/x;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 659
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/k;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/k;-><init>(Lcom/dropbox/android/payments/h;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 667
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 282
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 283
    iget-boolean v0, p0, Lcom/dropbox/android/payments/h;->h:Z

    return v0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 297
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 299
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    if-eqz v0, :cond_0

    .line 301
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->a(Lcom/dropbox/android/payments/u;)V

    .line 315
    :goto_0
    return-void

    .line 303
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    .line 305
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/payments/h;->l:Ljava/util/concurrent/ExecutorService;

    .line 307
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/dropbox/android/payments/h;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 310
    if-nez v0, :cond_1

    .line 311
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->a(Lcom/dropbox/android/payments/u;)V

    .line 313
    :cond_1
    iput-boolean v3, p0, Lcom/dropbox/android/payments/h;->k:Z

    goto :goto_0
.end method

.method protected final c(Lcom/dropbox/android/payments/u;)V
    .locals 2

    .prologue
    .line 648
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/j;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/j;-><init>(Lcom/dropbox/android/payments/h;Lcom/dropbox/android/payments/u;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 656
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->f:Ldbxyzptlk/db231222/e/a;

    .line 495
    if-nez v0, :cond_0

    .line 496
    new-instance v0, Lcom/dropbox/android/payments/u;

    const/16 v1, -0x3f5

    invoke-direct {v0, v1}, Lcom/dropbox/android/payments/u;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/payments/h;->d(Lcom/dropbox/android/payments/u;)V

    .line 505
    :goto_0
    return-void

    .line 498
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/payments/h;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/dropbox/android/payments/o;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/payments/o;-><init>(Lcom/dropbox/android/payments/h;Ldbxyzptlk/db231222/e/a;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected final d(Lcom/dropbox/android/payments/u;)V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/l;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/l;-><init>(Lcom/dropbox/android/payments/h;Lcom/dropbox/android/payments/u;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 678
    :cond_0
    return-void
.end method

.method protected final e()V
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->e:Lcom/dropbox/android/payments/v;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/dropbox/android/payments/h;->j:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/payments/p;

    invoke-direct {v1, p0}, Lcom/dropbox/android/payments/p;-><init>(Lcom/dropbox/android/payments/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 599
    :cond_0
    return-void
.end method
