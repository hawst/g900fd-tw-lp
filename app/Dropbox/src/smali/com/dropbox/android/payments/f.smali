.class final Lcom/dropbox/android/payments/f;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;
    .locals 3

    .prologue
    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 179
    new-instance v2, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    invoke-static {v0}, Lcom/dropbox/android/payments/g;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/payments/g;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;-><init>(Lcom/dropbox/android/payments/g;Ljava/lang/String;)V

    return-object v2
.end method

.method public final a(I)[Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;
    .locals 1

    .prologue
    .line 184
    new-array v0, p1, [Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/dropbox/android/payments/f;->a(Landroid/os/Parcel;)Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/dropbox/android/payments/f;->a(I)[Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;

    move-result-object v0

    return-object v0
.end method
