.class public final Lcom/dropbox/android/payments/d;
.super Ldbxyzptlk/db231222/l/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/l/a",
        "<",
        "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
        "Ldbxyzptlk/db231222/z/aB;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ldbxyzptlk/db231222/z/M;

.field private final b:Lcom/dropbox/android/service/a;

.field private final c:Lcom/dropbox/android/payments/x;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;Lcom/dropbox/android/payments/x;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/service/a;)V
    .locals 0

    .prologue
    .line 248
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/a;-><init>(Ljava/lang/Object;)V

    .line 249
    iput-object p2, p0, Lcom/dropbox/android/payments/d;->c:Lcom/dropbox/android/payments/x;

    .line 250
    iput-object p4, p0, Lcom/dropbox/android/payments/d;->b:Lcom/dropbox/android/service/a;

    .line 251
    iput-object p3, p0, Lcom/dropbox/android/payments/d;->a:Ldbxyzptlk/db231222/z/M;

    .line 252
    return-void
.end method


# virtual methods
.method protected final a()Ldbxyzptlk/db231222/l/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/d",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/aB;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/payments/d;->a:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Lcom/dropbox/android/payments/d;->c:Lcom/dropbox/android/payments/x;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/payments/x;)Ldbxyzptlk/db231222/z/aB;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 259
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/payments/d;->b:Lcom/dropbox/android/service/a;

    sget-object v2, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    :try_end_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    :goto_0
    :try_start_2
    invoke-static {v1}, Ldbxyzptlk/db231222/l/e;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/e;

    move-result-object v0

    .line 270
    :goto_1
    return-object v0

    .line 261
    :catch_0
    move-exception v0

    .line 263
    invoke-static {}, Lcom/dropbox/android/payments/DbxSubscriptions;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to update the account info"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 266
    :catch_1
    move-exception v0

    .line 267
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    const-string v2, "Failed to communicate the upgrade to the server"

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 270
    const/4 v0, 0x0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1
.end method
