.class final Lcom/dropbox/android/payments/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/payments/v;


# instance fields
.field final synthetic a:Lcom/dropbox/android/payments/DbxSubscriptions;


# direct methods
.method constructor <init>(Lcom/dropbox/android/payments/DbxSubscriptions;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/dropbox/android/payments/a;->a:Lcom/dropbox/android/payments/DbxSubscriptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/dropbox/android/payments/a;->a:Lcom/dropbox/android/payments/DbxSubscriptions;

    invoke-static {v0}, Lcom/dropbox/android/payments/DbxSubscriptions;->a(Lcom/dropbox/android/payments/DbxSubscriptions;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->d()V

    .line 92
    return-void
.end method

.method public final a(Lcom/dropbox/android/payments/u;)V
    .locals 1

    .prologue
    .line 96
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bz()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/payments/a;->a:Lcom/dropbox/android/payments/DbxSubscriptions;

    invoke-static {v0}, Lcom/dropbox/android/payments/DbxSubscriptions;->a(Lcom/dropbox/android/payments/DbxSubscriptions;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->a()V

    .line 98
    return-void
.end method

.method public final a(Lcom/dropbox/android/payments/x;)V
    .locals 1

    .prologue
    .line 81
    const-string v0, "This should not be called"

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    const-string v0, "This should not be called"

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final b(Lcom/dropbox/android/payments/u;)V
    .locals 1

    .prologue
    .line 86
    const-string v0, "This should not be called"

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/payments/x;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dropbox/android/payments/a;->a:Lcom/dropbox/android/payments/DbxSubscriptions;

    invoke-static {v0}, Lcom/dropbox/android/payments/DbxSubscriptions;->a(Lcom/dropbox/android/payments/DbxSubscriptions;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->a()V

    .line 57
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/dropbox/android/payments/b;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/payments/b;-><init>(Lcom/dropbox/android/payments/a;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 71
    return-void
.end method

.method public final c(Lcom/dropbox/android/payments/u;)V
    .locals 1

    .prologue
    .line 49
    const-string v0, "This should not be called"

    invoke-static {v0}, Lcom/dropbox/android/util/C;->b(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final d(Lcom/dropbox/android/payments/u;)V
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bz()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 76
    iget-object v0, p0, Lcom/dropbox/android/payments/a;->a:Lcom/dropbox/android/payments/DbxSubscriptions;

    invoke-static {v0}, Lcom/dropbox/android/payments/DbxSubscriptions;->a(Lcom/dropbox/android/payments/DbxSubscriptions;)Lcom/dropbox/android/payments/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->a()V

    .line 77
    return-void
.end method
