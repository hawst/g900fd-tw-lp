.class public final Lcom/dropbox/android/payments/DbxSubscriptions;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Lcom/dropbox/android/payments/v;

.field private final d:Lcom/dropbox/android/payments/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/dropbox/android/payments/DbxSubscriptions;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/payments/DbxSubscriptions;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;)V
    .locals 3

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/dropbox/android/payments/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/payments/a;-><init>(Lcom/dropbox/android/payments/DbxSubscriptions;)V

    iput-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->c:Lcom/dropbox/android/payments/v;

    .line 104
    iput-object p2, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->b:Ldbxyzptlk/db231222/z/M;

    .line 105
    new-instance v0, Lcom/dropbox/android/payments/h;

    invoke-static {}, Lcom/dropbox/android/payments/DbxSubscriptions;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->c:Lcom/dropbox/android/payments/v;

    invoke-direct {v0, p1, v1, v2}, Lcom/dropbox/android/payments/h;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/dropbox/android/payments/v;)V

    iput-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->d:Lcom/dropbox/android/payments/h;

    .line 106
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/payments/a;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/payments/DbxSubscriptions;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/payments/DbxSubscriptions;)Lcom/dropbox/android/payments/h;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->d:Lcom/dropbox/android/payments/h;

    return-object v0
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/dropbox/android/payments/DbxSubscriptions;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/dropbox/android/payments/DbxSubscriptions;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "XWaxlWFwpl14OfoFYD1r2/pkYYs2T+1vOwHkiRz9kT+XhzFgnwjccE4Dt8sgZmo0GSChOQwIDAQAB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/r/k;)V
    .locals 3

    .prologue
    .line 141
    if-nez p1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/s/k;->b:Ldbxyzptlk/db231222/s/k;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 149
    if-nez v0, :cond_2

    .line 150
    sget-object v0, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 152
    :cond_2
    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    .line 161
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 162
    new-instance v2, Lcom/dropbox/android/payments/c;

    invoke-direct {v2, p0, v0}, Lcom/dropbox/android/payments/c;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/dropbox/android/payments/DbxSubscriptions;)Ldbxyzptlk/db231222/z/M;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->b:Ldbxyzptlk/db231222/z/M;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/dropbox/android/payments/DbxSubscriptions;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/payments/DbxSubscriptions;->d:Lcom/dropbox/android/payments/h;

    invoke-virtual {v0}, Lcom/dropbox/android/payments/h;->c()V

    .line 110
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/payments/DbxSubscriptions;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dropbox/android/payments/DbxSubscriptions;->c()V

    return-void
.end method

.method private static d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "6r29HjxstzEuUPcTSnaWnoNQ1odfXXH8JEQ+Akd8oVnUN3ZU25ScJSJFdEEfrLVJ+XYBEWjAzAVMYHs4wtnnRr6sW9PsLSqpDnbxiy0vUu9HIPE4fsjxI16Hjqj+JEWVQzGyEuHCk0+Pfbep9b7/BNpndPIP7+tdajZat"

    return-object v0
.end method

.method private static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    const-string v0, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhkwcRmG1ZEw8OdfcH+s1YdvKPXs1or+13aO5yPHQywOIj2Mc2g224N7RcXpxRfNfPwtJHVQAfIAvcgLwQ+uq4RE2btsqtwTbgoRZSOM83y"

    return-object v0
.end method
