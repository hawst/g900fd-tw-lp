.class public final Lcom/dropbox/android/payments/u;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/analytics/m;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/payments/u;-><init>(ILjava/lang/Exception;)V

    .line 110
    return-void
.end method

.method public constructor <init>(ILjava/lang/Exception;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput p1, p0, Lcom/dropbox/android/payments/u;->a:I

    .line 114
    iput-object p2, p0, Lcom/dropbox/android/payments/u;->b:Ljava/lang/Exception;

    .line 115
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/dropbox/android/payments/u;->a:I

    return v0
.end method

.method public final a(Lcom/dropbox/android/util/analytics/l;)V
    .locals 3

    .prologue
    .line 151
    const-string v0, "error_code"

    iget v1, p0, Lcom/dropbox/android/payments/u;->a:I

    int-to-long v1, v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/payments/u;->b:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 153
    const-string v0, "exception"

    iget-object v1, p0, Lcom/dropbox/android/payments/u;->b:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 154
    const-string v0, "exception_message"

    iget-object v1, p0, Lcom/dropbox/android/payments/u;->b:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 156
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 128
    if-ne p0, p1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 131
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 135
    goto :goto_0

    .line 137
    :cond_3
    check-cast p1, Lcom/dropbox/android/payments/u;

    .line 138
    iget v2, p0, Lcom/dropbox/android/payments/u;->a:I

    iget v3, p1, Lcom/dropbox/android/payments/u;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 139
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/dropbox/android/payments/u;->a:I

    return v0
.end method
