.class public final Lcom/dropbox/android/payments/x;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/x/g;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/x/g;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    .line 27
    iput-object p2, p0, Lcom/dropbox/android/payments/x;->b:Ljava/lang/String;

    .line 28
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/payments/x;->c:Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    const-string v1, "purchaseToken"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/payments/x;->f:Ljava/lang/String;

    .line 30
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    const-string v1, "productId"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/payments/x;->d:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    const-string v1, "developerPayload"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/payments/x;->e:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    if-eqz p1, :cond_0

    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 66
    check-cast p1, Lcom/dropbox/android/payments/x;

    .line 67
    iget-object v2, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    if-nez v2, :cond_2

    .line 68
    iget-object v2, p1, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    if-nez v2, :cond_0

    .line 72
    :cond_2
    iget-object v2, p0, Lcom/dropbox/android/payments/x;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/payments/x;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/android/payments/x;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/payments/x;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/android/payments/x;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/payments/x;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/dropbox/android/payments/x;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/payments/x;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/payments/x;->a:Ldbxyzptlk/db231222/x/g;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
