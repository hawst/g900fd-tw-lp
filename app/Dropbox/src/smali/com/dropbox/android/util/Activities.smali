.class public final Lcom/dropbox/android/util/Activities;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Lcom/dropbox/android/util/c;

.field public static final b:Lcom/dropbox/android/util/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    new-instance v0, Lcom/dropbox/android/util/c;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "com.dropbox.android.activity.DropboxSendTo"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/c;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/Activities;->a:Lcom/dropbox/android/util/c;

    .line 99
    new-instance v0, Lcom/dropbox/android/util/c;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "com.dropbox.android.activity.DropboxSendTo"

    aput-object v2, v1, v3

    const-string v2, "com.google.android.apps.docs.app.SendTextToClipboardActivity"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/c;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/Activities;->b:Lcom/dropbox/android/util/c;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 137
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {}, Lcom/dropbox/android/util/bn;->d()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 138
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 139
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 140
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 141
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-direct {v2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 142
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x106000d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 143
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 144
    return-object v1
.end method

.method public static a(Lcom/dropbox/android/albums/Album;)Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;-><init>(Lcom/dropbox/android/albums/Album;)V

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;)Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/util/bz;Z)V
    .locals 3

    .prologue
    .line 402
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/dropbox/android/util/bz;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 404
    const-string v1, "EXTRA_TITLE"

    const v2, 0x7f0d01d3

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const-string v1, "EXTRA_REQUIRES_AUTH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 406
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 407
    return-void
.end method

.method public static a(Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    .line 343
    const-string v0, "com.dropbox.android.intent.extra.DROPBOX_PATH"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    return-void
.end method

.method public static a(Landroid/content/Intent;Ldbxyzptlk/db231222/r/d;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/a;->b(Ldbxyzptlk/db231222/r/d;)V

    .line 125
    invoke-virtual {p2, p0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 126
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 347
    iget-boolean v0, p2, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    .line 348
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot export a directory!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 351
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/k/f;->e()Landroid/net/Uri;

    move-result-object v1

    .line 353
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 354
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 356
    const v2, 0x10000003

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 361
    new-array v3, v7, [Landroid/content/Intent;

    aput-object v0, v3, v6

    .line 364
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    const/4 v4, 0x0

    const-class v5, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-direct {v0, v2, v4, p0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 365
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 366
    const-string v1, "EXPORT_DB_PROVIDER_URI"

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 367
    new-instance v1, Landroid/content/pm/LabeledIntent;

    const-string v2, "com.dropbox.android"

    const v4, 0x7f0d00a2

    const v5, 0x7f020223

    invoke-direct {v1, v0, v2, v4, v5}, Landroid/content/pm/LabeledIntent;-><init>(Landroid/content/Intent;Ljava/lang/String;II)V

    .line 368
    new-array v4, v7, [Landroid/content/Intent;

    aput-object v1, v4, v6

    .line 370
    const v0, 0x7f0d009e

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 372
    new-instance v0, Lcom/dropbox/android/widget/aU;

    sget-object v5, Lcom/dropbox/android/util/Activities;->a:Lcom/dropbox/android/util/c;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/aU;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;)V

    .line 374
    new-instance v1, Lcom/dropbox/android/util/a;

    invoke-direct {v1, p0, p1, p2}, Lcom/dropbox/android/util/a;-><init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aU;->a(Lcom/dropbox/android/widget/aX;)V

    .line 395
    invoke-virtual {v0}, Lcom/dropbox/android/widget/aU;->a()V

    .line 396
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/cB;Landroid/app/Activity;Lcom/dropbox/android/util/analytics/r;)V
    .locals 2

    .prologue
    .line 413
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    .line 416
    invoke-static {}, Lcom/dropbox/android/activity/auth/DropboxAuth;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/cB;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/IntroTourActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 418
    const/16 v1, 0x29

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 420
    :cond_0
    return-void
.end method

.method public static a(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 119
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/dropbox/android/filemanager/a;->b(Ldbxyzptlk/db231222/r/d;)V

    .line 120
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/Activities;->b(Landroid/content/Context;)V

    .line 121
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 131
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/base/BaseUserDialogFragment;
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    .line 227
    invoke-static {p0}, Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/dialog/SharedFolderPickerDialogFragment;

    move-result-object v0

    .line 229
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/dropbox/android/util/Activities;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/base/BaseUserDialogFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 148
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/DropboxBrowser;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 149
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 150
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 151
    return-void
.end method
