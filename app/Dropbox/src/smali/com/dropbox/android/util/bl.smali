.class public final Lcom/dropbox/android/util/bl;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static c:Lcom/dropbox/android/util/bl;


# instance fields
.field final a:Landroid/os/Handler;

.field private final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/dropbox/android/util/bm;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/bm;-><init>(Lcom/dropbox/android/util/bl;)V

    iput-object v0, p0, Lcom/dropbox/android/util/bl;->a:Landroid/os/Handler;

    .line 37
    iput-object p1, p0, Lcom/dropbox/android/util/bl;->b:Landroid/content/Context;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/util/bl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/dropbox/android/util/bl;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static a()Lcom/dropbox/android/util/bl;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/dropbox/android/util/bl;->c:Lcom/dropbox/android/util/bl;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 32
    :cond_0
    sget-object v0, Lcom/dropbox/android/util/bl;->c:Lcom/dropbox/android/util/bl;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dropbox/android/util/bl;->c:Lcom/dropbox/android/util/bl;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/dropbox/android/util/bl;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/bl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dropbox/android/util/bl;->c:Lcom/dropbox/android/util/bl;

    .line 26
    return-void

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/util/bl;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/bl;->a(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public final varargs a(I[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/util/bl;->b:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/bl;->a(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/dropbox/android/util/bl;->a:Landroid/os/Handler;

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 57
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 58
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 59
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/dropbox/android/util/bl;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/bl;->b(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/util/bl;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 76
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 77
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 78
    return-void
.end method
